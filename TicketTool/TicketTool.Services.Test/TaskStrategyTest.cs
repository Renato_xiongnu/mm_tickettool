﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TicketTool.Dal;
using TicketTool.Dal.Model;
using TicketTool.Data.Strategies;

namespace TicketTool.Data
{
    [TestClass]
    public class TaskStrategyTest
    {
        [TestMethod]
        public void GetNextTaskTest()
        {
            var provider = new DataProvider();

            provider.GetTaskAndUpdateForUser(new User {UserId = 95}, new SkillSet
            {
                SkillSetId = 3
            }, false);
        }
    }
}
