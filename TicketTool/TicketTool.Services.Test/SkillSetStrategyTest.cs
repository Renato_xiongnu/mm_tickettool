﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TicketTool.Data.Strategies;
using TicketTool.Services;

namespace TicketTool.Data
{
    [TestClass]
    public class SkillSetStrategyTest
    {
        [TestMethod]
        public void GetStatisticsForSkillSets()
        {
            var skillSetStrategy = new SkillSetStrategy(new TicketService());

            var sw = new Stopwatch();
            sw.Start();
            var stats = ServiceHelper.RunInContext(()=>skillSetStrategy.GetStatisticsForSkillSets());
            sw.Stop();
            Console.WriteLine(sw.Elapsed.TotalSeconds);

            sw.Restart();
            stats = ServiceHelper.RunInContext(() => skillSetStrategy.GetStatisticsForSkillSets());
            sw.Stop();
            Console.WriteLine(sw.Elapsed.TotalSeconds);

            Thread.Sleep(TimeSpan.FromSeconds(10));
            sw.Restart();
            stats = ServiceHelper.RunInContext(() => skillSetStrategy.GetStatisticsForSkillSets());
            sw.Stop();
            Console.WriteLine(sw.Elapsed.TotalSeconds);

            foreach (var skillSetStat in stats.OrderBy(el=>el.Name))
            {
                Console.WriteLine(@"{0}: {1} {2} {3} {4}", skillSetStat.Name, skillSetStat.TasksInQueue,
                    skillSetStat.TasksInProgress,skillSetStat.UsersCount,skillSetStat.EscalatedTasks);
                if (skillSetStat.IsClustered)
                {
                    foreach (var stat in skillSetStat.ChildStats)
                    {
                        Console.WriteLine(@"   {0}: {1} {2} {3} {4}", stat.Name, stat.TasksInQueue,
                            stat.TasksInProgress, stat.UsersCount, stat.EscalatedTasks);
                    }
                }
            }
        }

        //[TestMethod]
        //public void GetStatisticsForSkillSets_Old()
        //{
        //    var skillSetStrategy = new SkillSetStrategyOld(new TicketService());

        //    var sw = new Stopwatch();
        //    sw.Start();
        //    var stats = ServiceHelper.RunInContext(() => skillSetStrategy.GetStatisticsForSkillSets());
        //    sw.Stop();
        //    Console.WriteLine(sw.Elapsed.TotalSeconds);

        //    sw.Restart();
        //    stats = ServiceHelper.RunInContext(() => skillSetStrategy.GetStatisticsForSkillSets());
        //    sw.Stop();
        //    Console.WriteLine(sw.Elapsed.TotalSeconds);

        //    foreach (var skillSetStat in stats.OrderBy(el => el.SkillSet.Name))
        //    {
        //        Console.WriteLine(@"{0}: {1} {2} {3}", skillSetStat.SkillSet.Name, skillSetStat.TasksInQueue,
        //            skillSetStat.TasksInProgress, skillSetStat.SkillSet.Users.Count); 
        //        if (skillSetStat.IsClustered)
        //        {
        //            foreach (var stat in skillSetStat.ChildStats)
        //            {
        //                Console.WriteLine(@"   {0}: {1} {2} {3}", stat.SkillSet.Name, stat.TasksInQueue,
        //                    stat.TasksInProgress, stat.SkillSet.Users.Count);
        //            }
        //        }
        //    }
        //}
    }
}
