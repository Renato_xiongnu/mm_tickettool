﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TicketTool.Data.Contracts.Common;
using TicketTool.Data.Contracts.Operations;
using TicketTool.Data.Contracts.Tasks;
using TicketTool.Data.Strategies;

namespace TicketTool.Data
{
    [TestClass]
    public class TicketServiceMockTest
    {
        [TestMethod]
        public void ConstructorTest()
        {
            ITicketService service = new TicketServiceMock();

            Assert.IsNotNull(service.Tickets);
            Assert.IsNotNull(service.Tasks);
        }

        [TestMethod]
        public void CreateTaskTest()
        {
            ITicketService service = new TicketServiceMock();

            const int count = 1000;
            var realResults = new List<string>();

            for (var i = 0; i < count; i++)
            {
                realResults.Add(service.Tasks.CreateTask(new TicketTask()));
            }

            Assert.AreEqual(count, realResults.GroupBy(x => x).Count());
        }

        [TestMethod]
        public void CreateTaskDosNotThrowExceptionTest()
        {
            ITicketService service = new TicketServiceMock();

            service.Tasks.CreateTask(null);
        }

        [TestMethod]
        public void GetTaskTest()
        {
            ITicketService service = new TicketServiceMock();

            var taskId = service.Tasks.CreateTask(new TicketTask());
            var result = service.Tasks.GetTask(taskId);

            Assert.IsNotNull(result);
            Assert.AreEqual(TaskState.InProgress, result.State);
        }

        [TestMethod]
        public void GetTaskNotExistTaskTest()
        {
            ITicketService service = new TicketServiceMock();

            service.Tasks.CreateTask(new TicketTask());
            var result = service.Tasks.GetTask("123ABC");
            Assert.IsNull(result);
        }

        [TestMethod]
        public void CompleteTaskTest()
        {
            var service = new TicketServiceMock();
            var taskId = service.Tasks.CreateTask(new TicketTask{TaskId = Guid.NewGuid().ToString()});
            var expectedResult = new OperationResult {HasError = false, MessageText = string.Empty};
            var realResult = service.Tasks.CompleteTask(taskId, "WTF", new Collection<RequiredField>());

            Assert.IsNotNull(realResult);
            Assert.AreEqual(expectedResult.HasError, realResult.HasError);
            Assert.AreEqual(expectedResult.MessageText, realResult.MessageText); //TODO дёрнуть Callback
            Thread.Sleep(TimeSpan.FromSeconds(15));
            //mock.Verify(
            //    x =>
            //    x.Callback("IZztOrderProcessingService", It.Is<TicketTask>(y => y.TaskId != null && y.Outcome == "WTF")));
        }

        [TestMethod]
        public void CompleteTaskNotExistTaskTest()
        {
            var service = new TicketServiceMock();

            var expectedResult = new OperationResult { HasError = true, MessageText = "Error" };
            var realResult = service.Tasks.CompleteTask("123", "WTF", new Collection<RequiredField>());

            Assert.IsNotNull(realResult);
            Assert.AreEqual(expectedResult.HasError, realResult.HasError);
            Assert.AreEqual(expectedResult.MessageText, realResult.MessageText);
        }

        [TestMethod]
        public void CompleteTaskDosNotThrowExceptionTest()
        {
            var service = new TicketServiceMock();

            service.Tasks.CompleteTask("123", "WTF", new Collection<RequiredField>());
            service.Tasks.CompleteTask(string.Empty, string.Empty, null);
            service.Tasks.CompleteTask(null, null, null);
        }

        [TestMethod]
        public void CreateTicketTest()
        {
            ITicketService service = new TicketServiceMock();

            const int count = 1000;
            var realResults = new List<string>();

            for (var i = 0; i < count; i++)
            {
                realResults.Add(service.Tickets.CreateTicket("123ABC"));
            }

            Assert.AreEqual(count, realResults.GroupBy(x => x).Count());
        }

        [TestMethod]
        public void CreateTicketDosNotThrowExceptionTest()
        {
            ITicketService service = new TicketServiceMock();

            service.Tickets.CreateTicket("123ABC", "123ABC", "123ABC");
            service.Tickets.CreateTicket(string.Empty, string.Empty, string.Empty);
            service.Tickets.CreateTicket(null, null, null);
        }

        [TestMethod]
        public void CloseTicketTest()
        {
            ITicketService service = new TicketServiceMock();
            var ticketId = service.Tickets.CreateTicket("123ABC", "123ABC", "123ABC");
            service.Tickets.CloseTicket(ticketId);
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void CloseTicket_SecondTime_Test()
        {
            ITicketService service = new TicketServiceMock();
            var ticketId = service.Tickets.CreateTicket("123ABC", "123ABC", "123ABC");
            service.Tickets.CloseTicket(ticketId);
            service.Tickets.CloseTicket(ticketId);
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void CloseTicketThrowExceptionNormalStringTest()
        {
            ITicketService service = new TicketServiceMock();

            service.Tickets.CloseTicket("123ABC");
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void CloseTicketThrowExceptionNullTest()
        {
            ITicketService service = new TicketServiceMock();

            service.Tickets.CloseTicket(null);
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void CloseTicketThrowExceptionEmptyStringTest()
        {
            ITicketService service = new TicketServiceMock();

            service.Tickets.CloseTicket(string.Empty);
        }

        [TestMethod]
        public void GetTicketTest()
        {
            ITicketService service = new TicketServiceMock();
            var generagedTicketId=service.Tickets.CreateTicket("Test", "WorkItem", "Param");
            
            var ticketId = service.Tickets.GetTicket("WorkItem");

            Assert.IsTrue(!string.IsNullOrEmpty(ticketId));
            Assert.AreEqual(generagedTicketId, ticketId);
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetNotExistingTicketTest()
        {
            ITicketService service = new TicketServiceMock();

            var ticketId = service.Tickets.GetTicket("WorkItem");
        }
    }
}
