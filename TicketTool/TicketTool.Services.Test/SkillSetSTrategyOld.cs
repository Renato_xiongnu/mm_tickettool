﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Transactions;
using Newtonsoft.Json;
using TicketTool.Dal;
using TicketTool.Data.Context;
using TicketTool.Data.Contracts;
using TicketTool.Data.Contracts.Common;
using TicketTool.Data.Strategies.Contracts;
using Cluster = TicketTool.Dal.Model.Cluster;

//namespace TicketTool.Data.Strategies
//{
//    internal class SkillSetStrategyOld : TicketToolStrategyBase, ISkillSetStrategy
//    {
//        internal SkillSetStrategyOld(ITicketService ticketService)
//            : base(ticketService)
//        {
//        }

//        public void UpdateSkillSetRelation(string externalSystemId, ICollection<SkillSetRelation> addRelations)
//        {
//            var provider = TicketToolContext.Current.DataProvider;
//            provider.GetExternalSystem(externalSystemId);

//            using (var tran = new TransactionScope())
//            {
//                if (addRelations != null && addRelations.Count > 0)
//                {
//                    var skillSetRel = addRelations.ToDictionary(t => t.SkillSetName, t => t.TaskTypeName);
//                    var taskTypes = provider.GetTaskTypes(externalSystemId, skillSetRel.Values, true).ToList();
//                    var skillSets = provider.GetSkillSets(false, true).Where(t => skillSetRel.Keys.Contains(t.Name)).ToList();

//                    foreach (var newSkillSet in skillSets)
//                    {
//                        var taskTypeName = skillSetRel[newSkillSet.Name];

//                        var taskType = taskTypes.FirstOrDefault(t => t.Name == taskTypeName);
//                        if (taskType == null) continue;
//                        if (newSkillSet.IsEscalation)
//                            taskType.SlaSkillSet = newSkillSet;
//                        else
//                        {
//                            var oldSkillSet = taskType.SkillSet;
//                            taskType.SkillSet = newSkillSet;

//                            var defaultTasks = provider.GetAllTasks()
//                                                       .Where(t => t.TaskStatus < (int)Dal.Model.TaskStatus.Finished)
//                                                       .Where(t => t.CurrentAssignee == null)
//                                                       .Where(t => (t.CurrentSkillSet == null || t.CurrentSkillSet.SkillSetId == oldSkillSet.SkillSetId))
//                                                       .ToList();

//                            foreach (var task in defaultTasks)
//                            {
//                                this.TicketService.TaskOperations.InsertRemoveFromSkillSetOperation(task);
//                                task.CurrentSkillSet = newSkillSet;
//                                this.TicketService.TaskOperations.InsertAssignedToSkillSetOperation(task);
//                            }
//                        }
//                        provider.Save();
//                    }
//                }
//                tran.Complete();
//            }
//        }

//        public ICollection<SkillSet> GetSkillSets(bool withDefault = false, bool withRoles = false)
//        {
//            var provider = TicketToolContext.Current.DataProvider;
//            var skillSets = provider.GetSkillSets(false, withDefault, true, false, withRoles).ToList();

//            return skillSets.Select(t => new SkillSet
//            {
//                Name = t.Name,
//                ParamType =
//                    t.IsParameterizable
//                        ? SkillSetParamType.SapCode
//                        : SkillSetParamType.None,
//                IsEscalation = t.IsEscalation,
//                TasksDeliveredByEmail = t.DeliveryTaskByEmail,
//                Users =
//                    t.Users == null
//                        ? null
//                        : t.Users.Select(usr => usr.ToContract()).
//                            ToList(),
//                Roles = t.Roles == null ? null : t.Roles.Select(m => m.Name).ToList()
//            }).ToList();

//        }

//        public ICollection<SkillSet> CreateSkillSets(IEnumerable<SkillSet> skillSets)
//        {
//            var resultCollection = new List<SkillSet>();
//            var provider = TicketToolContext.Current.DataProvider;
//            foreach (var updSkillSet in skillSets)
//            {
//                var skillSet = provider.GetSkillSets(false, true).SingleOrDefault(t => t.Name == updSkillSet.Name);

//                if (skillSet == null)
//                {
//                    skillSet = provider.PrepareSkillSet(updSkillSet.Name);

//                    skillSet.IsParameterizable = updSkillSet.ParamType != SkillSetParamType.None;
//                    skillSet.IsEscalation = updSkillSet.IsEscalation;
//                    skillSet.DeliveryTaskByEmail = updSkillSet.TasksDeliveredByEmail;
//                    skillSet.Roles = updSkillSet.Roles == null ? provider.GetAllRoles().ToList() : provider.GetAllRoles().Where(m => updSkillSet.Roles.Contains(m.Name)).ToList();
//                }
//                else
//                {
//                    throw new InvalidOperationException(string.Format("Skillset with name: {0} already exists", updSkillSet.Name));
//                }


//                resultCollection.Add(new SkillSet
//                {
//                    Name = skillSet.Name,
//                    ParamType = skillSet.IsParameterizable ? SkillSetParamType.SapCode : SkillSetParamType.None,
//                    IsEscalation = skillSet.IsEscalation,
//                    TasksDeliveredByEmail = skillSet.DeliveryTaskByEmail,
//                    Users = skillSet.Users == null ? null
//                    : skillSet.Users.Select(usr => usr.ToContract()).ToList(),
//                    Roles = skillSet.Roles == null ? null : skillSet.Roles.Select(m => m.Name).ToList()
//                });

//                provider.Save();
//            }
//            return resultCollection;
//        }

//        public void RenameSkillSet(string oldName, string newName)
//        {
//            var provider = TicketToolContext.Current.DataProvider;
//            var s = provider.GetSkillSets().SingleOrDefault(ss => ss.Name == oldName);
//            if (s != null)
//            {
//                s.Name = newName;
//                provider.Save();
//            }
//        }

//        public ICollection<UserSkillSet> GetUserSkillSets()
//        {
//            var user = TicketToolContext.Current.DataProvider.GetUser(TicketToolContext.Current.Credentials.Name,
//                withRoles: true, withSkillSets: true, withAvailableSkillSets: true);
//            var userSkillSets = user.SkillSets
//                .Where(m => !m.IsDefault)
//                .Select(t => new UserSkillSet { Name = t.Name, IsLogged = true, IsClustered = t.IsClustered })
//                .ToDictionary(t => t.Name, t => t);
//            var availableSkillSets = user.AvailableSkillSets
//                .Where(m => !m.IsDefault)
//                .Select(t => new UserSkillSet { Name = t.Name, IsLogged = false, IsClustered = t.IsClustered })
//                .ToDictionary(t => t.Name, t => t);
//            var allSkillSets = this.GetAvailableSkillSetsForRoles(user.Roles.Select(m => m.Name).ToList())
//                .ToDictionary(t => t.Name, t => t);
//            foreach (var elem in availableSkillSets)
//            {
//                if (allSkillSets.ContainsKey(elem.Key) && !userSkillSets.ContainsKey(elem.Key))
//                    userSkillSets.Add(elem.Key, elem.Value);
//            }
//            return userSkillSets.Values;
//        }

//        public bool DeleteSkillSet(string skillSetName)
//        {
//            var provider = TicketToolContext.Current.DataProvider;
//            var ss = provider.GetSkillSets(withTaskTypes: true, withUsers: true, withRoles: true).FirstOrDefault(s => s.Name == skillSetName);
//            if (ss != null)
//            {
//                if (ss.TaskTypes.Any(tt => tt.SkillSet != null && tt.SkillSet.Name == skillSetName) ||
//                    ss.Users.Any(u => u.SkillSets.Any(s => s.Name == skillSetName)))
//                {
//                    return false;
//                }

//                if (provider.GetAllTaskOperations().Any(to => to.SkillSet != null && to.SkillSet.Name == skillSetName) ||
//                            provider.GetAllUserLogs().Any(ul => ul.SkillSet != null && ul.SkillSet.Name == skillSetName))
//                {
//                    ss.IsDeleted = true;
//                }
//                else
//                {
//                    provider.DeleteSkillSet(ss);
//                }
//                provider.Save();
//                return true;
//            }
//            return false;

//        }

//        public ICollection<SkillSetStat> GetStatisticsForSkillSets(bool withClusters = false)
//        {
//            var skillSets = TicketToolContext.Current.DataProvider.GetSkillSets(true).ToList();
//            var workSkillsets = skillSets.Where(t => !t.IsEscalation).ToList();
//            var esscalateSkillSets = skillSets.Where(t => t.IsEscalation).ToList();

//            var workResult = InitSkillSetStat(workSkillsets);
//            return workResult.Union(InitSkillSetStat(esscalateSkillSets)).ToList();
//        }

//        private static IEnumerable<SkillSetStat> InitSkillSetStat(IEnumerable<Dal.Model.SkillSet> workSkillsets)
//        {
//            var provider = TicketToolContext.Current.DataProvider;
//            var allClusters = provider.GetAllClusters().ToList();

//            var result = new List<SkillSetStat>();
//            foreach (var workSkillset in workSkillsets)
//            {
//                var skillSetStat = new SkillSetStat
//                {
//                    SkillSet = workSkillset.ToContract()
//                };

//                if (workSkillset.IsClustered)
//                {
//                    skillSetStat = UpdateFieldsFromSkillSet(provider, workSkillset, skillSetStat);
//                    skillSetStat.ChildStats = new List<SkillSetStat>();
//                    skillSetStat.IsClustered = true;

//                    foreach (var cluster in allClusters)
//                    {
//                        var clusteredSkillset =
//                            JsonConvert.DeserializeObject<SkillSetStat>(JsonConvert.SerializeObject(skillSetStat));

//                        clusteredSkillset.SkillSet.NameWithoutCluster = clusteredSkillset.SkillSet.Name;
//                        clusteredSkillset.SkillSet.Name = string.Format("{0}-{1}", clusteredSkillset.SkillSet.Name,
//                            cluster.Name);
//                        clusteredSkillset.IsClustered = true;
//                        var clusterParameterNames = cluster.Parameters.Select(t => t.ParameterName);
//                        var allTasks = provider.GetTaskStat(workSkillset)
//                            .Where(t => clusterParameterNames.Contains(t.Parameter)).ToList();

//                        var escalatedTask =
//                            provider.GetEscaltedTaskFromSkillSet(workSkillset)
//                                .Where(t => clusterParameterNames.Contains(t.Parameter)).ToList();
//                        var esclatedTaskIds = escalatedTask.Select(t => t.TaskId);
//                        var tasksInProgress =
//                            allTasks.Where(
//                                t =>
//                                    t.TaskStatus == (int)Dal.Model.TaskStatus.AssignedToUser &&
//                                    !esclatedTaskIds.Contains(t.TaskId))
//                                .ToList();

//                        var tasksInQueue =
//                            allTasks.Where(
//                                t =>
//                                    t.TaskStatus != (int)Dal.Model.TaskStatus.AssignedToUser &&
//                                    !esclatedTaskIds.Contains(t.TaskId))
//                                .ToList();

//                        clusteredSkillset.TasksInQueue = tasksInQueue.Count;
//                        clusteredSkillset.TasksInProgress = tasksInProgress.Count;
//                        clusteredSkillset.EscalatedTasks = escalatedTask.Count;
//                        clusteredSkillset.SkillSet.Users =
//                            skillSetStat.SkillSet.Users.Where(t => t.Clusters.Contains(cluster.Name)).ToList();

//                        skillSetStat.ChildStats.Add(clusteredSkillset);
//                    }

//                    result.Add(skillSetStat);
//                }
//                else
//                {
//                    result.Add(
//                        UpdateFieldsFromSkillSet(provider, workSkillset, skillSetStat));
//                }
//            }

//            return result;
//        }

//        private static SkillSetStat UpdateFieldsFromSkillSet(DataProvider provider, Dal.Model.SkillSet workSkillset, SkillSetStat skillSetStat)
//        {
//            var allTasks = provider.GetTaskStat(workSkillset).ToList();

//            var escalatedTask = provider.GetEscaltedTaskFromSkillSet(workSkillset).ToList();
//            var esclatedTaskIds = escalatedTask.Select(t => t.TaskId);
//            var tasksInProgress =
//                allTasks.Where(
//                    t =>
//                        t.TaskStatus == (int)Dal.Model.TaskStatus.AssignedToUser &&
//                        !esclatedTaskIds.Contains(t.TaskId))
//                    .ToList();

//            var tasksInQueue =
//                allTasks.Where(
//                    t =>
//                        t.TaskStatus != (int)Dal.Model.TaskStatus.AssignedToUser &&
//                        !esclatedTaskIds.Contains(t.TaskId))
//                    .ToList();

//            skillSetStat.TasksInQueue = tasksInQueue.Count;
//            skillSetStat.TasksInProgress = tasksInProgress.Count;
//            skillSetStat.EscalatedTasks = escalatedTask.Count;

//            return skillSetStat;
//        }

//        public ICollection<SkillSet> GetAvailableSkillSetsForRoles(ICollection<string> roles)
//        {
//            return TicketToolContext.Current.DataProvider.GetAllRoles(false, true)
//                           .Where(m => roles.Contains(m.Name))
//                           .ToList()
//                           .SelectMany(p => p.SkillSets)
//                           .Where(m => !m.IsDefault)
//                           .Distinct()
//                           .Select(t =>
//                                   new SkillSet
//                                   {
//                                       Name = t.Name,
//                                       ParamType = t.IsParameterizable ? SkillSetParamType.SapCode : SkillSetParamType.None,
//                                       IsEscalation = t.IsEscalation,
//                                       TasksDeliveredByEmail = t.DeliveryTaskByEmail,
//                                       Users = t.Users != null ? t.Users.Select(usr => usr.ToContract()).ToList() : null,
//                                       Roles = t.Roles != null ? t.Roles.Select(m => m.Name).ToList() : null
//                                   })
//                           .ToList();
//        }
//    }
//}