﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TicketTool.Dal;
using TicketTool.Data.Context;
using TicketTool.Data.Strategies;

namespace TicketTool.Data
{
    [TestClass]
    public class CompleteTaskTest
    {
        [TestMethod]
        public void UpdateTaskTest()
        {
            var context = new TicketToolContext();

            var fromDate = DateTime.Now.AddHours(-4);
            var provider = new DataProvider();
            var taskCompletionQueue = provider.GetAllTaskCompletionQueue().Where(t => t.CompletionStatus == 10
                                                                                      &&
                                                                                      t.ExternalSystem.ExternalSystemId ==
                                                                                      "ISmartStart2CashOrderProcessingServiceV2_1_0"
                                                                                      &&
                                                                                      t.CreateDate < fromDate);
            var cnt = taskCompletionQueue.Count();

            Parallel.ForEach(taskCompletionQueue.ToList(), (t) =>
                context.RunInServiceContext(() =>
                    new TaskStrategy(new TicketService()).CompleteTaskInExternalSystem(t)));
        }
    }
}
