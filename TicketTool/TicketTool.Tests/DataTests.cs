﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TicketTool.Data;
using TicketTool.Data.Context;

namespace TicketTool.Tests
{
    [TestClass]
    public class DataTests
    {
        [TestMethod]
        public void TestErroredQueue()
        {
            OperationContextHolder<TicketToolContext>.Current.RunInServiceContext(
                () => new TicketService().Tasks.ProcessErroredQueue());
        }
    }
}
