﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TicketTool.Tests.WF;

namespace TicketTool.Tests
{
    [TestClass]
    public class WfTests
    {
        [TestMethod]
        public void TestStart()
        {
            var client = new WorkflowTestServiceClient();
            var ticketId = client.StartProcess("123-000-321", "param-pam-pam");
            Assert.IsFalse(String.IsNullOrEmpty(ticketId));
        }
    }
}
