﻿using System.Activities;
using System.Activities.Statements;
using MMS.Activities;
using TicketTool.Activities.Properties;
using TicketTool.Activities.TicketToolProxy;

namespace TicketTool.Activities.Common
{
    public sealed class CloseTicket : NativeActivity
    {
        private ActivityAction<string> Close { get; set; }

        [RequiredArgument]
// ReSharper disable UnusedAutoPropertyAccessor.Global
        public InArgument<string> TicketId { get; set; }
// ReSharper restore UnusedAutoPropertyAccessor.Global

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            var ticketIdArg = new RuntimeArgument(Resources.Param_TicketId, typeof(string), ArgumentDirection.In, true);
            metadata.Bind(this.TicketId, ticketIdArg);
            metadata.AddArgument(ticketIdArg);

            var ticketId = new DelegateInArgument<string>();
            var ttClient = new Variable<TicketToolServiceClient>();
            this.Close =
                new ActivityAction<string>
                    {
                        Argument = ticketId,
                        Handler = new Sequence
                            {
                                Variables = {ttClient},
                                Activities =
                                    {
                                        new Failover
                                            {
                                                DisplayName = string.IsNullOrEmpty(DisplayName) ? GetType().FullName : DisplayName,
                                                Delegate = new InvokeDelegate
                                                    {
                                                        Delegate = new ActivityAction
                                                            {
                                                                Handler = new Sequence
                                                                    {
                                                                        Activities =
                                                                            {
                                                                                new Assign
                                                                                    {
                                                                                        To = new OutArgument<TicketToolServiceClient>(ttClient),
                                                                                        Value = new InArgument<TicketToolServiceClient>(c => new TicketToolServiceClient())
                                                                                    },
                                                                                new InvokeMethod
                                                                                    {
                                                                                        MethodName = "CloseTicket",
                                                                                        Parameters = {new InArgument<string>(ticketId)},
                                                                                        TargetObject = new InArgument<TicketToolServiceClient>(ttClient)
                                                                                    }
                                                                            }
                                                                    }
                                                            }
                                                    }
                                            }
                                    }
                            }
                    };
            metadata.AddDelegate(this.Close);
        }

        protected override void Execute(NativeActivityContext context)
        {
            context.ScheduleAction(this.Close, this.TicketId.Get(context));
        }
    }
}
