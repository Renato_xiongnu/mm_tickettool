﻿using System;
using System.Activities;
using System.Activities.Statements;
using System.Collections.Generic;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Activities;
using MMS.Activities;
using MMS.Activities.Extensions;
using TicketTool.Activities.Common.Design;
using TicketTool.Activities.Properties;
using TicketTool.Activities.TicketToolProxy;
using Common.Logging;

namespace TicketTool.Activities.Common
{
    [Designer(typeof(StartProcessActivityDesigner))]
    public sealed class StartProcess : NativeActivity<string>
    {
        private class StartProcessParams
        {
            public string WorkItemId { get; set; }

            public string Parameter { get; set; }

            public string TicketId { get; set; }
        }

        private ActivityFunc<CorrelationHandle, StartProcessParams> Child { get; set; }

        [RequiredArgument]
        public InArgument<CorrelationHandle> CorrelationHandle { get; set; }

        [Browsable(false)]
        public OutArgument<string> WorkItemId { get; set; }

        [Browsable(false)]
        public OutArgument<string> TicketId { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            CacheArguments(metadata);
            CacheChild(metadata);
        }

        private void CacheChild(NativeActivityMetadata metadata)
        {
            var handleArg = new DelegateInArgument<CorrelationHandle>();
            var result = new DelegateOutArgument<StartProcessParams>();
            var ticketId = new Variable<string>("TicketId");
            var workItemIdInternal = new Variable<string>("WorkItemIdInternal");
            var parameter = new Variable<string>("Parameter");

            var recieve = new Receive
            {
                CanCreateInstance = true,
                ServiceContractName = Settings.Default.ServiceContractName,
                OperationName = "StartProcess",
                Content = ReceiveContent.Create(
                    new Dictionary<string, OutArgument>
                    {
                        {"WorkItemId", new OutArgument<string>(workItemIdInternal)},
                        {"ParameterName", new OutArgument<string>(parameter)}
                    })
            };
            // ReSharper disable ImplicitlyCapturedClosure
            Child =
                new ActivityFunc<CorrelationHandle, StartProcessParams>
                {
                    Argument = handleArg,
                    Result = result,
                    Handler = new Sequence
                    {
                        Variables = {ticketId, workItemIdInternal, parameter},
                        Activities =
                        {
                            recieve,
                            new Log
                            {
                                DisplayName = "StartProcess receive log",
                                SelectedLogLevel = LogLevel.Info,
                                Text = new InArgument<string>(c=>string.Format("WorkItemId = {0}, Parameter={1}",workItemIdInternal.Get(c),parameter.Get(c)))
                            },
                            new If(c => !string.IsNullOrEmpty(workItemIdInternal.Get(c)))
                            {
                                Then = new Assign
                                {
                                    To = new OutArgument<string>(c => ticketId.Get(c)),
                                    Value =
                                        new InArgument<string>(
                                            c =>
                                                new TicketToolServiceClient().CreateTicket(
                                                    Settings.Default.ServiceContractName, workItemIdInternal.Get(c),
                                                    parameter.Get(c)))
                                },
                                Else = new Assign
                                {
                                    To = new OutArgument<string>(c => ticketId.Get(c)),
                                    Value =
                                        new InArgument<string>(
                                            c =>
                                                new TicketToolServiceClient().CreateTicket(
                                                    Settings.Default.ServiceContractName, String.Empty, parameter.Get(c)))
                                }
                            },
                            new Assign
                            {
                                To = new OutArgument<StartProcessParams>(result),
                                Value =
                                    new InArgument<StartProcessParams>(
                                        c =>
                                            new StartProcessParams
                                            {
                                                TicketId = ticketId.Get(c),
                                                WorkItemId = workItemIdInternal.Get(c),
                                                Parameter = parameter.Get(c)
                                            })
                            },
                            new SendReply
                            {
                                Request = recieve,
                                Content = new SendParametersContent
                                    (
                                    new Dictionary<string, InArgument>
                                    {
                                        {Settings.Default.TicketIdKey, new InArgument<string>(c => ticketId.Get(c))}
                                    }
                                    ),
                                CorrelationInitializers =
                                {
                                    new QueryCorrelationInitializer
                                    {
                                        CorrelationHandle = handleArg,
                                        MessageQuerySet =
                                            new MessageQuerySet
                                            {
                                                {Settings.Default.TicketIdKey, new NewTicketIdCorrelationQuery()}
                                            }
                                    }
                                }
                            }
                        }
                    }
                };
            // ReSharper restore ImplicitlyCapturedClosure
            metadata.AddDelegate(Child);
        }

        private void CacheArguments(NativeActivityMetadata metadata)
        {
            var result = new RuntimeArgument(Resources.Param_Result, typeof(string), ArgumentDirection.Out, true);
            metadata.Bind(Result, result);
            metadata.AddArgument(result);

            var handle = new RuntimeArgument(Resources.Param_CorrelationHandle, typeof(CorrelationHandle),
                ArgumentDirection.In, true);
            metadata.Bind(CorrelationHandle, handle);
            metadata.AddArgument(handle);

            var workItemId = new RuntimeArgument("WorkItemId", typeof(string), ArgumentDirection.Out);
            metadata.Bind(WorkItemId, workItemId);
            metadata.AddArgument(workItemId);
        }

        protected override void Execute(NativeActivityContext context)
        {
            context.ScheduleFunc(Child, CorrelationHandle.Get(context), OnCompleted, OnFaulted);
        }

        private void OnCompleted(NativeActivityContext context, ActivityInstance completedInstance,
            StartProcessParams result)
        {
            if(completedInstance.State == ActivityInstanceState.Canceled)
            {
                return;
            }

            if(WorkItemId != null)
            {
                WorkItemId.Set(context, result.WorkItemId);
            }
            var startProcessMessage =
                string.Format(
                    "Process started. WorkflowInstanceId= {0} ; WorkItemId= {1} ; TicketId = {2} ; Parameter = {3}",
                    context.WorkflowInstanceId,
                    result.WorkItemId,
                    result.TicketId,
                    result.Parameter);
            context.LogInfo(startProcessMessage);
            Result.Set(context, result.TicketId);
        }

        private void OnFaulted(NativeActivityFaultContext faultcontext, Exception propagatedexception,
            ActivityInstance propagatedfrom)
        {
            faultcontext.LogErrorFormat("{0} Faulted", propagatedexception, DisplayName);
            faultcontext.HandleFault();
            faultcontext.Abort();
        }
    }
}