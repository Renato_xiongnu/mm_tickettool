﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using MMS.Activities;
using MMS.Activities.Model;
using TicketTool.Activities.Proxy.TicketToolAdmin;
using User = TicketTool.Activities.Model.User;

namespace TicketTool.Activities.Common
{
    //[Designer(typeof(GetUsersActivityDesigner))]
    public sealed class GetUsers : ExecuteExternalServiceWithTwoArguments<string, string, IEnumerable<User>>
    {
        private const string ParamRole = "Role";

        private const string ParamSapCode = "SapCode";

        public override string Workitem1Name
        {
            get { return ParamRole; }
        }

        public override string Workitem2Name
        {
            get { return ParamSapCode; }
        }

        protected override ExecutingResult<IEnumerable<User>> ExecuteService(string workitem1, string workitem2)
        {
            using(var client = new TicketToolAdminClient())
            {
                var result = client.GetUsers(workitem2, new[] {workitem1});
                return new ExecutingResult<IEnumerable<User>>
                {
                    Success = true,
                    Result = result.Select(u => new User
                    {
                        Name = u.Name,
                        Email = u.Email
                    }).ToArray()
                };
            }
        }
    }
    
}