﻿using System.Activities;
using System.Activities.Statements;
using System.Collections.Generic;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Activities;
using MMS.Activities;
using TicketTool.Activities.Common.Design;
using TicketTool.Activities.Properties;
using TicketTool.Activities.TicketToolProxy;

namespace TicketTool.Activities.Common
{
    [Designer(typeof(WaitForCancelActivityDesigner))]
    public sealed class WaitForCancel : NativeActivity
    {
        public class CancelResult
        {
            public string Reason { get; set; }
            public string CancelBy { get; set; }
        }

        private ActivityFunc<CorrelationHandle, CancelResult> CancelTicket { get; set; }

        [RequiredArgument]
// ReSharper disable UnusedAutoPropertyAccessor.Global
        public InArgument<CorrelationHandle> CorrelationHandle { get; set; }


        [Browsable(false)]
        public OutArgument<string> Reason { get; set; }

        [Browsable(false)]
        public OutArgument<string> CancelBy { get; set; }
// ReSharper restore UnusedAutoPropertyAccessor.Global
        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            this.CacheArguments(metadata);
            this.CacheFlow(metadata);
        }

        private void CacheArguments(NativeActivityMetadata metadata)
        {
            var handle = new RuntimeArgument(Resources.Param_CorrelationHandle, typeof(CorrelationHandle), ArgumentDirection.In, true);
            metadata.Bind(this.CorrelationHandle, handle);
            metadata.AddArgument(handle);

            var reason = new RuntimeArgument(Resources.Param_Reason, typeof(string), ArgumentDirection.Out);
            metadata.Bind(this.Reason, reason);
            metadata.AddArgument(reason);

            var cancelBy = new RuntimeArgument(Resources.Param_CancelBy, typeof(string), ArgumentDirection.Out);
            metadata.Bind(this.CancelBy, cancelBy);
            metadata.AddArgument(cancelBy);
        }

        private void CacheFlow(NativeActivityMetadata metadata)
        {
            var handleArg = new DelegateInArgument<CorrelationHandle>();
            var resultArg = new DelegateOutArgument<CancelResult>();
            var workItemId = new Variable<string>();
            var reason = new Variable<string>();
            var cancelBy = new Variable<string>();
            var ticketId = new Variable<string>();
            var ttClient = new Variable<TicketToolServiceClient>();
            var receive = new Receive
                {
                    CanCreateInstance = false,
                    ServiceContractName = Settings.Default.ServiceContractName,
                    OperationName = "Cancel",
                    Content = ReceiveContent.Create(
                        new Dictionary<string, OutArgument>
                            {
                                {Settings.Default.WorkItemIdParam, new OutArgument<string>(workItemId)},
                                {Settings.Default.ReasonTextParam, new OutArgument<string>(reason)},
                                {Settings.Default.CancelByParam, new OutArgument<string>(cancelBy)}
                            }),
                    CorrelatesWith = handleArg,
                    CorrelatesOn = new MessageQuerySet {{Settings.Default.TicketIdKey, new CorrelationWorkItemIdQuery(Settings.Default.WorkItemIdParam)}}
                };
            this.CancelTicket =
                new ActivityFunc<CorrelationHandle, CancelResult>
                    {                        
                        Argument = handleArg,
                        Result = resultArg,
                        Handler = new Sequence
                            {
                                Variables = {workItemId, ticketId, ttClient, reason, cancelBy},
                                Activities =
                                    {
                                        receive,
                                        new SendReply {Request = receive, Content = new SendParametersContent(new Dictionary<string, InArgument> {{"CancelResult", new InArgument<bool>(true)}})},
                                        new Failover
                                            {
                                                DisplayName = string.IsNullOrEmpty(DisplayName) ? GetType().FullName : DisplayName,
                                                Delegate = new InvokeDelegate
                                                    {
                                                        Delegate = new ActivityAction
                                                            {
                                                                Handler = new Sequence
                                                                    {
                                                                        Activities =
                                                                            {
                                                                                new Assign
                                                                                    {
                                                                                        To = new OutArgument<TicketToolServiceClient>(ttClient),
                                                                                        Value = new InArgument<TicketToolServiceClient>(c => new TicketToolServiceClient())
                                                                                    },
                                                                                new Assign
                                                                                    {
                                                                                        To = new OutArgument<string>(ticketId),
                                                                                        Value = new InArgument<string>(c => ttClient.Get(c).GetTicket(workItemId.Get(c)))
                                                                                    },
                                                                                new Assign
                                                                                    {
                                                                                        To = new OutArgument<CancelResult>(resultArg),
                                                                                        Value = new InArgument<CancelResult>(c => new CancelResult{CancelBy = cancelBy.Get(c), Reason = reason.Get(c)})
                                                                                    }
                                                                            }
                                                                    }
                                                            }
                                                    }
                                            }
                                    }
                            }
                    };
            metadata.AddDelegate(this.CancelTicket);
        }

        protected override void Execute(NativeActivityContext context)
        {
            context.ScheduleFunc(this.CancelTicket, this.CorrelationHandle.Get(context), this.OnCompleted);
        }

        private void OnCompleted(NativeActivityContext context, ActivityInstance completedinstance, CancelResult result)
        {
            if (completedinstance.State == ActivityInstanceState.Canceled) return;
            if (result == null) return;

            if (this.CancelBy != null) this.CancelBy.Set(context, result.CancelBy);
            if (this.Reason != null) this.Reason.Set(context, result.Reason);
        }
    }
}
