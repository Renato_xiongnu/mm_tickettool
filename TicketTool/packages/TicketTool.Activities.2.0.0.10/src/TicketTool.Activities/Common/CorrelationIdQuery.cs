﻿using System.Linq;
using System.IO;
using System.ServiceModel.Channels;
using System.Xml;
using TicketTool.Activities.Properties;

namespace TicketTool.Activities.Common
{
    public class CorrelationIdQuery : MessageQueryBase
    {
        private readonly string _key;

        public CorrelationIdQuery(string key)
        {
            this._key = key;
        }

        public CorrelationIdQuery()
            : this(Settings.Default.TicketIdKey)
        {
        }

        public override TResult Evaluate<TResult>(Message message)
        {
            XmlDocument doc = new XmlDocument();
            using (StringReader sr = new StringReader(message.GetReaderAtBodyContents().ReadOuterXml()))
                doc.Load(sr);
            if (doc.DocumentElement != null)
            {
                var correlationElement = doc.DocumentElement.ChildNodes.Cast<XmlNode>().FirstOrDefault(x => x.LocalName == _key);
                if(correlationElement != null)
                    return (TResult) (object) correlationElement.InnerText;
            }
            return default(TResult);
        }
    }
}
