﻿using System.IO;
using System.Linq;
using System.ServiceModel.Channels;
using System.Xml.Linq;

namespace TicketTool.Activities.Common
{
    public class NewTicketIdCorrelationQuery : MessageQueryBase
    {
        public override TResult Evaluate<TResult>(Message message)
        {
            using (var sr = new StringReader(message.GetReaderAtBodyContents().ReadOuterXml()))
                return (TResult) (object) XDocument.Load(sr).Descendants().First().Value;
        }
    }
}