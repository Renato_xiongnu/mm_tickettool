using System.ServiceModel.Channels;
using TicketTool.Activities.TicketToolProxy;

namespace TicketTool.Activities.Common
{
    public class CorrelationWorkItemIdQuery : CorrelationIdQuery
    {
        public CorrelationWorkItemIdQuery(string key)
            : base(key)
        {
        }

        public override TResult Evaluate<TResult>(Message message)
        {
            var workItemId = base.Evaluate<string>(message);
            return (TResult) (object) new TicketToolServiceClient().GetTicket(workItemId);
        }
    }
}