﻿using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;

namespace TicketTool.Activities.Common
{
    public abstract class MessageQueryBase : MessageQuery
    {
        public override TResult Evaluate<TResult>(MessageBuffer buffer)
        {
            return this.Evaluate<TResult>(buffer.CreateMessage());
        }
    }
}
