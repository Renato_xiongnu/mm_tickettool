﻿using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel.Channels;

namespace TicketTool.Activities.Common
{
    public class NewCorrelationIdQuery<T> : MessageQueryBase
    {
        private const string NS = "http://schemas.microsoft.com/2003/10/Serialization/";

        public override TResult Evaluate<TResult>(Message message)
        {
            var dcs = new DataContractSerializer(typeof(T));
            object dataContract = dcs.GetType().GetProperty("RootContract", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(dcs);
            var typeName = dataContract.GetType().GetProperty("Name", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(dataContract).ToString();

            var x = XName.Get(typeName, NS);
            XDocument doc;
            using (var sr = new StringReader(message.GetReaderAtBodyContents().ReadOuterXml()))
                doc = XDocument.Load(sr);

            var result = doc.Descendants().FirstOrDefault(c => c.Name == x);
            return result != null ? (TResult)(object)result.Value : (TResult)(object)null;
        }
    }
}
