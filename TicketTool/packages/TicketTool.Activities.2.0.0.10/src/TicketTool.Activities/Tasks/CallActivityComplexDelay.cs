﻿using System;
using System.Activities;
using System.Activities.Statements;
using System.ComponentModel;
using System.Windows;
using Microsoft.CSharp.Activities;
using TicketTool.Activities.Properties;
using TicketTool.Activities.Tasks.Design;

namespace TicketTool.Activities.Tasks
{
    [Designer(typeof(CallActivityComplexDelayDesigner))]
    public class CallActivityComplexDelay : CallActivity
    {
        public string DelayDurationFunc { get; set; }

        protected override void CachingCallFlow()
        {
            var expression = String.Format("TimeSpan.FromSeconds({0})", this.DelayDurationFunc);
            this.Delay.Duration = new InArgument<TimeSpan>(new CSharpValue<TimeSpan>(expression));
        }

        public override Activity Create(DependencyObject target)
        {
            var task = new TaskActivity();
            task = (TaskActivity)task.Create(target);
            task.Outcomes.Clear();
            task.Outcomes.Add(new Outcome { DisplayName = Resources.IDidntGetThrough, Name = Resources.IDidntGetThrough });
            task.Outcomes.Add(CallBackLaterOutcome.NewCallBackLaterOutcome());
            return new CallActivityComplexDelay
            {
                Task = task,
                Delay = new Delay { Duration = new InArgument<TimeSpan>(TimeSpan.FromMinutes(5)) },
                Repetition = new InArgument<int>(1)
            };
        }
    }
}

