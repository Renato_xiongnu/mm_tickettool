﻿using System.Activities.Presentation;
using System.ComponentModel;
using System.Windows;
using TicketTool.Activities.Properties;
using TicketTool.Activities.Tasks.Design;

namespace TicketTool.Activities.Tasks
{
    [Designer(typeof(CallBackLaterOutcomeActivityDesigner))]
    public sealed class CallBackLaterOutcome : Outcome, IActivityTemplateFactory<Outcome>
    {
        [Browsable(false)]
        public CallAfterField CallMeAfter
        {
            get { return (CallAfterField)this.RequestedFields[0]; }
            set
            {
                if (this.RequestedFields.Count == 0)
                {
                    this.RequestedFields.Add(value);
                }
                else
                {
                    this.RequestedFields[0] = value;
                }
            }
        }

        internal override bool SkipTaskFormValidation
        {
            get { return true; }
        }

        internal static CallBackLaterOutcome NewCallBackLaterOutcome()
        {
            var result = new CallBackLaterOutcome();
            result.RequestedFields.Add(new CallAfterField {Name = Resources.CallMeLater});
            result.Name = Resources.CallBackLater;
            result.DisplayName = Resources.CallBackLater;
            return result;
        }

        public Outcome Create(DependencyObject target, IDataObject dataObject)
        {
            return NewCallBackLaterOutcome();
        }
    }
}
