using System.Activities;
using System.ComponentModel;

namespace TicketTool.Activities.Tasks
{
    [ToolboxItem(false)]
    public sealed class TaskField<T> : FieldBase
    {
        [Browsable(false)]
// ReSharper disable UnusedAutoPropertyAccessor.Global
        public InArgument<T> Value { get; set; }        
// ReSharper restore UnusedAutoPropertyAccessor.Global

        protected override void Execute(NativeActivityContext context)
        {
            if (this.Task == null || this.Value == null) return;
            var task = this.Task.Get(context);
            task.GetType().GetProperty(this.Name).SetValue(task, this.Value.Get(context));
        }
    }
}