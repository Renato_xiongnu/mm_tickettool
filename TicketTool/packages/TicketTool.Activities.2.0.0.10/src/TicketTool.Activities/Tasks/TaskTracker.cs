﻿namespace TicketTool.Activities.Tasks
{
    public class TaskTracker
    {
        public string TaskId { get; set; }
        public TaskStatus CurrentState { get; set; }
        public string TaskType { get; set; }
        public string Performer { get; set; }
    }
}
