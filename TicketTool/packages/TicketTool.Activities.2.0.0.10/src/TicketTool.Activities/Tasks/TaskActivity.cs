﻿using System;
using System.Activities;
using System.Activities.Presentation;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activities;
using System.Windows;
using MMS.Activities;
using MMS.Activities.Extensions;
using TicketTool.Activities.Common;
using TicketTool.Activities.Properties;
using TicketTool.Activities.Tasks.Design;
using TicketTool.Activities.TicketToolProxy;

namespace TicketTool.Activities.Tasks
{
    [Designer(typeof(TaskActivityDesigner))]
    public sealed class TaskActivity : NativeActivity, IActivityTemplateFactory
    {
        private class TaskParam
        {
            public string Outcome { get; set; }

            public Dictionary<string, object> Data { get; set; }

            public string Performer { get; set; }
        }

        private Collection<FieldBase> _fields;

        private Collection<Outcome> _outcomes;

        private Collection<RequestedDataFieldBase> _requestedData;

        private ActivityFunc<TicketTask, TaskTracker> CreateTask { get; set; }

        private ActivityFunc<CorrelationHandle, TaskParam> WaitForComplete { get; set; }

        private ActivityAction<Dictionary<string, object>> OutcomesFlow { get; set; }

        private ActivityAction<Dictionary<string, object>> UserData { get; set; }

        private ActivityAction CancelTaskFlow { get; set; }

        // ReSharper disable MemberCanBePrivate.Global
        // ReSharper disable UnusedMember.Global
        // ReSharper disable UnusedAutoPropertyAccessor.Global 
        // ReSharper disable ValueParameterNotUsed
        public Variable<string> TaskId { get; set; }

        [RequiredArgument]
        public InArgument<CorrelationHandle> CorrelationHandle { get; set; }

        public InOutArgument<TaskTracker> Tracker { get; set; }

        [RequiredArgument]
        [Browsable(false)]
        public OutArgument<string> Outcome { get; set; }

        [Browsable(false)]
        public OutArgument<string> InternalOutcome { get; set; }

        [Browsable(false)]
        public Collection<FieldBase> Fields
        {
            get { return _fields ?? (_fields = new Collection<FieldBase>()); }
            set { }
        }

        [Browsable(false)]
        public Collection<Outcome> Outcomes
        {
            get { return _outcomes ?? (_outcomes = new Collection<Outcome>()); }
            set { }
        }

        [Browsable(false)]
        public Collection<RequestedDataFieldBase> RequestedData
        {
            get { return _requestedData ?? (_requestedData = new Collection<RequestedDataFieldBase>()); }
            set { }
        }

        [Browsable(false)]
        public string CompleteOperationName
        {
            get { return String.Concat(DisplayName.Replace(" ", "_"), "Continue"); }
            set { }
        }

        // ReSharper restore ValueParameterNotUsed
        // ReSharper restore UnusedMember.Global
        // ReSharper restore UnusedAutoPropertyAccessor.Global
        // ReSharper restore MemberCanBePrivate.Global

        public Activity Create(DependencyObject target)
        {
            var task = new TaskActivity {DisplayName = Resources.NewTask, _fields = new TaskForm().Fields};
            task.Outcomes.Add(new Outcome {DisplayName = Resources.OK, Name = Resources.OK});
            return task;
        }

        #region CacheMetadata

        protected override void Cancel(NativeActivityContext context)
        {
            context.CancelChildren();
        }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            CacheArguments(metadata);

            ValidateOutcomes(metadata);
            ValidateFields(metadata);

            CacheCreateTask(metadata);
            CacheWaitForComplete(metadata);
            CacheCancelTask(metadata);
            CacheOutcomes(metadata);
            CacheRequestedData(metadata);
        }

        private void CacheCancelTask(NativeActivityMetadata metadata)
        {
            CancelTaskFlow = new ActivityAction
            {
                Handler = new InvokeMethod
                {
                    MethodName = "CancelTask",
                    TargetObject = new InArgument<TicketToolServiceClient>(c => new TicketToolServiceClient()),
                    Parameters = {new InArgument<string>(c => TaskId.Get(c))}
                }
            };
            metadata.AddDelegate(CancelTaskFlow);
        }

        private void CacheWaitForComplete(NativeActivityMetadata metadata)
        {
            var handleArg = new DelegateInArgument<CorrelationHandle>();
            var taskParamArg = new DelegateOutArgument<TaskParam>();

            var sequence = new Sequence {DisplayName = "Task"};
            var userOutcome = new Variable<string>("UserOutcome");
            var fieldData = new Variable<Dictionary<string, object>>("ReceivedData");
            var performer = new Variable<string>("Performer");
            var response = new Variable<TaskResponse>("Response");

            sequence.Variables.Add(userOutcome);
            sequence.Variables.Add(fieldData);
            sequence.Variables.Add(response);
            sequence.Variables.Add(performer);

            // ReSharper disable UseObjectOrCollectionInitializer
            var recieve = new Receive();
            // ReSharper restore UseObjectOrCollectionInitializer
            recieve.CanCreateInstance = false;
            recieve.ServiceContractName = Settings.Default.ServiceContractName;
            recieve.OperationName = CompleteOperationName;
            recieve.Content = ReceiveContent.Create(
                new Dictionary<string, OutArgument>
                {
                    {Settings.Default.TicketIdKey, new OutArgument<string>()},
                    {Settings.Default.OutcomeParamName, new OutArgument<string>(userOutcome)},
                    {Settings.Default.RequestedFieldsParam, new OutArgument<Dictionary<string, object>>(fieldData)},
                    {Settings.Default.PerformerParam, new OutArgument<string>(performer)}
                });
            recieve.CorrelatesWith = handleArg;
            recieve.CorrelatesOn = new MessageQuerySet {{Settings.Default.TicketIdKey, new CorrelationIdQuery()}};
// ReSharper disable ImplicitlyCapturedClosure
            sequence.Activities.Add(
                new DoWhile(c => !response.Get(c).CompleteSuccessful)
                {
                    Body = new Sequence
                    {
                        Activities =
                        {
                            recieve,
                            new Assign
                            {
                                To = new OutArgument<TaskResponse>(response),
                                Value =
                                    new InArgument<TaskResponse>(
                                        c => ValidateRecievedData(userOutcome.Get(c), fieldData.Get(c)))
                            },
                            new Assign
                            {
                                To = new OutArgument<TaskParam>(taskParamArg),
                                Value =
                                    new InArgument<TaskParam>(
                                        c =>
                                            new TaskParam
                                            {
                                                Outcome = userOutcome.Get(c),
                                                Data = fieldData.Get(c),
                                                Performer = performer.Get(c)
                                            })
                            },
                            new SendReply
                            {
                                Request = recieve,
                                Content = new SendParametersContent(
                                    new Dictionary<string, InArgument>
                                    {
                                        {Settings.Default.TaskResponseParam, new InArgument<TaskResponse>(response)}
                                    })
                            }
                        }
                    }
                });
            // ReSharper restore ImplicitlyCapturedClosure
            WaitForComplete = new ActivityFunc<CorrelationHandle, TaskParam>
            {
                Argument = handleArg,
                Handler = sequence,
                Result = taskParamArg
            };
            metadata.AddDelegate(WaitForComplete);
        }

        private void CacheCreateTask(NativeActivityMetadata metadata)
        {
            var taskArg = new DelegateInArgument<TicketTask>();
            TaskId = new Variable<string>();
            var taskId = new Variable<string>();
            var sequence = new Sequence {DisplayName = "Fields"};
            var taskTracker = new DelegateOutArgument<TaskTracker>("TaskTracker");
            sequence.Variables.Add(taskId);
            metadata.AddVariable(TaskId);
            foreach(var field in Fields)
            {
                field.Task = taskArg;
                sequence.Activities.Add(field);
            }
            sequence.Activities.Add(
                new Failover
                {
                    DisplayName = string.IsNullOrEmpty(DisplayName) ? GetType().FullName : DisplayName,
                    Delegate = new InvokeDelegate
                    {
                        Delegate = new ActivityAction
                        {
                            Handler = new Sequence
                            {
                                Activities =
                                {
                                    new Assign
                                    {
                                        To = new OutArgument<string>(TaskId),
                                        Value =
                                            new InArgument<string>(
                                                c => new TicketToolServiceClient().CreateTask(taskArg.Get(c)))
                                    },
                                    new Assign
                                    {
                                        To = new OutArgument<TaskTracker>(taskTracker),
                                        Value =
                                            new InArgument<TaskTracker>(
                                                c =>
                                                    new TaskTracker
                                                    {
                                                        TaskId = TaskId.Get(c),
                                                        CurrentState = TaskStatus.Created
                                                    })
                                    }
                                }
                            }
                        }
                    }
                });
            CreateTask = new ActivityFunc<TicketTask, TaskTracker>
            {
                Argument = taskArg,
                Handler = sequence,
                Result = taskTracker
            };
            metadata.AddDelegate(CreateTask);
        }

        private void CacheOutcomes(NativeActivityMetadata metadata)
        {
            var dataArg = new DelegateInArgument<Dictionary<string, object>>();
            var sequence = new Sequence {DisplayName = "Outcomes"};
            foreach(var outcome in Outcomes)
            {
                outcome.RequestedData = dataArg;
                sequence.Activities.Add(outcome);
            }
            OutcomesFlow = new ActivityAction<Dictionary<string, object>>
            {
                Argument = dataArg,
                Handler = sequence
            };
            metadata.AddDelegate(OutcomesFlow);
        }

        private void CacheRequestedData(NativeActivityMetadata metadata)
        {
            var dataArg = new DelegateInArgument<Dictionary<string, object>>();
            var sequence = new Sequence {DisplayName = "RequestedData"};
            foreach(var field in RequestedData)
            {
                field.RequestedData = dataArg;
                sequence.Activities.Add(field);
            }
            UserData = new ActivityAction<Dictionary<string, object>> {Argument = dataArg, Handler = sequence};
            metadata.AddDelegate(UserData);
        }

        private void CacheArguments(NativeActivityMetadata metadata)
        {
            var handle = new RuntimeArgument(Resources.Param_CorrelationHandle, typeof(CorrelationHandle),
                ArgumentDirection.In, true);
            metadata.Bind(CorrelationHandle, handle);
            metadata.AddArgument(handle);

            var outcome = new RuntimeArgument(Resources.Param_Outcome, typeof(string), ArgumentDirection.Out, true);
            metadata.Bind(Outcome, outcome);
            metadata.AddArgument(outcome);

            var internalOutcome = new RuntimeArgument(Resources.Param_InternalOutcome, typeof(string),
                ArgumentDirection.Out);
            metadata.Bind(InternalOutcome, internalOutcome);
            metadata.AddArgument(internalOutcome);

            var taskTracker = new RuntimeArgument(Resources.Tracker, typeof(TaskTracker), ArgumentDirection.InOut);
            metadata.Bind(Tracker, taskTracker);
            metadata.AddArgument(taskTracker);
        }

        #endregion

        #region Validation

        private TaskResponse ValidateRecievedData(string outcome, Dictionary<string, object> receivedData)
        {
            var result = new TaskResponse {Message = Resources.OK};

            var expectedOutcome = Outcomes.FirstOrDefault(o => o.Name == outcome);
            if(expectedOutcome == null)
            {
                result.Message = Resources.Err_OutcomeMismatch;
                return result;
            }
            if(RequestedData.Count > 0 && receivedData == null)
            {
                result.Message = Resources.Err_TheDataOfFieldsRequired;
                return result;
            }
            var allFields = new List<string>(RequestedData.Select(f => f.Name));
            allFields.AddRange(expectedOutcome.GetFullFieldNames());
            foreach(var field in allFields.Where(f => !receivedData.ContainsKey(f)))
            {
                result.Message = String.Format(Resources.Err_WorkflowExpectedField, field);
                return result;
            }
            var outcomeValidation = expectedOutcome.ValidateRequestedData(receivedData);
            if(outcomeValidation.Count > 0)
            {
                result.Message = String.Join(Environment.NewLine, outcomeValidation);
                return result;
            }
            result.CompleteSuccessful = true;
            return result;
        }

        private void ValidateOutcomes(NativeActivityMetadata metadata)
        {
            if(Outcomes == null || Outcomes.Count == 0)
            {
                metadata.AddValidationError(Resources.Err_OutcomeRequired);
                return;
            }
            if(Outcomes.GroupBy(o => o.Name).Any(g => g.Count() > 1))
            {
                metadata.AddValidationError(Resources.Err_OutcomesMustBeUnique);
                return;
            }
            if(Outcomes.Count(o => o.IsPositive) > 1)
            {
                metadata.AddValidationError(Resources.Err_OnePositiveOutcome);
            }
        }

        private void ValidateFields(NativeActivityMetadata metadata)
        {
            var allFields = new List<string>();
            allFields.AddRange(RequestedData.Select(f => f.Name));
            foreach(var outcome in Outcomes)
            {
                allFields.AddRange(outcome.GetFullFieldNames());
            }
            if(!allFields.GroupBy(f => f).Any(g => g.Count() > 1))
            {
                return;
            }
            metadata.AddValidationError(Resources.Err_FieldsMustBeUnique);
        }

        #endregion

        #region Execution

        protected override void Execute(NativeActivityContext context)
        {
            var task = new TicketTask
            {
                Type = DisplayName.Replace(" ", "_"),
                RequiredFields = RequestedData.Select(f => f.GetRequiredField()).ToArray(),
                Outcomes = Outcomes.Select(o => o.GetTicketToolProxyOutcome()).ToArray()
            };
            if(Tracker != null)
            {
                var tracker = Tracker.Get(context);
                if(tracker != null)
                {
                    tracker.CurrentState = TaskStatus.New;
                    tracker.TaskType = task.Type;
                }
            }

            context.ScheduleFunc(CreateTask, task, OnTaskFilled);
        }

        private void OnTaskFilled(NativeActivityContext context, ActivityInstance completedinstance, TaskTracker task)
        {
            if(completedinstance.State == ActivityInstanceState.Canceled)
            {
                CancelTask(context);
            }
            else
            {
                if(Tracker != null)
                {
                    Tracker.Set(context, task);
                }
                context.ScheduleFunc(WaitForComplete, CorrelationHandle.Get(context), OnCompleted);
            }
        }

        private void OnCompleted(NativeActivityContext context, ActivityInstance completedinstance, TaskParam result)
        {
            if(completedinstance.State == ActivityInstanceState.Canceled)
            {
                CancelTask(context);
            }
            else
            {
                Outcome.Set(context, result.Outcome);
                if(InternalOutcome != null)
                {
                    InternalOutcome.Set(context, result.Outcome);
                }

                if(Tracker != null)
                {
                    var tracker = Tracker.Get(context);
                    if(tracker != null)
                    {
                        tracker.Performer = result.Performer;
                    }
                }
                SetTaskStatus(context, TaskStatus.Completed);
                context.ScheduleAction(UserData, result.Data);
                context.ScheduleAction(OutcomesFlow, result.Data);
            }
        }

        private void SetTaskStatus(NativeActivityContext context, TaskStatus status)
        {
            if(Tracker != null)
            {
                var tracker = Tracker.Get(context);
                if(tracker != null)
                {
                    tracker.CurrentState = status;
                }
            }
        }

        private void CancelTask(NativeActivityContext context)
        {
            SetTaskStatus(context, TaskStatus.Canceled);
            context.ScheduleAction(CancelTaskFlow, onFaulted: OnCancelFaulted);
        }

        private void OnCancelFaulted(NativeActivityFaultContext faultcontext, Exception propagatedexception,
            ActivityInstance propagatedfrom)
        {
            faultcontext.LogErrorFormat("CancelTask \"{0}\" faulted", propagatedexception, DisplayName);
            faultcontext.HandleFault();
        }

        #endregion
    }
}