﻿using System;
using System.Activities;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using TicketTool.Activities.Properties;
using TicketTool.Activities.Tasks.Design;

namespace TicketTool.Activities.Tasks
{
    [Designer(typeof(OutcomeActivityDesigner))]
    public class Outcome : NativeActivity
    {
        public static readonly string Divider = ";#";

        private Collection<RequestedDataFieldBase> _requestedFields;

        private ActivityAction<Dictionary<string, object>> UserData { get; set; }

        [Browsable(false)]
        public Collection<RequestedDataFieldBase> RequestedFields
        {
            get { return this._requestedFields ?? (this._requestedFields = new Collection<RequestedDataFieldBase>()); }
            // ReSharper disable ValueParameterNotUsed
            set { }
            // ReSharper restore ValueParameterNotUsed
        }

        [Browsable(false)]
        public string Name { get; set; }

        [Browsable(false)]
// ReSharper disable UnusedAutoPropertyAccessor.Global
        public bool IsPositive { get; set; }
// ReSharper restore UnusedAutoPropertyAccessor.Global

        [Browsable(false)]
        public InArgument<Dictionary<string, object>> RequestedData { get; set; }

        internal virtual bool SkipTaskFormValidation
        {
            get { return false; }
        }

        internal virtual IEnumerable<string> GetFullFieldNames()
        {
            return this.RequestedFields.ToList().Select(f => String.Concat(this.Name, Divider, f.Name));
        }

        protected internal virtual ICollection<string> ValidateRequestedData(Dictionary<string, object> data)
        {
            var result = new Collection<string>();
            foreach (var field in this.RequestedFields)
            {
                var fieldName = String.Concat(this.Name, Divider, field.Name);
                if (!data.ContainsKey(fieldName))
                {
                    result.Add(String.Format(Resources.Err_NoDataProvidedField, field.Name, this.Name));
                }
                else
                {
                    if (!field.Validate(data[fieldName]))
                    {
                        result.Add(String.Format(Resources.Err_ValueFieldNotValid, data[fieldName], field.Name, this.Name));
                    }
                }
            }
            return result;
        }

        protected internal virtual TicketToolProxy.Outcome GetTicketToolProxyOutcome()
        {
            return new TicketToolProxy.Outcome
                {
                    Value = this.Name,
                    IsSuccess = this.IsPositive,
                    RequiredFields = this.RequestedFields.Select(f => f.GetRequiredField()).ToArray(),
                    SkipTaskRequeriedFields = this.SkipTaskFormValidation
                };
        }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            if (String.IsNullOrEmpty(this.Name)) metadata.AddValidationError(Resources.Err_OutcomeCouldntBeEmpty);
            if (this.RequestedFields.GroupBy(f => f.Name).Any(g => g.Count() > 1)) metadata.AddValidationError(Resources.Err_FieldsMustBeUnique);

            this.CacheArguments(metadata);
            this.CacheFields(metadata);
        }

        private void CacheArguments(NativeActivityMetadata metadata)
        {
            var arg = new RuntimeArgument(Resources.Param_RequestedValue, typeof(Dictionary<string, object>), ArgumentDirection.In);
            metadata.Bind(this.RequestedData, arg);
            metadata.AddArgument(arg);
        }

        private void CacheFields(NativeActivityMetadata metadata)
        {
            var dataArg = new DelegateInArgument<Dictionary<string, object>>();
            var sequence = new Sequence {DisplayName = "OutcomeRequestedFields"};
            foreach (var field in this.RequestedFields)
            {
                field.RequestedData = dataArg;
                sequence.Activities.Add(field);
            }
            this.UserData = new ActivityAction<Dictionary<string, object>> {Argument = dataArg, Handler = sequence};
            metadata.AddDelegate(this.UserData);
        }

        protected override void Execute(NativeActivityContext context)
        {
            if (this.RequestedFields.Count == 0) return;

            if (this.RequestedData != null)
            {
                var outcomeUserData = this.RequestedData.Get(context)
                                          .Where(f => f.Key.StartsWith(String.Concat(this.Name, Divider)))
                                          .Select(f => new {Key = f.Key.Substring(f.Key.IndexOf(Divider, StringComparison.Ordinal) + Divider.Length), f.Value})
                                          .ToDictionary(f => f.Key, f => f.Value);
                context.ScheduleAction(this.UserData, outcomeUserData);
            }
        }
    }
}
