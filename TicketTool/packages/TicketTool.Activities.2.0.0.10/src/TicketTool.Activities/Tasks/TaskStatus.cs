﻿namespace TicketTool.Activities.Tasks
{
    public enum TaskStatus
    {
        New,
        Created,
        Canceled,
        Completed        
    }
}