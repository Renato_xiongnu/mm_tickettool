﻿using System;
using System.ComponentModel;
using TicketTool.Activities.Tasks.Design;

namespace TicketTool.Activities.Tasks
{
    [Designer(typeof(CallAfterFieldActivityDesigner))]
    [ToolboxItem(false)]
    public sealed class CallAfterField : RequestedDataField<TimeSpan>
    {
    }
}
