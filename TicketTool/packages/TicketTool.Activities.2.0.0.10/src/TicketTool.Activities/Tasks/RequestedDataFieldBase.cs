﻿using System.Activities;
using System.Collections.Generic;
using System.ComponentModel;
using TicketTool.Activities.Properties;

namespace TicketTool.Activities.Tasks
{
    public abstract class RequestedDataFieldBase : FieldBase
    {
        [Browsable(false)]
        public InArgument<Dictionary<string, object>> RequestedData { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            var arg = new RuntimeArgument(Resources.Param_RequestedValue, typeof(Dictionary<string, object>), ArgumentDirection.In);
            metadata.Bind(this.RequestedData, arg);
            metadata.AddArgument(arg);
        }

        internal abstract bool Validate(object data);
    }
}