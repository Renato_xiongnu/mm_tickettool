﻿using System;
using System.Activities;
using System.Activities.Presentation;
using System.Activities.Statements;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using MMS.Activities;
using MMS.Activities.Extensions;
using MMS.Activities.Model;
using TicketTool.Activities.AuthenticationProxy;
using TicketTool.Activities.Infasrtucture;
using TicketTool.Activities.Properties;
using TicketTool.Activities.Proxy.TicketToolAdmin;
using TicketTool.Activities.Tasks.Design;
using TicketTool.Activities.TicketToolProxy;
using SkillSet = TicketTool.Activities.TicketToolProxy.SkillSet;

namespace TicketTool.Activities.Tasks
{
    [Designer(typeof(CompleteTaskDesigner))]
    public sealed class CompleteTask : NativeActivity<ExecutingResult<bool>>, IActivityTemplateFactory<CompleteTask>
    {
        private readonly object _lockObject = new object();
        private ActivityFunc<UserCredentials> CredentialsObtaining { get; set; }

        private ActivityFunc<TaskTracker, UserCredentials, OperationResult> Flow { get; set; }

        // ReSharper disable MemberCanBePrivate.Global
        [Browsable(false)]
        [RequiredArgument]
        // ReSharper disable UnusedAutoPropertyAccessor.Global
        public InArgument<TaskTracker> Tracker { get; set; }

        // ReSharper restore UnusedAutoPropertyAccessor.Global

        [Browsable(false)]
        public string Outcome { get; set; }

        [Browsable(false)]
        public Credentials Credentials { get; set; }

        // ReSharper restore MemberCanBePrivate.Global
        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            CacheArguments(metadata);
            CacheFlow(metadata);
            CacheCredentials(metadata);
        }

        private void CacheCredentials(NativeActivityMetadata metadata)
        {
            var credentials = new DelegateOutArgument<UserCredentials>("UserCredentials");
            Credentials.UserCredentials = credentials;
            CredentialsObtaining = new ActivityFunc<UserCredentials>
            {
                Handler = Credentials,
                Result = credentials
            };
            metadata.AddDelegate(CredentialsObtaining);
        }

        private void CacheFlow(NativeActivityMetadata metadata)
        {
            var tracker = new DelegateInArgument<TaskTracker>("Tracker");
            var credentials = new DelegateInArgument<UserCredentials>("Credentials");
            var result = new DelegateOutArgument<OperationResult>("Result");
            Flow = new ActivityFunc<TaskTracker, UserCredentials, OperationResult>
            {
                Argument1 = tracker,
                Argument2 = credentials,
                Result = result,
                Handler = new Failover
                {
                    DisplayName = string.IsNullOrEmpty(DisplayName) ? GetType().FullName : DisplayName,
                    Delegate = new InvokeDelegate
                    {
                        Delegate = new ActivityAction
                        {
                            Handler = new Assign
                            {
                                To = new OutArgument<OperationResult>(result),
                                Value =
                                    new InArgument<OperationResult>(
                                        c => CallCompleteTask(c,tracker.Get(c).TaskId, credentials.Get(c)))
                            }
                        }
                    }
                }
            };
            metadata.AddDelegate(Flow);
        }

        private void CacheArguments(NativeActivityMetadata metadata)
        {
            if(String.IsNullOrEmpty(Outcome))
            {
                metadata.AddValidationError(Resources.Err_OutcomeMustFilled);
            }
            var tracker = new RuntimeArgument(Resources.Param_Tracker, typeof(TaskTracker), ArgumentDirection.In, true);
            metadata.Bind(Tracker, tracker);
            metadata.AddArgument(tracker);
        }

        protected override void Execute(NativeActivityContext context)
        {
            context.ScheduleFunc(CredentialsObtaining, OnCredentialsObtained);
        }

        private void OnCredentialsObtained(NativeActivityContext context, ActivityInstance completedinstance,
            UserCredentials result)
        {
            context.ScheduleFunc(Flow, Tracker.Get(context), result, OnCompleted, OnFailed);
        }

        private void OnCompleted(NativeActivityContext context, ActivityInstance completedinstance,
            OperationResult result)
        {
            if(completedinstance.State == ActivityInstanceState.Canceled)
            {
                return;
            }

            if(result.HasError)
            {
                context.LogErrorFormat("\"{0}\" error: {1}", DisplayName, result.MessageText);
            }

            Result.Set(context, new ExecutingResult<bool>
            {
                Message = result.MessageText,
                Result = !result.HasError,
                Success = !result.HasError
            });
        }

        private void OnFailed(NativeActivityFaultContext faultcontext, Exception propagatedexception,
            ActivityInstance propagatedfrom)
        {
            Result.Set(faultcontext, new ExecutingResult<bool>
            {
                Exception = propagatedexception,
                Message = propagatedexception.Message,
                Result = false,
                Success = false
            });
            faultcontext.LogErrorFormat("\"{0}\" faulted", propagatedexception, DisplayName);
            faultcontext.HandleFault();
        }

        private OperationResult CallCompleteTask(ActivityContext context, string taskId, UserCredentials credentials)
        {
            TicketTask task;
            try
            {
                task = new TicketToolServiceClient().GetTask(taskId);
            }
// ReSharper disable EmptyGeneralCatchClause
            catch(Exception e)
// ReSharper restore EmptyGeneralCatchClause
            {
                return new OperationResult
                {
                    HasError = true,
                    MessageText = String.Format("Complete task invocation raised exception: {0}", e.Message)
                };
            }
            if(task == null)
            {
                return new OperationResult {HasError = true};
            }
            if(task.State >= TaskState.Completed)
            {
                return new OperationResult();
            }
            var authClient = new AuthenticationServiceClient();
            
            var authContext = new AuthContext();
            if (!authContext.Login(() => authClient.LogIn(credentials.UserName, credentials.Password), authClient.InnerChannel))
            {
                var message = string.Format("Login user fail {0}", credentials.UserName);
                context.LogInfoFormat(message);
                return new OperationResult
                {
                    HasError = true,
                    MessageText = message
                };
            }            

            var ticketToolClient = new TicketToolServiceClient();            
            var operationResult = authContext.Execute(() =>
            {
                var completeTaskResult = ticketToolClient.CompleteTask(taskId, Outcome, new RequiredField[] { });
                ////TODO Временное решение, пока не будет реализован функционал системного постоянно залогиненого в скилсеты пользователя
                //if (completeTaskResult.HasError)
                //{
                //    context.LogErrorFormat("CompleteTask {0}", completeTaskResult.MessageText);
                //    var ticketToolAdminClient = new TicketToolAdminClient();
                //    var availibleSkillsets = authContext.Execute(() => ticketToolAdminClient.GetUser(credentials.UserName).AvailableSkillSets,
                //        ticketToolAdminClient.InnerChannel);                        
                //    lock (_lockObject)
                //    {
                //        ticketToolClient.LogInToSkillSets(availibleSkillsets.Select(el => new SkillSet { Name = el }).ToArray());
                //    }
                //    context.LogInfoFormat("Try to login user {0} in skillsets {1}", credentials.UserName, string.Join(", ", availibleSkillsets));
                //    completeTaskResult = ticketToolClient.CompleteTask(taskId, Outcome, new RequiredField[] { });
                //}                                
                return completeTaskResult;
            }, ticketToolClient.InnerChannel);            
                        
            return operationResult;
        }

        public CompleteTask Create(DependencyObject target, IDataObject dataObject)
        {
            return new CompleteTask {Credentials = new Credentials(), Outcome = Resources.OK};
        }
    }
}