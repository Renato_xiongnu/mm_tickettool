﻿using System.Activities.Presentation.View;
using System.Activities.Statements;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace TicketTool.Activities.Tasks.Design
{
    public partial class CallActivityDisigner
    {
        public CallActivityDisigner()
        {
            InitializeComponent();
            this.Loaded += this.OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            var cb = this.Designer.CommandBindings.Cast<CommandBinding>()
                .FirstOrDefault(c => c.Command.Equals(ApplicationCommands.Delete));
            if (cb != null) cb.CanExecute += this.CanExecuteDeletion;
        }

        private void CanExecuteDeletion(object sender, CanExecuteRoutedEventArgs e)
        {
            var canDelete = false;
            var selection = this.Context.Items.GetValue<Selection>();
            if (selection != null && selection.SelectionCount > 0)
            {
                canDelete = selection.SelectedObjects
                    .All(p => p.ItemType != typeof(TaskActivity) && p.ItemType != typeof(Delay));
            }
            e.CanExecute = canDelete;
        }   
    }
}
