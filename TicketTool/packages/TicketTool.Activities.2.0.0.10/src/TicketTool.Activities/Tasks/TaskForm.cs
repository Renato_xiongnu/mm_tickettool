﻿using System.Collections.ObjectModel;
using System.Reflection;
using System;
using System.Linq;
using System.Runtime.Serialization;
using TicketTool.Activities.TicketToolProxy;

namespace TicketTool.Activities.Tasks
{
    public class TaskForm
    {
        private Collection<FieldBase> _fields;

        public Collection<FieldBase> Fields
        {
            get { return this._fields ?? (this._fields = this.GetFields()); }
        }

        private Collection<FieldBase> GetFields()
        {
            string[] exclude =
                {
                    "TaskId", 
                    "Outcomes", 
                    "Outcome", 
                    "RequiredFields", 
                    "Type", 
                    "Comment", 
                    "RelatedContent", 
                    "Escalated", 
                    "WorkItemId",
                    "State",
                    "WorkitemPageUrl",
                    "Created",
                    "Completed",
                    "Performer",
                    "SkillSet",
                };
            return new Collection<FieldBase>(
                typeof (TicketTask).GetProperties().Where(p => p.GetCustomAttributes(typeof (DataMemberAttribute), false).Any())
                    .Where(p => !exclude.Contains(p.Name))
                    .Select(GetField).ToList());
        }

        private static FieldBase GetField(PropertyInfo pi)
        {
            var field = (FieldBase) Activator.CreateInstance(typeof (TaskField<>).MakeGenericType(pi.PropertyType));
            field.DisplayName = pi.Name;
            field.Name = pi.Name;
            field.Type = pi.PropertyType;
            return field;
        }
    }
}
