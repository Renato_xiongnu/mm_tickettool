﻿using System.Runtime.Serialization;

namespace TicketTool.Activities.Tasks
{
    [DataContract]
    public class TaskResponse
    {
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public bool CompleteSuccessful { get; set; }
    }
}
