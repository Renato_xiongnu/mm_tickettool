﻿using System;
using System.Activities;
using System.ComponentModel;
using TicketTool.Activities.Properties;
using TicketTool.Activities.Tasks.Design;

namespace TicketTool.Activities.Tasks
{
    [Designer(typeof(RequestedDataFieldDesigner))]
    public class RequestedDataField<TType> : RequestedDataFieldBase
    {
        public RequestedDataField()
        {
            this.Type = typeof (TType);
        }

        [Browsable(false)]
        public OutArgument<TType> Value { get; set; }        

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            base.CacheMetadata(metadata);

            if (String.IsNullOrEmpty(this.Name))
            {
                metadata.AddValidationError(Resources.Err_NameMustBeFilledOut);
            }
            if (!IsAllowedType(typeof (TType)))
            {
                metadata.AddValidationError(Resources.Err_OnlyPrimitiveTypesAreAllowed);
            }

            var arg = new RuntimeArgument(Resources.Param_Value, typeof(TType), ArgumentDirection.Out, true);
            metadata.Bind(this.Value, arg);
            metadata.AddArgument(arg);
        }

        internal override bool Validate(object data)
        {
            return data != null && data.GetType() == typeof(TType);
        }

        protected override void Execute(NativeActivityContext context)
        {
            if (this.Value == null || this.RequestedData == null) return;
            var data = this.RequestedData.Get(context);
            if (data.ContainsKey(this.Name))
            {
                this.Value.Set(context, data[this.Name]);
            }
        }
    }
}
