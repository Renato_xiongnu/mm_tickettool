﻿using System;

namespace TicketTool.Services.Common.Context
{
    public class TicketToolContext
    {
        private UserCredential _userCredential;
        private ServiceContext _context;

        public string ContextId { get; private set; }

        public TicketToolContext()
        {
            ContextId = Guid.NewGuid().ToString();
        }

        public UserCredential UserCredential
        {
            get { return _userCredential ?? (_userCredential = new UserCredential()); }
            set  { _userCredential = value; }
        }        

        public T RunInServiceContext<T>(Func<T> function, bool forceOpenConnection = false)
        {
            using (_context = new ServiceContext(UserCredential))
            {
                return function();
            }
        }

        public void RunInServiceContext(Action action, bool forceOpenConnection = false)
        {
            using (_context = new ServiceContext(UserCredential))
            {
                action();
            }
        }

        public static ServiceContext Ctx
        {
            get { return OperationContextHolder<TicketToolContext>.Current._context; }
        }

    }
}