﻿using System.ServiceModel;
using System.Threading;

namespace TicketTool.Services.Common.Context
{
    public class OperationContextHolder<T> : IExtension<OperationContext> where T : new()
    {       
        private static readonly ThreadLocal<T> StaticExtension;

        static OperationContextHolder()
        {
            StaticExtension = new ThreadLocal<T>(() => new T());
        }        

        public OperationContextHolder()
        {
            Extension = new T();
        }

        public T Extension { get; private set; }

        public void Attach(OperationContext owner)
        {            
        }

        public void Detach(OperationContext owner)
        {            
        }

        public static OperationContextHolder<T> GetHolder()
        {            
            if (OperationContext.Current == null)
                return null;
            return OperationContext.Current.Extensions.Find<OperationContextHolder<T>>();
        }

        public static T Current
        {
            get
            {
                var holder = GetHolder();
                if (holder == null)
                    return StaticExtension.Value;

                return holder.Extension;
            }
        }
    }
}