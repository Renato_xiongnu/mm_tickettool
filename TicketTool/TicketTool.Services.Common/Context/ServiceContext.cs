﻿using System;
using System.Collections.Generic;
using TicketTool.Dal;

namespace TicketTool.Services.Common.Context
{
    public class UserCredential
    {
        public string Name { get; set; }

        public IReadOnlyCollection<string> Roles { get; set; }
    }

    public class ServiceContext : IDisposable
    {        
        private DataProvider _dataProvider;
        public DataProvider DataProvider
        {
            get { return _dataProvider ?? (_dataProvider = new DataProvider()); }            
        }

        public UserCredential UserCredential { get; private set; }      

        public ServiceContext(UserCredential credential)
        {
            UserCredential = credential;
        }

        public void Dispose()
        {
            if (_dataProvider != null)
            {
                _dataProvider.Dispose();
                _dataProvider = null;
            }
        }              
    }
}
