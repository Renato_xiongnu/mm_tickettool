﻿using Quartz;
using TicketTool.Data;
using TicketTool.Data.Context;
using log4net;

namespace TicketTool.Scheduler
{
    public class UpdateExternalSystemsJob : TicketToolJobBase
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(UpdateExternalSystemsJob).Name);

        protected override void ExecuteInternal(JobExecutionContext context)
        {
            Log.Info("Update external systems start");
            OperationContextHolder<TicketToolContext>.Current.RunInServiceContext(
                () => new TicketService().ExternalSystems.UpdateTaskTypes());
            Log.Info("Update external systems end");
            SendOk("Scheduler.UpdateExternalSystemsJob");
        }
    }
}

