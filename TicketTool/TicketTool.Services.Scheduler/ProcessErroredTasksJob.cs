﻿using Quartz;
using TicketTool.Data;
using TicketTool.Data.Context;
using log4net;

namespace TicketTool.Scheduler
{
    public class ProcessErroredTasksJob : TicketToolJobBase
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(ProcessErroredTasksJob).Name);

        protected override void ExecuteInternal(JobExecutionContext context)
        {
            Log.Info("Completion errored tasks was started");
            OperationContextHolder<TicketToolContext>.Current.RunInServiceContext(
                () => new TicketService().Tasks.ProcessErroredQueue());
            Log.Info("Completion errored tasks was ended");
            SendOk("Scheduler.ProcessErroredTasksJob");
        }
    }
}
