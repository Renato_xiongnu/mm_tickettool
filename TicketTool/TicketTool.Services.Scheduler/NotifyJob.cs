﻿using TicketTool.Data;
using TicketTool.Data.Context;
using log4net;
using Quartz;

namespace TicketTool.Scheduler
{
    public class NotifyJob : TicketToolJobBase
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(NotifyJob).Name);

        protected override void ExecuteInternal(JobExecutionContext context)
        {
            Log.Info("Notify start");
            OperationContextHolder<TicketToolContext>.Current.RunInServiceContext(
                () => new TicketService().Tasks.SendTasks());
            Log.Info("Notify end");
            SendOk("Scheduler.NotifyJob");
        }
    }
}
