﻿using TicketTool.Data;
using TicketTool.Data.Context;
using log4net;
using Quartz;

namespace TicketTool.Scheduler
{
    public class CheckAndUpdateTasksJob : TicketToolJobBase
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(CheckAndUpdateTasksJob).Name);

        protected override void ExecuteInternal(JobExecutionContext context)
        {
            Log.Info("CheckAndUpdate start");
            OperationContextHolder<TicketToolContext>.Current.RunInServiceContext(
                () => new TicketService().Tasks.CheckAndUpdateTasks());
            Log.Info("CheckAndUpdate end");
            SendOk("Scheduler.CheckAndUpdateTasksJob");
        }
    }
}
