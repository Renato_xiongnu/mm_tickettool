﻿using System;
using System.Data.Entity.Validation;
using System.Text;
using log4net;
using MMS.Proxy.Monitoring;
using Quartz;
using TicketTool.Data.Common;
using TicketTool.Scheduler.Properties;

namespace TicketTool.Scheduler
{
    public abstract class TicketToolJobBase : IStatefulJob
    {
        public void Execute(JobExecutionContext context)
        {
            var quartzLog = LogManager.GetLogger("QuartzScheduler");
            var jobName = (context == null ? "test" : context.JobDetail.FullName);
            quartzLog.Info("Start Action: " + jobName);
            try
            {
                ExecuteInternal(context);
            }
            catch(DbEntityValidationException ex)
            {
                var errorStringBilder = new StringBuilder();

                foreach(var entityValidationError in ex.EntityValidationErrors)
                {
                    errorStringBilder.AppendFormat("Entity \"{0}\" in state \"{1}\", errors:",
                        entityValidationError.Entry.Entity.GetType().Name, entityValidationError.Entry.State);

                    foreach(var error in entityValidationError.ValidationErrors)
                    {
                        errorStringBilder.AppendFormat(" (Property: \"{0}\", Error: \"{1}\")", error.PropertyName,
                            error.ErrorMessage);
                    }
                }
                ex.Data.Add("EntityValidationErrors", errorStringBilder.ToString());
                quartzLog.Error(ex.GetInnerExceptionsText(), ex);
                throw new JobExecutionException(ex, false);
            }
            catch(Exception ex)
            {
                quartzLog.Error(ex.GetInnerExceptionsText(), ex);
                throw new JobExecutionException(ex, false);
            }
            quartzLog.Info("End Action: " + jobName);
        }

        protected static void SendOk(string serviceName)
        {
            using (var monitoring = new MonitoringServiceClient())
                monitoring.OK(serviceName, Resources.OK, null, Settings.Default.NagiosHost);
        }

        protected abstract void ExecuteInternal(JobExecutionContext context);
    }
}