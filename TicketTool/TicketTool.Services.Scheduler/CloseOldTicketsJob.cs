﻿using log4net;
using Quartz;
using TicketTool.Data;
using TicketTool.Data.Context;

namespace TicketTool.Scheduler
{
    public class CloseOldTicketsJob : TicketToolJobBase
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(CloseOldTicketsJob).Name);

        protected override void ExecuteInternal(JobExecutionContext context)
        {
            Log.Info("Closing tickets job was started");
            OperationContextHolder<TicketToolContext>.Current.RunInServiceContext(
                () => new TicketService().Tickets.CloseOldTickets());
            Log.Info("Closing tickets job was ended");
        }
    }
}
