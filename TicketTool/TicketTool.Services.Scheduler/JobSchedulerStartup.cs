﻿using System;
using Common.Logging;
using Quartz;
using Quartz.Impl;

namespace TicketTool.Scheduler
{
    public class JobSchedulerStartup
    {
        private static readonly ILog Logger = LogManager.GetLogger("QuartzScheduler");

        private IScheduler _scheduler;

        public void StartQuartz()
        {
            try
            {
                ISchedulerFactory factory = new StdSchedulerFactory();

                _scheduler = factory.GetScheduler();
                _scheduler.Start();

                Logger.Info("QuartzScheduler has been started");
            }
            catch(Exception ex)
            {
                Logger.Error(ex);
            }
        }

        public void StopQuartz()
        {
            try
            {
                if(_scheduler != null)
                {
                    _scheduler.Shutdown(false);
                    _scheduler = null;
                }
                Logger.Info("QuartzScheduler has been stopped");
            }
            catch(Exception ex)
            {
                Logger.Error(ex);
            }
        }
    }
}