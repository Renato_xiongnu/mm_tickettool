﻿using TicketTool.Data;
using TicketTool.Data.Context;
using log4net;
using Quartz;

namespace TicketTool.Scheduler
{
    public class CompleteTasksJob : TicketToolJobBase
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(CompleteTasksJob).Name);

        protected override void ExecuteInternal(JobExecutionContext context)
        {
            Log.Info("FinishTasks start");
            OperationContextHolder<TicketToolContext>.Current.RunInServiceContext(
                () => new TicketService().Tasks.ProcessTaskQueue());
            Log.Info("FinishTasks end");
            SendOk("Scheduler.CompleteTasksJob");
        }
    }
}
