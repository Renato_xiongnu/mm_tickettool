﻿using System;
using System.ServiceProcess;
using Common.Logging;
using TicketTool.Data.Common;

namespace TicketTool.Scheduler
{
    internal class Program
    {
        private static readonly ILog Logger = LogManager.GetLogger("QuartzScheduler");
        private static JobSchedulerStartup _scheduler;

        private static void Main()
        {
            log4net.Config.XmlConfigurator.Configure();            
            var start = DateTime.Now;
            try
            {
                _scheduler = new JobSchedulerStartup();
                if(!Environment.UserInteractive)
                {
                    using(var service = new SchedulerService(_scheduler))
                    {
                        ServiceBase.Run(service);
                    }
                }
                else
                {
                    Start();
                    while(true)
                    {
                        System.Threading.Thread.Sleep(1000);
                        Console.Clear();
                        Console.CursorTop = 0;
                        Console.CursorLeft = 0;
                        Console.WriteLine("{0:g}", DateTime.Now - start);
                    }                    
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.GetInnerExceptionsText(), ex);
            }
            
        }

        private static void Start()
        {
            _scheduler.StartQuartz();
        }

        private static void Stop()
        {
            _scheduler.StopQuartz();
        }
    }
}
