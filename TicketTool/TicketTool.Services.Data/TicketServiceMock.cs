﻿using Microsoft.Practices.Unity;
using TicketTool.Data.Strategies;
using TicketTool.Data.Strategies.Contracts;

namespace TicketTool.Data
{
    public class TicketServiceMock : ITicketService
    {
        [InjectionConstructor]
        public TicketServiceMock()
        {
            Tasks = new TaskStrategyMock();
            Tickets = new TicketStrategyMock();
        }

// ReSharper disable UnusedAutoPropertyAccessor.Local
        public ITaskStrategy Tasks { get; private set; }
        public IExternalSystemStrategy ExternalSystems { get; private set; }
        public ISkillSetStrategy SkillSets { get; private set; }
        public IParametersStrategy Parameters { get; private set; }
        public ITicketStrategy Tickets { get; private set; }
        public IUserStrategy Users { get; private set; }
        public ITaskOperationStrategy TaskOperations { get; private set; }
        public ISecurityStrategy Security { get; private set; }
// ReSharper restore UnusedAutoPropertyAccessor.Local
    }
}
