﻿using TicketTool.Data.Strategies.Contracts;

namespace TicketTool.Data
{
    public interface ITicketService
    {
        ITaskStrategy Tasks { get; }
        IExternalSystemStrategy ExternalSystems { get; }
        ISkillSetStrategy SkillSets { get; }
        IParametersStrategy Parameters { get; }
        ITicketStrategy Tickets { get; }
        IUserStrategy Users { get; }
        ITaskOperationStrategy TaskOperations { get; }
        ISecurityStrategy Security { get; }
    }
}
