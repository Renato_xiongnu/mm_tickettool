﻿using TicketTool.Data.Strategies;
using TicketTool.Data.Strategies.Contracts;

namespace TicketTool.Data
{
    public sealed class TicketService : ITicketService
    {        
        private IExternalSystemStrategy _externalSystems;
        private ISkillSetStrategy _skillSets;
        private IParametersStrategy _parameters;
        private ITicketStrategy _tickets;
        private IUserStrategy _users;
        private ITaskStrategy _tasks;
        private ITaskOperationStrategy _taskOperations;
        private ISecurityStrategy _security;

        public ITicketStrategy Tickets
        {
            get { return this._tickets ?? (this._tickets = new TicketStrategy(this)); }
        }

        public ISkillSetStrategy SkillSets
        {
            get { return _skillSets ?? (this._skillSets = new SkillSetStrategy(this)); }
        }

        public ITaskStrategy Tasks
        {
            get { return this._tasks ?? (this._tasks = new TaskStrategy(this)); }
        }

        public IExternalSystemStrategy ExternalSystems
        {
            get { return this._externalSystems ?? (this._externalSystems = new ExternalSystemStrategy(this)); }
        }

        public IParametersStrategy Parameters
        {
            get { return this._parameters ?? (this._parameters = new ParametersStrategy(this)); }
        }

        public IUserStrategy Users
        {
            get { return this._users ?? (this._users = new UserStrategy(this)); }
        }

        public ITaskOperationStrategy TaskOperations
        {
            get { return this._taskOperations ?? (this._taskOperations = new TaskOperationStrategy(this)); }
        }

        public ISecurityStrategy Security
        {
            get { return this._security ?? (this._security = new SecurityStrategy(this)); }
        }
    }
}
