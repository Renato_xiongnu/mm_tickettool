﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TicketTool.Data.Contracts
{
    [DataContract]
    public class UserCluster
    {
        [DataMember(IsRequired = true)]
        public string Name { get; set; }

        [DataMember(IsRequired = true)]
        public ICollection<string> Parameters { get; set; }

        [DataMember]
        public bool IsLogged { get; set; }
    }
}
