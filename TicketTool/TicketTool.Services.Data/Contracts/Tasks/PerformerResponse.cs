﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TicketTool.Data.Contracts.Tasks
{
    [DataContract]
    public class PerformerResponse
    {
        [DataMember]
        public string Outcome { get; set; }
        [DataMember]
        public Dictionary<string, object> FilledForm { get; set; }
    }
}
