﻿namespace TicketTool.Data.Contracts.Tasks
{
    public enum TaskState
    {
        Created,
        InProgress,
        Completed,
        Cancelled
    }
}