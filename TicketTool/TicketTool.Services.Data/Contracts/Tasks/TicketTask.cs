﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using TicketTool.Data.Contracts.Common;

namespace TicketTool.Data.Contracts.Tasks
{    
    [DataContract]
    public class TicketTask
    {
        [DataMember]
        public string TaskId { get; set; }

        [DataMember]        
        public string TicketId { get; set; }

        [DataMember]
        public string WorkItemId { get; set; }

        [DataMember]
        public string Parameter { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }        
        
        [DataMember]
        public string Type { get; set; }          
     
        [DataMember]
        public ICollection<Outcome> Outcomes { get; set; }

        [DataMember]
        public string Outcome { get; set; }

        [DataMember]
        public bool Escalated { get; set; }

        [DataMember]
        public ICollection<RequiredField> RequiredFields { get; set; }

        [DataMember]
        public string WorkitemPageUrl { get; set; }

        [DataMember]
        public string TaskPageUrl { get; set; }

        [DataMember]
        public TaskState State { get; set; }

        [DataMember]
        public string Performer { get; set; }

        [DataMember]
        public SkillSet SkillSet { get; set; }

        [DataMember]
        public DateTime Created { get; set; }

        [DataMember]
        public DateTime? Completed { get; set; }

        [DataMember]
        public PerformerResponse Response { get; set; }

        [DataMember]
        public string AssignedTo { get; set; }
    }
}
