﻿using System;
using System.Runtime.Serialization;

namespace TicketTool.Data.Contracts.Tasks
{
    [DataContract]
    public class PingTask
    {
        [DataMember]
        public string TaskId { get; set; }

        [DataMember]
        public TimeSpan UserTime { get; set; }

        [DataMember]
        public TimeSpan MaxTimeForUser { get; set; }
    }
}
