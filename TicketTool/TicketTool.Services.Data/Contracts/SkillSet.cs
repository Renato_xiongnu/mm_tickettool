﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using TicketTool.Data.Contracts.Common;

namespace TicketTool.Data.Contracts
{
    [DataContract]
    public class SkillSet
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string NameWithoutCluster { get; set; }

        //[DataMember]
        //public bool IsParameterizable { get; set; }

        [DataMember]
        public SkillSetParamType ParamType { get; set; }

        [DataMember]
        public bool IsEscalation { get; set; }

        [DataMember]
        public bool TasksDeliveredByEmail { get; set; }

        [DataMember]
        public ICollection<User> Users { get; set; }

        [DataMember]
        public ICollection<string> Roles { get; set; }   

    }
}
