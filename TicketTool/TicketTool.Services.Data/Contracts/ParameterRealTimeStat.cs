﻿using System;
using System.Runtime.Serialization;

namespace TicketTool.Data.Contracts
{
    [DataContract]
    public class ParameterRealTimeStat
    {
        [DataMember]
        public string ParameterName { get; set; }

        [DataMember]
        public int QtyOnCallCenter { get; set; }
        
        [DataMember]
        public TimeSpan? MaxTimeOnCallCenter { get; set; }

        [DataMember]
        public int QtyOnStore { get; set; }

        [DataMember]
        public TimeSpan? MaxTimeOnStore { get; set; }

        [DataMember]
        public int InProgressOnCallCenter { get; set; }

        [DataMember]
        public int InProgressOnStore { get; set; }

        [DataMember]
        public int FinishedTasksQty { get; set; }
    }
}
