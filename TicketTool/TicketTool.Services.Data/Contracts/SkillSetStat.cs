﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TicketTool.Data.Contracts
{
    [DataContract]
    public class SkillSetStat
    {
        [DataMember]
        public int TasksInQueue { get; set; }

        [DataMember]
        public int TasksInProgress { get; set; }

        [DataMember]
        public int EscalatedTasks { get; set; }

        [DataMember]
        public bool IsClustered { get; set; }

        [DataMember]
        public IList<SkillSetStat> ChildStats { get; set; } //For clustered skillsets
        //new fielsd
        [DataMember]
        public Int32? UsersCount { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string NameWithoutCluster { get; set; } 
    
        [DataMember]
        public SkillSetParamType ParamType { get; set; }

        [DataMember]
        public bool IsEscalation { get; set; }

        [DataMember]
        public bool TasksDeliveredByEmail { get; set; } 

        [DataMember]
        public ICollection<string> Roles { get; set; }

        public List<String> UsersClastersNames { get; set; } 

      
    }
}
