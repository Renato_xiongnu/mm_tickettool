﻿using System.Runtime.Serialization;

namespace TicketTool.Data.Contracts
{
    [DataContract]
    public class UserSkillSet
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public bool IsLogged { get; set; }

        [DataMember]
        public bool IsClustered { get; set; }

		[DataMember]
		public bool? IsEscalation { get; set; }

    }
}
