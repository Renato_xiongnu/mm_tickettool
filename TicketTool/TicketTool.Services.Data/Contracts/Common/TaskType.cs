﻿using System;
using System.Runtime.Serialization;

namespace TicketTool.Data.Contracts.Common
{
    [DataContract]
    public class TaskType
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string ExternalSystemName { get; set; }

        [DataMember]
        public TimeSpan MaxSkillSetTime { get; set; }

        [DataMember]
        public TimeSpan MaxUserSetTime { get; set; }

        [DataMember]
        public SkillSet EscalatedSkillSet { get; set; }

        [DataMember]
        public SkillSet SkillSet { get; set; }
    }

}
