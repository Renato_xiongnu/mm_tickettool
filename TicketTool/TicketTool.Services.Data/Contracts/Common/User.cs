﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TicketTool.Data.Contracts.Common
{
    [DataContract]  
    public class User
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public List<string> Roles { get; set; }

        [DataMember]
        public List<string> AvailableSkillSets { get; set; }

        [DataMember]
        public List<Parameter> Parameters { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public DateTime LastLoginDate { get; set; }
        [DataMember]
        public Boolean IsLocked { get; set; }

        [DataMember]
        public List<string> Clusters { get; set; } 
    }
}
