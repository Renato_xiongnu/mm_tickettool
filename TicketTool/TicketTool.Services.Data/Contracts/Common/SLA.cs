﻿using System;
using System.Runtime.Serialization;

namespace TicketTool.Data.Contracts.Common
{
    [DataContract]  
    public class SLA
    {
        [DataMember]
        public TimeSpan ControlTime { get; set; }

        [DataMember]
        public DateTime ControlDate { get; set; }

        [DataMember]
        public string Name { get; set; }        
    }
}
