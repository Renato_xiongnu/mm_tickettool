﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TicketTool.Data.Contracts.Common
{
    [DataContract]  
    public class Outcome
    {
        [DataMember]
        public string Value { get; set; }

        [DataMember]
        public bool IsSuccess { get; set; }

        [DataMember]
        public bool SkipTaskRequeriedFields { get; set; }

        [DataMember]
        public ICollection<RequiredField> RequiredFields { get; set; }        
    }
}
