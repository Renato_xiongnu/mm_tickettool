﻿using System.Runtime.Serialization;

namespace TicketTool.Data.Contracts.Common
{
    [DataContract]
    public class Parameter
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string ParameterName { get; set; }

        [DataMember]
        public string TimeZoneOffset { get; set; }
    }
}
