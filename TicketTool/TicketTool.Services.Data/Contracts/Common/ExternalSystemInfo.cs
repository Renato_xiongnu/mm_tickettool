﻿using System.Runtime.Serialization;

namespace TicketTool.Data.Contracts.Common
{
    [DataContract]
    public class ExternalSystemInfo
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
