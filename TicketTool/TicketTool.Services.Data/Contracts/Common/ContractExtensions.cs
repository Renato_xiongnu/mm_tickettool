﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.RegularExpressions;
using TicketTool.Dal.Model;
using TicketTool.Data.Common;
using TicketTool.Data.Contracts.Tasks;
using TicketTool.Data.Properties;
using TicketTool.Data.Strategies;
using TicketTask = TicketTool.Data.Contracts.Tasks.TicketTask;

namespace TicketTool.Data.Contracts.Common
{
    internal static class ContractExtensions
    {
        public static Regex OrderIdMatch = new Regex(@"\d{3}\-\d{8,12}");

        public static User ToContract(this Dal.Model.User model)
        {
            return new User
            {
                           Id = model.UserId,
                           Name = model.Name,
                           Parameters = model.Parameters != null ? model.Parameters.Select(p => p.ToContract()).ToList() : new List<Parameter>(),
                           Roles = model.Roles != null ? model.Roles.Select(r => r.Name).ToList() : new List<string>(),
                           AvailableSkillSets = model.AvailableSkillSets != null ? model.AvailableSkillSets.Select(r => r.Name).ToList() : new List<string>(),
                           Email = model.Email,
                           LastLoginDate = model.LastLoginDate,
                           CreatedDate = model.CreateDate,
                           IsLocked = model.IsLocked,
                           Clusters = model.Clusters.Select(el=>el.Name).ToList()
                       };
        }

        public static Parameter ToContract(this Dal.Model.Parameter parameter)
        {
            return new Parameter
            {
                ParameterName = parameter.ParameterName,
                Name = parameter.Name,
                TimeZoneOffset = parameter.TimeZoneOffset
            };
        }

        public static SkillSet ToContract(this Dal.Model.SkillSet model)
        {
            return new SkillSet
            {
                Users = model.Users == null ? null : model.Users.Select(t => t.ToContract()).ToList(),
                Name = model.Name,
                TasksDeliveredByEmail = model.DeliveryTaskByEmail,
                IsEscalation = model.IsEscalation,
                ParamType = model.IsParameterizable ? SkillSetParamType.SapCode : SkillSetParamType.None,
              
            };
        }

        public static TaskListItem ToContract(this Dal.Model.TicketTask task, string userName)
        {
            return new TaskListItem
            {
                Name = task.Name,
                CreationDate = task.CreateDate,
                MaxRunTime = task.TaskType.MaxSkillSetRunTime,
                Url = TaskStrategy.GenerateTaskUrl(task, userName),
                WorkitemId = task.Ticket.WorkItemId,
                IsAssignedOnMe = task.CurrentAssignee != null && task.CurrentAssignee.Name == userName,
                Escalated = task.Escalated,
                AssignedOn = task.CurrentAssignee != null ? task.CurrentAssignee.Name : null,
            };
        }

        public static TicketTask CreateFromModel(this Dal.Model.TicketTask model)
        {
            var matchOrderId = OrderIdMatch.Match(model.Ticket.WorkItemId);

            var ticketTask = new TicketTask
            {
                TicketId = model.Ticket.TicketId,
                TaskId = model.TaskId.ToString(CultureInfo.InvariantCulture),
                WorkItemId = model.Ticket.WorkItemId,
                Name = model.Name,
                Description = model.Description,
                Performer = model.Performer == null ? String.Empty : model.Performer.Name,
                AssignedTo = model.CurrentAssignee != null ? model.CurrentAssignee.Name : string.Empty,
                Outcomes = DeseralizeOtcomes(model.Outcomes.Data).ToList(),
                Outcome = model.CurrentOutcome,
                Escalated = model.Escalated,
                Created = model.CreateDate,
                Completed = model.FinishDate,
                SkillSet = model.CurrentSkillSet.ToContract(),                
                Parameter = model.Parameter,
                Type = model.TaskType.Name,
                RequiredFields = model.TaskFields == null
                    ? null
                    : model.TaskFields.Select(t =>
                    {
                        RequiredField result;
                        if (!string.IsNullOrEmpty(t.FieldType.PredefinedValuesList))
                        {
                            result = new RequiredFieldValueList();
                            ((RequiredFieldValueList)result).PredefinedValuesList = FieldTypeUtils.StringsToArray(t.FieldType.PredefinedValuesList);
                        }
                        else
                        {
                            result = new RequiredField();
                        }

                        result.Type = t.FieldType.Type;
                        result.Name = t.FieldType.Name;
                        result.DefaultValue = t.FieldType.DefaultValue;
                        result.Value = t.Value;
                        return result;
                    }).ToList()
                ,
                WorkitemPageUrl = model.TaskType.WorkitemPageUrl != null
                    ? model.TaskType.WorkitemPageUrl.Replace("{id}", matchOrderId.Success ? matchOrderId.Value : model.Ticket.WorkItemId)
                    : null,
                    TaskPageUrl = model.TaskType.TaskPageUrl,
                State = ToContractState((TaskStatus)model.TaskStatus)
            };

            if (model.TaskCompletionQueue != null && !String.IsNullOrEmpty(model.TaskCompletionQueue.SerializedTask))
            {
                var completedTask = DataContractJsonHelper<TicketTask>.DeserializeData(model.TaskCompletionQueue.SerializedTask);
                ticketTask.Response = new PerformerResponse
                {
                    Outcome = completedTask.Outcome,
                    FilledForm = completedTask.RequiredFields.ToDictionary(f => GetFieldName(f.Name), f => f.Value)
                };
            }
            return ticketTask;
        }

        private static string GetFieldName(string name)
        {
            var delIndex = name.IndexOf(Settings.Default.FieldDelimiter, StringComparison.Ordinal);
            if (delIndex == -1) return name;

            var fieldName = name.Split(new[] {Settings.Default.FieldDelimiter}, StringSplitOptions.RemoveEmptyEntries);
            return fieldName.Length >= 2 ? fieldName[1] : fieldName[0];
        }

        private static TaskState ToContractState(TaskStatus taskStatus)
        {
            switch (taskStatus)
            {
                case TaskStatus.None:
                case TaskStatus.Created:
                    return TaskState.Created;
                case TaskStatus.AssignedToSkillSet:
                case TaskStatus.AssignedToUser:
                case TaskStatus.RemovedFromUser:
                case TaskStatus.RemovedFromSkillSet:
                case TaskStatus.Escalated:
                    return TaskState.InProgress;
                case TaskStatus.Finished:
                    return TaskState.Completed;
                case TaskStatus.Cancel:
                    return TaskState.Cancelled;
                default:
                    throw new ArgumentOutOfRangeException("taskStatus");
            }
        }

        public static string SeralizeOtcomes(IEnumerable<Outcome> outcomes)
        {
            // return JsonConvert.SerializeObject(outcomes);            
            var stream1 = new MemoryStream();
            var ser = new DataContractJsonSerializer(typeof(IEnumerable<Outcome>));
            ser.WriteObject(stream1, outcomes);
            stream1.Position = 0;
            var sr = new StreamReader(stream1, Encoding.UTF8);
            return sr.ReadToEnd();
        }

        private static IEnumerable<Outcome> DeseralizeOtcomes(string value)
        {
            var ser = new DataContractJsonSerializer(typeof(IEnumerable<Outcome>));
            Stream s = new MemoryStream(Encoding.UTF8.GetBytes(value));
            s.Position = 0;
            return (IEnumerable<Outcome>)ser.ReadObject(s);
            //return JsonConvert.DeserializeObject<IEnumerable<Outcome>>(value);          
        }
    }
}
