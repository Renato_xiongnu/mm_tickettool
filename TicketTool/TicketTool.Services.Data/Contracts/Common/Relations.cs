﻿using System.Runtime.Serialization;

namespace TicketTool.Data.Contracts.Common
{
    [DataContract]
    public  class  SkillSetRelation
    {
        [DataMember]
        public string SkillSetName { get; set; }

        [DataMember]
        public string TaskTypeName { get; set; }
    }
}
