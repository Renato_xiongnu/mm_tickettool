﻿using System.Runtime.Serialization;
using TicketTool.Dal.Model;

namespace TicketTool.Data.Contracts.Common
{    
    [KnownType(typeof(RequiredFieldValueList))]
    [DataContract]    
    public class RequiredField
    {
        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public object DefaultValue { get; set; }

        [DataMember]
        public object Value { get; set; }

        public virtual string GetPredefinedValuesList()
        {
            return default(string);
        }        
    }
  
    [DataContract]
    public class RequiredFieldValueList : RequiredField
    {
        [DataMember]
        public object[] PredefinedValuesList { get; set; }

        public override string GetPredefinedValuesList()
        {
            return FieldTypeUtils.ArrayToStrings(this.PredefinedValuesList);
        }
    }
}
