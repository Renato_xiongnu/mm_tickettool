﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TicketTool.Data.Contracts.Common
{
    [DataContract]
    public class TicketTO
    {
        [DataMember]
        public string TicketId { get; set; }

        /// <summary>
        /// По факту - это Sap Code
        /// </summary>
        [DataMember]
        public string Parameter { get; set; }

        [DataMember]
        public string WorkitemId { get; set; }

        /// <summary>
        /// Название системы, для которой создается (???)
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public DateTime CreateDate { get; set; }

        [DataMember]
        public DateTime UpdateDate { get; set; }

        [DataMember]
        public DateTime? CloseDate { get; set; }

        [DataMember]
        public string ExternalSystem { get; set; }

    }
}
