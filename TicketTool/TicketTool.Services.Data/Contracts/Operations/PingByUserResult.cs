﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using TicketTool.Data.Contracts.Tasks;

namespace TicketTool.Data.Contracts.Operations
{
    [DataContract]
    public class PingByUserResult
    {
        [DataMember]
        public ICollection<PingTask> Tasks { get; set; }

        [DataMember ]
        public ICollection<string> LoggedOnSkillSets { get; set; } 

        //Бездействие пользователя
        //[DataMember]
        //public DateTime UserIdleTime { get; set; }
    }
}