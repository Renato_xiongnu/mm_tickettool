﻿using System.Runtime.Serialization;

namespace TicketTool.Data.Contracts.Operations
{
    [DataContract]
    public class OperationResult
    {
        [DataMember]
        public bool HasError { get; set; }

        [DataMember]
        public string MessageText { get; set; }
    }


    [DataContract]
    public class OperationResult<TResult> : OperationResult
    {
        [DataMember]
        public TResult Result { get; set; }
    }
}
