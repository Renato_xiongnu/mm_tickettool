﻿using TicketTool.Data.Contracts.Tasks;

namespace TicketTool.Data.Contracts.Operations
{
    public class AssignTaskForMeResult : OperationResult<TicketTask>
    {
    }
}