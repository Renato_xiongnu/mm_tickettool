﻿using System;
using System.Runtime.Serialization;

namespace TicketTool.Data.Contracts
{    
    [DataContract]
    public class UserStat
    {
        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string SkillSetName { get; set; }

        [DataMember]
        public DateTime LoginDate { get; set; }

        [DataMember]
        public DateTime? LogoutDate { get; set; }

        [DataMember]
        public int AbandonedQty { get; set; }

        [DataMember]
        public int ProcessedQty { get; set; }

        [DataMember]
        public TimeSpan SkillSetTime { get; set; }        
    }
}
