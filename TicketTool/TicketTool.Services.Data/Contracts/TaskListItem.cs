﻿using System;
using System.Runtime.Serialization;

namespace TicketTool.Data.Contracts
{
    [DataContract]
    public class TaskListItem
    {
        [DataMember]
        public DateTime CreationDate { get; set; }

        [DataMember]
        public DateTime DeadlineDate { get; set; } // should delete

        [DataMember]
        public TimeSpan? MaxRunTime { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public bool IsAssignedOnMe { get; set; }

        [DataMember]
        public string AssignedOn { get; set; }

        [DataMember]
        public string WorkitemId { get; set; }

        [DataMember]
        public bool Escalated { get; set; }
    }
}
