﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TicketTool.Data.Contracts
{
    [DataContract]
    public class Cluster
    {
        [DataMember(IsRequired = true)]
        public string Name { get; set; }

        [DataMember(IsRequired = true)]
        public ICollection<string> Parameters { get; set; } 
    }
}
