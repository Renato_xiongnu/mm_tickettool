﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TicketTool.Data.Contracts.Integration
{
    [DataContract]
    public class CallCenterStat
    {
        [DataMember]
        public string SkillSetName { get; set; }

        [DataMember]
        public int TaskCount { get; set; }

        [DataMember]
        public TimeSpan MaxDelay { get; set; }
    }
}
