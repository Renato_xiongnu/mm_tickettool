﻿using System.Runtime.Serialization;

namespace TicketTool.Data.Contracts
{
    [DataContract]
    public class TaskContentInfo
    {
        [DataMember]
        public string WorkItemId { get; set; }

        [DataMember]
        public string TicketParameter { get; set; }

        [DataMember]
        public string TaskParameter { get; set; }

        [DataMember]
        public string ContentName { get; set; }

        [DataMember]
        public bool IsReadOnlyContent { get; set; }
    }
}
