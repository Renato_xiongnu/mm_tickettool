﻿using System;
using TicketTool.Data.Context;
using TicketTool.Data.Strategies.Contracts;
using TicketTool.Dal.Model;

namespace TicketTool.Data.Strategies
{
    internal class TaskOperationStrategy : TicketToolStrategyBase, ITaskOperationStrategy
    {       
        private static readonly TimeSpan MaxTime = new TimeSpan(0, 23, 59, 59, 999);

        internal TaskOperationStrategy(ITicketService ticketService)
            : base(ticketService)
        {
        }

        private static TimeSpan? GetConstrainedTime(TimeSpan? timeSpan)
        {
            return timeSpan > MaxTime ? MaxTime : timeSpan;
        }

        public void UpdateOperation(TaskOperation operation, TimeSpan? userRuntime, TimeSpan? skillSetRuntime, SkillSet currentSkillSet, User currentUser, TaskOperation refOperation)
        {
            operation.UserRuntime = GetConstrainedTime(userRuntime);
            operation.SkillSetRuntime = GetConstrainedTime(skillSetRuntime);
            operation.RefOperation = refOperation;
            operation.User = currentUser;
            operation.SkillSet = currentSkillSet;
        }

        public TaskOperation CreateTask(TicketTask task)
        {
            var provider = TicketToolContext.Current.DataProvider;
            var currentDate = DateTime.Now.ToUniversalTime();
            var firstOp = provider.AddOperation(task, TaskStatus.None, TaskStatus.Created, currentDate);
            var secondOp = provider.AddOperation(task, (TaskStatus)task.TaskStatus, TaskStatus.AssignedToSkillSet, currentDate);

            secondOp.SkillSetDateAssigned = currentDate;

            this.UpdateOperation(secondOp, null, default(TimeSpan), task.CurrentSkillSet, null, firstOp);

            task.SkillSetDateAssigned = secondOp.SkillSetDateAssigned;
            task.SkillSetRuntime = secondOp.SkillSetRuntime;
            task.LastSkillSetOperation = secondOp;

            return secondOp;
        }        

        public TaskOperation CancelTask(TicketTask task)
        {
            var provider = TicketToolContext.Current.DataProvider;

            var currentDate = DateTime.Now.ToUniversalTime();
            var skillSetOp = provider.GetLastSkillSetOperation(task);
            var userOp = provider.GetLastUserOperation(task);
            var cancelOp = provider.AddOperation(task, (TaskStatus)task.TaskStatus, TaskStatus.Cancel, currentDate);
            TimeSpan? userRuntime = null;
            TimeSpan? skillSetRuntime = null;
            if (userOp != null)
            {
                userRuntime = currentDate - userOp.UserDateAssigned;
                UpdateOperation(cancelOp, userRuntime, currentDate - skillSetOp.SkillSetDateAssigned, task.CurrentSkillSet, task.CurrentAssignee, userOp);
            }
            else if (skillSetOp != null)
            {
                userRuntime = currentDate - skillSetOp.UserDateAssigned;
                skillSetRuntime = currentDate - skillSetOp.SkillSetDateAssigned;
                UpdateOperation(cancelOp, currentDate - skillSetOp.UserDateAssigned, skillSetRuntime, task.CurrentSkillSet, task.CurrentAssignee, skillSetOp); 
            }

            task.UserRuntime = GetConstrainedTime(userRuntime);
            task.SkillSetRuntime = GetConstrainedTime(skillSetRuntime);            
            task.CurrentAssignee = null;
            task.FinishDate = currentDate;

            return cancelOp;
        }

        public TaskOperation CompleteTask(TicketTask task)
        {
            var provider = TicketToolContext.Current.DataProvider;

            var currentDate = DateTime.Now.ToUniversalTime();
            var firstOp = provider.AddOperation(task, (TaskStatus)task.TaskStatus, TaskStatus.RemovedFromUser, currentDate);
            var secondOp = provider.AddOperation(task, (TaskStatus)task.TaskStatus, TaskStatus.RemovedFromSkillSet, currentDate);
            var lastOp = provider.AddOperation(task, (TaskStatus)task.TaskStatus, TaskStatus.Finished, currentDate);

            var skillSetOp = provider.GetLastSkillSetOperation(task);
            var userOp = provider.GetLastUserOperation(task);
            var userRuntime = currentDate - userOp.UserDateAssigned;
            var skillSetRuntime = currentDate - skillSetOp.SkillSetDateAssigned;

            UpdateOperation(firstOp, userRuntime, skillSetRuntime, task.CurrentSkillSet, task.CurrentAssignee, userOp);
            UpdateOperation(secondOp, userRuntime, skillSetRuntime, task.CurrentSkillSet, task.CurrentAssignee, skillSetOp);
            UpdateOperation(lastOp, userRuntime, skillSetRuntime, task.CurrentSkillSet, task.CurrentAssignee, secondOp);            

            task.UserRuntime = GetConstrainedTime(userRuntime);
            task.SkillSetRuntime = GetConstrainedTime(skillSetRuntime);            
            task.LastSkillSetOperation = secondOp;
            task.LastUserOperation = firstOp;
            task.CurrentAssignee = null;
            task.FinishDate = currentDate;

            return lastOp;
        }

        public TaskOperation InsertAssignedToUserOperation(TicketTask task)
        {
            var provider = TicketToolContext.Current.DataProvider;

            var currentDate = DateTime.Now.ToUniversalTime();
            var assignedOp = provider.AddOperation(task, (TaskStatus)task.TaskStatus, TaskStatus.AssignedToUser, currentDate); //+
                            
            var skillSetOp = provider.GetLastSkillSetOperation(task); //+

            assignedOp.UserDateAssigned = currentDate;   //+          

            UpdateOperation(assignedOp, default(TimeSpan), currentDate - skillSetOp.SkillSetDateAssigned, task.CurrentSkillSet, task.CurrentAssignee, null); //+

            task.UserDateAssigned = assignedOp.UserDateAssigned;
            task.UserRuntime = assignedOp.UserRuntime;
            task.LastUserOperation = assignedOp;            

            task.SkillSetRuntime = assignedOp.SkillSetRuntime;

            return assignedOp;
        }

        public TaskOperation InsertSkillSetAbandonedOperation(TicketTask task, SkillSet newSkillSet)
        {
            var provider = TicketToolContext.Current.DataProvider;

            var currentDate = DateTime.Now.ToUniversalTime();
            var abandonedOp = provider.AddOperation(task, (TaskStatus)task.TaskStatus, TaskStatus.RemovedFromSkillSet, currentDate);
            var escalationOp = provider.AddOperation(task, (TaskStatus)task.TaskStatus, TaskStatus.Escalated, currentDate);
            var skillSetOp = provider.GetLastSkillSetOperation(task);                   

            UpdateOperation(abandonedOp, null, currentDate - task.SkillSetDateAssigned, task.CurrentSkillSet, null, skillSetOp);
            UpdateOperation(escalationOp, null, null, newSkillSet, null, abandonedOp);
            
            task.SkillSetRuntime = abandonedOp.SkillSetRuntime;
            task.LastSkillSetOperation = escalationOp;
            task.CurrentSkillSet = null;

            return escalationOp;
        }

        public TaskOperation InsertRemoveFromSkillSetOperation(TicketTask task)
        {
            var provider = TicketToolContext.Current.DataProvider;

            var currentDate = DateTime.Now.ToUniversalTime();
            var removeOp = provider.AddOperation(task, (TaskStatus)task.TaskStatus, TaskStatus.RemovedFromSkillSet, currentDate);            
            var skillSetOp = provider.GetLastSkillSetOperation(task);  
            UpdateOperation(removeOp, null, currentDate - task.SkillSetDateAssigned, task.CurrentSkillSet, null, skillSetOp);

            task.SkillSetRuntime = removeOp.SkillSetRuntime;
            task.LastSkillSetOperation = removeOp;
            task.CurrentSkillSet = null;

            return removeOp;
        }

        public TaskOperation InsertAssignedToSkillSetOperation(TicketTask task)
        {
            var provider = TicketToolContext.Current.DataProvider;

            var currentDate = DateTime.Now.ToUniversalTime();
            var assignedOp = provider.AddOperation(task, (TaskStatus)task.TaskStatus, TaskStatus.AssignedToSkillSet, currentDate);

            assignedOp.SkillSetDateAssigned = currentDate;

            UpdateOperation(assignedOp, null, default(TimeSpan), task.CurrentSkillSet, null, task.LastSkillSetOperation);

            task.SkillSetDateAssigned = assignedOp.SkillSetDateAssigned;
            task.SkillSetRuntime = assignedOp.SkillSetRuntime;            
            task.LastSkillSetOperation = assignedOp;

            return assignedOp;
        }

        public TaskOperation InsertErrorOperation(TicketTask task, string errorText)
        {
            var provider = TicketToolContext.Current.DataProvider;
            var currentDate = DateTime.Now.ToUniversalTime();
            var operation = provider.AddOperation(task, (TaskStatus)task.TaskStatus, (TaskStatus)task.TaskStatus);

            if (task.CurrentAssignee != null)
            {
                operation.User = task.CurrentAssignee;
                operation.UserRuntime = GetConstrainedTime(currentDate - task.UserDateAssigned);
                task.UserRuntime = operation.UserRuntime;
            }

            if (task.CurrentSkillSet != null)
            {
                operation.SkillSet = task.CurrentSkillSet;
                operation.SkillSetRuntime = GetConstrainedTime(currentDate - task.SkillSetDateAssigned);
                task.SkillSetRuntime = operation.SkillSetRuntime;
            }
            
            operation.HasError = true;
            operation.MessageText = errorText;

            return operation;
        }

        public TaskOperation InsertUserAbandonedOperation(TicketTask task)
        {
            var provider = TicketToolContext.Current.DataProvider;

            var currentDate = DateTime.Now.ToUniversalTime();
            var abandonedOp = provider.AddOperation(task, (TaskStatus)task.TaskStatus, TaskStatus.RemovedFromUser, currentDate);
            var userOp = provider.GetLastUserOperation(task);
            var skillSetOp = provider.GetLastSkillSetOperation(task);

            this.TicketService.TaskOperations.UpdateOperation(abandonedOp, currentDate - task.UserDateAssigned, currentDate - skillSetOp.SkillSetDateAssigned, task.CurrentSkillSet, task.CurrentAssignee, userOp);

            task.UserRuntime = abandonedOp.UserRuntime;
            task.LastUserOperation = abandonedOp;
            task.CurrentAssignee = null;
            task.SkillSetRuntime = abandonedOp.SkillSetRuntime;

            return abandonedOp;
        }
    }
}
