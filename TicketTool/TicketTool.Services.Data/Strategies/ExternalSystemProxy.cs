﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Description;
using TicketTool.Data.Properties;
using TicketTool.Data.Strategies.Contracts;

namespace TicketTool.Data.Strategies
{
    internal class ExternalSystemProxy : IExternalSystemProxy
    {
        private readonly Assembly _assembly;
        private readonly ServiceEndpoint _endpoint;
        private readonly Type _contractInterface;
        private readonly string _contractName;

        public ExternalSystemProxy(Assembly assembly, ServiceEndpoint endpoint)
        {
            this._assembly = assembly;
            this._contractName = endpoint.Contract.Name;
            this._endpoint = endpoint;
            this._contractInterface = this._assembly.GetTypes().First(t => t.IsInterface && t.Name == this._contractName);
        }

        public object Invoke(string operationName, object[] parameters)
        {
            this.ValidateParameters(parameters);

            this._endpoint.Binding.ReceiveTimeout = TimeSpan.FromSeconds(Settings.Default.TaskCompletionTimeout);
            this._endpoint.Binding.SendTimeout = TimeSpan.FromSeconds(Settings.Default.TaskCompletionTimeout);
            this._endpoint.Binding.OpenTimeout = TimeSpan.FromSeconds(Settings.Default.TaskCompletionTimeout);
            this._endpoint.Binding.CloseTimeout = TimeSpan.FromSeconds(Settings.Default.TaskCompletionTimeout);

            var proxyType = this._assembly.GetTypes().First(t => t.IsClass && t.GetInterface(this._contractName) != null && t.GetInterface(typeof(ICommunicationObject).Name) != null);
            var proxyInstance = this._assembly.CreateInstance(
                proxyType.Name, false, BindingFlags.CreateInstance, null, new object[] {this._endpoint.Binding, this._endpoint.Address}, CultureInfo.CurrentCulture, null);
            var method = proxyType.GetMethod(operationName);
            var numberOfParameters = method.GetParameters().Count();
            //TODO Временная мера для совместимости со старым процессом
            if (parameters != null && parameters.Length > numberOfParameters)
            {
                parameters = parameters.Take(numberOfParameters).ToArray();
            }
            var result = method.Invoke(proxyInstance, parameters);
            proxyType.GetMethod("Close").Invoke(proxyInstance, null);
            return result;
        }

        public IEnumerable<string> Operations
        {
            get
            {
                return this._contractInterface.GetMethods().Select(t => new {t.Name, Index = t.Name.LastIndexOf("Continue", StringComparison.Ordinal)})
                           .Where(x => x.Index > -1)
                           .Select(x => x.Name.Substring(0, x.Index));
            }
        }

        private void ValidateParameters(IEnumerable<object> parameters)
        {
            if (parameters == null) return;
            var contractTypes = this._assembly.GetTypes()
                                                .Where(t => t.GetCustomAttribute<DataContractAttribute>() != null)
                                                .ToArray();
            foreach (var p in parameters)
            {
                if (p == null) continue;

                var paramType = p.GetType();
                if (paramType.Name.StartsWith("Dictionary")) //TODO: add real validation with calling ValidateParameters two times for generic args
                    continue;

                if (paramType.IsArray)
                {
                    ValidateParameters((IEnumerable<object>)p);
                }
                else
                {
                    if (!(IsPrimitive(paramType) || contractTypes.Any(t => t.FullName == paramType.FullName)))
                        throw new InvalidDataContractException(string.Format("Couldn't resolve type {0} in service proxy memory assemble", paramType.Name));
                }
            }
        }

        private static bool IsPrimitive(Type type)
        {
            return type.IsPrimitive || type == typeof(string) || type == typeof(decimal) || type == typeof(TimeSpan);
        }
    }
}