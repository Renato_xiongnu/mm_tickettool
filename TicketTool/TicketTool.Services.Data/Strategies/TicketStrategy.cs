﻿using System;
using System.Linq;
using TicketTool.Dal.Model;
using TicketTool.Data.Context;
using TicketTool.Data.Contracts.Common;
using TicketTool.Data.Strategies.Contracts;

namespace TicketTool.Data.Strategies
{
    internal class TicketStrategy : TicketToolStrategyBase, ITicketStrategy
    {
        internal TicketStrategy(ITicketService ticketService)
            : base(ticketService)
        {
        }

        public string CreateTicket(string externalSystemId, string workItemId = "", string parameter = "")
        {
            var provider = TicketToolContext.Current.DataProvider;

            if (provider.GetAllTickets().Any(t => t.WorkItemId == workItemId && t.ExternalSystem.ExternalSystemId == externalSystemId && t.CloseDate == null))
                throw new Exception(String.Format("At least one ticket already exist in TT for {0}. ExternalSystem is {1}", workItemId, externalSystemId));

            var ticket = provider.PrepareTicket(externalSystemId);
            ticket.WorkItemId = workItemId;
            ticket.Parameter = parameter;
            provider.Save();
            return ticket.TicketId;
        }

        public string GetTicket(string workItemId)
        {
            var provider = TicketToolContext.Current.DataProvider;
            var result = provider.GetTicketByWorkitemId(workItemId);
            return result != null ? result.TicketId : string.Empty;
        }

        public TicketTO GetTicketInfo(string ticketId)
        {
            TicketTO result = null;

            var provider = TicketToolContext.Current.DataProvider;
            var ticket = provider.GetTicketById(ticketId);

            if (ticket != null)
            {
                result = new TicketTO()
                        {
                            TicketId = ticket.TicketId,
                            WorkitemId = ticket.WorkItemId,
                            Parameter = ticket.Parameter,
                            Name = ticket.Name,
                            CreateDate = ticket.CreateDate,
                            UpdateDate = ticket.UpdateDate,
                            CloseDate = ticket.CloseDate,
                            //ExternalSystem = ticket.ExternalSystem.Name
                        };
            }

            return result;
        }

        public void CloseTicket(string ticketId)
        {
            var provider = TicketToolContext.Current.DataProvider;
            var ticket = provider.GetTicket(ticketId);

            ticket.CloseDate = DateTime.Now.ToUniversalTime();

            var tasks = provider.GetTasksByTicket(ticketId).Where(t => t.TaskStatus < (int)TaskStatus.Finished).ToList();
            if (tasks.Count > 0)
            {
                foreach (var ticketTask in tasks)
                {
                    this.TicketService.TaskOperations.CancelTask(ticketTask);
                }
            }

            provider.Save();
        }

        public void CloseOldTickets()
        {
            var date = DateTime.Now.AddMonths(-3);
            TicketToolContext.Current.DataProvider.ModelContext.Tickets
                .Where(t => t.CreateDate < date)
                .Where(t => t.CloseDate == null)
                .Select(t => t.TicketId)
                .ToList()
                .ForEach(this.CloseTicket);

            var provider = TicketToolContext.Current.DataProvider;
            var queue = provider.ModelContext.TicketTasks
                .Where(t => t.TaskCompletionQueue != null)
                .Where(t => t.TaskCompletionQueue.FinishDate == null)
                .Where(t => t.Ticket.CloseDate != null)
                .Select(t => t.TaskCompletionQueue)
                .ToList();
            foreach (var q in queue)
            {
                q.FinishDate = DateTime.UtcNow;
                q.CompletionStatus = (int)TaskCompletionStatus.Canceled;
                provider.Save();
            }
        }

    }
}