﻿using log4net;

namespace TicketTool.Data.Strategies
{
    public class TicketToolStrategyBase
    {        
        internal TicketToolStrategyBase(ITicketService ticketService)
        {
            Logger = LogManager.GetLogger(this.GetType().Name);
            TicketService = ticketService;
        }

        protected ILog Logger { get; private set; }

        protected ITicketService TicketService { get; private set; }
    }
}
