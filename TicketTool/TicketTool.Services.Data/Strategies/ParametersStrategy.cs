﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using TicketTool.Dal;
using TicketTool.Data.Context;
using TicketTool.Data.Contracts;
using TicketTool.Data.Contracts.Common;
using TicketTool.Data.Strategies.Contracts;

namespace TicketTool.Data.Strategies
{
    internal class ParametersStrategy : TicketToolStrategyBase, IParametersStrategy
    {
        internal ParametersStrategy(ITicketService ticketService)
            : base(ticketService)
        {
        }

        public ICollection<ParameterRealTimeStat> GetStatisticsForParameters()
        {
            var provider = TicketToolContext.Current.DataProvider;
            var currentDate = DateTime.Now.ToUniversalTime();

            var reportResult = provider.GetParameterRealTimeReport(/*currentDate.Date*/DataProvider.MinDbDateTime, currentDate.Date.AddDays(1));

            var finishedTasks = provider.GetCompletedTasks().Where(t => t.CreateDate > currentDate.Date)
                        .Select(t => new
                        {
                            t.Ticket.Parameter,
                            t.TaskId
                        }).ToList();

            return reportResult.Select(t => new ParameterRealTimeStat
            {
                ParameterName = t.ParameterName,
                QtyOnCallCenter = t.QtyOnCallCenter + t.InProgressOnCallCenter,
                MaxTimeOnCallCenter = t.MinDateOnCallCenter.HasValue ? currentDate - t.MinDateOnCallCenter : null,
                QtyOnStore = t.QtyOnStore + t.InProgressOnStore,
                MaxTimeOnStore = t.MinDateOnStore.HasValue ? currentDate - t.MinDateOnStore : null,
                InProgressOnCallCenter = t.InProgressOnCallCenter,
                InProgressOnStore = t.InProgressOnStore,
                FinishedTasksQty = finishedTasks.Count(x => x.Parameter == t.ParameterName)
            }).ToList();
        }

        public ICollection<Cluster> GetAllClusters()
        {
            return TicketToolContext.Current.DataProvider.GetAllClusters()
                .ToList()
                .Select(el=>new Cluster
                {
                    Name = el.Name,
                    Parameters = el.Parameters.Select(t=>t.ParameterName).ToList()
                })
                .ToList();
        }

        public void CreateCluster(Cluster cluster)
        {
            var provider = TicketToolContext.Current.DataProvider;
            if (provider.GetAllClusters().Any(t => t.Name == cluster.Name))
                throw new DuplicateNameException(string.Format("Cluster with name {0} already exists", cluster.Name));
            
            var dalCluster = new Dal.Model.Cluster
            {
                Name = cluster.Name,
                Parameters = provider.GetAllParameters().Where(t=>cluster.Parameters.Contains(t.ParameterName)).ToList()
            };

            provider.AddOrUpdateCluster(dalCluster);

            provider.Save();
        }

        public void UpdateCluster(string name, ICollection<string> newParameters)
        {
            var provider = TicketToolContext.Current.DataProvider;
            if (!provider.GetAllClusters().Any(t => t.Name == name))
                throw new ObjectNotFoundException(string.Format("Cluster with name {0} not found", name));

            var dalCluster = new Dal.Model.Cluster
            {
                Name = name,
                Parameters = provider.GetAllParameters().Where(t => newParameters.Contains(t.ParameterName)).ToList()
            };

            provider.AddOrUpdateCluster(dalCluster);

            provider.Save();
        }

        public void DeleteCluster(string name)
        {
            var provider = TicketToolContext.Current.DataProvider;
            provider.DeleteCluster(name);

            provider.Save();
        }

        public ICollection<Parameter> GetAllParameters()
        {
            return TicketToolContext.Current.DataProvider.GetAllParameters()
                .ToList()
                .Select(s => s.ToContract())
                .ToList();
        }
    }
}