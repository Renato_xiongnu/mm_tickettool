﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using log4net;
using TicketTool.Data.Strategies.Contracts;

namespace TicketTool.Data.Strategies
{
    public class TicketStrategyMock : ITicketStrategy
    {
        private static readonly Dictionary<string, string> TicketIds = new Dictionary<string, string>();
        private static readonly object SyncRoot = new object();
        private static readonly ILog Log;
        static TicketStrategyMock()
        {
            Log = LogManager.GetLogger("TicketTool.Services");
        }

        public string CreateTicket(string externalSystemId, string workItemId = "", string parameter = "")
        {
            var ticketId = Guid.NewGuid().ToString();
            Log.InfoFormat("workItemId = {0} , ticketId = {1}", workItemId, ticketId);
            lock (SyncRoot)
            {
                TicketIds.Add(ticketId, workItemId);
            }
            return ticketId;
        }

        public string GetTicket(string workItemId)
        {
            lock(SyncRoot)
            {
                if(!TicketIds.ContainsValue(workItemId))
                {
                    throw new ObjectNotFoundException("workItemId not founded");
                }
                return TicketIds.First(el => el.Value == workItemId).Key;
            }
        }

        public void CloseTicket(string ticketId)
        {
            lock (SyncRoot)
            {
                if (string.IsNullOrEmpty(ticketId))
                {
                    throw new ArgumentException("ticketId can not be empty");
                }
                if(!TicketIds.ContainsKey(ticketId))
                {
                    var ticketidNotFounded = string.Format("ticketId {0} not founded", ticketId);
                    throw new ObjectNotFoundException(ticketidNotFounded);
                }
                TicketIds.Remove(ticketId);
                Log.InfoFormat("Ticket {0} closed", ticketId);
            }
        }

        public void CloseOldTickets()
        {
            throw new NotImplementedException();
        }


        public Data.Contracts.Common.TicketTO GetTicketInfo(string ticketId)
        {
            throw new NotImplementedException();
        }
    }
}