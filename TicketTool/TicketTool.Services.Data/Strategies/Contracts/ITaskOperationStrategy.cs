﻿using System;
using TicketTool.Dal.Model;
using TicketTool.Data.Contracts.Operations;

namespace TicketTool.Data.Strategies.Contracts
{
    public interface ITaskOperationStrategy
    {
        void UpdateOperation(TaskOperation operation, TimeSpan? userRuntime, TimeSpan? skillSetRuntime, SkillSet currentSkillSet, User currentUser, TaskOperation refOperation);
        TaskOperation CreateTask(TicketTask task);
        TaskOperation CancelTask(TicketTask task);
        TaskOperation CompleteTask(TicketTask task);
        TaskOperation InsertAssignedToUserOperation(TicketTask task);
        TaskOperation InsertSkillSetAbandonedOperation(TicketTask task, SkillSet newSkillSet);
        TaskOperation InsertRemoveFromSkillSetOperation(TicketTask task);
        TaskOperation InsertAssignedToSkillSetOperation(TicketTask task);
        TaskOperation InsertErrorOperation(TicketTask task, string errorText);
        TaskOperation InsertUserAbandonedOperation(TicketTask task);
    }
}
