﻿using System.Collections.Generic;
using TicketTool.Data.Contracts;
using TicketTool.Data.Contracts.Common;

namespace TicketTool.Data.Strategies.Contracts
{
    public interface IParametersStrategy
    {
        ICollection<Parameter> GetAllParameters();
        ICollection<ParameterRealTimeStat> GetStatisticsForParameters();
        ICollection<Cluster> GetAllClusters();
        void CreateCluster(Cluster cluster);
        void UpdateCluster(string name, ICollection<string> newParameters);
        void DeleteCluster(string name);
    }
}
