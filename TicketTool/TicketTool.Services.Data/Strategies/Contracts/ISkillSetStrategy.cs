﻿using System.Collections.Generic;
using TicketTool.Data.Contracts;
using TicketTool.Data.Contracts.Common;

namespace TicketTool.Data.Strategies.Contracts
{
    public interface ISkillSetStrategy
    {
        ICollection<SkillSet> GetSkillSets(bool withDefault = false, bool withRoles = false);
        ICollection<SkillSet> GetAvailableSkillSetsForRoles(ICollection<string> roles);
        void RenameSkillSet(string oldName, string newName);
        ICollection<SkillSet> CreateSkillSets(IEnumerable<SkillSet> skillSets);
        ICollection<SkillSetStat> GetStatisticsForSkillSets(bool withClusters = true);
        void UpdateSkillSetRelation(string externalSystemId, ICollection<SkillSetRelation> addRelations);
        ICollection<UserSkillSet> GetUserSkillSets();
        ICollection<UserCluster> GetUserClusters();
        bool DeleteSkillSet(string skillSetName);
    }
}
