﻿using System.Collections.Generic;

namespace TicketTool.Data.Strategies.Contracts
{
    public interface IExternalSystemProxy
    {
        object Invoke(string operationName, object[] parameters);
        IEnumerable<string> Operations { get; }
    }
}
