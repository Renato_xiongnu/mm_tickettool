﻿using System.Collections.Generic;
using TicketTool.Data.Common;
using TicketTool.Data.Contracts.Common;
using TicketTool.Data.Contracts.Tasks;

namespace TicketTool.Data.Strategies.Contracts
{
    public interface IExternalSystemStrategy
    {
        void UpdateTaskTypes();
        ICollection<TaskType> GetTasksTypes(string externalSystemId);
        ICollection<ExternalSystemInfo> GetAllExternalSystems();
        void UpdateTaskType(string externalSystemId, TaskType ttype);
        TaskResponse Callback(string externalSystemId, TicketTask ticketTask);
        string GetCallbackName(TicketTask task);
    }
}
