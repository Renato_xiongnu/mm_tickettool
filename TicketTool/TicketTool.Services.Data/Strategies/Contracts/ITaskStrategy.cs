﻿using System;
using System.Collections.Generic;
using TicketTool.Data.Contracts;
using TicketTool.Data.Contracts.Common;
using TicketTool.Data.Contracts.Operations;
using TicketTool.Data.Contracts.Tasks;

namespace TicketTool.Data.Strategies.Contracts
{
    public interface ITaskStrategy
    {
        string CreateTask(TicketTask task);
        void CancelTask(long taskId);
        OperationResult CompleteTask(string taskId, string outcome, ICollection<RequiredField> requiredFields);
        OperationResult PostponeTask(string taskId, TimeSpan? time);
        TicketTask ReserveAndGetNextTask(bool skipAssigned);
        ICollection<TaskListItem> GetMyTasks(Parameter parameter);
        ICollection<TaskListItem> GetTasks(SkillSet skillSet, Parameter parameter);
        IEnumerable<TicketTask> GetTasks(string workitemId);
        void ProcessTaskQueue();
        void ProcessErroredQueue();
        void SendTasks();
        void CheckAndUpdateTasks();
        TicketTask GetTask(string taskId);
        ICollection<TaskListItem> GetMyTasksInProgress();
        ICollection<TaskListItem> GetAllTasksInProgress(string[] parameters, string[] skillSets);
        bool CanProcessed(string taskId, string userName);
        TicketTask AssignFreeTaskForMe(string taskId);

        /// <summary>
        /// Assigns the free task for me.
        /// </summary>
        /// <param name="taskId">The task identifier.</param>
        /// <returns></returns>
        TicketTask ForceAssignFreeTaskForMe(string taskId);
    }
}
