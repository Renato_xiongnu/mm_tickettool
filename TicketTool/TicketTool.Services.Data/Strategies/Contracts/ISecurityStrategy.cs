﻿namespace TicketTool.Data.Strategies.Contracts
{
    public interface ISecurityStrategy
    {
        bool CheckToken(string message, string token);
    }
}
