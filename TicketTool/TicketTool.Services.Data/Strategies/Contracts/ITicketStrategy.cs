﻿namespace TicketTool.Data.Strategies.Contracts
{
    public interface ITicketStrategy
    {
        string CreateTicket(string externalSystemId, string workItemId = "", string parameter = "");
        string GetTicket(string workItemId);
        void CloseTicket(string ticketId);
        void CloseOldTickets();

        TicketTool.Data.Contracts.Common.TicketTO GetTicketInfo(string ticketId);
    }
}
