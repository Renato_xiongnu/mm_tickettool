﻿using System.Collections.Generic;
using TicketTool.Data.Contracts;
using TicketTool.Data.Contracts.Operations;
using Parameter = TicketTool.Data.Contracts.Common.Parameter;
using SkillSet = TicketTool.Data.Contracts.SkillSet;
using User = TicketTool.Data.Contracts.Common.User;

namespace TicketTool.Data.Strategies.Contracts
{
    public interface IUserStrategy
    {
        ICollection<UserStat> GetStatisticsForUsers();
        void AddUserToSkillSetsIfSkillSetsNotExist(IEnumerable<SkillSet> skillSets, IEnumerable<Cluster> clusters=null);
        PingByUserResult PingByUser();
        void RemoveUserWithTasksFromSkillSets();
        ICollection<User> GetAllUsers();
        User GetUser(string userName);
        ICollection<User> GetUsers(Parameter parameter, IEnumerable<string> roles);
        ICollection<User> GetDefaultUsersByParam(Parameter parameter);
        long UpdateUserParams(string login, IEnumerable<string> parameters);
        long UpdateUserAvailableSkillSets(string login, IEnumerable<string> skillSets);
        void Abandon(string userName);
        void LockUnlockUser(string userName, bool isLock);
    }
}
