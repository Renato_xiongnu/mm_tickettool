﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Newtonsoft.Json;
using TicketTool.Dal;
using TicketTool.Dal.Model;
using TicketTool.Data.Context;
using TicketTool.Data.Contracts;
using TicketTool.Data.Contracts.Common;
using TicketTool.Data.Strategies.Contracts;
using SkillSet = TicketTool.Data.Contracts.SkillSet;

namespace TicketTool.Data.Strategies
{
    public class SkillSetStrategy : TicketToolStrategyBase, ISkillSetStrategy
    {

        public SkillSetStrategy(ITicketService ticketService)
            : base(ticketService)
        {

        }

        public void UpdateSkillSetRelation(string externalSystemId, ICollection<SkillSetRelation> addRelations)
        {
            var provider = TicketToolContext.Current.DataProvider;
            provider.GetExternalSystem(externalSystemId);

            using (var tran = new TransactionScope())
            {
                if (addRelations != null && addRelations.Count > 0)
                {
                    var skillSetRel = addRelations.ToDictionary(t => t.SkillSetName, t => t.TaskTypeName);
                    var taskTypes = provider.GetTaskTypes(externalSystemId, skillSetRel.Values, true).ToList();
                    var skillSets = provider.GetSkillSets(false, true).Where(t => skillSetRel.Keys.Contains(t.Name)).ToList();

                    foreach (var newSkillSet in skillSets)
                    {
                        var taskTypeName = skillSetRel[newSkillSet.Name];

                        var taskType = taskTypes.FirstOrDefault(t => t.Name == taskTypeName);
                        if (taskType == null) continue;
                        if (newSkillSet.IsEscalation)
                            taskType.SlaSkillSet = newSkillSet;
                        else
                        {
                            var oldSkillSet = taskType.SkillSet;
                            taskType.SkillSet = newSkillSet;

                            var defaultTasks = provider.GetAllTasks()
                                                       .Where(t => t.TaskStatus < (int)TaskStatus.Finished)
                                                       .Where(t => t.CurrentAssignee == null)
                                                       .Where(t => (t.CurrentSkillSet == null || t.CurrentSkillSet.SkillSetId == oldSkillSet.SkillSetId))
                                                       .ToList();

                            foreach (var task in defaultTasks)
                            {
                                TicketService.TaskOperations.InsertRemoveFromSkillSetOperation(task);
                                task.CurrentSkillSet = newSkillSet;
                                TicketService.TaskOperations.InsertAssignedToSkillSetOperation(task);
                            }
                        }
                        provider.Save();
                    }
                }
                tran.Complete();
            }
        }

        public ICollection<SkillSet> GetSkillSets(bool withDefault = false, bool withRoles = false)
        {
            var provider = TicketToolContext.Current.DataProvider;
            var skillSets = provider.GetSkillSets(false, withDefault, true, false, withRoles).ToList();

            return skillSets.Select(t => new SkillSet
            {
                Name = t.Name,
                ParamType =
                    t.IsParameterizable
                        ? SkillSetParamType.SapCode
                        : SkillSetParamType.None,
                IsEscalation = t.IsEscalation,
                TasksDeliveredByEmail = t.DeliveryTaskByEmail,
                Users =
                    t.Users == null
                        ? null
                        : t.Users.Select(usr => usr.ToContract()).
                            ToList(),
                Roles = t.Roles == null ? null : t.Roles.Select(m => m.Name).ToList()
            }).ToList();

        }

        public ICollection<SkillSet> CreateSkillSets(IEnumerable<SkillSet> skillSets)
        {
            var resultCollection = new List<SkillSet>();
            var provider = TicketToolContext.Current.DataProvider;
            foreach (var updSkillSet in skillSets)
            {
                var skillSet = provider.GetSkillSets(false, true).SingleOrDefault(t => t.Name == updSkillSet.Name);

                if (skillSet == null)
                {
                    skillSet = provider.PrepareSkillSet(updSkillSet.Name);

                    skillSet.IsParameterizable = updSkillSet.ParamType != SkillSetParamType.None;
                    skillSet.IsEscalation = updSkillSet.IsEscalation;
                    skillSet.DeliveryTaskByEmail = updSkillSet.TasksDeliveredByEmail;
                    skillSet.Roles = updSkillSet.Roles == null ? provider.GetAllRoles().ToList() : provider.GetAllRoles().Where(m => updSkillSet.Roles.Contains(m.Name)).ToList();
                }
                else
                {
                    throw new InvalidOperationException(string.Format("Skillset with name: {0} already exists", updSkillSet.Name));
                }


                resultCollection.Add(new SkillSet
                {
                    Name = skillSet.Name,
                    ParamType = skillSet.IsParameterizable ? SkillSetParamType.SapCode : SkillSetParamType.None,
                    IsEscalation = skillSet.IsEscalation,
                    TasksDeliveredByEmail = skillSet.DeliveryTaskByEmail,
                    Users = skillSet.Users == null ? null
                    : skillSet.Users.Select(usr => usr.ToContract()).ToList(),
                    Roles = skillSet.Roles == null ? null : skillSet.Roles.Select(m => m.Name).ToList()
                });

                provider.Save();
            }
            return resultCollection;
        }

        public void RenameSkillSet(string oldName, string newName)
        {
            var provider = TicketToolContext.Current.DataProvider;
            var s = provider.GetSkillSets().SingleOrDefault(ss => ss.Name == oldName);
            if (s != null)
            {
                s.Name = newName;
                provider.Save();
            }
        }

        public ICollection<UserSkillSet> GetUserSkillSets()
        {
            var user = TicketToolContext.Current.DataProvider.GetUser(TicketToolContext.Current.Credentials.Name,
                withRoles: true, withSkillSets: true, withAvailableSkillSets: true);
            var userSkillSets = user.SkillSets
                .Where(m => !m.IsDefault)
                .Select(t => new UserSkillSet { Name = t.Name, IsLogged = true, IsClustered = t.IsClustered, IsEscalation = t.IsEscalation })
                .ToDictionary(t => t.Name, t => t);
            var availableSkillSets = user.AvailableSkillSets
                .Where(m => !m.IsDefault)
                .Select(t => new UserSkillSet { Name = t.Name, IsLogged = false, IsClustered = t.IsClustered, IsEscalation = t.IsEscalation })
                .ToDictionary(t => t.Name, t => t);
            var allSkillSets = GetAvailableSkillSetsForRoles(user.Roles.Select(m => m.Name).ToList())
                .ToDictionary(t => t.Name, t => t);
            foreach (var elem in availableSkillSets)
            {
                if (allSkillSets.ContainsKey(elem.Key) && !userSkillSets.ContainsKey(elem.Key))
                    userSkillSets.Add(elem.Key, elem.Value);
            }
            return userSkillSets.Values;
        }

        public ICollection<UserCluster> GetUserClusters()
        {
            var provider = TicketToolContext.Current.DataProvider;
            var user = TicketToolContext.Current.DataProvider.GetUser(TicketToolContext.Current.Credentials.Name,
                withRoles: true, withSkillSets: true, withAvailableSkillSets: true, withClusters: true);

            return provider.GetAllClusters().ToList().Select(c => new UserCluster { Name = c.Name, Parameters = c.Parameters.Select(t => t.ParameterName).ToList(), IsLogged = user.Clusters.Contains(c) }).ToList();
        }

        public bool DeleteSkillSet(string skillSetName)
        {
            var provider = TicketToolContext.Current.DataProvider;
            var ss = provider.GetSkillSets(withTaskTypes: true, withUsers: true, withRoles: true).FirstOrDefault(s => s.Name == skillSetName);
            if (ss != null)
            {
                if (ss.TaskTypes.Any(tt => tt.SkillSet != null && tt.SkillSet.Name == skillSetName) ||
                    ss.Users.Any(u => u.SkillSets.Any(s => s.Name == skillSetName)))
                {
                    return false;
                }

                if (provider.GetAllTaskOperations().Any(to => to.SkillSet != null && to.SkillSet.Name == skillSetName) ||
                            provider.GetAllUserLogs().Any(ul => ul.SkillSet != null && ul.SkillSet.Name == skillSetName))
                {
                    ss.IsDeleted = true;
                }
                else
                {
                    provider.DeleteSkillSet(ss);
                }
                provider.Save();
                return true;
            }
            return false;

        }

        public ICollection<SkillSetStat> GetStatisticsForSkillSets(bool withClusters = true)
        {
            TicketToolContext.Current.DataProvider.ModelContext.Configuration.LazyLoadingEnabled = false;
            var skillSets = TicketToolContext.Current.DataProvider.GetSkillSets(true, withUserClusters: true).ToList();
            var result = InitSkillSetStat(skillSets, withClusters);

            return result;
        }

        private static IList<SkillSetStat> InitSkillSetStat(IList<Dal.Model.SkillSet> workSkillsets, bool withClusters = true)
        {
            var provider = TicketToolContext.Current.DataProvider;

            var allTasks = provider.GetTaskStat(workSkillsets).ToList();

            var allClusters = provider.GetAllClusters().ToList();
            var skillSets = new List<SkillSetStat>(workSkillsets.Count);

            foreach (var skillSet in workSkillsets)
            {
                var skillSetStat = new SkillSetStat();

                var allTasksInSkillset =
                    allTasks.Where(t => t.CurrentSkillSet.SkillSetId == skillSet.SkillSetId).ToList();
                var tasksInProgress = new List<DataProvider.TaskDto>();
                var tasksInQueue = new List<DataProvider.TaskDto>();
                if (!skillSet.IsEscalation)
                {
                    tasksInProgress =
                        allTasksInSkillset.Where(
                            t =>
                                t.TaskStatus == (int)TaskStatus.AssignedToUser &&
                                !t.Escalated)
                            .ToList();

                    tasksInQueue =
                        allTasksInSkillset.Where(
                            t =>
                                t.TaskStatus != (int)TaskStatus.AssignedToUser &&
                                !t.Escalated)
                            .ToList();
                }
                else
                {
                    tasksInProgress =
                        allTasksInSkillset.Where(
                            t =>
                                t.TaskStatus == (int)TaskStatus.AssignedToUser &&
                                t.Escalated)
                            .ToList();

                    tasksInQueue =
                        allTasksInSkillset.Where(
                            t =>
                                t.TaskStatus != (int)TaskStatus.AssignedToUser &&
                                t.Escalated)
                            .ToList();
                }

                skillSetStat.TasksInQueue = tasksInQueue.Count;
                skillSetStat.TasksInProgress = tasksInProgress.Count;

                var eskaletedTasks = provider.GetEscaltedTaskFromSkillSet(skillSet);
                skillSetStat.EscalatedTasks = eskaletedTasks.Count();
                skillSetStat.Name = skillSet.Name;
                skillSetStat.TasksDeliveredByEmail = skillSet.DeliveryTaskByEmail;
                skillSetStat.IsEscalation = skillSet.IsEscalation;
                skillSetStat.ParamType = skillSet.IsParameterizable ? SkillSetParamType.SapCode : SkillSetParamType.None;
                skillSetStat.UsersCount = skillSet.Users == null ? 0 : skillSet.Users.Count(x => x.Clusters.Any());
                if (skillSet.Users != null && skillSet.Users.Any(x => x.Clusters != null))
                {
                    skillSetStat.UsersClastersNames = skillSet.Users.SelectMany(x => x.Clusters.Select(n => n.Name)).ToList();
                }
                else
                {
                    skillSetStat.UsersClastersNames = new List<string>();
                }
                if (skillSet.IsClustered && withClusters)
                {
                    skillSetStat.IsClustered = skillSet.IsClustered;
                    skillSetStat.ChildStats = new List<SkillSetStat>();
                    foreach (var cluster in allClusters)
                    {
                        var clusteredSkillsetStat = JsonConvert.DeserializeObject<SkillSetStat>(JsonConvert.SerializeObject(skillSetStat));

                        clusteredSkillsetStat.NameWithoutCluster = clusteredSkillsetStat.Name;
                        clusteredSkillsetStat.Name = string.Format("{0}-{1}", clusteredSkillsetStat.Name, cluster.Name);
                        clusteredSkillsetStat.IsClustered = true;
                        var clusterParameterNames = cluster.Parameters.Select(t => t.ParameterName);

                        var escalatedTaskCount = eskaletedTasks.Count(t => clusterParameterNames.Contains(t.Parameter));
                        tasksInProgress = allTasksInSkillset.Where(t =>
                                    t.TaskStatus == (int)TaskStatus.AssignedToUser
                                    && !t.Escalated)
                                .Where(t => clusterParameterNames.Contains(t.Parameter)).ToList();

                        tasksInQueue =
                            allTasksInSkillset.Where(t =>
                                    t.TaskStatus != (int)TaskStatus.AssignedToUser
                                    && !t.Escalated)
                                .Where(t => clusterParameterNames.Contains(t.Parameter)).ToList();

                        clusteredSkillsetStat.TasksInQueue = tasksInQueue.Count;
                        clusteredSkillsetStat.TasksInProgress = tasksInProgress.Count;
                        clusteredSkillsetStat.EscalatedTasks = escalatedTaskCount;

                        clusteredSkillsetStat.UsersCount = 0;
                        if (skillSetStat.UsersClastersNames.Any())
                            clusteredSkillsetStat.UsersCount = skillSetStat.UsersClastersNames.Count(x => x.Contains(cluster.Name));

                        skillSetStat.ChildStats.Add(clusteredSkillsetStat);
                    }
                }

                skillSets.Add(skillSetStat);
            }

            return skillSets;
        }

        public ICollection<SkillSet> GetAvailableSkillSetsForRoles(ICollection<string> roles)
        {
            var skillSets = TicketToolContext.Current.DataProvider.GetSkillSets(withUsers: true, withDefault: false, withEscalation: true, withRoles: true, withTaskTypes: false, withUserClusters: true, withUsersData: true)
                .ToList()
                .Where(s => s.Roles.Select(r => r.Name).Intersect(roles).Any())
                .Select(t =>
                    new SkillSet
                    {
                        Name = t.Name,
                        ParamType = t.IsParameterizable ? SkillSetParamType.SapCode : SkillSetParamType.None,
                        IsEscalation = t.IsEscalation,
                        TasksDeliveredByEmail = t.DeliveryTaskByEmail,
                        Users = t.Users != null ? t.Users.Select(usr => usr.ToContract()).ToList() : null,
                        Roles = t.Roles != null ? t.Roles.Select(m => m.Name).ToList() : null
                    }
                ).ToList();
            return skillSets;
            //return TicketToolContext.Current.DataProvider.GetAllRoles(true, true)
            //               .Where(m => roles.Contains(m.Name))
            //               .ToList()
            //               .SelectMany(p => p.SkillSets)
            //               .Where(m => !m.IsDefault)
            //               .Distinct()
            //               .Select(t =>
            //                       new SkillSet
            //                       {
            //                           Name = t.Name,
            //                           ParamType = t.IsParameterizable ? SkillSetParamType.SapCode : SkillSetParamType.None,
            //                           IsEscalation = t.IsEscalation,
            //                           TasksDeliveredByEmail = t.DeliveryTaskByEmail,
            //                           Users = t.Users != null ? t.Users.Select(usr => usr.ToContract()).ToList() : null,
            //                           Roles = t.Roles != null ? t.Roles.Select(m => m.Name).ToList() : null
            //                       })
            //               .ToList();
        }
    }
}