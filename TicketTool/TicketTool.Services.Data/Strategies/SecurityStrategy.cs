﻿using TicketTool.Cryptography;
using TicketTool.Data.Strategies.Contracts;

namespace TicketTool.Data.Strategies
{
    public class SecurityStrategy : TicketToolStrategyBase, ISecurityStrategy
    {
        internal SecurityStrategy(ITicketService ticketService)
            : base(ticketService)
        {
        }

        public bool CheckToken(string message, string token)
        {
            return Encoder.Encode(message).Equals(token);
        }
    }
}