﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using TicketTool.Dal;
using TicketTool.Dal.Model;
using TicketTool.Data.Common;
using TicketTool.Data.Context;
using TicketTool.Data.Contracts.Common;
using TicketTool.Data.Properties;
using TicketTool.Data.Strategies.Contracts;
using TaskType = TicketTool.Data.Contracts.Common.TaskType;
using TicketTask = TicketTool.Data.Contracts.Tasks.TicketTask;

namespace TicketTool.Data.Strategies
{
    internal class ExternalSystemStrategy : TicketToolStrategyBase, IExternalSystemStrategy
    {
        internal ExternalSystemStrategy(ITicketService ticketService)
            : base(ticketService)
        {

        }

        public ICollection<ExternalSystemInfo> GetAllExternalSystems()
        {
            return TicketToolContext.Current.DataProvider.GetAllExternalSystems().ToList()
                                    .Select(t => new ExternalSystemInfo {Id = t.ExternalSystemId, Name = t.Name})
                                    .ToList();
        }

        public ICollection<TaskType> GetTasksTypes(string externalSystemId)
        {
            var taskTypes= TicketToolContext.Current.DataProvider.GetTaskTypes(externalSystemId).ToList();

            return taskTypes.Select(
                t => new TaskType
                    {
                        Name = t.Name,
                        ExternalSystemName = externalSystemId,
                        MaxSkillSetTime = t.MaxSkillSetRunTime ?? default(TimeSpan),
                        MaxUserSetTime = t.MaxUserRuntime ?? default(TimeSpan),
                        SkillSet = t.SkillSet == null ? null : t.SkillSet.ToContract(),
                        EscalatedSkillSet = t.SlaSkillSet == null ? null : t.SlaSkillSet.ToContract()
                    }).ToList();
        }

        public void UpdateTaskTypes()
        {
            var provider = TicketToolContext.Current.DataProvider;
            var systems = provider.GetAvailableExternalSystems().ToList();
            foreach (var system in systems)
            {
                try
                {
                    UpdateTaskTypes(system, provider);
                }
                catch(Exception ex)
                {
                    var message = string.Format("UpdateTaskTypes {0} exception", system.ExternalSystemId);
                    Logger.Error(message,ex);
                }
            }
        }

        private static void UpdateTaskTypes(ExternalSystem system, DataProvider provider)
        {
            var taskTypenames = SoapServiceBuilder.GetProxy(system.WsdlUrl).Operations.ToList();
            var taskTypes = provider.GetTaskTypes(system.ExternalSystemId, taskTypenames, true)
                .ToDictionary(t => new Tuple<string, string>(t.Name, system.ExternalSystemId));
            
            var settings = new Dictionary<string, Dal.Model.TaskType>();
            if (!String.IsNullOrEmpty(system.UseMapping))
                settings = provider.GetTaskTypes(system.UseMapping).ToDictionary(tt => tt.Name);

            foreach (var name in taskTypenames)
            {
                var key = new Tuple<string, string>(name, system.ExternalSystemId);
                if (taskTypes.ContainsKey(key)) continue;

                var taskType = provider.PrepareTaskType(system.ExternalSystemId, name);
                if (settings.ContainsKey(name))
                {
                    var tt = settings[name];
                    taskType.SkillSet = tt.SkillSet;
                    taskType.SlaSkillSet = tt.SlaSkillSet;
                    taskType.CanPostponeCallingWorkflow = tt.CanPostponeCallingWorkflow;
                    taskType.MaxSkillSetRunTime = tt.MaxSkillSetRunTime;
                    taskType.MaxUserRuntime = tt.MaxUserRuntime;
                    taskType.SlaName = tt.SlaName;
                    continue;
                }
                taskType.CanPostponeCallingWorkflow = true;
                if (!taskType.MaxSkillSetRunTime.HasValue) taskType.MaxSkillSetRunTime = Settings.Default.DefaultTaskTimeOnSkillSet;
                if (!taskType.MaxUserRuntime.HasValue) taskType.MaxUserRuntime = Settings.Default.DefaultTaskTimeOnUser;
                if (String.IsNullOrEmpty(taskType.SlaName)) taskType.SlaName = "Default";
            }
            provider.Save();
        }

        public void UpdateTaskType(string externalSystemId, TaskType ttype)
        {
            var provider = TicketToolContext.Current.DataProvider;
            var dbTType = provider.GetTaskTypes(externalSystemId, new[] {ttype.Name}).Single();
            if (dbTType == null) return;
            dbTType.MaxSkillSetRunTime = ttype.MaxSkillSetTime;
            dbTType.MaxUserRuntime = ttype.MaxUserSetTime;
            provider.Save();
        }

        public TaskResponse Callback(string externalSystemId, TicketTask ticketTask)
        {
            using (var provider = new DataProvider())
            {
                var reqObjectFields = new Dictionary<string, object>();
                if (ticketTask.RequiredFields != null && ticketTask.RequiredFields.Count > 0)
                {
                    reqObjectFields = ticketTask.RequiredFields.ToDictionary(f => f.Name, f => f.Value);
                }
                var parameters = new object[] {ticketTask.TicketId, ticketTask.Outcome, reqObjectFields, ticketTask.Performer};

                return SoapServiceBuilder.GetProxy(provider.GetExternalSystem(externalSystemId).WsdlUrl)
                                         .Invoke(GetCallbackName(ticketTask), parameters)
                                         .CopyObjectPropertiesTo<TaskResponse>();
            }
        }

        public string GetCallbackName(TicketTask task)
        {
            return String.Concat(task.Type, "Continue");
        }
    }
}