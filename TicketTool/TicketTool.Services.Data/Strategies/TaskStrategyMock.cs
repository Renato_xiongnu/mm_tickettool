﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TicketTool.Dal;
using TicketTool.Data.Common;
using TicketTool.Data.Contracts;
using TicketTool.Data.Contracts.Common;
using TicketTool.Data.Contracts.Operations;
using TicketTool.Data.Contracts.Tasks;
using TicketTool.Data.Strategies.Contracts;

namespace TicketTool.Data.Strategies
{
    public class TaskStrategyMock : ITaskStrategy
    {
        private static readonly List<Tuple<string, TicketTask>> _taskIds = new List<Tuple<string, TicketTask>>();  
        private static readonly object SyncRoot = new object();

        public string CreateTask(TicketTask task)
        {
            var taskId = Guid.NewGuid().ToString();
            lock (SyncRoot)
            {
                _taskIds.Add(new Tuple<string, TicketTask>(taskId, task));
            }

            return taskId;
        }

        public void CancelTask(long taskId)
        {
            throw new NotImplementedException();
        }

        public OperationResult CompleteTask(string taskId, string outcome, ICollection<RequiredField> requiredFields)
        {
            Tuple<string, TicketTask> tuple;

            lock (SyncRoot)
            {
                tuple = _taskIds.FirstOrDefault(x => x.Item1 == taskId);
                if (tuple == null)
                    return new OperationResult {HasError = true, MessageText = "Task not found"};
            }
            tuple.Item2.Outcome = outcome;

            Task.Factory.StartNew(() =>
                {
                    Thread.Sleep(TimeSpan.FromSeconds(10));
                    using (var provider = new DataProvider())
                    {
                        var reqObjectFields = new Dictionary<string, object>();
                        if (tuple.Item2.RequiredFields != null && tuple.Item2.RequiredFields.Count > 0)
                        {
                            reqObjectFields = tuple.Item2.RequiredFields.ToDictionary(f => f.Name, f => f.Value);
                        }
                        var parameters = new object[] { tuple.Item2.TicketId, tuple.Item2.Outcome, reqObjectFields, tuple.Item2.Performer };

                        return SoapServiceBuilder.GetProxy(provider.GetExternalSystem("IZztOrderProcessingService").WsdlUrl)
                                                 .Invoke(String.Concat(tuple.Item2.Type, "Continue"), parameters)
                                                 .CopyObjectPropertiesTo<TaskResponse>();
                    }
                });
            
            return new OperationResult { HasError = false, MessageText = string.Empty };
        }

        public OperationResult PostponeTask(string taskId, TimeSpan? time)
        {
            throw new NotImplementedException();
        }

        public TicketTask ReserveAndGetNextTask(bool skipAssigned)
        {
            throw new NotImplementedException();
        }

        public ICollection<TaskListItem> GetMyTasks(Parameter parameter)
        {
            throw new NotImplementedException();
        }

        public ICollection<TaskListItem> GetTasks(SkillSet skillSet, Parameter parameter)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TicketTask> GetTasks(string workitemId)
        {
            throw new NotImplementedException();
        }

        public void ProcessTaskQueue()
        {
            throw new NotImplementedException();
        }

        public void ProcessErroredQueue()
        {
            throw new NotImplementedException();
        }

        public void SendTasks()
        {
            throw new NotImplementedException();
        }

        public void CheckAndUpdateTasks()
        {
            throw new NotImplementedException();
        }

        public TicketTask GetTask(string taskId)
        {
            lock (SyncRoot)
            {
                if (!_taskIds.Exists(x => x.Item1 == taskId))
                    return null;
            }
            return new TicketTask {State = TaskState.InProgress};
        }

        public ICollection<TaskListItem> GetMyTasksInProgress()
        {
            throw new NotImplementedException();
        }

        public ICollection<TaskListItem> GetAllTasksInProgress(string[] parameters, string[] skillSets)
        {
            throw new NotImplementedException();
        }

        public bool CanProcessed(string taskId, string userName)
        {
            throw new NotImplementedException();
        }

        public TicketTask AssignFreeTaskForMe(string taskId)
        {
            throw new NotImplementedException();
        }

        public TicketTask ForceAssignFreeTaskForMe(string taskId)
        {
            throw new NotImplementedException();
        }

        public ICollection<TicketTask> ReserveAndGetNextTasks()
        {
            throw new NotImplementedException();
        }

        public TicketTask ReserveAndGetNextTaskSkipAssigned()
        {
            throw new NotImplementedException();
        }
    }
}