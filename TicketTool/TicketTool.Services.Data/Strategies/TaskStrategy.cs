﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Transactions;
using TicketTool.Dal.Model;
using TicketTool.Data.Common;
using TicketTool.Data.Context;
using TicketTool.Data.Contracts;
using TicketTool.Data.Contracts.Common;
using TicketTool.Data.Contracts.Operations;
using TicketTool.Data.Properties;
using TicketTool.Data.Strategies.Contracts;
using log4net;
using Encoder = TicketTool.Cryptography.Encoder;
using Parameter = TicketTool.Data.Contracts.Common.Parameter;
using SkillSet = TicketTool.Data.Contracts.SkillSet;
using TicketTask = TicketTool.Data.Contracts.Tasks.TicketTask;
using User = TicketTool.Dal.Model.User;

namespace TicketTool.Data.Strategies
{
    internal class TaskStrategy : TicketToolStrategyBase, ITaskStrategy
    {
        internal TaskStrategy(ITicketService ticketService)
            : base(ticketService)
        {
        }

        public string CreateTask(TicketTask task)
        {
            return CreateTask(task,
                string.IsNullOrEmpty(task.Parameter)
                    ? null
                    : new Parameter { ParameterName = task.Parameter });
        }

        private string CreateTask(TicketTask task, Parameter parameter)
        {
            var provider = TicketToolContext.Current.DataProvider;

            var taskOld = provider.GetTasksByTicket(task.TicketId).Where(t =>
                (task.Parameter == t.Parameter || !t.TaskType.SkillSet.IsParameterizable) &&
                task.Type == t.TaskType.Name &&
                (t.TaskCompletionQueue.CompletionStatus < (int) TaskCompletionStatus.Finished ||
                 t.TaskStatus < (int) TaskStatus.Finished)).ToList();

            if (taskOld.Any())
            {
                foreach (var ticketTask in taskOld)
                {
                    using (var trans = new TransactionScope())
                    {
                        CancelTask(ticketTask.TaskId);
                        if (ticketTask.TaskCompletionQueue != null)
                            CancelTaskCompletion(ticketTask.TaskCompletionQueue, "New task with same type was created");

                        trans.Complete();
                    }
                    Logger.WarnFormat("Task {0} for ticket {1} was created, but previous was canceled ({2})",
                        task.Type,
                        task.TicketId,
                        ticketTask.TaskId);
                }
                //Case, when WF try to create same task, when previous not completed
            }

            using (var trans = new TransactionScope())
            {
                var currentDate = DateTime.Now.ToUniversalTime();
                var isHtmlRegEx = new Regex(@"<(.|\n)*?>");
                var modelTask = new Dal.Model.TicketTask
                {
                    Name = task.Name,
                    Description =
                        (string.IsNullOrWhiteSpace(task.Description) || isHtmlRegEx.IsMatch(task.Description))
                            ? task.Description
                            : task.Description.Replace(Environment.NewLine, @"<br/>"),
                    CreateDate = currentDate,
                    UpdateDate = currentDate,
                    Outcomes = new Dal.Model.Outcome {Data = ContractExtensions.SeralizeOtcomes(task.Outcomes)},
                    Escalated = false,
                    Parameter = parameter != null ? EnsureParameter(parameter).ParameterName : null
                };

                var fields = task.RequiredFields.Select(t =>
                    new TaskField
                    {
                        FieldType = new FieldType
                        {
                            Name = t.Name,
                            Type = t.Type,
                            DefaultValue = t.DefaultValue == null ? null : t.DefaultValue.ToString(),
                            PredefinedValuesList = t.GetPredefinedValuesList()
                        }
                    }).ToList();

                var resultTask = provider.PrepareTask(task.TicketId, task.Type, modelTask, fields);
                this.TicketService.TaskOperations.CreateTask(resultTask);

                provider.Save();
                trans.Complete();

                return resultTask.TaskId.ToString(CultureInfo.InvariantCulture);
            }
        }

        private static Parameter EnsureParameter(Parameter parameter)
        {
            var provider = TicketToolContext.Current.DataProvider;
            Dal.Model.Parameter result;
            try
            {
                result = provider.GetParameter(parameter.ParameterName);
            }
            catch (ObjectNotFoundException)
            {
                result = provider.CreateParameter(
                    new Dal.Model.Parameter
                    {
                        ParameterName = parameter.ParameterName,
                        Name = string.IsNullOrEmpty(parameter.Name)
                            ? parameter.ParameterName
                            : parameter.Name,
                        Description = string.IsNullOrEmpty(parameter.Description)
                            ? parameter.ParameterName
                            : parameter.Description,
                    });
            }
            return result.ToContract();
        }

        public OperationResult CompleteTask(string taskId, string outcome, ICollection<RequiredField> requiredFields)
        {
            var provider = TicketToolContext.Current.DataProvider;
            var user = provider.GetUser(TicketToolContext.Current.Credentials.Name);
            var task = provider.GetTask(Convert.ToInt64(taskId), includeCompletionQueue: true);

            var canComplete = CanComplete(user, task, requiredFields);
            if (canComplete.HasError) return canComplete;

            try
            {
                return CompleteTaskInternal(task, user, outcome, requiredFields);
            }
            catch (Exception ex)
            {
                var fullMessage = new StringBuilder(ex.Message, 1024);
                foreach (var innerErrors in ex.GetInnerExceptionsText())
                {
                    fullMessage.AppendLine(innerErrors + Environment.NewLine);
                }
                return new OperationResult { HasError = true, MessageText = fullMessage.ToString() };
            }
        }

        public ICollection<TaskListItem> GetMyTasks(Parameter parameter)
        {
            string userName = TicketToolContext.Current.Credentials.Name;
            return TicketToolContext.Current.DataProvider.GetTasks(userName, parameter.ParameterName)
                                    .ToList()
                                    .Select(t => new TaskListItem
                                    {
                                        Name = t.Name,
                                        CreationDate = t.CreateDate,
                                        MaxRunTime = t.TaskType.MaxSkillSetRunTime,
                                        Url = GenerateTaskUrl(t, userName),
                                        WorkitemId = t.Ticket.WorkItemId,
                                        IsAssignedOnMe = t.CurrentAssignee != null && t.CurrentAssignee.Name == userName
                                    })
                                    .ToList();
        }

        public ICollection<TaskListItem> GetTasks(SkillSet skillSet, Parameter parameter)
        {
            var provider = TicketToolContext.Current.DataProvider;
            var user = provider.GetUsersByParameter(new Dal.Model.Parameter { ParameterName = parameter.ParameterName }, skillSet.Name).FirstOrDefault();
            if (user != null)
            {
                return provider.GetTasks(new[] { skillSet.Name }, parameter.ParameterName)
                               .ToList()
                               .Select(t => new TaskListItem
                               {
                                   Name = t.Name,
                                   CreationDate = t.CreateDate,
                                   MaxRunTime = t.TaskType.MaxSkillSetRunTime,
                                   Url = GenerateTaskUrl(t, user.Name),
                                   WorkitemId = t.Ticket.WorkItemId,
                                   IsAssignedOnMe = t.CurrentAssignee != null && t.CurrentAssignee == user
                               })
                               .ToList();
            }
            return new List<TaskListItem>();
        }

        public IEnumerable<TicketTask> GetTasks(string workitemId)
        {
            return TicketToolContext.Current.DataProvider
                .GetTasks(workitemId)
                .ToArray()
                .Select(t => t.CreateFromModel())
                .ToArray();
        }

        public TicketTask ReserveAndGetNextTask(bool skipAssigned)
        {
            var provider = TicketToolContext.Current.DataProvider;
            var userName = TicketToolContext.Current.Credentials.Name;
            var user = provider.GetUser(userName, true, withClusters: true);
            if (user.SkillSets.Count == 0)
                throw new InvalidOperationException(string.Format("User {0} doesn't login in any skillset", userName));

            foreach (var skillSet in user.SkillSets)
            {
                Dal.Model.TicketTask userTask;
                using (var trans = new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                {
                    userTask = provider.GetTaskAndUpdateForUser(user, skillSet, skipAssigned);

                    if (userTask == null)
                        continue;

                    if (!userTask.IsOldTask)
                        this.TicketService.TaskOperations.InsertAssignedToUserOperation(userTask);

                    provider.Save();
                    trans.Complete();
                }
                return userTask.CreateFromModel();
            }

            return null;
        }

        public OperationResult PostponeTask(string taskId, TimeSpan? time)
        {
            var provider = TicketToolContext.Current.DataProvider;
            var task = provider.GetTask(Convert.ToInt64(taskId));
            var user = provider.GetUser(TicketToolContext.Current.Credentials.Name);
            var check = this.CheckTaskAssignToUser(task, user);

            if (check.HasError)
            {
                provider.Save();
                return check;
            }
            this.RemoveUser(task, time);
            provider.Save();

            return new OperationResult();
        }

        public void CancelTask(long taskId)
        {
            var provider = TicketToolContext.Current.DataProvider;
            var task = provider.GetTask(taskId);

            if (task == null) return;
            this.TicketService.TaskOperations.CancelTask(task);
            if (task.TaskCompletionQueue != null)
            {
                task.TaskCompletionQueue.FinishDate = DateTime.UtcNow;
                task.TaskCompletionQueue.CompletionStatus = (int)TaskCompletionStatus.Canceled;
            }
            provider.Save();
        }

        private OperationResult EnqueueExternalCompletion(Dal.Model.TicketTask task, ICollection<RequiredField> requiredFields)
        {
            var provider = TicketToolContext.Current.DataProvider;
            var ticketTask = new TicketTask
            {
                TicketId = task.Ticket.TicketId,
                Name = task.Name,
                Type = task.TaskType.Name,
                TaskId = task.TaskId.ToString(CultureInfo.InvariantCulture),
                Outcome = task.CurrentOutcome,
                RequiredFields = requiredFields,
                Performer = task.Performer.Name
            };

            if (task.TaskCompletionQueue == null)
            {
                var taskQueue = provider.PrepairTaskCompletionQueue(task);
                taskQueue.CompletionStatus = (int)TaskCompletionStatus.Created;
                taskQueue.ExternalSystem = task.TaskType.ExternalSystem;
                taskQueue.ExternalSystemOperationName = this.TicketService.ExternalSystems.GetCallbackName(ticketTask);
                taskQueue.SerializedTask = DataContractJsonHelper<TicketTask>.SerializeData(ticketTask);
                taskQueue.QueueLogs = new List<TaskCompletionQueueLog>
                    {
                        provider.PrepairTaskCompletionQueueLog(taskQueue, TaskCompletionStatus.None, TaskCompletionStatus.Created)
                    };

                task.TaskCompletionQueue = taskQueue;
                return new OperationResult();
            }

            return new OperationResult
            {
                HasError = true,
                MessageText = String.Format("Complete event for Task {0} has been already running", task.TaskId)
            };
        }

        private OperationResult CompleteTaskInternal(Dal.Model.TicketTask task, User user, string outcome, ICollection<RequiredField> requiredFields)
        {
            var provider = TicketToolContext.Current.DataProvider;

            task.CurrentOutcome = outcome;
            task.Comment = String.Empty;
            if (task.CurrentAssignee == null)
            {
                task.CurrentAssignee = user;
                this.TicketService.TaskOperations.InsertAssignedToUserOperation(task);
            }
            task.Performer = user;
            provider.Save();

            OperationResult result;
            using (var trans = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            {
                result = EnqueueExternalCompletion(task, requiredFields);
                if (result.HasError)
                {
                    this.TicketService.TaskOperations.InsertErrorOperation(task, result.MessageText);
                }
                else
                {
                    this.TicketService.TaskOperations.CompleteTask(task);
                }

                provider.Save();
                trans.Complete();
            }

            UpdateUserLog(result, user, task);

            return result;
        }

        private static void UpdateUserLog(OperationResult result, User user, Dal.Model.TicketTask task)
        {
            var provider = TicketToolContext.Current.DataProvider;
            if (result.HasError) return;
            var userAction = provider.CreateUserAction(user, task.CurrentSkillSet);
            userAction.Task = task;
            userAction.ActionTime = DateTime.Now.ToUniversalTime();
            userAction.ActionType = ActionType.Complete;
            provider.Save();
        }

        private static OperationResult CanComplete(User user, Dal.Model.TicketTask task, ICollection<RequiredField> requiredFields)
        {
            if (task == null) return GetResultWithError(Resources.Ex_TaskNotExists);
            if (user == null) return GetResultWithError(Resources.Ex_UserNotExists);

            if (task.TaskStatus == (int)TaskStatus.Finished) return GetResultWithError(Resources.Ex_TaskAlreadyCompleted);
            if (task.TaskStatus == (int)TaskStatus.Cancel) return GetResultWithError(Resources.Ex_TaskCanceled);
            if (!IsAdmin(user))
            {
                if (task.CurrentAssignee != null)
                {
                    if (!task.CurrentAssignee.Name.Equals(user.Name, StringComparison.InvariantCultureIgnoreCase))
                    {
                        return GetResultWithError(Resources.Ex_AnotherUserAlreadyAssigned);
                    }
                }
                else if (user.SkillSets.All(s => s.SkillSetId != task.TaskType.SkillSet.SkillSetId))
                {
                    return GetResultWithError(String.Format(Resources.Ex_UserNotHavePermissions, user.Name, task.TaskType.Name));
                }
            }

            var errors = new List<string>();
            foreach (var taskField in task.TaskFields)
            {
                var reqField = requiredFields
                    .SingleOrDefault(t => taskField.FieldType.Type == t.Type && taskField.FieldType.Name == t.Name);

                if (reqField == null || reqField.Value == null)
                    errors.Add(string.Format("Couldn't find field {0}", taskField.FieldType.Name));
                else
                    taskField.Value = reqField.Value.ToString();
            }
            return errors.Count > 0
                       ? new OperationResult { HasError = true, MessageText = string.Join("\n", errors) }
                       : new OperationResult();
        }

        private static bool IsAdmin(User user)
        {
            return user.Roles.Select(r => r.Name).ToArray().Contains("Admin");
        }

        private static bool IsSuperviser(User user)
        {
            return user.Roles.Select(r => r.Name).ToArray().Contains("Superviser");
        }

        private static bool IsAgent(User user)
        {
            return user.Roles.Select(r => r.Name).ToArray().Contains("Agent");
        }

        private OperationResult CheckTaskAssignToUser(Dal.Model.TicketTask task, User user)
        {
            var result = new OperationResult();

            if (task == null)
            {
                result.HasError = true;
                result.MessageText = string.Format("Task doesn't exists");
                return result;
            }

            if (user == null)
            {
                result.HasError = true;
                result.MessageText = string.Format("Can't check user for task {0}, because user doesn't exists", task.TaskId);
                return result;
            }

            if (task.CurrentAssignee == null || task.CurrentAssignee != user)
            {
                var errorMessage = string.Format("User {0} doesn't assign to task {1}", user.Name, task.Name);
                this.TicketService.TaskOperations.InsertErrorOperation(task, errorMessage);
                result.HasError = true;
                result.MessageText = errorMessage;
            }

            return result;
        }

        private static OperationResult GetResultWithError(string message)
        {
            return new OperationResult { HasError = true, MessageText = message };
        }

        public void SendTasks()
        {
            var log = LogManager.GetLogger("SendTasks");
            var provider = TicketToolContext.Current.DataProvider;
            var skillSets = provider.GetSkillSets(withRoles: true).Where(t => t.DeliveryTaskByEmail).ToList();

            foreach (var skillSet in skillSets)
            {
                var tasks = provider.GetAvalibleTaskId(skillSet).ToList()
                                    .Where(t => !t.InformedUserId.HasValue);

                foreach (var key in tasks)
                {
                    try
                    {
                        var task = provider.GetTask(key.TaskId);
                        var parameter = new Dal.Model.Parameter { ParameterName = key.Parameter };
                        using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.RepeatableRead }))
                        {
                            provider.GetUsersByParameter(parameter, skillSet.Name).ToList()
                                    .ForEach(u => SendTask(task, u, log));
                            provider.Save();
                            scope.Complete();
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);
                    }
                }
            }
            provider.Save();
        }

        private static void SendTask(Dal.Model.TicketTask task, User user, ILog log)
        {
            try
            {
                var url = GenerateTaskUrl(task, user.Name);
                var body = task.Description.Replace(Settings.Default.TaskIdMarker, url);
                new SmtpClient().Send(new MailMessage(Settings.Default.FromEmail, user.Email, task.Name, body) { IsBodyHtml = true });
                task.InformedUser = user;
                log.InfoFormat("Task {0} sent to the user {1} ", task.TaskId, user.Name);
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }

        public void CheckAndUpdateTasks()
        {
            var log = LogManager.GetLogger("CheckAndUpdateTasks");
            FindUnboundedTasks(log);
            CheckAndUpdateUserLoginTime(log);
            UpdateFrozenTask();
            CancelForWrongTask(log);
        }

        public TicketTask GetTask(string taskId)
        {
            var task = TicketToolContext.Current.DataProvider.GetTask(Int64.Parse(taskId));
            return task != null ? task.CreateFromModel() : null;
        }

        public ICollection<TaskListItem> GetMyTasksInProgress()
        {
            var provider = TicketToolContext.Current.DataProvider;
            var user = provider.GetUser(TicketToolContext.Current.Credentials.Name, true);
            var userParameters = user.Parameters.Select(p => p.ParameterName).ToArray();
            var skillSets = user.SkillSets.Select(s => s.SkillSetId).ToArray();
            return provider.GetAllTasks(true, true)
                                    .Where(t => t.TaskStatus < (int)TaskStatus.Finished)
                                    .Where(t => skillSets.Contains(t.TaskType.SkillSet.SkillSetId) || (t.Escalated && skillSets.Contains(t.TaskType.SlaSkillSet.SkillSetId)))
                                    .Where(t => t.TaskCompletionQueue == null)
                                    .Where(t => t.Parameter == null || userParameters.Contains(t.Parameter))
                                    .ToList()
                                    .Select(t => t.ToContract(user.Name))
                                    .ToList();
        }

        public ICollection<TaskListItem> GetAllTasksInProgress(string[] parameters, string[] skillSets)
        {
            var provider = TicketToolContext.Current.DataProvider;
            var user = provider.GetUser(TicketToolContext.Current.Credentials.Name, true);
            var availibleSkillsets = user.AvailableSkillSets
                .Where(s => s.DeliveryTaskByEmail)
                .Select(s => new { s.SkillSetId, s.Name }).ToArray();
            if (skillSets != null)
            {
                availibleSkillsets = availibleSkillsets.Where(s => skillSets.Contains(s.Name)).ToArray();
            }
            var skillSetIds = availibleSkillsets.Select(s => s.SkillSetId).ToArray();
            var query = provider.GetAllTasks(true, true)
                .Where(t => t.TaskStatus < (int)TaskStatus.Finished)
                .Where(t => t.TaskCompletionQueue == null)
                .Where(t => skillSetIds.Contains(t.TaskType.SkillSet.SkillSetId) || (t.Escalated && skillSetIds.Contains(t.TaskType.SlaSkillSet.SkillSetId)));
            if (parameters != null)
            {
                query = query.Where(t => parameters.Contains(t.Parameter));
            }
            return query
                .ToList()
                .Select(t => t.ToContract(user.Name))
                .ToList();
        }

        public bool CanProcessed(string taskId, string userName)
        {
            return CanProcessed(Int64.Parse(taskId), userName);
        }

        /// <summary>
        /// Assigns the free task for me.
        /// </summary>
        /// <param name="taskId">The task identifier.</param>
        /// <returns></returns>
        public TicketTask AssignFreeTaskForMe(string taskId)
        {
            return AssignFreeTaskForMe(taskId, false);
        }

        /// <summary>
        /// Assigns the free task for me.
        /// </summary>
        /// <param name="taskId">The task identifier.</param>
        /// <returns></returns>
        public TicketTask ForceAssignFreeTaskForMe(string taskId)
        {
            return AssignFreeTaskForMe(taskId, true);
        }

        /// <summary>
        /// Assigns the free task for me.
        /// </summary>
        /// <param name="taskId">The task identifier.</param>
        /// <param name="force">if set to <c>true</c> [force].</param>
        /// <returns></returns>
        /// <exception cref="System.InvalidOperationException">
        /// Задача уже назначена на другого пользователя
        /// or
        /// </exception>
        /// <exception cref="System.Data.ObjectNotFoundException"></exception>
        /// <exception cref="System.ServiceModel.AddressAccessDeniedException"></exception>
        private TicketTask AssignFreeTaskForMe(string taskId, bool force)
        {
            var taskKey = Convert.ToInt64(taskId);
            var provider = TicketToolContext.Current.DataProvider;
            var userName = TicketToolContext.Current.Credentials.Name;

            var user = provider.GetUser(userName, true);
            if (!IsAdmin(user) && !IsSuperviser(user) && user.SkillSets.Count == 0)
                throw new InvalidOperationException(string.Format("Пользователь {0} не залогинен ни в один скилсет", userName));

            var task = provider.GetTask(taskKey);

            if (task == null)
                throw new ObjectNotFoundException(string.Format("Задача {0} не найдена", taskKey));
            Dal.Model.TicketTask userTask = null;
            if (!IsAdmin(user) && !IsSuperviser(user))
            {
                if (!user.SkillSets.Contains(task.CurrentSkillSet))
                    throw new InvalidOperationException(string.Format("Пользователь {0} не залогинен в скилсет {1}", userName, task.CurrentSkillSet.Name));
            }

            if (!force && task.CurrentAssignee != null && task.CurrentAssignee.UserId != user.UserId)
            {
                throw new InvalidOperationException("Задача уже назначена на другого пользователя");
            }

            if (task.TaskStatus == (int)TaskStatus.Finished)
                throw new InvalidOperationException(string.Format("Задача уже завершена с исходом \"{0}\"", task.CurrentOutcome));

            userTask = provider.AssignFreeTaskForUser(task, user, force);
            if (userTask == null)
                return null;

            if (!userTask.IsOldTask)
                TicketService.TaskOperations.InsertAssignedToUserOperation(userTask);
            provider.Save();

            return task.CreateFromModel();
        }

        public bool CanProcessed(long taskId, string userName)
        {
            var provider = TicketToolContext.Current.DataProvider;
            var user = provider.GetUser(userName, true);
            var skillSets = IsAdmin(user) || IsSuperviser(user)
                ? user.AvailableSkillSets.Select(s => s.SkillSetId).ToArray()
                : user.SkillSets.Select(s => s.SkillSetId).ToArray();
            // ReSharper disable ReplaceWithSingleCallToAny
            return provider.GetAllTasks(true, true)
                                    .Where(t => t.TaskId == taskId)
                                    .Where(t => t.TaskStatus < (int)TaskStatus.Finished)
                                    .Where(t => skillSets.Contains(t.TaskType.SkillSet.SkillSetId) || (t.Escalated && skillSets.Contains(t.TaskType.SlaSkillSet.SkillSetId)))
                                    .Where(t => t.TaskCompletionQueue == null)
                                    .Any();
            // ReSharper restore ReplaceWithSingleCallToAny
        }

        private void RemoveTaskFromUser(Dal.Model.TicketTask task)
        {
            var provider = TicketToolContext.Current.DataProvider;
            var userAction = provider.CreateUserAction(task.CurrentAssignee, task.CurrentSkillSet);
            userAction.ActionTime = DateTime.Now.ToUniversalTime();
            userAction.ActionType = ActionType.Postpone;
            userAction.Task = task;
            RemoveUser(task, TimeSpan.FromSeconds(Settings.Default.TaskFrozeTimeSec));
        }

        private void CancelForWrongTask(ILog log)
        {
            log.Info("Searching for wrong task is started:");

            var provider = TicketToolContext.Current.DataProvider;
            var currentDate = DateTime.Now.ToUniversalTime().AddMinutes(-10);

            var tasks = provider.GetAllTasks(true)
                                .Where(t => t.Ticket.CloseDate != null && t.Ticket.CloseDate < currentDate && t.TaskStatus < (int)TaskStatus.Finished)
                                .ToArray();

            if (tasks.Length > 0)
            {
                log.Warn(string.Format("{0} not canceled tasks detected", tasks.Length));
                foreach (var task in tasks)
                {
                    this.TicketService.TaskOperations.CancelTask(task);
                    log.Warn(string.Format("Cancel task {0}", task.TaskId));
                }
                provider.Save();
            }
            log.Info("Searching for wrong task is finished");
        }

        private void FindUnboundedTasks(ILog log)
        {
            var provider = TicketToolContext.Current.DataProvider;
            var currentDate = DateTime.Now.ToUniversalTime();
            var tasksOnUsers = provider.GetUserAbandonedTasksOnDate(currentDate).ToList();
            foreach (var taskOnUser in tasksOnUsers)
            {
                try
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required,
                                                            new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        RemoveTaskFromUser(taskOnUser);
                        provider.Save();
                        scope.Complete();
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                }
            }

            var taskOnSkillsets = provider.GetSkillSetAbandonedTasksOnDate(currentDate).ToList();
            foreach (var task in taskOnSkillsets)
            {
                try
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required,
                                                            new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        if (task.TaskStatus == (int)TaskStatus.AssignedToUser)
                            RemoveTaskFromUser(task);

                        var newSkillSet = task.TaskType.SlaSkillSet;
                        if (task.TaskType.SlaSkillSet != null)
                        {
                            this.TicketService.TaskOperations.InsertSkillSetAbandonedOperation(task, newSkillSet);
                            task.CurrentSkillSet = newSkillSet;
                            task.Escalated = true;
                            task.CurrentAssignee = null;
                            this.TicketService.TaskOperations.InsertAssignedToSkillSetOperation(task);
                        }
                        provider.Save();
                        scope.Complete();
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                }
            }
            provider.Save();
        }

        private static void UpdateFrozenTask()
        {
            var provider = TicketToolContext.Current.DataProvider;
            var currentDate = DateTime.Now.ToUniversalTime();
            provider.GetAllTasks()
                    .Where(t => t.ReturnToQueueAfterDate.HasValue && t.ReturnToQueueAfterDate < currentDate)
                    .ToList()
                    .ForEach(t => { t.ReturnToQueueAfterDate = null; });
            provider.Save();
        }

        private void CheckAndUpdateUserLoginTime(ILog log)
        {
            var provider = TicketToolContext.Current.DataProvider;
            var currentDate = DateTime.Now.ToUniversalTime();

            var userForLogout = new List<User>();

            var users = provider.GetAllUsers(false, true).ToArray() // TODO need refactoring !!!
                                .Where(u => u.Roles.Select(t => t.Name).Contains("Agent")).ToList();

            //int abandonUsersNum = 0;
            foreach (var user in users)
            {
                var userLogs = provider.UserLogFindOpenedRecords(user).ToList();
                if (userLogs.Count == 0)
                    continue;
                if(user.IsSystemUser)
                    continue;

                if (currentDate - user.LastPingDateTime > Settings.Default.MaxUserIdleTime)
                    userForLogout.Add(user);
                else
                {
                    var abandonQty = userLogs.Sum(t => t.AbandonedQty);
                    if (abandonQty > Settings.Default.MaxUserAbandonQty)
                        userForLogout.Add(user);
                }
            }

            foreach (var user in userForLogout)
            {
                try
                {
                    using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                    {
                        this.TicketService.Users.Abandon(user.Name);
                        provider.Save();
                        scope.Complete();
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                }
            }
            provider.Save();
        }

        public void ProcessErroredQueue()
        {
            var minDate = DateTime.Now.Subtract(Settings.Default.CompletionQueueLifeTime);
            var query = TicketToolContext.Current.DataProvider
                .GetAllTaskCompletionQueue()
                .Where(t => t.QueueLogs.All(l => l.HasError || l.FromStatus == (int)TaskCompletionStatus.None))
                .Where(t => t.QueueLogs.Count() > Settings.Default.MaxNumberOfAttempts)
                .Where(t => t.CreateDate > minDate)
                .OrderBy(t => t.CreateDate)
                .Take(Settings.Default.MaxCompletionQueueInSinglePass);
            var queue = query.ToList();

            foreach (var taskCompletion in queue)
            {
                CompleteTaskInExternalSystem(taskCompletion);
                Thread.Sleep(Settings.Default.DelayBeforeProcessCall);
            }
        }

        public void ProcessTaskQueue()
        {
            var provider = TicketToolContext.Current.DataProvider;
            var finishingQueue = provider.GetAllTaskCompletionQueue()
                                         .Where(t => t.CompletionStatus < (int)TaskCompletionStatus.Finished)
                                         .OrderBy(t => t.CreateDate)
                                         .Take(Settings.Default.MaxCompletionQueueInSinglePass)
                                         .ToList();

            foreach (var taskCompletion in finishingQueue)
            {
                Thread.Sleep(Settings.Default.DelayBeforeProcessCall);

                if (taskCompletion.QueueLogs.Count(t => t.HasError) < Settings.Default.MaxNumberOfAttempts)
                {
                    try
                    {
                        CompleteTaskInExternalSystem(taskCompletion);
                        LogManager.GetLogger("ProcessTaskQueue").Info(taskCompletion.TaskQueueId);
                    }
                    catch (Exception ex)
                    {
                        LogManager.GetLogger("ProcessTaskQueue").Error(new Exception(taskCompletion.TaskQueueId.ToString(), ex));
                    }
                }
                else
                {
                    CancelTaskCompletion(taskCompletion);
                }
            }
        }

        private static void AddErrorFinishTaskNotification(TaskCompletionQueue taskCompletion)
        {
            var provider = TicketToolContext.Current.DataProvider;
            var eventLogGroup = provider.GetDefaultNotificationLogGroup();
            var task = DataContractJsonHelper<TicketTask>.DeserializeData(taskCompletion.SerializedTask);
            var ticket = provider.GetTicket(task.TicketId);

            try
            {
                var eventRecord = provider.PrepairEventLogRecord(NotificationRecType.Error, eventLogGroup);
                eventRecord.InvocationMarker = taskCompletion.InvocationMarker;
                eventRecord.RefId = taskCompletion.TaskQueueId.ToString(CultureInfo.InvariantCulture);
                var type = taskCompletion.GetType();
                eventRecord.SourceName = type.BaseType != null ? type.BaseType.Name : type.Name;
                eventRecord.Text = ticket == null ? string.Format("Ticket for TicketId={0} not exist", task.TicketId) :
                    string.Format("Ticket: {0}, WorkItem: {1}, reached the maximum number of attempts to complete a task (id: {2}, name: {3})",
                                                 ticket.TicketId, ticket.WorkItemId, task.TaskId, task.Name);
            }
            catch (Exception ex)
            {
                LogManager.GetLogger("ProcessTaskQueue").Error(ex);
            }

            provider.Save();
        }

        private static void CancelTaskCompletion(TaskCompletionQueue taskCompletion, string message = null)
        {
            var invocationMarker = Guid.NewGuid().ToString();
            var provider = TicketToolContext.Current.DataProvider;
            taskCompletion.UpdateDate = DateTime.Now.ToUniversalTime();
            taskCompletion.InvocationMarker = invocationMarker;
            taskCompletion.QueueLogs.Add(PrepairQueueLog(taskCompletion, invocationMarker, false,
                message ?? "reached the maximum number of attempts to complete a task"));
            provider.Save();

            AddErrorFinishTaskNotification(taskCompletion);
        }

        private void CompleteTaskInExternalSystem(TaskCompletionQueue taskCompletion)
        {
            var provider = TicketToolContext.Current.DataProvider;

            try
            {
                var invocationMarker = Guid.NewGuid().ToString();
                var ticketTask = DataContractJsonHelper<TicketTask>.DeserializeData(taskCompletion.SerializedTask);
                taskCompletion.UpdateDate = DateTime.Now.ToUniversalTime();
                taskCompletion.InvocationMarker = invocationMarker;

                var response = this.TicketService.ExternalSystems.Callback(taskCompletion.ExternalSystem.ExternalSystemId, ticketTask);

                taskCompletion.QueueLogs.Add(PrepairQueueLog(taskCompletion, invocationMarker, response.CompleteSuccessful, response.Message));
            }
            catch (Exception ex)
            {
                var queueLog = provider.PrepairTaskCompletionQueueLog(taskCompletion);
                queueLog.HasError = true;
                queueLog.Message = ex.GetExceptionFullInfoText();
                if (taskCompletion.QueueLogs == null)
                    taskCompletion.QueueLogs = new List<TaskCompletionQueueLog>();
                taskCompletion.QueueLogs.Add(queueLog);

                LogManager.GetLogger("ProcessTaskQueue").Error(ex);
            }

            provider.Save();
        }

        private static TaskCompletionQueueLog PrepairQueueLog(TaskCompletionQueue taskCompletion, string invocationMarker, bool completeSuccessful, string errorMessage)
        {
            var provider = TicketToolContext.Current.DataProvider;
            var log = provider.PrepairTaskCompletionQueueLog(taskCompletion);
            log.InvocationMarker = invocationMarker;
            if (!completeSuccessful)
            {
                log.HasError = true;
                log.Message = errorMessage;
                taskCompletion.CompletionStatus = (int)TaskCompletionStatus.Canceled;
            }
            else
            {
                taskCompletion.CompletionStatus = (int)TaskCompletionStatus.Finished;
            }
            taskCompletion.FinishDate = DateTime.Now.ToUniversalTime();
            log.ToStatus = taskCompletion.CompletionStatus;
            return log;
        }

        internal static string GenerateTaskUrl(Dal.Model.TicketTask task, string userName)
        {
            var taskPageUrl = task.TaskType.TaskPageUrl;
            if (String.IsNullOrEmpty(taskPageUrl)) return String.Empty;

            var taskId = task.TaskId.ToString(CultureInfo.InvariantCulture);
            string queryString = String.Format("TaskId={0}&Key={1}&m={2}&u={3}", taskId, Encoder.Encode(taskId), Encoder.Encode(userName), userName);
            var urlParts = taskPageUrl.Split('?');
            taskPageUrl = urlParts[0];
            var originalQuery = urlParts.Length > 1 ? urlParts[1] : string.Empty;

            if (!string.IsNullOrEmpty(originalQuery) && !string.IsNullOrWhiteSpace(originalQuery))
            {
                queryString = string.Join("&", originalQuery, queryString);
            }
            return string.Join("?", taskPageUrl, queryString);
        }

        private void RemoveUser(Dal.Model.TicketTask task, TimeSpan? frizeTaskOnTime = null)
        {
            if (frizeTaskOnTime.HasValue)
                task.ReturnToQueueAfterDate = DateTime.Now.ToUniversalTime() + frizeTaskOnTime.Value;

            this.TicketService.TaskOperations.InsertUserAbandonedOperation(task);
        }
    }
}