﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using TicketTool.Data.Context;
using TicketTool.Data.Contracts;
using TicketTool.Data.Contracts.Common;
using TicketTool.Data.Contracts.Operations;
using TicketTool.Data.Contracts.Tasks;
using TicketTool.Data.Strategies.Contracts;
using Parameter = TicketTool.Data.Contracts.Common.Parameter;
using SkillSet = TicketTool.Data.Contracts.SkillSet;
using User = TicketTool.Data.Contracts.Common.User;

namespace TicketTool.Data.Strategies
{
    internal class UserStrategy : TicketToolStrategyBase, IUserStrategy
    {
        internal UserStrategy(ITicketService ticketService)
            : base(ticketService)
        {
        }

        public void AddUserToSkillSetsIfSkillSetsNotExist(IEnumerable<SkillSet> skillSets,
            IEnumerable<Cluster> clusters = null)
        {
            var provider = TicketToolContext.Current.DataProvider;
            var skillsetNames = skillSets.Select(t => t.Name).ToList();
            if (skillsetNames.Count == 0)
                throw new InvalidOperationException("Skillset collection must have at least one skillset");

            var userSkillSets = TicketService.SkillSets.GetUserSkillSets();
            var loggedSkillSets = userSkillSets
                .Where(us => us.IsLogged)
                .Select(us => us.Name).ToList();
            skillsetNames = skillsetNames.Except(loggedSkillSets).ToList();
            var availableSkillSets = userSkillSets.Select(s => s.Name).ToList();
            var filteredSkillSets = provider.GetSkillSets()
                .Where(t => skillsetNames.Contains(t.Name))
                .Where(s => availableSkillSets.Contains(s.Name))
                .ToList();
            var currentDate = DateTime.Now.ToUniversalTime();
            var user = provider.GetUser(TicketToolContext.Current.Credentials.Name);
            foreach (var skillSet in filteredSkillSets)
            {
                provider.AddSkillSetToUser(user, skillSet);
                var userLog = provider.UserLogCreateRecord(user, skillSet);
                userLog.LoginDate = currentDate;
            }
            if (clusters != null)
            {
                var clustersToLog = clusters.Select(el => el.Name);
                var allClusters = provider.GetAllClusters();
                var filteredClusterToLog = allClusters.Where(t => clustersToLog.Contains(t.Name)).ToList();
                foreach (var cluster in filteredClusterToLog)
                {
                    provider.AddClusterToUser(user, cluster);
                }
            }
            provider.Save();
        }

        public void RemoveUserWithTasksFromSkillSets()
        {
            var provider = TicketToolContext.Current.DataProvider;
            this.Abandon(TicketToolContext.Current.Credentials.Name);
            provider.Save();
        }

        public void Abandon(string userName)
        {
            var provider = TicketToolContext.Current.DataProvider;
            var user = provider.GetUser(userName);
            var currentDate = DateTime.Now.ToUniversalTime();
            foreach (var skillSet in user.SkillSets.ToArray())
            {
                provider.RemoveSkillSetFromUser(user, skillSet);
                var userLog = provider.GetUserLog(user, skillSet);
                if (userLog.LoginDate == DateTime.MinValue)
                {
                    userLog.LoginDate = user.LastLoginDate;
                }
                userLog.LogoutDate = currentDate;
            }
            foreach (var cluster in user.Clusters.ToArray())
            {
                provider.RemoveClusterFromUser(user, cluster);
            }
            provider.GetAllTaskByUser(user).ToList()
                .ForEach(t => this.TicketService.TaskOperations.InsertUserAbandonedOperation(t));
        }      

        public void LockUnlockUser(String userName, bool isLock)
        {
            var provider = TicketToolContext.Current.DataProvider;
            var user = provider.GetUser(userName);
            user.IsLocked = isLock;
            provider.Save();
        }

        public ICollection<User> GetAllUsers()
        {
            var provider = TicketToolContext.Current.DataProvider;
            var users = provider.GetAllUsers(withRoles: true, withParameters: true, withAvailableSkillSets: true).ToList();
            return users.Select(t => t.ToContract()).ToList();
        }

        public User GetUser(string userName)
        {
            var provider = TicketToolContext.Current.DataProvider;
            var user = provider.GetUser(userName, withAvailableSkillSets: true, withRoles: true, withStores: true, withSkillSets: true);
            return user.ToContract();
        }

        public ICollection<User> GetUsers(Parameter parameter, IEnumerable<string> roles)
        {            
            return
                TicketToolContext.Current.DataProvider.GetAllUsers(withParameters: true, withRoles: true)
                    .Where(t => t.Parameters.Select(x => x.ParameterName).Contains(parameter.ParameterName))
                    .Where(t => t.Roles.Select(x => x.Name).Intersect(roles).Any())
                    .ToArray()
                    .Select(t => t.ToContract())
                    .ToList();
        }

        public ICollection<User> GetDefaultUsersByParam(Parameter parameter)
        {
            var users = TicketToolContext.Current.DataProvider
                .GetUsersByParameter(
                    new Dal.Model.Parameter {ParameterName = parameter.ParameterName})
                .ToList();
            return users
                                    .Select(u => u.ToContract())
                                    .ToList();
        }

        public ICollection<UserStat> GetStatisticsForUsers()
        {
            var provider = TicketToolContext.Current.DataProvider;
            var currentDate = DateTime.Now.ToUniversalTime().Date;
            return provider.GetUserActions(currentDate)
                           .Select(ul => new UserStat
                               {                                             
                                   UserName = ul.User.Name,
                                   SkillSetName = ul.SkillSet.Name,
                                   AbandonedQty = ul.AbandonedQty,
                                   ProcessedQty = ul.ProcessedQty,
                                   LoginDate = ul.LoginDate,
                                   LogoutDate = ul.LogoutDate,
                                   SkillSetTime = ul.LogoutDate.HasValue ? ul.LogoutDate.Value - ul.LoginDate : DateTime.Now.ToUniversalTime() - ul.LoginDate
                               })
                           .ToList();
        }

        public PingByUserResult PingByUser()
        {
            var provider = TicketToolContext.Current.DataProvider;
            var userName = TicketToolContext.Current.Credentials.Name;
            var result = provider.GetPingUserInfo(userName);

            var currentDate = DateTime.Now.ToUniversalTime();
            return new PingByUserResult
                {
                    LoggedOnSkillSets = result.SkillSetName,
                    Tasks = result.SkillSetName.Count > 0
                                ? result.AssingnedTasks.Select(
                                    t => new PingTask
                                        {
                                            TaskId = t.TaskId.ToString(CultureInfo.InvariantCulture),
                                            UserTime = t.UserDateAssigned.HasValue ? currentDate - t.UserDateAssigned.Value : default(TimeSpan),
                                            MaxTimeForUser = t.MaxUserRuntime ?? default(TimeSpan)
                                        }).ToList()
                                : new List<PingTask>()
                };
        }

        public long UpdateUserParams(string login, IEnumerable<string> parameters)
        {
            var provider = TicketToolContext.Current.DataProvider;
            var user = provider.GetUser(login, withStores: true);
            user.Parameters.RemoveAll(p => parameters.All(pp => pp != p.ParameterName));
            var userParameters = user.Parameters.Select(p => p.ParameterName).ToArray();
            var storedParams = provider
                .GetAllParameters()
                .Where(s => parameters.Contains(s.ParameterName))
                .Where(s => !userParameters.Contains(s.ParameterName))
                .ToArray();
            foreach (var store in storedParams)
            {
                user.Parameters.Add(store);
            }

            provider.Save();
            return user.UserId;
        }

        public long UpdateUserAvailableSkillSets(string login, IEnumerable<string> skillSets)
        {
            var provider = TicketToolContext.Current.DataProvider;
            var user = provider.GetUser(login, withStores: true, withAvailableSkillSets: true);
            user.AvailableSkillSets.RemoveAll(ass => skillSets.All(ss => ss != ass.Name));
            var userAvailableSkillSets = user.AvailableSkillSets.Select(ass => ass.Name);
            var newSkillSets = provider
                .GetSkillSets(withDefault: true)
                .Where(s => skillSets.Contains(s.Name))
                .Where(s => !userAvailableSkillSets.Contains(s.Name))
                .ToArray();
            foreach (var skillSet in newSkillSets)
            {
                user.AvailableSkillSets.Add(skillSet);
            }

            provider.Save();
            return user.UserId;
        }
    }
}