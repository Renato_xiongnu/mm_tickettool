﻿namespace TicketTool.Data.Common
{    
    public class TaskResponse
    {        
        public string Message { get; set; }        
        public bool CompleteSuccessful { get; set; }
    }
}
