﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using TicketTool.Data.Context;
using IsolationLevel = System.Data.IsolationLevel;

namespace TicketTool.Data.Common
{
    public static class TransactionHelper
    {
        public static T ExecuteWithReadUncommited<T>(Func<T> func)
        {
            return func();

            var objectContext =
            ((IObjectContextAdapter)TicketToolContext.Current.DataProvider.ModelContext).ObjectContext;
            objectContext.Connection.Open();
            using (
                var transaction =
                    objectContext.Connection.BeginTransaction(
                        IsolationLevel.ReadUncommitted))
            {
                try
                {
                    var result = func();

                    transaction.Commit();
                    return result;

                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }
    }
}
