﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TicketTool.Data.Common
{
    public static class Extensions
    {
        public static T CopyObjectPropertiesTo<T>(this object value) where T : new()
        {
            return (T) value.CopyObjectPropertiesTo(typeof (T), new T());
        }

        private static object CopyObjectPropertiesTo(this object value, Type T, object retInstance)
        {
            var valueProperties = value.GetType().GetProperties();
            foreach (var respProperty in T.GetProperties())
            {
                var prop = valueProperties.SingleOrDefault(t => t.Name == respProperty.Name);
                if (prop != null)
                {
                    T.GetProperty(respProperty.Name).SetValue(retInstance, prop.GetValue(value));
                }
            }
            return retInstance;
        }

        public static IEnumerable<string> GetInnerExceptionsText(this Exception error)
        {
            ICollection<string> exceptionList = new LinkedList<string>();
            Exception realerror = error;
            while (realerror.InnerException != null)
            {
                realerror = realerror.InnerException;
                exceptionList.Add(realerror.Message);
            }

            return exceptionList;
        }

        private static string GetExceptionDetails(this Exception exception)
        {
            var fields = exception.GetType().GetProperties()
                .Select(property => new {property.Name, Value = property.GetValue(exception, null)})
                .Select(x => String.Format("{0} = {1}", x.Name, x.Value != null ? x.Value.ToString() : String.Empty));
            return String.Join("\n", fields);
        }


        public static string GetExceptionFullInfoText(this Exception ex)
        {
            return String.Join("\n", new[] {"Fields:", ex.GetExceptionDetails(), "Inner exceptions:"}.Union(ex.GetInnerExceptionsText()));
        }
    }
}