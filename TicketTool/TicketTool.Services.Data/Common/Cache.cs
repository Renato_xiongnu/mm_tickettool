﻿using System;
using System.Runtime.Caching;

namespace TicketTool.Data.Common
{
    public class Cache
    {
        public static T ExecuteOrGetCache<T>(Func<T> func,string cacheKey,TimeSpan cacheLifetime) where T:class
        {
            var cache = MemoryCache.Default;
            if (cache == null) return func();

            var cachedResult = cache[cacheKey] as T;

            if (cachedResult != null) return cachedResult;

            var result = func();
            cache.Add(cacheKey, result, DateTimeOffset.Now.Add(cacheLifetime));

            return result;
        }
    }
}
