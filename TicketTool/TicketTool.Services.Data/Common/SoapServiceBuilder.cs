﻿using System;
using System.Linq;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ServiceModel.Description;
using TicketTool.Data.Strategies;
using TicketTool.Data.Strategies.Contracts;

namespace TicketTool.Data.Common
{
    public static class SoapServiceBuilder
    {
        private static readonly Dictionary<string, IExternalSystemProxy> Cache = new Dictionary<string, IExternalSystemProxy>();
        private static readonly Object LockObj = new Object();

        public static IExternalSystemProxy GetProxy(string wsdlUrl)
        {
            lock (LockObj)
            {
                if (!Cache.ContainsKey(wsdlUrl))
                {
                    var proxy = Compile(wsdlUrl);
                    Cache.Add(wsdlUrl, proxy);
                    return proxy;
                }
                return Cache[wsdlUrl];
            }            
        }

        private static IExternalSystemProxy Compile(string wsdlUrl)
        {
            var mexClient = new MetadataExchangeClient(new Uri(wsdlUrl), MetadataExchangeClientMode.HttpGet) { ResolveMetadataReferences = true };
            var importer = new WsdlImporter(mexClient.GetMetadata());
            var endpoint = importer.ImportAllEndpoints().First();
            var contract = importer.ImportAllContracts().First();

            var generator = new ServiceContractGenerator();
            generator.GenerateServiceContractType(contract);
            if (generator.Errors.Count != 0) throw new Exception("There were errors during code compilation.");

            var dlls = new[] {"System.dll", "System.ServiceModel.dll", "System.Runtime.Serialization.dll"};
            var results = CodeDomProvider.CreateProvider("C#")
                                         .CompileAssemblyFromDom(new CompilerParameters(dlls) {GenerateInMemory = true}, generator.TargetCompileUnit);
            if (results.Errors.Count > 0) throw new Exception("There were errors during generated code compilation");

            return new ExternalSystemProxy(results.CompiledAssembly, endpoint);
        }
    }
}
