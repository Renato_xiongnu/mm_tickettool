﻿using System;
using System.Xml;
using Newtonsoft.Json;

namespace TicketTool.Data.Common
{
    public static class DataContractJsonHelper<T>
    {
        public static string SerializeData(T c)
        {
            return JsonConvert.SerializeObject(c, new TimeSpanToJsonConverter());
        }

        public static T DeserializeData(string value)
        {
            return JsonConvert.DeserializeObject<T>(value, new JsonToTimeSpanConverter());
        }
    }

    public class TimeSpanToJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof (TimeSpan);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(XmlConvert.ToString((TimeSpan)value));
            writer.Flush();
        }
    }

    public class JsonToTimeSpanConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof (Object);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.ValueType == typeof(string))
            {
                try
                {
                    return XmlConvert.ToTimeSpan(reader.Value.ToString());
                }
                // ReSharper disable EmptyGeneralCatchClause
                catch
                // ReSharper restore EmptyGeneralCatchClause
                {
                    return reader.Value;
                }
            }
            return reader.Value;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}