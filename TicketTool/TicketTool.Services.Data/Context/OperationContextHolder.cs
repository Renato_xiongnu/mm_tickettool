﻿using System.ServiceModel;
using System.Threading;

namespace TicketTool.Data.Context
{
    public class OperationContextHolder<T> : IExtension<OperationContext> where T : new()
    {       
        private static readonly ThreadLocal<T> StaticExtension;

        static OperationContextHolder()
        {
            StaticExtension = new ThreadLocal<T>(() => new T());
        }        

        public OperationContextHolder()
        {
            Extension = new T();
        }

        public T Extension { get; private set; }

        public void Attach(OperationContext owner)
        {            
        }

        public void Detach(OperationContext owner)
        {            
        }

        public static OperationContextHolder<T> GetExtension()
        {
            return OperationContext.Current != null ? OperationContext.Current.Extensions.Find<OperationContextHolder<T>>() : null;
        }

        public static T Current
        {
            get
            {
                var extension = GetExtension();
                return extension == null ? StaticExtension.Value : extension.Extension;
            }
        }
    }
}