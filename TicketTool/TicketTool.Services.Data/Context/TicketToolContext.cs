﻿using System;
using TicketTool.Dal;

namespace TicketTool.Data.Context
{
    public class TicketToolContext
    {
        private UserCredential _credentials;        

        public UserCredential Credentials
        {
            get { return _credentials ?? (_credentials = new UserCredential()); }
            set { _credentials = value; }
        }

        public DataProvider DataProvider { get; private set; }

        public T RunInServiceContext<T>(Func<T> function)
        {
            T result;
            using (this.DataProvider = new DataProvider())
            {
                result = function();
            }
            this.DataProvider = null;
            return result;
        }

        public void RunInServiceContext(Action action)
        {
            using (this.DataProvider = new DataProvider())
            {
                action();
            }
            this.DataProvider = null;
        }

        public static TicketToolContext Current
        {
            get { return OperationContextHolder<TicketToolContext>.Current; }
        }
    }
}