﻿using System.Collections.Generic;

namespace TicketTool.Data.Context
{
    public class UserCredential
    {
        public string Name { get; set; }

// ReSharper disable UnusedAutoPropertyAccessor.Global
        public IReadOnlyCollection<string> Roles { get; set; }
// ReSharper restore UnusedAutoPropertyAccessor.Global
    }
}