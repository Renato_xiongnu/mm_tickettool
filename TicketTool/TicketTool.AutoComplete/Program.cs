﻿using System;
using System.IO;
using System.Threading;
using TicketTool.AutoComplete.Properties;
using TicketTool.AutoComplete.TicketToolAuthProxy;
using TicketTool.AutoComplete.TicketToolProxy;

namespace TicketTool.AutoComplete
{
    class Program
    {
        static void Main()
        {
            new AuthenticationServiceClient().LogIn(Settings.Default.Login, Settings.Default.Password);
            var t = new TicketToolServiceClient();
            var orders = File.ReadAllLines("orders.txt");
            foreach (var order in orders)
            {
                var ticket = t.GetTicket(order);
                t.CloseTicket(ticket);
            }

            //while (true)
            //{
            //    new AuthenticationServiceClient().LogIn(Settings.Default.Login, Settings.Default.Password);
            //    var t = new TicketToolServiceClient().GetNextTask();
            //    if (t != null && !String.IsNullOrEmpty(t.TaskId))
            //    {
            //        ClearConsole();
            //        Console.WriteLine("Trying to complete task {0}", t.TaskId);
            //        new AuthenticationServiceClient().LogIn(Settings.Default.Login, Settings.Default.Password);
            //        var result = new TicketToolServiceClient().CompleteTask(t.TaskId, Settings.Default.DefaultOutcome, new RequiredField[0]);
            //        Console.WriteLine(result.HasError ? result.MessageText : "Completed");
            //    }
            //    else
            //    {
            //        ClearConsole();
            //        Console.WriteLine("Waiting");
            //        Thread.Sleep(Settings.Default.Ping);
            //        Console.WriteLine("Retrying");
            //    }
            //}
        }

        static void ClearConsole()
        {
            Console.Clear();
            Console.CursorLeft = 0;
            Console.CursorTop = 0;
        }
    }
}
