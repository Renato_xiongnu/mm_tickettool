﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Orders.Backend.Activities.OrdersProxy;
using CustomerOrder = Orders.Backend.Model.CustomerOrder;

namespace TicketTool.DeleteDeadWF
{
    static class Program
    {
        static void Main()
        {
            using (var ctx = new AppFabricPersistence())
            {
                using (var iaCtx = new InstancesAnalyzerEntities())
                {
                    var ttzzt = new List<long> {12594, 36493, 44009, 45540, 75328, 95451, 100239, 112516, 117833};
                    var instances = ctx.Instances
                        .Where(i => ttzzt.Contains(i.SurrogateIdentityId))
                        .Where(i => i.ComplexDataProperties != null).ToList();

                    foreach (var instance in instances)
                    {
                        CustomerOrder order = null;
                        string persistedPath;
                        string orderId = String.Empty;
                        string ticketId = Guid.Empty.ToString();
                        try
                        {
                            var serializer = new GZipInstanceSerializer();
                            var data = serializer.DeserializePropertyBag(instance.ComplexDataProperties);
                            var activityExecutor = data.First().Value;
                            ticketId = activityExecutor.ExecuteProperty<string>("SerializedProgramMapping.SerializedInstanceLists[0].SingleItem.SerializedEnvironment.SerializedLocations[3].SerializedValue");
                            order = activityExecutor.ExecuteProperty<CustomerOrder>("SerializedProgramMapping.SerializedInstanceLists[0].SingleItem.SerializedChildren.SingleItem.SerializedEnvironment.SerializedLocations[1].SerializedValue");
                            orderId = order.OrderId;
                            var rootActivity = activityExecutor.ExecuteProperty<ActivityInstance>("SerializedProgramMapping.SerializedInstanceLists[0].SingleItem");
                            var sb = new StringBuilder();
                            IterateActivitiesTree(rootActivity, sb);
                            persistedPath = sb.ToString();
                        }
                        catch
                        {
                            persistedPath = String.Empty;
                        }


                        var suspended = new Suspended
                        {
                            WorkflowId = instance.Id,
                            WorkitemId = orderId,
                            TicketId = new Guid(ticketId),
                            ExceptionName = instance.SuspensionExceptionName,
                            PersistedPoint = persistedPath,
                            Hash = persistedPath.GetHashCode(),
                            CustomerOrder = (order == null ? null : MMS.Json.JsonConvert.SerializeObject(order)),
                            Created = instance.CreationTime,
                            State = instance.ExecutionStatus,
                            IsSuspended = instance.IsSuspended,
                            OrderState = GetOrderState(orderId)
                        };
                        iaCtx.Suspendeds.Add(suspended);
                        iaCtx.SaveChanges();
                    }
                }
            }

            //var obj = (WorkflowService)XamlServices.Load(@"C:\git\tickettool\TicketTool\TicketTool.Projects\TicketTool.Projects.Zzt\TicketTool.Projects.Zzt.WF\ZztOrders.xamlx");
            //var wis = WorkflowInspectionServices.Resolve(obj.Body, "18");           
            //Console.WriteLine(wis.DisplayName);
            //Console.WriteLine(obj);

            Console.WriteLine("Done");
            Console.ReadLine();
        }

        private static string GetOrderState(string orderId)
        {
            if (String.IsNullOrEmpty(orderId)) return String.Empty;
            var result = new OrderServiceClient().GetOrder(orderId);
            return result.ReturnCode == ReturnCode.Ok ? result.CustomerOrder.OrderInfo.OrderState : String.Empty;
        }

        private static void IterateActivitiesTree(ActivityInstance activity, StringBuilder sb)
        {
            sb.AppendFormat("{0}{1}", activity.Id, activity.ExecuteProperty<string>("OwnerName"));
            var children = activity.ExecuteProperty<IList<ActivityInstance>>("SerializedChildren.MultipleItems");
            var child = activity.ExecuteProperty<ActivityInstance>("SerializedChildren.SingleItem");
            if (child != null)
            {
                IterateActivitiesTree(child, sb);
                return;
            }
            if (children == null) return;
            foreach (var instance in children)
            {
                IterateActivitiesTree(instance, sb);
            }
        }
    }
}