﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace TicketTool.DeleteDeadWF
{
    public static class ReflectorUtil
    {
        public static T ExecuteProperty<T>(this object value, string path)
        {
            if (value == null) throw new ArgumentNullException("value");
            if (path == null) throw new ArgumentNullException("path");

            Type currentType = value.GetType();

            object obj = value;
            foreach (string propertyName in path.Split('.'))
            {
                if (currentType != null)
                {
                    int brackStart = propertyName.IndexOf("[");
                    int brackEnd = propertyName.IndexOf("]");

                    PropertyInfo property = currentType.GetProperty(brackStart > 0 ? propertyName.Substring(0, brackStart) : propertyName, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.GetProperty);
                    obj = property.GetValue(obj, null);

                    if (brackStart > 0)
                    {
                        string index = propertyName.Substring(brackStart + 1, brackEnd - brackStart - 1);
                        foreach (Type iType in obj.GetType().GetInterfaces())
                        {
                            if (iType.IsGenericType && iType.GetGenericTypeDefinition() == typeof (IDictionary<,>))
                            {
                                obj = typeof (ReflectorUtil).GetMethod("GetDictionaryElement")
                                    .MakeGenericMethod(iType.GetGenericArguments())
                                    .Invoke(null, new[] {obj, index});
                                break;
                            }
                            if (iType.IsGenericType && iType.GetGenericTypeDefinition() == typeof (IList<>))
                            {
                                obj = typeof (ReflectorUtil).GetMethod("GetListElement")
                                    .MakeGenericMethod(iType.GetGenericArguments())
                                    .Invoke(null, new[] {obj, index});
                                break;
                            }
                        }
                    }

                    currentType = obj != null ? obj.GetType() : null; //property.PropertyType;
                }
                else
                {
                    return default(T);
                }
            }
            return (T)obj;
        }

// ReSharper disable once UnusedMember.Global
        public static TValue GetDictionaryElement<TKey, TValue>(IDictionary<TKey, TValue> dict, object index)
        {
            var key = (TKey)Convert.ChangeType(index, typeof(TKey), null);
            return dict[key];
        }

// ReSharper disable once UnusedMember.Global
        public static T GetListElement<T>(IList<T> list, object index)
        {
            return list[Convert.ToInt32(index)];
        }

    }
}
