﻿using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Xml.Linq;

namespace TicketTool.DeleteDeadWF
{
    internal class GZipInstanceSerializer : InstanceSerializer
    {
        protected override Dictionary<XName, object> DeserializePropertyBag(Stream stream)
        {
            using (var gzipStream = new GZipStream(stream, CompressionMode.Decompress, true))
                return base.DeserializePropertyBag(gzipStream);
        }

        protected override object DeserializeValue(Stream stream)
        {
            using (var gzipStream = new GZipStream(stream, CompressionMode.Decompress, true))
                return base.DeserializeValue(gzipStream);
        }

        protected override void SerializePropertyBag(Stream stream, Dictionary<XName, object> propertyBag)
        {
            using (var gzipStream = new GZipStream(stream, CompressionLevel.Fastest, true))
                base.SerializePropertyBag(gzipStream, propertyBag);
        }

        protected override void SerializeValue(Stream stream, object value)
        {
            using (var gzipStream = new GZipStream(stream, CompressionLevel.Fastest, true))
                base.SerializeValue(gzipStream, value);
        }
    }
}
