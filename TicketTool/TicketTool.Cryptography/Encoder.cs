﻿using System;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;

namespace TicketTool.Cryptography
{
    public static class Encoder
    {
        private const string Key =
            "6235EF5FF5523129C283FE10BC9F478F7E4A8B7AA376D9B59C18E08BD93C282D09B1402FE6CB072765169B4A140FFE620E56F8C633388ACFABD26102691C5C23FFA0E053E0F369332C64995D8BEF30381D0E3A4CD6C0345234578A32FED842D71D9C023172651BC01C85EB1AA04E0EB96F468E02C694414DE8CE4E41A8D2E9BB";

        public static string Encode(string str)
        {
            var hash = new HMACSHA1(HexToByte(Key));
            var processed = BitConverter.ToInt64(hash.ComputeHash(Encoding.Unicode.GetBytes(str)), 0);
            return Math.Abs(processed).ToString(CultureInfo.InvariantCulture);
        }

        private static byte[] HexToByte(string hexString)
        {
            var returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        }
    }
}
