﻿using System.Data.Entity;
using TicketTool.Dal.Model;


namespace TicketTool.Dal
{
   
    public class ModelContext : DbContext
    {
        public DbSet<SkillSet> SkillSets { get; set; }

        public DbSet<TaskType> TaskTypes { get; set; }

        public DbSet<TicketTask> TicketTasks { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Parameter> Parameters { get; set; } 

        public DbSet<ExternalSystem> ExternalSystems { get; set; }

        public DbSet<Ticket> Tickets { get; set; }

        public DbSet<Outcome> Outcomes { get; set; }

        public DbSet<FieldType> FieldTypes { get; set; }

        public DbSet<TaskField> TaskFields { get; set; }

        public DbSet<TaskOperation> TaskOperations { get; set; }

        public DbSet<UserLog> UserLog { get; set; }

        public DbSet<UserAction> UserActions { get; set; }

        public DbSet<SchedulerLog> SchedulerLog { get; set; }

        public DbSet<Event> Events { get; set; }

        public DbSet<TaskCompletionQueue> TaskCompletionQueue { get; set; }

        public DbSet<TaskCompletionQueueLog> TaskCompletionQueueLog { get; set; }

        public DbSet<NotificationLogRecord> EventLogRecord { get; set; }

        public DbSet<NotificationLogGroup> EventLogGroup { get; set; }

        public DbSet<Cluster> Cluster { get; set; } 

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SkillSet>().HasMany(t => t.TaskTypes).WithOptional(t => t.SkillSet);

            modelBuilder.Entity<TicketTask>().HasMany(t => t.OperationLog).WithOptional(t => t.TicketTask);
            modelBuilder.Entity<User>()
                .HasMany(p => p.AvailableSkillSets)
                .WithMany(t => t.AvailableUsers)
                .Map(mc =>
                {
                    mc.ToTable("UserAvailableSkillSets");
                    mc.MapLeftKey("UserId");
                    mc.MapRightKey("SkillSetId");
                });
        }
    }
}
