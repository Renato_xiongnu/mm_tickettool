namespace TicketTool.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v203 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TicketTasks", "Performer_UserId", c => c.Long());
            AddForeignKey("dbo.TicketTasks", "Performer_UserId", "dbo.Users", "UserId");
            CreateIndex("dbo.TicketTasks", "Performer_UserId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.TicketTasks", new[] { "Performer_UserId" });
            DropForeignKey("dbo.TicketTasks", "Performer_UserId", "dbo.Users");
            DropColumn("dbo.TicketTasks", "Performer_UserId");
        }
    }
}
