using TicketTool.Dal.Model.Design;

namespace TicketTool.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TicketTool_V10 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SkillSets",
                c => new
                    {
                        SkillSetId = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 255),
                        IsDefault = c.Boolean(nullable: false),
                        IsParameterizable = c.Boolean(nullable: false),
                        DeliveryTaskByEmail = c.Boolean(nullable: false),
                        IsEscalation = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.SkillSetId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 255),
                        Email = c.String(maxLength: 4000),
                        Password = c.String(nullable: false, maxLength: 4000),
                        LastPingDateTime = c.DateTime(nullable: false),
                        IsLocked = c.Boolean(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        LastLoginDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Stores",
                c => new
                    {
                        SapCode = c.String(nullable: false, maxLength: 4),
                        Name = c.String(nullable: false, maxLength: 255),
                        TimeZone = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.SapCode);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        RoleId = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.RoleId);
            
            CreateTable(
                "dbo.TaskTypes",
                c => new
                    {
                        TaskTypeId = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 255),
                        SlaControlDate = c.DateTime(),
                        SlaName = c.String(maxLength: 255),
                        MaxSkillSetRunTime = c.Time(),
                        MaxUserRuntime = c.Time(),
                        Process_ProcessId = c.String(maxLength: 128),
                        SlaSkillSet_SkillSetId = c.Long(),
                        SkillSet_SkillSetId = c.Long(),
                    })
                .PrimaryKey(t => t.TaskTypeId)
                .ForeignKey("dbo.WorkflowProcesses", t => t.Process_ProcessId)
                .ForeignKey("dbo.SkillSets", t => t.SlaSkillSet_SkillSetId)
                .ForeignKey("dbo.SkillSets", t => t.SkillSet_SkillSetId)
                .Index(t => t.Process_ProcessId)
                .Index(t => t.SlaSkillSet_SkillSetId)
                .Index(t => t.SkillSet_SkillSetId);
            
            CreateTable(
                "dbo.WorkflowProcesses",
                c => new
                    {
                        ProcessId = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 4000),
                    })
                .PrimaryKey(t => t.ProcessId);
            
            CreateTable(
                "dbo.TicketTasks",
                c => new
                    {
                        TaskId = c.Long(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        Description = c.String(),
                        CurrentOutcome = c.String(maxLength: 255),
                        Comment = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        FinishDate = c.DateTime(),
                        TaskStatus = c.Int(nullable: false),
                        Escalated = c.Boolean(nullable: false),
                        SkillSetDateAssigned = c.DateTime(),
                        SkillSetRuntime = c.Time(),
                        UserDateAssigned = c.DateTime(),
                        ReturnToQueueAfterDate = c.DateTime(),
                        UserRuntime = c.Time(),
                        Parameter = c.String(maxLength: 255),
                        RelatedContent = c.String(),
                        RelatedContentName = c.String(maxLength: 255),
                        RelatedContentRight = c.Int(nullable: false),
                        Ticket_TicketId = c.String(maxLength: 36),
                        TaskType_TaskTypeId = c.Long(),
                        Outcomes_Id = c.Long(),
                        InformedUser_UserId = c.Long(),
                        CurrentAssignee_UserId = c.Long(),
                        CurrentSkillSet_SkillSetId = c.Long(),
                        LastUserOperation_OperationId = c.Long(),
                        LastSkillSetOperation_OperationId = c.Long(),
                    })
                .PrimaryKey(t => t.TaskId)
                .ForeignKey("dbo.Tickets", t => t.Ticket_TicketId)
                .ForeignKey("dbo.TaskTypes", t => t.TaskType_TaskTypeId)
                .ForeignKey("dbo.Outcomes", t => t.Outcomes_Id)
                .ForeignKey("dbo.Users", t => t.InformedUser_UserId)
                .ForeignKey("dbo.Users", t => t.CurrentAssignee_UserId)
                .ForeignKey("dbo.SkillSets", t => t.CurrentSkillSet_SkillSetId)
                .ForeignKey("dbo.TaskOperations", t => t.LastUserOperation_OperationId)
                .ForeignKey("dbo.TaskOperations", t => t.LastSkillSetOperation_OperationId)
                .Index(t => t.Ticket_TicketId)
                .Index(t => t.TaskType_TaskTypeId)
                .Index(t => t.Outcomes_Id)
                .Index(t => t.InformedUser_UserId)
                .Index(t => t.CurrentAssignee_UserId)
                .Index(t => t.CurrentSkillSet_SkillSetId)
                .Index(t => t.LastUserOperation_OperationId)
                .Index(t => t.LastSkillSetOperation_OperationId);
            
            CreateTable(
                "dbo.Tickets",
                c => new
                    {
                        TicketId = c.String(nullable: false, maxLength: 36),
                        Parameter = c.String(maxLength: 255),
                        WorkItemId = c.String(maxLength: 255),
                        Name = c.String(maxLength: 4000),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        CloseDate = c.DateTime(),
                        Process_ProcessId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.TicketId)
                .ForeignKey("dbo.WorkflowProcesses", t => t.Process_ProcessId)
                .Index(t => t.Process_ProcessId);
            
            CreateTable(
                "dbo.Outcomes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Data = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TaskFields",
                c => new
                    {
                        TaskFieldId = c.Long(nullable: false, identity: true),
                        Value = c.String(),
                        FieldType_FieldTypeId = c.Long(),
                        TicketTask_TaskId = c.Long(),
                    })
                .PrimaryKey(t => t.TaskFieldId)
                .ForeignKey("dbo.FieldTypes", t => t.FieldType_FieldTypeId)
                .ForeignKey("dbo.TicketTasks", t => t.TicketTask_TaskId)
                .Index(t => t.FieldType_FieldTypeId)
                .Index(t => t.TicketTask_TaskId);
            
            CreateTable(
                "dbo.FieldTypes",
                c => new
                    {
                        FieldTypeId = c.Long(nullable: false, identity: true),
                        Type = c.String(nullable: false, maxLength: 255),
                        Name = c.String(maxLength: 255),
                        IsMandatory = c.Boolean(nullable: false),
                        DefaultValue = c.String(),
                        PredefinedValuesList = c.String(maxLength: 4000),
                    })
                .PrimaryKey(t => t.FieldTypeId);
            
            CreateTable(
                "dbo.TaskOperations",
                c => new
                    {
                        OperationId = c.Long(nullable: false, identity: true),
                        FromStatus = c.Int(nullable: false),
                        ToStatus = c.Int(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        HasError = c.Boolean(nullable: false),
                        MessageText = c.String(maxLength: 4000),
                        SkillSetDateAssigned = c.DateTime(),
                        SkillSetRuntime = c.Time(),
                        UserDateAssigned = c.DateTime(),
                        UserRuntime = c.Time(),
                        RefOperation_OperationId = c.Long(),
                        SkillSet_SkillSetId = c.Long(),
                        User_UserId = c.Long(),
                        TicketTask_TaskId = c.Long(),
                    })
                .PrimaryKey(t => t.OperationId)
                .ForeignKey("dbo.TaskOperations", t => t.RefOperation_OperationId)
                .ForeignKey("dbo.SkillSets", t => t.SkillSet_SkillSetId)
                .ForeignKey("dbo.Users", t => t.User_UserId)
                .ForeignKey("dbo.TicketTasks", t => t.TicketTask_TaskId)
                .Index(t => t.RefOperation_OperationId)
                .Index(t => t.SkillSet_SkillSetId)
                .Index(t => t.User_UserId)
                .Index(t => t.TicketTask_TaskId);
            
            CreateTable(
                "dbo.UserLogs",
                c => new
                    {
                        UserLogId = c.Long(nullable: false, identity: true),
                        LoginDate = c.DateTime(nullable: false),
                        LogoutDate = c.DateTime(),
                        AbandonedQty = c.Int(nullable: false),
                        ProcessedQty = c.Int(nullable: false),
                        User_UserId = c.Long(nullable: false),
                        SkillSet_SkillSetId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.UserLogId)
                .ForeignKey("dbo.Users", t => t.User_UserId, cascadeDelete: true)
                .ForeignKey("dbo.SkillSets", t => t.SkillSet_SkillSetId, cascadeDelete: true)
                .Index(t => t.User_UserId)
                .Index(t => t.SkillSet_SkillSetId);
            
            CreateTable(
                "dbo.SchedulerLogs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        StartTime = c.DateTime(nullable: false),
                        EndTime = c.DateTime(nullable: false),
                        EscalatedTasksNum = c.Int(nullable: false),
                        EmailSendTasksNum = c.Int(nullable: false),
                        AbandonUsersNum = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StoreUsers",
                c => new
                    {
                        Store_SapCode = c.String(nullable: false, maxLength: 4),
                        User_UserId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.Store_SapCode, t.User_UserId })
                .ForeignKey("dbo.Stores", t => t.Store_SapCode, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.User_UserId, cascadeDelete: true)
                .Index(t => t.Store_SapCode)
                .Index(t => t.User_UserId);
            
            CreateTable(
                "dbo.UserSkillSets",
                c => new
                    {
                        User_UserId = c.Long(nullable: false),
                        SkillSet_SkillSetId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_UserId, t.SkillSet_SkillSetId })
                .ForeignKey("dbo.Users", t => t.User_UserId, cascadeDelete: true)
                .ForeignKey("dbo.SkillSets", t => t.SkillSet_SkillSetId, cascadeDelete: true)
                .Index(t => t.User_UserId)
                .Index(t => t.SkillSet_SkillSetId);
            
            CreateTable(
                "dbo.RoleUsers",
                c => new
                    {
                        Role_RoleId = c.Long(nullable: false),
                        User_UserId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.Role_RoleId, t.User_UserId })
                .ForeignKey("dbo.Roles", t => t.Role_RoleId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.User_UserId, cascadeDelete: true)
                .Index(t => t.Role_RoleId)
                .Index(t => t.User_UserId);

            //Apply custom scripts                        
            foreach (var script in QueryProvider.GetDeploymentScripts_V1_0())
            {
                Sql(script);
            }
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.RoleUsers", new[] { "User_UserId" });
            DropIndex("dbo.RoleUsers", new[] { "Role_RoleId" });
            DropIndex("dbo.UserSkillSets", new[] { "SkillSet_SkillSetId" });
            DropIndex("dbo.UserSkillSets", new[] { "User_UserId" });
            DropIndex("dbo.StoreUsers", new[] { "User_UserId" });
            DropIndex("dbo.StoreUsers", new[] { "Store_SapCode" });
            DropIndex("dbo.UserLogs", new[] { "SkillSet_SkillSetId" });
            DropIndex("dbo.UserLogs", new[] { "User_UserId" });
            DropIndex("dbo.TaskOperations", new[] { "TicketTask_TaskId" });
            DropIndex("dbo.TaskOperations", new[] { "User_UserId" });
            DropIndex("dbo.TaskOperations", new[] { "SkillSet_SkillSetId" });
            DropIndex("dbo.TaskOperations", new[] { "RefOperation_OperationId" });
            DropIndex("dbo.TaskFields", new[] { "TicketTask_TaskId" });
            DropIndex("dbo.TaskFields", new[] { "FieldType_FieldTypeId" });
            DropIndex("dbo.Tickets", new[] { "Process_ProcessId" });
            DropIndex("dbo.TicketTasks", new[] { "LastSkillSetOperation_OperationId" });
            DropIndex("dbo.TicketTasks", new[] { "LastUserOperation_OperationId" });
            DropIndex("dbo.TicketTasks", new[] { "CurrentSkillSet_SkillSetId" });
            DropIndex("dbo.TicketTasks", new[] { "CurrentAssignee_UserId" });
            DropIndex("dbo.TicketTasks", new[] { "InformedUser_UserId" });
            DropIndex("dbo.TicketTasks", new[] { "Outcomes_Id" });
            DropIndex("dbo.TicketTasks", new[] { "TaskType_TaskTypeId" });
            DropIndex("dbo.TicketTasks", new[] { "Ticket_TicketId" });
            DropIndex("dbo.TaskTypes", new[] { "SkillSet_SkillSetId" });
            DropIndex("dbo.TaskTypes", new[] { "SlaSkillSet_SkillSetId" });
            DropIndex("dbo.TaskTypes", new[] { "Process_ProcessId" });
            DropForeignKey("dbo.RoleUsers", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.RoleUsers", "Role_RoleId", "dbo.Roles");
            DropForeignKey("dbo.UserSkillSets", "SkillSet_SkillSetId", "dbo.SkillSets");
            DropForeignKey("dbo.UserSkillSets", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.StoreUsers", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.StoreUsers", "Store_SapCode", "dbo.Stores");
            DropForeignKey("dbo.UserLogs", "SkillSet_SkillSetId", "dbo.SkillSets");
            DropForeignKey("dbo.UserLogs", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.TaskOperations", "TicketTask_TaskId", "dbo.TicketTasks");
            DropForeignKey("dbo.TaskOperations", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.TaskOperations", "SkillSet_SkillSetId", "dbo.SkillSets");
            DropForeignKey("dbo.TaskOperations", "RefOperation_OperationId", "dbo.TaskOperations");
            DropForeignKey("dbo.TaskFields", "TicketTask_TaskId", "dbo.TicketTasks");
            DropForeignKey("dbo.TaskFields", "FieldType_FieldTypeId", "dbo.FieldTypes");
            DropForeignKey("dbo.Tickets", "Process_ProcessId", "dbo.WorkflowProcesses");
            DropForeignKey("dbo.TicketTasks", "LastSkillSetOperation_OperationId", "dbo.TaskOperations");
            DropForeignKey("dbo.TicketTasks", "LastUserOperation_OperationId", "dbo.TaskOperations");
            DropForeignKey("dbo.TicketTasks", "CurrentSkillSet_SkillSetId", "dbo.SkillSets");
            DropForeignKey("dbo.TicketTasks", "CurrentAssignee_UserId", "dbo.Users");
            DropForeignKey("dbo.TicketTasks", "InformedUser_UserId", "dbo.Users");
            DropForeignKey("dbo.TicketTasks", "Outcomes_Id", "dbo.Outcomes");
            DropForeignKey("dbo.TicketTasks", "TaskType_TaskTypeId", "dbo.TaskTypes");
            DropForeignKey("dbo.TicketTasks", "Ticket_TicketId", "dbo.Tickets");
            DropForeignKey("dbo.TaskTypes", "SkillSet_SkillSetId", "dbo.SkillSets");
            DropForeignKey("dbo.TaskTypes", "SlaSkillSet_SkillSetId", "dbo.SkillSets");
            DropForeignKey("dbo.TaskTypes", "Process_ProcessId", "dbo.WorkflowProcesses");
            DropTable("dbo.RoleUsers");
            DropTable("dbo.UserSkillSets");
            DropTable("dbo.StoreUsers");
            DropTable("dbo.SchedulerLogs");
            DropTable("dbo.UserLogs");
            DropTable("dbo.TaskOperations");
            DropTable("dbo.FieldTypes");
            DropTable("dbo.TaskFields");
            DropTable("dbo.Outcomes");
            DropTable("dbo.Tickets");
            DropTable("dbo.TicketTasks");
            DropTable("dbo.WorkflowProcesses");
            DropTable("dbo.TaskTypes");
            DropTable("dbo.Roles");
            DropTable("dbo.Stores");
            DropTable("dbo.Users");
            DropTable("dbo.SkillSets");
        }
    }
}
