namespace TicketTool.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v205 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ExternalSystems", "UseMapping", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ExternalSystems", "UseMapping");
        }
    }
}
