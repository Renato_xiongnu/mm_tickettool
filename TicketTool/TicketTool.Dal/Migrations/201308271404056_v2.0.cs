namespace TicketTool.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v20 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TaskTypes", "Process_ProcessId", "dbo.WorkflowProcesses");
            DropForeignKey("dbo.Tickets", "Process_ProcessId", "dbo.WorkflowProcesses");
            DropForeignKey("dbo.TaskCompletionQueues", "Process_ProcessId", "dbo.WorkflowProcesses");

            //DropIndex("dbo.TaskTypes", new[] { "Process_ProcessId" });
            //DropIndex("dbo.Tickets", new[] { "Process_ProcessId" });
            //DropIndex("dbo.TaskCompletionQueues", new[] { "Process_ProcessId" });

            RenameTable("dbo.WorkflowProcesses", "ExternalSystems");

            RenameColumn(table: "dbo.ExternalSystems", name: "ProcessId", newName: "ExternalSystemId");
            RenameColumn(table: "dbo.TaskTypes", name: "Process_ProcessId", newName: "ExternalSystem_ExternalSystemId");
            RenameColumn(table: "dbo.Tickets", name: "Process_ProcessId", newName: "ExternalSystem_ExternalSystemId");
            RenameColumn(table: "dbo.TaskCompletionQueues", name: "Process_ProcessId", newName: "ExternalSystem_ExternalSystemId");
            RenameColumn(table: "dbo.TaskCompletionQueues", name: "ProcessOperationName", newName: "ExternalSystemOperationName");

            AddForeignKey("dbo.TaskTypes", "ExternalSystem_ExternalSystemId", "dbo.ExternalSystems", "ExternalSystemId");
            AddForeignKey("dbo.Tickets", "ExternalSystem_ExternalSystemId", "dbo.ExternalSystems", "ExternalSystemId");
            AddForeignKey("dbo.TaskCompletionQueues", "ExternalSystem_ExternalSystemId", "dbo.ExternalSystems", "ExternalSystemId");

            CreateIndex("dbo.TaskTypes", "ExternalSystem_ExternalSystemId");
            CreateIndex("dbo.Tickets", "ExternalSystem_ExternalSystemId");
            CreateIndex("dbo.TaskCompletionQueues", "ExternalSystem_ExternalSystemId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.TaskCompletionQueues", new[] { "ExternalSystem_ExternalSystemId" });
            DropIndex("dbo.Tickets", new[] { "ExternalSystem_ExternalSystemId" });
            DropIndex("dbo.TaskTypes", new[] { "ExternalSystem_ExternalSystemId" });

            DropForeignKey("dbo.TaskCompletionQueues", "ExternalSystem_ExternalSystemId", "dbo.ExternalSystems");
            DropForeignKey("dbo.Tickets", "ExternalSystem_ExternalSystemId", "dbo.ExternalSystems");
            DropForeignKey("dbo.TaskTypes", "ExternalSystem_ExternalSystemId", "dbo.ExternalSystems");

            RenameTable("dbo.ExternalSystems", "WorkflowProcesses");

            RenameColumn(table: "dbo.WorkflowProcesses", name: "ExternalSystemId", newName: "ProcessId");
            RenameColumn(table: "dbo.TaskCompletionQueues", name: "ExternalSystem_ExternalSystemId", newName: "Process_ProcessId");
            RenameColumn(table: "dbo.Tickets", name: "ExternalSystem_ExternalSystemId", newName: "Process_ProcessId");
            RenameColumn(table: "dbo.TaskTypes", name: "ExternalSystem_ExternalSystemId", newName: "Process_ProcessId");
            RenameColumn(table: "dbo.TaskCompletionQueues", name: "ExternalSystemOperationName", newName: "ProcessOperationName");

            CreateIndex("dbo.TaskCompletionQueues", "Process_ProcessId");
            CreateIndex("dbo.Tickets", "Process_ProcessId");
            CreateIndex("dbo.TaskTypes", "Process_ProcessId");

            AddForeignKey("dbo.TaskCompletionQueues", "Process_ProcessId", "dbo.WorkflowProcesses", "ProcessId");
            AddForeignKey("dbo.Tickets", "Process_ProcessId", "dbo.WorkflowProcesses", "ProcessId");
            AddForeignKey("dbo.TaskTypes", "Process_ProcessId", "dbo.WorkflowProcesses", "ProcessId");
        }
    }
}
