namespace TicketTool.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _208 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "IsSystemUser", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "IsSystemUser");
        }
    }
}
