namespace TicketTool.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v13 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.StoreUsers", "Store_SapCode", "dbo.Stores");
            DropForeignKey("dbo.StoreUsers", "User_UserId", "dbo.Users");
            //DropIndex("dbo.StoreUsers", new[] {"Store_SapCode"});
            //DropIndex("dbo.StoreUsers", new[] { "User_UserId" });
            RenameTable("dbo.StoreUsers", "ParameterUsers");
            RenameTable("dbo.Stores", "Parameters");
            RenameColumn("dbo.Parameters", "SapCode", "ParameterName");
            RenameColumn("dbo.ParameterUsers", "Store_SapCode", "Parameter_ParameterName");
            AddForeignKey("dbo.ParameterUsers", "User_UserId", "dbo.Users", "UserId", cascadeDelete: true);
            AddForeignKey("dbo.ParameterUsers", "Parameter_ParameterName", "dbo.Parameters", "ParameterName", cascadeDelete: true);
            CreateIndex("dbo.ParameterUsers", "User_UserId");
            CreateIndex("dbo.ParameterUsers", "Parameter_ParameterName");
        }
        
        public override void Down()
        {            
            DropIndex("dbo.ParameterUsers", new[] { "User_UserId" });
            DropIndex("dbo.ParameterUsers", new[] { "Parameter_SapCode" });
            DropForeignKey("dbo.ParameterUsers", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.ParameterUsers", "Parameter_SapCode", "dbo.Parameters");
            RenameTable("dbo.ParameterUsers", "StoreUsers");
            RenameTable("dbo.Parameters", "Stores");
            CreateIndex("dbo.StoreUsers", "User_UserId");
            CreateIndex("dbo.StoreUsers", "Store_SapCode");
            AddForeignKey("dbo.StoreUsers", "User_UserId", "dbo.Users", "UserId", cascadeDelete: true);
            AddForeignKey("dbo.StoreUsers", "Store_SapCode", "dbo.Stores", "SapCode", cascadeDelete: true);
        }
    }
}
