using TicketTool.Dal.Model.Design;

namespace TicketTool.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TicketTool_V11 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProcessMessages",
                c => new
                    {
                        MessageId = c.Long(nullable: false, identity: true),
                        Message = c.String(maxLength: 255),
                        SerialazedData = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        Ticket_TicketId = c.String(maxLength: 36),
                    })
                .PrimaryKey(t => t.MessageId)
                .ForeignKey("dbo.Tickets", t => t.Ticket_TicketId)
                .Index(t => t.Ticket_TicketId);
            
            AddColumn("dbo.SkillSets", "IsDeleted", c => c.Boolean(nullable: false));

            //Apply custom scripts                        
            foreach (var script in QueryProvider.GetDeploymentScripts_V1_1())
            {
                Sql(script);
            }
        }
        
        public override void Down()
        {
            DropIndex("dbo.ProcessMessages", new[] { "Ticket_TicketId" });
            DropForeignKey("dbo.ProcessMessages", "Ticket_TicketId", "dbo.Tickets");
            DropColumn("dbo.SkillSets", "IsDeleted");
            DropTable("dbo.ProcessMessages");
        }
    }
}
