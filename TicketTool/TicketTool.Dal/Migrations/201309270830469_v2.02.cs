namespace TicketTool.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v202 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TaskTypes", "WorkitemPageUrl", c => c.String(maxLength: 4000));
            AddColumn("dbo.TaskTypes", "TaskPageUrl", c => c.String(maxLength: 4000));
            AddColumn("dbo.ExternalSystems", "WsdlUrl", c => c.String(maxLength: 4000));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ExternalSystems", "WsdlUrl");
            DropColumn("dbo.TaskTypes", "TaskPageUrl");
            DropColumn("dbo.TaskTypes", "WorkitemPageUrl");
        }
    }
}
