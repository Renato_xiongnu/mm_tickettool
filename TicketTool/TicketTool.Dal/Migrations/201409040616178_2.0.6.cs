namespace TicketTool.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _206 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clusters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ClusterUsers",
                c => new
                    {
                        Cluster_Id = c.Int(nullable: false),
                        User_UserId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.Cluster_Id, t.User_UserId })
                .ForeignKey("dbo.Clusters", t => t.Cluster_Id, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.User_UserId, cascadeDelete: true)
                .Index(t => t.Cluster_Id)
                .Index(t => t.User_UserId);
            
            AddColumn("dbo.SkillSets", "IsClustered", c => c.Boolean(nullable: false));
            AddColumn("dbo.Parameters", "Cluster_Id", c => c.Int());
            AddForeignKey("dbo.Parameters", "Cluster_Id", "dbo.Clusters", "Id");
            CreateIndex("dbo.Parameters", "Cluster_Id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.ClusterUsers", new[] { "User_UserId" });
            DropIndex("dbo.ClusterUsers", new[] { "Cluster_Id" });
            DropIndex("dbo.Parameters", new[] { "Cluster_Id" });
            DropForeignKey("dbo.ClusterUsers", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.ClusterUsers", "Cluster_Id", "dbo.Clusters");
            DropForeignKey("dbo.Parameters", "Cluster_Id", "dbo.Clusters");
            DropColumn("dbo.Parameters", "Cluster_Id");
            DropColumn("dbo.SkillSets", "IsClustered");
            DropTable("dbo.ClusterUsers");
            DropTable("dbo.Clusters");
        }
    }
}
