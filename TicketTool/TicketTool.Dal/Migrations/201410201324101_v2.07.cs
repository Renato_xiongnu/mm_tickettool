namespace TicketTool.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v207 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FieldTypes", "Hash", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FieldTypes", "Hash");
        }
    }
}
