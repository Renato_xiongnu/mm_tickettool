// <auto-generated />
namespace TicketTool.Dal.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class TicketTool_V11 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(TicketTool_V11));
        
        string IMigrationMetadata.Id
        {
            get { return "201303042205270_TicketTool_V1.1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
