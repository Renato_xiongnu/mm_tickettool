namespace TicketTool.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v204 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserActions",
                c => new
                    {
                        UserActionId = c.Long(nullable: false, identity: true),
                        ActionTime = c.DateTime(nullable: false),
                        ActionType = c.Int(nullable: false),
                        UserLog_UserLogId = c.Long(nullable: false),
                        Task_TaskId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.UserActionId)
                .ForeignKey("dbo.UserLogs", t => t.UserLog_UserLogId, cascadeDelete: true)
                .ForeignKey("dbo.TicketTasks", t => t.Task_TaskId, cascadeDelete: true)
                .Index(t => t.UserLog_UserLogId)
                .Index(t => t.Task_TaskId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.UserActions", new[] { "Task_TaskId" });
            DropIndex("dbo.UserActions", new[] { "UserLog_UserLogId" });
            DropForeignKey("dbo.UserActions", "Task_TaskId", "dbo.TicketTasks");
            DropForeignKey("dbo.UserActions", "UserLog_UserLogId", "dbo.UserLogs");
            DropTable("dbo.UserActions");
        }
    }
}
