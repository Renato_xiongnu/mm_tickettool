using TicketTool.Dal.Model.Design;

namespace TicketTool.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TicketTool_V12 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserAvailableSkillSets",
                c => new
                    {
                        UserId = c.Long(nullable: false),
                        SkillSetId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.SkillSetId })
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.SkillSets", t => t.SkillSetId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.SkillSetId);
            
            CreateTable(
                "dbo.RoleSkillSets",
                c => new
                    {
                        Role_RoleId = c.Long(nullable: false),
                        SkillSet_SkillSetId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.Role_RoleId, t.SkillSet_SkillSetId })
                .ForeignKey("dbo.Roles", t => t.Role_RoleId, cascadeDelete: true)
                .ForeignKey("dbo.SkillSets", t => t.SkillSet_SkillSetId, cascadeDelete: true)
                .Index(t => t.Role_RoleId)
                .Index(t => t.SkillSet_SkillSetId);
            
            AddColumn("dbo.Stores", "TimeZoneOffset", c => c.String(maxLength: 8));
            AddColumn("dbo.Stores", "StartWorkingTime", c => c.Time(nullable: false));
            AddColumn("dbo.Stores", "EndWorkingTime", c => c.Time(nullable: false));
            AddColumn("dbo.Stores", "UtcStartWorkingTime", c => c.Int(nullable: false));
            AddColumn("dbo.Stores", "UtcEndWorkingTime", c => c.Int(nullable: false));
            AddColumn("dbo.Stores", "CanSynchronize", c => c.Boolean(nullable: false));
            AddColumn("dbo.Roles", "NeedNotification", c => c.Boolean(nullable: false));

            //Apply custom scripts                        
            foreach (var script in QueryProvider.GetDeploymentScripts_V1_2())
            {
                Sql(script);
            }
        }
        
        public override void Down()
        {
            DropIndex("dbo.RoleSkillSets", new[] { "SkillSet_SkillSetId" });
            DropIndex("dbo.RoleSkillSets", new[] { "Role_RoleId" });
            DropIndex("dbo.UserAvailableSkillSets", new[] { "SkillSetId" });
            DropIndex("dbo.UserAvailableSkillSets", new[] { "UserId" });
            DropForeignKey("dbo.RoleSkillSets", "SkillSet_SkillSetId", "dbo.SkillSets");
            DropForeignKey("dbo.RoleSkillSets", "Role_RoleId", "dbo.Roles");
            DropForeignKey("dbo.UserAvailableSkillSets", "SkillSetId", "dbo.SkillSets");
            DropForeignKey("dbo.UserAvailableSkillSets", "UserId", "dbo.Users");
            DropColumn("dbo.Stores", "CanSynchronize");
            DropColumn("dbo.Stores", "UtcEndWorkingTime");
            DropColumn("dbo.Stores", "UtcStartWorkingTime");
            DropColumn("dbo.Stores", "EndWorkingTime");
            DropColumn("dbo.Stores", "StartWorkingTime");
            DropColumn("dbo.Stores", "TimeZoneOffset");
            DropTable("dbo.RoleSkillSets");
            DropTable("dbo.UserAvailableSkillSets");
            DropColumn("dbo.Roles", "NeedNotification");
        }
    }
}
