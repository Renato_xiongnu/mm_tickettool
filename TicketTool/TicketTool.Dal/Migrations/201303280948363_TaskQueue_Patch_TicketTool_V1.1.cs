namespace TicketTool.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TaskQueue_Patch_TicketTool_V11 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.NotificationLogGroups",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(maxLength: 255),
                        IsDefault = c.Boolean(nullable: false),
                        DefaultRecipientEmail = c.String(nullable: false, maxLength: 4000),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TaskCompletionQueues",
                c => new
                    {
                        TaskQueueId = c.Long(nullable: false, identity: true),
                        InvocationMarker = c.String(maxLength: 36),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        FinishDate = c.DateTime(),
                        ProcessOperationName = c.String(nullable: false, maxLength: 4000),
                        Outcome = c.String(nullable: false, maxLength: 255),
                        SerializedTask = c.String(nullable: false),
                        CompletionStatus = c.Int(nullable: false),
                        Process_ProcessId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.TaskQueueId)
                .ForeignKey("dbo.WorkflowProcesses", t => t.Process_ProcessId)
                .Index(t => t.Process_ProcessId);
            
            CreateTable(
                "dbo.TaskCompletionQueueLogs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CreateDate = c.DateTime(nullable: false),
                        FromStatus = c.Int(nullable: false),
                        ToStatus = c.Int(nullable: false),
                        HasError = c.Boolean(nullable: false),
                        Message = c.String(),
                        InvocationMarker = c.String(maxLength: 36),
                        TaskCompletionQueue_TaskQueueId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TaskCompletionQueues", t => t.TaskCompletionQueue_TaskQueueId)
                .Index(t => t.TaskCompletionQueue_TaskQueueId);
            
            CreateTable(
                "dbo.NotificationLogRecords",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        NotificationRecType = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        InvocationMarker = c.String(maxLength: 36),
                        RefId = c.String(maxLength: 36),
                        SourceName = c.String(maxLength: 255),
                        Text = c.String(nullable: false),
                        NotificationGroup_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NotificationLogGroups", t => t.NotificationGroup_Id, cascadeDelete: true)
                .Index(t => t.NotificationGroup_Id);
            
            CreateTable(
                "dbo.NotificationLogGroupUsers",
                c => new
                    {
                        NotificationLogGroup_Id = c.Long(nullable: false),
                        User_UserId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.NotificationLogGroup_Id, t.User_UserId })
                .ForeignKey("dbo.NotificationLogGroups", t => t.NotificationLogGroup_Id, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.User_UserId, cascadeDelete: true)
                .Index(t => t.NotificationLogGroup_Id)
                .Index(t => t.User_UserId);
            
            AddColumn("dbo.TaskTypes", "CanPostponeCallingWorkflow", c => c.Boolean(nullable: false));
            AddColumn("dbo.TaskTypes", "NotificationGroup_Id", c => c.Long());
            AddColumn("dbo.TicketTasks", "TaskCompletionQueue_TaskQueueId", c => c.Long());
            AddForeignKey("dbo.TaskTypes", "NotificationGroup_Id", "dbo.NotificationLogGroups", "Id");
            AddForeignKey("dbo.TicketTasks", "TaskCompletionQueue_TaskQueueId", "dbo.TaskCompletionQueues", "TaskQueueId");
            CreateIndex("dbo.TaskTypes", "NotificationGroup_Id");
            CreateIndex("dbo.TicketTasks", "TaskCompletionQueue_TaskQueueId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.NotificationLogGroupUsers", new[] { "User_UserId" });
            DropIndex("dbo.NotificationLogGroupUsers", new[] { "NotificationLogGroup_Id" });
            DropIndex("dbo.NotificationLogRecords", new[] { "NotificationGroup_Id" });
            DropIndex("dbo.TaskCompletionQueueLogs", new[] { "TaskCompletionQueue_TaskQueueId" });
            DropIndex("dbo.TaskCompletionQueues", new[] { "Process_ProcessId" });
            DropIndex("dbo.TicketTasks", new[] { "TaskCompletionQueue_TaskQueueId" });
            DropIndex("dbo.TaskTypes", new[] { "NotificationGroup_Id" });
            DropForeignKey("dbo.NotificationLogGroupUsers", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.NotificationLogGroupUsers", "NotificationLogGroup_Id", "dbo.NotificationLogGroups");
            DropForeignKey("dbo.NotificationLogRecords", "NotificationGroup_Id", "dbo.NotificationLogGroups");
            DropForeignKey("dbo.TaskCompletionQueueLogs", "TaskCompletionQueue_TaskQueueId", "dbo.TaskCompletionQueues");
            DropForeignKey("dbo.TaskCompletionQueues", "Process_ProcessId", "dbo.WorkflowProcesses");
            DropForeignKey("dbo.TicketTasks", "TaskCompletionQueue_TaskQueueId", "dbo.TaskCompletionQueues");
            DropForeignKey("dbo.TaskTypes", "NotificationGroup_Id", "dbo.NotificationLogGroups");
            DropColumn("dbo.TicketTasks", "TaskCompletionQueue_TaskQueueId");
            DropColumn("dbo.TaskTypes", "NotificationGroup_Id");
            DropColumn("dbo.TaskTypes", "CanPostponeCallingWorkflow");
            DropTable("dbo.NotificationLogGroupUsers");
            DropTable("dbo.NotificationLogRecords");
            DropTable("dbo.TaskCompletionQueueLogs");
            DropTable("dbo.TaskCompletionQueues");
            DropTable("dbo.NotificationLogGroups");
        }
    }
}
