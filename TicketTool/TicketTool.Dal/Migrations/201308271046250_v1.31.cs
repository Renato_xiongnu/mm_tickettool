namespace TicketTool.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v131 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProcessMessages", "Ticket_TicketId", "dbo.Tickets");
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        EventId = c.Long(nullable: false, identity: true),
                        Message = c.String(maxLength: 255),
                        SerialazedData = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        Ticket_TicketId = c.String(maxLength: 36),
                    })
                .PrimaryKey(t => t.EventId)
                .ForeignKey("dbo.Tickets", t => t.Ticket_TicketId)
                .Index(t => t.Ticket_TicketId);
            
            AddColumn("dbo.Parameters", "Description", c => c.String());
            DropTable("dbo.ProcessMessages");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ProcessMessages",
                c => new
                    {
                        MessageId = c.Long(nullable: false, identity: true),
                        Message = c.String(maxLength: 255),
                        SerialazedData = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        Ticket_TicketId = c.String(maxLength: 36),
                    })
                .PrimaryKey(t => t.MessageId);
                        
            DropIndex("dbo.Events", new[] { "Ticket_TicketId" });
            DropForeignKey("dbo.Events", "Ticket_TicketId", "dbo.Tickets");
            DropColumn("dbo.Parameters", "Description");
            DropTable("dbo.Events");
            CreateIndex("dbo.ProcessMessages", "Ticket_TicketId");
            AddForeignKey("dbo.ProcessMessages", "Ticket_TicketId", "dbo.Tickets", "TicketId");
        }
    }
}
