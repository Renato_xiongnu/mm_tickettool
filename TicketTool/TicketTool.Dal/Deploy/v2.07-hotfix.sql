﻿SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[SP_GetNextTask]	
	 @UserId BIGINT,
	 @SkillSetId BIGINT,
	 @StartStatus INT,
	 @EndStatus INT,	 
	 @MaxStatus INT,
	 @SecondStatus INT,
	 @CurrentUtcTime INT
AS
BEGIN
	SET NOCOUNT ON;
	
	
	DECLARE @TaskId BIGINT;
	DECLARE @IsParametaraseble BIT;
	DECLARE @IsClustered BIT;
	SELECT TOP 1 @IsParametaraseble = s.IsParameterizable from dbo.SkillSets s
		where s.SkillSetId = @SkillSetId
	SELECT TOP 1 @IsClustered = s.IsClustered from dbo.SkillSets s
		where s.SkillSetId = @SkillSetId
			
		
	IF @IsParametaraseble = 0 AND @IsClustered=0
	BEGIN
		SELECT TOP 1 @TaskId = TaskId FROM
			(
		    SELECT task.TaskId, task.CreateDate, relInfo.IsWorkingHours from [dbo].[TicketTasks] task WITH (READPAST, updlock, ROWLOCK)						
			INNER JOIN [dbo].[UserSkillSets] userSkillSet on userSkillSet.SkillSet_SkillSetId = task.CurrentSkillSet_SkillSetId 
			INNER JOIN [dbo].[GetOperationGroup](@StartStatus, @EndStatus) opGroup on task.TaskId = opGroup.TaskId
			INNER JOIN [dbo].GetWokingHoursOfStore(@CurrentUtcTime) relInfo on relInfo.TicketId = task.Ticket_TicketId
			where task.TaskStatus < @MaxStatus AND userSkillSet.SkillSet_SkillSetId = @SkillSetId AND userSkillSet.User_UserId = @UserId AND task.CurrentAssignee_UserId is NULL AND DiffDate is NULL
			AND (task.TaskStatus <> @SecondStatus OR ReturnToQueueAfterDate IS NULL) 
													 -- OR (task.UpdateDate < @FromDate) )
			) as taskQuery
			order by taskQuery.IsWorkingHours desc, taskQuery.CreateDate asc
	END		
	ELSE IF @IsParametaraseble=1 AND @IsClustered=0
	BEGIN
		SELECT top 1 @TaskId = TaskId FROM (
		SELECT task.TaskId, task.CreateDate, relInfo.IsWorkingHours  from [dbo].[TicketTasks] task WITH (READPAST, updlock, ROWLOCK)
			INNER JOIN [dbo].[UserSkillSets] userSkillSet on userSkillSet.SkillSet_SkillSetId = task.CurrentSkillSet_SkillSetId
			INNER JOIN [dbo].[GetOperationGroup](@StartStatus, @EndStatus) opGroup on task.TaskId = opGroup.TaskId
			INNER JOIN [dbo].GetWokingHoursOfStore(@CurrentUtcTime) relInfo on relInfo.TicketId = task.Ticket_TicketId
			where task.TaskStatus < @MaxStatus AND userSkillSet.SkillSet_SkillSetId = @SkillSetId AND userSkillSet.User_UserId = @UserId AND task.CurrentAssignee_UserId is NULL AND DiffDate is NULL
			AND (task.TaskStatus <> @SecondStatus OR ReturnToQueueAfterDate IS NULL) -- OR (task.UpdateDate < @FromDate) )			
			AND EXISTS(SELECT 1 from dbo.ParameterUsers susr where susr.User_UserId = @UserId AND susr.Parameter_ParameterName = relInfo.ParameterName)
			) as taskQuery 
			order by taskQuery.IsWorkingHours desc, taskQuery.CreateDate asc
	END
	ELSE IF @IsParametaraseble=0 AND @IsClustered=1
	BEGIN
		SELECT top 1 @TaskId = TaskId FROM (
		SELECT task.TaskId, task.CreateDate, relInfo.IsWorkingHours  from [dbo].[TicketTasks] task WITH (READPAST, updlock, ROWLOCK)
			INNER JOIN [dbo].[UserSkillSets] userSkillSet on userSkillSet.SkillSet_SkillSetId = task.CurrentSkillSet_SkillSetId
			INNER JOIN [dbo].[ClusterUsers] clusterUsers on clusterUsers.User_UserId=@UserId
			INNER JOIN [dbo].[Parameters] parameter on parameter.Cluster_Id=clusterUsers.Cluster_Id AND Task.Parameter = parameter.ParameterName
			INNER JOIN [dbo].[GetOperationGroup](@StartStatus, @EndStatus) opGroup on task.TaskId = opGroup.TaskId
			INNER JOIN [dbo].GetWokingHoursOfStore(@CurrentUtcTime) relInfo on relInfo.TicketId = task.Ticket_TicketId
			where task.TaskStatus < @MaxStatus AND userSkillSet.SkillSet_SkillSetId = @SkillSetId AND userSkillSet.User_UserId = @UserId AND task.CurrentAssignee_UserId is NULL --AND DiffDate is NULL
			AND (task.TaskStatus <> @SecondStatus OR ReturnToQueueAfterDate IS NULL)
			) -- OR (task.UpdateDate < @FromDate) )			
			as taskQuery 
			order by taskQuery.IsWorkingHours desc, taskQuery.CreateDate asc
	END
	ELSE
	BEGIN
	    RAISERROR(N'Both IsParametaraseble and IsClustered skilsets not supported',15,1)
	END
		SELECT @TaskId
		
					
END
GO


