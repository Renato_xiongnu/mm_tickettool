﻿namespace TicketTool.Dal.Model
{
    public enum TaskStatus
    {
        None = 0,
        Created = 10,
        AssignedToSkillSet = 20,        
        AssignedToUser = 30,
        RemovedFromUser = 40,
        RemovedFromSkillSet = 50,
        Escalated = 60,
        Finished = 100,
        Cancel = 110,
    }

    public enum ContentRightType
    {
        None,
        Readonly,
        Edit        
    }


    public enum TaskCompletionStatus
    {
        None = 0,
        Created = 10,        
        Finished = 100,
        Canceled = 110
    }

    public  enum NotificationRecType
    {
        Info,
        Warning,
        Error
    }
}
