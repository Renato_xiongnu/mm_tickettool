﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicketTool.Dal.Model.Design;

namespace TicketTool.Dal.Model
{
    public class NotificationLogGroup
    {
        [Key]
        public long Id { get; set; } //TODO: add Reference to SkillSet or TaskType table

        [MaxLength(QueryProvider.StringLength)]
        public string Name { get; set; }

        public bool IsDefault { get; set; }

        [Required]
        [MaxLength(QueryProvider.DescriptionLength)]
        public string DefaultRecipientEmail { get; set; }

        public virtual List<User> RecipientsList { get; set; }

    }
}
