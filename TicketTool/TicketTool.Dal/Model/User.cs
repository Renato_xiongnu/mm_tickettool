﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TicketTool.Dal.Model.Design;

namespace TicketTool.Dal.Model
{    
    public class User
    {
        [Key]
        public long UserId { get; set; }

        [Required]
        [MaxLength(QueryProvider.StringLength)]
        public string Name { get; set; }

        [MaxLength(QueryProvider.DescriptionLength)]
        public string Email { get; set; }

        [Required]
        [MaxLength(QueryProvider.DescriptionLength)]
        public string Password { get; set; }
        
        public virtual List<Parameter> Parameters  { get; set; }

        public virtual List<SkillSet> SkillSets { get; set; }

        public virtual List<SkillSet> AvailableSkillSets { get; set; }

        public virtual List<Cluster> Clusters { get; set; } 

        public virtual List<NotificationLogGroup> NotificationLogGroup { get; set; }

        public virtual List<Role> Roles { get; set; }

        public DateTime LastPingDateTime { get; set; }

        public bool IsLocked { get; set; }

        /// <summary>
        /// Если true, то учетку не вылогинивыет.
        /// Нужно для зингаи, учеток из процесса и т.д.
        /// </summary>
        public bool IsSystemUser { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime LastLoginDate { get; set; }
    }
}
