﻿using TicketTool.Dal.Model.Design;

namespace TicketTool.Dal.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class Ticket
    {
        [Key,  StringLength(QueryProvider.GuidLength)]
        public string TicketId { get; set; }

        [MaxLength(QueryProvider.StringLength)]
        public string Parameter { get; set; }

        [MaxLength(QueryProvider.StringLength)]
        public string WorkItemId { get; set; }

        [MaxLength(QueryProvider.DescriptionLength)]
        public string Name { get; set; }

        public ExternalSystem ExternalSystem { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime UpdateDate { get; set; }

        public DateTime? CloseDate { get; set; }


    }
}
