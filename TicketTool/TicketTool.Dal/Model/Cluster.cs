﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TicketTool.Dal.Model.Design;

namespace TicketTool.Dal.Model
{
    public class Cluster
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(QueryProvider.StringLength)]
        public string Name { get; set; }

        public virtual List<Parameter> Parameters { get; set; }

        public virtual List<User> Users { get; set; } 
    }
}
