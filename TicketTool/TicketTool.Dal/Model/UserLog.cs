﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TicketTool.Dal.Model
{
    public class UserLog
    {
        [Key]
        public long UserLogId { get; set; }
        [Required]
        public User User { get; set; }
        [Required]
        public SkillSet SkillSet { get; set; }
        [Required]
        public DateTime LoginDate { get; set; }
        public DateTime? LogoutDate { get; set; }
        public int AbandonedQty { get; set; }
        public int ProcessedQty { get; set; }
        public List<UserAction> UserActions { get; set; }
    }
}
