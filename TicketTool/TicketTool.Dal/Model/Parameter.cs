﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TicketTool.Dal.Model.Design;

namespace TicketTool.Dal.Model
{
    public class Parameter
    {
        [Key, StringLength(QueryProvider.SapCodeLength)]
        public string ParameterName { get; set; }

        [Required]
        [MaxLength(QueryProvider.StringLength)]
        public string Name { get; set; }

        [Required]
        [MaxLength(QueryProvider.StringLength)]
        public string TimeZone { get; set; }

        [MaxLength(8)]
        public string TimeZoneOffset { get; set; }

        public TimeSpan StartWorkingTime { get; set; }

        public TimeSpan EndWorkingTime { get; set; }

        public int UtcStartWorkingTime { get; set; }

        public int UtcEndWorkingTime { get; set; }

        public virtual List<User> Users { get; set; }

        public bool CanSynchronize { get; set; }

        public string Description { get; set; }

        public Cluster Cluster { get; set; }

        public override string ToString()
        {
            return string.Format("{0} - {1}", ParameterName, Name);
        }
    }
}
