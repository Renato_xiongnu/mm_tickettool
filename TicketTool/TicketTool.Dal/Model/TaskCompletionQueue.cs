﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TicketTool.Dal.Model.Design;

namespace TicketTool.Dal.Model
{
    public class TaskCompletionQueue
    {
        [Key]
        public long TaskQueueId { get; set; }
        
        [MaxLength(QueryProvider.GuidLength)]
        public string InvocationMarker { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime UpdateDate { get; set; }

        public DateTime? FinishDate { get; set; }

        public ExternalSystem ExternalSystem { get; set; }

        [Required]
        [MaxLength(QueryProvider.DescriptionLength)]     
        public string ExternalSystemOperationName { get; set; }

        [Required]
        [MaxLength(QueryProvider.StringLength)]     
        public string Outcome { get; set; }

        [Required]
        public string SerializedTask { get; set; }

        public int CompletionStatus { get; set; }

        public virtual List<TaskCompletionQueueLog> QueueLogs { get; set; }
    }
}
