﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using TicketTool.Dal.Model.Design;

namespace TicketTool.Dal.Model
{
    using System;
    using System.ComponentModel.DataAnnotations;
    
    public class TicketTask
    {        
        [Key]
        public long TaskId { get; set; }
        
        public virtual Ticket Ticket { get; set; }

        public TaskType TaskType { get; set; }

        [MaxLength(QueryProvider.StringLength)]
        public string Name { get; set; }

        //[MaxLength(QueryProvider.DescriptionLength)]
        public string Description { get; set; }

        [MaxLength(QueryProvider.StringLength)]              
        public string CurrentOutcome { get; set; }

        //[MaxLength(QueryProvider.DescriptionLength)]
        public string Comment { get; set; }      

        public DateTime CreateDate { get; set; }

        public DateTime UpdateDate { get; set; }

        public DateTime? FinishDate { get; set; }

        public int TaskStatus { get; set; }

        public virtual Outcome Outcomes { get; set; }

        public virtual List<TaskField> TaskFields { get; set; }

        public virtual List<TaskOperation> OperationLog { get; set; } 

        public bool Escalated { get; set; }

        public virtual User InformedUser { get; set; }

        public virtual User CurrentAssignee { get; set; }

        public virtual User Performer { get; set; }
        
        public SkillSet CurrentSkillSet { get; set; }                      
        
        public DateTime? SkillSetDateAssigned { get; set; }

        public TimeSpan? SkillSetRuntime { get; set; }        

        public DateTime? UserDateAssigned { get; set; }

        public DateTime? ReturnToQueueAfterDate { get; set; }

        public TimeSpan? UserRuntime { get; set; }

        public TaskOperation LastUserOperation { get; set; }
      
        public TaskOperation LastSkillSetOperation { get; set; }
        
        [MaxLength(QueryProvider.StringLength)]
        public string Parameter { get; set; }

        public string RelatedContent { get; set; }

        [MaxLength(QueryProvider.StringLength)]
        public string RelatedContentName { get; set; }

        public ContentRightType RelatedContentRight { get; set; }
                
        public virtual TaskCompletionQueue TaskCompletionQueue { get; set; }

        [NotMapped]
        public bool IsOldTask { get; set; }

        [NotMapped]
		//
		// Property for Monitoring UI
		// in context should be Included OperationLog & OperationLog.User for better perfomance
        public string CurrentOrPreviousWorker
        {
            get
            {
                if (CurrentAssignee != null)
                {
                    return CurrentAssignee.Name;
                }
                else
                {
                    if (OperationLog != null)
                    {
                        var log = OperationLog.Where(o => o.User != null).OrderBy(l => l.UpdateDate).FirstOrDefault();
                        if (log != null && log.User != null)
                        {
                            return log.User.Name;
                        }
                    }
                }
                return null;
            }
        }
    }
}
