﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicketTool.Dal.Model.Design;

namespace TicketTool.Dal.Model
{
    public class TaskCompletionQueueLog
    {
        [Key]
        public long Id { get; set; }

        public DateTime CreateDate { get; set; }

        public int FromStatus { get; set; }

        public int ToStatus { get; set; }

        public bool HasError { get; set; }

        public string Message { get; set; }

        [MaxLength(QueryProvider.GuidLength)]
        public string InvocationMarker { get; set; }
    }
}
