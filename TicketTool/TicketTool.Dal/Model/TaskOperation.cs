﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicketTool.Dal.Model.Design;

namespace TicketTool.Dal.Model
{
    public class TaskOperation
    {
        [Key]
        public long OperationId { get; set; }

        public TicketTask TicketTask { get; set; }

        //public string TicketTaskId { get; set; }
        
        public int FromStatus { get; set; }

        public int ToStatus { get; set; }

        public DateTime UpdateDate { get; set; }        

        public bool HasError { get; set; }

        [MaxLength(QueryProvider.DescriptionLength)]
        public string MessageText { get; set; }


        //public long RefOperionId { get; set; }
        public virtual TaskOperation RefOperation { get; set; }

        public SkillSet SkillSet { get; set; }

        public DateTime? SkillSetDateAssigned { get; set; }

        public TimeSpan? SkillSetRuntime { get; set; }

        public User User { get; set; }

        public DateTime? UserDateAssigned { get; set; }

        public TimeSpan? UserRuntime { get; set; }

    }
}
