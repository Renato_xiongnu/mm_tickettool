﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace TicketTool.Dal.Model
{
    public class Outcome
    {
        [Key]
        public long Id { get; set; }

        [Required]
        public string Data { get; set; }
    }
}