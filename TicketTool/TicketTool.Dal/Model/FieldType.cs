﻿using System;
using System.Collections;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TicketTool.Dal.Model.Design;

namespace TicketTool.Dal.Model
{
    public static class FieldTypeUtils
    {
        public static object[] StringsToArray(string data)
        {
            return data
                .Split(new[] {Environment.NewLine, "\n", "\r"}, StringSplitOptions.RemoveEmptyEntries)
                .Select(i => (object) i).ToArray();
        }

        public static string ArrayToStrings(object[] data)
        {
            return (data == null || data.Length == 0)
                                       ? null
                                       : string.Join(Environment.NewLine, data);
        }
    }

    public class FieldType : IEqualityComparer<FieldType>
    {
        [Key]
        public long FieldTypeId { get; set; }

        [Required]
        [MaxLength(QueryProvider.StringLength)]
        public string Type { get; set; }

        [MaxLength(QueryProvider.StringLength)]
        public string Name { get; set; }

        public bool IsMandatory { get; set; } //default true

        public string DefaultValue { get; set; }

        [MaxLength(QueryProvider.DescriptionLength)]
        public string PredefinedValuesList { get; set; }

        public int Hash { get; set; }
        
        public bool Equals(FieldType x, FieldType y)
        {
            return x.Name.Equals(y.Name) && x.Type.Equals(y.Type) && x.PredefinedValuesList.Equals(y.PredefinedValuesList);
        }

        public int GetHashCode(FieldType obj)
        {
            return String.Concat(obj.Name, obj.Type, obj.PredefinedValuesList).GetHashCode();
        }

        public static IEqualityComparer<FieldType> DefaultComparer { get { return new FieldType(); } }
    }
}
