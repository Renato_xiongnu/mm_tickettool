﻿using System.ComponentModel.DataAnnotations;


namespace TicketTool.Dal.Model
{    
    public class TaskField
    {
        [Key]
        public long TaskFieldId { get; set; }

        public FieldType FieldType { get; set; }

        public string Value { get; set; }
    }
}
