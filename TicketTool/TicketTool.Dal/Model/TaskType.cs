﻿using System;
using System.ComponentModel.DataAnnotations;
using TicketTool.Dal.Model.Design;

namespace TicketTool.Dal.Model
{    
    public class TaskType
    {
        [Key]
        public long TaskTypeId { get; set; }

        public ExternalSystem ExternalSystem { get; set; }

        [Required]
        [MaxLength(QueryProvider.StringLength)]
        public string Name { get; set; }

        public virtual SkillSet SkillSet { get; set; }

        #region SLA        
        
        public DateTime? SlaControlDate { get; set; }

        [MaxLength(QueryProvider.StringLength)]
        public string SlaName { get; set; }    
        
        public virtual SkillSet SlaSkillSet { get; set; }        

        public TimeSpan? MaxSkillSetRunTime { get; set; }

        public TimeSpan? MaxUserRuntime { get; set; }

        #endregion

        public NotificationLogGroup NotificationGroup { get; set; }

        public bool CanPostponeCallingWorkflow { get; set; }

        [MaxLength(QueryProvider.DescriptionLength)]
        public string WorkitemPageUrl { get; set; }

        [MaxLength(QueryProvider.DescriptionLength)]
        public string TaskPageUrl { get; set; }
    }
}
