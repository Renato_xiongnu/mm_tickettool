﻿using System.ComponentModel.DataAnnotations;
using TicketTool.Dal.Model.Design;

namespace TicketTool.Dal.Model
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public class SkillSet
    {
        public long SkillSetId { get; set; }

        [Required]
        [MaxLength(QueryProvider.StringLength)]
        public string Name { get; set; }

        public bool IsDefault { get; set; }

        public bool IsParameterizable { get; set; }
        
        public bool DeliveryTaskByEmail { get; set; }

        public bool IsEscalation { get; set; }

        public virtual List<User> Users { get; set; }

        public virtual List<User> AvailableUsers { get; set; }

        public virtual List<TaskType> TaskTypes { get; set; }

        public List<Role> Roles { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsClustered { get; set; }
    }
}
