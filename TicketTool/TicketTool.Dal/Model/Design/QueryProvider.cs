﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TicketTool.Dal.Model.Design
{
    public static class QueryProvider
    {
        public const int SapCodeLength = 4;
        public const int GuidLength = 36;
        public const int StringLength = 255;
        public const int DescriptionLength = 4000;

        private static string GetQuery(System.Reflection.Assembly assembly, string name, string version = null)
        {
            const string comment = "---";
            const string marker = comment + "!V";

            var streamReader = GetStream(assembly, name);
            string data = streamReader.ReadToEnd();
            streamReader.Close();
            if (string.IsNullOrEmpty(version))
                return data;

            var startQuery = data.IndexOf(version, StringComparison.Ordinal);
            if (startQuery == -1)
                throw new InvalidOperationException(string.Format("Script version {0} doesn't specified", version));
            startQuery -= comment.Length + 1;
            var endQuery = data.IndexOf(marker, startQuery + 1, StringComparison.Ordinal);
            return endQuery != -1 ? data.Substring(startQuery, endQuery - startQuery) : data.Substring(startQuery, data.Length - startQuery);
        }

        private static StreamReader GetStream(System.Reflection.Assembly assembly, string name)
        {
            return assembly.GetManifestResourceNames()
                .Where(n => n == name)
                .Select(n => new StreamReader(assembly.GetManifestResourceStream(n)))
                .FirstOrDefault();
        }


        public static IEnumerable<string> GetDeploymentScripts_V1_0()
        {
            const string version = "V1_0";
            var assembly = typeof(QueryProvider).Assembly;
            return new[]
                {
                    GetQuery(assembly, "TicketTool.Dal.Scripts.Func.GetOperationGroup.Sql"),
                    GetQuery(assembly, "TicketTool.Dal.Scripts.Func.GetActiveTasks.sql"),
                    GetQuery(assembly, "TicketTool.Dal.Scripts.SP.GetNextTask.sql"),
                    GetQuery(assembly, "TicketTool.Dal.Scripts.SP.GetAvalibleTasks.sql"),
                    GetQuery(assembly, "TicketTool.Dal.Scripts.SP.ParameterRealTimeReport.sql"),
                    GetQuery(assembly, "TicketTool.Dal.Scripts.Init.InsertData.sql"),
                };
        }

        public static IEnumerable<string> GetDeploymentScripts_V1_1()
        {
            const string version = "V1_1";
            var assembly = typeof (QueryProvider).Assembly;
            return new[]
                       {
                           GetQuery(assembly, "TicketTool.Dal.Scripts.SP.GetPingUserInfo.sql")
                       };
        }


        public static IEnumerable<string> GetDeploymentScripts_V1_2()
        {
            const string version = "V1_2";
            var assembly = typeof(QueryProvider).Assembly;
            return new[] 
                       {
                           GetQuery(assembly, "TicketTool.Dal.Scripts.Func.GetWokingHoursOfStore.sql", version),
                           GetQuery(assembly, "TicketTool.Dal.Scripts.SP.GetNextTask.sql", version)
                       };
        }
    }
}
