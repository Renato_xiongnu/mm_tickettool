﻿using System.ComponentModel.DataAnnotations;
using TicketTool.Dal.Model.Design;

namespace TicketTool.Dal.Model
{   
    public class ExternalSystem
    {
        [Key]
        public string ExternalSystemId { get; set; }

        [Required]
        [MaxLength(QueryProvider.DescriptionLength)]
        public string Name { get; set; }

        [MaxLength(QueryProvider.DescriptionLength)]
        public string WsdlUrl { get; set; }

        public string UseMapping { get; set; }
    }
}
