﻿using System;
using System.ComponentModel.DataAnnotations;
using TicketTool.Dal.Model.Design;

namespace TicketTool.Dal.Model
{
    public class NotificationLogRecord
    {
        [Key]
        public long Id { get; set; }               

        [Required]
        public NotificationLogGroup NotificationGroup { get; set; }

        [Required]
        public NotificationRecType NotificationRecType { get; set; }

        public DateTime CreatedDate { get; set; }

        [MaxLength(QueryProvider.GuidLength)]
        public string InvocationMarker { get; set; }

        [MaxLength(QueryProvider.GuidLength)]
        public string RefId { get; set; }

        [MaxLength(QueryProvider.StringLength)]
        public string SourceName { get; set; }

        [Required]
        public string Text { get; set; }
    }
}
