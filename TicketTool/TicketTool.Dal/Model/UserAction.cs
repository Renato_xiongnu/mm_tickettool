﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TicketTool.Dal.Model
{
    public class UserAction
    {
        [Key]
        public long UserActionId { get; set; }
        [Required]
        public UserLog UserLog { get; set; }
        [Required]
        public TicketTask Task { get; set; }
        [Required]
        public DateTime ActionTime { get; set; }
        [Required]
        public ActionType ActionType { get; set; }
    }
}
