﻿using System;
using System.ComponentModel.DataAnnotations;
using TicketTool.Dal.Model.Design;

namespace TicketTool.Dal.Model
{
    public class Event
    {
        [Key]
        public long EventId { get; set; }

        public Ticket Ticket { get; set; }

        [MaxLength(QueryProvider.StringLength)]
        public string Message { get; set; }

        public string SerialazedData { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
