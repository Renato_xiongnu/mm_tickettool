﻿namespace TicketTool.Dal.Model
{
    public enum ActionType
    {
        Postpone,
        Complete
    }
}