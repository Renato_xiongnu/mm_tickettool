﻿using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;


namespace TicketTool.Dal.Model
{
    public class SchedulerLog
    {
        [Key]
        public long Id { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public int EscalatedTasksNum { get; set; }

        public int EmailSendTasksNum { get; set; }

        public int AbandonUsersNum { get; set; }
    }
}
