﻿namespace TicketTool.Dal.Model.SP
{
    public class AvalibleTasksResult
    {
        public long TaskId { get; set; }
        public string Parameter { get; set; }
        public TaskStatus TaskStatus { get; set; }
        public long? CurrentSkillSetId { get; set; }
        public long? CurrentAssigneeId { get; set; }
        public long? InformedUserId { get; set; }
    }

    public class GetAvalibleTasks
    {        
        public long SkillSetId { get; set; }
        public int StartStatus { get; set; }
        public int EndStatus { get; set; }
        public int MaxStatus { get; set; }    
    }
}
