﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicketTool.Dal.Model.SP
{

    public class GetPingUserInfoResult
    {
        public ICollection<string> SkillSetName { get; set; }

        public ICollection<AssingnedTask> AssingnedTasks { get; set; }
    }

    public class AssingnedTask
    {
        public long TaskId { get; set; }

        public DateTime? UserDateAssigned { get; set; }

        public TimeSpan? MaxUserRuntime { get; set; }
    }

    
    //public class GetTasksForPingCommand : DatabaseExtensions.IStoredProcedure<GetTasksForPingCommandResult>
    //{
    //    public string UserName { get; set; }

    //    public DateTime LastPingTime { get; set; }

    //    public int FromStatus { get; set; }

    //    public int ToStatus { get; set; }
    //}
}
