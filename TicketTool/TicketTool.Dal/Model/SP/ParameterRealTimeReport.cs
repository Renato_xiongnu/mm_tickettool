﻿using System;

namespace TicketTool.Dal.Model.SP
{
    public class ParameterRealTimeReportResult
    {
        public string ParameterName { get; set; }
        public int QtyOnCallCenter { get; set; }
        public DateTime? MinDateOnCallCenter { get; set; }
        public int QtyOnStore { get; set; }
        public DateTime? MinDateOnStore { get; set; }
        public int InProgressOnCallCenter { get; set; }
        public int InProgressOnStore { get; set; }
    }

    public class ParameterRealTimeReport
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}
