﻿namespace TicketTool.Dal.Model.SP
{
    public class GetNextTask
    {
        public long UserId { get; set; }
        public long SkillSetId { get; set; }
        public int StartStatus { get; set; }
        public int EndStatus { get; set; }
        public int MaxStatus { get; set; }        
        public int SecondStatus { get; set; }
        public int CurrentUtcTime { get; set; }
    }
}
