﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TicketTool.Dal.Model.Design;

namespace TicketTool.Dal.Model
{
    public class Role
    {
        [Key]
        public long RoleId { get; set; }

        [Required]
        [MaxLength(QueryProvider.StringLength)]
        public string Name { get; set; }

        public virtual List<User> Users { get; set; }

        public virtual List<SkillSet> SkillSets { get; set; }

        public bool NeedNotification { get; set; }
    }
}
