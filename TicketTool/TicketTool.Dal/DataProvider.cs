﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Objects;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using System.Runtime.Serialization;
using TicketTool.Dal.Model;
using TicketTool.Dal.Model.SP;

namespace TicketTool.Dal
{
    public partial class DataProvider : LightModelProvider<ModelContext>
    {
        public static readonly DateTime MinDbDateTime = new DateTime(1900, 1, 1);

        public DataProvider(bool forceOpenConnection = false)
            : base(forceOpenConnection)
        {
        }

        #region Tickets
        public Ticket GetTicket(string ticketId)
        {
            return ModelContext.Tickets.Include("ExternalSystem").FirstOrDefault(t => t.TicketId == ticketId);
        }

        public IQueryable<Ticket> GetAllTickets()
        {
            return ModelContext.Tickets;
        }

        public Ticket GetTicketByWorkitemId(string workitemId)
        {
            return ModelContext.Tickets.FirstOrDefault(t => t.WorkItemId == workitemId);
        }

        public Ticket GetTicketById(string ticketId)
        {
            return ModelContext.Tickets.FirstOrDefault(t => t.TicketId == ticketId);
        }

        public Ticket PrepareTicket(string externalSystemId)
        {
            var externalSystem = GetExternalSystem(externalSystemId);
            return ModelContext.Tickets.Add(
                new Ticket
                    {
                        TicketId = Guid.NewGuid().ToString(),
                        ExternalSystem = externalSystem,
                        Name = string.Format("Ticket for external system {0}", externalSystem.Name),
                        CreateDate = DateTime.Now.ToUniversalTime(),
                        UpdateDate = DateTime.Now.ToUniversalTime()
                    });
        }
        #endregion

        #region Parameters
        public IQueryable<Parameter> GetAllParameters()
        {
            return ModelContext.Parameters;
        }

        public Parameter GetParameter(string parameterName)
        {
            var parameter = ModelContext.Parameters.SingleOrDefault(t => t.ParameterName == parameterName);
            if (parameter == null)
                throw new ObjectNotFoundException(string.Format("The parameter {0} doesn't exists", parameterName));

            return parameter;
        }

        public IEnumerable<ParameterRealTimeReportResult> GetParameterRealTimeReport(DateTime fromDate, DateTime toDate)
        {
            return ModelContext.Database.ExecuteStoredProcedure<ParameterRealTimeReportResult>(
                new ParameterRealTimeReport { FromDate = fromDate, ToDate = toDate });
        }

        public Parameter CreateParameter(Parameter parameter)
        {
            return this.ModelContext.Parameters.Add(parameter);
        }

        #endregion

        #region Skillsets
        public IQueryable<SkillSet> GetSkillSets(bool withUsers = false, bool withDefault = false, bool withEscalation = true, bool withTaskTypes = false, bool withRoles = false
            ,bool withUserClusters = false
            ,bool withUsersData = false)
        {
            DbQuery<SkillSet> dbQuery = ModelContext.SkillSets;
            if (withUsers)
                dbQuery = dbQuery.Include("Users");
            if (withTaskTypes)
                dbQuery = dbQuery.Include("TaskTypes");
            if (withRoles)
                dbQuery = dbQuery.Include("Roles");
            if (withUserClusters)
                dbQuery = dbQuery.Include("Users.Clusters");
            if (withUsersData)
            {
                dbQuery = dbQuery.Include("Users.Parameters");
                dbQuery = dbQuery.Include("Users.SkillSets");
                dbQuery = dbQuery.Include("Users.AvailableSkillSets");
                dbQuery = dbQuery.Include("Users.Clusters");
                dbQuery = dbQuery.Include("Users.Roles");
            }

            var query = dbQuery.Where(ss => !ss.IsDeleted);

            if (!withDefault)
                query = query.Where(t => t.IsDefault == false);

            if (!withEscalation)
                query = query.Where(t => t.IsEscalation == false);


            return query;
        }

        //public IQueryable<SkillSetNew> GetSkillSetsNew(bool withUsers = false, bool withDefault = false, bool withEscalation = true, bool withTaskTypes = false, bool withRoles = false,
        //   bool withUserClusters = false)
        //{
        //    DbQuery<SkillSet> dbQuery = ModelContext.SkillSets;
        //    if (withUsers)
        //        dbQuery = dbQuery.Include("Users");
        //    if (withTaskTypes)
        //        dbQuery = dbQuery.Include("TaskTypes");
        //    if (withRoles)
        //        dbQuery = dbQuery.Include("Roles");
        //    if (withUserClusters)
        //        dbQuery = dbQuery.Include("Users.Clusters");

        //    var query = dbQuery.Where(ss => !ss.IsDeleted);

        //    if (!withDefault)
        //        query = query.Where(t => t.IsDefault == false);

        //    if (!withEscalation)
        //        query = query.Where(t => t.IsEscalation == false);


        //    return query;
        //}

        public void AddClusterToUser(User user, Cluster cluster)
        {
            Cluster useClusters = null;
            if (user.Clusters != null)
                useClusters = user.Clusters.SingleOrDefault(t => t.Name == cluster.Name);
            else
                user.Clusters = new List<Cluster>();

            if (useClusters == null)
            {
                useClusters = GetAllClusters().SingleOrDefault(t => t.Name == cluster.Name);
                if (useClusters != null)
                    user.Clusters.Add(useClusters);
            }
        }

        public void RemoveClusterFromUser(User user, Cluster cluster)
        {
            if (user.Clusters != null)
                user.Clusters.Remove(cluster);
        }

        public void AddSkillSetToUser(User user, SkillSet skillSet)
        {
            SkillSet userSkillSet = null;
            if (user.SkillSets != null)
                userSkillSet = user.SkillSets.SingleOrDefault(t => t.Name == skillSet.Name);
            else
                user.SkillSets = new List<SkillSet>();

            if (userSkillSet == null)
            {
                userSkillSet = GetSkillSets().SingleOrDefault(t => t.Name == skillSet.Name);
                if (userSkillSet != null)
                    user.SkillSets.Add(userSkillSet);
            }
        }

        public void RemoveSkillSetFromUser(User user, SkillSet skillSet)
        {
            if (user.SkillSets != null)
                user.SkillSets.Remove(skillSet);
        }

        public SkillSet PrepareSkillSet(string name)
        {
            return ModelContext.SkillSets.Add(new SkillSet { Name = name });
        }

        public void DeleteSkillSet(SkillSet skillSet)
        {
            ModelContext.SkillSets.Remove(skillSet);
        }


        #endregion

        #region Tasks

        public TicketTask PrepareTask(string ticketId, string taskTypeName, TicketTask task, List<TaskField> fields)
        {
            var ticket = GetTicket(ticketId);
            if (ticket == null) throw new ObjectNotFoundException(string.Format("ticket with name {0} wasn't found", taskTypeName));

            var taskType = this.GetTaskTypes(ticket.ExternalSystem.ExternalSystemId, new[] { taskTypeName }, true).FirstOrDefault();
            if (taskType == null) throw new ObjectNotFoundException(string.Format("taskType with name {0} wasn't found", taskTypeName));

            task.Ticket = ticket;
            task.TaskType = taskType;

            var outcome = this.ModelContext.Outcomes.FirstOrDefault(t => t.Data == task.Outcomes.Data);
            if (outcome == null)
            {
                this.ModelContext.Outcomes.Add(task.Outcomes);
            }
            else
            {
                task.Outcomes = outcome;
            }

            var taskFieldTypes = fields.Select(t => t.FieldType).ToList();
            var existingFieldTypes = this.GetFieldType(taskFieldTypes).ToList();
            var newTaskFields = taskFieldTypes.Where(f => existingFieldTypes.All(m => m.Hash != f.GetHashCode())).ToList();

            this.AddFieldTypes(newTaskFields);

            task.TaskFields = existingFieldTypes.Union(newTaskFields)
                .Select(t => fields
                    .Where(x => x.FieldType != null)
                    .Where(x => x.FieldType.GetHashCode() == t.Hash)
                    .Select(x => new TaskField { FieldType = t, Value = x.Value })
                    .FirstOrDefault())
                .ToList();

            task.CurrentSkillSet = taskType.SkillSet ?? this.GetDefaultSkillSet();

            this.ModelContext.TicketTasks.Add(task);
            this.Save();

            return task;
        }

        public IQueryable<TicketTask> GetAllTaskByUser(User user)
        {
            return ModelContext.TicketTasks
                .Include("TaskFields.FieldType")
                .Include("CurrentAssignee")
                .Include("TaskType")
                .Where(t => t.CurrentAssignee.UserId == user.UserId);
        }

        public IQueryable<TicketTask> GetTasks(IEnumerable<string> skillSetNames, string parameterName)
        {
            var query = this.ModelContext.TicketTasks
                            .Include("Ticket")
                            .Include("TaskType")
                            .Where(t => skillSetNames.Contains(t.CurrentSkillSet.Name))
                            .Where(t => t.TaskStatus >= (int)TaskStatus.AssignedToSkillSet)
                            .Where(t => t.TaskStatus <= (int)TaskStatus.RemovedFromSkillSet);
            return String.IsNullOrEmpty(parameterName) ? query : query.Where(t => t.Parameter == parameterName);

        }

        public IQueryable<TicketTask> GetTasks(string userName, string parameterName)
        {
            return from skillSet in this.ModelContext.SkillSets
                   from task in this.ModelContext.TicketTasks
                   from user in skillSet.Users
                   where user.Name == userName
                   where task.CurrentSkillSet.SkillSetId == skillSet.SkillSetId
                   where task.Parameter == parameterName
                   select task;
        }

        public IQueryable<TicketTask> GetTasks(string workitemId)
        {
            return GetAllTasks(true, true).Where(t => t.Ticket.WorkItemId == workitemId);
        }

        public IQueryable<TicketTask> GetAllTasks(bool includeTicket = false, bool includeCompletionQueue = false)
        {
            var tasks = includeTicket ? ModelContext.TicketTasks.Include("Ticket") : ModelContext.TicketTasks;
            tasks = includeCompletionQueue ? tasks.Include("TaskCompletionQueue") : tasks;

            return tasks
                .Include("TaskType")
                .Include("TaskFields.FieldType")
                .Include("TaskType.ExternalSystem")
                .Include("CurrentAssignee")
                .Include("Performer")
                .Include("CurrentSkillSet")
                .Include("Outcomes");
        }

        public TicketTask GetTask(long taskId, bool includeTicket = false, bool includeCompletionQueue = false)
        {
            return GetAllTasks(includeTicket, includeCompletionQueue).SingleOrDefault(t => t.TaskId == taskId);
        }

        public IQueryable<TicketTask> GetTasksByTicket(string ticketId)
        {
            return ModelContext.TicketTasks
                .Include("CurrentAssignee")
                .Include("CurrentSkillSet")
                .Where(t => t.Ticket.TicketId == ticketId);
        }

        public TicketTask GetTaskAndUpdateForUser(User user, SkillSet skillSet, bool skipAssigned)
        {
            var task = GetUserTask(user, skillSet, skipAssigned);
            if (task != null)
                task.CurrentAssignee = user;
            return task;
        }

        public IEnumerable<AvalibleTasksResult> GetAvalibleTaskId(SkillSet skillSet)
        {
            return ModelContext.Database.ExecuteStoredProcedure<AvalibleTasksResult>(
                new GetAvalibleTasks
                {
                    SkillSetId = skillSet.SkillSetId,
                    StartStatus = (int)TaskStatus.AssignedToSkillSet,
                    EndStatus = (int)TaskStatus.RemovedFromSkillSet,
                    MaxStatus = (int)TaskStatus.Finished
                });
        }

        public IEnumerable<TicketTask> GetUserAbandonedTasksOnDate(DateTime currentDate)
        {
            var userTasks = ModelContext.TicketTasks
                .Where(t => !t.Escalated && t.TaskStatus >= (int)TaskStatus.AssignedToUser && t.TaskStatus < (int)TaskStatus.RemovedFromUser);

            //EntityFunctions..DiffDays()
            //var t = currentDate.TimeOfDay;
            //System.Data.Objects.SqlClient.SqlFunctions.co

            var res = (from ts in userTasks
                       join tp in ModelContext.TaskTypes on ts.TaskType equals tp
                       let maxTime = tp.MaxUserRuntime.HasValue ? tp.MaxUserRuntime.Value : default(TimeSpan)
                       let userDate = ts.UserDateAssigned.HasValue ? ts.UserDateAssigned.Value : currentDate
                       let maxTimeH = EntityFunctions.AddHours(userDate, maxTime.Hours)
                       let maxTimeM = EntityFunctions.AddMinutes(maxTimeH, maxTime.Minutes)
                       let maxTimeDate = EntityFunctions.AddSeconds(maxTimeM, maxTime.Seconds)

                       where maxTimeDate < currentDate
                       select new { task = ts, user = ts.CurrentAssignee, skillSet = ts.CurrentSkillSet });

            return res.ToList().Select(t => t.task);
        }

        public IEnumerable<TicketTask> GetSkillSetAbandonedTasksOnDate(DateTime currentDate)
        {

            var skillSetTasks = ModelContext.TicketTasks.Where(
                t => !t.Escalated &&
                     t.TaskStatus >= (int)TaskStatus.AssignedToSkillSet &&
                     t.TaskStatus < (int)TaskStatus.RemovedFromSkillSet &&
                     t.TaskStatus != (int)TaskStatus.AssignedToUser);


            var res = (from ts in skillSetTasks
                       join tp in ModelContext.TaskTypes on ts.TaskType equals tp
                       let maxTime = tp.MaxSkillSetRunTime.HasValue ? tp.MaxSkillSetRunTime.Value : default(TimeSpan)
                       let skillSetDate = ts.SkillSetDateAssigned.HasValue ? ts.SkillSetDateAssigned.Value : currentDate

                       let maxTimeH = EntityFunctions.AddHours(skillSetDate, maxTime.Hours)
                       let maxTimeM = EntityFunctions.AddMinutes(maxTimeH, maxTime.Minutes)
                       let maxTimeDate = EntityFunctions.AddSeconds(maxTimeM, maxTime.Seconds)
                       where maxTimeDate < currentDate
                       select new { task = ts, user = ts.CurrentAssignee, skillSet = ts.CurrentSkillSet, taskType = ts.TaskType });
            return res.ToList().Select(t => t.task);
        }

        public IQueryable<TaskDto> GetTaskStat(IList<SkillSet> skillSets)
        {
            var skilset = skillSets.Select(t => t.SkillSetId);
            return ModelContext.TicketTasks
                .Include(t => t.CurrentSkillSet)
                .Where(
                    t =>
                        skilset.Any(el => el == t.CurrentSkillSet.SkillSetId) &&
                        t.TaskStatus < (int)TaskStatus.Finished)
                .Select(t => new TaskDto
                {
                    TaskStatus = t.TaskStatus,
                    Parameter = t.Parameter,
                    CurrentSkillSet = t.CurrentSkillSet,
                    Escalated = t.Escalated
                });
        }

        public class TaskDto
        {
            public int TaskStatus { get; set; }
            public string Parameter { get; set; }
            public SkillSet CurrentSkillSet { get; set; }
            public bool Escalated { get; set; }
        }

        public IQueryable<TicketTask> GetEscaltedTaskFromSkillSet(SkillSet skillSet)
        {
            var res = (from task in ModelContext.TicketTasks
                       join escalateOp in ModelContext.TaskOperations on task equals escalateOp.TicketTask
                       let removeFromSkillSetOp = escalateOp.RefOperation
                       where
                           task.TaskStatus < (int)TaskStatus.Finished && task.Escalated &&
                           escalateOp.ToStatus == (int)TaskStatus.Escalated &&
                           removeFromSkillSetOp.ToStatus == (int)TaskStatus.RemovedFromSkillSet &&
                           removeFromSkillSetOp.SkillSet.SkillSetId == skillSet.SkillSetId
                       select task).Distinct();

            return res;
        }

        public IQueryable<TicketTask> GetCompletedTasks()
        {
            return ModelContext.TicketTasks.Include("Ticket").Where(t => t.TaskStatus == (int)TaskStatus.Finished);
        }

        #endregion

        #region UserLogs

        public UserAction CreateUserAction(User user, SkillSet skillSet)
        {
            var ul = this.GetUserLog(user, skillSet);
            if (ul.UserLogId == 0 || ul.LoginDate == DateTime.MinValue)
                ul.LoginDate = DateTime.Now.ToUniversalTime();
            return this.ModelContext.UserActions.Add(new UserAction { UserLog = ul });
        }

        public IEnumerable<UserLog> GetUserActions(DateTime date)
        {
            var start = date.Date.AddDays(-1);
            var end = date.Date.AddDays(1);

            var logs = (from a in this.ModelContext.UserActions
                        where a.ActionTime > start
                        where a.ActionTime < end
                        group a by new
                        {
                            a.UserLog.User,
                            a.UserLog.SkillSet
                        }
                            into groupedActions
                            let postponed = groupedActions.Count(ga => ga.ActionType == ActionType.Postpone)
                            let completed = groupedActions.Count(ga => ga.ActionType == ActionType.Complete)
                            let lastlogin = (from gr in groupedActions
                                             orderby gr.UserLog.LoginDate descending
                                             select gr.UserLog).FirstOrDefault()
                            select new { lastlogin, lastlogin.User, lastlogin.SkillSet, completed, postponed }).ToList();

            foreach (var log in logs)
            {
                log.lastlogin.AbandonedQty = log.postponed;
                log.lastlogin.ProcessedQty = log.completed;
            }

            return logs.Select(l => l.lastlogin);
        }

        public UserLog GetUserLog(User user, SkillSet skillSet)
        {
            var latestUL = ModelContext.UserLog
                                     .Where(t => t.User.UserId == user.UserId)
                                     .Where(t => t.SkillSet.SkillSetId == skillSet.SkillSetId)
                                     .Where(t => t.LogoutDate == null)
                                     .OrderByDescending(t => t.LoginDate)
                                     .FirstOrDefault();
            if (latestUL == null)
                return this.UserLogCreateRecord(user, skillSet);

            if (latestUL.LoginDate.Date < DateTime.Now.ToUniversalTime().Date)
            {
                latestUL.LogoutDate = latestUL.LoginDate.Date;
                var newUL = this.UserLogCreateRecord(user, skillSet);
                newUL.LoginDate = DateTime.Now.ToUniversalTime();
            }
            return latestUL;
        }

        public UserLog UserLogCreateRecord(User user, SkillSet skillSet)
        {
            return ModelContext.UserLog.Add(new UserLog { User = user, SkillSet = skillSet, });
        }

        public IQueryable<UserLog> UserLogFindOpenedRecords(User user)
        {
            return ModelContext.UserLog
                .Where(ul => ul.User.UserId == user.UserId)
                .Where(ul => ul.LogoutDate == null);
        }

        public IQueryable<UserLog> GetAllUserLogs()
        {
            return ModelContext.UserLog;
        }
        #endregion

        #region Fields
        private IQueryable<FieldType> GetFieldType(IEnumerable<FieldType> fieldTypes)
        {
            var whereList = fieldTypes.ToArray().Select(x => x.GetHashCode()).ToList();
            return ModelContext.FieldTypes.Where(x => whereList.Contains(x.Hash));
        }

        private void AddFieldTypes(IEnumerable<FieldType> fieldTypes)
        {
            foreach (var type in fieldTypes)
            {
                type.IsMandatory = true;
                type.Hash = type.GetHashCode();
                ModelContext.FieldTypes.Add(type);
            }
        }
        #endregion

        #region Users

        public GetPingUserInfoResult GetPingUserInfo(string userName)
        {
            var currentDate = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd HH:mm:ss.fff");
            var command = String.Format("exec SP_GetPingUserInfo N'{0}', {1}, {2}, N'{3}'", userName, (int)TaskStatus.AssignedToUser, (int)TaskStatus.RemovedFromUser, currentDate);
            return ModelContext.Database.ExecuteMultyRecordCommand(command, reader =>
                {
                    var skillSets = new List<string>(10);
                    var tasks = new List<AssingnedTask>(10);

                    if (reader == null)
                    {
                    }
                    else
                    {
                        while (reader.Read())
                            skillSets.Add(reader["SkillSetName"].ToString());

                        if (reader.NextResult())
                        {
                            while (reader.Read())
                            {
                                tasks.Add(
                                    new AssingnedTask
                                        {
                                            TaskId = Convert.ToInt64(reader["TaskId"]),
                                            MaxUserRuntime = (TimeSpan?)reader["MaxUserRuntime"],
                                            UserDateAssigned = DBNull.Value.Equals(reader["UserDateAssigned"]) ? null : (DateTime?)Convert.ToDateTime(reader["UserDateAssigned"])
                                        });
                            }
                        }
                    }
                    return new GetPingUserInfoResult { SkillSetName = skillSets, AssingnedTasks = tasks };
                });
        }

        public IQueryable<User> GetUsersByParameter(Parameter parameter)
        {
            return from u in this.GetAllUsers(false, true, true, true)
                   from p in u.Parameters
                   where p.ParameterName == parameter.ParameterName
                   select u;
        }

        public IQueryable<User> GetUsersByParameter(Parameter parameter, string skillSetName)
        {
            return from u in this.GetAllUsers(false, true, true)
                   from p in u.Parameters
                   from s in u.SkillSets
                   where p.ParameterName == parameter.ParameterName
                   where s.Name == skillSetName
                   select u;
        }

        #endregion

        #region TaskTypes
        public IQueryable<TaskType> GetTaskTypes(string externalSystemId, IEnumerable<string> taskTypeName, bool withFullInfo = false)
        {
            var taskTypes = ModelContext.TaskTypes;
            if (withFullInfo)
                taskTypes.Include("SkillSet");
            return taskTypes.Where(t => t.ExternalSystem.ExternalSystemId == externalSystemId && taskTypeName.Contains(t.Name));
        }

        public TaskType PrepareTaskType(string externalSystemId, string name)
        {
            return ModelContext.TaskTypes.Add(
                new TaskType
                    {
                        Name = name,
                        ExternalSystem = GetExternalSystem(externalSystemId),
                        SkillSet = GetDefaultSkillSet(),
                        SlaSkillSet = GetFirstEscalationSkillSet()
                    });
        }

        #endregion

        #region ExternalSystems

        public IQueryable<ExternalSystem> GetAllExternalSystems()
        {
            return ModelContext.ExternalSystems;
        }

        public IQueryable<ExternalSystem> GetAvailableExternalSystems()
        {
            return ModelContext.ExternalSystems.Where(es => !String.IsNullOrEmpty(es.WsdlUrl));
        }

        public ExternalSystem GetExternalSystem(string externalSystemId, bool throwIfNotExists = true)
        {
            var externalSystem = ModelContext.ExternalSystems.SingleOrDefault(t => t.ExternalSystemId == externalSystemId);
            if (externalSystem == null && throwIfNotExists)
                throw new ObjectNotFoundException(string.Format("External system with id {0} wasn't found", externalSystemId));

            return externalSystem;
        }

        public IQueryable<TaskType> GetTaskTypes(string externalSystemId)
        {
            return ModelContext.TaskTypes
                               .Include("SkillSet")
                               .Where(t => t.ExternalSystem.ExternalSystemId == externalSystemId);
        }

        #endregion

        #region TaskOperations
        public IQueryable<TaskOperation> GetAllTaskOperations()
        {
            return ModelContext.TaskOperations;
        }

        public TaskOperation AddOperation(TicketTask task, TaskStatus fromStatus, TaskStatus toStatus, DateTime? date = null)
        {
            if (task.OperationLog == null)
                task.OperationLog = new List<TaskOperation>();
            var currentDate = date ?? DateTime.Now.ToUniversalTime();
            var operation = new TaskOperation
            {
                FromStatus = (int)fromStatus,
                ToStatus = (int)toStatus,
                UpdateDate = currentDate
            };
            task.OperationLog.Add(operation);

            task.TaskStatus = (int)toStatus;
            task.UpdateDate = currentDate;

            return operation;
        }

        public TaskOperation GetLastSkillSetOperation(TicketTask task)
        {
            TaskOperation operation;
            if (task.LastSkillSetOperation == null)
            {
                operation = ModelContext.TaskOperations
                    .Where(t => t.TicketTask.TaskId == task.TaskId && t.ToStatus == (int)TaskStatus.AssignedToSkillSet)
                    .OrderByDescending(t => t.UpdateDate)
                    .FirstOrDefault();

                if (operation != null)
                    task.LastSkillSetOperation = operation; //TODO: check constrain
            }
            else
                operation = task.LastSkillSetOperation;

            return operation;
        }

        public TaskOperation GetLastUserOperation(TicketTask task)
        {
            TaskOperation operation;
            if (task.LastUserOperation == null)
            {
                operation = ModelContext.TaskOperations
                    .Where(t => t.TicketTask.TaskId == task.TaskId && t.ToStatus == (int)TaskStatus.AssignedToUser)
                    .OrderByDescending(t => t.UpdateDate)
                    .FirstOrDefault();

                if (operation != null)
                    task.LastUserOperation = operation; //TODO: check constraint
            }
            else
                operation = task.LastUserOperation;

            return operation;
        }

        #endregion

        #region TaskQueue
        public IQueryable<TaskCompletionQueue> GetAllTaskCompletionQueue()
        {
            return ModelContext.TaskCompletionQueue.Include("ExternalSystem").Include("QueueLogs");
        }

        public TaskCompletionQueue PrepairTaskCompletionQueue(TicketTask task)
        {
            var currentDate = DateTime.Now.ToUniversalTime();
            return ModelContext.TaskCompletionQueue.Add(new TaskCompletionQueue
                {
                    CreateDate = currentDate,
                    UpdateDate = currentDate,
                    Outcome = task.CurrentOutcome,
                });
        }

        public TaskCompletionQueueLog PrepairTaskCompletionQueueLog(TaskCompletionQueue taskCompletionQueue, TaskCompletionStatus? fromStatus = null, TaskCompletionStatus? toStatus = null)
        {
            return ModelContext.TaskCompletionQueueLog.Add(
                new TaskCompletionQueueLog
                {
                    CreateDate = taskCompletionQueue.UpdateDate,
                    FromStatus = fromStatus == null
                        ? taskCompletionQueue.CompletionStatus
                        : (int)fromStatus.Value,
                    ToStatus = toStatus == null
                        ? taskCompletionQueue.CompletionStatus
                        : (int)toStatus.Value,
                });
        }

        #endregion

        #region Notifications
        public NotificationLogGroup GetDefaultNotificationLogGroup()
        {
            return ModelContext.EventLogGroup.Single(t => t.IsDefault);
        }

        public NotificationLogRecord PrepairEventLogRecord(NotificationRecType logType, NotificationLogGroup eventLogGroup)
        {
            return ModelContext.EventLogRecord.Add(
                new NotificationLogRecord
                    {
                        NotificationGroup = eventLogGroup,
                        NotificationRecType = logType,
                        CreatedDate = DateTime.Now.ToUniversalTime()
                    });
        }

        #endregion

        public TicketTask AssignFreeTaskForUser(TicketTask task, User user, bool force)
        {
            if (task.CurrentAssignee == user)
            {
                task.IsOldTask = true;
                return task;
            }

            if (!force)
            {
                if (task.CurrentAssignee != null)
                    return null;

                if (!(task.TaskStatus >= (int)TaskStatus.AssignedToSkillSet && task.TaskStatus < (int)TaskStatus.RemovedFromSkillSet))
                    return null;
            }

            task.CurrentAssignee = user;

            return task;
        }

        public IQueryable<Cluster> GetAllClusters()
        {
            var clusters= ModelContext.Cluster.Include(t => t.Parameters);

            return clusters;
        }

        public void AddOrUpdateCluster(Cluster cluster)
        {
            var oldCluster = ModelContext.Cluster.SingleOrDefault(t => t.Name == cluster.Name);
            if (oldCluster == null)
            {
                ModelContext.Cluster.Add(cluster);
            }
            else
            {
                foreach (var parameter in oldCluster.Parameters.ToList())
                {
                    parameter.Cluster = null;
                    ModelContext.Entry(parameter).State = EntityState.Modified;
                }
                oldCluster.Parameters = cluster.Parameters;
                ModelContext.Entry(oldCluster).State = EntityState.Modified;
            }
        }

        public void DeleteCluster(string name)
        {
            var cluster = ModelContext.Cluster.SingleOrDefault(t => t.Name == name);

            if (cluster == null)
            {
                throw new ObjectNotFoundException(string.Format("Cluster with name {0} not found", name));
            }

            ModelContext.Cluster.Remove(cluster);
        }

        #region Private
        private TicketTask GetUserTask(User user, SkillSet skillSet, bool skipAssigned)
        {
            if (!skipAssigned)
            {
                var oldTask = this.GetAllTaskByUser(user).FirstOrDefault();
                if (oldTask != null)
                {
                    oldTask.IsOldTask = true;
                    return oldTask;
                }
            }

            var currentDate = DateTime.Now.ToUniversalTime();
            var getNextTask = new GetNextTask
            {
                UserId = user.UserId,
                SkillSetId = skillSet.SkillSetId,
                StartStatus = (int)TaskStatus.AssignedToSkillSet,
                EndStatus = (int)TaskStatus.RemovedFromSkillSet,
                MaxStatus = (int)TaskStatus.Finished,
                SecondStatus = (int)TaskStatus.RemovedFromUser,
                CurrentUtcTime = (int)currentDate.TimeOfDay.Ticks,
            };

            var tasks = ModelContext.Database.ExecuteStoredProcedure<long?>(getNextTask);
            var taskId = tasks.First();
            return taskId.HasValue ? GetTask(taskId.Value) : null;
        }

        private SkillSet GetFirstEscalationSkillSet()
        {
            return ModelContext.SkillSets.First(t => t.IsEscalation);
        }

        private SkillSet GetDefaultSkillSet()
        {
            return ModelContext.SkillSets.Single(t => t.IsDefault);
        }
        #endregion
    }



    public class SkillSetNew
    {
        public long SkillSetId { get; set; }  
       
        public string Name { get; set; }

        public bool IsDefault { get; set; }

        public bool IsParameterizable { get; set; }

        public bool DeliveryTaskByEmail { get; set; }

        public bool IsEscalation { get; set; }

        public virtual List<User> Users { get; set; }

        public virtual List<User> AvailableUsers { get; set; }

        public virtual List<TaskType> TaskTypes { get; set; }

        public List<Role> Roles { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsClustered { get; set; }
    }
}
