﻿using System;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace TicketTool.Dal
{
    public abstract class LightModelProvider<T>: IDisposable where T : DbContext, new()
    {
        private readonly bool _forceOpenConnection;
        public T ModelContext { get; private set; }        

        protected void ThrowIfDisposed()
        {
            if (ModelContext == null)
                throw new ObjectDisposedException(GetType().Name);
        }


        protected LightModelProvider(bool forceOpenConnection = false)
        {
            _forceOpenConnection = forceOpenConnection;
            ModelContext = new T();
            
            if (_forceOpenConnection)
                ((IObjectContextAdapter)ModelContext).ObjectContext.Connection.Open();
        }

        public void Dispose()
        {
            ThrowIfDisposed();

            if (_forceOpenConnection)
                ((IObjectContextAdapter)ModelContext).ObjectContext.Connection.Close();

            ModelContext.Dispose();
            ModelContext = null;
        }

        public void Save()
        {
            ThrowIfDisposed();
            try
            {
                ModelContext.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                var sb = new StringBuilder("Entity Validation Failed - errors follow:\n");

                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }

                throw new DbEntityValidationException(sb.ToString(), ex.EntityValidationErrors);
            }                       
        }
    }
}
