﻿-- add GO everywhere

print 'CreateDb TicketToolDb$(country)'
USE [master]
if db_id('TicketToolDb$(country)') is not null
	BEGIN
		EXEC msdb.dbo.sp_delete_database_backuphistory @database_name = N'TicketToolDb$(country)'
		ALTER DATABASE [TicketToolDb$(country)] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
		ALTER DATABASE [TicketToolDb$(country)] SET SINGLE_USER
		DROP DATABASE [TicketToolDb$(country)]
	END
GO

CREATE DATABASE [TicketToolDb$(country)]
GO

ALTER DATABASE [TicketToolDb$(country)] SET RECOVERY SIMPLE WITH NO_WAIT
GO

ALTER DATABASE [TicketToolDb$(country)] SET ALLOW_SNAPSHOT_ISOLATION ON
GO

ALTER DATABASE [TicketToolDb$(country)] SET READ_COMMITTED_SNAPSHOT ON
GO

-- NOTE [sg]: in order to fix http://mediamarkt.jira.com/browse/GMSREPORTING-164 we need to enable deadlocks logging
DBCC TRACEON (1222, 3605, -1)
GO


IF NOT EXISTS (SELECT * FROM sys.server_principals WHERE name = N'TicketToolLogin$(country)')
CREATE LOGIN [TicketToolLogin$(country)] WITH PASSWORD=N'Rep@rting1', DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO




USE [TicketToolDb$(country)]
GO
CREATE USER [TicketToolUser] FOR LOGIN TicketToolLogin$(country) WITH DEFAULT_SCHEMA=dbo
EXEC sp_addrolemember N'db_datareader', TicketToolUser
EXEC sp_addrolemember N'db_datawriter', TicketToolUser
EXEC sp_addrolemember N'db_owner', TicketToolUser
GO
