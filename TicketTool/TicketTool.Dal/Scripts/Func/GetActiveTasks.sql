﻿CREATE FUNCTION [dbo].[GetActiveTasks]
(		
	@FromDate DATETIME,
	@ToDate DATETIME,  
	@Parameter NVARCHAR(255)
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT task.*, 
		   skillSet.IsDefault as IsDefaultSkillSet, 
		   skillSet.IsParameterizable as IsParameterizableSkillSet, 
		   skillSet.DeliveryTaskByEmail as DeliveryTaskByEmailSkillSet, 
		   skillSet.IsEscalation as IsEscalationSkillSet
		   FROM [dbo].[Tickets] ticket
		join [dbo].[TicketTasks] task on ticket.TicketId = task.Ticket_TicketId
		join [dbo].[SkillSets] skillSet on skillSet.SkillSetId =  task.CurrentSkillSet_SkillSetId	
			where ticket.Parameter = @Parameter and task.CreateDate >= @FromDate and task.CreateDate < @ToDate and task.TaskStatus < 100 						
)