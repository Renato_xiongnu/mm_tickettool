﻿---!V1_2-->
CREATE FUNCTION [dbo].[GetWokingHoursOfStore]
(		
	@CurrentUtcTime INT
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT ticket.TicketId, parameter.ParameterName,														
			(CASE
				WHEN parameter.UtcStartWorkingTime <= @CurrentUtcTime AND @CurrentUtcTime < parameter.UtcEndWorkingTime
					THEN 1 
				WHEN parameter.UtcStartWorkingTime <= @CurrentUtcTime +24*60 AND @CurrentUtcTime +24*60 < parameter.UtcEndWorkingTime
					THEN 1 
				ELSE 0
			END )AS IsWorkingHours
			from [dbo].Tickets ticket									
			INNER JOIN [dbo].Parameters parameter on parameter.ParameterName = ticket.Parameter
)
--<--