﻿USE [TicketToolDb$(country)]
print 'Create code first objects:'
GO
/****** Object:  Table [dbo].[Users]    Script Date: 12/19/2013 15:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserId] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Email] [nvarchar](4000) NULL,
	[Password] [nvarchar](4000) NOT NULL,
	[LastPingDateTime] [datetime] NOT NULL,
	[IsLocked] [bit] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastLoginDate] [datetime] NOT NULL,
 CONSTRAINT [PK_dbo.Users] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NotificationLogGroups]    Script Date: 12/19/2013 15:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationLogGroups](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[IsDefault] [bit] NOT NULL,
	[DefaultRecipientEmail] [nvarchar](4000) NOT NULL,
 CONSTRAINT [PK_dbo.NotificationLogGroups] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FieldTypes]    Script Date: 12/19/2013 15:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FieldTypes](
	[FieldTypeId] [bigint] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](255) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[IsMandatory] [bit] NOT NULL,
	[DefaultValue] [nvarchar](max) NULL,
	[PredefinedValuesList] [nvarchar](4000) NULL,
 CONSTRAINT [PK_dbo.FieldTypes] PRIMARY KEY CLUSTERED 
(
	[FieldTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExternalSystems]    Script Date: 12/19/2013 15:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExternalSystems](
	[ExternalSystemId] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](4000) NOT NULL,
	[WsdlUrl] [nvarchar](4000) NULL,
	[UseMapping] [nvarchar](4000) NULL,
 CONSTRAINT [PK_dbo.WorkflowProcesses] PRIMARY KEY CLUSTERED 
(
	[ExternalSystemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TaskCompletionQueues]    Script Date: 12/19/2013 15:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaskCompletionQueues](
	[TaskQueueId] [bigint] IDENTITY(1,1) NOT NULL,
	[InvocationMarker] [nvarchar](36) NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[FinishDate] [datetime] NULL,
	[ExternalSystemOperationName] [nvarchar](4000) NOT NULL,
	[Outcome] [nvarchar](255) NOT NULL,
	[SerializedTask] [nvarchar](max) NOT NULL,
	[CompletionStatus] [int] NOT NULL,
	[ExternalSystem_ExternalSystemId] [nvarchar](128) NULL,
 CONSTRAINT [PK_dbo.TaskCompletionQueues] PRIMARY KEY CLUSTERED 
(
	[TaskQueueId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SkillSets]    Script Date: 12/19/2013 15:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SkillSets](
	[SkillSetId] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[IsDefault] [bit] NOT NULL,
	[IsParameterizable] [bit] NOT NULL,
	[DeliveryTaskByEmail] [bit] NOT NULL,
	[IsEscalation] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.SkillSets] PRIMARY KEY CLUSTERED 
(
	[SkillSetId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TaskTypes]    Script Date: 12/19/2013 15:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaskTypes](
	[TaskTypeId] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[SlaControlDate] [datetime] NULL,
	[SlaName] [nvarchar](255) NULL,
	[MaxSkillSetRunTime] [time](7) NULL,
	[MaxUserRuntime] [time](7) NULL,
	[ExternalSystem_ExternalSystemId] [nvarchar](128) NULL,
	[SlaSkillSet_SkillSetId] [bigint] NULL,
	[SkillSet_SkillSetId] [bigint] NULL,
	[CanPostponeCallingWorkflow] [bit] NOT NULL,
	[NotificationGroup_Id] [bigint] NULL,
	[WorkitemPageUrl] [nvarchar](4000) NULL,
	[TaskPageUrl] [nvarchar](4000) NULL,
 CONSTRAINT [PK_dbo.TaskTypes] PRIMARY KEY CLUSTERED 
(
	[TaskTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tickets]    Script Date: 12/19/2013 15:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tickets](
	[TicketId] [nvarchar](36) NOT NULL,
	[Parameter] [nvarchar](255) NULL,
	[WorkItemId] [nvarchar](255) NULL,
	[Name] [nvarchar](4000) NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[CloseDate] [datetime] NULL,
	[ExternalSystem_ExternalSystemId] [nvarchar](128) NULL,
 CONSTRAINT [PK_dbo.Tickets] PRIMARY KEY CLUSTERED 
(
	[TicketId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Outcomes]    Script Date: 12/19/2013 15:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Outcomes](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Data] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_dbo.Outcomes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TicketTasks]    Script Date: 12/19/2013 15:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TicketTasks](
	[TaskId] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Description] [nvarchar](max) NULL,
	[CurrentOutcome] [nvarchar](255) NULL,
	[Comment] [nvarchar](max) NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[FinishDate] [datetime] NULL,
	[TaskStatus] [int] NOT NULL,
	[Escalated] [bit] NOT NULL,
	[SkillSetDateAssigned] [datetime] NULL,
	[SkillSetRuntime] [time](7) NULL,
	[UserDateAssigned] [datetime] NULL,
	[ReturnToQueueAfterDate] [datetime] NULL,
	[UserRuntime] [time](7) NULL,
	[Parameter] [nvarchar](255) NULL,
	[RelatedContent] [nvarchar](max) NULL,
	[RelatedContentName] [nvarchar](255) NULL,
	[RelatedContentRight] [int] NOT NULL,
	[Ticket_TicketId] [nvarchar](36) NULL,
	[TaskType_TaskTypeId] [bigint] NULL,
	[Outcomes_Id] [bigint] NULL,
	[InformedUser_UserId] [bigint] NULL,
	[CurrentAssignee_UserId] [bigint] NULL,
	[CurrentSkillSet_SkillSetId] [bigint] NULL,
	[LastUserOperation_OperationId] [bigint] NULL,
	[LastSkillSetOperation_OperationId] [bigint] NULL,
	[TaskCompletionQueue_TaskQueueId] [bigint] NULL,
	[Performer_UserId] [bigint] NULL,
 CONSTRAINT [PK_dbo.TicketTasks] PRIMARY KEY CLUSTERED 
(
	[TaskId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Performer_UserId] ON [dbo].[TicketTasks] 
(
	[Performer_UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TaskFields]    Script Date: 12/19/2013 15:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaskFields](
	[TaskFieldId] [bigint] IDENTITY(1,1) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[FieldType_FieldTypeId] [bigint] NULL,
	[TicketTask_TaskId] [bigint] NULL,
 CONSTRAINT [PK_dbo.TaskFields] PRIMARY KEY CLUSTERED 
(
	[TaskFieldId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Parameters]    Script Date: 12/19/2013 15:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Parameters](
	[ParameterName] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[TimeZone] [nvarchar](255) NOT NULL,
	[TimeZoneOffset] [nvarchar](8) NULL,
	[StartWorkingTime] [time](7) NOT NULL,
	[EndWorkingTime] [time](7) NOT NULL,
	[UtcStartWorkingTime] [int] NOT NULL,
	[UtcEndWorkingTime] [int] NOT NULL,
	[CanSynchronize] [bit] NOT NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Stores] PRIMARY KEY CLUSTERED 
(
	[ParameterName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 12/19/2013 15:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[RoleId] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[NeedNotification] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Roles] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TaskOperations]    Script Date: 12/19/2013 15:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaskOperations](
	[OperationId] [bigint] IDENTITY(1,1) NOT NULL,
	[FromStatus] [int] NOT NULL,
	[ToStatus] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[HasError] [bit] NOT NULL,
	[MessageText] [nvarchar](4000) NULL,
	[SkillSetDateAssigned] [datetime] NULL,
	[SkillSetRuntime] [time](7) NULL,
	[UserDateAssigned] [datetime] NULL,
	[UserRuntime] [time](7) NULL,
	[RefOperation_OperationId] [bigint] NULL,
	[SkillSet_SkillSetId] [bigint] NULL,
	[User_UserId] [bigint] NULL,
	[TicketTask_TaskId] [bigint] NULL,
 CONSTRAINT [PK_dbo.TaskOperations] PRIMARY KEY CLUSTERED 
(
	[OperationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SchedulerLogs]    Script Date: 12/19/2013 15:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchedulerLogs](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[EscalatedTasksNum] [int] NOT NULL,
	[EmailSendTasksNum] [int] NOT NULL,
	[AbandonUsersNum] [int] NOT NULL,
 CONSTRAINT [PK_dbo.SchedulerLogs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RoleUsers]    Script Date: 12/19/2013 15:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleUsers](
	[Role_RoleId] [bigint] NOT NULL,
	[User_UserId] [bigint] NOT NULL,
 CONSTRAINT [PK_dbo.RoleUsers] PRIMARY KEY CLUSTERED 
(
	[Role_RoleId] ASC,
	[User_UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RoleSkillSets]    Script Date: 12/19/2013 15:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleSkillSets](
	[Role_RoleId] [bigint] NOT NULL,
	[SkillSet_SkillSetId] [bigint] NOT NULL,
 CONSTRAINT [PK_dbo.RoleSkillSets] PRIMARY KEY CLUSTERED 
(
	[Role_RoleId] ASC,
	[SkillSet_SkillSetId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ParameterUsers]    Script Date: 12/19/2013 15:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ParameterUsers](
	[Parameter_ParameterName] [nvarchar](4) NOT NULL,
	[User_UserId] [bigint] NOT NULL,
 CONSTRAINT [PK_dbo.StoreUsers] PRIMARY KEY CLUSTERED 
(
	[Parameter_ParameterName] ASC,
	[User_UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NotificationLogRecords]    Script Date: 12/19/2013 15:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationLogRecords](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[NotificationRecType] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[InvocationMarker] [nvarchar](36) NULL,
	[RefId] [nvarchar](36) NULL,
	[SourceName] [nvarchar](255) NULL,
	[Text] [nvarchar](max) NOT NULL,
	[NotificationGroup_Id] [bigint] NOT NULL,
 CONSTRAINT [PK_dbo.NotificationLogRecords] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NotificationLogGroupUsers]    Script Date: 12/19/2013 15:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationLogGroupUsers](
	[NotificationLogGroup_Id] [bigint] NOT NULL,
	[User_UserId] [bigint] NOT NULL,
 CONSTRAINT [PK_dbo.NotificationLogGroupUsers] PRIMARY KEY CLUSTERED 
(
	[NotificationLogGroup_Id] ASC,
	[User_UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserLogs]    Script Date: 12/19/2013 15:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserLogs](
	[UserLogId] [bigint] IDENTITY(1,1) NOT NULL,
	[LoginDate] [datetime] NOT NULL,
	[LogoutDate] [datetime] NULL,
	[AbandonedQty] [int] NOT NULL,
	[ProcessedQty] [int] NOT NULL,
	[User_UserId] [bigint] NOT NULL,
	[SkillSet_SkillSetId] [bigint] NOT NULL,
 CONSTRAINT [PK_dbo.UserLogs] PRIMARY KEY CLUSTERED 
(
	[UserLogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserActions]    Script Date: 12/19/2013 15:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserActions](
	[UserActionId] [bigint] IDENTITY(1,1) NOT NULL,
	[ActionTime] [datetime] NOT NULL,
	[ActionType] [int] NOT NULL,
	[UserLog_UserLogId] [bigint] NOT NULL,
	[Task_TaskId] [bigint] NOT NULL,
 CONSTRAINT [PK_dbo.UserActions] PRIMARY KEY CLUSTERED 
(
	[UserActionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Task_TaskId] ON [dbo].[UserActions] 
(
	[Task_TaskId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_UserLog_UserLogId] ON [dbo].[UserActions] 
(
	[UserLog_UserLogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserAvailableSkillSets]    Script Date: 12/19/2013 15:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserAvailableSkillSets](
	[UserId] [bigint] NOT NULL,
	[SkillSetId] [bigint] NOT NULL,
 CONSTRAINT [PK_dbo.UserAvailableSkillSets] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[SkillSetId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserSkillSets]    Script Date: 12/19/2013 15:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserSkillSets](
	[User_UserId] [bigint] NOT NULL,
	[SkillSet_SkillSetId] [bigint] NOT NULL,
 CONSTRAINT [PK_dbo.UserSkillSets] PRIMARY KEY CLUSTERED 
(
	[User_UserId] ASC,
	[SkillSet_SkillSetId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TaskCompletionQueueLogs]    Script Date: 12/19/2013 15:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaskCompletionQueueLogs](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[FromStatus] [int] NOT NULL,
	[ToStatus] [int] NOT NULL,
	[HasError] [bit] NOT NULL,
	[Message] [nvarchar](max) NULL,
	[InvocationMarker] [nvarchar](36) NULL,
	[TaskCompletionQueue_TaskQueueId] [bigint] NULL,
 CONSTRAINT [PK_dbo.TaskCompletionQueueLogs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Events]    Script Date: 12/19/2013 15:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events](
	[EventId] [bigint] IDENTITY(1,1) NOT NULL,
	[Message] [nvarchar](255) NULL,
	[SerialazedData] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Ticket_TicketId] [nvarchar](36) NULL,
 CONSTRAINT [PK_dbo.Events] PRIMARY KEY CLUSTERED 
(
	[EventId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF__Parameter__Start__2D27B809]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[Parameters] ADD  DEFAULT ('00:00:00') FOR [StartWorkingTime]
GO
/****** Object:  Default [DF__Parameter__EndWo__2E1BDC42]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[Parameters] ADD  DEFAULT ('00:00:00') FOR [EndWorkingTime]
GO
/****** Object:  Default [DF__Parameter__UtcSt__2F10007B]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[Parameters] ADD  DEFAULT ((0)) FOR [UtcStartWorkingTime]
GO
/****** Object:  Default [DF__Parameter__UtcEn__300424B4]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[Parameters] ADD  DEFAULT ((0)) FOR [UtcEndWorkingTime]
GO
/****** Object:  Default [DF__Parameter__CanSy__30F848ED]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[Parameters] ADD  DEFAULT ((0)) FOR [CanSynchronize]
GO
/****** Object:  Default [DF__Roles__NeedNotif__31EC6D26]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[Roles] ADD  DEFAULT ((0)) FOR [NeedNotification]
GO
/****** Object:  Default [DF__SkillSets__IsDel__32E0915F]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[SkillSets] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF__TaskTypes__CanPo__33D4B598]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[TaskTypes] ADD  DEFAULT ((0)) FOR [CanPostponeCallingWorkflow]
GO
/****** Object:  ForeignKey [FK_dbo.Events_dbo.Tickets_Ticket_TicketId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[Events]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Events_dbo.Tickets_Ticket_TicketId] FOREIGN KEY([Ticket_TicketId])
REFERENCES [dbo].[Tickets] ([TicketId])
GO
ALTER TABLE [dbo].[Events] CHECK CONSTRAINT [FK_dbo.Events_dbo.Tickets_Ticket_TicketId]
GO
/****** Object:  ForeignKey [FK_dbo.NotificationLogGroupUsers_dbo.NotificationLogGroups_NotificationLogGroup_Id]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[NotificationLogGroupUsers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.NotificationLogGroupUsers_dbo.NotificationLogGroups_NotificationLogGroup_Id] FOREIGN KEY([NotificationLogGroup_Id])
REFERENCES [dbo].[NotificationLogGroups] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[NotificationLogGroupUsers] CHECK CONSTRAINT [FK_dbo.NotificationLogGroupUsers_dbo.NotificationLogGroups_NotificationLogGroup_Id]
GO
/****** Object:  ForeignKey [FK_dbo.NotificationLogGroupUsers_dbo.Users_User_UserId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[NotificationLogGroupUsers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.NotificationLogGroupUsers_dbo.Users_User_UserId] FOREIGN KEY([User_UserId])
REFERENCES [dbo].[Users] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[NotificationLogGroupUsers] CHECK CONSTRAINT [FK_dbo.NotificationLogGroupUsers_dbo.Users_User_UserId]
GO
/****** Object:  ForeignKey [FK_dbo.NotificationLogRecords_dbo.NotificationLogGroups_NotificationGroup_Id]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[NotificationLogRecords]  WITH CHECK ADD  CONSTRAINT [FK_dbo.NotificationLogRecords_dbo.NotificationLogGroups_NotificationGroup_Id] FOREIGN KEY([NotificationGroup_Id])
REFERENCES [dbo].[NotificationLogGroups] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[NotificationLogRecords] CHECK CONSTRAINT [FK_dbo.NotificationLogRecords_dbo.NotificationLogGroups_NotificationGroup_Id]
GO
/****** Object:  ForeignKey [FK_dbo.ParameterUsers_dbo.Parameters_Parameter_ParameterName]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[ParameterUsers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ParameterUsers_dbo.Parameters_Parameter_ParameterName] FOREIGN KEY([Parameter_ParameterName])
REFERENCES [dbo].[Parameters] ([ParameterName])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ParameterUsers] CHECK CONSTRAINT [FK_dbo.ParameterUsers_dbo.Parameters_Parameter_ParameterName]
GO
/****** Object:  ForeignKey [FK_dbo.ParameterUsers_dbo.Users_User_UserId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[ParameterUsers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ParameterUsers_dbo.Users_User_UserId] FOREIGN KEY([User_UserId])
REFERENCES [dbo].[Users] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ParameterUsers] CHECK CONSTRAINT [FK_dbo.ParameterUsers_dbo.Users_User_UserId]
GO
/****** Object:  ForeignKey [FK_dbo.RoleSkillSets_dbo.Roles_Role_RoleId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[RoleSkillSets]  WITH CHECK ADD  CONSTRAINT [FK_dbo.RoleSkillSets_dbo.Roles_Role_RoleId] FOREIGN KEY([Role_RoleId])
REFERENCES [dbo].[Roles] ([RoleId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[RoleSkillSets] CHECK CONSTRAINT [FK_dbo.RoleSkillSets_dbo.Roles_Role_RoleId]
GO
/****** Object:  ForeignKey [FK_dbo.RoleSkillSets_dbo.SkillSets_SkillSet_SkillSetId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[RoleSkillSets]  WITH CHECK ADD  CONSTRAINT [FK_dbo.RoleSkillSets_dbo.SkillSets_SkillSet_SkillSetId] FOREIGN KEY([SkillSet_SkillSetId])
REFERENCES [dbo].[SkillSets] ([SkillSetId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[RoleSkillSets] CHECK CONSTRAINT [FK_dbo.RoleSkillSets_dbo.SkillSets_SkillSet_SkillSetId]
GO
/****** Object:  ForeignKey [FK_dbo.RoleUsers_dbo.Roles_Role_RoleId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[RoleUsers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.RoleUsers_dbo.Roles_Role_RoleId] FOREIGN KEY([Role_RoleId])
REFERENCES [dbo].[Roles] ([RoleId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[RoleUsers] CHECK CONSTRAINT [FK_dbo.RoleUsers_dbo.Roles_Role_RoleId]
GO
/****** Object:  ForeignKey [FK_dbo.RoleUsers_dbo.Users_User_UserId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[RoleUsers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.RoleUsers_dbo.Users_User_UserId] FOREIGN KEY([User_UserId])
REFERENCES [dbo].[Users] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[RoleUsers] CHECK CONSTRAINT [FK_dbo.RoleUsers_dbo.Users_User_UserId]
GO
/****** Object:  ForeignKey [FK_dbo.TaskCompletionQueueLogs_dbo.TaskCompletionQueues_TaskCompletionQueue_TaskQueueId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[TaskCompletionQueueLogs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TaskCompletionQueueLogs_dbo.TaskCompletionQueues_TaskCompletionQueue_TaskQueueId] FOREIGN KEY([TaskCompletionQueue_TaskQueueId])
REFERENCES [dbo].[TaskCompletionQueues] ([TaskQueueId])
GO
ALTER TABLE [dbo].[TaskCompletionQueueLogs] CHECK CONSTRAINT [FK_dbo.TaskCompletionQueueLogs_dbo.TaskCompletionQueues_TaskCompletionQueue_TaskQueueId]
GO
/****** Object:  ForeignKey [FK_dbo.TaskCompletionQueues_dbo.ExternalSystems_ExternalSystem_ExternalSystemId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[TaskCompletionQueues]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TaskCompletionQueues_dbo.ExternalSystems_ExternalSystem_ExternalSystemId] FOREIGN KEY([ExternalSystem_ExternalSystemId])
REFERENCES [dbo].[ExternalSystems] ([ExternalSystemId])
GO
ALTER TABLE [dbo].[TaskCompletionQueues] CHECK CONSTRAINT [FK_dbo.TaskCompletionQueues_dbo.ExternalSystems_ExternalSystem_ExternalSystemId]
GO
/****** Object:  ForeignKey [FK_dbo.TaskFields_dbo.FieldTypes_FieldType_FieldTypeId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[TaskFields]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TaskFields_dbo.FieldTypes_FieldType_FieldTypeId] FOREIGN KEY([FieldType_FieldTypeId])
REFERENCES [dbo].[FieldTypes] ([FieldTypeId])
GO
ALTER TABLE [dbo].[TaskFields] CHECK CONSTRAINT [FK_dbo.TaskFields_dbo.FieldTypes_FieldType_FieldTypeId]
GO
/****** Object:  ForeignKey [FK_dbo.TaskFields_dbo.TicketTasks_TicketTask_TaskId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[TaskFields]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TaskFields_dbo.TicketTasks_TicketTask_TaskId] FOREIGN KEY([TicketTask_TaskId])
REFERENCES [dbo].[TicketTasks] ([TaskId])
GO
ALTER TABLE [dbo].[TaskFields] CHECK CONSTRAINT [FK_dbo.TaskFields_dbo.TicketTasks_TicketTask_TaskId]
GO
/****** Object:  ForeignKey [FK_dbo.TaskOperations_dbo.SkillSets_SkillSet_SkillSetId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[TaskOperations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TaskOperations_dbo.SkillSets_SkillSet_SkillSetId] FOREIGN KEY([SkillSet_SkillSetId])
REFERENCES [dbo].[SkillSets] ([SkillSetId])
GO
ALTER TABLE [dbo].[TaskOperations] CHECK CONSTRAINT [FK_dbo.TaskOperations_dbo.SkillSets_SkillSet_SkillSetId]
GO
/****** Object:  ForeignKey [FK_dbo.TaskOperations_dbo.TaskOperations_RefOperation_OperationId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[TaskOperations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TaskOperations_dbo.TaskOperations_RefOperation_OperationId] FOREIGN KEY([RefOperation_OperationId])
REFERENCES [dbo].[TaskOperations] ([OperationId])
GO
ALTER TABLE [dbo].[TaskOperations] CHECK CONSTRAINT [FK_dbo.TaskOperations_dbo.TaskOperations_RefOperation_OperationId]
GO
/****** Object:  ForeignKey [FK_dbo.TaskOperations_dbo.TicketTasks_TicketTask_TaskId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[TaskOperations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TaskOperations_dbo.TicketTasks_TicketTask_TaskId] FOREIGN KEY([TicketTask_TaskId])
REFERENCES [dbo].[TicketTasks] ([TaskId])
GO
ALTER TABLE [dbo].[TaskOperations] CHECK CONSTRAINT [FK_dbo.TaskOperations_dbo.TicketTasks_TicketTask_TaskId]
GO
/****** Object:  ForeignKey [FK_dbo.TaskOperations_dbo.Users_User_UserId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[TaskOperations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TaskOperations_dbo.Users_User_UserId] FOREIGN KEY([User_UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[TaskOperations] CHECK CONSTRAINT [FK_dbo.TaskOperations_dbo.Users_User_UserId]
GO
/****** Object:  ForeignKey [FK_dbo.TaskTypes_dbo.ExternalSystems_ExternalSystem_ExternalSystemId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[TaskTypes]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TaskTypes_dbo.ExternalSystems_ExternalSystem_ExternalSystemId] FOREIGN KEY([ExternalSystem_ExternalSystemId])
REFERENCES [dbo].[ExternalSystems] ([ExternalSystemId])
GO
ALTER TABLE [dbo].[TaskTypes] CHECK CONSTRAINT [FK_dbo.TaskTypes_dbo.ExternalSystems_ExternalSystem_ExternalSystemId]
GO
/****** Object:  ForeignKey [FK_dbo.TaskTypes_dbo.NotificationLogGroups_NotificationGroup_Id]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[TaskTypes]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TaskTypes_dbo.NotificationLogGroups_NotificationGroup_Id] FOREIGN KEY([NotificationGroup_Id])
REFERENCES [dbo].[NotificationLogGroups] ([Id])
GO
ALTER TABLE [dbo].[TaskTypes] CHECK CONSTRAINT [FK_dbo.TaskTypes_dbo.NotificationLogGroups_NotificationGroup_Id]
GO
/****** Object:  ForeignKey [FK_dbo.TaskTypes_dbo.SkillSets_SkillSet_SkillSetId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[TaskTypes]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TaskTypes_dbo.SkillSets_SkillSet_SkillSetId] FOREIGN KEY([SkillSet_SkillSetId])
REFERENCES [dbo].[SkillSets] ([SkillSetId])
GO
ALTER TABLE [dbo].[TaskTypes] CHECK CONSTRAINT [FK_dbo.TaskTypes_dbo.SkillSets_SkillSet_SkillSetId]
GO
/****** Object:  ForeignKey [FK_dbo.TaskTypes_dbo.SkillSets_SlaSkillSet_SkillSetId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[TaskTypes]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TaskTypes_dbo.SkillSets_SlaSkillSet_SkillSetId] FOREIGN KEY([SlaSkillSet_SkillSetId])
REFERENCES [dbo].[SkillSets] ([SkillSetId])
GO
ALTER TABLE [dbo].[TaskTypes] CHECK CONSTRAINT [FK_dbo.TaskTypes_dbo.SkillSets_SlaSkillSet_SkillSetId]
GO
/****** Object:  ForeignKey [FK_dbo.Tickets_dbo.ExternalSystems_ExternalSystem_ExternalSystemId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[Tickets]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Tickets_dbo.ExternalSystems_ExternalSystem_ExternalSystemId] FOREIGN KEY([ExternalSystem_ExternalSystemId])
REFERENCES [dbo].[ExternalSystems] ([ExternalSystemId])
GO
ALTER TABLE [dbo].[Tickets] CHECK CONSTRAINT [FK_dbo.Tickets_dbo.ExternalSystems_ExternalSystem_ExternalSystemId]
GO
/****** Object:  ForeignKey [FK_dbo.TicketTasks_dbo.Outcomes_Outcomes_Id]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[TicketTasks]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TicketTasks_dbo.Outcomes_Outcomes_Id] FOREIGN KEY([Outcomes_Id])
REFERENCES [dbo].[Outcomes] ([Id])
GO
ALTER TABLE [dbo].[TicketTasks] CHECK CONSTRAINT [FK_dbo.TicketTasks_dbo.Outcomes_Outcomes_Id]
GO
/****** Object:  ForeignKey [FK_dbo.TicketTasks_dbo.SkillSets_CurrentSkillSet_SkillSetId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[TicketTasks]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TicketTasks_dbo.SkillSets_CurrentSkillSet_SkillSetId] FOREIGN KEY([CurrentSkillSet_SkillSetId])
REFERENCES [dbo].[SkillSets] ([SkillSetId])
GO
ALTER TABLE [dbo].[TicketTasks] CHECK CONSTRAINT [FK_dbo.TicketTasks_dbo.SkillSets_CurrentSkillSet_SkillSetId]
GO
/****** Object:  ForeignKey [FK_dbo.TicketTasks_dbo.TaskCompletionQueues_TaskCompletionQueue_TaskQueueId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[TicketTasks]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TicketTasks_dbo.TaskCompletionQueues_TaskCompletionQueue_TaskQueueId] FOREIGN KEY([TaskCompletionQueue_TaskQueueId])
REFERENCES [dbo].[TaskCompletionQueues] ([TaskQueueId])
GO
ALTER TABLE [dbo].[TicketTasks] CHECK CONSTRAINT [FK_dbo.TicketTasks_dbo.TaskCompletionQueues_TaskCompletionQueue_TaskQueueId]
GO
/****** Object:  ForeignKey [FK_dbo.TicketTasks_dbo.TaskOperations_LastSkillSetOperation_OperationId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[TicketTasks]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TicketTasks_dbo.TaskOperations_LastSkillSetOperation_OperationId] FOREIGN KEY([LastSkillSetOperation_OperationId])
REFERENCES [dbo].[TaskOperations] ([OperationId])
GO
ALTER TABLE [dbo].[TicketTasks] CHECK CONSTRAINT [FK_dbo.TicketTasks_dbo.TaskOperations_LastSkillSetOperation_OperationId]
GO
/****** Object:  ForeignKey [FK_dbo.TicketTasks_dbo.TaskOperations_LastUserOperation_OperationId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[TicketTasks]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TicketTasks_dbo.TaskOperations_LastUserOperation_OperationId] FOREIGN KEY([LastUserOperation_OperationId])
REFERENCES [dbo].[TaskOperations] ([OperationId])
GO
ALTER TABLE [dbo].[TicketTasks] CHECK CONSTRAINT [FK_dbo.TicketTasks_dbo.TaskOperations_LastUserOperation_OperationId]
GO
/****** Object:  ForeignKey [FK_dbo.TicketTasks_dbo.TaskTypes_TaskType_TaskTypeId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[TicketTasks]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TicketTasks_dbo.TaskTypes_TaskType_TaskTypeId] FOREIGN KEY([TaskType_TaskTypeId])
REFERENCES [dbo].[TaskTypes] ([TaskTypeId])
GO
ALTER TABLE [dbo].[TicketTasks] CHECK CONSTRAINT [FK_dbo.TicketTasks_dbo.TaskTypes_TaskType_TaskTypeId]
GO
/****** Object:  ForeignKey [FK_dbo.TicketTasks_dbo.Tickets_Ticket_TicketId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[TicketTasks]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TicketTasks_dbo.Tickets_Ticket_TicketId] FOREIGN KEY([Ticket_TicketId])
REFERENCES [dbo].[Tickets] ([TicketId])
GO
ALTER TABLE [dbo].[TicketTasks] CHECK CONSTRAINT [FK_dbo.TicketTasks_dbo.Tickets_Ticket_TicketId]
GO
/****** Object:  ForeignKey [FK_dbo.TicketTasks_dbo.Users_CurrentAssignee_UserId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[TicketTasks]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TicketTasks_dbo.Users_CurrentAssignee_UserId] FOREIGN KEY([CurrentAssignee_UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[TicketTasks] CHECK CONSTRAINT [FK_dbo.TicketTasks_dbo.Users_CurrentAssignee_UserId]
GO
/****** Object:  ForeignKey [FK_dbo.TicketTasks_dbo.Users_InformedUser_UserId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[TicketTasks]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TicketTasks_dbo.Users_InformedUser_UserId] FOREIGN KEY([InformedUser_UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[TicketTasks] CHECK CONSTRAINT [FK_dbo.TicketTasks_dbo.Users_InformedUser_UserId]
GO
/****** Object:  ForeignKey [FK_dbo.TicketTasks_dbo.Users_Performer_UserId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[TicketTasks]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TicketTasks_dbo.Users_Performer_UserId] FOREIGN KEY([Performer_UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[TicketTasks] CHECK CONSTRAINT [FK_dbo.TicketTasks_dbo.Users_Performer_UserId]
GO
/****** Object:  ForeignKey [FK_dbo.UserActions_dbo.TicketTasks_Task_TaskId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[UserActions]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserActions_dbo.TicketTasks_Task_TaskId] FOREIGN KEY([Task_TaskId])
REFERENCES [dbo].[TicketTasks] ([TaskId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserActions] CHECK CONSTRAINT [FK_dbo.UserActions_dbo.TicketTasks_Task_TaskId]
GO
/****** Object:  ForeignKey [FK_dbo.UserActions_dbo.UserLogs_UserLog_UserLogId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[UserActions]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserActions_dbo.UserLogs_UserLog_UserLogId] FOREIGN KEY([UserLog_UserLogId])
REFERENCES [dbo].[UserLogs] ([UserLogId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserActions] CHECK CONSTRAINT [FK_dbo.UserActions_dbo.UserLogs_UserLog_UserLogId]
GO
/****** Object:  ForeignKey [FK_dbo.UserAvailableSkillSets_dbo.SkillSets_SkillSetId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[UserAvailableSkillSets]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserAvailableSkillSets_dbo.SkillSets_SkillSetId] FOREIGN KEY([SkillSetId])
REFERENCES [dbo].[SkillSets] ([SkillSetId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserAvailableSkillSets] CHECK CONSTRAINT [FK_dbo.UserAvailableSkillSets_dbo.SkillSets_SkillSetId]
GO
/****** Object:  ForeignKey [FK_dbo.UserAvailableSkillSets_dbo.Users_UserId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[UserAvailableSkillSets]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserAvailableSkillSets_dbo.Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserAvailableSkillSets] CHECK CONSTRAINT [FK_dbo.UserAvailableSkillSets_dbo.Users_UserId]
GO
/****** Object:  ForeignKey [FK_dbo.UserLogs_dbo.SkillSets_SkillSet_SkillSetId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[UserLogs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserLogs_dbo.SkillSets_SkillSet_SkillSetId] FOREIGN KEY([SkillSet_SkillSetId])
REFERENCES [dbo].[SkillSets] ([SkillSetId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserLogs] CHECK CONSTRAINT [FK_dbo.UserLogs_dbo.SkillSets_SkillSet_SkillSetId]
GO
/****** Object:  ForeignKey [FK_dbo.UserLogs_dbo.Users_User_UserId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[UserLogs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserLogs_dbo.Users_User_UserId] FOREIGN KEY([User_UserId])
REFERENCES [dbo].[Users] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserLogs] CHECK CONSTRAINT [FK_dbo.UserLogs_dbo.Users_User_UserId]
GO
/****** Object:  ForeignKey [FK_dbo.UserSkillSets_dbo.SkillSets_SkillSet_SkillSetId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[UserSkillSets]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserSkillSets_dbo.SkillSets_SkillSet_SkillSetId] FOREIGN KEY([SkillSet_SkillSetId])
REFERENCES [dbo].[SkillSets] ([SkillSetId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserSkillSets] CHECK CONSTRAINT [FK_dbo.UserSkillSets_dbo.SkillSets_SkillSet_SkillSetId]
GO
/****** Object:  ForeignKey [FK_dbo.UserSkillSets_dbo.Users_User_UserId]    Script Date: 12/19/2013 15:44:11 ******/
ALTER TABLE [dbo].[UserSkillSets]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserSkillSets_dbo.Users_User_UserId] FOREIGN KEY([User_UserId])
REFERENCES [dbo].[Users] ([UserId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserSkillSets] CHECK CONSTRAINT [FK_dbo.UserSkillSets_dbo.Users_User_UserId]
GO
