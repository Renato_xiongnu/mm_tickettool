/****** Object:  StoredProcedure [dbo].[rp_Tasks]    Script Date: 09/06/2013 14:57:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rp_Tasks]
-- TicketTool.Tasks
@StartDate AS DATETIME,
@EndDate AS DATETIME, 
@OffsetInMinutes AS INTEGER = 240
AS
BEGIN
SELECT @StartDate = CAST(DATEADD(MINUTE, -@OffsetInMinutes, @StartDate) AS DATETIME)
SELECT @EndDate = CAST(DATEADD(MINUTE, -@OffsetInMinutes, @EndDate) AS DATETIME)

SELECT 
--      tt.TaskId,
      tt.Name Task
      ,tt.CurrentOutcome
      ,CAST(DATEADD(MINUTE, @OffsetInMinutes, tt.CreateDate) AS DATE) CreateDate
      ,CAST(DATEADD(MINUTE, @OffsetInMinutes, tt.CreateDate) AS TIME(0)) CreateTime 
      ,CAST(DATEADD(MINUTE, @OffsetInMinutes, tt.FinishDate) AS DATE) FinishDate
      ,CAST(DATEADD(MINUTE, @OffsetInMinutes, tt.FinishDate) AS TIME(0)) FinishTime 
      ,DATEDIFF(ss, tt.CreateDate, ISNULL(tt.FinishDate, GETDATE())) [Time]
      ,u.Name [User]
      ,t.WorkItemId OrderId
      --,TaskStatus
      --,Escalated
      --,SkillSetDateAssigned
      --,SkillSetRuntime
      --,UserDateAssigned
      --,ReturnToQueueAfterDate
      --,UserRuntime
      --,Ticket_TicketId
      --,InformedUser_UserId
      --,CurrentAssignee_UserId
      --,LastUserOperation_OperationId
  FROM TicketTasks tt
  INNER JOIN Tickets t ON t.TicketId = tt.Ticket_TicketId
  INNER JOIN TaskOperations o ON o.OperationId = tt.LastUserOperation_OperationId
  INNER JOIN Users u ON u.UserId = o.User_UserId
  WHERE (tt.FinishDate IS NULL AND tt.CreateDate < @EndDate) 
        OR (tt.FinishDate >= @StartDate AND tt.FinishDate < @EndDate)
END
GO