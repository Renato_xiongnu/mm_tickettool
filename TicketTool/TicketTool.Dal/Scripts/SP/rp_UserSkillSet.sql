/****** Object:  StoredProcedure [dbo].[rp_UserSkillSet]    Script Date: 09/06/2013 14:08:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rp_UserSkillSet]
-- TicketTool.UserSkillSet
@StartDate AS DATETIME, 
@EndDate AS DATETIME, 
@OffsetInMinutes AS INTEGER = 240
AS
BEGIN
--declare @StartDate DATETIME = '2013-04-16'
--declare @EndDate DATETIME = '2013-04-17'
SELECT @StartDate = CAST(DATEADD(MINUTE, -@OffsetInMinutes, @StartDate) AS DATETIME)
SELECT @EndDate = CAST(DATEADD(MINUTE, -@OffsetInMinutes, @EndDate) AS DATETIME)

SELECT 
u.Name [User], 
s.Name SkillSet, 
o.FullTime, 
o.Tasks,
(SELECT COUNT(DISTINCT ot.TicketTask_TaskId) FROM TaskOperations ot WHERE ot.User_UserId = o.User_UserId AND ot.SkillSet_SkillSetId = o.SkillSet_SkillSetId
    AND ToStatus = 100 AND UpdateDate >= @StartDate AND UpdateDate <= @EndDate ) FinishedTasks,
o.AvgTime,
o.MinTime,
o.MaxTime,
l.SkillSetTime
FROM
	(SELECT 
	o.User_UserId, 	o.SkillSet_SkillSetId, 
	SUM(DATEDIFF(ms, '00:00:00.000', UserRuntime))/1000 FullTime,
	COUNT(DISTINCT o.TicketTask_TaskId) Tasks,
	AVG(DATEDIFF(ms, '00:00:00.000', UserRuntime))/1000 AvgTime,
	MIN(DATEDIFF(ms, '00:00:00.000', UserRuntime))/1000 MinTime,
	MAX(DATEDIFF(ms, '00:00:00.000', UserRuntime))/1000 MaxTime
	FROM [dbo].[TaskOperations] o
	WHERE FromStatus = 30 AND UpdateDate >= @StartDate AND UpdateDate <= @EndDate AND UserRuntime > '00:00:00.999'
	GROUP BY  o.User_UserId, o.SkillSet_SkillSetId ) o
INNER JOIN 
	(SELECT l.User_UserId, l.SkillSet_SkillSetId,
	SUM(DATEDIFF(SECOND, (CASE WHEN l.LoginDate >= @StartDate THEN l.LoginDate ElSE @StartDate END), (CASE WHEN ISNULL(l.LogoutDate, GETDATE()) <= @EndDate THEN ISNULL(l.LogoutDate, GETDATE()) ElSE @EndDate END))) SkillSetTime
	FROM UserLogs l
	WHERE (l.LoginDate >= @StartDate AND l.LoginDate < @EndDate) 
	    OR (ISNULL(l.LogoutDate, GETDATE()) >= @StartDate AND ISNULL(l.LogoutDate, GETDATE()) <= @EndDate)
	    OR (l.LoginDate < @StartDate AND ISNULL(l.LogoutDate, GETDATE()) > @EndDate)
	GROUP BY l.User_UserId, l.SkillSet_SkillSetId) l
	ON o.User_UserId = l.User_UserId AND o.SkillSet_SkillSetId = l.SkillSet_SkillSetId
INNER JOIN RoleUsers r ON r.User_UserId = o.User_UserId 
INNER JOIN Users u ON u.UserId = o.User_UserId
INNER JOIN SkillSets s ON s.SkillSetId = o.SkillSet_SkillSetId 
order by 1,2

END
GO