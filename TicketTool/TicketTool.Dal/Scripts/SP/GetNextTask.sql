﻿---!V1_0
CREATE PROCEDURE [dbo].[SP_GetNextTask]	
	 @UserId BIGINT,
	 @SkillSetId BIGINT,
	 @StartStatus INT,
	 @EndStatus INT,	 
	 @MaxStatus INT,
	 @SecondStatus INT
	 --@FromDate DATETIME	 	 
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @TaskId BIGINT;
	DECLARE @IsParametaraseble BIT;
	SELECT TOP 1 @IsParametaraseble = s.IsParameterizable from dbo.SkillSets s
		where s.SkillSetId = @SkillSetId
		
	IF @IsParametaraseble = 0
	BEGIN
		SELECT TOP 1 @TaskId = task.TaskId from [dbo].[TicketTasks] task WITH (READPAST, updlock)						
			INNER JOIN [dbo].[UserSkillSets] userSkillSet on userSkillSet.SkillSet_SkillSetId = @SkillSetId
			INNER JOIN (Select * from [dbo].[GetOperationGroup](@StartStatus, @EndStatus)) 
			opGroup on task.TaskId = opGroup.TaskId
			where task.TaskStatus < @MaxStatus AND userSkillSet.User_UserId = @UserId AND task.CurrentSkillSet_SkillSetId = @SkillSetId AND task.CurrentAssignee_UserId is NULL AND DiffDate is NULL
			AND (task.TaskStatus <> @SecondStatus OR ReturnToQueueAfterDate IS NULL) 
													 -- OR (task.UpdateDate < @FromDate) )
	END		
	ELSE
	BEGIN
		SELECT TOP 1 @TaskId = task.TaskId from [dbo].[TicketTasks] task WITH (READPAST, updlock)						
			INNER JOIN [dbo].[UserSkillSets] userSkillSet on userSkillSet.SkillSet_SkillSetId = @SkillSetId
			INNER JOIN [dbo].Tickets ticket on ticket.TicketId = task.Ticket_TicketId			
			INNER JOIN (Select * from [dbo].[GetOperationGroup](@StartStatus, @EndStatus)) 
			opGroup on task.TaskId = opGroup.TaskId
			where task.TaskStatus < @MaxStatus AND userSkillSet.User_UserId = @UserId AND task.CurrentSkillSet_SkillSetId = @SkillSetId AND task.CurrentAssignee_UserId is NULL AND DiffDate is NULL
			AND (task.TaskStatus <> @SecondStatus OR ReturnToQueueAfterDate IS NULL)
													 -- OR (task.UpdateDate < @FromDate) )			
			AND EXISTS(SELECT 1 from dbo.StoreUsers susr where susr.User_UserId = @UserId AND susr.Store_SapCode = ticket.Parameter)
	END
		SELECT @TaskId
	
END
---!V1_2-->
ALTER PROCEDURE [dbo].[SP_GetNextTask]	
	 @UserId BIGINT,
	 @SkillSetId BIGINT,
	 @StartStatus INT,
	 @EndStatus INT,	 
	 @MaxStatus INT,
	 @SecondStatus INT,
	 @CurrentUtcTime INT
	 --@FromDate DATETIME	 	 
AS
BEGIN
	SET NOCOUNT ON;
	
	
	DECLARE @TaskId BIGINT;
	DECLARE @IsParametaraseble BIT;
	SELECT TOP 1 @IsParametaraseble = s.IsParameterizable from dbo.SkillSets s
		where s.SkillSetId = @SkillSetId
	
	
	 	
	--INNER JOIN  from [dbo].LocalStoreTime(@CurrentUtcTime) StoreTime on StoreTime.SapCode = ticket.WorkItemId
		
	IF @IsParametaraseble = 0
	BEGIN
		SELECT TOP 1 @TaskId = TaskId FROM
			(
		    SELECT task.TaskId, task.CreateDate, relInfo.IsWorkingHours from [dbo].[TicketTasks] task WITH (READPAST, updlock)						
			INNER JOIN [dbo].[UserSkillSets] userSkillSet on userSkillSet.SkillSet_SkillSetId = task.CurrentSkillSet_SkillSetId 
			INNER JOIN [dbo].[GetOperationGroup](@StartStatus, @EndStatus) opGroup on task.TaskId = opGroup.TaskId
			INNER JOIN [dbo].GetWokingHoursOfStore(@CurrentUtcTime) relInfo on relInfo.TicketId = task.Ticket_TicketId
			where task.TaskStatus < @MaxStatus AND userSkillSet.SkillSet_SkillSetId = @SkillSetId AND userSkillSet.User_UserId = @UserId AND task.CurrentAssignee_UserId is NULL AND DiffDate is NULL
			AND (task.TaskStatus <> @SecondStatus OR ReturnToQueueAfterDate IS NULL) 
													 -- OR (task.UpdateDate < @FromDate) )
			) as taskQuery
			order by taskQuery.IsWorkingHours desc, taskQuery.CreateDate asc
	END		
	ELSE
	BEGIN
		SELECT top 1 @TaskId = TaskId FROM (
		SELECT task.TaskId, task.CreateDate, relInfo.IsWorkingHours  from [dbo].[TicketTasks] task WITH (READPAST, updlock)
			INNER JOIN [dbo].[UserSkillSets] userSkillSet on userSkillSet.SkillSet_SkillSetId = task.CurrentSkillSet_SkillSetId 			
			INNER JOIN [dbo].[GetOperationGroup](@StartStatus, @EndStatus) opGroup on task.TaskId = opGroup.TaskId
			INNER JOIN [dbo].GetWokingHoursOfStore(@CurrentUtcTime) relInfo on relInfo.TicketId = task.Ticket_TicketId
			where task.TaskStatus < @MaxStatus AND userSkillSet.SkillSet_SkillSetId = @SkillSetId AND userSkillSet.User_UserId = @UserId AND task.CurrentAssignee_UserId is NULL AND DiffDate is NULL
			AND (task.TaskStatus <> @SecondStatus OR ReturnToQueueAfterDate IS NULL) -- OR (task.UpdateDate < @FromDate) )			
			AND EXISTS(SELECT 1 from dbo.StoreUsers susr where susr.User_UserId = @UserId AND susr.Store_SapCode = relInfo.SapCode)
			) as taskQuery 
			order by taskQuery.IsWorkingHours desc, taskQuery.CreateDate asc
	END
		SELECT @TaskId
		
					

END
---!V2_0-->
ALTER PROCEDURE [dbo].[SP_GetNextTask]	
	 @UserId BIGINT,
	 @SkillSetId BIGINT,
	 @StartStatus INT,
	 @EndStatus INT,	 
	 @MaxStatus INT,
	 @SecondStatus INT,
	 @CurrentUtcTime INT
AS
BEGIN
	SET NOCOUNT ON;
	
	
	DECLARE @TaskId BIGINT;
	DECLARE @IsParametaraseble BIT;
	SELECT TOP 1 @IsParametaraseble = s.IsParameterizable from dbo.SkillSets s
		where s.SkillSetId = @SkillSetId
			
		
	IF @IsParametaraseble = 0
	BEGIN
		SELECT TOP 1 @TaskId = TaskId FROM
			(
		    SELECT task.TaskId, task.CreateDate, relInfo.IsWorkingHours from [dbo].[TicketTasks] task WITH (READPAST, updlock)						
			INNER JOIN [dbo].[UserSkillSets] userSkillSet on userSkillSet.SkillSet_SkillSetId = task.CurrentSkillSet_SkillSetId 
			INNER JOIN [dbo].[GetOperationGroup](@StartStatus, @EndStatus) opGroup on task.TaskId = opGroup.TaskId
			INNER JOIN [dbo].GetWokingHoursOfStore(@CurrentUtcTime) relInfo on relInfo.TicketId = task.Ticket_TicketId
			where task.TaskStatus < @MaxStatus AND userSkillSet.SkillSet_SkillSetId = @SkillSetId AND userSkillSet.User_UserId = @UserId AND task.CurrentAssignee_UserId is NULL AND DiffDate is NULL
			AND (task.TaskStatus <> @SecondStatus OR ReturnToQueueAfterDate IS NULL) 
													 -- OR (task.UpdateDate < @FromDate) )
			) as taskQuery
			order by taskQuery.IsWorkingHours desc, taskQuery.CreateDate asc
	END		
	ELSE
	BEGIN
		SELECT top 1 @TaskId = TaskId FROM (
		SELECT task.TaskId, task.CreateDate, relInfo.IsWorkingHours  from [dbo].[TicketTasks] task WITH (READPAST, updlock)
			INNER JOIN [dbo].[UserSkillSets] userSkillSet on userSkillSet.SkillSet_SkillSetId = task.CurrentSkillSet_SkillSetId 			
			INNER JOIN [dbo].[GetOperationGroup](@StartStatus, @EndStatus) opGroup on task.TaskId = opGroup.TaskId
			INNER JOIN [dbo].GetWokingHoursOfStore(@CurrentUtcTime) relInfo on relInfo.TicketId = task.Ticket_TicketId
			where task.TaskStatus < @MaxStatus AND userSkillSet.SkillSet_SkillSetId = @SkillSetId AND userSkillSet.User_UserId = @UserId AND task.CurrentAssignee_UserId is NULL AND DiffDate is NULL
			AND (task.TaskStatus <> @SecondStatus OR ReturnToQueueAfterDate IS NULL) -- OR (task.UpdateDate < @FromDate) )			
			AND EXISTS(SELECT 1 from dbo.ParameterUsers susr where susr.User_UserId = @UserId AND susr.Parameter_ParameterName = relInfo.ParameterName)
			) as taskQuery 
			order by taskQuery.IsWorkingHours desc, taskQuery.CreateDate asc
	END
		SELECT @TaskId
		
					

END
--<--