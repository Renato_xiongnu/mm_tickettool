﻿Create PROCEDURE [dbo].[SP_GetAvalibleTasks]
	@SkillSetId BIGINT,
	@StartStatus INT,
    @EndStatus INT,
	@MaxStatus INT	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT task.TaskId, task.TaskStatus, 
			  task.CurrentSkillSet_SkillSetId as CurrentSkillSetId,
			  task.CurrentAssignee_UserId as CurrentAssigneeId, 
			  task.InformedUser_UserId as InformedUserId,
			 ISNULL(task.Parameter,ticket.Parameter) from [dbo].[TicketTasks] task 			
			INNER JOIN [dbo].[Tickets] ticket on ticket.TicketId = task.Ticket_TicketId	
			INNER JOIN [dbo].[SkillSets] skillSet on skillSet.SkillSetId =  task.CurrentSkillSet_SkillSetId						
			INNER JOIN (Select * from [dbo].[GetOperationGroup](@StartStatus, @EndStatus)) 
			opGroup on task.TaskId = opGroup.TaskId
				where task.TaskStatus < @MaxStatus AND SkillSetId = @SkillSetId AND	task.CurrentAssignee_UserId is NULL AND DiffDate is NULL	
END