﻿CREATE PROCEDURE [dbo].[SP_GetPingUserInfo]
	@UserName NVARCHAR(255),
	@FromStatus int,
	@ToStatus int,
	@LastPingTime DATETIME	
AS
BEGIN
	DECLARE @UserID BIGINT;
	select top 1 @UserID = usr.UserId from Users usr
		where usr.Name = @UserName
	
	update Users
		set LastPingDateTime  = @LastPingTime
	where users.UserId = @UserID

	select skill.Name as SkillSetName from SkillSets skill
		join UserSkillSets rel on rel.SkillSet_SkillSetId  = skill.SkillSetId
		join users usr on usr.UserId = rel.User_UserId
			where usr.UserId = @UserID
			
	select task.TaskId, ttype.MaxUserRuntime, task.UserDateAssigned from TicketTasks task
	join TaskTypes ttype on task.TaskType_TaskTypeId = ttype.TaskTypeId
	join Users usr on  task.CurrentAssignee_UserId = usr.UserId
	where usr.UserId = @UserID  and task.TaskStatus >= @FromStatus and task.TaskStatus < @ToStatus 
END