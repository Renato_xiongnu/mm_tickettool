/****** Object:  StoredProcedure [dbo].[rp_TaskStatistics]    Script Date: 09/06/2013 13:59:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rp_TaskStatistics]
-- TicketTool.rp_TaskStatistics
@StartDate AS DATETIME, 
@EndDate AS DATETIME, 
@OffsetInMinutes AS INTEGER = 240,
@SLATime AS INTEGER
AS
BEGIN
DECLARE @StartWorkTime AS TIME = '09:00:00.000'
DECLARE @EndWorkTime AS TIME = '21:50:00.000'
SELECT @StartDate = CAST(DATEADD(MINUTE, -@OffsetInMinutes, @StartDate) AS DATETIME)
SELECT @EndDate = CAST(DATEADD(MINUTE, -@OffsetInMinutes, @EndDate) AS DATETIME)
SELECT @StartWorkTime = CAST(DATEADD(MINUTE, -@OffsetInMinutes, @StartWorkTime) AS DATETIME)
SELECT @EndWorkTime = CAST(DATEADD(MINUTE, -@OffsetInMinutes, @EndWorkTime) AS DATETIME)


;WITH Ticket_Tasks AS 
        (SELECT 
        Name Task,
        TaskStatus,
        FinishDate FinishDate,
        DATEDIFF(ss, CreateDate, ISNULL(FinishDate, GETDATE())) ProcessingTime,
        DATEDIFF(ss, '00:00:00.000', UserRuntime) UserRuntime,
        DATEDIFF(ss, CreateDate, ISNULL(UserDateAssigned, GETDATE())) WaitTime
        FROM TicketTasks
        WHERE CAST( CreateDate AS TIME) > @StartWorkTime
        AND CAST(CreateDate AS TIME) < @EndWorkTime  
        AND CreateDate >= @StartDate AND CreateDate < @EndDate  --AND TaskStatus = 100 AND FinishDate IS NOT NULL 
        )
SELECT 
    Task,
    (SELECT COUNT(*) FROM Ticket_Tasks t1 WHERE t.Task = t1.Task) Tasks,
    COUNT(*) FinishedTasks,
    --AVG(ProcessingTime) AvgProcessingTime,
    --MIN(ProcessingTime) MinProcessingTime,
    --MAX(ProcessingTime) MaxProcessingTime,
    AVG(UserRuntime) AvgProcessingTime,
    MIN(UserRuntime) MinProcessingTime,
    MAX(UserRuntime) MaxProcessingTime,
    AVG(WaitTime) AvgWaitTime,
    MIN(WaitTime) MinWaitTime,
    MAX(WaitTime) MaxWaitTime,
    CASE WHEN (SELECT COUNT(*) FROM Ticket_Tasks t1 WHERE t.Task = t1.Task) = 0 THEN NULL 
        ELSE (SELECT COUNT(*) FROM Ticket_Tasks t2 WHERE t.Task = t2.Task AND t2.ProcessingTime < @SLATime * 60) * 100 / (SELECT COUNT(*) FROM Ticket_Tasks t1 WHERE t.Task = t1.Task)  END SLA
    FROM Ticket_Tasks t
    WHERE FinishDate IS NOT NULL AND TaskStatus = 100 
    GROUP BY Task
    ORDER BY Task
END
GO