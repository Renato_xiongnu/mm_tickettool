CREATE PROCEDURE NewStore (
@SapCode NVARCHAR(4), 
@Name NVARCHAR(255),
@TimeZone  NVARCHAR(255)
)
AS
BEGIN
BEGIN TRAN
INSERT INTO Stores (SapCode, Name, TimeZone)
    VALUES (@SapCode, @Name, @TimeZone)

-- store manager
INSERT [dbo].[Users] ([Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) 
VALUES ( N'Manager'+@SapCode, N'MM'+RIGHT(@SapCode, 3)+'-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', 
'1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')

INSERT INTO [dbo].[StoreUsers] (Store_SapCode, User_UserId) 
	SELECT @SapCode, u.UserId FROM [dbo].Users u WHERE u.Name = 'Manager'+@SapCode

INSERT INTO [dbo].[RoleUsers] (Role_RoleId, User_UserId)
	SELECT  3, u.UserId FROM [dbo].Users u WHERE u.Name like 'Manager'+@SapCode
	
INSERT INTO [dbo].[UserSkillSets] (SkillSet_SkillSetId, User_UserId)
	SELECT  3, u.UserId FROM [dbo].Users u WHERE u.Name like 'Manager'+@SapCode

INSERT INTO [dbo].[UserLogs] (LoginDate ,LogoutDate,AbandonedQty ,ProcessedQty ,User_UserId,SkillSet_SkillSetId)
	SELECT  GETDATE(), NULL, 0, 0, u.UserId, 3 FROM [dbo].Users u WHERE u.Name like 'Manager'+@SapCode
-- agent delivery
INSERT [dbo].[Users] ([Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) 
VALUES ( N'Delivery'+@SapCode, N't'+RIGHT(@SapCode, 3)+'@t.ru', N'', 
'1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')

INSERT INTO [dbo].[StoreUsers] (Store_SapCode, User_UserId) 
	SELECT @SapCode, u.UserId FROM [dbo].Users u WHERE u.Name = 'Delivery'+@SapCode

INSERT INTO [dbo].[RoleUsers] (Role_RoleId, User_UserId)
	SELECT  1, u.UserId FROM [dbo].Users u WHERE u.Name like 'Delivery'+@SapCode
	
INSERT INTO [dbo].[UserSkillSets] (SkillSet_SkillSetId, User_UserId)
	SELECT  4, u.UserId FROM [dbo].Users u WHERE u.Name like 'Delivery'+@SapCode
COMMIT TRAN
END