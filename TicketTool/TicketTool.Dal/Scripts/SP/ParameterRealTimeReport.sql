﻿CREATE PROCEDURE [dbo].[SP_ParameterRealTimeReport]	
	 @FromDate DATETIME,
	 @ToDate DATETIME
AS
BEGIN	
   SELECT st.ParameterName,
   (
	 select COUNT(*) FROM dbo.GetActiveTasks(@FromDate, @ToDate, st.ParameterName) task
		where task.CurrentAssignee_UserId is null and (task.IsParameterizableSkillSet = 0 or task.DeliveryTaskByEmailSkillSet = 0)
   ) QtyOnCallCenter,   
   (
	 select MIN(task.CreateDate) FROM dbo.GetActiveTasks(@FromDate, @ToDate, st.ParameterName) task
		where task.IsEscalationSkillSet = 0 and (task.IsParameterizableSkillSet = 0 or task.DeliveryTaskByEmailSkillSet = 0)
   ) MinDateOnCallCenter,
   (
    select COUNT(*) FROM dbo.GetActiveTasks(@FromDate, @ToDate, st.ParameterName) task
	 where task.CurrentAssignee_UserId is null and task.IsParameterizableSkillSet = 1 and task.DeliveryTaskByEmailSkillSet = 1
   ) as QtyOnStore,   
   (
    select MIN(task.CreateDate) FROM dbo.GetActiveTasks(@FromDate, @ToDate, st.ParameterName) task
	 where task.IsParameterizableSkillSet = 1 and task.DeliveryTaskByEmailSkillSet = 1
   ) as MinDateOnStore,
   
   (
	select COUNT(*)
		FROM dbo.GetActiveTasks(@FromDate, @ToDate, st.ParameterName) task		
		where task.CurrentAssignee_UserId is not null and (task.IsParameterizableSkillSet = 0 or task.DeliveryTaskByEmailSkillSet = 0)
   ) as InProgressOnCallCenter,
   (
	select COUNT(*)
		FROM dbo.GetActiveTasks(@FromDate, @ToDate, st.ParameterName) task	
		where task.CurrentAssignee_UserId is not null and task.IsParameterizableSkillSet = 1 and task.DeliveryTaskByEmailSkillSet = 1
   ) as InProgressOnStore
  from [dbo].[Parameters] st
END