﻿USE [TicketToolDb$(country)]
print 'Insert data:'
GO

SET IDENTITY_INSERT [dbo].[SkillSets] ON
INSERT [dbo].[SkillSets] ([SkillSetId], [Name], [IsDefault], [IsParameterizable], [DeliveryTaskByEmail], [IsEscalation]) VALUES (1, N'По умолчанию', 1, 0, 0, 0)
INSERT [dbo].[SkillSets] ([SkillSetId], [Name], [IsDefault], [IsParameterizable], [DeliveryTaskByEmail], [IsEscalation]) VALUES (2, N'Эскалации', 0, 0, 0, 1)
INSERT [dbo].[SkillSets] ([SkillSetId], [Name], [IsDefault], [IsParameterizable], [DeliveryTaskByEmail], [IsEscalation]) VALUES (3, N'Call центр', 0, 0, 0, 0)
INSERT [dbo].[SkillSets] ([SkillSetId], [Name], [IsDefault], [IsParameterizable], [DeliveryTaskByEmail], [IsEscalation]) VALUES (4, N'Магазин', 0, 1, 1, 0)
insert [dbo].[SkillSets] ([SkillSetId], [Name], [IsDefault], [IsParameterizable], [DeliveryTaskByEmail], [IsEscalation])values (5,N'Доставка', 0, 1, 1, 0)
insert [dbo].[SkillSets] ([SkillSetId], [Name], [IsDefault], [IsParameterizable], [DeliveryTaskByEmail], [IsEscalation])values (6,N'Кредитный брокер', 0, 1, 1, 0)
SET IDENTITY_INSERT [dbo].[SkillSets] OFF

-----
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R004', N'test', N'MSK', NULL, CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 0, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R006', N'test', N'MSK', NULL, CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 0, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R008', N'test', N'MSK', NULL, CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 0, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R201', N'Москва Ленинградское шоссе', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R202', N'Москва проспект Вернадского', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R203', N'Санкт-Петербург пр. Космонавтов', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R204', N'Санкт-Петербург МЕГА Дыбенко', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R205', N'Ростов-на-Дону пр-т Нагибина', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R206', N'Москва Севастопольский пр-т', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R207', N'Санкт-Петербург пр-т. Маршала Жукова', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R208', N'Москва Каширское шоссе	', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R209', N'Москва МЕГА Белая Дача	', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R210', N'Самара улица Дыбенко', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R211', N'Санкт-Петербург Коломяжский пр', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R215', N'Ростов-на-Дону ул. Малиновского', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R216', N'Москва Рублевское шоссе', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R217', N'Краснодар улица Стасова', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R218', N'Москва проспект Мира', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R219', N'Санкт-Петербург Пулковское шоссе', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R220', N'Самара Московское шоссе', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R221', N'Казань проспект Победы	', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R222', N'Тольятти Автозаводское шоссе', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R223', N'Воронеж бульвар Победы	', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R224', N'Омск МЕГА', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R225', N'Казань ул. Петербургская', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R226', N'Москва Варшавское шоссе', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R227', N'Екатеринбург ул. Малышева', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R228', N'Липецк ул Советская', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R229', N'Москва Бутово', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R231', N'Казань проспект Ямашева', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R232', N'Саратов улица Верхняя', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R233', N'Набережные Челны пр. Яшьлек', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R234', N'Оренбург улица Новая', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R235', N'Новосибирск ул. Военная', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R236', N'Чебоксары Президентский бульвар', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R237', N'Рязань Московское шоссе ', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R238', N'Ульяновск Московское шоссе', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R239', N'Москва Мытищи', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R240', N'Челябинск ул. Цвиллинга', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R241', N'Сургут Нефтеюганское шоссе', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R242', N'Екатеринбург ул. Репина', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R243', N'Белгород проспект Богдана Хмельницкого', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R244', N'Волгоград Акварель', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R245', N'Екатеринбург ул. Сулимова', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R246', N'Москва Алтуфьевское шоссе', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R247', N'Нижний Новгород площадь Комсомольская', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R501', N'Москва Vegas', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R502', N'Воронеж Град', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R503', N'Saturn Челябинск Фиеста', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R504', N'Saturn Челябинск Дарвина', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
INSERT [dbo].[Parameters] ([ParameterName], [Name], [TimeZone], [TimeZoneOffset], [StartWorkingTime], [EndWorkingTime], [UtcStartWorkingTime], [UtcEndWorkingTime], [CanSynchronize], [Description]) VALUES (N'R930', N'003', N'MSK', N'04:00:00', CAST(0x0700000000000000 AS Time), CAST(0x0700000000000000 AS Time), 0, 0, 1, NULL)
-----

SET IDENTITY_INSERT [dbo].[Roles] ON
INSERT [dbo].[Roles] ([RoleId], [Name]) VALUES (1, N'Agent')
INSERT [dbo].[Roles] ([RoleId], [Name]) VALUES (2, N'Superviser')
INSERT [dbo].[Roles] ([RoleId], [Name], [NeedNotification]) VALUES (3, N'StoreManager', 1)
INSERT [dbo].[Roles] ([RoleId], [Name]) VALUES (4, N'Admin')
INSERT [dbo].[Roles] ([RoleId], [Name]) VALUES (5, N'AvitoManager')
INSERT [dbo].[Roles] ([RoleId], [Name], [NeedNotification]) VALUES (6, N'Broker', 1)
SET IDENTITY_INSERT [dbo].[Roles] OFF


SET IDENTITY_INSERT [dbo].[Users] ON
--MMDefManager1
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (1, N'ManagerR201', N'MM201-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (2, N'ManagerR202', N'MM202-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (3, N'ManagerR203', N'MM203-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (4, N'ManagerR204', N'MM204-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (5, N'ManagerR205', N'MM205-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (6, N'ManagerR206', N'MM206-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (7, N'ManagerR207', N'MM207-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (8, N'ManagerR208', N'MM208-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (9, N'ManagerR209', N'MM209-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')

INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (10, N'ManagerR210', N'MM210-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (11, N'ManagerR211', N'MM211-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (12, N'ManagerR215', N'MM215-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (13, N'ManagerR216', N'MM216-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (14, N'ManagerR217', N'MM217-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (15, N'ManagerR218', N'MM218-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (16, N'ManagerR219', N'MM219-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (17, N'ManagerR220', N'MM220-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')

INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (18, N'ManagerR221', N'MM221-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (19, N'ManagerR222', N'MM222-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (20, N'ManagerR223', N'MM223-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (21, N'ManagerR224', N'MM224-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (22, N'ManagerR225', N'MM225-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (23, N'ManagerR226', N'MM226-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (24, N'ManagerR227', N'MM227-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (25, N'ManagerR228', N'MM228-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (26, N'ManagerR229', N'MM229-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (27, N'ManagerR231', N'MM231-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (28, N'ManagerR232', N'MM232-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (29, N'ManagerR233', N'MM233-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (30, N'ManagerR234', N'MM234-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (31, N'ManagerR235', N'MM235-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (32, N'ManagerR236', N'MM236-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (33, N'ManagerR237', N'MM237-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (34, N'ManagerR238', N'MM238-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (35, N'ManagerR239', N'MM239-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (36, N'ManagerR240', N'MM240-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (37, N'ManagerR241', N'MM241-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')

INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (38, N'ManagerR242', N'MM242-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (39, N'ManagerR243', N'MM243-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (40, N'ManagerR244', N'MM244-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (41, N'ManagerR245', N'MM245-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (42, N'ManagerR246', N'MM246-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (43, N'ManagerR247', N'MM247-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (44, N'ManagerR501', N'MM501-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (45, N'ManagerR502', N'MM502-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (46, N'ManagerR503', N'MM503-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (47, N'ManagerR504', N'MM504-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')

--Brokers
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (48, N'BrokerR201', N'MM201-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (49, N'BrokerR202', N'MM202-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (50, N'BrokerR203', N'MM203-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (51, N'BrokerR204', N'MM204-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (52, N'BrokerR205', N'MM205-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (53, N'BrokerR206', N'MM206-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (54, N'BrokerR207', N'MM207-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (55, N'BrokerR208', N'MM208-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (56, N'BrokerR209', N'MM209-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')

INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (57, N'BrokerR210', N'MM210-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (58, N'BrokerR211', N'MM211-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (59, N'BrokerR215', N'MM215-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (60, N'BrokerR216', N'MM216-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (61, N'BrokerR217', N'MM217-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (62, N'BrokerR218', N'MM218-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (63, N'BrokerR219', N'MM219-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (64, N'BrokerR220', N'MM220-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')

INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (65, N'BrokerR221', N'MM221-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (66, N'BrokerR222', N'MM222-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (67, N'BrokerR223', N'MM223-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (68, N'BrokerR224', N'MM224-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (69, N'BrokerR225', N'MM225-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (70, N'BrokerR226', N'MM226-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (71, N'BrokerR227', N'MM227-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (72, N'BrokerR228', N'MM228-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (73, N'BrokerR229', N'MM229-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (74, N'BrokerR231', N'MM231-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (75, N'BrokerR232', N'MM232-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (76, N'BrokerR233', N'MM233-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (77, N'BrokerR234', N'MM234-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (78, N'BrokerR235', N'MM235-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (79, N'BrokerR236', N'MM236-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (80, N'BrokerR237', N'MM237-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (81, N'BrokerR238', N'MM238-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (82, N'BrokerR239', N'MM239-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (83, N'BrokerR240', N'MM240-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (84, N'BrokerR241', N'MM241-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')

INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (85, N'BrokerR242', N'MM242-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (86, N'BrokerR243', N'MM243-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (87, N'BrokerR244', N'MM244-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (88, N'BrokerR245', N'MM245-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (89, N'BrokerR246', N'MM246-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (90, N'BrokerR247', N'MM247-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (91, N'BrokerR501', N'MM501-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (92, N'BrokerR502', N'MM502-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (93, N'BrokerR503', N'MM503-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
INSERT [dbo].[Users] ([UserId], [Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (94, N'BrokerR504', N'MM504-Order@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01 00:00:00.000', 0, GETDATE(), '1900-01-01 00:00:00.000')
SET IDENTITY_INSERT [dbo].[Users] OFF

-- pas: 1
INSERT [dbo].[Users] ([Name], [Email], [Password], [LastPingDateTime], [IsLocked], [CreateDate], [LastLoginDate]) VALUES (N'admin', N'q1@q.q', N'NMAIcGbfX9RMZ4r0vtaab444LDs=', CAST(0x0000A10F00BBBCB8 AS DateTime), 0, CAST(0x0000A10E015D64DA AS DateTime), CAST(0x0000A10F00BBA48E AS DateTime))
INSERT [dbo].[RoleUsers] ([User_UserId], [Role_RoleId]) VALUES 
	((select top 1 UserId from dbo.Users where Name=N'admin'),
	 (select top 1 RoleId from dbo.Roles where Name=N'Admin'))


INSERT [dbo].[RoleSkillSets] ([Role_RoleId], [SkillSet_SkillSetId])		 
	 select (select top 1 RoleId from dbo.Roles where Name=N'Admin') as [Role_RoleId],
	 SkillSetId	from [SkillSets] where SkillSetId != 1 

INSERT [dbo].[RoleSkillSets] ([Role_RoleId], [SkillSet_SkillSetId])		 
	 select (select top 1 RoleId from dbo.Roles where Name=N'Agent') as [Role_RoleId],
	 SkillSetId	from [SkillSets] where SkillSetId != 1

INSERT [dbo].[RoleSkillSets] ([Role_RoleId], [SkillSet_SkillSetId])		 
	 select (select top 1 RoleId from dbo.Roles where Name=N'Superviser') as [Role_RoleId],
	 SkillSetId	from [SkillSets] where SkillSetId != 1

INSERT [dbo].[RoleSkillSets] ([Role_RoleId], [SkillSet_SkillSetId])	VALUES(
	 (select top 1 RoleId from dbo.Roles where Name=N'StoreManager'),
	 4)

INSERT [dbo].[RoleSkillSets] ([Role_RoleId], [SkillSet_SkillSetId])	VALUES(
	 (select top 1 RoleId from dbo.Roles where Name=N'Broker'),
	 6)	 


INSERT INTO [dbo].[UserAvailableSkillSets] ([UserId], [SkillSetId])
	select (select top 1 UserId from dbo.Users where Name=N'admin') as UserId,
	 SkillSetId	from [SkillSets] where SkillSetId != 1



insert into [dbo].[ParameterUsers] (Parameter_ParameterName, User_UserId)
	select s.ParameterName, u.UserId from [dbo].Users u join [dbo].Parameters s on u.Name = N'Manager'+s.ParameterName

insert into [dbo].[ParameterUsers] (Parameter_ParameterName, User_UserId) 
	select s.ParameterName, u.UserId from [dbo].Users u join [dbo].Parameters s on u.Name = N'Broker'+s.ParameterName

insert into [dbo].[RoleUsers] (Role_RoleId, User_UserId)
	select  3, u.UserId from [dbo].Users u where u.Name like N'ManagerR%'

insert into [dbo].[RoleUsers] (Role_RoleId, User_UserId)
	select  6, u.UserId from [dbo].Users u where u.Name like N'BrokerR%'

insert into [dbo].[UserSkillSets] (SkillSet_SkillSetId, User_UserId)
	select  4, u.UserId from [dbo].Users u where u.Name like N'ManagerR%'

insert into [dbo].[UserSkillSets] (SkillSet_SkillSetId, User_UserId)
	select  5, u.UserId from [dbo].Users u where u.Name like N'ManagerR%'

insert into [dbo].[UserSkillSets] (SkillSet_SkillSetId, User_UserId)
	select  6, u.UserId from [dbo].Users u where u.Name like N'BrokerR%'

insert into [dbo].[UserLogs] (LoginDate ,LogoutDate,AbandonedQty ,ProcessedQty ,User_UserId,SkillSet_SkillSetId)
	select  GETDATE(), NULL, 0, 0, u.UserId, 4 from [dbo].Users u where u.Name like N'ManagerR%'

insert into [dbo].[UserLogs] (LoginDate ,LogoutDate,AbandonedQty ,ProcessedQty ,User_UserId,SkillSet_SkillSetId)
	select  GETDATE(), NULL, 0, 0, u.UserId, 5 from [dbo].Users u where u.Name like N'ManagerR%'

insert into [dbo].[UserLogs] (LoginDate ,LogoutDate,AbandonedQty ,ProcessedQty ,User_UserId,SkillSet_SkillSetId)
	select  GETDATE(), NULL, 0, 0, u.UserId, 6 from [dbo].Users u where u.Name like N'BrokerR%'


SET IDENTITY_INSERT [dbo].[NotificationLogGroups] ON
INSERT [dbo].[NotificationLogGroups] ([Id], [Name], [IsDefault], [DefaultRecipientEmail]) VALUES (1, N'Общие сообщения', 1, N'vasilevig@media-saturn.com')
SET IDENTITY_INSERT [dbo].[NotificationLogGroups] OFF