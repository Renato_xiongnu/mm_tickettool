﻿USE [TicketToolDb$(country)]
print 'Insert test data:'
GO

begin tran
-- add Manager for R006
insert into dbo.Parameters (ParameterName, Name, TimeZone) values (N'R006', N'test', 'MSK')

insert into dbo.Users
(Name, Email, Password, LastPingDateTime, IsLocked, CreateDate, LastLoginDate)
values (N'ManagerR006', N'korolyov@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01', 0, '1900-01-01', '1900-01-01')

insert into dbo.Users
(Name, Email, Password, LastPingDateTime, IsLocked, CreateDate, LastLoginDate)
values (N'BrokerR006', N'korolyov@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01', 0, '1900-01-01', '1900-01-01')


insert into dbo.ParameterUsers (Parameter_ParameterName, User_UserId)
select N'R006', UserId from dbo.Users where Name = N'ManagerR006'

insert into dbo.ParameterUsers (Parameter_ParameterName, User_UserId)
select N'R006', UserId from dbo.Users where Name = N'BrokerR006'

insert into dbo.RoleUsers (Role_RoleId, User_UserId)
select 3, UserId from dbo.Users where Name = N'ManagerR006'

insert into dbo.RoleUsers (Role_RoleId, User_UserId)
select 6, UserId from dbo.Users where Name = N'BrokerR006'

insert into dbo.UserSkillSets (User_UserId, SkillSet_SkillSetId)
select UserId, 4 from dbo.Users where Name = N'ManagerR006'

insert into dbo.UserSkillSets (User_UserId, SkillSet_SkillSetId)
select UserId, 5 from dbo.Users where Name = N'ManagerR006'

INSERT INTO [dbo].[UserAvailableSkillSets] ([UserId], [SkillSetId])
select UserId, 4 from dbo.Users where Name = N'ManagerR006'

INSERT INTO [dbo].[UserAvailableSkillSets] ([UserId], [SkillSetId])
select UserId, 5 from dbo.Users where Name = N'ManagerR006'

insert into dbo.UserSkillSets (User_UserId, SkillSet_SkillSetId)
select UserId, 6 from dbo.Users where Name = N'BrokerR006'

INSERT INTO [dbo].[UserAvailableSkillSets] ([UserId], [SkillSetId])
select UserId, 6 from dbo.Users where Name = N'BrokerR006'


insert into [dbo].[UserLogs] (LoginDate ,LogoutDate,AbandonedQty ,ProcessedQty ,User_UserId,SkillSet_SkillSetId)
	select  GETDATE(), NULL, 0, 0, u.UserId, 4 from [dbo].Users u where u.Name like N'ManagerR006'

insert into [dbo].[UserLogs] (LoginDate ,LogoutDate,AbandonedQty ,ProcessedQty ,User_UserId,SkillSet_SkillSetId)
	select  GETDATE(), NULL, 0, 0, u.UserId, 5 from [dbo].Users u where u.Name like N'ManagerR006'

insert into [dbo].[UserLogs] (LoginDate ,LogoutDate,AbandonedQty ,ProcessedQty ,User_UserId,SkillSet_SkillSetId)
	select  GETDATE(), NULL, 0, 0, u.UserId, 6 from [dbo].Users u where u.Name like N'BrokerR006'

-- add Manager for R008
insert into dbo.Parameters (ParameterName, Name, TimeZone) values (N'R008', N'test', 'MSK')

insert into dbo.Users
(Name, Email, Password, LastPingDateTime, IsLocked, CreateDate, LastLoginDate)
values (N'ManagerR008', N'korolyov@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01', 0, '1900-01-01', '1900-01-01')

insert into dbo.Users
(Name, Email, Password, LastPingDateTime, IsLocked, CreateDate, LastLoginDate)
values (N'BrokerR008', N'korolyov@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01', 0, '1900-01-01', '1900-01-01')

insert into dbo.ParameterUsers (Parameter_ParameterName, User_UserId)
select N'R008', UserId from dbo.Users where Name = N'ManagerR008'

insert into dbo.RoleUsers (Role_RoleId, User_UserId)
select 3, UserId from dbo.Users where Name = N'ManagerR008'

insert into dbo.UserSkillSets (User_UserId, SkillSet_SkillSetId)
select UserId, 4 from dbo.Users where Name = N'ManagerR008'

insert into dbo.UserSkillSets (User_UserId, SkillSet_SkillSetId)
select UserId, 5 from dbo.Users where Name = N'ManagerR008'

insert into dbo.UserSkillSets (User_UserId, SkillSet_SkillSetId)
select UserId, 6 from dbo.Users where Name = N'BrokerR008'

INSERT INTO [dbo].[UserAvailableSkillSets] ([UserId], [SkillSetId])
select UserId, 4 from dbo.Users where Name = N'ManagerR008'

INSERT INTO [dbo].[UserAvailableSkillSets] ([UserId], [SkillSetId])
select UserId, 5 from dbo.Users where Name = N'ManagerR008'

INSERT INTO [dbo].[UserAvailableSkillSets] ([UserId], [SkillSetId])
select UserId, 6 from dbo.Users where Name = N'BrokerR008'

insert into [dbo].[UserLogs] (LoginDate ,LogoutDate,AbandonedQty ,ProcessedQty ,User_UserId,SkillSet_SkillSetId)
	select  GETDATE(), NULL, 0, 0, u.UserId, 4 from [dbo].Users u where u.Name like N'ManagerR008'

insert into [dbo].[UserLogs] (LoginDate ,LogoutDate,AbandonedQty ,ProcessedQty ,User_UserId,SkillSet_SkillSetId)
	select  GETDATE(), NULL, 0, 0, u.UserId, 5 from [dbo].Users u where u.Name like N'ManagerR008'

insert into [dbo].[UserLogs] (LoginDate ,LogoutDate,AbandonedQty ,ProcessedQty ,User_UserId,SkillSet_SkillSetId)
	select  GETDATE(), NULL, 0, 0, u.UserId, 6 from [dbo].Users u where u.Name like N'BrokerR008'

--add agent Mulder
insert into dbo.Users
(Name, Email, Password, LastPingDateTime, IsLocked, CreateDate, LastLoginDate)
values (N'Mulder', N'korolyov@media-saturn.com', N'NMAIcGbfX9RMZ4r0vtaab444LDs=', '1900-01-01', 0, '1900-01-01', '1900-01-01')

insert into dbo.RoleUsers (Role_RoleId, User_UserId)
select 1, UserId from dbo.Users where Name = N'Mulder'

--add agent Smith for shop R008
insert into dbo.Users
(Name, Email, Password, LastPingDateTime, IsLocked, CreateDate, LastLoginDate)
values (N'Smith', N'korolyov@media-saturn.com', N'NMAIcGbfX9RMZ4r0vtaab444LDs=', '1900-01-01', 0, '1900-01-01', '1900-01-01')

insert into dbo.RoleUsers (Role_RoleId, User_UserId)
select 1, UserId from dbo.Users where Name = N'Smith'

insert into dbo.ParameterUsers (Parameter_ParameterName, User_UserId)
select N'R008', UserId from dbo.Users where Name = N'Smith'

--add agent Bond for shop R006
insert into dbo.Users
(Name, Email, Password, LastPingDateTime, IsLocked, CreateDate, LastLoginDate)
values (N'Bond', N'korolyov@media-saturn.com', N'NMAIcGbfX9RMZ4r0vtaab444LDs=', '1900-01-01', 0, '1900-01-01', '1900-01-01')

insert into dbo.RoleUsers (Role_RoleId, User_UserId)
select 1, UserId from dbo.Users where Name = N'Bond'

insert into dbo.ParameterUsers (Parameter_ParameterName, User_UserId)
select N'R006', UserId from dbo.Users where Name = N'Bond'



-- add Manager for R004
insert into dbo.Parameters (ParameterName, Name, TimeZone) values (N'R004', N'test', 'MSK')

insert into dbo.Users
(Name, Email, Password, LastPingDateTime, IsLocked, CreateDate, LastLoginDate)
values (N'ManagerR004', N'korolyov@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01', 0, '1900-01-01', '1900-01-01')

insert into dbo.Users
(Name, Email, Password, LastPingDateTime, IsLocked, CreateDate, LastLoginDate)
values (N'BrokerR004', N'korolyov@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01', 0, '1900-01-01', '1900-01-01')


insert into dbo.ParameterUsers (Parameter_ParameterName, User_UserId)
select N'R004', UserId from dbo.Users where Name = N'ManagerR004'

insert into dbo.ParameterUsers (Parameter_ParameterName, User_UserId)
select N'R004', UserId from dbo.Users where Name = N'BrokerR004'

insert into dbo.RoleUsers (Role_RoleId, User_UserId)
select 3, UserId from dbo.Users where Name = N'ManagerR004'

insert into dbo.RoleUsers (Role_RoleId, User_UserId)
select 6, UserId from dbo.Users where Name = N'BrokerR004'

insert into dbo.UserSkillSets (User_UserId, SkillSet_SkillSetId)
select UserId, 4 from dbo.Users where Name = N'ManagerR004'

insert into dbo.UserSkillSets (User_UserId, SkillSet_SkillSetId)
select UserId, 5 from dbo.Users where Name = N'ManagerR004'

INSERT INTO [dbo].[UserAvailableSkillSets] ([UserId], [SkillSetId])
select UserId, 4 from dbo.Users where Name = N'ManagerR004'

INSERT INTO [dbo].[UserAvailableSkillSets] ([UserId], [SkillSetId])
select UserId, 5 from dbo.Users where Name = N'ManagerR004'

insert into dbo.UserSkillSets (User_UserId, SkillSet_SkillSetId)
select UserId, 6 from dbo.Users where Name = N'BrokerR004'

INSERT INTO [dbo].[UserAvailableSkillSets] ([UserId], [SkillSetId])
select UserId, 6 from dbo.Users where Name = N'BrokerR004'


insert into [dbo].[UserLogs] (LoginDate ,LogoutDate,AbandonedQty ,ProcessedQty ,User_UserId,SkillSet_SkillSetId)
	select  GETDATE(), NULL, 0, 0, u.UserId, 4 from [dbo].Users u where u.Name like N'ManagerR004'

insert into [dbo].[UserLogs] (LoginDate ,LogoutDate,AbandonedQty ,ProcessedQty ,User_UserId,SkillSet_SkillSetId)
	select  GETDATE(), NULL, 0, 0, u.UserId, 5 from [dbo].Users u where u.Name like N'ManagerR004'

insert into [dbo].[UserLogs] (LoginDate ,LogoutDate,AbandonedQty ,ProcessedQty ,User_UserId,SkillSet_SkillSetId)
	select  GETDATE(), NULL, 0, 0, u.UserId, 6 from [dbo].Users u where u.Name like N'BrokerR004'

-- add Manager for R008
insert into dbo.Parameters (ParameterName, Name, TimeZone) values (N'R008', N'test', 'MSK')

insert into dbo.Users
(Name, Email, Password, LastPingDateTime, IsLocked, CreateDate, LastLoginDate)
values (N'ManagerR008', N'korolyov@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01', 0, '1900-01-01', '1900-01-01')

insert into dbo.Users
(Name, Email, Password, LastPingDateTime, IsLocked, CreateDate, LastLoginDate)
values (N'BrokerR008', N'korolyov@media-saturn.com', N'UoIL9jrz/MmaYwpgxwdlRxcmEvo=', '1900-01-01', 0, '1900-01-01', '1900-01-01')

insert into dbo.ParameterUsers (Parameter_ParameterName, User_UserId)
select N'R008', UserId from dbo.Users where Name = N'ManagerR008'

insert into dbo.RoleUsers (Role_RoleId, User_UserId)
select 3, UserId from dbo.Users where Name = N'ManagerR008'

insert into dbo.UserSkillSets (User_UserId, SkillSet_SkillSetId)
select UserId, 4 from dbo.Users where Name = N'ManagerR008'

insert into dbo.UserSkillSets (User_UserId, SkillSet_SkillSetId)
select UserId, 5 from dbo.Users where Name = N'ManagerR008'

insert into dbo.UserSkillSets (User_UserId, SkillSet_SkillSetId)
select UserId, 6 from dbo.Users where Name = N'BrokerR008'

INSERT INTO [dbo].[UserAvailableSkillSets] ([UserId], [SkillSetId])
select UserId, 4 from dbo.Users where Name = N'ManagerR008'

INSERT INTO [dbo].[UserAvailableSkillSets] ([UserId], [SkillSetId])
select UserId, 5 from dbo.Users where Name = N'ManagerR008'

INSERT INTO [dbo].[UserAvailableSkillSets] ([UserId], [SkillSetId])
select UserId, 6 from dbo.Users where Name = N'BrokerR008'

insert into [dbo].[UserLogs] (LoginDate ,LogoutDate,AbandonedQty ,ProcessedQty ,User_UserId,SkillSet_SkillSetId)
	select  GETDATE(), NULL, 0, 0, u.UserId, 4 from [dbo].Users u where u.Name like N'ManagerR008'

insert into [dbo].[UserLogs] (LoginDate ,LogoutDate,AbandonedQty ,ProcessedQty ,User_UserId,SkillSet_SkillSetId)
	select  GETDATE(), NULL, 0, 0, u.UserId, 5 from [dbo].Users u where u.Name like N'ManagerR008'

insert into [dbo].[UserLogs] (LoginDate ,LogoutDate,AbandonedQty ,ProcessedQty ,User_UserId,SkillSet_SkillSetId)
	select  GETDATE(), NULL, 0, 0, u.UserId, 6 from [dbo].Users u where u.Name like N'BrokerR008'



--add agent Smith for shop R008
insert into dbo.ParameterUsers (Parameter_ParameterName, User_UserId)
select N'R008', UserId from dbo.Users where Name = N'Smith'

--add agent Bond for shop R004
insert into dbo.ParameterUsers (Parameter_ParameterName, User_UserId)
select N'R004', UserId from dbo.Users where Name = N'Bond'




--add another skillset Escalation
insert into [dbo].[SkillSets] (Name, IsDefault, IsParameterizable, DeliveryTaskByEmail, IsEscalation )
values ('Escalation 2', 0, 0, 0, 1)

insert into [dbo].[SkillSets] (Name, IsDefault, IsParameterizable, DeliveryTaskByEmail, IsEscalation )
values (N'Доставка_test', 0, 0, 0, 0)

--add another IsParameterizable skillset
insert into [dbo].[SkillSets] (Name, IsDefault, IsParameterizable, DeliveryTaskByEmail, IsEscalation )
values (N'тестовый 2 с параметром', 0, 1, 0, 0)


INSERT INTO [dbo].[UserAvailableSkillSets] ([UserId], [SkillSetId])
select (select UserId from dbo.Users where Name = N'Mulder'), SkillSetId
from [SkillSets] where SkillSetId != 1

INSERT INTO [dbo].[UserAvailableSkillSets] ([UserId], [SkillSetId])
select (select UserId from dbo.Users where Name = N'Smith'), SkillSetId
from [SkillSets] where SkillSetId != 1

INSERT INTO [dbo].[UserAvailableSkillSets] ([UserId], [SkillSetId])
select (select UserId from dbo.Users where Name = N'Bond'), SkillSetId
from [SkillSets] where SkillSetId != 1

-- AVITO Role
insert into dbo.RoleUsers (Role_RoleId, User_UserId)
select 5, UserId from dbo.Users where Name = N'BrokerR004'


commit tran