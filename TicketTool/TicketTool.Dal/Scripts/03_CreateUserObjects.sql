﻿USE [TicketToolDb$(country)]
print 'Create user objects:'
/****** Object:  UserDefinedFunction [dbo].[GetOperationGroup]    Script Date: 06/12/2013 11:04:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetOperationGroup]
(	
	@StartStatus INT,
    @EndStatus INT
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT FirstOperation.TicketTask_TaskId as TaskId, FirstOperation.OperationId as FirsOpId,
			FirstOperation.ToStatus as StarStatus, LastOperation.OperationId as LastOpId, 
			LastOperation.ToStatus as EndStatus, FirstOperation.UpdateDate, LastOperation.UpdateDate - FirstOperation.UpdateDate AS DiffDate
			
	FROM ( SELECT TicketTask_TaskId, OperationId, ToStatus, UpdateDate FROM [dbo].[TaskOperations]
			where ToStatus = @StartStatus) FirstOperation
	LEFT JOIN( SELECT TicketTask_TaskId, OperationId, ToStatus, UpdateDate, RefOperation_OperationId as RefOperationId FROM [dbo].[TaskOperations] 
		    where ToStatus = @EndStatus) LastOperation
  on FirstOperation.TicketTask_TaskId = LastOperation.TicketTask_TaskId AND FirstOperation.OperationId = LastOperation.RefOperationId
)
GO
/****** Object:  UserDefinedFunction [dbo].[GetActiveTasks]    Script Date: 06/12/2013 11:04:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetActiveTasks]
(		
	@FromDate DATETIME,
	@ToDate DATETIME,  
	@Parameter NVARCHAR(255)
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT task.*, 
		   skillSet.IsDefault as IsDefaultSkillSet, 
		   skillSet.IsParameterizable as IsParameterizableSkillSet, 
		   skillSet.DeliveryTaskByEmail as DeliveryTaskByEmailSkillSet, 
		   skillSet.IsEscalation as IsEscalationSkillSet
		   FROM [dbo].[Tickets] ticket
		join [dbo].[TicketTasks] task on ticket.TicketId = task.Ticket_TicketId
		join [dbo].[SkillSets] skillSet on skillSet.SkillSetId =  task.CurrentSkillSet_SkillSetId	
			where ticket.Parameter = @Parameter and task.CreateDate >= @FromDate and task.CreateDate < @ToDate and task.TaskStatus < 100 						
)
GO
/****** Object:  StoredProcedure [dbo].[SP_StoreRealTimeReport]    Script Date: 06/12/2013 11:04:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_ParameterRealTimeReport]	
	 @FromDate DATETIME,
	 @ToDate DATETIME
AS
BEGIN	
   SELECT st.ParameterName,
   (
	 select COUNT(*) FROM dbo.GetActiveTasks(@FromDate, @ToDate, st.ParameterName) task
		where task.CurrentAssignee_UserId is null and (task.IsParameterizableSkillSet = 0 or task.DeliveryTaskByEmailSkillSet = 0)
   ) QtyOnCallCenter,   
   (
	 select MIN(task.CreateDate) FROM dbo.GetActiveTasks(@FromDate, @ToDate, st.ParameterName) task
		where task.IsEscalationSkillSet = 0 and (task.IsParameterizableSkillSet = 0 or task.DeliveryTaskByEmailSkillSet = 0)
   ) MinDateOnCallCenter,
   (
    select COUNT(*) FROM dbo.GetActiveTasks(@FromDate, @ToDate, st.ParameterName) task
	 where task.CurrentAssignee_UserId is null and task.IsParameterizableSkillSet = 1 and task.DeliveryTaskByEmailSkillSet = 1
   ) as QtyOnStore,   
   (
    select MIN(task.CreateDate) FROM dbo.GetActiveTasks(@FromDate, @ToDate, st.ParameterName) task
	 where task.IsParameterizableSkillSet = 1 and task.DeliveryTaskByEmailSkillSet = 1
   ) as MinDateOnStore,
   
   (
	select COUNT(*)
		FROM dbo.GetActiveTasks(@FromDate, @ToDate, st.ParameterName) task		
		where task.CurrentAssignee_UserId is not null and (task.IsParameterizableSkillSet = 0 or task.DeliveryTaskByEmailSkillSet = 0)
   ) as InProgressOnCallCenter,
   (
	select COUNT(*)
		FROM dbo.GetActiveTasks(@FromDate, @ToDate, st.ParameterName) task	
		where task.CurrentAssignee_UserId is not null and task.IsParameterizableSkillSet = 1 and task.DeliveryTaskByEmailSkillSet = 1
   ) as InProgressOnStore
  from [dbo].[Parameters] st
END
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAvalibleTasks]    Script Date: 06/12/2013 11:04:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[SP_GetAvalibleTasks]
	@SkillSetId BIGINT,
	@StartStatus INT,
    @EndStatus INT,
	@MaxStatus INT	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT task.TaskId, task.TaskStatus, 
			  task.CurrentSkillSet_SkillSetId as CurrentSkillSetId,
			  task.CurrentAssignee_UserId as CurrentAssigneeId, 
			  task.InformedUser_UserId as InformedUserId,
			 ticket.Parameter from [dbo].[TicketTasks] task 			
			INNER JOIN [dbo].[Tickets] ticket on ticket.TicketId = task.Ticket_TicketId	
			INNER JOIN [dbo].[SkillSets] skillSet on skillSet.SkillSetId =  task.CurrentSkillSet_SkillSetId						
			INNER JOIN (Select * from [dbo].[GetOperationGroup](@StartStatus, @EndStatus)) 
			opGroup on task.TaskId = opGroup.TaskId
				where task.TaskStatus < @MaxStatus AND SkillSetId = @SkillSetId AND	task.CurrentAssignee_UserId is NULL AND DiffDate is NULL	
END
GO
/****** Object:  StoredProcedure [dbo].[SP_GetPingUserInfo]    Script Date: 06/12/2013 11:04:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GetPingUserInfo]
	@UserName NVARCHAR(255),
	@FromStatus int,
	@ToStatus int,
	@LastPingTime DATETIME	
AS
BEGIN
	DECLARE @UserID BIGINT;
	select top 1 @UserID = usr.UserId from Users usr with (nolock)
		where usr.Name = @UserName
	
	update Users
		set LastPingDateTime  = @LastPingTime
	where users.UserId = @UserID

	select skill.Name as SkillSetName from SkillSets skill with (nolock)
		join UserSkillSets rel with (nolock) on rel.SkillSet_SkillSetId  = skill.SkillSetId
		join users usr with (nolock) on usr.UserId = rel.User_UserId
			where usr.UserId = @UserID
			
	select task.TaskId, ttype.MaxUserRuntime, task.UserDateAssigned from TicketTasks task
	join TaskTypes ttype with (nolock) on task.TaskType_TaskTypeId = ttype.TaskTypeId
	join Users usr with (nolock) on  task.CurrentAssignee_UserId = usr.UserId
	where usr.UserId = @UserID and task.TaskStatus >= @FromStatus and task.TaskStatus < @ToStatus 
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetWokingHoursOfStore]    Script Date: 06/12/2013 11:04:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---!V1_2-->
CREATE FUNCTION [dbo].[GetWokingHoursOfStore]
(		
	@CurrentUtcTime INT
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT ticket.TicketId, parameter.ParameterName,														
			(CASE
				WHEN parameter.UtcStartWorkingTime <= @CurrentUtcTime AND @CurrentUtcTime < parameter.UtcEndWorkingTime
					THEN 1 
				WHEN parameter.UtcStartWorkingTime <= @CurrentUtcTime +24*60 AND @CurrentUtcTime +24*60 < parameter.UtcEndWorkingTime
					THEN 1 
				ELSE 0
			END )AS IsWorkingHours
			from [dbo].Tickets ticket									
			INNER JOIN [dbo].Parameters parameter on parameter.ParameterName = ticket.Parameter
)
--<--
GO
/****** Object:  StoredProcedure [dbo].[SP_GetNextTask]    Script Date: 06/12/2013 11:04:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---!V1_2-->
CREATE PROCEDURE [dbo].[SP_GetNextTask]	
	 @UserId BIGINT,
	 @SkillSetId BIGINT,
	 @StartStatus INT,
	 @EndStatus INT,	 
	 @MaxStatus INT,
	 @SecondStatus INT,
	 @CurrentUtcTime INT
AS
BEGIN
	SET NOCOUNT ON;
	
	
	DECLARE @TaskId BIGINT;
	DECLARE @IsParametaraseble BIT;
	SELECT TOP 1 @IsParametaraseble = s.IsParameterizable from dbo.SkillSets s
		where s.SkillSetId = @SkillSetId
			
		
	IF @IsParametaraseble = 0
	BEGIN
		SELECT TOP 1 @TaskId = TaskId FROM
			(
		    SELECT task.TaskId, task.CreateDate, relInfo.IsWorkingHours from [dbo].[TicketTasks] task WITH (READPAST, updlock)						
			INNER JOIN [dbo].[UserSkillSets] userSkillSet on userSkillSet.SkillSet_SkillSetId = task.CurrentSkillSet_SkillSetId 
			INNER JOIN [dbo].[GetOperationGroup](@StartStatus, @EndStatus) opGroup on task.TaskId = opGroup.TaskId
			INNER JOIN [dbo].GetWokingHoursOfStore(@CurrentUtcTime) relInfo on relInfo.TicketId = task.Ticket_TicketId
			where task.TaskStatus < @MaxStatus AND userSkillSet.SkillSet_SkillSetId = @SkillSetId AND userSkillSet.User_UserId = @UserId AND task.CurrentAssignee_UserId is NULL AND DiffDate is NULL
			AND (task.TaskStatus <> @SecondStatus OR ReturnToQueueAfterDate IS NULL) 
													 -- OR (task.UpdateDate < @FromDate) )
			) as taskQuery
			order by taskQuery.IsWorkingHours desc, taskQuery.CreateDate asc
	END		
	ELSE
	BEGIN
		SELECT top 1 @TaskId = TaskId FROM (
		SELECT task.TaskId, task.CreateDate, relInfo.IsWorkingHours  from [dbo].[TicketTasks] task WITH (READPAST, updlock)
			INNER JOIN [dbo].[UserSkillSets] userSkillSet on userSkillSet.SkillSet_SkillSetId = task.CurrentSkillSet_SkillSetId 			
			INNER JOIN [dbo].[GetOperationGroup](@StartStatus, @EndStatus) opGroup on task.TaskId = opGroup.TaskId
			INNER JOIN [dbo].GetWokingHoursOfStore(@CurrentUtcTime) relInfo on relInfo.TicketId = task.Ticket_TicketId
			where task.TaskStatus < @MaxStatus AND userSkillSet.SkillSet_SkillSetId = @SkillSetId AND userSkillSet.User_UserId = @UserId AND task.CurrentAssignee_UserId is NULL AND DiffDate is NULL
			AND (task.TaskStatus <> @SecondStatus OR ReturnToQueueAfterDate IS NULL) -- OR (task.UpdateDate < @FromDate) )			
			AND EXISTS(SELECT 1 from dbo.ParameterUsers susr where susr.User_UserId = @UserId AND susr.Parameter_ParameterName = relInfo.ParameterName)
			) as taskQuery 
			order by taskQuery.IsWorkingHours desc, taskQuery.CreateDate asc
	END
		SELECT @TaskId
		
					
END
--<--
GO
