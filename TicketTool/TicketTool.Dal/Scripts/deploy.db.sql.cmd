@rem working dir
@cd %1
@set server=%2
@set country=%3

@echo  create local db
@sqlcmd -S %server% -E -v country="%country%" -i 01_CreateDb.sql, 02_CreateCodeFirstObjects.sql, 03_CreateUserObjects.sql, 04_CreateSystemObjects.sql, 05_InsertData.sql

@echo  test data
@sqlcmd -S %server% -E -v country="%country%" -i InsertTestData.sql

@pause