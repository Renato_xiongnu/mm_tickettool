﻿using System;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Collections.Generic;
using TicketTool.Dal.Model;

namespace TicketTool.Dal
{
    public partial class DataProvider
    {
        #region users

        public IQueryable<User> GetAllUsers(bool withSkillSets = false, bool withRoles = false,
            bool withParameters = false, bool withAvailableSkillSets = false, bool withClusters = false)
        {
            DbQuery<User> query = ModelContext.Users;

            if (withSkillSets)
                query = query.Include("SkillSets");

            if (withRoles)
                query = query.Include("Roles");

            if (withParameters)
                query = query.Include("Parameters");

            if (withAvailableSkillSets)
                query = query.Include("AvailableSkillSets");

            if (withClusters)
                query = query.Include("Clusters");

            return query;
        }

        public bool CreateUser(User user)
        {
            var foundUser = GetAllUsers(true).FirstOrDefault(t => t.Name == user.Name);
            if (foundUser == null)
            {
                user.CreateDate = DateTime.Now.ToUniversalTime();
                user.LastPingDateTime = user.LastLoginDate = MinDbDateTime;
                ModelContext.Users.Add(user);
                return true;
            }
            return false;
        }

        public void DeleteUser(string userName)
        {
            var user = GetUser(userName, withRoles:true);
            user.Roles.Clear();
            ModelContext.Users.Remove(user);
        }

        public User GetUser(string userName, bool withSkillSets = false, bool withRoles = false, bool withStores = false, bool throwIfNotExists = true, bool withAvailableSkillSets = false, bool withClusters=false)
        {
            var user = GetAllUsers(withSkillSets, withRoles, withStores, withAvailableSkillSets, withClusters)
                       .FirstOrDefault(t => string.Compare(t.Name, userName, StringComparison.InvariantCultureIgnoreCase) == 0);
            if (user == null && throwIfNotExists)
            {
                throw new ObjectNotFoundException(string.Format("User with name {0} wasn't found", userName));
            }
            return user;
        }

        //public IList<UserLog> GetGroupedUserLog(DateTime? fromDate = null)
        //{
        //    var date = fromDate ?? MinDbDateTime;
        //    var query = (from ul in ModelContext.UserLog
        //                 where ul.LoginDate >= date
        //                 group ul by new {ul.User, ul.SkillSet}
        //                 into groups
        //                 let top = (from gr in groups
        //                            orderby gr.LoginDate descending
        //                            select gr).FirstOrDefault()
        //                 select new {top, top.User, top.SkillSet}).ToList();
        //    return query.Select(t => t.top).ToList();
        //}

        #endregion users

        #region roles
        public IEnumerable<Role> GetAllRoles(bool withUsers = false, bool withSkillSets = false)
        {
            DbQuery<Role> query = ModelContext.Roles;

            if (withUsers)
            {
                query = query.Include("Users");
                query = query.Include("Users.Parameters");
                query = query.Include("Users.SkillSets");
                query = query.Include("Users.AvailableSkillSets");
                query = query.Include("Users.Clusters");
                query = query.Include("Users.Roles");
            }
            if (withSkillSets)
                query = query.Include("SkillSets");

            return query;
        }

        public bool CreateRole(string roleName)
        {
            var dbRole = GetAllUsers(true).FirstOrDefault(t => t.Name == roleName);
            if (dbRole == null)
            {
                ModelContext.Roles.Add(new Role {Name = roleName});
                return true;
            }
            return false;
        }

        public void DeleteRole(string roleName)
        {
            var role = GetRole(roleName, withUsers: true);
            role.Users.Clear();
            ModelContext.Roles.Remove(role);
        }

        public Role GetRole(string roleName, bool withUsers = false, bool throwIfNotExists = true)
        {
            var role = GetAllRoles(withUsers)
                       .FirstOrDefault(t => string.Compare(t.Name, roleName, StringComparison.InvariantCultureIgnoreCase) == 0);
            if (role == null && throwIfNotExists)
            {
                throw new ObjectNotFoundException(string.Format("Role with name {0} wasn't found", roleName));
            }
            return role;
        }

        #endregion roles
    }
}
