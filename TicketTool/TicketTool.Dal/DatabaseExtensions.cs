﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;

namespace TicketTool.Dal
{
    public static class DatabaseExtensions
    {
        public static TResult ExecuteMultyRecordCommand<TResult>(this Database database, string command, Func<DbDataReader, TResult> readerFunc)
        {
            try
            {
                database.Connection.Open();
                using (var cmd = database.Connection.CreateCommand())
                {
                    cmd.CommandText = command;
                    using (var reader = cmd.ExecuteReader())
                    {
                        return readerFunc(reader);
                    }
                }
            }
            finally
            {
                database.Connection.Close();
            }

        }

        public static IEnumerable<TResult> ExecuteStoredProcedure<TResult>(this Database database, object procedure)
        {             
            var parameters = CreateSqlParametersFromProperties(procedure);
            var format = CreateSPCommand(procedure.GetType().Name, parameters);
            return database.SqlQuery<TResult>(format, parameters.Cast<object>().ToArray());
        }

        private static List<SqlParameter> CreateSqlParametersFromProperties(object procedure)
        {
            return procedure.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)
                            .Select(pi => new SqlParameter(string.Format("@{0}", pi.Name), pi.GetValue(procedure, new object[] { })))
                            .ToList();
        }
        
        private static string CreateSPCommand(string procedureName, List<SqlParameter> parameters)
        {            
            string queryString = string.Format("sp_{0}", procedureName);
            parameters.ForEach(x => queryString = string.Format("{0} {1},", queryString, x.ParameterName));
            return queryString.TrimEnd(',');
        }
    }
}
