﻿using System.Linq;
using TicketTool.Dal;

namespace UpdateTaskFields207
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var ctx = new ModelContext())
            {
                var fields = ctx.FieldTypes.Where(f => f.Hash == 0).ToList();
                foreach (var field in fields)
                {
                    field.Hash = field.GetHashCode();
                    ctx.SaveChanges();
                }
            }
        }
    }
}
