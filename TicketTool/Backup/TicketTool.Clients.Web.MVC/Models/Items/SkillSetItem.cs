﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace TicketTool.Web.Models.Items
{
    public class SkillSetItem
    {
        [Display(Name = "Имя")]
        public string Name { get; set; }

        [Display(Name = "Параметр")]
        public TicketToolAdminLink.SkillSetParamType ParamType { get; set; }

        public bool IsChecked { get; set; }

        [Display(Name = "Эскалации")]
        public bool IsEscalated { get; set; }

        [Display(Name = "Задачи почтой")]
        public bool TasksDeliveredByEmail { get; set; }

        public List<string> Roles { get; set; }

        public string RolesString
        {
            get { return Roles != null ? string.Join(", ", Roles) : ""; }
            set { Roles = !String.IsNullOrEmpty(value) ? value.Split(new[] { ", " }, StringSplitOptions.RemoveEmptyEntries).ToList() : new List<string>(); }
        }

        public List<SelectListItem> ParamTypeList
        {
            get
            {
                return Enum.GetValues(typeof(TicketToolLink.SkillSetParamType)).Cast<TicketToolLink.SkillSetParamType>().Select(v => new SelectListItem
                {
                    Text = v.ToString().Replace("_", " "),
                    Value = ((int)v).ToString(CultureInfo.InvariantCulture)
                }).ToList();
            }
        }

        public bool IsClustered { get; set; }
    }
}