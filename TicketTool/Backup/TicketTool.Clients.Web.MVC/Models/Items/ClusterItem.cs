﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TicketTool.Web.Models.Items
{
    public class ClusterItem:SelectableItem
    {
        public List<string> Parameters { get; set; }

        public List<ParameterItem> AllParameters { get; set; } 
    }
}