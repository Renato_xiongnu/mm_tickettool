﻿using System.Collections.Generic;
using TicketTool.Web.Models.Items;
using TicketTool.Web.TicketToolLink;

namespace TicketTool.Web.Models
{
    public class SimpleTaskData
    {
        public SimpleTaskData()
        {
            LoggedSkillsets = new List<SkillSetItem>();
        }

        public List<SkillSetItem> LoggedSkillsets { get; set; }

        public TaskItem Task { get; set; }

        public PingTask PingTask { get; set; }

        public bool IsSinglePage { get; set; }
    }
}