﻿using System.Collections.Generic;
using System.Linq;
using TicketTool.Web.Common;
using TicketTool.Web.Models.Items;
using TicketTool.Web.TicketToolAdminLink;

namespace TicketTool.Web.Models
{
    public class TaskTypesData
    {
        public ExternalSystemInfo[] Processes { get; set; }

        public ExternalSystemInfo CurrentProcess 
        { 
            get
            {
                if (Processes != null && Processes.Length > 0)
                    return Processes[0];
                return null;
            } 
        }

        public List<TaskTypeItem> TaskTypes { get; set; }

        public List<SkillSetItem> BaseSkillSets { get; set; }

        public List<SkillSetItem> EscalatedSkillSets { get; set; }

        public static List<string> Roles
        {
            get
            {
                return ProxyCaller.GetAllRoles().ToList();
            }
        }
    }
}