﻿using System.Collections.Generic;
using TicketTool.Web.Models.Items;

namespace TicketTool.Web.Models
{
    public class SimpleData
    {
        public SimpleData()
        {
            UserSkillSet = new List<SkillSetItem>();
        }

        public List<SkillSetItem> UserSkillSet { get; set; }

        public List<ClusterItem> Clusters { get; set; } 
    }
}