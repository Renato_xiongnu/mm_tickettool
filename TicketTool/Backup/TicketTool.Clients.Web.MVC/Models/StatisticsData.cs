﻿using System.Collections.Generic;
using System.Linq;
using TicketTool.Web.Common;
using TicketTool.Web.Models.Items;
using TicketTool.Web.TicketToolAdminLink;

namespace TicketTool.Web.Models
{
    public class StatisticsData
    {
        public StatisticsData()
        {
            SkillSetStat = new SkillSetStat[0];
            StoreRealTimeStat = new ParameterRealTimeStat[0];
            UserLogStat = new List<UserLogItem>();
        }

        public SkillSetStat[] SkillSetStat { private get; set; }

        public SkillSetStatItem[] WorkSkillSets
        {
            get { return SkillSetStat != null ? SkillSetStat.Where(t => !t.IsEscalation).Select(m => m.ToModel()).ToArray() : new SkillSetStatItem[0]; }
        }

        public SkillSetStatItem[] EscalatedSkillSets
        {
            get { return SkillSetStat != null ? SkillSetStat.Where(t => t.IsEscalation).Select(m => m.ToModel()).ToArray() : new SkillSetStatItem[0]; }
        }

        public ParameterRealTimeStat[] StoreRealTimeStat { get; set; }

        public List<UserLogItem> UserLogStat { get; private set; }

        public void UpdateUserLog(IEnumerable<UserStat> userStats)
        {
            var groupUserStat = userStats.GroupBy(t => t.UserName);

            foreach (var user in groupUserStat)
            {
                var oldUser = UserLogStat.SingleOrDefault(t => t.UserName == user.Key);
                var stats = user.ToArray();
                if (oldUser == null)
                {
                    UserLogStat.Add(UserLogItem.FromContractModel(user.Key, stats));
                }
                else
                {
                    oldUser.UpdateFromContractModel(stats);
                    foreach (var userStat in stats)
                    {
                        oldUser.Lines.Add(UserLogLineItem.FromContractModel(userStat));
                    }
                }
            }
        }
    }
}