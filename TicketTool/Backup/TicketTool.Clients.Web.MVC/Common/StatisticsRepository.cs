﻿using TicketTool.Web.Models;

namespace TicketTool.Web.Common
{
    public static class Repository
    {
        public static StatisticsData StatisticsRepository
        {
            get
            {
                return Extensions.GetFromSession<StatisticsData>("StatisticsRepository");
            }
        }

        public static TaskTypesData TaskTypesRepository
        {
            get
            {
                return Extensions.GetFromSession<TaskTypesData>("TaskTypesRepository");
            }
        }


        public static UsersData UsersRepository
        {
            get
            {
                return Extensions.GetFromSession<UsersData>("UsersRepository");
            }
        }
    }
}