﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using TicketTool.Web.Models.Items;

namespace TicketTool.Web.Common
{
    public static class Extensions
    {
        public static object GetDefaultValue(Type t)
        {
            return t.IsValueType ? Activator.CreateInstance(t) : null;
        }

        public static bool Convert(this string input, Type outType, out object result)
        {
            result = GetDefaultValue(outType);
            if (input == null)
                return true;
            try
            {
                var converter = TypeDescriptor.GetConverter(outType);
                result = converter.ConvertFromString(input);
                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }

        public static string FormatTime(this string value)
        {
            TimeSpan time;
            if (!TimeSpan.TryParse(value, out time)) return "";
            return FormatTime(time);
        }

        public static string FormatTime(this TimeSpan time)
        {
            return String.Format("{0:hh\\:mm\\:ss}", time);
        }

        public static string FormatTime(this TimeSpan? time)
        {
            return time.HasValue ? time.Value.FormatTime() : "";
        }

        public static ICollection<T> SortCollection<T>(this ICollection<T> data, string sortString)
        {
            if (!String.IsNullOrEmpty(sortString))
            {
                return data.AsQueryable().OrderBy(sortString).ToList();
            }
            return data;
        }

        public static T GetFromSession<T>(string key) where T : class, new()
        {
            if (HttpContext.Current.Session[key] == null)
            {
                lock (HttpContext.Current.Session.SyncRoot)
                {
                    if (HttpContext.Current.Session[key] == null)
                        HttpContext.Current.Session[key] = new T();
                }
            }
            return HttpContext.Current.Session[key] as T;
        }

        public static TicketToolAdminLink.SkillSet ToContract(this SkillSetItem item)
        {
            return new TicketToolAdminLink.SkillSet
            {
                Name = item.Name,
                ParamType = item.ParamType,
                IsEscalation = item.IsEscalated,
                TasksDeliveredByEmail = item.TasksDeliveredByEmail,
                Roles = item.Roles.ToArray()
            };
        }

        public static SkillSetItem ToModel(this TicketToolAdminLink.SkillSet item)
        {
            return new SkillSetItem
            {
                Name = item.Name,
                ParamType = item.ParamType,
                TasksDeliveredByEmail = item.TasksDeliveredByEmail,
                IsEscalated = item.IsEscalation,
                Roles = item.Roles.ToList()
            };
        }

        public static TaskTypeItem ToModel(this TicketToolAdminLink.TaskType task)
        {
            return new TaskTypeItem
            {
                Name = task.Name,
                MaxSkillSetRunTime = task.MaxSkillSetTime,
                MaxUserSetTime = task.MaxUserSetTime,

                SkillSet = task.SkillSet,
                EscalatedSkillSet = task.EscalatedSkillSet,
            };
        }

        public static SkillSetStatItem ToModel(this TicketToolAdminLink.SkillSetStat skillSetStat)
        {
            return new SkillSetStatItem
            {
                Name = skillSetStat.Name,
                UsersQty = skillSetStat.UsersCount??0,//.SkillSet.Users.Length,
                EscalatedTasks = skillSetStat.EscalatedTasks,
                TasksInProgress = skillSetStat.TasksInProgress,
                TasksInQueue = skillSetStat.TasksInQueue,
                IsClustered = skillSetStat.IsClustered,
                ChildSkillSets =
                    skillSetStat.ChildStats != null ? skillSetStat.ChildStats.Select(el => el.ToModel()).ToList() : null
            };
        }

        public static UserItem ToModel(this TicketToolAdminLink.User user)
        {
            return new UserItem
            {
                Id = user.Id,
                Email = user.Email,
                Name = user.Name,
                Roles = user.Roles.ToList(),
                Stores = user.Parameters.Select(p=>p.ParameterName).ToList(),
                AvailableSkillSets = user.AvailableSkillSets.ToList(),
                CreatedDate = user.CreatedDate,
                LastLoginDate = user.LastLoginDate,
                IsLocked = user.IsLocked,
            };
        }

    }
}