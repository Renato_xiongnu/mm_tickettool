﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace TicketTool.Web.Common
{
    public static class HtmlExtensions
    {
        public static IHtmlString EditorForCollection<TModel, TProperty>(
            this HtmlHelper<TModel> html,
            Expression<Func<TModel, IList<TProperty>>> expression
        )
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            if (string.IsNullOrEmpty(metadata.TemplateHint))
            {
                return html.EditorFor(expression);
            }

            var collection = metadata.Model as IList<TProperty>;

            var sb = new StringBuilder();
            for (int i = 0; i < collection.Count; i++)
            {
                var indexExpression = Expression.Constant(i, typeof(int));
                var itemGetter = expression.Body.Type.GetProperty("Item", new[] { typeof(int) }).GetGetMethod();
                var methodCallExpression = Expression.Call(expression.Body, itemGetter, indexExpression);
                var itemExpression = Expression.Lambda<Func<TModel, TProperty>>(methodCallExpression, expression.Parameters[0]);
                var result = html.EditorFor(itemExpression, metadata.TemplateHint, new { index = i }).ToHtmlString();
                sb.AppendLine(result);
            }
            return new HtmlString(sb.ToString());
        }
        public static IHtmlString DisplayForCollection<TModel, TProperty>(
            this HtmlHelper<TModel> html,
            Expression<Func<TModel, IList<TProperty>>> expression
        )
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            if (string.IsNullOrEmpty(metadata.TemplateHint))
            {
                return html.EditorFor(expression);
            }

            var collection = metadata.Model as IList<TProperty>;

            var sb = new StringBuilder();
            for (int i = 0; i < collection.Count; i++)
            {
                var indexExpression = Expression.Constant(i, typeof(int));
                var itemGetter = expression.Body.Type.GetProperty("Item", new[] { typeof(int) }).GetGetMethod();
                var methodCallExpression = Expression.Call(expression.Body, itemGetter, indexExpression);
                var itemExpression = Expression.Lambda<Func<TModel, TProperty>>(methodCallExpression, expression.Parameters[0]);
                var result = html.DisplayFor(itemExpression, metadata.TemplateHint, new { index = i }).ToHtmlString();
                sb.AppendLine(result);
            }
            return new HtmlString(sb.ToString());
        }
    }
}