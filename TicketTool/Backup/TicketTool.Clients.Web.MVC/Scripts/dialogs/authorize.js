﻿$(function () {
    makeDialog("#loginUserDialog", {
        title: "Вход",
        width: 450,
        open: function () {
            $('[id$=returnUrl]').val($(this).data('returnUrl'));
            $('[type="password"]', this).on('keypress', function (e, args) {
                if (e.keyCode == $.ui.keyCode.ENTER) {
                    $('.ui-button:contains("OK")').click();
                }
            });
        },
        buttons: {
            "OK": function () {
                $('form', this).submit();
                $(this).dialog("close");
            },
            "Отмена": function () {
                $(this).dialog("close");
            }
        }
    });
    $(".loginUserLink").click(function () {
        $("#loginUserDialog").data("returnUrl", $('[id$="returnUrl"]').val()).dialog("open");
    });
});