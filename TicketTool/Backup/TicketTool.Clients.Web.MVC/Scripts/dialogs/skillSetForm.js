﻿$(function () {
    makeDialog("#skillSetFormDialog", {
        title: "Скилсет",
        width: 450,
        open: function(){
            var type = $(this).data('type');
            if (type === "edit") {
                $('input', this).attr('disabled', 'disabled');
                $('select', this).attr('disabled', 'disabled');
                $('input#Name, input#oldSkillName', this).removeAttr('disabled').val($(this).data('name'));
                $('input#RolesString', this).val($(this).data('roles'));
                var param = $(this).data('param');
                $("select#ParamType option").filter(function () {
                    return $(this).text() == param;
                }).prop('selected', true);
                $('input#IsEscalated', this).prop('checked', $(this).data('isEsc') === 'True');
                $('input#TasksDeliveredByEmail', this).prop('checked', $(this).data('isMail') === 'True');
            }
            hookSelectedChecks($('.rolesTable', this), $('#RolesString', this), ', ');
            selectChecks($('.rolesTable', this), $('#RolesString', this).val(), ', ');
        },
        buttons: {
            "OK": function () {
                var type = $(this).data('type');
                var serialized = serializeFormDisabled($(this));
                $.ajax({
                    type: "get",
                    url: $.routeLinks.UpdateSkillSet,
                    data: serialized + '&type=' + type,
                    success: function (response) {
                        if (response === true) {
                            reloadTaskTypes();
                            reloadSkillSets();
                        }
                        else {
                            alert("не удалось " + (type === "edit" ? "отредактировать" : "создать") + " скиллсет");
                        }
                    }
                });
                $(this).dialog("close");
            },
            Cancel: function () {
                $(this).dialog("close");
            }
        }
    });
    $('.skillCreateLink').click(function () {
        $("#skillSetFormDialog").data("type", "create").dialog("open");
    });
});