﻿$(function () {
    makeDialog("#userFormDialog", {
        title: 'Пользователь',
        width: 700,
        position: { my: "top", at: "top" },
        open: function () {
            var userName = $(this).data('userName');
            $(this).html(generateLoader($(this), null, 50));
            $(this).load(userName === '' ? $.routeLinks.CreateUserDialog : $.routeLinks.UpdateUserDialog, { userName: userName }, function () {
                setUserDialog($(this), userName === '' ? "create" : "edit");
            });
        },
        buttons: {
            "Сохранить": function () {
                var thisForm = $('form', this);
                var userName = $(this).data('userName');
                if (thisForm.valid()) {
                    var serialized = thisForm.serialize();
                    $.ajax({
                        type: "post",
                        url: userName === '' ? $.routeLinks.CreateUser : $.routeLinks.UpdateUser,
                        data: serialized,
                        success: function (response) {
                            if (response === true) {
                                refreshControl($.routeLinks.Users, 'usersContainer', false, true, function () {
                                    refreshEvents();
                                });
                            }
                            else {
                                alert("не удалось " + (userName === '' ? "создать" : "отредактировать") + " пользователя");
                            }
                        }
                    });
                    $(this).dialog("close");
                }
            },
            Cancel: function () {
                $(this).dialog("close");
            }
        }
    });
    $('.userCreateLink').click(function () {
        $("#userFormDialog").data('userName', '').dialog("open");
    });
    $('.userEditLink').click(function () {
        var userName = $(this).attr('data-userName');
        $("#userFormDialog").data('userName', userName).dialog("open");
    });
});