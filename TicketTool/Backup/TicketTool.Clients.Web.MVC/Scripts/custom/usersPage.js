﻿function setUserDialog(container, type) {
    $form = $('form', container).validate({
        onfocusout: false,
        onkeyup: false,
        rules: {
            Email: { required: true, regex: /^[^<>\s\@]+(\@[^<>\s\@]+(\.[^<>\s\@]+)+)$/ },
            Name: { required: true },
            RepeatPassword: { equalTo: "input#Password" }
        },
        messages: {
            Name: { required: "Поле 'Имя' обязательно." },
            Email: {
                required: "Поле 'Email' обязательно.",
                regex: "Неверный email."
            },
            RepeatPassword: {
                equalTo: "Пароль и подтвержение не совпадают"
            }

        },
        errorPlacement: function (error, element) {
            error.appendTo(element.parent());
        }
    });
    $form.resetForm();
    setValidationRules(container, type);
    hookSelectedChecks($('.rolesContainer'), $('#RolesString'), ', ');
    selectChecks($('.rolesContainer'), $('#RolesString').val(), ', ');
    hookSelectedChecks($('.storesContainer'), $('#StoresString'), ', ');
    selectChecks($('.storesContainer'), $('#StoresString').val(), ', ');
    $('.rolesContainer input[type="checkbox"]').on('click', function () {
        loadSkillSets();
    });
    loadSkillSets();
}

function refreshEvents() {
    $('.userEditLink').click(function () {
        var userName = $(this).attr('data-userName');
        $("#userFormDialog").data('userName', userName).dialog("open");
    });

}

function setValidationRules(container, type) {
    if (type === 'create') {
        $('form input#Password', container).rules("add", {
            required: true,
            messages: {
                required: "Поле 'Пароль' обязательно.",
            }
        });
        $('form input#Name', container).rules("add", {
            remote: {
                url: $.routeLinks.CheckUser,
                type: "get",
                data: {
                    id: function () {
                        var val = $('form input#id').val();
                        return val === undefined ? 0 : val;
                    }
                }
            },
            messages: {
                remote: "Недоступное значение поля 'Имя'."
            }
        });
    }
    else if (type === 'edit') {
        $('form input#Password', container).rules("remove");
        $('form input#Name', container).rules("remove", "remote");
    }
}

function loadSkillSets() {
    $.ajax({
        type: "get",
        url: $.routeLinks.AvailableSkillSets,
        beforeSend: function () {
            $('.skillSetsContainer').html(generateLoader($('.skillSetsContainer')));
        },
        data: { roles: $('#RolesString').val() },
        success: function (response) {
            $('.skillSetsContainer').html(response);
            hookSelectedChecks($('.skillSetsContainer'), $('#AvailableSkillSetsString'), ', ');
            selectChecks($('.skillSetsContainer'), $('#AvailableSkillSetsString').val(), ', ');
        }
    });
}

function usersGridViewCallback() {
    refreshEvents();
}