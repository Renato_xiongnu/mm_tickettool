﻿function setFrameUrl(url) {
    $('iframe#orderFrame').attr("src", url);
}

function setChangingTimeout(callback, firstTime, stableTime) {
    var internalCallback = function (stableTime) {
        return function () {
            window.setTimeout(internalCallback, stableTime);
            callback();
        }
    }(stableTime);
    window.setTimeout(internalCallback, firstTime);
};

$(function () {
    var pingInterval = $('#pingInterval').val() * 1000;
    if (pingInterval <= 0)
        pingInterval = 5000;
    var sleepInterval = $('#sleepInterval').val() * 1000;
    if (sleepInterval <= 0)
        sleepInterval = 3000;
    setChangingTimeout(function(){
        $.ajax({
            type: "get",
            cache:false,
            url: $.routeLinks.SimplePulse,
            data: { taskId : $('[id$="TaskId"]').val() },
            success: function (response) {
                if (response === null || response === '') {
                }
                $('.taskSkillsets').html(response.skillSets);
                $('.taskTime').html(response.taskTime);
                $('.taskMaxTime').html(response.maxTime);
            }
        });
    }, sleepInterval, pingInterval);
    setChangingTimeout(function () {
        $.ajax({
            type: "get",
            cache: false,
            url: $.routeLinks.SimpleCheckTask,
            data: { taskId: $('[id$="TaskId"]').val() },
            success: function (response) {
                if (response !== null && response !== '' && response.taskExists) {
                    $('#reloadButton').click();
                }
            }
        });
    }, sleepInterval, pingInterval);
    //setInterval(function () {
    //    $.ajax({
    //        type: "get",
    //        url: $.routeLinks.SimplePulse,
    //        data: { taskId : $('[id$="TaskId"]').val() },
    //        success: function (response) {
    //            if (response === null || response === '') {
    //            }
    //            $('.taskSkillsets').html(response.skillSets);
    //            $('.taskTime').html(response.taskTime);
    //            $('.taskMaxTime').html(response.maxTime);
    //        }
    //    });
    //}, pingInterval);
    //setInterval(function () {
    //    $.ajax({
    //        type: "get",
    //        url: $.routeLinks.SimpleCheckTask,
    //        data: { taskId: $('[id$="TaskId"]').val() },
    //        success: function (response) {
    //            if (response !== null && response !== '' && response.taskExists) {
    //                $('#reloadButton').click();
    //            }
    //        }
    //    });
    //}, pingInterval);

    $('.datepicker').inputmask("d.m.y").datepicker({
        minDate: new Date(),
        dateFormat: 'dd.mm.yy',
        buttonImage: approot + '/Content/Images/calendar.gif',
        buttonImageOnly: true,
        showOn: 'both'
    });

    $('.accordion').accordion({ active: false, collapsible: true, heightStyle: "content" });
    setFrameUrl($('[id$="InfoUrl"]').val());
    $('.spinner').spinner();
    $('form').on('submit', function () {
        $('.outcomeSubmit').attr('disabled', 'disabled');
    });
    var $timespans = $('.timespanField');
    $('.textField.datepicker').on('change keyup', function () {
        var textDate = $(this).val();
        var $button = $(this).closest('.outcomeBlock').find('.outcomeSubmit');
        if (/\d{2}\.\d{2}\.\d{4}/.test(textDate)) {
            $button.removeAttr('disabled');
        }
        else {
            $button.attr('disabled', 'disabled');
        }
    });
    $('.textField').on('change keyup', function () {
        var text = $(this).val();
        var $button = $(this).closest('.outcomeBlock').find('.outcomeSubmit');
        var empty = $(this).closest('.outcomeBlock').find(".textField").filter(function () {
            return this.value === "";
        });
        if (text && empty.length===0) {
            $button.removeAttr('disabled');
        } else {
            $button.attr('disabled', 'disabled');
        }
    });
    $('.textField.datepicker').trigger('change');
    $('.textField').trigger('change');
    $timespans.on('change', function () {
        var $button = $(this).closest('.outcomeBlock').find('.outcomeSubmit');
        validateTimeSpan($timespans, $button);
    });
    $timespans.trigger('change');
});

function validateTimeSpan(timespans, button) {
    var counter = 0;
    timespans.each(function () {
        if ($(this).val() == 0)
            counter++;
    });
    button.removeAttr('disabled');
    if (counter === timespans.length) {
        button.attr('disabled', 'disabled');
    }
}