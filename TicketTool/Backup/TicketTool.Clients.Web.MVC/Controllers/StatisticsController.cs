﻿using System.Linq;
using System.Web.Mvc;
using TicketTool.Web.Common;

namespace TicketTool.Web.Controllers
{
    public class StatisticsController : Controller
    {
        public ActionResult Index()
        {
            var user = HttpContext.User;
            if (user == null || !user.Identity.IsAuthenticated)
            {
                if (Request.Browser.IsMobileDevice)
                {
                    return RedirectToAction("Index", "Account", new { returnUrl = Request.Url.ToString() });
                }
                return View(Repository.StatisticsRepository);
            }
            Repository.StatisticsRepository.SkillSetStat = ProxyCaller.GetSkillStatistics();
            Repository.StatisticsRepository.StoreRealTimeStat = ProxyCaller.GetStoreStatistics();
            Repository.StatisticsRepository.UpdateUserLog(ProxyCaller.GetUserStatistics());
            return View(Repository.StatisticsRepository);
        }

        [Authorize]
        [OutputCache(CacheProfile = "StatisticsCache")]
        public ActionResult WorkSkillsetGridView(string customSort)
        {
            Repository.StatisticsRepository.SkillSetStat = ProxyCaller.GetSkillStatistics();
            return PartialView("StatisticsControls/WorkSkillsetGridView", Repository.StatisticsRepository.WorkSkillSets.ToList().SortCollection(customSort));
        }

        [Authorize]
        [OutputCache(CacheProfile = "StatisticsCache")]
        public ActionResult EscalatedSkillsetGridView(string customSort)
        {
            Repository.StatisticsRepository.SkillSetStat = ProxyCaller.GetSkillStatistics();
            return PartialView("StatisticsControls/EscalatedSkillsetGridView", Repository.StatisticsRepository.EscalatedSkillSets.ToList().SortCollection(customSort));
        }

        [Authorize]
        [OutputCache(CacheProfile = "StatisticsCache")]
        public ActionResult StoresGridView(string customSort)
        {
            Repository.StatisticsRepository.StoreRealTimeStat = ProxyCaller.GetStoreStatistics();
            return PartialView("StatisticsControls/StoresGridView", Repository.StatisticsRepository.StoreRealTimeStat.ToList().SortCollection(customSort));
        }

        [Authorize]
        [OutputCache(CacheProfile = "StatisticsCache")]
        public ActionResult UsersTreeView(string customSort)
        {
            Repository.StatisticsRepository.UpdateUserLog(ProxyCaller.GetUserStatistics());
            return PartialView("StatisticsControls/UsersTreeView", Repository.StatisticsRepository.UserLogStat);
        }
    }
}
