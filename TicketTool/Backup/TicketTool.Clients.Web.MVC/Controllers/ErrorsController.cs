﻿using System;
using System.Web.Mvc;

namespace TicketTool.Web.Controllers
{
    public class ErrorsController : Controller
    {
        public ActionResult General(Exception exception)
        {
            return View("Error", exception);
        }

        public ActionResult Http404()
        {
            return View("Error", new Exception("Not found"));
        }

        public ActionResult Http403()
        {
            return View("Error", new Exception("Forbidden"));
        }

        public ActionResult JavascriptError()
        {
            return View("Error", new Exception("Server Error"));
        }
    }
}
