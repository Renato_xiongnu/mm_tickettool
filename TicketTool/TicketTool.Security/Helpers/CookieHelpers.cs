﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;

namespace TicketTool.Security.Helpers
{
    public static class CookieHelpers
    {
        public static HttpCookie[] ParseSetCookies(string str)
        {
            var cookies = Regex.Split(str, @",{1}(?=[^\s])"); ;            
            return cookies.Length > 0 ? cookies.Select(ParseCookie).ToArray() : new HttpCookie[0];
        }
        

        public static HttpCookie ParseCookie(string str)
        {
            var regexp = new Regex(@"(?<key>[^=;\s]+)(=(?<value>[^=;]+))?", RegexOptions.IgnoreCase);            
            DateTime? expires = null;
            var path = "/";
            var httpOnly = false;
            var name = string.Empty;
            var cvalue = string.Empty;
            foreach (Match match in regexp.Matches(str))
            {
                var key = match.Groups["key"].Value.Trim();
                var value = match.Groups["value"].Value.Trim();
                switch(key)
                {
                    case "expires":
                        expires = DateTime.Parse(value);
                        break;
                    case "path":
                        path = value;
                        break;
                    case "HttpOnly":
                        httpOnly = true;
                        break;
                    default:
                        name = key.Trim();
                        cvalue = value;
                        break;

                }
            }
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }
            var httpCookie = new HttpCookie(name)
            {
                Value = cvalue,
                Path = path,                
                HttpOnly = httpOnly
            };
            if(expires.HasValue)
            {
                httpCookie.Expires = expires.Value;
            }
            return httpCookie;
        }
        
    }
}