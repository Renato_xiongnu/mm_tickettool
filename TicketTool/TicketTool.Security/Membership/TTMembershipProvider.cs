﻿using System;
using System.Collections.Specialized;
using System.Configuration.Provider;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.Configuration;
using System.Web.Security;
using TicketTool.Dal;
using TicketTool.Dal.Model;

namespace TicketTool.Security.Membership
{
    public class TTMembershipProvider : MembershipProvider
    {
        private int newPasswordLength = 6;
        private string eventSource = "TTMembershipProvider";

        private MembershipPasswordFormat _pPasswordFormat;
        private MachineKeySection _machineKey;

        #region Initialize method

        public override void Initialize(string name, NameValueCollection config)
        {
            if (config == null)
                throw new ArgumentNullException("config");

            if (name == String.Empty) name = eventSource;

            if (String.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "Sample TT Membership provider");
            }

            base.Initialize(name, config);

            ApplicationName = name;

            var tempFormat = config["passwordFormat"] ?? "Hashed";
            switch (tempFormat)
            {
                case "Hashed":
                    _pPasswordFormat = MembershipPasswordFormat.Hashed;
                    break;
                case "Encrypted":
                    _pPasswordFormat = MembershipPasswordFormat.Encrypted;
                    break;
                case "Clear":
                    _pPasswordFormat = MembershipPasswordFormat.Clear;
                    break;
                default:
                    throw new ProviderException("Password format not supported.");
            }

            var cfg = WebConfigurationManager.OpenWebConfiguration(System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
            _machineKey = (MachineKeySection)cfg.GetSection("system.web/machineKey");
            if (_machineKey.ValidationKey.Contains("AutoGenerate") && PasswordFormat != MembershipPasswordFormat.Clear)
            {
                throw new ProviderException("Hashed or Encrypted passwords are not supported with auto-generated keys.");
            }
        }

        #endregion Initialize method

        #region public properties

        public override string ApplicationName
        {
            get;
            set;
        }

        public override bool EnablePasswordReset
        {
            get { return true; }
        }

        public override bool EnablePasswordRetrieval
        {
            get { return false; }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { return false; }
        }

        public override bool RequiresUniqueEmail
        {
            get { return true; }
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { return 1000; }
        }

        public override int PasswordAttemptWindow
        {
            get { return 0; }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { return _pPasswordFormat; }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return 0; }
        }

        public override int MinRequiredPasswordLength
        {
            get { return 1; }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { return ""; }
        }

        #endregion public properties

        #region public methods

        public override bool ChangePassword(string username, string oldPwd, string newPwd)
        {
            if (!ValidateUser(username, oldPwd))
                return false;

            var args = new ValidatePasswordEventArgs(username, newPwd, true);
            OnValidatingPassword(args);
            if (args.Cancel)
            {
                throw args.FailureInformation ??
                      new MembershipPasswordException("Change password canceled due to new password validation failure.");
            }

            using (var provider = new DataProvider())
            {
                var user = provider.GetUser(username);
                if (user != null)
                {
                    user.Password = EncodePassword(newPwd);
                    provider.Save();
                    return true;
                }
                return false;
            }
        }

        public override bool ChangePasswordQuestionAndAnswer(string username,
                      string password,
                      string newPwdQuestion,
                      string newPwdAnswer)
        {
            throw new NotImplementedException("ChangePasswordQuestionAndAnswer");
        }

        public override MembershipUser CreateUser(string username,
                 string password,
                 string email,
                 string passwordQuestion,
                 string passwordAnswer,
                 bool isApproved,
                 object providerUserKey,
                 out MembershipCreateStatus status)
        {
            var args = new ValidatePasswordEventArgs(username, password, true);
            OnValidatingPassword(args);
            if (args.Cancel)
            {
                status = MembershipCreateStatus.InvalidPassword;
                return null;
            }
            if (RequiresUniqueEmail && GetUserNameByEmail(email) != "")
            {
                status = MembershipCreateStatus.DuplicateEmail;
                return null;
            }
            var u = GetUser(username, false);
            if (u == null)
            {
                if (providerUserKey != null)
                {
                    status = MembershipCreateStatus.InvalidProviderUserKey;
                    return null;
                }

                using (var provider = new DataProvider())
                {
                    var res = provider.CreateUser(new User
                        {
                                                          Name = username,
                                                          Email = email,
                                                          Password = EncodePassword(password),
                                                          IsLocked = false
                                                      });
                    status = !res ? MembershipCreateStatus.DuplicateUserName : MembershipCreateStatus.Success;
                    provider.Save();
                }
                return GetUser(username, false);
            }

            status = MembershipCreateStatus.ProviderError;
            return null;
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            using (var provider = new DataProvider())
            {
                provider.DeleteUser(username);
                provider.Save();
                return true;
            }
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            using (var provider = new DataProvider())
            {
                var users = provider.GetAllUsers(withParameters: true).ToList();
                totalRecords = users.Count;
                var usersColl = new MembershipUserCollection();
                foreach (var u in users.Skip(pageIndex * pageSize).Take(pageSize))
                {
                    usersColl.Add(CreateUserInternal(u));
                }
                return usersColl;
            }
        }

        public override int GetNumberOfUsersOnline()
        {
            using (var provider = new DataProvider())
            {
                var onlineSpan = new TimeSpan(0, System.Web.Security.Membership.UserIsOnlineTimeWindow, 0);
                var lastActivity = DateTime.Now.ToUniversalTime() - onlineSpan;
                return provider.GetAllUsers().Count(u => u.LastPingDateTime < lastActivity);
            }
        }

        public override string GetPassword(string username, string answer)
        {
            if (!EnablePasswordRetrieval)
            {
                throw new ProviderException("Password Retrieval Not Enabled.");
            }

            if (PasswordFormat == MembershipPasswordFormat.Hashed)
            {
                throw new ProviderException("Cannot retrieve Hashed passwords.");
            }

            throw new NotImplementedException("GetPassword");
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            using (var provider = new DataProvider())
            {
                var user = provider.GetUser(username, withStores: true, throwIfNotExists: false, withRoles: true);
                //MembershipUser u = null;
                if (user != null && userIsOnline)
                {
                    user.LastPingDateTime = DateTime.Now.ToUniversalTime();
                    provider.Save();
                }
                return CreateUserInternal(user);
            }
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            using (var provider = new DataProvider())
            {
                var userId = (long)providerUserKey;
                var user = provider.GetAllUsers(withParameters: true).FirstOrDefault(u => u.UserId == userId);
                if (user != null && userIsOnline)
                {
                    user.LastPingDateTime = DateTime.Now.ToUniversalTime();
                    provider.Save();
                }
                return CreateUserInternal(user);
            }
        }

        public override bool UnlockUser(string username)
        {
            using (var provider = new DataProvider())
            {
                var user = provider.GetUser(username, throwIfNotExists: false);
                if (user != null)
                {
                    user.IsLocked = false;
                    provider.Save();
                    return true;
                }
                return false;
            }
        }

        public override string GetUserNameByEmail(string email)
        {
            using (var provider = new DataProvider())
            {
                var user = provider.GetAllUsers()
                           .FirstOrDefault(u => string.Compare(u.Email, email, StringComparison.InvariantCultureIgnoreCase) == 0);
                return user == null ? "" : user.Name;
            }
        }

        public override string ResetPassword(string username, string answer)
        {
            if (!EnablePasswordReset)
            {
                throw new NotSupportedException("Password reset is not enabled.");
            }
            if (answer == null && RequiresQuestionAndAnswer)
            {
                //UpdateFailureCount(username, "passwordAnswer");
                throw new ProviderException("Password answer required for password reset.");
            }

            var newPassword = System.Web.Security.Membership.GeneratePassword(newPasswordLength, MinRequiredNonAlphanumericCharacters);
            var args = new ValidatePasswordEventArgs(username, newPassword, true);
            OnValidatingPassword(args);
            if (args.Cancel)
            {
                throw args.FailureInformation ??
                      new MembershipPasswordException("Reset password canceled due to password validation failure.");
            }

            using (var provider = new DataProvider())
            {
                var user = provider.GetUser(username);
                if (user.IsLocked)
                {
                    throw new MembershipPasswordException("The supplied user is locked out.");
                }
                user.Password = EncodePassword(newPassword);
                provider.Save();
                return newPassword;
            }
        }

        public override void UpdateUser(MembershipUser user)
        {
            using (var provider = new DataProvider())
            {
                var dbuser = provider.GetUser(user.UserName, throwIfNotExists: false);
                if (dbuser != null)
                {
                    dbuser.IsLocked = user.IsLockedOut;
                    dbuser.Email = user.Email;

                    provider.Save();
                }
            }
        }

        public override bool ValidateUser(string username, string password)
        {
            using (var provider = new DataProvider())
            {
                var user = provider.GetUser(username, throwIfNotExists: false);
                if (user != null && !user.IsLocked && CheckPassword(password, user.Password))
                {
                    user.LastPingDateTime = user.LastLoginDate = DateTime.Now.ToUniversalTime();
                    provider.Save();
                    return true;
                }
                return false;
            }
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            using (var provider = new DataProvider())
            {
                var users = provider.GetAllUsers(withParameters: true).ToArray() // TODO need refactoring !!!
                                    .Where(u => u.Name.StartsWith(usernameToMatch)).ToList();
                totalRecords = users.Count;
                var usersColl = new MembershipUserCollection();
                foreach (var u in users.Skip(pageIndex * pageSize).Take(pageSize))
                {
                    usersColl.Add(CreateUserInternal(u));
                }
                return usersColl;
            }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            using (var provider = new DataProvider())
            {
                var users = provider.GetAllUsers(withParameters: true).ToArray() // // TODO need refactoring !!!
                    .Where(u => u.Email.StartsWith(emailToMatch)).ToList();
                totalRecords = users.Count;
                var usersColl = new MembershipUserCollection();
                foreach (var u in users.Skip(pageIndex * pageSize).Take(pageSize))
                {
                    usersColl.Add(CreateUserInternal(u));
                }
                return usersColl;
            }
        }

        #endregion public methods

        #region private mathods

        private bool CheckPassword(string password, string dbpassword)
        {
            string pass1 = password;
            string pass2 = dbpassword;
            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Encrypted:
                    pass2 = UnEncodePassword(dbpassword);
                    break;
                case MembershipPasswordFormat.Hashed:
                    pass1 = EncodePassword(password);
                    break;
            }
            return pass1 == pass2;
        }

        private string EncodePassword(string password)
        {
            var encodedPassword = password;
            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Clear:
                    break;
                case MembershipPasswordFormat.Encrypted:
                    encodedPassword = Convert.ToBase64String(EncryptPassword(Encoding.Unicode.GetBytes(password)));
                    break;
                case MembershipPasswordFormat.Hashed:
                    var hash = new HMACSHA1 { Key = HexToByte(_machineKey.ValidationKey) };
                    encodedPassword = Convert.ToBase64String(hash.ComputeHash(Encoding.Unicode.GetBytes(password)));
                    break;
                default:
                    throw new ProviderException("Unsupported password format.");
            }
            return encodedPassword;
        }

        private static byte[] HexToByte(string hexString)
        {
            var returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        }

        private string UnEncodePassword(string encodedPassword)
        {
            var password = encodedPassword;
            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Clear:
                    break;
                case MembershipPasswordFormat.Encrypted:
                    password = Encoding.Unicode.GetString(DecryptPassword(Convert.FromBase64String(password)));
                    break;
                case MembershipPasswordFormat.Hashed:
                    throw new ProviderException("Cannot unencode a hashed password.");
                default:
                    throw new ProviderException("Unsupported password format.");
            }
            return password;
        }

        private MembershipUser CreateUserInternal(User u)
        {
            return u == null ? null : new TTMembershipUser(
                u.UserId, u.Name, u.Email, u.IsLocked,
                u.CreateDate, u.LastLoginDate, u.LastPingDateTime,
                u.Parameters == null ? new string[0] : u.Parameters.Select(s => s.ParameterName).ToArray());
        }

        #endregion private mathods
    }
}
