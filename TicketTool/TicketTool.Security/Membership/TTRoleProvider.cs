﻿using System;
using System.Collections.Specialized;
using System.Configuration.Provider;
using System.Linq;
using System.Web.Security;
using TicketTool.Dal;

namespace TicketTool.Security.Membership
{
    public class TTRoleProvider : RoleProvider
    {
        #region Initialize method

        public override void Initialize(string name, NameValueCollection config)
        {
            if (config == null)
                throw new ArgumentNullException("config");

            if (String.IsNullOrEmpty(name))
                name = "TTRoleProvider";

            if (String.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "Sample TT Role provider");
            }

            base.Initialize(name, config);

            ApplicationName = name;
        }

        #endregion Initialize method

        #region public properties

        public override string ApplicationName
        {
            get;
            set;
        }

        #endregion public properties

        #region public methods

        public override void AddUsersToRoles(string[] usernames, string[] rolenames)
        {
            if (rolenames.Any(rolename => !RoleExists(rolename)))
            {
                throw new ProviderException("Role name not found.");
            }

            foreach (var username in usernames)
            {
                if (username.Contains(","))
                {
                    throw new ArgumentException("User names cannot contain commas.");
                }
                if (rolenames.Any(rolename => IsUserInRole(username, rolename)))
                {
                    throw new ProviderException("User is already in role.");
                }
            }

            var lowerUserNames = usernames.Select(u => u.ToLower());
            var lowerRolesNames = rolenames.Select(u => u.ToLower());

            using (var provider = new DataProvider())
            {
                var roles = provider.GetAllRoles()
                                    .Where(r => lowerRolesNames.Contains(r.Name.ToLower())).ToList();

                var users = provider.GetAllUsers(withRoles: true).ToArray() // TODO need refactoring !!!
                                    .Where(u => lowerUserNames.Contains(u.Name.ToLower())).ToList();

                foreach (var u in users)
                {
                    var roles2Add = roles.Where(r => !u.Roles.Any(rr => rr.RoleId == r.RoleId));
                    u.Roles.AddRange(roles2Add);
                }

                provider.Save();
            }
        }

        public override void CreateRole(string rolename)
        {
            if (rolename.Contains(","))
            {
                throw new ArgumentException("Role names cannot contain commas.");
            }

            using (var provider = new DataProvider())
            {
                var res = provider.CreateRole(rolename);
                if (!res)
                {
                    throw new ProviderException("Role name already exists.");
                }
                provider.Save();
            }
        }

        public override bool DeleteRole(string rolename, bool throwOnPopulatedRole)
        {
            if (!RoleExists(rolename))
            {
                throw new ProviderException("Role does not exist.");
            }

            if (throwOnPopulatedRole && GetUsersInRole(rolename).Length > 0)
            {
                throw new ProviderException("Cannot delete a populated role.");
            }

            using (var provider = new DataProvider())
            {
                provider.DeleteRole(rolename);
                provider.Save();
            }
            return true;
        }

        public override string[] GetAllRoles()
        {
            using (var provider = new DataProvider())
            {
                return provider.GetAllRoles().Select(r => r.Name).ToArray();
            }
        }

        public override string[] GetRolesForUser(string username)
        {
            using (var provider = new DataProvider())
            {
                return provider.GetUser(username, withRoles: true).Roles.Select(r => r.Name).ToArray();
            }
        }

        public override string[] GetUsersInRole(string rolename)
        {
            using (var provider = new DataProvider())
            {
                return provider.GetRole(rolename, withUsers: true).Users.Select(u => u.Name).ToArray();
            }
        }

        public override bool IsUserInRole(string username, string rolename)
        {
            using (var provider = new DataProvider())
            {
                return provider.GetUser(username, withRoles: true)
                               .Roles.Any(r => string.Compare(r.Name, rolename, StringComparison.CurrentCultureIgnoreCase) == 0);
            }
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] rolenames)
        {
            if (rolenames.Any(rolename => !RoleExists(rolename)))
            {
                throw new ProviderException("Role name not found.");
            }

            if (usernames.Any(username => rolenames.Any(rolename => !IsUserInRole(username, rolename))))
            {
                throw new ProviderException("User is not in role.");
            }

            var lowerUserNames = usernames.Select(u => u.ToLower());
            var lowerRolesNames = rolenames.Select(u => u.ToLower());

            using (var provider = new DataProvider())
            {
                var roles = provider.GetAllRoles()
                                    .Where(r => lowerRolesNames.Contains(r.Name.ToLower())).ToList();

                var users = provider.GetAllUsers(withRoles: true).ToArray() // TODO need refactoring !!!
                                   .Where(u => lowerUserNames.Contains(u.Name.ToLower())).ToList();

                foreach (var u in users)
                {
                    var roles2Remove = roles.Where(r => u.Roles.Any(rr => rr.RoleId == r.RoleId)).Select(r => r.RoleId);
                    foreach (var rId in roles2Remove)
                    {
                        var role = u.Roles.First(r => r.RoleId == rId);
                        u.Roles.Remove(role);
                    }
                }

                provider.Save();
            }
        }

        public override bool RoleExists(string rolename)
        {
            using (var provider = new DataProvider())
            {
                return provider.GetRole(rolename, throwIfNotExists: false) != null;
            }
        }

        public override string[] FindUsersInRole(string rolename, string usernameToMatch)
        {
            using (var provider = new DataProvider())
            {
                return provider.GetRole(rolename, withUsers: true).Users
                    .Where(u => u.Name.StartsWith(usernameToMatch))
                    .Select(u => u.Name).ToArray();
            }
        }

        #endregion public methods
    }
}
