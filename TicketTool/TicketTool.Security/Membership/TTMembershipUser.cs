﻿using System;
using System.Web.Security;

namespace TicketTool.Security.Membership
{
    [Serializable]
    public class TTMembershipUser : MembershipUser
    {
        public string[] SapCodes
        {
            get;
            set;
        }

        public TTMembershipUser(long id,
                                string name,
                                string email,
                                bool isLocked,
                                DateTime creationDate,
                                DateTime lastLoginDate,
                                DateTime lastPingDate,
                                string[] sapCodes) :
            base("TTMembershipProvider",
                name,
                id,
                email,
                "",
                "",
                true,
                isLocked,
                creationDate,
                lastLoginDate,
                lastPingDate,
                creationDate,
                creationDate)
        {
            SapCodes = sapCodes;
        }
    }
}
