﻿using System;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Text.RegularExpressions;
using System.Web;
using TicketTool.Security.Helpers;

namespace TicketTool.Security.Authentication
{
    /// <summary>
    ///     Используется для получения, хранения и дальнейшей передачи cookie от wcf сервиса
    /// </summary>
    public class CookieMessageInspector : IClientMessageInspector
    {
        #region [Singletone]

// ReSharper disable InconsistentNaming
        private static CookieMessageInspector instance;
// ReSharper restore InconsistentNaming

        private CookieMessageInspector()
        {
            this._cookie = String.Empty;
        }

        public static CookieMessageInspector Instance
        {
            get { return instance ?? (instance = new CookieMessageInspector()); }
        }

        #endregion

        private string _cookie;

        /// <summary>
        ///     получить и записать cookie из сообщения
        /// </summary>
        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
            var httpResponse = reply.Properties[HttpResponseMessageProperty.Name] as HttpResponseMessageProperty;
            if (httpResponse == null) return;
            var cookie = httpResponse.Headers[HttpResponseHeader.SetCookie];
            if(string.IsNullOrEmpty(cookie) || !Regex.IsMatch(cookie, @".ASPXAUTH=[A-Z0-9]*"))
            {
                return;
            }
            var httpCookies = CookieHelpers.ParseSetCookies(cookie);            
            cookie = Regex.Match(cookie, @".ASPXAUTH=([A-Z0-9]*)").Groups[1].Value;
            var context = HttpContext.Current ?? (correlationState as HttpContext);
            if (context != null)
            {
                var authCookie = httpCookies.FirstOrDefault(c => c.Name == ".ASPXAUTH");
                context.Response.AppendCookie(authCookie ?? new HttpCookie(".ASPXAUTH", cookie));
            }
            else
            {
                _cookie = String.Concat(".ASPXAUTH=", cookie);
            }
        }

        /// <summary>
        ///     добавить cookie к запросу
        /// </summary>
        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            var cookie = HttpContext.Current != null ? HttpContext.Current.Request.Headers[HttpRequestHeader.Cookie.ToString()] : this._cookie;
            if (!request.Properties.ContainsKey(HttpRequestMessageProperty.Name))
            {
                request.Properties.Add(HttpRequestMessageProperty.Name, new HttpRequestMessageProperty());
            }
            var httpRequest = (HttpRequestMessageProperty) request.Properties[HttpRequestMessageProperty.Name];
            httpRequest.Headers.Add(HttpRequestHeader.Cookie, cookie);
            return HttpContext.Current;
        }
    }
}