﻿using System;
using System.ServiceModel.Configuration;

namespace TicketTool.Security.Authentication
{
    public class CookieBehaviorExtension : BehaviorExtensionElement
    {
        public override Type BehaviorType
        {
            get { return typeof (CookieEndpointBehavior); }
        }

        protected override object CreateBehavior()
        {
            return new CookieEndpointBehavior();
        }
    }
}