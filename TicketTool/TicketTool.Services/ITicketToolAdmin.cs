﻿using System.Collections.Generic;
using System.ServiceModel;
using TicketTool.Data.Contracts;
using TicketTool.Data.Contracts.Common;

namespace TicketTool.Services
{
    [ServiceContract]
    public interface ITicketToolAdmin
    {
        [OperationContract]
        IEnumerable<ExternalSystemInfo> GetAllExternalSystems();

        [OperationContract]
        IEnumerable<Parameter> GetAllParameters();
            
        [OperationContract]
        IEnumerable<TaskType> GetTasksTypes(string externalSystemId);

        [OperationContract]
        void UpdateTasksType(string externalSystemId, TaskType ttype);

        [OperationContract]
        void UpdateSkillSetRelation(string processId, ICollection<SkillSetRelation> addRelations);

        [OperationContract]
        IEnumerable<SkillSet> GetSkillSets(bool withDefault, bool withRoles);

        [OperationContract]
        IEnumerable<SkillSet> GetAvailableSkillSets(ICollection<string> roles);

        [OperationContract]
        IEnumerable<UserSkillSet> GetUserSkillSets();

        [OperationContract]
        IEnumerable<UserCluster> GetUserClusters();

        [OperationContract]
        bool DeleteSkillSet(string skillSetName);

        [OperationContract]
        void RenameSkillSet(string oldName, string newName);

        [OperationContract]
        IEnumerable<SkillSet> CreateSkillSets(ICollection<SkillSet> skillSets);

        [OperationContract]
        User GetUser(string userName);

        [OperationContract]
        IEnumerable<User> GetAllUsers();

        [OperationContract]
        IEnumerable<Cluster> GetAllClusters();

        [OperationContract]
        bool CheckUserName(long id, string name);

        [OperationContract]
        IEnumerable<User> GetDefaultUsers(Parameter parameter);

        [OperationContract]
        IEnumerable<User> GetUsers(string sapCode, ICollection<string> roles);

        [OperationContract]
        IEnumerable<UserStat> GetUsersStat();

        [OperationContract]
        IEnumerable<SkillSetStat> GetSkillSetStat();

        [OperationContract]
        IEnumerable<ParameterRealTimeStat> GetParamsStat();

        [OperationContract]
        void CreateCluster(Cluster cluster);

        [OperationContract]
        void UpdateCluster(string name, ICollection<string> newParameters);

        [OperationContract]
        void DeleteCluster(Cluster cluster);
    }
}
