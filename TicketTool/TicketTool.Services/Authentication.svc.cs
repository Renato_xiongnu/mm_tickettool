﻿using System;
using System.Web;
using System.Web.Security;
using TicketTool.Data;
using TicketTool.Data.Contracts.Common;
using TicketTool.Security.Membership;
using TicketTool.Services.Behaviors;

namespace TicketTool.Services
{
    [OperationContextBehavior]
    public class Authentication : TtServiceBase, IAuthenticationService
    {
        
        public UserInfo GetCurrentUser()
        {
            var user = Membership.GetUser() as TTMembershipUser;
            return user != null 
                ? new UserInfo { IsAuthtorized = true, LoginName = user.UserName, Email = user.Email, Roles = Roles.GetRolesForUser(), Parameters = user.SapCodes, IsLocked = user.IsLockedOut}
                : new UserInfo();
        }

        public bool LogIn(string login, string password, bool rememberMe)
        {
            if (!Membership.ValidateUser(login, password))
                return false; 

            if (CheckIsLocked(login))
                return false;

            SetAuthCookie(login, rememberMe);
            return true;
        }

        private Boolean CheckIsLocked(String userName)
        {
            var srv = new TicketService();
            User user = null;
            ServiceHelper.RunInContext( ()=>
            {
                user = srv.Users.GetUser(userName);
            });
            return user != null && user.IsLocked;
        }

        public bool LoginByToken(string userName, string userToken, string taskId, string taskToken)
        {
            var user = Membership.GetUser(userName) as TTMembershipUser;
            if(user == null) 
                throw new Exception("User not found");

            var ts = new TicketService();
            if (!ts.Security.CheckToken(userName, userToken))
                throw new Exception("User token isn't valid");

            if (!ts.Security.CheckToken(taskId, taskToken))
                throw new Exception("Task token isn't valid");

            if(!ServiceHelper.RunInContext(() => ts.Tasks.CanProcessed(taskId, userName)))
            {
                return false;
            }
            SetAuthCookie(userName);
            return true;
        }

        private static void SetAuthCookie(string userName, bool remember=false)
        {
            FormsAuthentication.SetAuthCookie(userName, remember);
        }

        public void LogOut()
        {
            FormsAuthentication.SignOut();
        }

        public long CreateUser(string login, string password, string email, string[] roles, string[] sapCodes, string[] availableSkillSets = null)
        {
            try
            {
                var user = Membership.CreateUser(login, password, email);
                if (roles != null && roles.Length > 0)
                {
                    Roles.AddUserToRoles(user.UserName, roles);
                }
                ITicketService srv = new TicketService();
                var id = ServiceHelper.RunInContext(() => srv.Users.UpdateUserParams(user.UserName, sapCodes));
                if (availableSkillSets != null)
                {
                    ServiceHelper.RunInContext(() => srv.Users.UpdateUserAvailableSkillSets(user.UserName, availableSkillSets));
                }
                return id;
            }
            catch (Exception ex)
            {
                Logger.Error("CreateUser exception", ex);
                return 0;
            }
        }

        public bool UpdateUser(bool isLocked, string login, string email, string password, string[] roles, string[] sapCodes, string[] availableSkillSets = null)
        {
            try
            {
                var user = Membership.GetUser(login);
                
                if (user == null)
                {
                    return false;
                }
                user.Email = email;
                
                Membership.UpdateUser(user);
                if (!string.IsNullOrWhiteSpace(password))
                {
                    user.ChangePassword(user.ResetPassword(), password);
                }
                var currRoles = Roles.GetRolesForUser(user.UserName);
                if (currRoles.Length > 0)
                {
                    Roles.RemoveUserFromRoles(user.UserName, currRoles);
                }
                if (roles != null && roles.Length > 0)
                {
                    Roles.AddUserToRoles(user.UserName, roles);
                }
                var srv = new TicketService();
                ServiceHelper.RunInContext(() => srv.Users.UpdateUserParams(user.UserName, sapCodes));
                ServiceHelper.RunInContext(() => srv.Users.LockUnlockUser(user.UserName, isLocked));
                if (availableSkillSets != null)
                {
                    ServiceHelper.RunInContext(() => srv.Users.UpdateUserAvailableSkillSets(user.UserName, availableSkillSets));
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("UpdateUser exception", ex);
                return false;
            }
        }

        public string[] GetAllRoles()
        {
            return Roles.GetAllRoles();
        }
    }
}
