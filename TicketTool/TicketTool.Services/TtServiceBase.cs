﻿using log4net;

namespace TicketTool.Services
{
    public class TtServiceBase
    {
        public TtServiceBase()
        {
            Logger = LogManager.GetLogger(GetType().Name);
        }

        protected readonly ILog Logger;
    }
}