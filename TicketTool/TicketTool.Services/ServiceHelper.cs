﻿using System;
using TicketTool.Data.Context;

namespace TicketTool.Services
{
    public static class ServiceHelper
    {
        public static T RunInContext<T>(Func<T> function)
        {
            return OperationContextHolder<TicketToolContext>.Current.RunInServiceContext(function);
        }

        public static void RunInContext(Action action)
        {
            OperationContextHolder<TicketToolContext>.Current.RunInServiceContext(action);
        }
    }
}