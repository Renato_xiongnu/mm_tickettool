﻿using System;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using log4net;

namespace TicketTool.Services.Behaviors
{
    [AttributeUsage(AttributeTargets.Interface)]
    public class ErrorPolicyBehaviorAttribute : Attribute, IContractBehavior, IErrorHandler
    {
        private ILog _logger;

        #region IErrorHandler

        public void ProvideFault(Exception error, MessageVersion version, ref Message fault)
        {
            return;
        }

        public bool HandleError(Exception error)
        {
            _logger.Error(error.Message, error);
            return true;
        }

        #endregion

        #region IContractBehavior

        public void Validate(ContractDescription contractDescription, ServiceEndpoint endpoint)
        {
            
        }

        public void ApplyDispatchBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, DispatchRuntime dispatchRuntime)
        {
            log4net.Config.XmlConfigurator.Configure();
            _logger = LogManager.GetLogger("TicketTool.Services");
            dispatchRuntime.ChannelDispatcher.ErrorHandlers.Add(this);
        }

        public void ApplyClientBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
        }

        public void AddBindingParameters(ContractDescription contractDescription, ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        #endregion
    }
}