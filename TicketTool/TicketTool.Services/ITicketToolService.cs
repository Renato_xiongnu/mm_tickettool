﻿using System;
using System.ServiceModel;
using System.Collections.Generic;
using TicketTool.Data.Contracts;
using TicketTool.Data.Contracts.Common;
using TicketTool.Data.Contracts.Operations;
using TicketTool.Data.Contracts.Tasks;
using TicketTool.Services.Behaviors;

namespace TicketTool.Services
{    
    [ErrorPolicyBehavior]
    [ServiceContract]
    public interface ITicketToolService
    {
        [OperationContract]
        string CreateTicket(string externalSystemId, string workItemId, string parameter);

        /// <summary>
        /// Get ticket info by workitem (order) ID
        /// </summary>
        /// <param name="workItemId"></param>
        /// <returns></returns>
        [OperationContract]
        string GetTicket(string workItemId);

        [OperationContract]
        void CloseTicket(string ticketId);

        [OperationContract]
        string CreateTask(TicketTask task);

        [OperationContract]
        ICollection<TaskListItem> GetTasksByParameter(Parameter parameter);

        [OperationContract]
        ICollection<TaskListItem> GetTasks(SkillSet skillSet, Parameter parameter);

        [OperationContract]
        TicketTask GetTask(string taskId);

        [OperationContract]
        AssignTaskForMeResult AssignTaskForMe(string taskId);

        [OperationContract]
        AssignTaskForMeResult ForceAssignTaskForMe(string taskId);

        [OperationContract]
        IEnumerable<TicketTask> GetTasksByWorkitemId(string workitemId);

        [OperationContract]
        OperationResult CompleteTask(string taskId, string outcome, ICollection<RequiredField> requiredFields);

        [OperationContract]
        TicketTask GetNextTask();
        
        [OperationContract]
        TicketTask GetNextTaskSkipAssigned();

        [OperationContract]
        OperationResult PostponeTask(string taskId, TimeSpan? time);

        [OperationContract]
        void CancelTask(string taskId);

        [OperationContract]
        PingByUserResult Ping();        

        [OperationContract]
        void LogInToSkillSets(ICollection<SkillSet> skillSets);

        [OperationContract]
        void LogOutFromSkillSets();

        [OperationContract]
        void LogOutAndLogInToSkillSets(ICollection<SkillSet> skillSets, ICollection<Cluster> clusters = null);

        [OperationContract]
        User GetCurrentPerformer();

        [OperationContract]
        ICollection<TaskListItem> GetMyTasksInProgress(string[] parameters, string[] skillSets);

        [OperationContract]
        void LogInToAvailableSkillSets();

        [OperationContract]
        ICollection<Parameter> GetMyParameters();

        /// <summary>
        /// Get ticket info by ticket ID
        /// </summary>
        /// <param name="ticketId"></param>
        /// <returns></returns>
        [OperationContract]
        TicketTO GetTicketInfo(string ticketId);
    }

}
