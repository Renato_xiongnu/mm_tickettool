﻿using System;
using System.Runtime.Serialization;
using System.ServiceModel;
using TicketTool.Services.Behaviors;

namespace TicketTool.Services
{
    [ErrorPolicyBehavior]
    [ServiceContract]
    public interface IAuthenticationService
    {
        [OperationContract]
        UserInfo GetCurrentUser();

        [OperationContract]
        bool LogIn(string login, string password, bool rememberMe);

        [OperationContract]
        bool LoginByToken(string userName, string userToken, string taskId, string taskToken);

        [OperationContract]
        void LogOut();

        [OperationContract]
        long CreateUser(string login, string password, string email, string[] roles, string[] sapCodes, string[] availableSkillSets);

        [OperationContract]
        bool UpdateUser(bool isLocked, string login, string email, string password, string[] roles, string[] sapCodes, string[] availableSkillSets);

        [OperationContract]
        string[] GetAllRoles();
    }

    [DataContract]
    public class UserInfo
    {
        public UserInfo()
        {
            Roles = new string[0];
            Parameters = new string[0];
        }

        [DataMember]
        public bool IsAuthtorized { get; set; }

        [DataMember]
        public string LoginName { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string[] Roles { get; set; }

        [DataMember]
        public string[] Parameters { get; set; }

        [DataMember]
        public Boolean IsLocked { get; set; }
    }
}
