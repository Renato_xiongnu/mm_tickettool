﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Authentication;
using System.Web;
using System.Web.Profile;
using TicketTool.Data;
using TicketTool.Data.Common;
using TicketTool.Data.Contracts;
using TicketTool.Data.Contracts.Common;
using TicketTool.Services.Behaviors;

namespace TicketTool.Services
{
    [OperationContextBehavior]
    public class TicketToolAdmin : TtServiceBase, ITicketToolAdmin
    {
        private readonly ITicketService _ticketService = new TicketService();

        public IEnumerable<ExternalSystemInfo> GetAllExternalSystems()
        {
            return ServiceHelper.RunInContext(() => _ticketService.ExternalSystems.GetAllExternalSystems());
        }

        public IEnumerable<Parameter> GetAllParameters()
        {
            return ServiceHelper.RunInContext(() => _ticketService.Parameters.GetAllParameters());
        }

        public IEnumerable<TaskType> GetTasksTypes(string externalSystemId)
        {
            return AuthContext.IsAdmin
                ? ServiceHelper.RunInContext(() => _ticketService.ExternalSystems.GetTasksTypes(externalSystemId))
                : new Collection<TaskType>();
        }

        public void UpdateTasksType(string externalSystemId, TaskType ttype)
        {
            if (!AuthContext.IsAdmin) throw new AuthenticationException();
            ServiceHelper.RunInContext(() => _ticketService.ExternalSystems.UpdateTaskType(externalSystemId, ttype));
        }

        public IEnumerable<SkillSet> GetSkillSets(bool withDefault, bool withRoles = false)
        {
            return ServiceHelper.RunInContext(() => _ticketService.SkillSets.GetSkillSets(withDefault, withRoles));
        }

        public IEnumerable<SkillSet> GetAvailableSkillSets(ICollection<string> roles)
        {
            return ServiceHelper.RunInContext(() => _ticketService.SkillSets.GetAvailableSkillSetsForRoles(roles));
        }

        public IEnumerable<SkillSet> CreateSkillSets(ICollection<SkillSet> skillSets)
        {
            return ServiceHelper.RunInContext(() => _ticketService.SkillSets.CreateSkillSets(skillSets));
        }

        public void RenameSkillSet(string oldName, string newName)
        {
            ServiceHelper.RunInContext(() => _ticketService.SkillSets.RenameSkillSet(oldName, newName));
        }

        public IEnumerable<UserSkillSet> GetUserSkillSets()
        {
            return AuthContext.IsAuthenticated 
                ? ServiceHelper.RunInContext(() => _ticketService.SkillSets.GetUserSkillSets()) 
                : new List<UserSkillSet>();
        }
        public IEnumerable<UserCluster> GetUserClusters()
        {
            return AuthContext.IsAuthenticated
                ? ServiceHelper.RunInContext(() => _ticketService.SkillSets.GetUserClusters())
                : new List<UserCluster>();
        }

        public bool DeleteSkillSet(string skillSetName)
        {
            return ServiceHelper.RunInContext(() => _ticketService.SkillSets.DeleteSkillSet(skillSetName));
        }

        public void UpdateSkillSetRelation(string processId, ICollection<SkillSetRelation> addRelations)
        {
            ServiceHelper.RunInContext(() => _ticketService.SkillSets.UpdateSkillSetRelation(processId, addRelations));
        }

        public IEnumerable<User> GetDefaultUsers(Parameter parameter)
        {
            return ServiceHelper.RunInContext(() => _ticketService.Users.GetDefaultUsersByParam(parameter));
        }

        public IEnumerable<User> GetUsers(string sapCode, ICollection<string> roles)
        {
            return ServiceHelper.RunInContext(() => _ticketService.Users.GetUsers(new Parameter { ParameterName = sapCode }, roles));
        }

        public User GetUser(string userName)
        {
            return ServiceHelper.RunInContext(() => _ticketService.Users.GetUser(userName));
        }

        public IEnumerable<User> GetAllUsers()
        {
            return ServiceHelper.RunInContext(() => _ticketService.Users.GetAllUsers());
        }

        public IEnumerable<Cluster> GetAllClusters()
        {
            return ServiceHelper.RunInContext(() => _ticketService.Parameters.GetAllClusters());
        }

        public bool CheckUserName(long id, string name)
        {
            if (!AuthContext.IsAdmin) return false;
            return !ServiceHelper.RunInContext(() => _ticketService.Users.GetAllUsers().Where(u => u.Name == name).ToList().Any());
        }

        public IEnumerable<SkillSetStat> GetSkillSetStat()
        {
            if (AuthContext.IsAdmin)
            {
                return ServiceHelper.RunInContext(() =>
                            Cache.ExecuteOrGetCache(() => _ticketService.SkillSets.GetStatisticsForSkillSets(),
                            "SkillSetStat",
                            TimeSpan.FromSeconds(Data.Properties.Settings.Default.SkillSetCacheExpiration)));
            }
            if (AuthContext.IsSuperviser)
            {
                var userSkillSets = ServiceHelper.RunInContext(() => _ticketService.SkillSets.GetUserSkillSets());
                var skillSetStats = ServiceHelper.RunInContext(() =>
                            Cache.ExecuteOrGetCache(() => _ticketService.SkillSets.GetStatisticsForSkillSets(),
                            "SkillSetStat",
                            TimeSpan.FromSeconds(Data.Properties.Settings.Default.SkillSetCacheExpiration)));
                return skillSetStats.Where(ss => userSkillSets.Any(uss => uss.Name == ss.Name)).ToArray();
            }
            return new Collection<SkillSetStat>();
        }

        public IEnumerable<UserStat> GetUsersStat()
        {
            if (AuthContext.IsAdmin)
            {
                return
                    ServiceHelper.RunInContext(() => Cache.ExecuteOrGetCache(() =>
                                TransactionHelper.ExecuteWithReadUncommited(() => _ticketService.Users.GetStatisticsForUsers()), 
                                "UserStat",
                                TimeSpan.FromSeconds(Data.Properties.Settings.Default.SkillSetCacheExpiration)));
            }
            if (AuthContext.IsSuperviser)
            {
                var userSkillSets =
                    ServiceHelper.RunInContext(() => TransactionHelper.ExecuteWithReadUncommited(() => _ticketService.SkillSets.GetUserSkillSets()));

                var statisticsForUsers = ServiceHelper.RunInContext(() => Cache.ExecuteOrGetCache(
                            () => TransactionHelper.ExecuteWithReadUncommited(() => _ticketService.Users.GetStatisticsForUsers()), 
                            "UserStat",
                            TimeSpan.FromSeconds(Data.Properties.Settings.Default.SkillSetCacheExpiration)));
                return statisticsForUsers
                    .Where(ss => userSkillSets.Any(uss => uss.Name == ss.SkillSetName))
                    .ToArray();
            }
            return new Collection<UserStat>();
        }

        public IEnumerable<ParameterRealTimeStat> GetParamsStat()
        {
            if (AuthContext.IsAdmin)
            {
                return ServiceHelper.RunInContext(() => Cache.ExecuteOrGetCache(()=>_ticketService.Parameters.GetStatisticsForParameters(),
                    "ParamsStat", TimeSpan.FromSeconds(Data.Properties.Settings.Default.SkillSetCacheExpiration)));
            }
            if (AuthContext.IsSuperviser)
            {
                var superviser = ServiceHelper.RunInContext(() => _ticketService.Users.GetUser(HttpContext.Current.User.Identity.Name));
                var parameterRealTimeStats = ServiceHelper.RunInContext(() => Cache.ExecuteOrGetCache(() => _ticketService.Parameters.GetStatisticsForParameters(),
                    "ParamsStat", TimeSpan.FromSeconds(Data.Properties.Settings.Default.SkillSetCacheExpiration)));
                return parameterRealTimeStats.Where(ss => superviser.Parameters.Any(p => p.ParameterName == ss.ParameterName)).ToArray();
            }
            return new Collection<ParameterRealTimeStat>();
        }

        public void CreateCluster(Cluster cluster)
        {
            ServiceHelper.RunInContext(() => _ticketService.Parameters.CreateCluster(cluster));
        }

        public void UpdateCluster(string name, ICollection<string> newParameters)
        {
            ServiceHelper.RunInContext(() => _ticketService.Parameters.UpdateCluster(name, newParameters));
        }

        public void DeleteCluster(Cluster cluster)
        {
            ServiceHelper.RunInContext(() => _ticketService.Parameters.DeleteCluster(cluster.Name));
        }

       
    }
}
