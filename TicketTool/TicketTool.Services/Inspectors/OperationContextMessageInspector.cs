﻿using System.ServiceModel;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Channels;
using TicketTool.Data.Context;

namespace TicketTool.Services.Inspectors
{
    public class OperationContextMessageInspector : IDispatchMessageInspector
    {
        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            OperationContext.Current.Extensions.Add(
                new OperationContextHolder<TicketToolContext>
                    {
                        Extension = {Credentials = AuthContext.GetUserCredential()}
                    });
            return request.Headers.MessageId;
        }

        public void BeforeSendReply(ref Message reply, object correlationState)
        {
            OperationContext.Current.Extensions.Remove(OperationContextHolder<TicketToolContext>.GetExtension());
        }
    }
}