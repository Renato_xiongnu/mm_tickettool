﻿using System.Web;
using System.Web.Security;
using TicketTool.Data.Context;

namespace TicketTool.Services
{
    public static class AuthContext
    {
        public static bool IsAuthenticated
        {
            get { return HttpContext.Current.User.Identity.IsAuthenticated; }
        }

        public static bool IsAdmin
        {
            get { return Roles.IsUserInRole("Admin"); }
        }

        public static bool IsSuperviser
        {
            get { return Roles.IsUserInRole("Superviser"); }
        }

        public static UserCredential GetUserCredential()
        {
            return new UserCredential {Name = HttpContext.Current.User.Identity.Name, Roles = Roles.GetAllRoles()};
        }
    }
}