﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using TicketTool.Data;
using TicketTool.Data.Contracts;
using TicketTool.Data.Contracts.Common;
using TicketTool.Data.Contracts.Operations;
using TicketTool.Data.Contracts.Tasks;
using TicketTool.Services.Behaviors;

namespace TicketTool.Services
{
    [OperationContextBehavior]
    public class TicketToolService : TtServiceBase,ITicketToolService
    {
        private const string AccessDeniedText = "Method call is denied for anonymous user";

        private static IUnityContainer _container=new UnityContainer();
// ReSharper disable UnusedAutoPropertyAccessor.Local
        private ITicketService TicketService
        {
            get { return _container.Resolve<ITicketService>(); }
        }

// ReSharper restore UnusedAutoPropertyAccessor.Local

        static TicketToolService()
        {
            _container.LoadConfiguration();
        }

        public string CreateTicket(string externalSystemId, string workItemId, string parameter)
        {
            return ServiceHelper.RunInContext(() => TicketService.Tickets.CreateTicket(externalSystemId, workItemId, parameter));
        }

        public string GetTicket(string workItemId)
        {
            return ServiceHelper.RunInContext(() => TicketService.Tickets.GetTicket(workItemId));
        }

        public void CloseTicket(string ticketId)
        {
            ServiceHelper.RunInContext(() => TicketService.Tickets.CloseTicket(ticketId));
        }

        public ICollection<TaskListItem> GetTasksByParameter(Parameter parameter)
        {
            return ServiceHelper.RunInContext(() => TicketService.Tasks.GetMyTasks(parameter));
        }

        public ICollection<TaskListItem> GetTasks(SkillSet skillSet, Parameter parameter)
        {
            if (AuthContext.IsAdmin || AuthContext.IsSuperviser)
            {
                return ServiceHelper.RunInContext(() => TicketService.Tasks.GetTasks(skillSet, parameter));
            }
            return new Collection<TaskListItem>();
        }

        public TicketTask GetTask(string taskId)
        {
            return ServiceHelper.RunInContext(() => TicketService.Tasks.GetTask(taskId));
        }

        public AssignTaskForMeResult AssignTaskForMe(string taskId)
        {            
            var result = new AssignTaskForMeResult();
            if(!AuthContext.IsAuthenticated)
            {
                result.HasError = true;
                result.MessageText = AccessDeniedText;
                return result;
            }
            try
            {
                result.Result = ServiceHelper.RunInContext(() => TicketService.Tasks.AssignFreeTaskForMe(taskId));
            }
            catch(Exception ex)
            {
                Logger.Error("AssignTaskForMe", ex);
                result.HasError = true;
                result.MessageText = ex.Message;
            }
            return result;
        }

        /// <summary>
        /// Forces the assign task for me.
        /// </summary>
        /// <param name="taskId">The task identifier.</param>
        /// <returns></returns>
        public AssignTaskForMeResult ForceAssignTaskForMe(string taskId)
        {
            var result = new AssignTaskForMeResult();
            if (!AuthContext.IsAuthenticated)
            {
                result.HasError = true;
                result.MessageText = AccessDeniedText;
                return result;
            }
            try
            {
                result.Result = ServiceHelper.RunInContext(() => TicketService.Tasks.ForceAssignFreeTaskForMe(taskId));
            }
            catch (Exception ex)
            {
                Logger.Error("ForceAssignTaskForMe", ex);
                result.HasError = true;
                result.MessageText = ex.Message;
            }
            return result;
        }

        public IEnumerable<TicketTask> GetTasksByWorkitemId(string workitemId)
        {            
            return ServiceHelper.RunInContext(() => TicketService.Tasks.GetTasks(workitemId)).ToArray();
        }

        public string CreateTask(TicketTask task)
        {
            return ServiceHelper.RunInContext(() => TicketService.Tasks.CreateTask(task));
        }

        public TicketTask GetNextTask()
        {
            return ServiceHelper.RunInContext(() => TicketService.Tasks.ReserveAndGetNextTask(false));
        }
        
        public TicketTask GetNextTaskSkipAssigned()
        {
            return ServiceHelper.RunInContext(() => TicketService.Tasks.ReserveAndGetNextTask(true));
        }

        public OperationResult CompleteTask(string taskId, string outcome, ICollection<RequiredField> requiredFields)
        {
            OperationResult result;
            if (AuthContext.IsAuthenticated)
            {
                result = ServiceHelper.RunInContext(() => TicketService.Tasks.CompleteTask(taskId, outcome, requiredFields));
            }
            else
            {
                result = new OperationResult
                {
                    HasError = true,
                    MessageText = AccessDeniedText
                };
            }

            return result;
        }

        public OperationResult PostponeTask(string taskId, TimeSpan? time)
        {
            OperationResult result;

            if (AuthContext.IsAuthenticated)
            {
                result = ServiceHelper.RunInContext(() => TicketService.Tasks.PostponeTask(taskId, time));
            }
            else
            {
                result = new OperationResult
                {
                    HasError = true,
                    MessageText = AccessDeniedText
                };
            }

            return result;
        }

        public void CancelTask(string taskId)
        {
            long id;
            if (long.TryParse(taskId, out id))
            {
                ServiceHelper.RunInContext(() => TicketService.Tasks.CancelTask(id));
            }
        }

        public void LogInToAvailableSkillSets()
        {
            if(!AuthContext.IsAuthenticated)
            {
                return;
            }
            var skillSets = ServiceHelper.RunInContext(() => TicketService.SkillSets.GetUserSkillSets())
                .Where(s=>!s.IsLogged)
                .Select(s=>new SkillSet{Name = s.Name})
                .ToArray();
            ServiceHelper.RunInContext(() => TicketService.Users.AddUserToSkillSetsIfSkillSetsNotExist(skillSets));
        }

        public void LogInToSkillSets(ICollection<SkillSet> skillSets)
        {
            if (AuthContext.IsAuthenticated)
            {
                ServiceHelper.RunInContext(() => TicketService.Users.AddUserToSkillSetsIfSkillSetsNotExist(skillSets));
            }
        }

        public void LogOutFromSkillSets()
        {
            if (AuthContext.IsAuthenticated)
            {
                ServiceHelper.RunInContext(() => TicketService.Users.RemoveUserWithTasksFromSkillSets());
            }
        }

        public void LogOutAndLogInToSkillSets(ICollection<SkillSet> skillSets, ICollection<Cluster> clusters = null)
        {
            if (!AuthContext.IsAuthenticated) return;
            ServiceHelper.RunInContext(() => TicketService.Users.RemoveUserWithTasksFromSkillSets());
            ServiceHelper.RunInContext(
                () => TicketService.Users.AddUserToSkillSetsIfSkillSetsNotExist(skillSets, clusters));
        }

        public User GetCurrentPerformer()
        {
            return AuthContext.IsAuthenticated ? ServiceHelper.RunInContext(() => TicketService.Users.GetUser(HttpContext.Current.User.Identity.Name)) : null;
        }

        public PingByUserResult Ping()
        {
            return AuthContext.IsAuthenticated ? ServiceHelper.RunInContext(() => TicketService.Users.PingByUser()) : new PingByUserResult();
        }

        public ICollection<TaskListItem> GetMyTasksInProgress(string[] parameters, string[] skillSets)
        {
            if (!AuthContext.IsAuthenticated)
            {
                return new Collection<TaskListItem>();
            }
            if (AuthContext.IsAdmin || AuthContext.IsSuperviser)
            {
                return ServiceHelper.RunInContext(() => TicketService.Tasks.GetAllTasksInProgress(parameters, skillSets));
            }            
            return ServiceHelper.RunInContext(() => TicketService.Tasks.GetMyTasksInProgress());
        }

        public ICollection<Parameter> GetMyParameters()
        {
            if (!AuthContext.IsAuthenticated)
            {
                return new Collection<Parameter>();
            }
            if (AuthContext.IsAdmin || AuthContext.IsSuperviser)
            {
                return ServiceHelper.RunInContext(() => TicketService.Parameters.GetAllParameters());
            }
            return ServiceHelper.RunInContext(() => TicketService.Users.GetUser(HttpContext.Current.User.Identity.Name)).Parameters;            
        }

        public TicketTO GetTicketInfo(string ticketId)
        {
            return ServiceHelper.RunInContext(() => TicketService.Tickets.GetTicketInfo(ticketId));
        }
    }
}
