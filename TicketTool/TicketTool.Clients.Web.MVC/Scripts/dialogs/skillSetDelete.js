﻿$(function () {
    makeDialog("#skillSetDeleteDialog", {
        title: 'Удаление',
        width: 450,
        open: function () {
            $(this).dialog('option', 'title', 'Удаление скиллсета "' + $(this).data('skillName') + '"');
        },
        buttons: {
            "OK": function () {
                $.ajax({
                    type: "get",
                    url: $.routeLinks.DeleteSkillSet,
                    data: { skillSetName: $(this).data('skillName') },
                    success: function (response) {
                        if (response === true) {
                            reloadTaskTypes();
                            reloadSkillSets();
                        }
                        else {
                            alert("не удалось удалить скиллсет");
                        }
                    }
                });
                $(this).dialog("close");
            },
            Cancel: function () {
                $(this).dialog("close");
            }
        }
    });
    $(".loginUserLink").click(function () {
        $("#loginUserDialog").dialog("open");
    });
});