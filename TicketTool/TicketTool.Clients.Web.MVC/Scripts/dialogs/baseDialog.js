﻿function makeDialog(container, options) {
    var baseOptions = {
        autoOpen: false,
        width: 450,
        title: "Default",
        modal: true,
        close: function () {
            cleanInputs($(this));
        }
    };
    var buttons = {
        "OK": function () {
            $(this).dialog("close");
        }
    }
    $.extend(baseOptions, options);
    $(container).dialog(baseOptions);
}
