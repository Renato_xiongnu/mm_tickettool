﻿$(function () {
    makeDialog("#taskFormDialog", {
        title: "Редактирование типа задачи",
        width: 450,
        open: function () {
            $('input#taskName', this).attr('readonly','readonly').val($(this).data('taskName'));
            $('input#skillsetTime', this).val($(this).data('skillsetTime'));
            $('input#userTime', this).val($(this).data('userTime'));
        },
        buttons: {
            "OK": function () {
                var serialized = serializeFormDisabled($(this));
                $.ajax({
                    type: "get",
                    url: $.routeLinks.EditTaskType,
                    data: serialized + '&processName=' + getCurrentProcessName(),
                    success: function (response) {
                        if (response === true) {
                            reloadTaskTypes();
                            reloadSkillSets();
                        }
                        else {
                            alert("не удалось отредактировать задачу");
                        }
                    }
                });
                $(this).dialog("close");
            },
            Cancel: function () {
                $(this).dialog("close");
            }
        }
    });
});