﻿$(function () {
    makeDialog("#postponeTaskDialog", {
        title: "Отложить",
        width: 450,
        open: function () {
            $('.spinner', this).val('1');
        },
        buttons: {
            "OK": function () {
                //var serialized = serializeFormDisabled($(this));
                $.ajax({
                    type: "get",
                    url: $.routeLinks.SimplePostponeTask,
                    data: { taskId: $('[id$="TaskId"]').val(), minutes: $('.spinner', this).val() },
                    success: function (response) {
                        if (response !== null && response !== '' && response.isPostponed) {
                            $('#reloadButton').click();
                        }
                    }
                });
                $(this).dialog("close");
            },
            Cancel: function () {
                $(this).dialog("close");
            }
        }
    });
    $('.postponeTaskLink').click(function (e) {
        e.preventDefault();
        $("#postponeTaskDialog").dialog("open");
    });
});