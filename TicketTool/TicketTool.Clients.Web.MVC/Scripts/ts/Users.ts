﻿/// <reference path="d/_references.d.ts" />
var d = React.DOM;
var $ = jQuery;

function getAllUsers() {
    return $.ajax({
        url: '/TicketTool.Web/Users/GetAllUsers',
        dataType: 'json',
        data: {}
    });
}

var UsersStore = new Rx.BehaviorSubject([]);

var UsersActions = {
    getAllUsers: new Rx.Subject()
};

UsersActions.getAllUsers
    .flatMapLatest(() => Rx.Observable.fromPromise(getAllUsers()))
    .subscribe(UsersStore);

class MaterialDataTable extends React.Component<any, any> {
    classList: Array<string>;
    Constant = {
        // None at the moment.
    };
    CssClasses = {
        DATA_TABLE: 'mdl-data-table',
        JS_DATA_TABLE: 'mdl-js-data-table',
        SELECTABLE: 'mdl-data-table--selectable',
        IS_SELECTED: 'is-selected',
        IS_UPGRADED: 'is-upgraded',
        SHADOW: 'mdl-shadow--2dp'
    };
    _selectRow(e, rowIndex) {
        
    };
    componentWillMount() {
        this.classList = this.props.classList || [this.CssClasses.DATA_TABLE, this.CssClasses.SELECTABLE, this.CssClasses.SHADOW, this.CssClasses.IS_UPGRADED];
    };
    render() {
        var CheckBox = React.createFactory(MaterialCheckBox);

        var header = this.props.header || [];
        var children = this.props.children || [];
        if (this.classList.indexOf(this.CssClasses.SELECTABLE) > -1) {
            var checkBoxTd = d.td({}, CheckBox({ id: "checkbox_header", onChange: this._selectRow }))
            header.unshift(checkBoxTd);

            children = React.Children.map(this.props.children, (child: React.ReactElement<any>) => {
                return React.cloneElement(child, {
                    selectable: true,
                    selectRow: this._selectRow
                });
            });
        }
        return (
            d.table({ ref: "table", className: this.classList.join(" ") },
                d.thead({}, header),
                d.tbody({}, children)                
            )
        );
    };
};

class MaterialButton extends React.Component<any, any> {
    render() {
        return d.button({ className: "mdl-button mdl-js-button mdl-button--accent", disabled: this.props.disabled || false, onClick: this.props.onClick },
            this.props.label
        );
    }
};

class MaterialCheckBox extends React.Component<any, any> {
    render() {
        return d.label({ className: "mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect", htmlFor: this.props.id },
            d.input({ type: "checkbox", id: this.props.id, className: "mdl-checkbox__input", checked: this.props.checked, disabled: this.props.disabled, onChange: this.props.onChange }),
            d.span({ className: "mdl-checkbox__label" }, this.props.label)
        );
    }
};

class UsersTableRow extends React.Component<any, any> {
    _editClick: Function;
    _selectRow(e) {
        this.props.selectRow(e, this.props.index);
    };
    componentWillMount() {
        var user = this.props;
        this._editClick = function (e) {            
            $("#userFormDialog").data('userName', user.Name).dialog("open");
        }
    }
    render() {
        var CheckBox = React.createFactory(MaterialCheckBox);
        var Button = React.createFactory(MaterialButton);

        var user = this.props.data;
        return d.tr({},
            this.props.selectable ? d.td({}, CheckBox({ id: "checkbox_" + user.Id, onChange: this._selectRow.bind(this), checked: this.props.selected })) : null,
            d.td({ className: "" }, CheckBox({ id: "IsLocked" + user.Id, checked: user.IsLocked, disabled: true })),
            d.td({ className: "mdl-data-table__cell--non-numeric" }, user.Name),
            d.td({ className: "mdl-data-table__cell--non-numeric" }, user.Roles.join(", ")),
            d.td({ className: "mdl-data-table__cell--non-numeric" }, user.StoresString),
            d.td({ className: "mdl-data-table__cell--non-numeric" }, user.Email),
            d.td({ className: "", style: { verticalAlign: "middle", paddingTop: 0 } },
                Button({ label: "Редактировать", onClick: this._editClick })
            )
        );
    }

};

class UsersTable extends React.Component<any, any> {
    render() {
        var Table = React.createFactory(MaterialDataTable);
        var Row = React.createFactory(UsersTableRow);
        var rows = this.props.data.map((user, index) => Row({ key: user.Id, data: user, index: index }));

        var header = [
            d.th({ className: "" }, "Заблокирован"),
            d.th({ className: "mdl-data-table__cell--non-numeric" }, "Имя"),
            d.th({ className: "mdl-data-table__cell--non-numeric" }, "Роль"),
            d.th({ className: "mdl-data-table__cell--non-numeric" }, "Магазин"),
            d.th({ className: "mdl-data-table__cell--non-numeric" }, "Email"),
            d.th({ className: "" }, "")
        ];

        return (
            Table({ header: header }, rows)
        );
    }
};


class UsersApp extends React.Component<any, any> {
    _addUserClick: Function;
    componentWillMount() {
        var user = this.props;
        this._addUserClick = function (e) {
            $("#userFormDialog").data("userName", "").dialog("open");
        }
    }
    render() {
        var Table = React.createFactory(UsersTable);
        var Button = React.createFactory(MaterialButton);

        return (
            d.div({ className: "container" },
                d.h3({ className: "" }, "Пользователи"),
                Button({ label: "Создать", onClick: this._addUserClick }),
                d.div({ className: "" }, Table(this.props))
            )
        );
    }
};

// Init
UsersActions.getAllUsers.onNext({});

var App = React.createFactory(UsersApp);

UsersStore.subscribe(function (value) {
    React.render(App({ data: value }), document.getElementById('app'));
}, function (error) {
        console.log(error);
    });
