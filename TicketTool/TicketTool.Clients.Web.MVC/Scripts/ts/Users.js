var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
/// <reference path="d/_references.d.ts" />
var d = React.DOM;
var $ = jQuery;
function getAllUsers() {
    return $.ajax({
        url: '/TicketTool.Web/Users/GetAllUsers',
        dataType: 'json',
        data: {}
    });
}
var UsersStore = new Rx.BehaviorSubject([]);
var UsersActions = {
    getAllUsers: new Rx.Subject()
};
UsersActions.getAllUsers
    .flatMapLatest(function () { return Rx.Observable.fromPromise(getAllUsers()); })
    .subscribe(UsersStore);
var MaterialDataTable = (function (_super) {
    __extends(MaterialDataTable, _super);
    function MaterialDataTable() {
        _super.apply(this, arguments);
        this.Constant = {};
        this.CssClasses = {
            DATA_TABLE: 'mdl-data-table',
            JS_DATA_TABLE: 'mdl-js-data-table',
            SELECTABLE: 'mdl-data-table--selectable',
            IS_SELECTED: 'is-selected',
            IS_UPGRADED: 'is-upgraded',
            SHADOW: 'mdl-shadow--2dp'
        };
    }
    MaterialDataTable.prototype._selectRow = function (e, rowIndex) {
    };
    ;
    MaterialDataTable.prototype.componentWillMount = function () {
        this.classList = this.props.classList || [this.CssClasses.DATA_TABLE, this.CssClasses.SELECTABLE, this.CssClasses.SHADOW, this.CssClasses.IS_UPGRADED];
    };
    ;
    MaterialDataTable.prototype.render = function () {
        var _this = this;
        var CheckBox = React.createFactory(MaterialCheckBox);
        var header = this.props.header || [];
        var children = this.props.children || [];
        if (this.classList.indexOf(this.CssClasses.SELECTABLE) > -1) {
            var checkBoxTd = d.td({}, CheckBox({ id: "checkbox_header", onChange: this._selectRow }));
            header.unshift(checkBoxTd);
            children = React.Children.map(this.props.children, function (child) {
                return React.cloneElement(child, {
                    selectable: true,
                    selectRow: _this._selectRow
                });
            });
        }
        return (d.table({ ref: "table", className: this.classList.join(" ") }, d.thead({}, header), d.tbody({}, children)));
    };
    ;
    return MaterialDataTable;
})(React.Component);
;
var MaterialButton = (function (_super) {
    __extends(MaterialButton, _super);
    function MaterialButton() {
        _super.apply(this, arguments);
    }
    MaterialButton.prototype.render = function () {
        return d.button({ className: "mdl-button mdl-js-button mdl-button--accent", disabled: this.props.disabled || false, onClick: this.props.onClick }, this.props.label);
    };
    return MaterialButton;
})(React.Component);
;
var MaterialCheckBox = (function (_super) {
    __extends(MaterialCheckBox, _super);
    function MaterialCheckBox() {
        _super.apply(this, arguments);
    }
    MaterialCheckBox.prototype.render = function () {
        return d.label({ className: "mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect", htmlFor: this.props.id }, d.input({ type: "checkbox", id: this.props.id, className: "mdl-checkbox__input", checked: this.props.checked, disabled: this.props.disabled, onChange: this.props.onChange }), d.span({ className: "mdl-checkbox__label" }, this.props.label));
    };
    return MaterialCheckBox;
})(React.Component);
;
var UsersTableRow = (function (_super) {
    __extends(UsersTableRow, _super);
    function UsersTableRow() {
        _super.apply(this, arguments);
    }
    UsersTableRow.prototype._selectRow = function (e) {
        this.props.selectRow(e, this.props.index);
    };
    ;
    UsersTableRow.prototype.componentWillMount = function () {
        var user = this.props;
        this._editClick = function (e) {
            $("#userFormDialog").data('userName', user.Name).dialog("open");
        };
    };
    UsersTableRow.prototype.render = function () {
        var CheckBox = React.createFactory(MaterialCheckBox);
        var Button = React.createFactory(MaterialButton);
        var user = this.props.data;
        return d.tr({}, this.props.selectable ? d.td({}, CheckBox({ id: "checkbox_" + user.Id, onChange: this._selectRow.bind(this), checked: this.props.selected })) : null, d.td({ className: "" }, CheckBox({ id: "IsLocked" + user.Id, checked: user.IsLocked, disabled: true })), d.td({ className: "mdl-data-table__cell--non-numeric" }, user.Name), d.td({ className: "mdl-data-table__cell--non-numeric" }, user.Roles.join(", ")), d.td({ className: "mdl-data-table__cell--non-numeric" }, user.StoresString), d.td({ className: "mdl-data-table__cell--non-numeric" }, user.Email), d.td({ className: "", style: { verticalAlign: "middle", paddingTop: 0 } }, Button({ label: "Редактировать", onClick: this._editClick })));
    };
    return UsersTableRow;
})(React.Component);
;
var UsersTable = (function (_super) {
    __extends(UsersTable, _super);
    function UsersTable() {
        _super.apply(this, arguments);
    }
    UsersTable.prototype.render = function () {
        var Table = React.createFactory(MaterialDataTable);
        var Row = React.createFactory(UsersTableRow);
        var rows = this.props.data.map(function (user, index) { return Row({ key: user.Id, data: user, index: index }); });
        var header = [
            d.th({ className: "" }, "Заблокирован"),
            d.th({ className: "mdl-data-table__cell--non-numeric" }, "Имя"),
            d.th({ className: "mdl-data-table__cell--non-numeric" }, "Роль"),
            d.th({ className: "mdl-data-table__cell--non-numeric" }, "Магазин"),
            d.th({ className: "mdl-data-table__cell--non-numeric" }, "Email"),
            d.th({ className: "" }, "")
        ];
        return (Table({ header: header }, rows));
    };
    return UsersTable;
})(React.Component);
;
var UsersApp = (function (_super) {
    __extends(UsersApp, _super);
    function UsersApp() {
        _super.apply(this, arguments);
    }
    UsersApp.prototype.componentWillMount = function () {
        var user = this.props;
        this._addUserClick = function (e) {
            $("#userFormDialog").data("userName", "").dialog("open");
        };
    };
    UsersApp.prototype.render = function () {
        var Table = React.createFactory(UsersTable);
        var Button = React.createFactory(MaterialButton);
        return (d.div({ className: "container" }, d.h3({ className: "" }, "Пользователи"), Button({ label: "Создать", onClick: this._addUserClick }), d.div({ className: "" }, Table(this.props))));
    };
    return UsersApp;
})(React.Component);
;
// Init
UsersActions.getAllUsers.onNext({});
var App = React.createFactory(UsersApp);
UsersStore.subscribe(function (value) {
    React.render(App({ data: value }), document.getElementById('app'));
}, function (error) {
    console.log(error);
});
//# sourceMappingURL=Users.js.map