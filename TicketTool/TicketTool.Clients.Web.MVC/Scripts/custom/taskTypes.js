﻿var taskTypeIndex = 0;


$(function () {
    $("#processTypesList").on('change', function () {
        reloadTaskTypes();
    });
    refreshTaskTypesEvents();
    refreshSkillSetsEvents();
    setHighlight();
    refreshRelation();
    setRelation($('div.baseSkillSets table tbody tr'), false);
    setRelation($('div.escalatedSkillSets table tbody tr'), true);
    $('div.taskTypesContainer table tbody tr').eq(taskTypeIndex).click();
});

function getCurrentProcessName() {
    return $('#processTypesList').val();
}

function reloadTaskTypes() {
    $.ajax({
        type: "get",
        url: $.routeLinks.TaskTypes,
        data: { processName: getCurrentProcessName() },
        beforeSend: function () {
            $('.taskTypesContainer').html(generateLoader($('.taskTypesContainer')));
        },
        success: function (response) {
            $('.taskTypesContainer').html(response);
            refreshTaskTypesEvents();
            refreshRelation();
            setHighlight();
            $('div.taskTypesContainer table tbody tr').eq(taskTypeIndex).click();
        }
    });
}

function reloadSkillSets() {
    refreshControl($.routeLinks.BaseSkillSets, 'baseSkillSets', false, true, function () {
        refreshSkillSetsEvents();
        setRelation($('div.baseSkillSets table tbody tr'), false);
        setHighlight();
        $('div.taskTypesContainer table tbody tr').eq(taskTypeIndex).click();
    });
    refreshControl($.routeLinks.EscSkillSets, 'escalatedSkillSets', false, true, function () {
        refreshSkillSetsEvents();
        setRelation($('div.escalatedSkillSets table tbody tr'), true);
        setHighlight();
        $('div.taskTypesContainer table tbody tr').eq(taskTypeIndex).click();
    });
}

function refreshRelation() {
    var $specialTrs = $('div.taskTypesContainer table tbody tr');
    $specialTrs.on('click', function () {
        taskTypeIndex = $(this).index();
        selectSkillSet($('div.baseSkillSets table tbody tr'), $(this).find('.baseSkillRelation').html());
        selectSkillSet($('div.escalatedSkillSets table tbody tr'), $(this).find('.escSkillRelation').html());
    });

}

function refreshTaskTypesEvents() {
    $('.taskEditLink').click(function () {
        $("#taskFormDialog")
            .data('taskName', $(this).attr('data-taskName'))
            .data('skillsetTime', $(this).attr('data-skillsetTime'))
            .data('userTime', $(this).attr('data-userTime'))
            .dialog("open");
    });
}

function refreshSkillSetsEvents() {
    $('.skillEditLink').click(function () {
        $("#skillSetFormDialog")
            .data('type', 'edit')
            .data('name', $(this).attr('data-name'))
            .data('roles', $(this).attr('data-roles'))
            .data('param', $(this).attr('data-param'))
            .data('isEsc', $(this).attr('data-isEsc'))
            .data('isMail', $(this).attr('data-isMail'))
            .dialog("open");
    });
    $('.skillDeleteLink').click(function () {
        $("#skillSetDeleteDialog").data('skillName', $(this).attr('data-name')).dialog("open");
    });
}

function selectSkillSet(container, sName) {
    $(container).each(function () {
        var name = $(this).find('.skillName').html();
        var checkedRadio = $(this).find(':radio');
        if (name === sName) {
            checkedRadio.prop('checked', true);
        }
    })
}

function setRelation(container, isEscalated) {
    $(container).each(function () {
        var name = $(this).find('.skillName').html();
        $(this).find(':radio').on('click', function () {
            var $tds = $('div.taskTypesContainer table tbody').find('.shadedSelected, .selected').children();
            var previousName = $tds.filter(isEscalated ? '.escSkillRelation' : '.baseSkillRelation').html();
            var $taskType = $tds.filter('.taskName').html();
            if (previousName !== name) {
                $.ajax({
                    type: "get",
                    url: $.routeLinks.UpdateSkillSetRelation,
                    data: { processName: getCurrentProcessName(), skillSetName: name, taskTypeName: $taskType },
                    success: function (response) {
                        reloadTaskTypes();
                        reloadSkillSets();
                    }
                });
            }
        });
    });
}

function setHighlight() {
    var $tables = $('table:not(.notHighlight)');
    var $globalTrs = $('table:not(.notHighlight) tbody tr');
    $tables.each(function () {
        var $trs = $(this).find('tbody tr');
        $trs.on('mouseout', function () {
            $(this).removeClass('hover');
        });
        $trs.on('mouseover', function () {
            $(this).addClass('hover');
        });
        $trs.on('click', function () {
            $globalTrs.each(function () {
                if ($(this).hasClass('selected')) {
                    $(this).removeClass('selected');
                    $(this).addClass('shadedSelected');
                }
            });
            $trs.removeClass('shadedSelected').removeClass('selected');
            $(this).addClass('selected');
        });
    });

}
