﻿var TtIntegrations =
{
    registerTaskHandlerOnce: function (cb,validated) {
        //return false;
        if (this.callback) return;
        this.callback = cb;
        var validatedOutcomes = typeof validated !== 'undefined' ? validated : ['Подтверждено'];

        var callback = this.callback;
        var callbackCount = 0; //фикс пролблемы двух сабмитов - нормального и левого
        var result = false;
        var isSecondCallback = false;
        window.parent.$('form').unbind('submit.TtIntegrations');
        window.parent.$('form').bind('submit.TtIntegrations', function (e) {
            var formUrl = $(this).attr('action');
            var selector = 'input[data-href="' + formUrl + '"]';
            var outcomeButton = window.parent.$(selector);
            var outcome = '';
            if (outcomeButton.length > 0) {
                outcome = outcomeButton.parent().siblings('input[type="hidden"]');
            }
            if (validatedOutcomes.indexOf(outcome.val())==-1) return true;
            callbackCount++;
            if (callbackCount > 1 && result) {
                window.parent.$('form').unbind('submit.TtIntegrations');
            }
            if (!isSecondCallback)
            {
                result = callback();
                isSecondCallback = true;
            } else {
                isSecondCallback = false;
            }
            if (!result) {
                e.preventDefault();
                e.stopPropagation();
                window.parent.$('.outcomeSubmit').removeAttr('disabled');
                return result;
            }

            return result;
        });
    }
};