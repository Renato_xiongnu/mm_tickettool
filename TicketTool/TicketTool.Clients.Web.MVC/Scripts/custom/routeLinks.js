﻿jQuery.routeLinks || (function ($, window, undefined) {
    $.routeLinks = {
        UpdateSkillSetRelation: approot + '/TaskTypes/UpdateRelation',
        EditTaskType: approot + '/TaskTypes/EditTask',
        UpdateSkillSet: approot + '/TaskTypes/UpdateSkillSet',
        DeleteSkillSet: approot + '/TaskTypes/DeleteSkillSet',
        TaskTypes: approot + '/TaskTypes/TaskTypesGrid',
        BaseSkillSets: approot + '/TaskTypes/BaseSkillSetsGrid',
        EscSkillSets: approot + '/TaskTypes/EscalatedSkillSetsGrid',
        CreateUser: approot + '/Users/CreateUser',
        UpdateUser: approot + '/Users/UpdateUser',
        CheckUser: approot + '/Users/CheckUser',
        CreateUserDialog: approot + '/Users/DialogCreate',
        UpdateUserDialog: approot + '/Users/DialogEdit',
        AvailableSkillSets: approot + '/Users/AvailableSkillSetsGrid',
        Users: approot + '/Users/UsersGrid',
        SimplePulse: approot + '/Simple/Pulse',
        SimpleCheckTask: approot + '/Simple/CheckTask',
        SimplePostponeTask: approot + '/Simple/PostponeTask'
    };
})(jQuery, this);