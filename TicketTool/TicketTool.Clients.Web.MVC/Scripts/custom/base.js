﻿function generateLoader(container) {
    var height = $(container).height();
    var width = $(container).width()
    if (height === 0) height = 50;
    if (width === 0) width = 50;
    return '<div style="text-align:center;width:' + width + 'px;height:' + height + 'px;"><img src="' + approot + '/Content/Images/ajax-loader.gif"></div>';
}
$(function () {
    $('.submitButton').on('click', function () {
        var href = $(this).attr('data-href');
        if (href !== null && href !== undefined) {
            $(this).closest('form').attr('action', href).submit();
        }
    });
});
function refreshControl(url, containerClass, customSort, withLoader, callbackFunction) {
    var $container = $("." + containerClass);
    var sortHelper;
    if (customSort) {
        var sortElem = $container.find('th[class*="headerSort"]');
        var index;
        var order;
        if (sortElem.size() > 0) {
            var elems = $container.find('th');
            index = elems.index(sortElem);
            var regexp = /sort-(.+?) /ig;
            var fieldDesc = regexp.exec(sortElem.attr('class'));
            if (fieldDesc != null) {
                order = sortElem.hasClass('headerSortDown') ? 'asc' : 'desc';
                sortHelper = fieldDesc[1] + " " + order;
            }
        }
    }
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            if (withLoader) {
                $container.html(generateLoader($($container)));
            }
        },
        data: { customSort: sortHelper },
        success: function (response) {
            $container.html(response);
            if (customSort === true) {
                $container.find(".tablesorter").tablesorter({ sortList: index !== undefined ? [[index, order === 'asc' ? 0 : 1]] : undefined });
            }
            if (callbackFunction !== null && callbackFunction !== undefined) {
                callbackFunction();
            }
        }
    });
}

function cleanInputs(container) {
    if (container !== undefined) {
        $('input[type!=checkbox]', container).removeAttr('disabled').removeAttr('readonly').val('');
        $('input[type=checkbox]', container).removeAttr('disabled').removeAttr('readonly').removeAttr('checked');
        $('select', container).removeAttr('disabled').removeAttr('readonly').val('');
    }
}
function serializeFormDisabled(container) {
    if (container !== undefined) {
        var form = $('form', container);
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var serialized = form.serialize();
        disabled.attr('disabled', 'disabled');
        return serialized;
    }
    return null;
}

function hookSelectedChecks(container, element, separator) {
    var checkInputs = $(container).find('input[type="checkbox"]');
    checkInputs.on('click', function () {
        var result = '';
        checkInputs.each(function () {
            if ($(this).attr('checked') === 'checked') {
                result += $(this).val() + separator;
            }
        });
        $(element).val(result);
    });
}
function selectChecks(container, selectedValues, separator) {
    var arrValues = [];
    var inputs = container.find('input[type="checkbox"]');
    if (selectedValues !== undefined && selectedValues !== null) {
        arrValues = selectedValues.split(separator);
    }
    if (arrValues.length >= 1) {
        inputs.each(function () {
            for (var i = 0; i < arrValues.length; i++) {
                if ($(this).val() == arrValues[i]) {
                    $(this).attr('checked', 'checked');
                    break;
                }
            }
        });
    }
}