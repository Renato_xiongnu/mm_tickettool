﻿function generateLoader(container) {
    var height = $(container).height();
    var width = $(container).width()
    if (height === 0) height = 50;
    if (width === 0) width = 50;
    return '<div style="text-align:center;width:' + width + 'px;height:' + height + 'px;"><img src="Content/Images/ajax-loader.gif"></div>';
}

function refreshControl(url, containerClass, customSort, withLoader, callbackFunction) {
    var $container = $("." + containerClass);
    if (customSort) {
        var sortElem = $container.find('th[class*="headerSort"]');
        var customSort;
        var index;
        var order;
        if (sortElem.size() > 0) {
            var elems = $container.find('th');
            index = elems.index(sortElem);
            var regexp = /sort-(.+?) /ig;
            var fieldDesc = regexp.exec(sortElem.attr('class'));
            if (fieldDesc != null) {
                order = sortElem.hasClass('headerSortDown') ? 'asc' : 'desc';
                customSort = fieldDesc[1] + " " + order;
            }
        }
    }
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            if (withLoader) {
                $container.html(generateLoader($($container)));
            }
        },
        data: { customSort: customSort },
        success: function (response) {
            $container.html(response);
            if (customSort === true) {
                $container.find(".tablesorter").tablesorter({ sortList: index !== undefined ? [[index, order === 'asc' ? 0 : 1]] : undefined });
            }
            if (callbackFunction !== null && callbackFunction !== undefined) {
                callbackFunction();
            }
        }
    });
}

function cleanInputs(container) {
    if (container !== undefined) {
        $('input', container).val('');
    }
}