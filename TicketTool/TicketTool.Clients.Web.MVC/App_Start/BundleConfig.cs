﻿using System.Web.Optimization;

namespace TicketTool.Web.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery.inputmask/jquery.inputmask-{version}.js",
                        "~/Scripts/jquery.validate.js",
                        "~/Scripts/jquery.form.js",
                        "~/Scripts/jquery.tablesorter.js",
                        "~/Scripts/jquery-migrate-{version}.js",
                        "~/Scripts/custom/jquery.validation.fixes.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        "~/Scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.full.js"));


            bundles.Add(new ScriptBundle("~/bundles/dialogs").IncludeDirectory("~/Scripts/dialogs/", "*.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/bootstrap.css",
                        "~/Content/bootstrap-responsive.css",
                        "~/Content/site.css",
                        "~/Content/jquery-ui-1.10.3.full.css",
                        "~/Scripts/mdl/material.css"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                "~/Scripts/react/react-with-addons.js",
                "~/Scripts/rx.lite.js",
                "~/Scripts/mdl/material.js",
                "~/Scripts/ts/Users.js"));
        }
    }
}