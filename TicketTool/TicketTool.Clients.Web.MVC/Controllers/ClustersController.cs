﻿using System.Linq;
using System.Web.Mvc;
using TicketTool.Web.Common;
using TicketTool.Web.Models.Items;
using TicketTool.Web.TicketToolAdminLink;

namespace TicketTool.Web.Controllers
{
    public class ClustersController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            var currentUser = ProxyCaller.GetCurrentUser();
            if (!currentUser.IsAuthtorized)
            {
                HttpContext.Response.Status = "403 Forbidden";
                //the next line is untested - thanks to strider for this line
                HttpContext.Response.StatusCode = 403;
                HttpContext.Response.End();
                return null;
            }

            var client = new TicketToolAdminClient();
            var allClusters = client.GetAllClusters().Select(m => new ClusterItem
            {
                IsSelected = true,
                Name = m.Name,
                Parameters = m.Parameters.ToList()
            });

            return View(allClusters);
        }

        public ActionResult Create()
        {
            var client = new TicketToolAdminClient();
            var cluster = new ClusterItem();
            var allParameters = client.GetAllParameters();
            cluster.AllParameters =
                allParameters.Select(
                    m =>
                        new ParameterItem
                        {
                            Name = m.ParameterName,
                            IsSelected = false,
                            DisplayName = m.Name
                        }).ToList();
            return View(cluster);
        }

        [HttpPost]
        public ActionResult Create(ClusterItem clusterItem)
        {
            var client = new TicketToolAdminClient();
            client.CreateCluster(new Cluster
            {
                Name = clusterItem.Name,
                Parameters =
                    clusterItem.AllParameters != null
                        ? clusterItem.AllParameters.Where(m => m.IsSelected).Select(m => m.Name).ToArray()
                        : new string[0]
            });
            return RedirectToAction("Index");
        }

        public ActionResult Edit(string name)
        {
            var client = new TicketToolAdminClient();
            var allClusters = client.GetAllClusters().Select(m => new ClusterItem
            {
                IsSelected = true,
                Name = m.Name,
                Parameters = m.Parameters.ToList()
            });
            var cluster = allClusters.First(m => m.Name == name);
            var allParameters = client.GetAllParameters();
            cluster.AllParameters =
                allParameters.Select(
                    m =>
                        new ParameterItem
                        {
                            Name = m.ParameterName,
                            IsSelected = cluster.Parameters.Contains(m.ParameterName),
                            DisplayName = m.Name
                        }).ToList();
            return View(cluster);
        }

        [HttpPost]
        public ActionResult Edit(ClusterItem clusterItem)
        {
            var client = new TicketToolAdminClient();
            var selected = clusterItem.AllParameters != null
                ? clusterItem.AllParameters.Where(m => m.IsSelected).Select(m => m.Name).ToArray()
                : new string[0];
            client.UpdateCluster(clusterItem.Name, selected);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Delete(string name)
        {
            var client = new TicketToolAdminClient();
            client.DeleteCluster(new Cluster { Name = name });

            return RedirectToAction("Index");
        }
    }
}
