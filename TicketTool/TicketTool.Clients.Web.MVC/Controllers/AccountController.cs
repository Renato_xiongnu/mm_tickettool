﻿using System;
using System.Web.Mvc;
using TicketTool.Web.Common;

namespace TicketTool.Web.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult Index()
        {
            ViewData["ShowSubmit"] = true;
            ViewData["returnUrl"] = Request.QueryString["ReturnUrl"];
            return View();
        }

        public ActionResult Login(string name, string password, string returnUrl)
        {
            if (ProxyCaller.Login(name, password))
            {
                if (!String.IsNullOrEmpty(returnUrl))
                {
                    return Redirect(returnUrl);
                }
                return Redirect(Request.UrlReferrer.ToString());
            }
            return Redirect(Request.UrlReferrer.ToString());
        }

        public ActionResult Logout()
        {
            ProxyCaller.Logout();
            return Redirect(Request.UrlReferrer.ToString());
        }

        public ActionResult GetHeader()
        {
            return PartialView("AccountControls/LoginHeader");
        }
    }
}
