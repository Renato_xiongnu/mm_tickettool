﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TicketTool.Web.Common;
using TicketTool.Web.Models.Items;

namespace TicketTool.Web.Controllers
{
    public class TaskTypesController : Controller
    {
        //
        // GET: /TaskTypes/

        public ActionResult Index()
        {
            var user = HttpContext.User;
            if (user == null || !user.Identity.IsAuthenticated)
            {
                if (Request.Browser.IsMobileDevice)
                {
                    return RedirectToAction("Index", "Account", new { returnUrl = Request.Url.ToString() });
                }
                return View(Repository.TaskTypesRepository);
            }
            Repository.TaskTypesRepository.Processes = ProxyCaller.GetProcesses();
            if (Repository.TaskTypesRepository.CurrentProcess != null)
                Repository.TaskTypesRepository.TaskTypes = ProxyCaller.GetTaskTypes(Repository.TaskTypesRepository.CurrentProcess.Id).Select(m => m.ToModel()).ToList();
            var skillsets = ProxyCaller.GetSkillSets(true, true);
            Repository.TaskTypesRepository.BaseSkillSets = skillsets.Where(m => !m.IsEscalation).Select(m => m.ToModel()).ToList();
            Repository.TaskTypesRepository.EscalatedSkillSets = skillsets.Where(m => m.IsEscalation).Select(m => m.ToModel()).ToList();
            return View(Repository.TaskTypesRepository);
        }

        [Authorize]
        public ActionResult TaskTypesGrid(string processName)
        {
            Repository.TaskTypesRepository.TaskTypes = ProxyCaller.GetTaskTypes(processName).Select(m => m.ToModel()).ToList();
            return PartialView("TaskTypesControls/TaskTypesGrid", Repository.TaskTypesRepository.TaskTypes);
        }

        [Authorize]
        public ActionResult BaseSkillSetsGrid()
        {
            var skillsets = ProxyCaller.GetSkillSets(true, true);
            Repository.TaskTypesRepository.BaseSkillSets = skillsets.Where(m => !m.IsEscalation).Select(m => m.ToModel()).ToList();
            return PartialView("TaskTypesControls/SkillSetsGrid", Repository.TaskTypesRepository.BaseSkillSets);
        }

        [Authorize]
        public ActionResult EscalatedSkillSetsGrid()
        {
            var skillsets = ProxyCaller.GetSkillSets(true, true);
            Repository.TaskTypesRepository.EscalatedSkillSets = skillsets.Where(m => m.IsEscalation).Select(m => m.ToModel()).ToList();
            return PartialView("TaskTypesControls/SkillSetsGrid", Repository.TaskTypesRepository.EscalatedSkillSets);
        }

        public JsonResult UpdateRelation(string processName, string skillSetName, string taskTypeName)
        {
            ProxyCaller.UpdateSkillSetRelation(processName, new List<TicketToolAdminLink.SkillSetRelation>{new TicketToolAdminLink.SkillSetRelation{SkillSetName = skillSetName, TaskTypeName = taskTypeName}});
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public JsonResult EditTask(string processName, string taskName, string skillsetTime, string userTime)
        {
            TimeSpan taskMaxTime;
            TimeSpan taskMaxUserTime;
            if(String.IsNullOrEmpty(processName) || String.IsNullOrEmpty(taskName) || !TimeSpan.TryParse(skillsetTime, out taskMaxTime) || !TimeSpan.TryParse(userTime, out taskMaxUserTime))
                return Json(false, JsonRequestBehavior.AllowGet);
            ProxyCaller.EditTaskType(processName, new TicketToolAdminLink.TaskType {Name = taskName, MaxSkillSetTime = taskMaxTime, MaxUserSetTime = taskMaxUserTime});
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public JsonResult DeleteSkillSet(string skillSetName)
        {
            if (String.IsNullOrEmpty(skillSetName))
                return Json(false, JsonRequestBehavior.AllowGet);
            ProxyCaller.DeleteSkillSet(skillSetName);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public JsonResult UpdateSkillSet(SkillSetItem item, string type, string oldSkillName)
        {
            switch (type)
            {
                case "create":
                    if (String.IsNullOrEmpty(item.Name))
                        return Json(false, JsonRequestBehavior.AllowGet);
                    ProxyCaller.CreateSkillSet(new List<TicketToolAdminLink.SkillSet> {item.ToContract()});
                    return Json(true, JsonRequestBehavior.AllowGet);
                case "edit":
                    if (String.IsNullOrEmpty(oldSkillName) || String.IsNullOrEmpty(item.Name))
                        return Json(false, JsonRequestBehavior.AllowGet);
                    ProxyCaller.RenameSkillSet(oldSkillName, item.Name);
                    return Json(true, JsonRequestBehavior.AllowGet);
                default:
                    return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
