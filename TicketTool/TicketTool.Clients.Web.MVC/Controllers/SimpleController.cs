﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketTool.Web.Common;
using TicketTool.Web.Models;
using TicketTool.Web.Models.Items;
using TicketTool.Web.TicketToolAdminLink;
using TicketTool.Web.TicketToolLink;
using SkillSet = TicketTool.Web.TicketToolLink.SkillSet;

namespace TicketTool.Web.Controllers
{
    public class SimpleController : Controller
    {
        public ActionResult BlankPage()
        {
            return View();
        }

        public ActionResult Index()
        {
            var data = new SimpleData();
            var user = HttpContext.User;
            if (user == null || !user.Identity.IsAuthenticated)
            {
                return View();
            }
            var client = new TicketToolServiceClient();
            var admin = new TicketToolAdminClient();
            var result = client.Ping();
            if (result.LoggedOnSkillSets.Length != 0)
                return RedirectToAction("Task", "Simple");
            data.UserSkillSet =
                admin.GetUserSkillSets()
                    .Select(m => new SkillSetItem { IsChecked = m.IsLogged, Name = m.Name, IsClustered = m.IsClustered })
                    .ToList();

            data.Clusters = admin.GetAllClusters().Select(m => new ClusterItem
            {
                IsSelected = true,
                Name = m.Name,
                Parameters = m.Parameters.ToList()
            }).ToList();
            return View(data);
        }

        public ActionResult LoginSkillSets(SimpleData data)
        {
            var skillSets = data.UserSkillSet.Where(m => m.IsChecked).ToList();
            if (skillSets.Count == 0)
                return RedirectToAction("Index", "Simple");
            var client = new TicketToolServiceClient();
            client.LogOutAndLogInToSkillSets(skillSets.Select(m => new SkillSet { Name = m.Name }).ToArray(),
                data.Clusters != null
                    ? data.Clusters.Where(m => m.IsSelected).Select(m => new TicketToolLink.Cluster
                    {
                        Name = m.Name,
                    }).ToArray()
                    : null);

            return RedirectToAction("Task", "Simple");
        }

        public ActionResult LogoutSkillSets()
        {
            var client = new TicketToolServiceClient();
            client.LogOutFromSkillSets();
            return RedirectToAction("Index", "Simple");
        }

        public ActionResult Task()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetMaxAge(new TimeSpan(0));
            Response.Cache.SetExpires(DateTime.Now);
            Response.Cache.SetNoStore();
            Response.Cache.SetRevalidation(HttpCacheRevalidation.None);
            Response.Cache.SetNoServerCaching();
            var user = HttpContext.User;
            if (user == null || !user.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Simple");
            }
            ViewData["PingInterval"] = ConfigurationManager.AppSettings["PingTaskIntervalInSec"];
            ViewData["SleepInterval"] = ConfigurationManager.AppSettings["SleepIntervalInSec"];
            var taskData = new SimpleTaskData();
            var client = new TicketToolServiceClient();
            var result = client.Ping();
            if (result.LoggedOnSkillSets.Length == 0)
                return RedirectToAction("Index", "Simple");
            taskData.LoggedSkillsets = result.LoggedOnSkillSets.Select(m => new SkillSetItem { Name = m }).ToList();
            var task = TempData["currentTask"] as TicketTask;
            if (task != null)
            {
                taskData.Task = new TaskItem(task);
                taskData.PingTask = result.Tasks.SingleOrDefault(m => m.TaskId == taskData.Task.TaskId);
            }
            //var task = client.GetNextTask();
            //if (task != null)
            //{
            //    taskData.Task = new TaskItem(task);
            //    taskData.PingTask = task != null ? result.Tasks.Where(m => m.TaskId == taskData.Task.TaskId).SingleOrDefault() : null;
            //}
            return View("Task", taskData);
        }

        /// <summary>
        /// Singles the specified task identifier.
        /// </summary>
        /// <param name="taskId">The task identifier.</param>
        /// <returns></returns>
        public ActionResult Single(string taskId)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetMaxAge(new TimeSpan(0));
            Response.Cache.SetExpires(DateTime.Now);
            Response.Cache.SetNoStore();
            Response.Cache.SetRevalidation(HttpCacheRevalidation.None);
            Response.Cache.SetNoServerCaching();
            var user = HttpContext.User;
            if (user == null || !user.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Simple");
            }
            ViewData["PingInterval"] = ConfigurationManager.AppSettings["PingTaskIntervalInSec"];
            ViewData["SleepInterval"] = ConfigurationManager.AppSettings["SleepIntervalInSec"];
            var taskData = new SimpleTaskData();
            using (var client = new TicketToolServiceClient())
            {
                var result = client.Ping();
                var task = TempData["currentTask"] as TicketTask;
                if (task == null)
                {
                    task = client.GetTask(taskId);
                }
                
                if (task.State == TaskState.Completed || task.State == TaskState.Cancelled ||
                    !string.Equals(task.AssignedTo,user.Identity.Name,StringComparison.InvariantCultureIgnoreCase))
                {
                    return RedirectToAction("BlankPage", "Simple");
                }
                
                taskData.Task = new TaskItem(task);
                taskData.PingTask = result.Tasks.SingleOrDefault(m => m.TaskId == taskData.Task.TaskId);
                
                //var task = client.GetNextTask();
                //if (task != null)
                //{
                //    taskData.Task = new TaskItem(task);
                //    taskData.PingTask = task != null ? result.Tasks.Where(m => m.TaskId == taskData.Task.TaskId).SingleOrDefault() : null;
                //}
                return View("Single", taskData);
            }

        }

        [HttpPost]
        public ActionResult Finish(SimpleTaskData data, int outcomeIndex)
        {
            var selectedOutcome = data.Task.Outcomes[outcomeIndex];
            var outcomeFields = selectedOutcome.RequiredFields.Select(m => new RequiredField
                                 {
                                     Name = string.Format("{0};#{1}", selectedOutcome.Value, m.Name),
                                     Type = m.StringType,
                                     Value = m.GetConvertedValue()
                                 }).ToList();

            var taskFields = data.Task.Fields.Select(m => new RequiredField
            {
                Name = m.Name,
                Type = m.StringType,
                Value = m.GetConvertedValue()
            });

            var client = new TicketToolServiceClient();
            client.CompleteTask(data.Task.TaskId, selectedOutcome.Value, taskFields.Union(outcomeFields).ToArray());
            if (data.IsSinglePage)
            {
                return RedirectToAction("SelfClosePage", "Simple");
            }
            return RedirectToAction("Task", "Simple");
        }

        public ActionResult SelfClosePage()
        {
            return View();
        }

        public JsonResult Pulse(string taskId)
        {
            var client = new TicketToolServiceClient();
            if (String.IsNullOrEmpty(taskId))
                return null;
            var result = client.Ping();
            if (result.LoggedOnSkillSets == null)
                return null;
            var skillSets = "[" + String.Join("; ", result.LoggedOnSkillSets) + "]";
            PingTask pingTask = result.Tasks.SingleOrDefault(m => m.TaskId == taskId);
            if (pingTask == null)
                return null;
            return Json(new { skillSets, taskTime = String.Format("{0:hh\\:mm\\:ss}", pingTask.UserTime), maxTime = String.Format("{0:hh\\:mm\\:ss}", pingTask.MaxTimeForUser) }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckTask(string taskId)
        {
            var client = new TicketToolServiceClient();
            var task = client.GetNextTask();
            if ((task != null && task.TaskId != taskId) || (task == null && !String.IsNullOrEmpty(taskId)))
            {
                TempData["currentTask"] = task;
                return Json(new { taskExists = true }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { taskExists = false }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult PostponeTask(string taskId, int minutes)
        {
            var client = new TicketToolServiceClient();
            if (minutes < 1 || String.IsNullOrEmpty(taskId))
                return Json(new { isPostponed = false }, JsonRequestBehavior.AllowGet);
            var timespan = new TimeSpan(0, minutes, 0);
            var result = client.PostponeTask(taskId, timespan);
            if (!result.HasError)
            {
                return Json(new { isPostponed = true }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { isPostponed = false }, JsonRequestBehavior.AllowGet);
        }
    }
}
