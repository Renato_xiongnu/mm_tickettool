﻿using System;
using System.Linq;
using System.Web.Mvc;
using TicketTool.Web.Common;
using TicketTool.Web.Models;
using TicketTool.Web.Models.Items;

namespace TicketTool.Web.Controllers
{
    public class UsersController : Controller
    {
        //
        // GET: /Users/

        public ActionResult Index()
        {
            var user = HttpContext.User;
            if (user == null || !user.Identity.IsAuthenticated)
            {
                if (Request.Browser.IsMobileDevice)
                {
                    return RedirectToAction("Index", "Account", new { returnUrl = Request.Url.ToString() });
                }
                return View(Repository.UsersRepository);
            }
            Repository.UsersRepository.Users = ProxyCaller.GetAllUsers().Select(m => m.ToModel()).ToList();
            return View(Repository.UsersRepository);
        }

        [Authorize]
        public ActionResult DialogCreate()
        {
            return PartialView("UsersControls/UserForm", new UserItem());
        }
        [Authorize]
        public ActionResult DialogEdit(string userName)
        {
            var user = ProxyCaller.GetUser(userName);
            return PartialView("UsersControls/UserForm", user.ToModel());
        }

        #region [Grids]

        [Authorize]
        public ActionResult AvailableSkillSetsGrid(string roles)
        {
            if (String.IsNullOrEmpty(roles)) return null;
            var listRoles = roles.Split(new[] { ", " }, StringSplitOptions.RemoveEmptyEntries).ToArray();
            var data = ProxyCaller.GetAvailableSkillSets(listRoles).Select(m => m.ToModel());
            return PartialView("UsersControls/SkillSetsGrid", data);
        }

        [Authorize]
        public ActionResult UsersGrid()
        {
            Repository.UsersRepository.Users = ProxyCaller.GetAllUsers().Select(m => m.ToModel()).ToList();
            return PartialView("UsersControls/UsersGrid", Repository.UsersRepository.Users);
        }

        [Authorize]
        public ActionResult RolesGrid()
        {
            return PartialView("UsersControls/RolesGrid", UsersData.Roles);
        }

        [Authorize]
        public ActionResult StoresGrid()
        {
            return PartialView("UsersControls/StoresGrid", UsersData.Stores);
        }

        #endregion

        [Authorize]
        public JsonResult CreateUser(UserItem user)
        {
            var result = ProxyCaller.CreateUser(user.Name, user.Password, user.Email, user.Roles.ToArray(), user.Stores.ToArray(), user.AvailableSkillSets.ToArray());
            return Json(result != 0, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public JsonResult UpdateUser(UserItem user)
        {
            var b = ProxyCaller.UpdateUser(user.IsLocked, user.Name, user.Password, user.Email, user.Roles.ToArray(), user.Stores.ToArray(), user.AvailableSkillSets.ToArray());
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public JsonResult CheckUser(long id, string name)
        {
            return Json(ProxyCaller.CheckUserName(id, name), JsonRequestBehavior.AllowGet);
        }

        public ActionResult AllUsers()
        {
            return View();
        }


        [Authorize]
        public JsonResult GetAllUsers()
        {
            return Json(ProxyCaller.GetAllUsers().Select(m => m.ToModel()), JsonRequestBehavior.AllowGet);
        }

    }
}
