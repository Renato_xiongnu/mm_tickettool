﻿using System.Collections.Generic;
using System.Linq;
using TicketTool.Web.Common;
using TicketTool.Web.Models.Items;

namespace TicketTool.Web.Models
{
    public class UsersData
    {
        public List<UserItem> Users { get; set; }

        public static List<string> Roles
        {
            get { return ProxyCaller.GetAllRoles().ToList(); }
        }

        public static List<StoreItem> Stores
        {
            get { return ProxyCaller.GetAllStores().Select(m => new StoreItem(m.Name, m.ParameterName)).ToList(); }
        }
    }
}