﻿using System;

namespace TicketTool.Web.Models.Items
{
    public class TaskTypeItem
    {
        public string Name { get; set; }

        public TimeSpan MaxSkillSetRunTime { get; set; }
        public TimeSpan MaxUserSetTime { get; set; }

        public TicketToolAdminLink.SkillSet SkillSet { get; set; }
        public TicketToolAdminLink.SkillSet EscalatedSkillSet { get; set; }
    }
}