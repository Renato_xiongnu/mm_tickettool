﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Microsoft.Practices.Unity;
using TicketTool.Data;
using TicketTool.Data.Contracts;
using TicketTool.Data.Contracts.Integration;
using TicketTool.Services;

namespace TicketTool.Integration
{
    public class TicketToolIntegration : ITicketToolIntegration
    {
        private static IUnityContainer _container = new UnityContainer();
        // ReSharper disable UnusedAutoPropertyAccessor.Local
        private ITicketService TicketService
        {
            get { return _container.Resolve<ITicketService>(); }
        }

        public IList<CallCenterStat> GetStatistics()
        {
            return AuthContext.IsAdmin
                       ? ServiceHelper.RunInContext<IList<CallCenterStat>>(() => TicketService.SkillSets.GetStatisticsForSkillSets()
                                                                       .Select(el =>
                                                                           new CallCenterStat
                                                                               {
                                                                                   SkillSetName = el.SkillSet.Name,
                                                                                   TaskCount = el.TasksInProgress+el.TasksInQueue+el.EscalatedTasks,
                                                                                   MaxDelay = el.SkillSet.

                                                                               }).ToList());
                       : new Collection<SkillSetStat>();
        }
    }
}
