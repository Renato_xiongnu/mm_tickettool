﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using TicketTool.Data.Contracts.Integration;

namespace TicketTool.Integration
{
    [ServiceContract]
    public interface ITicketToolIntegration
    {
        IList<CallCenterStat> GetStatistics();
    }
}
