﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TicketTool.Web.V2.Startup))]
namespace TicketTool.Web.V2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
