﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TicketTool.Data.Contracts;
using TicketTool.Web.V2.TicketToolAdminLink;
using TicketTool.Web.V2.TicketToolLink;
using TicketTool.Web.V2.Models;

namespace TicketTool.Web.V2.Controllers
{
    public class SkillSetController : ApiController
    {
        // GET: api/SkillSet
        [Authorize]
        [Route("api/SkillSet")]
        public async Task<SimpleData> Get()
        {
            var admin = new TicketToolAdminClient();
            var skillSets = admin.GetUserSkillSetsAsync();
            var clusters = admin.GetUserClustersAsync();
            await Task.WhenAll(skillSets, clusters);
            return new SimpleData { UserSkillSet = skillSets.Result, Clusters = clusters.Result };
            //var skillSets = await admin.GetUserSkillSetsAsync();
            //var clusters = await admin.GetUserClustersAsync();
            //return new SimpleData { UserSkillSet = skillSets, Clusters = clusters };
        }

        //// GET: api/SkillSet/5
        //public async Task<string> Get(int id)
        //{
        //    return "value";
        //}

        // POST: api/SkillSet
        [Route("api/SkillSet")]
        [HttpPost]
        public async Task<SimpleData> Post([FromBody]SimpleData data)
        {
            var client = new TicketToolServiceClient();
            if (data == null || data.UserSkillSet == null || data.UserSkillSet.Length == 0)
            {
                await client.LogOutFromSkillSetsAsync();
            }            
            else
            {
                var skillSets = data.UserSkillSet.Select(m => new SkillSet { Name = m.Name }).ToArray();
                var clusters = data.Clusters != null && data.Clusters.Length > 0 ? data.Clusters.Select(c => new Cluster { Name = c.Name }).ToArray() : null;
                await client.LogOutAndLogInToSkillSetsAsync(skillSets, clusters);
            }
            return await Get();
        }

        //// PUT: api/SkillSet/5
        //public async Task Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE: api/SkillSet/5
        //public async Task Delete(int id)
        //{
        //}
    }
}