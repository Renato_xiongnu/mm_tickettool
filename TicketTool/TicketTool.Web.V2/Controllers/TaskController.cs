﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using TicketTool.Data.Contracts.Common;
using TicketTool.Data.Contracts.Operations;
using TicketTool.Data.Contracts.Tasks;
using TicketTool.Web.V2.Models;
using TicketTool.Web.V2.Models.Items;
using TicketTool.Web.V2.TicketToolLink;

namespace TicketTool.Web.V2.Controllers
{
    public class TaskController : ApiController
    {
        // GET: api/Task
        [Authorize]
        [Route("api/Task")]
        [HttpGet]
        public async Task<TaskItem> Get()
        {
            var client = new TicketToolServiceClient();
            var task = await client.GetNextTaskAsync();
            if (task != null)
            {
                var ticket = await client.GetTicketInfoAsync(task.TicketId);
                var taskItem = new TaskItem(task, ticket);
                ChangeWorkItemUrl(taskItem);
                return taskItem;
            }
            return null;
        }

        //// GET: api/Task/5
        [Authorize]
        [Route("api/Task/{taskId}")]
        [HttpGet]
        public async Task<PingByUserResult> Get(string taskId)
        {
            var client = new TicketToolServiceClient();
            if (string.IsNullOrEmpty(taskId))
            {
                return null;
            }
            var result = await client.PingAsync();
            return result;
        }

        // POST: api/Task.5
        [Authorize]
        [Route("api/Task/{taskId}")]
        [HttpPost]
        public async Task<OperationResult> Post(string taskId, [FromBody]TaskOutcomeItem outcome)
        {
            var client = new TicketToolServiceClient();
            var task = await client.GetNextTaskAsync();
            if (task == null || task.TaskId != taskId)
            {
                return new OperationResult { HasError = true, MessageText = "Задача не соответствует назначенной" };
            }

            var outcomeFields = outcome.RequiredFields.Select(f => new RequiredField
            {
                Name = string.Format("{0};#{1}", outcome.Value, f.Name),
                Type = f.StringType,
                Value = f.GetConvertedValue()
            }).ToArray();

            return await client.CompleteTaskAsync(taskId, outcome.Value, outcomeFields);
        }

        // POST: api/Task/5/
        [Authorize]
        [Route("api/Task/{taskId}/Postpone")]
        [HttpPost]
        public async Task<OperationResult> PostponeTask(string taskId, [FromBody]int minutes)
        {
            if (minutes < 1)
            {
                return new OperationResult { HasError = true, MessageText = "Количество минут должно быть больше 0" };
            }
            if (string.IsNullOrEmpty(taskId))
            {
                return new OperationResult { HasError = true, MessageText = "Не задан идентификатор задачи" };
            }
            var client = new TicketToolServiceClient();
            var timespan = new TimeSpan(0, minutes, 0);
            return await client.PostponeTaskAsync(taskId, timespan);
        }

        /// <summary>
        /// TODO: Only For Tests! 
        /// </summary>
        private void ChangeWorkItemUrl(TaskItem taskItem)
        {
            //
            //FROM: http://orders2.backend.mediasaturnrussia.ru/ru/Clients/CallCenter.MVC/CallCenter?orderId={id}&canEdit=lines;delivery;contacts
            //TO: http://orders2.backend.mediasaturnrussia.ru/ru/Clients/CallCenter.MVC/CallCenter/IndexNew?orderId={id}&canEdit=lines;delivery;contacts

            var userName = HttpContext.Current.User.Identity.Name;
            var newCallCenterUsers = (ConfigurationManager.AppSettings["NewCallCenterUsers"] ?? "").Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            
            if (taskItem.InfoUrl != null && newCallCenterUsers.Contains(userName, StringComparer.InvariantCultureIgnoreCase))
            {
                var replaceUrl = ConfigurationManager.AppSettings[taskItem.Type + "_ReplaceUrl"];
                if (!string.IsNullOrEmpty(replaceUrl))
                {
                    var url = replaceUrl.Replace("{id}", taskItem.WorkitemId);
                    taskItem.InfoUrl = new Uri(url);
                }
                else
                {
                    var url = taskItem.InfoUrl.AbsoluteUri;
                    url = url.Replace("CallCenter.MVC/CallCenter?", "CallCenter.MVC/CallCenter/IndexNew?");
                    url = url.Replace("CallCenter.MVC/CallCenter/Index?", "CallCenter.MVC/CallCenter/IndexNew?");
                    taskItem.InfoUrl = new Uri(url);
                }
            }
        }

        //// DELETE: api/Task/5
        //public void Delete(int id)
        //{
        //}
    }
}
