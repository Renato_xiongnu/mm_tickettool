﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TicketTool.Data.Contracts.Common;
using TicketTool.Data.Contracts.Operations;
using TicketTool.Data.Contracts.Tasks;
using TicketTool.Web.V2.Models;
using TicketTool.Web.V2.Models.Items;
using TicketTool.Web.V2.TicketToolLink;

namespace TicketTool.Web.V2.Controllers
{
    public class TestTaskController : ApiController
    {
        [Authorize]
        [Route("api/TestTask")]
        [HttpGet]
        public async Task<TaskItem> Get()
        {
            var client = new TicketToolServiceClient();
            var task = TestTask();
            if (task != null)
            {
                var ticket = new TicketTO() { WorkitemId = "111-12345678" };
                return new TaskItem(task, ticket);
            }
            return null;
        }

        [Authorize]
        [Route("api/TestTask/{taskId}")]
        [HttpGet]
        public async Task<PingByUserResult> Get(string taskId)
        {
            return new PingByUserResult
            {
                Tasks = new PingTask[]
                {
                    new PingTask
                    {
                        TaskId = "111",
                        UserTime = TimeSpan.FromMinutes(19),
                        MaxTimeForUser = TimeSpan.FromMinutes(20),
                    }
                }
            };
        }

        [Authorize]
        [Route("api/TestTask/{taskId}")]
        [HttpPost]
        public async Task<OperationResult> Post(string taskId, [FromBody]TaskOutcomeItem outcome)
        {
            var task = TestTask();

            var outcomeFields = outcome.RequiredFields.Select(f => new RequiredField
            {
                Name = string.Format("{0};#{1}", outcome.Value, f.Name),
                Type = f.StringType,
                Value = f.GetConvertedValue()
            }).ToArray();

            return new OperationResult { HasError = false };
        }


        public static TicketTask TestTask()
        {
            var task = new TicketTask
            {
                TaskId = "111",
                Name = "Тестовая задача",
                Description = "Протестируй меня! Протестируй меня! Протестируй меня!",
                Outcomes = new Outcome[]
                {
                    new Outcome
                    {
                        Value = "String Value 1",
                        RequiredFields = new RequiredField[]
                        {
                            new RequiredField
                            {
                                Name = "Required Field 1",
                                Type = "System.String",
                            },
                            new RequiredFieldValueList
                            {
                                Name = "Required Field 2",
                                Type = "Choice",
                                PredefinedValuesList = new object[] { "Value 1", "Value 2", "Value 3" }
                            },
                            new RequiredField
                            {
                                Name = "Required Field 3",
                                Type = "System.DateTime",
                            },
                            new RequiredField
                            {
                                Name = "Required Field 4",
                                Type = "System.Boolean",
                            },
                            new RequiredField
                            {
                                Name = "TimeSpan Field 5",
                                Type = "System.TimeSpan",
                            }
                        }
                    },
                    new Outcome
                    {
                        Value = "Choice Value 1",
                        RequiredFields = new RequiredField[]
                        {
                            new RequiredFieldValueList
                            {
                                Name = "Required Field 1",
                                Type = "Choice",
                                DefaultValue = "Value 2",
                                PredefinedValuesList = new object[] { "Value 1", "Value 2", "Value 3" }
                            }
                        }
                    },
                    new Outcome
                    {
                        Value = "DateTime Value 1",
                        RequiredFields = new RequiredField[]
                        {
                            new RequiredField
                            {
                                Name = "Required Field 1",
                                Type = "System.DateTime",
                                DefaultValue = DateTime.Now
                            }
                        }
                    },
                    new Outcome
                    {
                        Value = "Boolean Value 1",
                        RequiredFields = new RequiredField[]
                        {
                            new RequiredField
                            {
                                Name = "Required Field 1",
                                Type = "System.Boolean",
                                DefaultValue = true
                            }
                        }
                    },
                    new Outcome
                    {
                        Value = "TimeSpan Value 1",
                        RequiredFields = new RequiredField[]
                        {
                            new RequiredField
                            {
                                Name = "TimeSpan Field 1",
                                Type = "System.TimeSpan",
                                DefaultValue = TimeSpan.FromHours(36.5)
                            }
                        }
                    }

                },
                WorkitemPageUrl = "about:blank",
            };
            return task;
        }
    }
}
