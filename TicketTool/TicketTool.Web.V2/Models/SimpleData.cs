﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TicketTool.Data.Contracts;
using TicketTool.Web.V2.TicketToolAdminLink;

namespace TicketTool.Web.V2.Models
{
    public class SimpleData
    {
        public SimpleData()
        {            
        }

        public UserSkillSet[] UserSkillSet { get; set; }

        public UserCluster[] Clusters { get; set; }
    }
}