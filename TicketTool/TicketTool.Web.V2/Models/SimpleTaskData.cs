﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TicketTool.Data.Contracts.Tasks;
using TicketTool.Web.V2.Models.Items;

namespace TicketTool.Web.V2.Models
{
    public class SimpleTaskData
    {
        public SimpleTaskData()
        {
            LoggedSkillsets = new List<SkillSetItem>();
        }

        public List<SkillSetItem> LoggedSkillsets { get; set; }

        public TaskItem Task { get; set; }

        public PingTask PingTask { get; set; }

        public bool IsSinglePage { get; set; }
    }
}