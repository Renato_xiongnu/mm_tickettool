﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using TicketTool.Data.Contracts.Common;
using TicketTool.Data.Contracts.Tasks;

namespace TicketTool.Web.V2.Models.Items
{
    public class TaskItem
    {
        public static Regex OrderIdMatch = new Regex(@"\d{3}\-\d{8,12}");

        public string TaskId { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Uri InfoUrl { get; set; }
        public bool ShowInfoUrl { get { return InfoUrl != null; } }
        public bool IsEscalated { get; set; }

        public string WorkitemId { get; set; }

        public List<TaskFieldItem> Fields { get; set; }

        [UIHint("SimpleControls/Outcome")]
        public List<TaskOutcomeItem> Outcomes { get; set; }

        public TaskItem()
        {
            Fields = new List<TaskFieldItem>();
        }

        public TaskItem(TicketTask task, TicketTO ticket)
        {
            TaskId = task.TaskId;
            Type = task.Type;
            Name = task.Name;
            Description = task.Description;
            IsEscalated = task.Escalated;
            // copied from silverlight.
            task.RequiredFields = null;
            Fields = task.RequiredFields == null ?
                new List<TaskFieldItem>() :
                task.RequiredFields.Select(r => new TaskFieldItem(r)).ToList();
            //
            Outcomes = task.Outcomes.Select(o => new TaskOutcomeItem(o)).ToList();
            InfoUrl = task.WorkitemPageUrl != null ? new Uri(task.WorkitemPageUrl) : null;

            if (!string.IsNullOrEmpty(ticket.WorkitemId))
            {
                var match = OrderIdMatch.Match(ticket.WorkitemId);
                WorkitemId = match.Success ? match.Value : ticket.WorkitemId;
            }
        }
    }

    public class TaskOutcomeItem
    {
        public TaskOutcomeItem()
        {
            RequiredFields = new List<TaskFieldItem>();
        }

        public TaskOutcomeItem(Outcome outcome)
        {
            Value = outcome.Value;
            RequiredFields =
                outcome.RequiredFields == null ?
                new List<TaskFieldItem>() :
                outcome.RequiredFields.Select(x => new TaskFieldItem(x)).ToList();
            HaveRequiredFields = outcome.RequiredFields != null && outcome.RequiredFields.Count > 0;
            ToolTipText = outcome.RequiredFields != null ?
                ("Заполните следующие поля:" + Environment.NewLine + string.Join("," + Environment.NewLine, outcome.RequiredFields.Select(x => " -" + x.Name + "")) + "!") :
                "";
        }

        [UIHint("SimpleControls/Field")]
        public List<TaskFieldItem> RequiredFields { get; set; }

        public bool HaveRequiredFields { get; set; }
        public string ToolTipText { get; set; }
        public string Value { get; set; }
    }

    public class TaskFieldItem
    {
        public TaskFieldItem()
        {

        }

        public TaskFieldItem(RequiredField r)
        {
            Name = r.Name;
            StringType = r.Type;
            var rlist = r as RequiredFieldValueList;
            if (rlist != null && rlist.PredefinedValuesList.Length > 0)
            {
                PredefinedValuesList = rlist.PredefinedValuesList.Select(i => i.ToString()).ToArray();
            }
           
            var initValue = r.DefaultValue == null ? "" : r.DefaultValue.ToString();
            if (string.IsNullOrEmpty(initValue) && ActualType == typeof(Boolean))
            {
                initValue = "false";
            }
            // ?
            //Value = initValue;
            DefaultValue = initValue;
        }

        public string Name { get; set; }
        public Type ActualType
        {
            get
            {
                return Type.GetType(StringType);
            }
        }
        public string StringType { get; set; }
        public string DefaultValue { get; set; }

        public string[] PredefinedValuesList
        {
            get { return _predefinedValuesList??(_predefinedValuesList=new string[0]); }
            set { _predefinedValuesList = value; }
        }

        public string ErrorMessage { get; set; }
        public bool ShowDropDown
        {
            get
            {
                return PredefinedValuesList != null;
            }
        }

        //private string _value;
        public object Value
        {
            get 
            {
                if (ActualType == typeof(DateTime))
                {
                    return DateTimeValue;
                }
                if (ActualType == typeof(TimeSpan))
                {
                    return TimeSpanFullValue;
                }
                if (ActualType == typeof(Boolean))
                {
                    return BoolValue;
                }
                return TextValue;
            }
        }

        #region [TimeSpan]

        private string _timeSpanDayValue;
        public string TimeSpanDayValue
        {
            get { return _timeSpanDayValue; }
            set
            {
                _timeSpanDayValue = value;
                SetTimeSpanValue();
            }
        }

        private string _timeSpanHourValue;
        public string TimeSpanHourValue
        {
            get { return _timeSpanHourValue; }
            set
            {
                _timeSpanHourValue = value;
                SetTimeSpanValue();
            }
        }

        private string _timeSpanMinValue;

        private string[] _predefinedValuesList;

        public string TimeSpanMinValue
        {
            get { return _timeSpanMinValue; }
            set
            {
                _timeSpanMinValue = value;
                SetTimeSpanValue();
            }
        }

        public TimeSpan? TimeSpanFullValue { get; private set; }
        private void SetTimeSpanValue()
        {
            int day;
            int hour;
            int min;
            Int32.TryParse(TimeSpanDayValue, out day);
            Int32.TryParse(TimeSpanHourValue, out hour);
            Int32.TryParse(TimeSpanMinValue, out min);
            if (day == 0 && hour == 0 && min == 0)
            {
                TimeSpanFullValue = null;
            }
            else
            {
                TimeSpanFullValue = new TimeSpan(day, hour, min, 0);
            }
        }

        #endregion

        #region [DateTime]

        //rivate DateTime? _dateTimeValue;
        public DateTime? DateTimeValue { get; set; }

        #endregion

        #region [Boolean]

        //private bool _boolValue;
        public bool BoolValue { get; set; }

        #endregion

        #region [ValuesLists]

        public static List<NameValueObject> TimeSpanDayValues
        {
            get
            {
                List<NameValueObject> days = new List<NameValueObject>();
                for (var i = 0; i <= 31; i++)
                {
                    days.Add(new NameValueObject
                    {
                        Name = string.Format("{0} дней", i),
                        Value = "" + i
                    });
                }
                return days;
            }
        }

        public static List<NameValueObject> TimeSpanHourValues
        {
            get
            {
                List<NameValueObject> hours = new List<NameValueObject>();
                for (int i = 0; i <= 24; i++)
                {
                    hours.Add(new NameValueObject
                    {
                        Name = string.Format("{0} часов", i),
                        Value = "" + i
                    });
                }
                return hours;
            }
        }

        public static List<NameValueObject> TimeSpanMinValues
        {
            get
            {
                List<NameValueObject> mins = new List<NameValueObject>();
                for (int i = 0; i < 12; i++)
                {
                    mins.Add(new NameValueObject
                    {
                        Name = string.Format("{0} минут", (i * 5)),
                        Value = "" + (i * 5)
                    });
                }
                return mins;
            }
        }

        #endregion

        public string TextValue { get; set; }


        public object GetConvertedValue()
        {
            return Value;
        }

        //public string CheckValid()
        //{
        //    if (string.IsNullOrWhiteSpace(Value))
        //    {
        //        return String.Format("Поле {0} не может быть пустым", Name);

        //    }
        //    object result;
        //    if (ActualType != null && !Value.Convert(ActualType, out result))
        //    {
        //        return String.Format("Поле {0} ...", Name);
        //    }
        //    return "";
        //    //else if (ValueType == typeof(Int32) && !int.TryParse(value, out i32))
        //    //{
        //    //    ErrorMessage = string.Format("Поле {0} должно быть целым числом", Name);
        //    //}
        //    //else if (ValueType == typeof(Double) && !double.TryParse(value, out d))
        //    //{
        //    //    ErrorMessage = string.Format("Поле {0} должно быть дробным числом", Name);
        //    //}
        //    //else if (ValueType == typeof(DateTime) && !DateTime.TryParse(value, out dt))
        //    //{
        //    //    ErrorMessage = string.Format("Поле {0} должно быть датой", Name);
        //    //}
        //    //else if (ValueType == typeof(TimeSpan) && !TimeSpan.TryParse(value, out ts))
        //    //{
        //    //    ErrorMessage = string.Format("Поле {0} должно быть временным периодом", Name);
        //    //}
        //    //else if (ValueType == typeof(Boolean) && !bool.TryParse(value, out b))
        //    //{
        //    //    ErrorMessage = string.Format("Поле {0} должно быть да/нет", Name);
        //    //}
        //    //else // ok
        //    //{
        //    //    ErrorMessage = "";
        //    //}
        //}
    }

    public class NameValueObject
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }


}