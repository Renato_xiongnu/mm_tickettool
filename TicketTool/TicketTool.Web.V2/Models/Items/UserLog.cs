﻿using System;
using System.Collections.Generic;
using System.Linq;
using TicketTool.Data.Contracts;

namespace TicketTool.Web.V2.Models.Items
{
    public class UserLogItem
    {
        public string UserName { get; set; }
        public int SkillSetQty { get; set; }
        public int ProcessedQty { get; set; }

        public List<UserLogLineItem> Lines { get; set; }

        public static UserLogItem FromContractModel(string userName, ICollection<UserStat> userStats)
        {
            var result = new UserLogItem
            {
                UserName = userName,
            };
            if (userStats.Count > 0)
            {
                result.UpdateFromContractModel(userStats);
                foreach (var userStat in userStats)
                {
                    result.Lines.Add(UserLogLineItem.FromContractModel(userStat));
                }
            }
            return result;
        }

        public void UpdateFromContractModel(ICollection<UserStat> userStats)
        {
            if (userStats.Count > 0)
            {
                ProcessedQty = userStats.Sum(t => t.ProcessedQty);
                SkillSetQty = userStats.Count(t => !t.LogoutDate.HasValue);
                Lines = new List<UserLogLineItem> { new UserLogLineItem { IsHeader = true } };
            }
        }
    }

    public class UserLogLineItem
    {
        public bool IsHeader { get; set; }

        public string SkillSetName { get; set; }

        public DateTime LoginDate { get; set; }

        public TimeSpan SkillSetTime { get; set; }

        public DateTime? LogoutDate { get; set; }

        public int AbandonedQty { get; set; }

        public int ProcessedQty { get; set; }

        public static UserLogLineItem FromContractModel(UserStat userStat)
        {
            return new UserLogLineItem
            {
                SkillSetName = userStat.SkillSetName,
                AbandonedQty = userStat.AbandonedQty,
                ProcessedQty = userStat.ProcessedQty,
                LoginDate = userStat.LoginDate,
                LogoutDate = userStat.LogoutDate,
                SkillSetTime = userStat.SkillSetTime
            };
        }
    }
}