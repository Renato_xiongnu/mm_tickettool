﻿using System;
using TicketTool.Data.Contracts;

namespace TicketTool.Web.V2.Models.Items
{
    public class TaskTypeItem
    {
        public string Name { get; set; }

        public TimeSpan MaxSkillSetRunTime { get; set; }
        public TimeSpan MaxUserSetTime { get; set; }

        public SkillSet SkillSet { get; set; }
        public SkillSet EscalatedSkillSet { get; set; }
    }
}