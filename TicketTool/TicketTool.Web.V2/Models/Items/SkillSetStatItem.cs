﻿using System.Collections;
using System.Collections.Generic;

namespace TicketTool.Web.V2.Models.Items
{
    public class SkillSetStatItem
    {
        public string Name { get; set; }
        public int TasksInQueue { get; set; }
        public int TasksInProgress { get; set; }
        public int EscalatedTasks { get; set; }
        public int UsersQty { get; set; }
        public bool IsClustered { get; set; }
        public IList<SkillSetStatItem> ChildSkillSets { get; set; }
    }
}