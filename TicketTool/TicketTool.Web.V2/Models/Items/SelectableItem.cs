﻿namespace TicketTool.Web.V2.Models.Items
{
    public class SelectableItem
    {
        public SelectableItem(){}

        public SelectableItem(string name)
        {
            Name = name;
        }

        public bool IsSelected { get; set; }
        public string Name { get; set; }
    }
}