﻿namespace TicketTool.Web.V2.Models.Items
{
    public class StoreItem : SelectableItem
    {
        public  StoreItem()
        {
        }

        public StoreItem(string name, string sapCode)
            : base(name)
        {
            SapCode = sapCode;
        }

        public string SapCode { get; set; }
    }
}