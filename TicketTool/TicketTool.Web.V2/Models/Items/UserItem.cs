﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace TicketTool.Web.V2.Models.Items
{
    public class UserItem
    {
        [Display(Name = "Заблокирован")]
        public Boolean IsLocked { get; set; }

        public long Id { get; set; }

        [Display(Name = "Имя")]
        public string Name { get; set; }

        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Display(Name = "Повторите пароль")]
        public string RepeatPassword { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        public List<string> Roles { get; set; }
        public List<string> AvailableSkillSets { get; set; }
        public List<string> Stores { get; set; }

        public string RolesString
        {
            get { return Roles != null ? string.Join(", ", Roles) : ""; }
            set { Roles = !String.IsNullOrEmpty(value) ? value.Split(new[] { ", " }, StringSplitOptions.RemoveEmptyEntries).ToList() : new List<string>(); }
        }

        public string AvailableSkillSetsString
        {
            get { return AvailableSkillSets != null ? string.Join(", ", AvailableSkillSets) : ""; }
            set { AvailableSkillSets = !String.IsNullOrEmpty(value) ? value.Split(new[] { ", " }, StringSplitOptions.RemoveEmptyEntries).ToList() : new List<string>(); }
        }

        public string StoresString
        {
            get { return Stores != null ? string.Join(", ", Stores) : ""; }
            set { Stores = !String.IsNullOrEmpty(value) ? value.Split(new[] { ", " }, StringSplitOptions.RemoveEmptyEntries).ToList() : new List<string>(); }
        }


        public DateTime CreatedDate { get; set; }
        public DateTime LastLoginDate { get; set; }
        #region [DateTimeFormatted]

        public string CreatedDateFormatted
        {
            get
            {
                return String.Format("{0:dd.MM.yyyy}", CreatedDate);
            }
        }
        public string LastLoginDateFormatted
        {
            get
            {
                return String.Format("{0:dd.MM.yyyy}", LastLoginDate);
            }
        }

        #endregion

        public string CombinedData
        {
            get
            {
                return Id + ";" + Name + ";" + Email + ";" + RolesString + ";" + StoresString + ";" + AvailableSkillSetsString;
            }
        }

        public void CopyFrom(UserItem user)
        {
            Email = user.Email;
            Stores = user.Stores;
            Roles = user.Roles;
            CreatedDate = user.CreatedDate;
            LastLoginDate = user.LastLoginDate;
        }
    }
}