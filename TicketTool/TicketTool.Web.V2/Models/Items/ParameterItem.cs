﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicketTool.Web.V2.Models.Items
{
    public class ParameterItem:SelectableItem
    {
        public string DisplayName { get; set; }
    }
}