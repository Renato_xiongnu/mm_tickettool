﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TicketTool.Data.Contracts;
using TicketTool.Data.Contracts.Common;
using TicketTool.Web.V2.AuthenticationLink;
using TicketTool.Web.V2.TicketToolAdminLink;

namespace TicketTool.Web.V2.Common
{
    public static class ProxyCaller
    {
        private static readonly AuthenticationServiceClient Authentificator = new AuthenticationServiceClient();
        //private static readonly TicketToolAdminClient TicketToolAdmin = new TicketToolAdminClient();

        #region [Authentication]

        public static bool Login(string name, string password)
        {
            return Authentificator.LogIn(name, password, true);
        }

        public static async Task<bool> LoginAsync(string name, string password)
        {
            return await Authentificator.LogInAsync(name, password, true);
        }


        public static void Logout()
        {
            Authentificator.LogOut();
        }

        public static async Task LogoutAsync()
        {
            await Authentificator.LogOutAsync();
        }


        public static UserInfo GetCurrentUser()
        {
            return Authentificator.GetCurrentUser();
        }

        #endregion

        //public static SkillSetStat[] GetSkillStatistics()
        //{
        //    return TicketToolAdmin.GetSkillSetStat();
        //}

        //public static ParameterRealTimeStat[] GetStoreStatistics()
        //{
        //    return TicketToolAdmin.GetParamsStat();
        //}

        //public static UserStat[] GetUserStatistics()
        //{
        //    return TicketToolAdmin.GetUsersStat();
        //}

        //public static ExternalSystemInfo[] GetProcesses()
        //{
        //    return TicketToolAdmin.GetAllExternalSystems();
        //}

        //public static TaskType[] GetTaskTypes(string processName)
        //{
        //    return TicketToolAdmin.GetTasksTypes(processName);
        //}

        //public static SkillSet[] GetSkillSets(bool withDefault, bool withRoles)
        //{
        //    return TicketToolAdmin.GetSkillSets(withDefault, withRoles);
        //}

        //public static void UpdateSkillSetRelation(string processName, List<SkillSetRelation> relations)
        //{
        //    TicketToolAdmin.UpdateSkillSetRelation(processName, relations.ToArray());
        //}

        //public static void EditTaskType(string processName, TaskType tt)
        //{
        //    TicketToolAdmin.UpdateTasksType(processName, tt);
        //}

        //public static void RenameSkillSet(string oldName, string newName)
        //{
        //    TicketToolAdmin.RenameSkillSet(oldName, newName);
        //}

        //public static void CreateSkillSet(List<SkillSet> skillsets)
        //{
        //    TicketToolAdmin.CreateSkillSets(skillsets.ToArray());
        //}

        //public static void DeleteSkillSet(string skillSetName)
        //{
        //    TicketToolAdmin.DeleteSkillSet(skillSetName);
        //}

        //public static IEnumerable<User> GetAllUsers()
        //{
        //    return TicketToolAdmin.GetAllUsers();
        //}
        //public static User GetUser(string userName)
        //{
        //    return TicketToolAdmin.GetUser(userName);
        //}

        //public static IEnumerable<string> GetAllRoles()
        //{
        //    return Authentificator.GetAllRoles();
        //}

        //public static IEnumerable<Parameter> GetAllStores()
        //{
        //    return TicketToolAdmin.GetAllParameters();
        //}

        //public static long CreateUser(string login, string password, string email, string[] roles, string[] sapCodes, string[] availableSkillSets)
        //{
        //    return Authentificator.CreateUser(login, password, email, roles, sapCodes, availableSkillSets);
        //}

        //public static bool UpdateUser(bool isLocked, string login, string password, string email, string[] roles, string[] sapCodes, string[] availableSkillSets)
        //{
        //    return Authentificator.UpdateUser(isLocked, login, email, password, roles, sapCodes, availableSkillSets);
        //}

        //public static bool CheckUserName(long id, string name)
        //{
        //    return TicketToolAdmin.CheckUserName(id, name);
        //}

        //public static IEnumerable<SkillSet> GetAvailableSkillSets(string[] roles)
        //{
        //    return TicketToolAdmin.GetAvailableSkillSets(roles);
        //}

    }
}