﻿/// <reference path="moment/moment.d.ts" />
/// <reference path="jquery/jquery.d.ts" />
/// <reference path="react/react-addons-global.d.ts" />
/// <reference path="../rx-lite.d.ts" />

interface JQuery {
    datetimepicker(): JQuery;
    datetimepicker(settings?: any): JQuery;
    destroy(): JQuery;
}

interface DateTimePickerEvent extends JQueryEventObject {
    date: moment.Moment
}