﻿/// <reference path="../typings/_references.d.ts" />
module TicketTool {
    export interface ISkillSet {
        Name: string;
        IsLogged: boolean;
        IsClustered: boolean;
        IsEscalation?: boolean;
        Selected?: boolean;
    }

    export interface ICluster {
        Name: string;
        Parameters: string[];
        IsLogged: boolean;
        Selected?: boolean;
    }

    export interface ISkillSetStore {
        UserSkillSet: ISkillSet[];
        Clusters: ICluster[];
        editMode?: boolean;
        clustersEdit?: boolean;
    }


    var skillData: ISkillSetStore = {
        UserSkillSet: [],
        Clusters: [],
        editMode: true,
        clustersEdit: false
    };

    export var SkillSetStore = new Rx.BehaviorSubject<ISkillSetStore>(skillData);
}