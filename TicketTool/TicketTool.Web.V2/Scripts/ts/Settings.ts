﻿/// <reference path="../typings/_references.d.ts" />

module TicketTool {
    export interface ISettings {
        Root: string;
        PingInterval: number;
    }

    export declare var Settings: ISettings;
}
