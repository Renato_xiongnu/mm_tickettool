﻿/// <reference path="../typings/_references.d.ts" />
module TicketTool {
    export interface ITaskStore {
        Task: ITask;
        PingTask: IPingTask;
        Outcome: ITaskOutcome;
        OperationResult: IOperationResult;
    }

    export interface ITask {
        TaskId: string,
        Name: string,
        Description: string,
        InfoUrl: string,
        ShowInfoUrl: boolean,
        IsEscalated: boolean,
        Outcomes: ITaskOutcome[];
        WorkitemId: string;
        Message?: string;
    }

    export interface ITaskOutcome {
        HaveRequiredFields: boolean,
        ToolTipText: string,
        Value: string,
        RequiredFields: ITaskField[]
    }

    export interface ITaskField {
        Name: string;
        StringType: string;
        BoolValue: boolean;
        DateTimeValue: Date;
        DefaultValue: string;
        ErrorMessage: string;
        PredefinedValuesList: string[];
        TextValue: string;
        TimeSpanDayValue: number;
        TimeSpanHourValue: number;
        TimeSpanMinValue: number;
        TimeSpanFullValue: string;
    }

    export interface IPing {
        Tasks: IPingTask[];
        LoggedOnSkillSets: string[];
        Message?: string;
    }

    export interface IPingTask {
        TaskId: string;
        MaxTimeForUser: string;
        UserTime: string;
    }

    export interface IOperationResult {
        HasError: boolean;
        MessageText: string;
    }

    var taskData: ITaskStore = {
        Task: null,
        PingTask: null,
        Outcome: null,
        OperationResult: null
    }   

    export var TaskStore = new Rx.BehaviorSubject<ITaskStore>(taskData);
}
