/// <reference path="../typings/_references.d.ts" />
var TicketTool;
(function (TicketTool) {
    var taskData = {
        Task: null,
        PingTask: null,
        Outcome: null,
        OperationResult: null
    };
    TicketTool.TaskStore = new Rx.BehaviorSubject(taskData);
})(TicketTool || (TicketTool = {}));
//# sourceMappingURL=TaskStore.js.map