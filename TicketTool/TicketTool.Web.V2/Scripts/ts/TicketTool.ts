﻿/// <reference path="../typings/_references.d.ts" />
/// <reference path="TaskStore.ts" />
/// <reference path="TaskActions.ts" />
/// <reference path="TaskComponents.ts" />
/// <reference path="SkillSetStore.ts" />
/// <reference path="SkillSetActions.ts" />
/// <reference path="SkillSetComponents.ts" />
/// <reference path="Settings.ts" />

module TicketTool {
    TaskActionsSubscribe(TaskStore);

    var ticks = Rx.Observable.timer(0, Settings.PingInterval * 1000)
        .filter(() => !(SkillSetStore.getValue().editMode) && SkillSetStore.getValue().UserSkillSet.some(s => s.IsLogged))
        .map(() => TaskStore.getValue().Task)
        .share();
        
    ticks
        .filter(task => task === null)
        .subscribe(TaskActions.getTask);

    ticks
        .filter(task => task !== null)
        .map(task => task.TaskId)
        .subscribe(TaskActions.pingTask);

    SkillSetActionsSubscribe(SkillSetStore);
    SkillSetActions.getSkillData.onNext({});

    var emptyUrl = "about:blank";

    TaskStore
        .map(value => value.Task)
        .map(task => task !== null ? task.InfoUrl : emptyUrl)
        .distinctUntilChanged()
        .subscribe((url) => {
            var frame: HTMLIFrameElement = <HTMLIFrameElement>document.getElementById("orderFrame");
            if (frame.src !== url) {
                frame.src = url;
            }
        });

    var d = React.DOM;

    export interface IAppProps {
        skillSetStore: ISkillSetStore,
        taskStore: ITaskStore,
    }

    export class TicketToolApp extends React.Component<IAppProps, any> {
        render() {
            var SkillEdit = React.createFactory(SkillSetEdit);
            var SkillView = React.createFactory(SkillSetView);
            var Task = React.createFactory(TaskView);
            var Outcome = React.createFactory(TaskOutcome);
            var editMode = this.props.skillSetStore.editMode;
            var taskStore = this.props.taskStore;

            return (
                d.div({ className: "tt-container" },
                    editMode ? SkillEdit(this.props.skillSetStore) : null,
                    !editMode && taskStore.Outcome === null ? d.div({ className: "skillsets-selected scroll" },
                        d.div({ className: "padding scroll-content" },
                            SkillView(this.props.skillSetStore), 
                            taskStore.Task !== null ? Task({ task: taskStore.Task, ping: taskStore.PingTask }) : null
                            )
                        ) : null,
                    !editMode && taskStore.Task !== null && taskStore.Outcome !== null ? Outcome({ outcome: taskStore.Outcome }) : null,
                    taskStore.OperationResult && taskStore.OperationResult.HasError ? d.p({ className: "alert alert-danger" }, taskStore.OperationResult.MessageText) : null
                )
            );
        }
    };

    var App = React.createFactory(TicketToolApp);

    Rx.Observable
        .combineLatest(TaskStore.asObservable(), SkillSetStore.asObservable(), (tasks, skills) => { return { skillSetStore: skills, taskStore: tasks } })
        .subscribe(
            value => {
                React.render(App(value), document.getElementById("right_box"));
            },
            error => {
                console.log(error);
            }
        );

    //TaskStore.subscribe(
    //    value => {
    //        var skillSetValue = SkillSetStore.getValue();
    //        React.render(App({ skillSetStore: skillSetValue, taskStore: value }), document.getElementById("ticketApp"));
    //    },
    //    error => {
    //        console.log(error);
    //    }
    //);

    //SkillSetStore.subscribe(
    //    value => {
    //        var taskValue = TaskStore.getValue();
    //        React.render(App({ skillSetStore: value, taskStore: taskValue }), document.getElementById("ticketApp"));
    //    },
    //    error => {
    //        console.log(error);
    //    }
    //);
}