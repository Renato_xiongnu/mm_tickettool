/// <reference path="../typings/_references.d.ts" />
var TicketTool;
(function (TicketTool) {
    var skillData = {
        UserSkillSet: [],
        Clusters: [],
        editMode: true,
        clustersEdit: false
    };
    TicketTool.SkillSetStore = new Rx.BehaviorSubject(skillData);
})(TicketTool || (TicketTool = {}));
//# sourceMappingURL=SkillSetStore.js.map