/// <reference path="../typings/_references.d.ts" />
/// <reference path="SkillSetStore.ts" />
/// <reference path="SkillSetActions.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var TicketTool;
(function (TicketTool) {
    var d = React.DOM;
    var SkillSetName = (function (_super) {
        __extends(SkillSetName, _super);
        function SkillSetName() {
            _super.apply(this, arguments);
        }
        SkillSetName.prototype.render = function () {
            var Del = React.createFactory(DelIcon);
            var Clustered = React.createFactory(ClusteredIcon);
            var skillSet = this.props.skillSet;
            var classList = React.addons.classSet({
                "skillsets-call-center": true,
                "skillsets-escalation": skillSet.IsEscalation
            });
            return (d.a({ className: classList, href: "#", onClick: this.props.onClick }, skillSet.IsClustered ? Clustered() : null, skillSet.Name, this.props.canDelete ? d.span({ className: "delete" }, Del()) : null));
        };
        return SkillSetName;
    })(React.Component);
    TicketTool.SkillSetName = SkillSetName;
    ;
    var SkillSetList = (function (_super) {
        __extends(SkillSetList, _super);
        function SkillSetList() {
            _super.apply(this, arguments);
        }
        SkillSetList.prototype.render = function () {
            var _this = this;
            var SkillSet = React.createFactory(SkillSetName);
            var Clusters = React.createFactory(ClusterEdit);
            var editMode = this.props.editMode || false;
            var selectedMode = this.props.selected || false;
            var skillSets = this.props.skillSets;
            var classList = React.addons.classSet({
                "skillsets-list": !selectedMode,
                "skillsets-select": selectedMode,
                "edit": editMode
            });
            var skills = skillSets.map(function (s) {
                var logClick = function () {
                    s.Selected = false;
                    s.IsLogged = true;
                    _this.props.clusters.forEach(function (c) {
                        c.IsLogged = c.Selected;
                        c.Selected = false;
                    });
                    TicketTool.SkillSetActions.changeSkillSet.onNext(s);
                };
                var skillClick = function () {
                    if (!editMode) {
                        return;
                    }
                    if (selectedMode) {
                        s.IsLogged = false;
                    }
                    else {
                        if (s.IsClustered) {
                            s.Selected = !s.Selected;
                        }
                        else {
                            s.Selected = false;
                            s.IsLogged = true;
                        }
                    }
                    _this.props.clusters.forEach(function (c) {
                        c.Selected = c.IsLogged;
                    });
                    TicketTool.SkillSetActions.changeSkillSet.onNext(s);
                };
                var liClass = React.addons.classSet({
                    "js-skillsets": true,
                    "logged": s.IsLogged,
                    "select": s.Selected
                });
                var liStyle = s.Selected && s.IsClustered && !selectedMode ? { marginBottom: (35 + 50 + Math.ceil(_this.props.clusters.length / 2) * 30) + "px" } : {};
                return d.li({ key: s.Name, className: liClass, style: liStyle }, SkillSet({ skillSet: s, onClick: editMode ? skillClick : null, canDelete: editMode && selectedMode }), s.Selected && s.IsClustered && !selectedMode ? Clusters({ Clusters: _this.props.clusters, initialChecked: true, onClick: logClick }) : null);
            });
            return (d.ul({ className: classList }, skills));
        };
        return SkillSetList;
    })(React.Component);
    TicketTool.SkillSetList = SkillSetList;
    ;
    var ClusterEdit = (function (_super) {
        __extends(ClusterEdit, _super);
        function ClusterEdit() {
            _super.apply(this, arguments);
        }
        ClusterEdit.prototype.render = function () {
            var CheckIcon = React.createFactory(CheckBoxIcon);
            var clusters = this.props.Clusters
                .map(function (c) { return d.label({ key: c.Name }, d.span({ className: "form-checkbox" }, d.input({
                type: "checkbox",
                checked: c.Selected,
                onChange: function (e) {
                    c.Selected = !c.Selected;
                    TicketTool.SkillSetActions.changeCluster.onNext(c);
                } }), CheckIcon()), c.Name); });
            return (d.div({ className: "choice" }, d.div({ className: "js-clusters" }, d.fieldset({ id: "clusters_form" }, clusters)), this.props.onClick ? d.button({ className: "js-skillsets-select", disabled: !this.props.Clusters.some(function (c) { return c.Selected; }), onClick: this.props.onClick }, "Выбрать") : null));
        };
        return ClusterEdit;
    })(React.Component);
    TicketTool.ClusterEdit = ClusterEdit;
    var SkillSetEdit = (function (_super) {
        __extends(SkillSetEdit, _super);
        function SkillSetEdit() {
            _super.apply(this, arguments);
        }
        SkillSetEdit.prototype.render = function () {
            var SkillList = React.createFactory(SkillSetList);
            var Clusters = React.createFactory(ClusterEdit);
            var ClustersView = React.createFactory(ClusterList);
            var selectedSkills = this.props.UserSkillSet
                .filter(function (s) { return s.IsLogged; })
                .sort(function (a, b) { return (a.IsEscalation ? 1 : 0) - (b.IsEscalation ? 1 : 0); });
            var callCenterSkills = this.props.UserSkillSet
                .filter(function (s) { return !(s.IsEscalation || false); });
            var escalationSkills = this.props.UserSkillSet
                .filter(function (s) { return (s.IsEscalation || false); });
            return (d.div({ className: "skillsets-selected skillsets-selection scroll", style: { height: "100%" } }, d.div({ className: "padding scroll-content" }, d.div({ className: "selected" }, d.div({ className: "title" }, "Выбранные скиллсеты"), selectedSkills.length > 0
                ? SkillList({ skillSets: selectedSkills, clusters: this.props.Clusters, selected: true, editMode: true })
                : d.span({}, "Выберите скиллсеты для просмотра задач"), ClustersView({ clusters: this.props.Clusters, edit: true, clustersEdit: this.props.clustersEdit })), 
            //selectedSkills.filter(s => s.IsClustered).length > 0
            //    ? Clusters({ Clusters: this.props.Clusters, initialChecked: false, onClick: null })
            //    : null,
            callCenterSkills.length > 0
                ? d.div({ className: "call-center" }, d.div({ className: "title" }, "Call Center"), SkillList({ skillSets: callCenterSkills, clusters: this.props.Clusters, selected: false, editMode: true }))
                : null, escalationSkills.length > 0
                ? d.div({ className: "escalation" }, d.div({ className: "title" }, "Эскалация"), SkillList({ skillSets: escalationSkills, clusters: this.props.Clusters, selected: false, editMode: true }))
                : null), d.div({ className: "buttons" }, d.button({ disabled: (selectedSkills.length === 0), onClick: TicketTool.SkillSetActions.updateSkillSets.onNext.bind(TicketTool.SkillSetActions.updateSkillSets) }, "OK"))));
        };
        return SkillSetEdit;
    })(React.Component);
    TicketTool.SkillSetEdit = SkillSetEdit;
    ;
    var SkillSetView = (function (_super) {
        __extends(SkillSetView, _super);
        function SkillSetView() {
            _super.apply(this, arguments);
        }
        SkillSetView.prototype.render = function () {
            var SkillList = React.createFactory(SkillSetList);
            var ClustersView = React.createFactory(ClusterList);
            var Plus = React.createFactory(PlusIcon);
            var Exit = React.createFactory(ExitIcon);
            var skills = this.props.UserSkillSet
                .filter(function (s) { return s.IsLogged; })
                .sort(function (a, b) { return (a.IsEscalation ? 1 : 0) - (b.IsEscalation ? 1 : 0); });
            //const clusters = this.props.Clusters
            //    .filter(c => c.IsLogged)
            //    .map(c => c.Name).join(" | ");
            var clusters = this.props.Clusters
                .filter(function (c) { return c.IsLogged; })
                .map(function (c) { return d.li({ key: c.Name }, c.Name); });
            return (
            //d.div({ className: "skillsets-selected scroll" },
            //    d.div({ className: "padding scroll-content" },
            d.div({ className: "selected" }, d.div({ className: "title" }, "Выбранные скиллсеты", d.a({ href: "#", className: "plus", onClick: TicketTool.SkillSetActions.editSkillSets.onNext.bind(TicketTool.SkillSetActions.editSkillSets) }, Plus()), d.a({ href: "#", className: "exit", onClick: function (e) { return TicketTool.SkillSetActions.loginSkillSets.onNext({ UserSkillSet: [], Clusters: [] }); } }, "Выйти из скиллсетов", Exit())), SkillList({ skillSets: skills, clusters: [], selected: true, editMode: false }), ClustersView({ clusters: this.props.Clusters, edit: false })));
        };
        return SkillSetView;
    })(React.Component);
    TicketTool.SkillSetView = SkillSetView;
    ;
    var ClusterList = (function (_super) {
        __extends(ClusterList, _super);
        function ClusterList() {
            _super.apply(this, arguments);
        }
        ClusterList.prototype.render = function () {
            var ClustersEdit = React.createFactory(ClusterEdit);
            var edit = this.props.edit;
            var clustersEdit = this.props.clustersEdit;
            var clusters = this.props.clusters
                .filter(function (c) { return c.IsLogged; })
                .map(function (c) { return d.li({ key: c.Name }, c.Name); });
            var list = d.ul({ className: "clusters show" }, clusters);
            var classList = React.addons.classSet({
                "select": clustersEdit
            });
            var aStyle = clustersEdit ? { marginBottom: (55 + Math.ceil(this.props.clusters.length / 2) * 30) + "px" } : {};
            return (clusters.length > 0
                ? d.div({ className: "clusters show", id: "clusters_selected" }, d.div({ className: "title" }, "Выбранные кластеры"), edit ? d.div({ className: classList, style: aStyle }, d.a({ href: "#", onClick: function (e) { return TicketTool.SkillSetActions.editClusters.onNext(!clustersEdit); } }, list), clustersEdit ? ClustersEdit({ Clusters: this.props.clusters, initialChecked: true, onClick: function (e) { return TicketTool.SkillSetActions.changeClusters.onNext({}); } }) : null) : list)
                : null);
        };
        return ClusterList;
    })(React.Component);
    TicketTool.ClusterList = ClusterList;
    var CheckBoxIcon = (function (_super) {
        __extends(CheckBoxIcon, _super);
        function CheckBoxIcon() {
            _super.apply(this, arguments);
        }
        CheckBoxIcon.prototype.render = function () {
            return (React.createElement("svg", { className: "svg", xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 50 50", width: "16px", height: "16px" }, React.createElement("path", { className: "bg", fill: "#FF0099", "fill-rule": "evenodd", "clip-rule": "evenodd", d: "M6.25,0h37.5C47.202,0,50,2.799,50,6.25v37.5c0,3.452-2.798,6.25-6.25,6.25H6.25C2.798,50,0,47.202,0,43.75V6.25C0,2.799,2.798,0,6.25,0z" }), React.createElement("path", { className: "brd", fill: "#FF0099", d: "M43.75,3C45.542,3,47,4.458,47,6.25v37.5c0,1.792-1.458,3.25-3.25,3.25H6.25C4.458,47,3,45.542,3,43.75V6.25C3,4.458,4.458,3,6.25,3H43.75 M43.75,0H6.25C2.798,0,0,2.799,0,6.25v37.5C0,47.202,2.798,50,6.25,50h37.5c3.452,0,6.25-2.798,6.25-6.25V6.25C50,2.799,47.202,0,43.75,0L43.75,0z" }), React.createElement("path", { className: "ico", fill: "#FFFFFF", "fill-rule": "evenodd", "clip-rule": "evenodd", d: "M43.135,16.351L25.458,34.029h0l-4.42,4.42L9.99,27.4l4.419-4.42l6.629,6.629l17.678-17.678L43.135,16.351z" })));
        };
        return CheckBoxIcon;
    })(React.Component);
    TicketTool.CheckBoxIcon = CheckBoxIcon;
    ;
    var PlusIcon = (function (_super) {
        __extends(PlusIcon, _super);
        function PlusIcon() {
            _super.apply(this, arguments);
        }
        PlusIcon.prototype.render = function () {
            return (React.createElement("svg", { className: "svg", xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 50 50", width: "16px", height: "16px" }, React.createElement("path", { className: "minus", fill: "#96999F", "fill-rule": "evenodd", "clip-rule": "evenodd", d: "M0,21.429h50v7.142H0V21.429z" }), React.createElement("path", { className: "plus", fill: "#96999F", "fill-rule": "evenodd", "clip-rule": "evenodd", d: "M50,28.571H28.572V50h-7.143V28.571H0.001v-7.142h21.428V0h7.143v21.428H50V28.571z" })));
        };
        return PlusIcon;
    })(React.Component);
    TicketTool.PlusIcon = PlusIcon;
    ;
    var ExitIcon = (function (_super) {
        __extends(ExitIcon, _super);
        function ExitIcon() {
            _super.apply(this, arguments);
        }
        ExitIcon.prototype.render = function () {
            return (React.createElement("svg", { className: "svg", xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 50 50", width: "16px", height: "16px" }, React.createElement("path", { className: "ico", fill: "#A4A9B3", "fill-rule": "evenodd", "clip-rule": "evenodd", d: "M50,25l-12.5,12.5v-9.375H15.625v-6.25H37.5V12.5L50,25z M28.126,6.251H6.251V43.75h21.874v-6.25h6.249v12.5H0V0.001h34.375v12.5h-6.249V6.251z" })));
        };
        return ExitIcon;
    })(React.Component);
    TicketTool.ExitIcon = ExitIcon;
    ;
    var DelIcon = (function (_super) {
        __extends(DelIcon, _super);
        function DelIcon() {
            _super.apply(this, arguments);
        }
        DelIcon.prototype.render = function () {
            return (React.createElement("svg", { className: "svg", xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 50 50", width: "10px", height: "10px" }, React.createElement("path", { className: "ico", "fill-rule": "evenodd", "clip-rule": "evenodd", fill: "#ffffff", d: "M50,43.751L43.751,50L25,31.25L6.25,50L0,43.751L18.75,25L0,6.25L6.25,0L25,18.75L43.751,0L50,6.25L31.25,25L50,43.751z" })));
        };
        return DelIcon;
    })(React.Component);
    TicketTool.DelIcon = DelIcon;
    ;
    var ClusteredIcon = (function (_super) {
        __extends(ClusteredIcon, _super);
        function ClusteredIcon() {
            _super.apply(this, arguments);
        }
        ClusteredIcon.prototype.render = function () {
            return (React.createElement("svg", { className: "svg", xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 50 50", width: "16px", height: "16px" }, React.createElement("path", { className: "ico", fill: "#FF0099", "fill-rule": "evenodd", "clip-rule": "evenodd", d: "M28.125,50V28.125H50V50H28.125z M46.875,31.25H31.25v15.625h15.625V31.25z M28.125,0H50v21.875H28.125V0z M31.25,18.75h15.625V3.125H31.25V18.75z M0,28.125h21.875V50H0V28.125z M3.125,46.875H18.75V31.25H3.125V46.875z M0,0h21.875v21.875H0V0z M3.125,18.75H18.75V3.125H3.125V18.75z" })));
        };
        return ClusteredIcon;
    })(React.Component);
    TicketTool.ClusteredIcon = ClusteredIcon;
    ;
})(TicketTool || (TicketTool = {}));
//# sourceMappingURL=SkillSetComponents.js.map