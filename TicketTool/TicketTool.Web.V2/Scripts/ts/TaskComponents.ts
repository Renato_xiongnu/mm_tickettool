﻿/// <reference path="../typings/_references.d.ts" />
/// <reference path="TaskStore.ts" />
/// <reference path="TaskActions.ts" />
module TicketTool {
    var d = React.DOM;

    export interface IOutcomeProps {
        index: number;
        outcome: ITaskOutcome;
    }

    export interface ITaskProps {
        task: ITask;
        ping: IPingTask;
    }

    export interface IFieldProps {
        index: number;
        field: ITaskField;
        outcome: ITaskOutcome;
    }

    export class TaskView extends React.Component<ITaskProps, any> {
        render() {
            var task = this.props.task;
            if (task === null) {
                return null;
            }
            var ping = this.props.ping || { TaskId: task.TaskId, UserTime: "00:00:00", MaxTimeForUser: "00:20:00" };

            const TaskButton = React.createFactory(TaskOutcomeButton);
            const Time = React.createFactory(TimeTracker);
            var buttons = task.Outcomes.map((o, index) => d.li({ key: index }, TaskButton({ index: index, outcome: o })));
            return (
                d.div({ className: "basket-options" },
                    //d.a({ className: "btn btn-success", style: { width: "100%" }, onClick: (e) => TaskActions.postponeTask.onNext(1) }, "Отложить"),                    
                    Time({ UserTime: ping.UserTime, MaxTimeForUser: ping.MaxTimeForUser }),
                    d.div({ className: "title", style: { margin: "20px 0 5px 0" } }, task.Name),
                    d.div({ className: "note", style: { margin: "0 0 10px 0" } }, task.WorkitemId),
                    d.div({ className: "note", dangerouslySetInnerHTML: { __html: task.Description } }),
                    d.ul({ className: "basket-options-menu" },
                        buttons
                        )
                    )
                );
        }
    };

    export interface ITimeTrackerProps {
        UserTime: string;
        MaxTimeForUser: string;
    }

    export class TimeTracker extends React.Component<ITimeTrackerProps, any> {
        render() {
            var currentTime = moment.duration(this.props.UserTime);
            var maxTime = moment.duration(this.props.MaxTimeForUser);
            var percent = Math.round((currentTime.asSeconds() / maxTime.asSeconds()) * 100);
            var style = {                
                left: Math.min(percent, 100) + "%"
            };

            var classList = React.addons.classSet({
                "tt-task-time-green": percent < 50,
                "tt-task-time-yellow": percent >= 50 && percent < 75,
                "tt-task-time-red": percent >= 75,
            });

            return (
                d.div({ className: "timer" },                    
                    d.div({ className: "timeline" },
                        d.div({},
                            d.span({ className: classList, style: style }, moment.utc(currentTime.asMilliseconds()).format("mm:ss"))
                        )
                    ),
                    d.span({ className: "time" }, maxTime.asMinutes() + " мин")
                )
            );
        }
    }

    export class TaskOutcomeButton extends React.Component<IOutcomeProps, any> {
        render() {
            const OutcomeMore = React.createFactory(OutcomeIcon);

            var outcome = this.props.outcome;
            var classList = React.addons.classSet({
                "tt-task-outcome": true,
                "tt-task-outcome__fields": outcome.HaveRequiredFields,
            });
            var btnClass = React.addons.classSet({
                "btn": true,
                "btn-success": outcome.HaveRequiredFields,
                "btn-info": !outcome.HaveRequiredFields,
            });
            return (
                d.a({ href: "#", onClick: (e) => TaskActions.setOutcome.onNext(outcome) },
                    outcome.Value,
                    outcome.HaveRequiredFields ?  OutcomeMore() : null
                    )
                );
        }
    };

    export class TaskOutcome extends React.Component<IOutcomeProps, any> {
        constructor(props) {
            super(props);
            moment.locale("ru");

            props.outcome.RequiredFields.forEach((field: ITaskField) => {
                field.TextValue = field.TextValue === null ? field.DefaultValue : field.TextValue;
            });
        }

        validate() {
            return this.props.outcome.RequiredFields
                .every(f => f.TextValue !== null && f.TextValue !== "");
        }

        render() {            
            const TextField = React.createFactory(TaskTextField);
            const ChoiceField = React.createFactory(TaskChoiceField);
            const DateField = React.createFactory(TaskDateField);
            const BooleanField = React.createFactory(TaskBooleanField);
            const TimeSpanField = React.createFactory(TaskTimeSpanField);
            const Back = React.createFactory(BackIcon);
            var outcome = this.props.outcome;

            var fields = outcome.RequiredFields.map((field, index) => {
                switch (field.StringType) {
                    case "Choice":
                        return ChoiceField({ key: index, index: index, field: field, outcome: outcome });
                    case "DateTime":
                    case "System.DateTime":
                        return DateField({ key: index, index: index, field: field, outcome: outcome });
                    case "Boolean":
                    case "System.Boolean":
                        return BooleanField({ key: index, index: index, field: field, outcome: outcome });
                    case "TimeSpan":
                    case "System.TimeSpan":
                        return TimeSpanField({ key: index, index: index, field: field, outcome: outcome });
                    case "System.String":
                    case "System.Int32":
                    case "System.Decimal":
                    case "System.Double":
                    default:
                        return TextField({ key: index, index: index, field: field, outcome: outcome });
                }
            });

            return (
                d.div({ className: "call-back scroll" },
                    d.div({ className: "padding scroll-content" },
                        d.div({ className: "title" },
                            d.a({ href: "#", onClick: (e) => TaskActions.setOutcome.onNext(null)}, Back()),
                            outcome.Value
                        ),
                        d.div({}, fields)                        
                        ),
                    d.div({ className: "buttons" },
                        d.button({
                            disabled: !this.validate(), onClick: (e) => {
                                e.preventDefault();
                                TaskActions.completeTask.onNext(outcome);
                            }
                        }, "OK"),
                        d.button({ onClick: (e) => TaskActions.setOutcome.onNext(null) }, "Отмена")
                        )
                    )
                );
        }
    };

    export class TaskTextField extends React.Component<IFieldProps, any> {
        constructor(props) {
            super(props);
            var field = props.field;
            field.TextValue = field.DefaultValue;
        }

        setValue(value: string) {
            var outcome = this.props.outcome;
            var field = this.props.field;
            field.TextValue = value;
            TaskActions.setOutcome.onNext(outcome);
        }

        render() {
            var setState = this.setState.bind(this);
            var field = this.props.field;
            return (
                d.div({ className: "tt-task-field" },
                    d.div({ className: "note" }, field.Name),
                    d.div({ className: "form-group" },
                        d.input({ type: "text", className: "form-control", name: field.Name, value: field.TextValue, onChange: e => { this.setValue((<HTMLInputElement>e.target).value); } })
                        )
                    )
                );
        }
    };

    export class TaskChoiceField extends React.Component<IFieldProps, any> {
        constructor(props) {
            super(props);
            var field = props.field;
            field.TextValue = field.DefaultValue;
        }

        setValue(value: string) {
            var outcome = this.props.outcome;
            var field = this.props.field;
            field.TextValue = value;
            TaskActions.setOutcome.onNext(outcome);
        }

        render() {
            const Radio = React.createFactory(RadioIcon);

            var outcome = this.props.outcome;
            var field = this.props.field;
            var values = field.PredefinedValuesList.map((s, index) => {
                return d.li({ key: index },
                    d.label({},
                        d.var({ className: "form-radio" },
                            d.input({ type: "radio", name: field.Name, checked: (s === field.TextValue), onChange: e => { if ((<HTMLInputElement>e.target).checked) { this.setValue(s); } } }),
                            Radio()
                            ),
                        s
                        )
                    );
            });
            return (
                d.div({ className: "user-rejected" },
                    d.div({ className: "note" }, field.Name),
                    d.ul({ className: "rejected-list" }, values)
                    )
                );
        }
    };

    export class TaskDateField extends React.Component<IFieldProps, any> {
        constructor(props) {            
            super(props);

            var field = props.field;
            var m = moment(field.DefaultValue, "DD.MM.YYYY HH:mm:ss")
            field.DateTimeValue = m.isValid() ? m.toDate() : null;
        }

        setValue(value: moment.Moment) {
            var outcome = this.props.outcome;
            var field = this.props.field;
            field.DateTimeValue = value && value.isValid() ? new Date(value.format("YYYY-MM-DDT00:00:00") + "Z") : null;
            field.TextValue = value && value.isValid() ? value.format("YYYY-MM-DDT00:00:00") + "Z" : null;
            TaskActions.setOutcome.onNext(outcome);
        }

        componentDidMount() {
            var el = React.findDOMNode(this.refs["datepicker"]);
            $(el).datetimepicker({ format: "DD.MM.YYYY", locale: "ru", defaultDate: this.props.field.DateTimeValue });
            $(el).on("dp.change", (e) => {
                var m = (<DateTimePickerEvent>e).date;
                this.setValue(m);
            });
        }

        render() {
            var setState = this.setState.bind(this);
            var field = this.props.field;
            return (
                d.div({ className: "tt-task-field" },
                    d.div({ className: "note" }, field.Name),
                    d.div({ className: "form-group" },
                        d.div({ className: "input-group date", ref: "datepicker" },
                            d.input({ type: "datetime", className: "form-control datepicker", name: field.Name }), //, value: field.DateTimeValue.toString(), onChange: e => { this.setValue(new Date(Date.parse((<HTMLInputElement>e.target).value))); } 
                            d.span({ className: "input-group-addon" }, d.span({ className: "glyphicon glyphicon-calendar" }))
                            )
                        )
                    )
                );
        }

        componentWillUnmount() {
            var el = React.findDOMNode(this.refs["datepicker"]);
            $(el).data("DateTimePicker").destroy();
        }
    };

    export class TaskBooleanField extends React.Component<IFieldProps, any> {
        constructor(props) {
            super(props);
            var field = props.field;
            field.BoolValue = (field.DefaultValue || "").toLowerCase() === "true";
        }

        setValue(value: boolean) {
            var outcome = this.props.outcome;
            var field = this.props.field;
            field.BoolValue = value;
            field.TextValue = value.toString();
            TaskActions.setOutcome.onNext(outcome);
        }

        render() {
            const CheckBox = React.createFactory(CheckBoxIcon);

            var setState = this.setState.bind(this);
            var field = this.props.field;
            return (
                d.div({ className: "tt-task-field" },
                    d.div({ className: "checkbox" },
                        d.label({},
                            d.var({ className: "form-checkbox" },
                                d.input({ type: "checkbox", name: field.Name, checked: field.BoolValue, onChange: e => { this.setValue((<HTMLInputElement>e.target).checked); } }),
                                CheckBox()
                            ),
                            field.Name
                            )
                        )
                    )
                );
        }
    };

    export class TaskTimeSpanField extends React.Component<IFieldProps, any> {
        constructor(props) {
            super(props);

            var field = props.field;
            var d = moment.duration(field.DefaultValue);
            field.TimeSpanDayValue = d.days();
            field.TimeSpanHourValue = d.hours();
            field.TimeSpanMinValue = d.minutes();
            field.TimeSpanFullValue = d.asMilliseconds() > 0 ? d.days() + "." + moment.utc(d.asMilliseconds()).format("HH:mm:ss") : null;
        }

        setValue(name: string, value: number) {
            var outcome = this.props.outcome;
            var field = this.props.field;
            field[name] = value;
            var m = moment.duration({ days: field.TimeSpanDayValue, hours: field.TimeSpanHourValue, minutes: field.TimeSpanMinValue });
            var ts = m.asMilliseconds() > 0 ? m.days() + "." + moment.utc(m.asMilliseconds()).format("HH:mm:ss") : null;
            field.TextValue = ts;
            field.TimeSpanFullValue = ts;
            TaskActions.setOutcome.onNext(outcome);
        }

        getValues(lowEnd: number, highEnd: number, step: number = 1) {
            var list = [];
            for (var i = lowEnd; i <= highEnd; i += step) {
                list.push(i);
            }
            return list;
        }

        getHourUnits(n: number) {
            switch (true) {
                case n === 1 || n === 21: return n + " час";
                case (n > 1 && n < 5) || (n > 21 && n < 25): return n + " часа";
                default: return n + " часов";
            }
        }

        getDayUnits(n: number) {
            switch (true) {
                case n === 1 || n === 21 || n == 31: return n + " день";
                case (n > 1 && n < 5) || (n > 21 && n < 25): return n + " дня";
                default: return n + " дней";
            }
        }

        render() {
            var setState = this.setState.bind(this);
            var field = this.props.field;
            return (
                d.div({ className: "tt-task-field" },
                    d.p({}, field.Name),
                    d.div({ className: "form-group", style: { textAlign: "center" } },
                        d.select({ className: "form-control", style: { width: "30%", margin: "0 4px", display: "inline-block" }, value: field.TimeSpanDayValue.toString(), onChange: e => { this.setValue("TimeSpanDayValue", parseInt((<HTMLInputElement>e.target).value)); } },
                            this.getValues(0, 31).map(i => d.option({ key: i, value: i }, this.getDayUnits(i)))
                            ),
                        d.select({ className: "form-control", style: { width: "30%", margin: "0 4px", display: "inline-block" }, value: field.TimeSpanHourValue.toString(), onChange: e => { this.setValue("TimeSpanHourValue", parseInt((<HTMLInputElement>e.target).value)); } },
                            this.getValues(0, 23).map(i => d.option({ key: i, value: i }, this.getHourUnits(i)))
                            ),
                        d.select({ className: "form-control", style: { width: "30%", margin: "0 4px", display: "inline-block" }, value: field.TimeSpanMinValue.toString(), onChange: e => { this.setValue("TimeSpanMinValue", parseInt((<HTMLInputElement>e.target).value)); } },
                            this.getValues(0, 55, 5).map(i => d.option({ key: i, value: i }, i + " минут"))
                            )
                        )
                    )
                );
        }
    };

    export class OutcomeIcon extends React.Component<any, any> {
        render() {
            return (
                React.createElement("svg", { className: "svg", xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 50 50", width: "16px", height: "16px" },
                    React.createElement("path", { className: "right", style: { display: "none" }, fill: "#FF0099", "fill-rule": "evenodd", "clip-rule": "evenodd", d: "M40,25l-4.998,5l-0.001-0.001L15.001,50L10,45l20.001-20L10,5l5.001-5l20,20.001l0.001,0L40,25z" }),
                    React.createElement("path", { className: "left", style: { display: "none" }, fill: "#FF0099", "fill-rule": "evenodd", "clip-rule": "evenodd", d: "M14.998,20L14.998,20L34.999,0L40,5L19.998,25L40,45l-5.001,5l-20-20.001L14.998,30L10,25L14.998,20z" }),
                    React.createElement("path", { className: "down", style: { display: "none" }, fill: "#FF0099", "fill-rule": "evenodd", "clip-rule": "evenodd", d: "M20,35.002L20,35.002L0,15.001L5,10l20,20.001L45,10l5,5.001l-20.001,20L30,35.002L25,40L20,35.002z" }),
                    React.createElement("path", { className: "up", style: { display: "none" }, fill: "#FF0099", "fill-rule": "evenodd", "clip-rule": "evenodd", d: "M30,14.998L30,14.998l20,20.001L45,40L25,19.998L5,40l-5-5.001l20.001-20L20,14.998L25,10L30,14.998z" })
                    )
                );
        }
    };

    export class BackIcon extends React.Component<any, any> {
        render() {
            return (
                React.createElement("svg", { className: "svg", xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 50 50", width: "32px", height: "32px" },
                    React.createElement("path", { className: "ico", fill: "#FF0099", "fill-rule": "evenodd", "clip-rule": "evenodd", d: "M25,50C11.192,50,0,38.808,0,25C0,11.193,11.192,0,25,0c13.807,0,25,11.193,25,25C50,38.808,38.807,50,25,50z M25,3.125C12.938,3.125,3.125,12.938,3.125,25c0,12.062,9.813,21.924,21.875,21.924c12.062,0,21.875-9.862,21.875-21.924C46.875,12.938,37.062,3.125,25,3.125z M35.953,26.546H18.255l5.75,5.749c0.61,0.61,0.61,1.599,0,2.21C23.7,34.81,23.3,34.963,22.9,34.963c-0.4,0-0.799-0.153-1.105-0.458l-9.229-9.473l9.229-9.473c0.61-0.61,1.599-0.61,2.209,0c0.61,0.61,0.61,1.599,0,2.21l-5.653,5.653h17.6c0.864,0,1.562,0.7,1.562,1.562C37.516,25.846,36.817,26.546,35.953,26.546z" })
                    )
                );
        }
    };

    export class RadioIcon extends React.Component<any, any> {
        render() {
            return (
                React.createElement("svg", { className: "svg", xmlns: "http://www.w3.org/2000/svg", viewBox: "0 0 50 50", width: "20px", height: "20px" },
                    React.createElement("path", { className: "bg", fill: "#FFFFFF", "fill-rule": "evenodd", "clip-rule": "evenodd", d: "M25,0c13.807,0,25,11.193,25,25c0,13.807-11.193,25-25,25C11.193,50,0,38.807,0,25C0,11.193,11.193,0,25,0z" }),
                    React.createElement("path", { className: "brd", fill: "#E2E2E2", d: "M25,2c12.682,0,23,10.318,23,23S37.682,48,25,48C12.318,48,2,37.682,2,25S12.318,2,25,2 M25,0C11.193,0,0,11.193,0,25c0,13.807,11.193,25,25,25c13.807,0,25-11.193,25-25C50,11.193,38.807,0,25,0L25,0z" }),
                    React.createElement("path", { className: "ico", fill: "#FF0099", "fill-rule": "evenodd", "clip-rule": "evenodd", d: "M25,10c8.284,0,15,6.716,15,15s-6.716,15-15,15c-8.284,0-15-6.716-15-15S16.716,10,25,10z" })
                    )
                );
        }
    };
}