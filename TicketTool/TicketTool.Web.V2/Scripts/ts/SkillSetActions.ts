﻿/// <reference path="../typings/_references.d.ts" />
/// <reference path="SkillSetStore.ts" />
/// <reference path="Settings.ts" />

module TicketTool {
    function GetSkillSet() {
        return $.ajax({
            url: Settings.Root + "/api/SkillSet/",
            dataType: "json",
            data: {}
        });
    }

    function LoginSkillSet(data) {
        return $.ajax({
            url: Settings.Root + "/api/SkillSet/",
            method: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(data)
        });
    }

    export var SkillSetActions = {
        getSkillData: new Rx.Subject<any>(),
        setSkillData: new Rx.Subject<ISkillSetStore>(),
        changeSkillSet: new Rx.Subject<ISkillSet>(),
        changeCluster: new Rx.Subject<ICluster>(),
        updateSkillSets: new Rx.Subject<any>(),
        loginSkillSets: new Rx.Subject<ISkillSetStore>(),
        editSkillSets: new Rx.Subject<any>(),
        editClusters: new Rx.Subject<boolean>(),
        changeClusters: new Rx.Subject<any>(),
    }

    export function SkillSetActionsSubscribe(store: Rx.BehaviorSubject<ISkillSetStore>) {
        SkillSetActions.getSkillData
            .flatMapLatest(() => {
                return Rx.Observable.fromPromise(GetSkillSet()).catch(e => Rx.Observable.empty());
            })
            .subscribe(SkillSetActions.setSkillData);

        SkillSetActions.setSkillData
            .map((value) => {
                var edit = value.UserSkillSet.every(s => !s.IsLogged);
                value.editMode = edit;
                return value;
            })
            .subscribe(store);

        SkillSetActions.changeSkillSet
            .map(skillSet => {
                var value = store.getValue();
                value.UserSkillSet.filter(ss => ss.Name != skillSet.Name).forEach(ss => {
                    ss.Selected = false;
                });
                if (skillSet.IsClustered && skillSet.Selected && value.Clusters.every(s => !s.IsLogged)) {
                    value.Clusters.forEach(c => c.Selected = true);
                }
                if (!value.UserSkillSet.some(s => s.IsLogged && s.IsClustered)) {
                    value.Clusters.forEach(c => c.IsLogged = false);
                }
                value.clustersEdit = false;
                return value;
            })
            .subscribe(store);

        SkillSetActions.changeCluster
            .map(cluster => {
                var value = store.getValue();
                //var index = value.Clusters.indexOf(cluster);
                //value.Clusters[index].IsLogged = !value.Clusters[index].IsLogged;
                return value;
            })
            .subscribe(store);

        SkillSetActions.updateSkillSets
            .map(() => {
                var value = store.getValue();
                var data: ISkillSetStore = {
                    UserSkillSet: value.UserSkillSet.filter(s => s.IsLogged),
                    Clusters: value.Clusters.filter(c => c.IsLogged)
                };
                return data;
            })
            .subscribe(SkillSetActions.loginSkillSets);

        SkillSetActions.loginSkillSets
            .do(TaskActions.resetTask)
            .flatMapLatest((data) => {
                return Rx.Observable.fromPromise(LoginSkillSet(data)).catch(e => Rx.Observable.empty());
            })
            .subscribe(SkillSetActions.setSkillData);

        SkillSetActions.editSkillSets
            .do(TaskActions.resetTask)
            .map(() => {
                var value = store.getValue();
                value.editMode = true;
                return value;
            })
            .subscribe(store);

        SkillSetActions.editClusters
            .map((b) => {
                var value = store.getValue();
                value.UserSkillSet.forEach(ss => {
                    ss.Selected = false;
                });
                value.Clusters.forEach(c => c.Selected = c.IsLogged);
                if (!value.UserSkillSet.some(s => s.IsLogged && s.IsClustered)) {
                    value.Clusters.forEach(c => c.IsLogged = false);
                }
                value.clustersEdit = b;
                return value;
            })
            .subscribe(store);

        SkillSetActions.changeClusters.map(() => {
            var value = store.getValue();
            value.Clusters.forEach(c => c.IsLogged = c.Selected);
            value.clustersEdit = false;
            return value;
        }).subscribe(store);
    }
};