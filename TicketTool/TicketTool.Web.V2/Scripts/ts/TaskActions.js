/// <reference path="../typings/_references.d.ts" />
/// <reference path="TaskStore.ts" />
/// <reference path="Settings.ts" />
var TicketTool;
(function (TicketTool) {
    function GetTask() {
        return $.ajax({
            url: TicketTool.Settings.Root + "/api/Task/",
            dataType: "json",
            data: {}
        });
    }
    function GetTaskPulse(taskId) {
        return $.ajax({
            url: TicketTool.Settings.Root + "/api/Task/" + taskId,
            dataType: "json",
            data: {}
        });
    }
    function CompleteTask(taskId, outcome) {
        return $.ajax({
            url: TicketTool.Settings.Root + "/api/Task/" + taskId,
            dataType: "json",
            contentType: "application/json",
            method: "POST",
            data: JSON.stringify(outcome)
        });
    }
    function PostponeTask(taskId, minutes) {
        return $.ajax({
            url: TicketTool.Settings.Root + "/api/Task/" + taskId + "/Postpone",
            dataType: "json",
            contentType: "application/json",
            method: "POST",
            data: JSON.stringify(minutes)
        });
    }
    TicketTool.TaskActions = {
        getTask: new Rx.Subject(),
        pingTask: new Rx.Subject(),
        setOutcome: new Rx.Subject(),
        completeTask: new Rx.Subject(),
        postponeTask: new Rx.Subject(),
        resetTask: new Rx.Subject(),
    };
    function TaskActionsSubscribe(store) {
        var taskStream = TicketTool.TaskActions.getTask
            .flatMapLatest(function () {
            return Rx.Observable.fromPromise(GetTask()).catch(function (e) { return Rx.Observable.empty(); });
        })
            .share();
        taskStream
            .filter(function (task) { return task != null && !task.Message; })
            .map(function (task) {
            var value = store.getValue();
            value.Task = task;
            value.PingTask = null;
            return value;
        })
            .subscribe(store);
        taskStream
            .filter(function (task) { return task != null && !!(task.Message); })
            .subscribe(function (task) {
            console.log(task.Message);
            window.location.href = TicketTool.Settings.Root + "/Account/Login";
        });
        var pingStream = TicketTool.TaskActions.pingTask
            .flatMapLatest(function (taskId) {
            return Rx.Observable.fromPromise(GetTaskPulse(taskId)).catch(function (e) { return Rx.Observable.empty(); });
        })
            .share();
        pingStream
            .filter(function (ping) { return ping != null && !ping.Message; })
            .map(function (ping) {
            var value = store.getValue();
            if (value.Task != null && ping.Tasks.filter(function (t) { return t.TaskId === value.Task.TaskId; }).length > 0) {
                value.PingTask = ping.Tasks.filter(function (t) { return t.TaskId === value.Task.TaskId; })[0];
            }
            else {
                value.Task = null;
                value.PingTask = null;
            }
            return value;
        })
            .subscribe(store);
        pingStream
            .filter(function (ping) { return ping != null && !!(ping.Message); })
            .subscribe(function (ping) {
            console.log(ping.Message);
            window.location.href = TicketTool.Settings.Root + "/Account/Login";
        });
        TicketTool.TaskActions.setOutcome
            .filter(function (o) { return o != null && !o.HaveRequiredFields; })
            .subscribe(TicketTool.TaskActions.completeTask);
        TicketTool.TaskActions.setOutcome
            .filter(function (o) { return o === null || o.HaveRequiredFields; })
            .map(function (o) {
            var value = store.getValue();
            value.Outcome = o;
            return value;
        })
            .subscribe(store);
        var comleteResult = TicketTool.TaskActions.completeTask
            .flatMapLatest(function (outcome) {
            var value = store.getValue();
            var taskId = value.Task.TaskId;
            console.log(outcome);
            return Rx.Observable.fromPromise(CompleteTask(taskId, outcome)).catch(function (e) { return Rx.Observable.empty(); });
            //return Rx.Observable.empty<IOperationResult>();
        });
        var postponeResult = TicketTool.TaskActions.postponeTask
            .flatMapLatest(function (m) {
            var value = store.getValue();
            var taskId = value.Task.TaskId;
            return Rx.Observable.fromPromise(PostponeTask(taskId, m)).catch(function (e) { return Rx.Observable.empty(); });
        });
        var results = Rx.Observable.merge(comleteResult, postponeResult).share();
        results.filter(function (r) { return !r.HasError; })
            .do(function (r) {
            var value = store.getValue();
            value.OperationResult = r;
        })
            .subscribe(TicketTool.TaskActions.resetTask);
        results.filter(function (r) { return r.HasError; })
            .map(function (r) {
            var value = store.getValue();
            value.OperationResult = r;
            return value;
        })
            .subscribe(store);
        TicketTool.TaskActions.resetTask
            .map(function () {
            var value = store.getValue();
            value.Task = null;
            value.PingTask = null;
            value.Outcome = null;
            return value;
        })
            .subscribe(store);
    }
    TicketTool.TaskActionsSubscribe = TaskActionsSubscribe;
})(TicketTool || (TicketTool = {}));
//# sourceMappingURL=TaskActions.js.map