/// <reference path="../typings/_references.d.ts" />
/// <reference path="SkillSetStore.ts" />
/// <reference path="Settings.ts" />
var TicketTool;
(function (TicketTool) {
    function GetSkillSet() {
        return $.ajax({
            url: TicketTool.Settings.Root + "/api/SkillSet/",
            dataType: "json",
            data: {}
        });
    }
    function LoginSkillSet(data) {
        return $.ajax({
            url: TicketTool.Settings.Root + "/api/SkillSet/",
            method: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(data)
        });
    }
    TicketTool.SkillSetActions = {
        getSkillData: new Rx.Subject(),
        setSkillData: new Rx.Subject(),
        changeSkillSet: new Rx.Subject(),
        changeCluster: new Rx.Subject(),
        updateSkillSets: new Rx.Subject(),
        loginSkillSets: new Rx.Subject(),
        editSkillSets: new Rx.Subject(),
        editClusters: new Rx.Subject(),
        changeClusters: new Rx.Subject(),
    };
    function SkillSetActionsSubscribe(store) {
        TicketTool.SkillSetActions.getSkillData
            .flatMapLatest(function () {
            return Rx.Observable.fromPromise(GetSkillSet()).catch(function (e) { return Rx.Observable.empty(); });
        })
            .subscribe(TicketTool.SkillSetActions.setSkillData);
        TicketTool.SkillSetActions.setSkillData
            .map(function (value) {
            var edit = value.UserSkillSet.every(function (s) { return !s.IsLogged; });
            value.editMode = edit;
            return value;
        })
            .subscribe(store);
        TicketTool.SkillSetActions.changeSkillSet
            .map(function (skillSet) {
            var value = store.getValue();
            value.UserSkillSet.filter(function (ss) { return ss.Name != skillSet.Name; }).forEach(function (ss) {
                ss.Selected = false;
            });
            if (skillSet.IsClustered && skillSet.Selected && value.Clusters.every(function (s) { return !s.IsLogged; })) {
                value.Clusters.forEach(function (c) { return c.Selected = true; });
            }
            if (!value.UserSkillSet.some(function (s) { return s.IsLogged && s.IsClustered; })) {
                value.Clusters.forEach(function (c) { return c.IsLogged = false; });
            }
            value.clustersEdit = false;
            return value;
        })
            .subscribe(store);
        TicketTool.SkillSetActions.changeCluster
            .map(function (cluster) {
            var value = store.getValue();
            //var index = value.Clusters.indexOf(cluster);
            //value.Clusters[index].IsLogged = !value.Clusters[index].IsLogged;
            return value;
        })
            .subscribe(store);
        TicketTool.SkillSetActions.updateSkillSets
            .map(function () {
            var value = store.getValue();
            var data = {
                UserSkillSet: value.UserSkillSet.filter(function (s) { return s.IsLogged; }),
                Clusters: value.Clusters.filter(function (c) { return c.IsLogged; })
            };
            return data;
        })
            .subscribe(TicketTool.SkillSetActions.loginSkillSets);
        TicketTool.SkillSetActions.loginSkillSets
            .do(TicketTool.TaskActions.resetTask)
            .flatMapLatest(function (data) {
            return Rx.Observable.fromPromise(LoginSkillSet(data)).catch(function (e) { return Rx.Observable.empty(); });
        })
            .subscribe(TicketTool.SkillSetActions.setSkillData);
        TicketTool.SkillSetActions.editSkillSets
            .do(TicketTool.TaskActions.resetTask)
            .map(function () {
            var value = store.getValue();
            value.editMode = true;
            return value;
        })
            .subscribe(store);
        TicketTool.SkillSetActions.editClusters
            .map(function (b) {
            var value = store.getValue();
            value.UserSkillSet.forEach(function (ss) {
                ss.Selected = false;
            });
            value.Clusters.forEach(function (c) { return c.Selected = c.IsLogged; });
            if (!value.UserSkillSet.some(function (s) { return s.IsLogged && s.IsClustered; })) {
                value.Clusters.forEach(function (c) { return c.IsLogged = false; });
            }
            value.clustersEdit = b;
            return value;
        })
            .subscribe(store);
        TicketTool.SkillSetActions.changeClusters.map(function () {
            var value = store.getValue();
            value.Clusters.forEach(function (c) { return c.IsLogged = c.Selected; });
            value.clustersEdit = false;
            return value;
        }).subscribe(store);
    }
    TicketTool.SkillSetActionsSubscribe = SkillSetActionsSubscribe;
})(TicketTool || (TicketTool = {}));
;
//# sourceMappingURL=SkillSetActions.js.map