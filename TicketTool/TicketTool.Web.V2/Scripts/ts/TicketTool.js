/// <reference path="../typings/_references.d.ts" />
/// <reference path="TaskStore.ts" />
/// <reference path="TaskActions.ts" />
/// <reference path="TaskComponents.ts" />
/// <reference path="SkillSetStore.ts" />
/// <reference path="SkillSetActions.ts" />
/// <reference path="SkillSetComponents.ts" />
/// <reference path="Settings.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var TicketTool;
(function (TicketTool) {
    TicketTool.TaskActionsSubscribe(TicketTool.TaskStore);
    var ticks = Rx.Observable.timer(0, TicketTool.Settings.PingInterval * 1000)
        .filter(function () { return !(TicketTool.SkillSetStore.getValue().editMode) && TicketTool.SkillSetStore.getValue().UserSkillSet.some(function (s) { return s.IsLogged; }); })
        .map(function () { return TicketTool.TaskStore.getValue().Task; })
        .share();
    ticks
        .filter(function (task) { return task === null; })
        .subscribe(TicketTool.TaskActions.getTask);
    ticks
        .filter(function (task) { return task !== null; })
        .map(function (task) { return task.TaskId; })
        .subscribe(TicketTool.TaskActions.pingTask);
    TicketTool.SkillSetActionsSubscribe(TicketTool.SkillSetStore);
    TicketTool.SkillSetActions.getSkillData.onNext({});
    var emptyUrl = "about:blank";
    TicketTool.TaskStore
        .map(function (value) { return value.Task; })
        .map(function (task) { return task !== null ? task.InfoUrl : emptyUrl; })
        .distinctUntilChanged()
        .subscribe(function (url) {
        var frame = document.getElementById("orderFrame");
        if (frame.src !== url) {
            frame.src = url;
        }
    });
    var d = React.DOM;
    var TicketToolApp = (function (_super) {
        __extends(TicketToolApp, _super);
        function TicketToolApp() {
            _super.apply(this, arguments);
        }
        TicketToolApp.prototype.render = function () {
            var SkillEdit = React.createFactory(TicketTool.SkillSetEdit);
            var SkillView = React.createFactory(TicketTool.SkillSetView);
            var Task = React.createFactory(TicketTool.TaskView);
            var Outcome = React.createFactory(TicketTool.TaskOutcome);
            var editMode = this.props.skillSetStore.editMode;
            var taskStore = this.props.taskStore;
            return (d.div({ className: "tt-container" }, editMode ? SkillEdit(this.props.skillSetStore) : null, !editMode && taskStore.Outcome === null ? d.div({ className: "skillsets-selected scroll" }, d.div({ className: "padding scroll-content" }, SkillView(this.props.skillSetStore), taskStore.Task !== null ? Task({ task: taskStore.Task, ping: taskStore.PingTask }) : null)) : null, !editMode && taskStore.Task !== null && taskStore.Outcome !== null ? Outcome({ outcome: taskStore.Outcome }) : null, taskStore.OperationResult && taskStore.OperationResult.HasError ? d.p({ className: "alert alert-danger" }, taskStore.OperationResult.MessageText) : null));
        };
        return TicketToolApp;
    })(React.Component);
    TicketTool.TicketToolApp = TicketToolApp;
    ;
    var App = React.createFactory(TicketToolApp);
    Rx.Observable
        .combineLatest(TicketTool.TaskStore.asObservable(), TicketTool.SkillSetStore.asObservable(), function (tasks, skills) { return { skillSetStore: skills, taskStore: tasks }; })
        .subscribe(function (value) {
        React.render(App(value), document.getElementById("right_box"));
    }, function (error) {
        console.log(error);
    });
})(TicketTool || (TicketTool = {}));
//# sourceMappingURL=TicketTool.js.map