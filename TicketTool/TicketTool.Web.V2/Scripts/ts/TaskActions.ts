﻿/// <reference path="../typings/_references.d.ts" />
/// <reference path="TaskStore.ts" />
/// <reference path="Settings.ts" />

module TicketTool {
    function GetTask() {
        return $.ajax({
            url: Settings.Root + "/api/Task/",
            dataType: "json",
            data: {}
        });
    }

    function GetTaskPulse(taskId: string) {
        return $.ajax({
            url: Settings.Root + "/api/Task/" + taskId,
            dataType: "json",
            data: {}
        });
    }

    function CompleteTask(taskId: string, outcome: ITaskOutcome) {
        return $.ajax({
            url: Settings.Root + "/api/Task/" + taskId,
            dataType: "json",
            contentType: "application/json",
            method: "POST",
            data: JSON.stringify(outcome)
        });
    }

    function PostponeTask(taskId: string, minutes: number) {
        return $.ajax({
            url: Settings.Root + "/api/Task/" + taskId + "/Postpone",
            dataType: "json",
            contentType: "application/json",
            method: "POST",
            data: JSON.stringify(minutes)
        });
    }

    export var TaskActions = {
        getTask: new Rx.Subject<any>(),
        pingTask: new Rx.Subject<string>(),
        setOutcome: new Rx.Subject<ITaskOutcome>(),
        completeTask: new Rx.Subject<ITaskOutcome>(),
        postponeTask: new Rx.Subject<number>(),
        resetTask: new Rx.Subject<any>(),
    }

    export function TaskActionsSubscribe(store: Rx.BehaviorSubject<ITaskStore>) {
        var taskStream = TaskActions.getTask
            .flatMapLatest(() => {
                return Rx.Observable.fromPromise(GetTask()).catch(e => Rx.Observable.empty());
            })
            .share();

        taskStream
            .filter(task => task != null && !task.Message)
            .map(task => {
                var value = store.getValue();
                value.Task = task;
                value.PingTask = null;
                return value;
            })
            .subscribe(store);

        taskStream
            .filter(task => task != null && !!(task.Message))
            .subscribe(task => {
                console.log(task.Message);
                window.location.href = Settings.Root + "/Account/Login";
            });

        var pingStream = TaskActions.pingTask
            .flatMapLatest((taskId) => {
                return Rx.Observable.fromPromise<IPing>(GetTaskPulse(taskId)).catch(e => Rx.Observable.empty<IPing>());
            })
            .share();

        pingStream
            .filter(ping => ping != null && !ping.Message)
            .map(ping => {
                var value = store.getValue();
                if (value.Task != null && ping.Tasks.filter(t => t.TaskId === value.Task.TaskId).length > 0) {
                    value.PingTask = ping.Tasks.filter(t => t.TaskId === value.Task.TaskId)[0];
                }
                else {
                    value.Task = null;
                    value.PingTask = null;
                }
                return value;
            })
            .subscribe(store);

        pingStream
            .filter(ping => ping != null && !!(ping.Message))
            .subscribe(ping => {
                console.log(ping.Message);
                window.location.href = Settings.Root + "/Account/Login";
            });

        TaskActions.setOutcome
            .filter(o => o != null && !o.HaveRequiredFields)
            .subscribe(TaskActions.completeTask);

        TaskActions.setOutcome
            .filter(o => o === null || o.HaveRequiredFields)
            .map(o => {
                var value = store.getValue();
                value.Outcome = o;
                return value;
            })
            .subscribe(store);

        var comleteResult = TaskActions.completeTask
            .flatMapLatest((outcome) => {
                var value = store.getValue();
                var taskId = value.Task.TaskId;
                console.log(outcome);
                return Rx.Observable.fromPromise<IOperationResult>(CompleteTask(taskId, outcome)).catch(e => Rx.Observable.empty<IOperationResult>());
                //return Rx.Observable.empty<IOperationResult>();
            });

        var postponeResult = TaskActions.postponeTask
            .flatMapLatest((m) => {
                var value = store.getValue();
                var taskId = value.Task.TaskId;
                return Rx.Observable.fromPromise<IOperationResult>(PostponeTask(taskId, m)).catch(e => Rx.Observable.empty<IOperationResult>());
            });

        var results = Rx.Observable.merge(comleteResult, postponeResult).share();

        results.filter(r => !r.HasError)
            .do(r => {
                var value = store.getValue();
                value.OperationResult = r;
            })
            .subscribe(TaskActions.resetTask);

        results.filter(r => r.HasError)
            .map(r => {
                var value = store.getValue();
                value.OperationResult = r;
                return value;
            })
            .subscribe(store);

        TaskActions.resetTask
            .map(() => {
                var value = store.getValue();
                value.Task = null;
                value.PingTask = null;
                value.Outcome = null;
                return value;
            })
            .subscribe(store);
    }
}