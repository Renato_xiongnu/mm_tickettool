﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using log4net;
using log4net.Config;
using Microsoft.ApplicationServer.StoreManagement.Control;
using Microsoft.ApplicationServer.StoreManagement.Query;
using Microsoft.ApplicationServer.StoreManagement.Sql.Control;
using Microsoft.ApplicationServer.StoreManagement.Sql.Query;

namespace AppFabricResume
{
    internal class Program
    {
        private static void Main()
        {
            XmlConfigurator.Configure();
            var logger = LogManager.GetLogger("AppFabricResume");
            var sleepInSecond = int.Parse(ConfigurationManager.AppSettings["SleepInSecond"]);

            var dbInstances = new List<Guid>();
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["AppFabricPersistence"].ConnectionString))
            {
                var command = new SqlCommand(@"
                        SELECT top " + ConfigurationManager.AppSettings["Count"] + @"tt.Id
                        FROM [System.Activities.DurableInstancing].[InstancesTable] AS tt
                        INNER JOIN [System.Activities.DurableInstancing].[ServiceDeploymentsTable] AS sdt ON tt.ServiceDeploymentId=sdt.Id
                        WHERE 
                            PendingTimer is not null 
				            AND PendingTimer <  DATEADD(HOUR, -1.2, GETDATE())
                            AND CreationTime > '08/01/2014 01:00:00'
                            AND CreationTime < DATEADD(HOUR, -1, GETDATE())
                            AND ExecutionStatus='Idle'
                            AND IsSuspended = 0
                            AND sdt.RelativeApplicationPath='" + ConfigurationManager.AppSettings["VirtualPath"] + @"'
                            AND tt.SurrogateLockOwnerId is null
                        ORDER BY tt.PendingTimer asc", connection);
                connection.Open();

                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    dbInstances.Add((Guid) reader[0]);
                }
                reader.Close();
            }

            var provider = new SqlInstanceQueryProvider();
            var providerArgs = new NameValueCollection
            {
                {
                    "connectionString",
                    ConfigurationManager.ConnectionStrings["AppFabricPersistence"].ConnectionString
                }
            };
            provider.Initialize("ZZT", providerArgs);

            var query = provider.CreateInstanceQuery();
            var args = new InstanceQueryExecuteArgs {InstanceId = dbInstances};
            var asyncResult = query.BeginExecuteQuery(args, new TimeSpan(0, 0, 0, 30), null, null);
            var instances = query.EndExecuteQuery(asyncResult).ToList();

            logger.Debug(instances.Count);
            Console.WriteLine(instances.Count);

            var controlProvider = new SqlInstanceControlProvider();
            var controlProviderArgs = new NameValueCollection
            {
                {"connectionString", ConfigurationManager.ConnectionStrings["AppFabricPersistence"].ConnectionString}
            };
            controlProvider.Initialize("ZZT", controlProviderArgs);

            var control = controlProvider.CreateInstanceControl();
            var commands = instances.Select(instanceInfo =>
                new InstanceCommand {CommandType = CommandType.Resume, InstanceId = instanceInfo.InstanceId, ServiceIdentifier = instanceInfo.HostInfo.HostMetadata});
            foreach (var cmd in commands)
            {
                cmd.ServiceIdentifier["VirtualPath"] = ConfigurationManager.AppSettings["VirtualPathXaml"];
                try
                {
                    var commandAsyncResult = control.CommandSend.BeginSend(cmd, new TimeSpan(0, 0, 0, 30), null, null);
                    control.CommandSend.EndSend(commandAsyncResult);
                }
                catch (Exception ex)
                {
                    logger.Error(cmd.InstanceId, ex);
                }

                Thread.Sleep(sleepInSecond*1000);
            }
        }
    }
}

