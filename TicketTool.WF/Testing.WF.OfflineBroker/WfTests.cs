﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Testing.WF.OfflineBroker.Wf.OfflineBroker;

namespace Testing.WF.OfflineBroker
{
    [TestClass]
    public class WfTests
    {
        [TestMethod]
        public void Create_CorrectRequestAndMethod_ReturnsResponse()
        {
            var client = new Wf.OfflineBroker.OfflineBrokerServiceClient();
            var request =
                new GetItemsRequest {SapCode = "R006", OrderId = "1234567", WwsOrderId = "4004004"};

            var result = client.Start(request);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Create_NotCorrectRequestCorrectMethod_ReturnsNull()
        {
            var client = new Wf.OfflineBroker.OfflineBrokerServiceClient();
            var request =
                new CreateFinancialDocumentRequest {SapCode = "R006", WwsOrderId = "4004004"};

            client.CreateFinancialDocument(request);

            Assert.IsNotNull(request);
        }

    }
}
