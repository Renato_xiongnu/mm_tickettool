﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Testing.ORderProcessing.Process.OrderProcessing;

namespace Testing.ORderProcessing
{
    [TestClass]
    public class StartProcessTest
    {
        [TestMethod]
        public void StartProcess_StartsCorrectly()
        {
            var process = new OrderProcessingServiceClient();

            var result =
                process.StartProcess(new StartProcess { WorkItemId = "OrderId3f4043df-c979-4aa4-b571-eb16a7e23634" });

            Assert.AreEqual("OrderId4bd06c92-80e4-42db-a5a6-6afef83dd303", result);
        }
    }
}
