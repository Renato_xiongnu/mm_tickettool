﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net.Config;
using TT.LoadTester.Agents;
using TT.LoadTester.Agents.Infrastructure;

namespace TT.LoadSkillSetTester
{
    class Program
    {
        static void Main(string[] args)
        {
            BasicConfigurator.Configure();

            var credentials = new CredentialsProvider().GetCredentials();
            
            var agents =
                credentials.Select(
                    credential => AgenеFactory.Create(credential.Login, credential.Password, credential.Role))
                    .ToList();

            Parallel.ForEach(agents, t => t.Login());

            Parallel.ForEach(agents, ag=>ag.OpenStatistics());
        }
    }
}
