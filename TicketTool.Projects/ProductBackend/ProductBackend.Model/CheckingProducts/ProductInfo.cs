﻿using System.Runtime.Serialization;

namespace TicketTool.Projects.ProductBackend.Model.CheckingProducts
{
    [DataContract]
    public class ProductInfo
    {
        public long Article { get; set; }

        public int Quantity { get; set; }
    }
}