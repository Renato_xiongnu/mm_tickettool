﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TicketTool.Projects.ProductBackend.Model.CheckingProducts
{
    public class CheckingProductsResult
    {
        public ProductInfo[] Products { get; set; }
    }
}