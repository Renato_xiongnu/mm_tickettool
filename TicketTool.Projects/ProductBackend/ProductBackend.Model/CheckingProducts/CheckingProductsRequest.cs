﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace TicketTool.Projects.ProductBackend.Model.CheckingProducts
{
    public class CheckingProductsRequest
    {
        public ArticleData[] Articles { get; set; }

        public string SaleLocationId { get; set; }

        public string ChannelId { get; set; }

        public long[] ArticlesNo
        {
            get { return Articles != null ? Articles.Select(el => el.Article).ToArray() : null; }
        }
    }

    public class ArticleData
    {
        public long Article { get; set; }

        public int Qty { get; set; }
    }
}
