﻿using System.Activities;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TicketTool.Projects.ProductBackend.Activities.CheckingProducts;
using TicketTool.Projects.ProductBackend.Model.CheckingProducts;

namespace TicketTool.Projects.ProductBackend.Activities.Test
{
    [TestClass]
    public class CheckingProductsActivityTest
    {
        private Dictionary<string, object> _parameters;

        [TestInitialize]
        public void Init()
        {
            _parameters = new Dictionary<string, object>();
        }

        [TestMethod]
        public void SapCodeIsEmpty()
        {
            _parameters["Workitem"] = new CheckingProductsRequest
                {
                    Articles = new[] {new ArticleData{Article = 0,Qty = 0}},
                    SaleLocationId = ""
                };
            var activity = new CheckingProductsActivity();
            var result = WorkflowInvoker.Invoke(activity, _parameters);
            Assert.IsFalse(result.Success);
        }

        [TestMethod]
        public void ChannelIsEmpty()
        {
            _parameters["Workitem"] = new CheckingProductsRequest
            {
                Articles = new[] { new ArticleData { Article = 0, Qty = 0 } },
                SaleLocationId = "R004",
                ChannelId = ""
            };
            var activity = new CheckingProductsActivity();
            var result = WorkflowInvoker.Invoke(activity, _parameters);
            Assert.IsFalse(result.Success);
        }

        [TestMethod]
        public void ArticlesListIsEmpty()
        {
            _parameters["Workitem"] = new CheckingProductsRequest
                {
                    Articles = new[] { new ArticleData { Article = 0, Qty = 0 } },
                    SaleLocationId = "R004"
                };
            var activity = new CheckingProductsActivity();
            var result = WorkflowInvoker.Invoke(activity, _parameters);
            Assert.IsFalse(result.Success);
        }

        [TestMethod]
        public void RightRequest()
        {
            const int article = 1022506;
            const int qty = 1;
            _parameters["Workitem"] = new CheckingProductsRequest
                {
                    Articles = new[] { new ArticleData { Article = article, Qty = qty } },
                    SaleLocationId = "BR001",
                    ChannelId = "ZZT"
                };
            var activity = new CheckingProductsActivity();
            var result = WorkflowInvoker.Invoke(activity, _parameters);
            Assert.IsTrue(result.Success);
            Assert.IsTrue(result.Result.Products.First(p => p.Article == article).Quantity > 0);
        }

        [TestMethod]
        public void ArticleNotExist()
        {
            const int article = 0;
            const int qty = 1;
            _parameters["Workitem"] = new CheckingProductsRequest
                {
                    Articles = new[] { new ArticleData { Article = article, Qty = qty } },
                    SaleLocationId = "R206",
                    ChannelId = "ZZT"
                };
            var activity = new CheckingProductsActivity();
            var result = WorkflowInvoker.Invoke(activity, _parameters);
            Assert.IsFalse(result.Success);
            Assert.AreEqual(-1, result.Result.Products.First(p => p.Article == article).Quantity);
        }
    }
}