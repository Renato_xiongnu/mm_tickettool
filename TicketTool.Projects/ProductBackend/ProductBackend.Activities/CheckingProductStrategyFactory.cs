﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicketTool.Projects.ProductBackend.Activities.CheckingProducts;

namespace TicketTool.Projects.ProductBackend.Activities
{
    public class CheckingProductStrategyFactory
    {
        public static ICheckingProductStrategy CreateProductChecking()
        {
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["UseProductMock"]))
            {
                return new CheckingProductStrategyMock();
            }
            //1000000
            //1 - есть
            //2 - нет
            return new CheckingProductStrategy();
        }
    }
}
