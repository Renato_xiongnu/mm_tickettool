﻿using MMS.Activities.Model;
using TicketTool.Projects.ProductBackend.Activities.OrdersService;
using TicketTool.Projects.ProductBackend.Model.CheckingProducts;

namespace TicketTool.Projects.ProductBackend.Activities.CheckingProducts
{
    public interface ICheckingProductStrategy
    {
        bool TryGetItemsList(
            GetItemsRequest request,
            ExecutingResult<CheckingProductsResult> result,
            out GetArticleStockStatusResponse getItemsResponse);
    }
}