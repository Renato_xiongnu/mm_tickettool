﻿using System.Linq;
using MMS.Activities.Model;
using TicketTool.Projects.ProductBackend.Activities.OrdersService;
using TicketTool.Projects.ProductBackend.Model.CheckingProducts;

namespace TicketTool.Projects.ProductBackend.Activities.CheckingProducts
{
    public class CheckingProductStrategyMock : ICheckingProductStrategy
    {
        public bool TryGetItemsList(GetItemsRequest request, ExecutingResult<CheckingProductsResult> result,
                                    out GetArticleStockStatusResponse getItemsResponse)
        {
            getItemsResponse = new GetArticleStockStatusResponse();

            var items = request.Articles.Where(el => el.StartsWith("1")).Select(article => new OrdersBackendItem
                {
                    Article = long.Parse(article),
                    Qty = 100,
                    Price = 10000,
                    Status = StockStatusEnum.InStock
                }).ToList();

            items.AddRange(request.Articles.Where(el => el.StartsWith("2")).Select(article => new OrdersBackendItem
                {
                    Article = long.Parse(article),
                    Qty = 0,
                    Price = 10000,
                    Status = StockStatusEnum.IsBlocked
                }));
            getItemsResponse.Items = items.ToArray();

            return true;
        }
    }
}