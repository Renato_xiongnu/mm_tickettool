﻿using System.ComponentModel;
using System.Globalization;
using System.Linq;
using MMS.Activities;
using MMS.Activities.Model;
using TicketTool.Projects.ProductBackend.Activities.OrdersService;
using TicketTool.Projects.ProductBackend.Model.CheckingProducts;

namespace TicketTool.Projects.ProductBackend.Activities.CheckingProducts
{
    [Designer(typeof(CheckingProductsActivityDesigner))]
    public class CheckingProductsActivity : ExecuteExternalService<CheckingProductsRequest, CheckingProductsResult>
    {
        private ICheckingProductStrategy _checkingProductStrategy;

        public CheckingProductsActivity()
        {
            _checkingProductStrategy = CheckingProductStrategyFactory.CreateProductChecking();
        }

        protected override ExecutingResult<CheckingProductsResult> ExecuteService(CheckingProductsRequest workitem)
        {
            var result = ValidateRequest(workitem);
            if (!result.Success)
            {
                return result;
            }

            GetArticleStockStatusResponse getItemsResponse;
            if (!_checkingProductStrategy.TryGetItemsList(new GetItemsRequest
                {
                    Channel = workitem.ChannelId,
                    SaleLocation = workitem.SaleLocationId,
                    Articles = workitem.Articles.Select(el => el.Article.ToString(CultureInfo.InvariantCulture)).ToArray()
                }, result, out getItemsResponse))
            {
                return result;
            }

            var itemInfos = getItemsResponse.Items ?? new OrdersBackendItem[0];

            var products = (from article in workitem.ArticlesNo
                            join itemInfo in
                                itemInfos.Where(
                                    el =>
                                    el.Status == StockStatusEnum.DisplayItem || el.Status == StockStatusEnum.InStock) on
                                article equals itemInfo.Article into items
                            from itemInfo in items.DefaultIfEmpty()
                            select new ProductInfo
                                {
                                    Article = article,
                                    Quantity = itemInfo == null
                                                   ? -1
                                                   : itemInfo.Qty
                                }).ToList();

            result.Success =
                workitem.Articles.All(
                    sourceArticle =>
                    sourceArticle.Qty <= products.First(el => el.Article == sourceArticle.Article).Quantity);
            
            result.Result = new CheckingProductsResult
                {
                    Products = products.ToArray()
                };
            return result;
        }
        
        private ExecutingResult<CheckingProductsResult> ValidateRequest(CheckingProductsRequest workitem)
        {
            var result = new ExecutingResult<CheckingProductsResult> {Success = true};

            if(workitem.Articles == null || workitem.Articles.Length == 0)
            {
                result.Success = false;
                result.Message = "List of articles should not be empty";
            }
            if (string.IsNullOrWhiteSpace(workitem.SaleLocationId))
            {
                result.Success = false;
                result.Message = "You must specify the SaleLocationId";
            }
            if (string.IsNullOrWhiteSpace(workitem.ChannelId))
            {
                result.Success = false;
                result.Message = "You must specify the ChannelId";
            }

            return result;
        }
    }
}