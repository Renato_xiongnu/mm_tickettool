﻿using System.ServiceModel;
using MMS.Activities.Model;
using TicketTool.Projects.ProductBackend.Activities.OrdersService;
using TicketTool.Projects.ProductBackend.Model.CheckingProducts;

namespace TicketTool.Projects.ProductBackend.Activities.CheckingProducts
{
    public class CheckingProductStrategy : ICheckingProductStrategy
    {
        public bool TryGetItemsList(
           GetItemsRequest request,
           ExecutingResult<CheckingProductsResult> result,
           out GetArticleStockStatusResponse getItemsResponse)
       {
           try
           {
               var client = new OrdersBackendServiceClient();
               getItemsResponse = client.GetArticleStockStatus(request);
               return true;
           }
           catch (FaultException ex)
           {
               getItemsResponse = null;
               if (ex.Reason != null)
               {
                   var reasonString = ex.Reason.ToString();
                   switch (reasonString)
                   {
                       case "No channel provided":
                           result.Success = false;
                           result.Exception = ex;
                           result.Message = string.Format("{0} - {1}", request.Channel, reasonString);
                           return false;
                       case "No saleLocation provided":
                           result.Success = false;
                           result.Exception = ex;
                           result.Message = string.Format("{0} - {1}", request.SaleLocation, reasonString);
                           return false;
                   }
               }
               throw;
           }
       }
    }
}
