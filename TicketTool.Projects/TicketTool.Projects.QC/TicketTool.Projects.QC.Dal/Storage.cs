﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TicketTool.Projects.QC.Dal.Model;

namespace TicketTool.Projects.QC.Dal
{
    public class Storage : DbContext, IStorage
    {
        public Storage()
            : base("SurveyStorage")
        {            
        }

        public DbSet<Survey> Surveys { get; set; }

        public DbSet<Question> Questions { get; set; }

        public DbSet<Option> Options { get; set; }

        public DbSet<Answer> Answers { get; set; }

        public IQueryable<Survey> GetAllSurveys()
        {
            return this.Surveys.Include("SurveyQuestions.Question.Options");
        }

        public Survey GetSurvey(int id)
        {
            return this.Surveys.Include("SurveyQuestions.Question.Options").FirstOrDefault(s => s.Id == id);
        }

        public void Update()
        {
            this.SaveChanges();
        }

        public void AddAnswers(IEnumerable<Answer> answers)
        {
            foreach (var answer in answers)
            {
                this.Answers.Add(answer);
            }
        }        
    }
}