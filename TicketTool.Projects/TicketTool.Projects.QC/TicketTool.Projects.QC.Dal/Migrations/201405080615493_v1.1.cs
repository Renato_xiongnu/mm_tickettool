namespace TicketTool.Projects.QC.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class v11 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Questions", "Survey_Id", "dbo.Surveys");
            DropIndex("dbo.Questions", new[] { "Survey_Id" });
            CreateTable(
                "dbo.SurveyQuestions",
                c => new
                {
                    SurveyId = c.Int(nullable: false),
                    QuestionId = c.Int(nullable: false),
                    Order = c.Int(nullable: false, defaultValue: 0),
                })
                .PrimaryKey(t => new { t.SurveyId, t.QuestionId })
                .ForeignKey("dbo.Questions", t => t.QuestionId, cascadeDelete: true)
                .ForeignKey("dbo.Surveys", t => t.SurveyId, cascadeDelete: true)
                .Index(t => t.QuestionId)
                .Index(t => t.SurveyId);

            Sql("INSERT INTO [dbo].[SurveyQuestions] " +
                "([SurveyId],[QuestionId])  " +
                "SELECT [Survey_Id],[Id] AS [Question_Id] " +
                "FROM [dbo].[Questions]");

            AddColumn("dbo.Answers", "WorkItemId", c => c.String(nullable: false, defaultValue: "", maxLength: 64));
            AddColumn("dbo.Options", "Order", c => c.Int(nullable: false, defaultValue: 0));
            DropColumn("dbo.Questions", "Survey_Id");
        }

        public override void Down()
        {
            AddColumn("dbo.Questions", "Survey_Id", c => c.Int());
            DropForeignKey("dbo.SurveyQuestions", "SurveyId", "dbo.Surveys");
            DropForeignKey("dbo.SurveyQuestions", "QuestionId", "dbo.Questions");
            DropIndex("dbo.SurveyQuestions", new[] { "SurveyId" });
            DropIndex("dbo.SurveyQuestions", new[] { "QuestionId" });
            DropColumn("dbo.Options", "Order");
            DropColumn("dbo.Answers", "WorkItemId");
            DropTable("dbo.SurveyQuestions");
            CreateIndex("dbo.Questions", "Survey_Id");
            AddForeignKey("dbo.Questions", "Survey_Id", "dbo.Surveys", "Id");
        }
    }
}
