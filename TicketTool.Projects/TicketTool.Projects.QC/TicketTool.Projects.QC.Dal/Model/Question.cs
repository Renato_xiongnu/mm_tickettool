﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TicketTool.Projects.QC.Dal.Model
{
    public class Question
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(255)]
        public string Name { get; set; }

        public virtual ICollection<SurveyQuestion> SurveyQuestions { get; set; }

        public virtual ICollection<Option> Options { get; set; }
    }
}