﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TicketTool.Projects.QC.Dal.Model
{
    public class Answer
    {
        [Key]
        public int Id { get; set; }

        public Guid GroupId { get; set; }

        public DateTime Answered { get; set; }

        public Survey Survey { get; set; }

        public Question Question { get; set; }

        public Option Option { get; set; }

        [MaxLength(64)]
        public string WorkItemId { get; set; }
    }
}