﻿using System.ComponentModel.DataAnnotations;

namespace TicketTool.Projects.QC.Dal.Model
{
    public class Option
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(255)]
        public string Name { get; set; }

        public int Order { get; set; }

        public Question Question { get; set; }
    }
}