﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TicketTool.Projects.QC.Dal.Model
{
    public class SurveyQuestion
    {
        [Key, Column(Order = 0)]
        public int SurveyId { get; set; }

        [Key, Column(Order = 1)]
        public int QuestionId { get; set; }

        public int Order { get; set; }

        public virtual Survey Survey { get; set; }

        public virtual Question Question { get; set; }
    }
}