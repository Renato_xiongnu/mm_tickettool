﻿using System.Collections.Generic;
using System.Linq;
using TicketTool.Projects.QC.Dal.Model;

namespace TicketTool.Projects.QC.Dal
{
    public interface IStorage
    {
        IQueryable<Survey> GetAllSurveys();
        Survey GetSurvey(int id);
        void Update();
        void AddAnswers(IEnumerable<Answer> answers);
    }
}
