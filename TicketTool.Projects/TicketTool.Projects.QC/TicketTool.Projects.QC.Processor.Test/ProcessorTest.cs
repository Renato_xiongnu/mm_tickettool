﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TicketTool.Projects.QC.Dal;
using TicketTool.Projects.QC.Dal.Model;
using TicketTool.Projects.QC.Processor.Contracts;
using TicketTool.Projects.QC.Processor.Exceptions;
using Answer = TicketTool.Projects.QC.Processor.Contracts.Answer;
using Option = TicketTool.Projects.QC.Dal.Model.Option;
using Question = TicketTool.Projects.QC.Dal.Model.Question;
using Survey = TicketTool.Projects.QC.Dal.Model.Survey;

namespace TicketTool.Projects.QC.Processor
{
    [TestClass]
    public class ProcessorTest
    {
        private IProcessor _processor;

        private Mock<IStorage> _storageMock;

        [TestInitialize]
        public void Init()
        {
            _processor = Processor.Default;
            _storageMock = new Mock<IStorage>();
            // ReSharper disable PossibleNullReferenceException
            _processor.GetType().GetField("_storage", BindingFlags.NonPublic | BindingFlags.Instance)
                // ReSharper restore PossibleNullReferenceException
                .SetValue(_processor, _storageMock.Object);
        }

        [TestMethod]
        public void GetAllSurveysTest()
        {
            int id1 = 1, id2 = 2;
            string name1 = "Опрос для 003", name2 = "Опрос для ММ";

            _storageMock.Setup(x => x.GetAllSurveys()).Returns(
                new[] {new Survey {Id = id1, Name = name1}, new Survey {Id = id2, Name = name2}}.AsQueryable());

            var result = _processor.GetAllSurvey();

            Assert.AreEqual(result[id1], name1);
            Assert.AreEqual(result[id2], name2);
            _storageMock.Verify(x => x.GetAllSurveys());
        }

        [TestMethod]
        public void GetSurveyTest()
        {
            const int id = 10;
            var name = "Survey";
            int[] qId = {1, 2};
            string[] qNames = {"Are you alien?", "Have you ever been in Taganrog?"};
            int[] oId = {1, 2, 3, 4};
            string[] oNames = {"Yes", "No", "Maybe", "I don't know"};

            _storageMock.Setup(x => x.GetSurvey(id)).Returns(
                new Survey
                {
                    Id = id,
                    Name = name,
                    SurveyQuestions = new List<SurveyQuestion>
                    {
                        new SurveyQuestion
                        {
                            QuestionId = qId[0],
                            Question = new Question
                            {
                                Id = qId[0],
                                Name = qNames[0],
                                Options = new List<Option>
                                {
                                    new Option {Id = oId[0], Name = oNames[0]},
                                    new Option {Id = oId[1], Name = oNames[1]}
                                }
                            }
                        },
                        new SurveyQuestion
                        {
                            QuestionId = qId[1],
                            Question = new Question
                            {
                                Id = qId[1],
                                Name = qNames[1],
                                Options = new List<Option>
                                {
                                    new Option {Id = oId[2], Name = oNames[2]},
                                    new Option {Id = oId[3], Name = oNames[3]}
                                }
                            }
                        },
                    }
                }
                );

            var result = _processor.GetSurvey(id);

            Assert.IsNotNull(result);
            Assert.AreEqual(result.Id, id);
            Assert.AreEqual(result.Name, name);
            Assert.IsTrue(result.Questions.Any());
            var index = 0;
            var optionindex = 0;
            foreach(var question in result.Questions)
            {
                Assert.AreEqual(question.Id, qId[index]);
                Assert.AreEqual(question.Name, qNames[index]);
                Assert.IsTrue(question.Options.Any());

                foreach(var option in question.Options)
                {
                    Assert.AreEqual(option.Id, oId[optionindex]);
                    Assert.AreEqual(option.Name, oNames[optionindex]);
                    optionindex++;
                }
                index++;
            }
            _storageMock.Verify(x => x.GetSurvey(id));
        }

        [TestMethod]
        [ExpectedException(typeof(SurveyNotFoundException))]
        public void GetSurveyExTest()
        {
            const int id = 10;
            _storageMock.Setup(x => x.GetSurvey(id)).Returns(() => null);

            _processor.GetSurvey(id);
        }

        [TestMethod]
        public void CompleteSurveyTest()
        {
            const int questionId = 1;
            const int optionId = 1;
            const int surveyId = 1;
            var surveyResults = new SurveyResults
            {
                SurveyId = surveyId,
                Answers = new[] {new Answer {OptionId = optionId, QuestionId = questionId}}
            };
            _storageMock.Setup(x => x.GetSurvey(surveyId)).Returns(
                new Survey
                {
                    Id = surveyId,
                    SurveyQuestions = new List<SurveyQuestion>
                    {
                        new SurveyQuestion
                        {
                            QuestionId = questionId,
                            Question = new Question
                            {
                                Id = questionId,
                                Options = new List<Option>
                                {
                                    new Option {Id = optionId}
                                }
                            }
                        }
                    }
                });

            _processor.CompleteSurvey(surveyResults);

            _storageMock.Verify(x => x.GetSurvey(surveyResults.SurveyId));
            _storageMock.Verify(
                x => x.AddAnswers(It.Is<IEnumerable<Dal.Model.Answer>>(aa => aa.GroupBy(a => a.GroupId).Count() == 1)));
            _storageMock.Verify(x => x.Update());
        }

        [TestMethod]
        [ExpectedException(typeof(SurveyNotFoundException))]
        public void CompleteSurveySurveyNotFoundExTest()
        {
            var surveyResults = new SurveyResults
            {
                SurveyId = 322232,
                Answers = new[] {new Answer {OptionId = 1, QuestionId = 1}}
            };
            _storageMock.Setup(x => x.GetSurvey(surveyResults.SurveyId)).Returns(() => null);

            _processor.CompleteSurvey(surveyResults);
        }

        [TestMethod]
        [ExpectedException(typeof(QuestionNotFoundException))]
        public void CompleteSurveyQuestionNotFoundExTest()
        {
            const int questionId = 1;
            const int surveyId = 322232;
            var surveyResults = new SurveyResults
            {
                SurveyId = surveyId,
                Answers = new[] {new Answer {OptionId = 1, QuestionId = questionId}}
            };
            _storageMock.Setup(x => x.GetSurvey(surveyResults.SurveyId)).Returns(() => new Survey {Id = surveyId});

            _processor.CompleteSurvey(surveyResults);
        }

        [TestMethod]
        [ExpectedException(typeof(OptionNotFoundException))]
        public void CompleteSurveyOptionNotFoundExTest()
        {
            const int optionId = 1;
            const int surveyId = 322232;
            const int questionId = 1;

            var surveyResults = new SurveyResults
            {
                SurveyId = surveyId,
                Answers = new[] {new Answer {OptionId = optionId, QuestionId = questionId}}
            };
            _storageMock.Setup(x => x.GetSurvey(surveyId)).Returns(
                new Survey
                {
                    Id = surveyId,
                    SurveyQuestions = new List<SurveyQuestion>
                    {
                        new SurveyQuestion
                        {
                            QuestionId = questionId,
                            Question = new Question {Id = questionId}
                        }
                    }
                });

            _processor.CompleteSurvey(surveyResults);
        }
    }
}