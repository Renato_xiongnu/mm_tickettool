﻿using System.Collections.Generic;
using TicketTool.Projects.QC.Processor.Contracts;

namespace TicketTool.Projects.QC.Processor
{
    public interface IProcessor
    {
        IDictionary<int, string> GetAllSurvey();
        Survey GetSurvey(int id);
        void CompleteSurvey(SurveyResults results);
    }
}
