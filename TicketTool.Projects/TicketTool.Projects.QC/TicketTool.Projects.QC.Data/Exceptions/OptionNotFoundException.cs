﻿using System;

namespace TicketTool.Projects.QC.Processor.Exceptions
{
    public class OptionNotFoundException : Exception
    {
        public OptionNotFoundException(string message) : base(message)
        {            
        }
    }
}
