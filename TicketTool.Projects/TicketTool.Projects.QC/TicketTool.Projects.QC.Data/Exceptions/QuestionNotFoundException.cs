﻿using System;

namespace TicketTool.Projects.QC.Processor.Exceptions
{
    public class QuestionNotFoundException : Exception
    {
        public QuestionNotFoundException(string message)
            :base(message)
        {            
        }
    }
}
