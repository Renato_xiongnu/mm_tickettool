﻿using System;

namespace TicketTool.Projects.QC.Processor.Exceptions
{
    public class SurveyNotFoundException : Exception
    {
        public SurveyNotFoundException(string message) : base(message)
        {
        }
    }
}
