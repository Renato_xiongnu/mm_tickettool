﻿using System;
using System.Collections.Generic;
using System.Linq;
using TicketTool.Projects.QC.Dal;
using TicketTool.Projects.QC.Processor.Contracts;
using Answer = TicketTool.Projects.QC.Dal.Model.Answer;

namespace TicketTool.Projects.QC.Processor
{
    public class Processor : IProcessor
    {
        private static IProcessor _processor;

        private readonly IStorage _storage;

        private Processor()
        {
            _storage = new Storage();
        }

        public static IProcessor Default
        {
            get { return _processor ?? (_processor = new Processor()); }
        }

        IDictionary<int, string> IProcessor.GetAllSurvey()
        {
            return _storage.GetAllSurveys().ToDictionary(s => s.Id, s => s.Name);
        }

        public Survey GetSurvey(int id)
        {
            var survey = _storage.GetSurvey(id);
            if(survey == null)
            {
                throw Ex.SurveyNotFound();
            }

            return new Survey
            {
                Id = survey.Id,
                Name = survey.Name,
                Questions = survey.SurveyQuestions
                    .OrderBy(sq => sq.Order)
                    .ThenBy(o => o.QuestionId)
                    .Select(q => new Question
                    {
                        Id = q.Question.Id,
                        Name = q.Question.Name,
                        Options = q.Question.Options
                            .OrderBy(o => o.Order)
                            .ThenBy(o=>o.Id)
                            .Select(o => new Option {Id = o.Id, Name = o.Name})
                    })
            };
        }

        public void CompleteSurvey(SurveyResults results)
        {
            var groupId = Guid.NewGuid();
            var answered = DateTime.Now;
            var answers = new List<Answer>();

            var survey = _storage.GetSurvey(results.SurveyId);
            if(survey == null)
            {
                throw Ex.SurveyNotFound();
            }
            var workItemId = results.WorkItemId??string.Empty;

            foreach(var answer in results.Answers)
            {
                if (survey.SurveyQuestions == null)
                {
                    throw Ex.QuestionNotFound(answer.QuestionId);
                }
                var question = survey.SurveyQuestions
                    .Where(sq => sq.QuestionId == answer.QuestionId)
                    .Select(sq=>sq.Question)
                    .FirstOrDefault(q => q.Id == answer.QuestionId);
                if(question == null)
                {
                    throw Ex.QuestionNotFound(answer.QuestionId);
                }

                if(question.Options == null)
                {
                    throw Ex.OptionNotFound(answer.OptionId);
                }
                var option = question.Options.FirstOrDefault(o => o.Id == answer.OptionId);
                if(option == null)
                {
                    throw Ex.OptionNotFound(answer.OptionId);
                }

                answers.Add(new Answer
                {
                    WorkItemId = workItemId,
                    Answered = answered,
                    GroupId = groupId,
                    Survey = survey,
                    Question = question,
                    Option = option
                });
            }

            _storage.AddAnswers(answers);
            _storage.Update();
        }
    }
}