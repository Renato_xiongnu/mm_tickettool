﻿using System;
using TicketTool.Projects.QC.Processor.Exceptions;
using TicketTool.Projects.QC.Processor.Properties;

namespace TicketTool.Projects.QC.Processor
{
    internal static class Ex
    {
        internal static SurveyNotFoundException SurveyNotFound()
        {
            return new SurveyNotFoundException(Resources.Ex_SurveyNoFound);
        }

        internal static QuestionNotFoundException QuestionNotFound(int id)
        {
            return new QuestionNotFoundException(String.Format(Resources.Ex_QuestionNotFoundFormat, id));
        }

        internal static OptionNotFoundException OptionNotFound(int id)
        {
            return new OptionNotFoundException(String.Format(Resources.Ex_OptionNotFoundFormat, id));
        }
    }
}
