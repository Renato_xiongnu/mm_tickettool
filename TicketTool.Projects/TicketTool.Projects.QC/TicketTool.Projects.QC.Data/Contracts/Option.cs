﻿using System.Runtime.Serialization;

namespace TicketTool.Projects.QC.Processor.Contracts
{
    [DataContract]
    public class Option
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}