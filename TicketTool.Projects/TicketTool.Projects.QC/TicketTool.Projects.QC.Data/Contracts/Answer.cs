﻿using System.Runtime.Serialization;

namespace TicketTool.Projects.QC.Processor.Contracts
{
    [DataContract]
    public class Answer
    {
        [DataMember]
        public int OptionId { get; set; }

        [DataMember]
        public int QuestionId { get; set; }
    }
}
