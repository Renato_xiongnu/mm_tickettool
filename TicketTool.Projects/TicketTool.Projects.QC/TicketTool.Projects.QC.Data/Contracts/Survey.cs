﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TicketTool.Projects.QC.Processor.Contracts
{
    [DataContract]
    public class Survey
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public IEnumerable<Question> Questions { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
