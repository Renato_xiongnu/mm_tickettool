﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TicketTool.Projects.QC.Processor.Contracts
{
    [DataContract]
    public class Question
    {
        [DataMember]
        public IEnumerable<Option> Options { get; set; }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}