﻿using System.Runtime.Serialization;

namespace TicketTool.Projects.QC.Processor.Contracts
{
    [DataContract]
    public class SurveyResults
    {
        [DataMember]
        public int SurveyId { get; set; }

        [DataMember]
        public string WorkItemId { get; set; }

        [DataMember]
        public Answer[] Answers { get; set; }
    }
}
