﻿using System;
using System.Activities;
using System.Activities.Presentation;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using MMS.Activities.Extensions;
using MMS.Activities.Logging;
using TicketTool.Activities.Properties;
using TicketTool.Activities.TicketToolProxy;
using TicketTool.Projects.QC.Activities.Design;
using TicketTool.Projects.QC.Activities.Model;
using TicketTool.Projects.QC.Activities.TicketToolQCProxy;
using Outcome = TicketTool.Activities.Tasks.Outcome;
using Settings = TicketTool.Projects.QC.Activities.Properties.Settings;

namespace TicketTool.Projects.QC.Activities
{
    [Designer(typeof(SurveyOutcomeActivityDesigner))]
    public class SurveyOutcome : Outcome, IActivityTemplateFactory<Outcome>
    {
        [Browsable(false)]
        public SurveyItem Survey { get; set; }

        public string DesignUrl { get; set; }

        [Browsable(false)]
        public InArgument<string> WorkItemId { get; set; }

        protected override TicketTool.Activities.TicketToolProxy.Outcome GetTicketToolProxyOutcome()
        {
            var survey = new QCServiceClient().GetSurvey(Survey.Id);
            var outcome = new TicketTool.Activities.TicketToolProxy.Outcome {IsSuccess = IsPositive, Value = Name};
            if(!survey.HasErrors)
            {
                var fields = new List<RequiredField>();
                foreach(var question in survey.Item.Questions)
                {
                    var field = new RequiredFieldValueList {Name = question.Name, Type = "Choice"};
                    if(question.Options != null)
                    {
                        field.PredefinedValuesList = question.Options.Select(o => (object) o.Name).ToArray();
                        if(question.Options.Length > 0)
                        {
                            field.DefaultValue = question.Options[0].Name;
                        }
                    }
                    fields.Add(field);
                }
                outcome.RequiredFields = fields.ToArray();
            }
            return outcome;
        }

        protected override ICollection<string> ValidateRequestedData(Dictionary<string, object> data)
        {
            return new Collection<string>();
        }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            var arg = new RuntimeArgument(Resources.Param_RequestedValue, typeof(Dictionary<string, object>),
                ArgumentDirection.In);
            metadata.Bind(RequestedData, arg);
            metadata.AddArgument(arg);

            var workitemId = new RuntimeArgument(Resources.Param_WorkItemId, typeof(string), ArgumentDirection.In);
            metadata.Bind(WorkItemId, workitemId);
            metadata.AddArgument(workitemId);
        }

        protected override void Execute(NativeActivityContext context)
        {
            if(RequestedData == null)
            {
                return;
            }
            var requestedData = RequestedData.Get(context);
            if(requestedData == null)
            {
                return;
            }
            var outcomeUserData = RequestedData.Get(context)
                .Where(f => f.Key.StartsWith(String.Concat(Name, Divider)))
                .Select(
                    f =>
                        new
                        {
                            Key = f.Key.Substring(f.Key.IndexOf(Divider, StringComparison.Ordinal) + Divider.Length),
                            f.Value
                        })
                .ToDictionary(f => f.Key, f => f.Value);
            var workItemId = WorkItemId == null
                ? string.Empty
                : WorkItemId.Get(context);

            try
            {
                var answers = GetAnswers(outcomeUserData);
                if(answers.Length == 0)
                {
                    context.LogInfoFormat("CompleteSurvey: the number of answers equal to zero");
                    return;
                }
                var result = new QCServiceClient().CompleteSurvey(
                    new SurveyResults
                    {
                        SurveyId = Survey.Id,
                        WorkItemId = workItemId,
                        Answers = answers
                    });

                if(result.HasErrors)
                {
                    context.LogErrorFormat("CompleteSurvey error : {0}", result.Message);
                }
            }
            catch(Exception ex)
            {
                context.LogError("CompleteSurvey exception", ex);
            }
        }

        private Answer[] GetAnswers(IDictionary<string, object> data)
        {
            var survey = new QCServiceClient().GetSurvey(Survey.Id);
            if(survey.HasErrors)
            {
                Logger.Error(survey.Message);
                return new Answer[0];
            }

            var answers = new List<Answer>();
            foreach(var response in data)
            {
                var question = survey.Item.Questions.FirstOrDefault(q => q.Name == response.Key);
                if(question != null)
                {
                    var responseValue = (string) response.Value;
                    var option = question.Options.FirstOrDefault(o => o.Name == responseValue);
                    if(option != null)
                    {
                        answers.Add(new Answer {OptionId = option.Id, QuestionId = question.Id});
                    }
                }
            }
            return answers.ToArray();
        }

        public Outcome Create(DependencyObject target, IDataObject dataObject)
        {
            return new SurveyOutcome
            {
                Name = Properties.Resources.Survey,
                DisplayName = Properties.Resources.Survey,
                DesignUrl = Settings.Default.SurveyServiceDesignUrl
            };
        }
    }
}