﻿namespace TicketTool.Projects.QC.Activities.Model
{
    public class SurveyItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
