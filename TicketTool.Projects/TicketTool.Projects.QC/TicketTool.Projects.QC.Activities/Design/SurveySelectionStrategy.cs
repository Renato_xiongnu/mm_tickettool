﻿using System;
using System.Activities.Presentation;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.Windows;
using TicketTool.Projects.QC.Activities.Annotations;
using TicketTool.Projects.QC.Activities.Model;
using TicketTool.Projects.QC.Activities.TicketToolQCProxy;

namespace TicketTool.Projects.QC.Activities.Design
{
    public class SurveySelectionStrategy : INotifyPropertyChanged
    {
        private readonly ActivityDesigner _designer;
        private ObservableCollection<SurveyItem> _surveys;
        private SurveyItem _selectedSurvey;

        public SurveySelectionStrategy(ActivityDesigner designer)
        {
            this._designer = designer;
        }

        public ObservableCollection<SurveyItem> Surveys
        {
            get { return this._surveys; }
            private set
            {
                this._surveys = value;
                this.OnPropertyChanged();
            }
        }

        public SurveyItem Survey
        {
            get { return this._selectedSurvey; }
            set
            {
                this.SetValue(ref this._selectedSurvey, value, "Survey");
            }
        }

        private string DesignTimeServiceUrl
        {
            get
            {
                var property = this._designer.ModelItem.Properties["DesignUrl"];
                if (property != null)
                {
                    if (property.Value != null)
                    {
                        return (string)property.Value.GetCurrentValue();
                    }
                }
                return null;
            }
        }

        private void SetValue<T>(ref T oldValue, T newValue, string propertyName) where T : class
        {
            if (newValue == oldValue) return;
            oldValue = newValue;
            var property = this._designer.ModelItem.Properties[propertyName];
            if (property != null) property.SetValue(newValue);
            this.OnPropertyChanged(propertyName);
        }

        internal void Refresh()
        {
            var url = this.DesignTimeServiceUrl;
            if (String.IsNullOrEmpty(url)) return;
            try
            {
                var basicHttpBinding = new BasicHttpBinding
                {
                    MaxBufferPoolSize = 2147483647,
                    MaxReceivedMessageSize = 2147483647,
                    MaxBufferSize = 2147483647
                };
                var client = new QCServiceClient(basicHttpBinding, new EndpointAddress(url));
                var surveys = client.GetAllSurveys();
                if (surveys.HasErrors)
                {
                    MessageBox.Show(surveys.Message);
                    return;
                }
                this.Surveys = new ObservableCollection<SurveyItem>(surveys.Item.Select(s => new SurveyItem { Id = s.Key, Name = s.Value }));
                this.Survey = this.GetSelectedSurvey();
            }
            catch (Exception ex)
            {
                if (this.Surveys != null) this.Surveys.Clear();
                MessageBox.Show(ex.Message);
            }
        }

        private SurveyItem GetSelectedSurvey()
        {
            var property = this._designer.ModelItem.Properties["Survey"];
            if (property == null || property.Value == null) return this.Surveys.FirstOrDefault();
            var survey = (SurveyItem)property.Value.GetCurrentValue();
            return this.Surveys.FirstOrDefault(e => e.Id == survey.Id);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
