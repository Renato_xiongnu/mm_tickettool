﻿using System.Windows;

namespace TicketTool.Projects.QC.Activities.Design
{
    public partial class SurveyOutcomeActivityDesigner
    {
        private SurveySelectionStrategy _surveyStragegy;

        public SurveyOutcomeActivityDesigner()
        {
            InitializeComponent();
            this.Loaded += this.Refresh;
        }

        public SurveySelectionStrategy SurveyStrategy
        {
            get { return this._surveyStragegy ?? (this._surveyStragegy = new SurveySelectionStrategy(this)); }
        }

        private void Refresh(object sender, RoutedEventArgs e)
        {
            this.SurveyStrategy.Refresh();
        }
    }
}
