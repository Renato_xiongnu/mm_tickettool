﻿using System.ServiceModel;
using TicketTool.Projects.QC.Processor.Contracts;

namespace TicketTool.Projects.QC.Service
{
    [ServiceContract]
    public interface IQCService
    {
        [OperationContract]
        SurveysOperationResult GetAllSurveys();
        
        [OperationContract]
        SurveyOperationResult GetSurvey(int id);
        
        [OperationContract]
        OperationResult CompleteSurvey(SurveyResults results);
    }
}