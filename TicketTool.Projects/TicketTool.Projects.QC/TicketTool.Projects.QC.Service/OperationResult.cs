﻿using System.Collections.Generic;
using TicketTool.Projects.QC.Processor.Contracts;

namespace TicketTool.Projects.QC.Service
{
    public class OperationResult
    {
        public bool HasErrors { get; set; }
        public string Message { get; set; }
    }

    public class OperationResult<T> : OperationResult
    {
        public T Item { get; set; }
    }

    public class SurveysOperationResult : OperationResult<IDictionary<int, string>>
    {
    }

    public class SurveyOperationResult : OperationResult<Survey>
    {
    }
}