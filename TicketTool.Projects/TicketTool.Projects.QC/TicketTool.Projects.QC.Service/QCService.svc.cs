﻿using System;
using System.Diagnostics;
using TicketTool.Projects.QC.Processor;
using TicketTool.Projects.QC.Processor.Contracts;
using TicketTool.Projects.QC.Service.Properties;

namespace TicketTool.Projects.QC.Service
{
    public class QCService : IQCService
    {
        private readonly IProcessor _processor;

        public QCService()
        {
            _processor = Processor.Processor.Default;
        }

        public SurveysOperationResult GetAllSurveys()
        {
            try
            {
                return new SurveysOperationResult {Item = _processor.GetAllSurvey()};
            }
            catch(Exception e)
            {
                Trace.TraceError(String.Join(Environment.NewLine, new[] {e.Message, e.StackTrace}));
                return new SurveysOperationResult {HasErrors = true, Message = e.Message};
            }
        }

        public SurveyOperationResult GetSurvey(int id)
        {
            if(id <= 0)
            {
                return new SurveyOperationResult {HasErrors = true, Message = Resources.Ex_IdentifierMustGreaterZero};
            }

            try
            {
                return new SurveyOperationResult {Item = _processor.GetSurvey(id)};
            }
            catch(Exception e)
            {
                Trace.TraceError(String.Join(Environment.NewLine, new[] {e.Message, e.StackTrace}));
                return new SurveyOperationResult {HasErrors = true, Message = e.Message};
            }
        }

        public OperationResult CompleteSurvey(SurveyResults results)
        {
            if(results.SurveyId <= 0)
            {
                return GetErrorResult(Resources.Ex_IdentifierMustGreaterZero);
            }

            if(results.Answers == null || results.Answers.Length == 0)
            {
                return GetErrorResult(Resources.Ex_NoAnswersFound);
            }

            try
            {
                _processor.CompleteSurvey(results);
                return new OperationResult();
            }
            catch(Exception e)
            {
                return GetErrorResult(e);
            }
        }

        private static OperationResult GetErrorResult(Exception e)
        {
            Trace.TraceError(String.Join(Environment.NewLine, new[] {e.Message, e.StackTrace}));
            return GetErrorResult(e.Message);
        }

        private static OperationResult GetErrorResult(string message)
        {
            return new OperationResult {HasErrors = true, Message = message};
        }
    }
}