﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TicketTool.Projects.QC.Processor;
using TicketTool.Projects.QC.Processor.Contracts;

namespace TicketTool.Projects.QC.Service
{
    [TestClass]
    public class QCServiceTest
    {
        private IQCService _service;
        private Mock<IProcessor> _processorMock;
            
        [TestInitialize]
        public void Init()
        {
            this._service = new QCService();
            this._processorMock = new Mock<IProcessor>();
// ReSharper disable PossibleNullReferenceException
            this._service.GetType().GetField("_processor", BindingFlags.NonPublic | BindingFlags.Instance)
// ReSharper restore PossibleNullReferenceException
                .SetValue(this._service, this._processorMock.Object);
        }

        [TestMethod]
        public void GetAllSurveysTest()
        {
            this._processorMock.Setup(x => x.GetAllSurvey()).Returns(new Dictionary<int, string> {{1, "Survey"}});
            
            OperationResult<IDictionary<int, string>> result = this._service.GetAllSurveys();
            
            Assert.IsNotNull(result);
            Assert.IsFalse(result.HasErrors);
            Assert.IsNotNull(result.Item);
            Assert.IsTrue(result.Item.Any());
            this._processorMock.Verify(x => x.GetAllSurvey());
        }

        [TestMethod]
        public void GetAllSurveysExTest()
        {
            this._processorMock.Setup(x => x.GetAllSurvey()).Throws<Exception>();

            OperationResult<IDictionary<int, string>> result = this._service.GetAllSurveys();
            
            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasErrors);
            Assert.IsFalse(String.IsNullOrEmpty(result.Message));
        }

        [TestMethod]
        public void GetSurveyTest()
        {
            const int id = 10;
            this._processorMock.Setup(x => x.GetSurvey(id))
                .Returns(new Survey
                    {
                        Id = id,
                        Questions = new[]
                            {
                                new Question
                                    {
                                        Options = new[] {new Option()}
                                    }
                            }
                    });

            OperationResult<Survey> result = this._service.GetSurvey(id);
            
            Assert.IsNotNull(result);
            Assert.IsFalse(result.HasErrors);
            Assert.IsNotNull(result.Item);
            Assert.AreEqual(result.Item.Id, 10);
            Survey survey = result.Item;
            Assert.IsTrue(survey.Questions.Any());
            foreach (var question in survey.Questions)
            {
                Assert.IsTrue(question.Options.Any());
            }
            this._processorMock.Verify(x => x.GetSurvey(id));
        }

        [TestMethod]
        public void GetSurveyExTest()
        {
            const int id = 10;
            this._processorMock.Setup(x => x.GetSurvey(id)).Throws<Exception>();

            OperationResult<Survey> result = this._service.GetSurvey(id);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasErrors);
            Assert.IsFalse(String.IsNullOrEmpty(result.Message));
        }

        [TestMethod]
        public void GetSurveyWrongArgTest()
        {
            const int id = -1;
            OperationResult<Survey> result = this._service.GetSurvey(id);
            Assert.IsTrue(result.HasErrors);
            Assert.IsFalse(String.IsNullOrEmpty(result.Message));
            this._processorMock.Verify(x => x.GetSurvey(It.IsAny<int>()), Times.Never);
        }

        [TestMethod]
        public void CompleteSurveyTest()
        {
            const int id = 10;
            var results = new SurveyResults
                {
                    SurveyId = id,
                    Answers = new[]
                        {
                            new Answer {OptionId = 1, QuestionId = 1},
                            new Answer {OptionId = 2, QuestionId = 1}
                        }
                };
            this._processorMock.Setup(x => x.CompleteSurvey(results));

            OperationResult result = this._service.CompleteSurvey(results);

            Assert.IsNotNull(result);
            Assert.IsFalse(result.HasErrors);
            this._processorMock.Verify(x => x.CompleteSurvey(results));
        }

        [TestMethod]
        public void CompleteSurveyExTest()
        {
            const int id = 10;
            var results = new SurveyResults
            {
                SurveyId = id,
                Answers = new[]
                        {
                            new Answer {OptionId = 1, QuestionId = 1},
                            new Answer {OptionId = 2, QuestionId = 1}
                        }
            };
            this._processorMock.Setup(x => x.CompleteSurvey(results)).Throws<Exception>();

            OperationResult result = this._service.CompleteSurvey(results);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasErrors);
            Assert.IsFalse(String.IsNullOrEmpty(result.Message));
        }

        [TestMethod]
        public void CompleteSurveyWithNullResultsTest()
        {
            const int id = 10;
            var results = new SurveyResults {SurveyId = id};

            OperationResult result = this._service.CompleteSurvey(results);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasErrors);
            Assert.IsFalse(String.IsNullOrEmpty(result.Message));
        }

        [TestMethod]
        public void CompleteSurveyWithIncompleteResultsTest()
        {
            const int id = 10;
            var results = new SurveyResults {SurveyId = id, Answers = new Answer[0]};

            OperationResult result = this._service.CompleteSurvey(results);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasErrors);
            Assert.IsFalse(String.IsNullOrEmpty(result.Message));
        }

        [TestMethod]
        public void CompleteSurveyWithIncorrectSurveyIdTest()
        {
            const int id = -10;
            var results = new SurveyResults {SurveyId = id};

            OperationResult result = this._service.CompleteSurvey(results);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasErrors);
            Assert.IsFalse(String.IsNullOrEmpty(result.Message));
        }
    }
}
