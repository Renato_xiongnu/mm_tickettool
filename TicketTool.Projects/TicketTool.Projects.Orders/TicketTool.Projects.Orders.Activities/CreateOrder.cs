﻿using Orders.Backend.Model;
using TicketTool.Activities.Integration;
using TicketTool.Projects.Orders.Activities.Mapping;
using ReturnCode = TicketTool.Projects.Orders.Activities.OrdersProxy.ReturnCode;

namespace TicketTool.Projects.Orders.Activities
{
    public class CreateOrder:ExecuteExternalService<CustomerOrder,string>
    {
        protected override ExecutingResult<string> ExecuteService(CustomerOrder workitem)
        {
            var client = new OrdersProxy.OrderServiceClient();
            var createOrderResult = client.CreateOrder(workitem.ConvertTo<OrdersProxy.CustomerOrder>());

            return new ExecutingResult<string>
                {
                    Success = createOrderResult.ReturnCode == ReturnCode.Ok,
                    Message = createOrderResult.Message,
                    Result = createOrderResult.OrderId
                };
        }
    }
}
