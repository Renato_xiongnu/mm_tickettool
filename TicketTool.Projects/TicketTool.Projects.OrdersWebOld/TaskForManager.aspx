﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TaskForManager.aspx.cs" Inherits="Orders.Backend.Clients.Web.TaskForManager" %>

<!DOCTYPE html>

<%@ Register Src="~/OrderInfoControl.ascx" TagName="OrderInfoControl" TagPrefix="my" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxLoadingPanel" Assembly="DevExpress.Web.v12.2, Version=12.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<html xmlns="http://www.w3.org/1999/xhtml" style="-webkit-text-size-adjust:none">
<head runat="server">
    <link type="text/css" rel="stylesheet" href="~/Content/Site.css">
    <link type="text/css" rel="stylesheet" media="only screen and (max-device-width: 480px)" href="~/Content/Site.css">
    <meta name="viewport"  content="width=550" />
    <title>Задача</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        
        <div  id="mainDiv">
             <br/>
            <br/>
            <br/>
            <br/>
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="mainPanel">
                <ProgressTemplate>
                    <font color="green">Идет загрузка...</font>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <asp:UpdatePanel runat="server" ID="mainPanel">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pContent">
                        <table style="color: #333333; background-color: #F7F6F3;" width="100%">
                            <tr>
                                <td colspan="2" style="border-bottom: 1px solid darkblue; text-align: center; font-size: larger">
                                    <h1>
                                        Задача
                                    </h1>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom: 1px solid darkblue;">Идентификатор заявка</td>
                                <td style="border-bottom: 1px solid darkblue;">
                                    <asp:Literal runat="server" ID="lTicket"></asp:Literal>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom: 1px solid darkblue;">Название задачи</td>
                                <td style="border-bottom: 1px solid darkblue;">
                                    <asp:Literal runat="server" ID="lName"></asp:Literal>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom: 1px solid darkblue;">Время</td>
                                <td style="border-bottom: 1px solid darkblue;">
                                    <asp:Literal runat="server" ID="lTime"></asp:Literal>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom: 1px solid darkblue;vertical-align: top">Описание задачи</td>
                                <td style="border-bottom: 1px solid darkblue;">
                                    <asp:Literal runat="server" ID="lDescription"></asp:Literal>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                        <br />
                        Информация о заказе
                    <br />
                        <div style="border: 1px solid darkblue;">
                            <my:OrderInfoControl runat="server" OrderId="<%#OrderId%>" 
                                CanChangeArticleStatus="<%# CanChangeArticleStatus %>" 
                                CanChangeArticles="<%# CanChangeArticles %>" 
                                InStatusEditMode ="True"
                                InArticleEditMode="False"
                                InPriceEditMode="False"
                                ID="orderControl" />
                        </div>
                        
                    <br />
                        <asp:Panel runat="server" ID="pComment">
                            Комментарий:
                            <br />
                            <asp:Label runat="server" ID="lCommentError" Visible="False" Text="Комментарий здесь обязатлен!" ForeColor="Red" />
                            <br />
                            <asp:TextBox runat="server" ID="tbComment" TextMode="MultiLine" Width="400" Height="100"></asp:TextBox>
                        </asp:Panel>
                    
                        <br />
                        <asp:Panel runat="server" ID="pRequiredFields">
                            <h4>
                                Поля:
                            </h4>
                        </asp:Panel>
                        
                    <br />
                        <asp:ListView runat="server" OnItemDataBound="RequiredFieldBound" ID="lvRequiredFields">
                            <LayoutTemplate>
                                <table  width="100%">
                                    <thead style="background-color: #5D7B9D; color: #FFFFFF;">
                                        <tr>
                                            <th width="30" align="left"></th>
                                            <th>Название поля</th>
                                            <th>Тип поля</th>
                                            <th>Значение</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:PlaceHolder runat="server" ID="itemPlaceholder" />
                                    </tbody>
                                </table>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <asp:HiddenField runat="server" ID="hfName" Value='<%# Eval("Name") %>' />
                                <asp:HiddenField runat="server" ID="hfType" Value='<%# Eval("Type") %>' />
                                <tr id="rows" style="color: #333333; background-color: #F7F6F3;">
                                    <td>
                                        <asp:CheckBox runat="server" ID="selectCheckBox" name="checkboxlist" 
                                            style="-webkit-border-radius:20px;-webkit-text-size-adjust:200%; -webkit-tap-highlight-color:#c80000; -webkit-tap-highlight-color:rgba(200,0,0,0.4);"
                                                Height="30"/>
                                    </td>
                                    <td>
                                        <%# Eval("Name")%>
                                    </td>
                                    <td>
                                        <%# Eval("Type")%>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="tbValue" Width="100" Style="text-align: right" />
                                        <asp:RequiredFieldValidator ID="v3" runat="server"
                                            ControlToValidate="tbValue"
                                            SetFocusOnError="True"
                                            ErrorMessage="Please Enter Only Numbers" ForeColor="Red"
                                            Text="!" ValidationGroup="mainForm" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <asp:HiddenField runat="server" ID="hfName" Value='<%# Eval("Name") %>' />
                                <asp:HiddenField runat="server" ID="hfType" Value='<%# Eval("Type") %>' />
                                <tr id="rows" style="color: #284775; background-color: #FFFFFF;">
                                    <td>
                                        <asp:CheckBox runat="server" ID="selectCheckBox" name="checkboxlist" 
                                            style="-webkit-border-radius:20px;-webkit-text-size-adjust:200%; -webkit-tap-highlight-color:#c80000; -webkit-tap-highlight-color:rgba(200,0,0,0.4);"
                                                Height="30"/>
                                    </td>
                                    <td>
                                        <%# Eval("Name")%>
                                    </td>
                                    <td>
                                        <%# Eval("Type")%>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="tbValue" Width="100" Style="text-align: right" />
                                        <asp:RequiredFieldValidator ID="v3" runat="server"
                                            ControlToValidate="tbValue"
                                            SetFocusOnError="True"
                                            ErrorMessage="Please Enter Only Numbers" ForeColor="Red"
                                            Text="!" ValidationGroup="mainForm" />
                                    </td>
                                </tr>
                            </AlternatingItemTemplate>
                        </asp:ListView>
                        <asp:Label runat="server" ID="lError" Visible="False" Text="Комментарий здесь обязателен!" ForeColor="Red" />
                        <br />
                        <table width="100%">
                            <tr style="color: #284775; background-color: lightblue;">
                                <td>
                                    <asp:Repeater runat="server" ID="rOutcomes">
                                        <ItemTemplate>
                                            <dx:ASPxButton runat="server"
                                                style="-webkit-text-size-adjust:200%; -webkit-tap-highlight-color:#c80000; -webkit-tap-highlight-color:rgba(200,0,0,0.4);"
                                                Height="60"
                                                Text='<%#Eval("Value") %>'
                                                CommandArgument='<%#Eval("Value") + "|" + Eval("IsSuccess") %>' 
                                                OnClick="OutcomeClick" 
                                                ValidationGroup="mainForm">
                                                <ClientSideEvents 
                                                    Click="function(s, e) {LoadingPanel.Show();}" 
                                                    Init="function(s, e) {LoadingPanel.Hide();}"/>
                                            </dx:ASPxButton>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pResult" Visible="False">
                        <asp:Literal runat="server" ID="lResult"></asp:Literal>
                        <dx:ASPxButton runat="server"
                            style="-webkit-text-size-adjust:200%; -webkit-tap-highlight-color:#c80000; -webkit-tap-highlight-color:rgba(200,0,0,0.4);"
                            AutoPostBack="False" 
                            Height="60"
                            Text='Закрыть'>
                            <ClientSideEvents 
                                Click="function(s, e) {window.close();}"
                                Init="function(s, e) {LoadingPanel.Hide();}"/>
                        </dx:ASPxButton>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="mainPanel">
                <ProgressTemplate>
                    <font color="green">Идет загрузка...</font>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <dx:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel"
                Text="Загрузка! Пожалуйста, подождите!"
                ContainerElementID="mainDiv" Modal="True">
            </dx:ASPxLoadingPanel>
        </div>
    </form>
</body>
</html>
