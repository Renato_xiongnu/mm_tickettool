﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using OnlineOrders.Common;
using Orders.Backend.Clients.Web.Common;
using Orders.Backend.Clients.Web.Common.Configuration;
using Orders.Backend.Clients.Web.TicketTool;

namespace Orders.Backend.Clients.Web
{
    public partial class TaskForManager : Page
    {    

        public string OrderId;
        public bool CanChangeArticleStatus = true;
        public bool CanChangeArticles = true;
        private string _taskId;

        protected void Page_Load(object sender, EventArgs e)
        {
            _taskId = string.Empty;

            var sapCode = Request.QueryString[Settings.SapCodeParameter];

            var isWrongUrl = true;
            if (Request.QueryString.AllKeys.Contains(Settings.TaskIdParameter))
            {
                _taskId = Request.QueryString[Settings.TaskIdParameter];
                var key1 = Request.QueryString[Settings.KeyParameter];
                var key2 = Encoder.Encode(_taskId);                
                var manager = Request.QueryString[Settings.ManagerParameter];

                //       if key1 == key2 => login managerStr (get user name from qstring)
                //       else throw exception (wrong url)
                //       add additional params: C:\Projects\svn\BACKEND\trunk\Orders.Backend\Orders.Backend.Services.TicketTool.Data\ManagerMailer.cs
                if (string.Compare(key1, key2, StringComparison.InvariantCultureIgnoreCase) == 0)
                {                    
                    isWrongUrl = LoginManager(string.Format(Settings.DefaultManagerFormat, sapCode), manager);
                }

                if (isWrongUrl)
                {
                    pContent.Visible = false;
                    pResult.Visible = true;
                    lResult.Text = "----";
                    return;
                }
            }
            if (string.IsNullOrEmpty(_taskId))
            {
                pContent.Visible = false;
                pResult.Visible = true;
                lResult.Text = "Не правильный параметр задачи!";
                return;
            }
            

            pContent.Visible = true;
            pResult.Visible = false;
            

            var ticketService = Services.GetTicketToolService();
            try
            {
                if (!IsPostBack)
                {
                    lCommentError.Visible = false;
                    lError.Visible = false;
                    var task = AuthContext.Auth.Execute(() => ticketService.GetNextTaskById(_taskId), ticketService.InnerChannel);
                    
                    if (task == null)
                    {
                        pContent.Visible = false;
                        pResult.Visible = true;
                        lResult.Text = "Задача не существует либо у вас на нее нет прав!";
                        return;
                    }
                    //lTime.Text = task..ToString("yyyy-MM-dd HH:mm:ss");
                    lName.Text = task.Name;
                    lDescription.Text = task.Description;
                    lTicket.Text = task.TicketId;
                    //lType.Text = task.Type;

                    //task.Outcomes.First().CommentRequeried
                    SetOrderId(task);
                    
                    orderControl.DataBind();
                    
                    lvRequiredFields.DataSource = task.RequiredFields;
                    lvRequiredFields.DataBind();
                    pRequiredFields.Visible = task.RequiredFields.Count() > 0;

                    rOutcomes.DataSource = task.Outcomes;
                    rOutcomes.DataBind();
                    //todo delete panel for comment
                   // var commentReq = task.Outcomes.Any(o => o.CommentRequeried);
                    pComment.Visible = false;
                }

                //ticketService.FinishTicketTask()
            }
            catch (Exception exp)
            {
                pContent.Visible = false;
                pResult.Visible = true;
                lResult.Text = string.Format("Проблема с задачей либо она не доступна для Вас! \n {0}", exp);
            }

        }

        private void SetOrderId(TicketTask task)
        {
            var orderUrl = task.RelatedContent == null || task.RelatedContent.Url == null
                               ? ""
                               : task.RelatedContent.Url.AbsoluteUri.ToLowerInvariant();
            int index = orderUrl.LastIndexOf(Settings.OrderMarkerParam + "=");
            if (index > 0)
            {
                index = index + 3;
                OrderId = orderUrl.Substring(index, orderUrl.Length - index);
            }
            else
            {
                OrderId = "";
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            var inEditMode = orderControl.InEditMode;
          //  rOutcomes.Visible = !inEditMode;
         //   tbComment.Enabled = !inEditMode;
        }

        private bool LoginManager(string userName, string managerStr)
        {
            bool isWrongUrl = true;
            var encodeManager = Encoder.Encode(userName);
            if (string.Compare(encodeManager, managerStr, StringComparison.InvariantCultureIgnoreCase) == 0)
            {
                var authService = Services.GetTicketToolAuthService();
                isWrongUrl = ! AuthContext.Auth.Login(() => authService.LogIn(userName, Settings.DefaultManagerPassword),
                                       authService.InnerChannel);
                //isWrongUrl = false;
            }
            return isWrongUrl;
        }

        protected void RequiredFieldBound(object sender, ListViewItemEventArgs e)
        {
            var tbValue = e.Item.FindControl("tbValue") as TextBox;
            var rf = ((RequiredField)e.Item.DataItem);
            tbValue.Text = rf.Value == null ? string.Empty : rf.Value.ToString();
        }

        protected void OutcomeClick(object sender, EventArgs e)
        {
            var b = (ASPxButton)sender;
            var commandArgument = b.CommandArgument;
            var outcomeParameters = commandArgument.Split('|');
            var outcomeValue = outcomeParameters[0];
            var outcomeIsSuccessStr = outcomeParameters[1];
          /*  var comment = tbComment.Text;
            if (string.IsNullOrEmpty(comment))
            {
                lCommentError.Visible = true;
                return;
            }
            
           
            lCommentError.Visible = false;
            */
            var saveorder = orderControl.SaveOrder();
            //для хороших исходов заказ обязательно должен сохраняться
            if (outcomeIsSuccessStr == "True" && !saveorder)
            {
                lError.Text = "Проблема с сохранением заказа! Проверьте все строки";
                lError.Visible = true;
                return;
            }

      //      comment = string.IsNullOrEmpty(comment) ? saveorder.Value : string.Format("{0}\\n{1}", comment, saveorder.Value);
            var requiredFields = new List<RequiredField>();
            foreach (var field in lvRequiredFields.Items)
            {
                var hfName = (HiddenField)field.FindControl("hfName");
                var hfType = (HiddenField)field.FindControl("hfType");
                var tbValue = (TextBox)field.FindControl("tbValue");
                var value = tbValue.Text;
                if (string.IsNullOrEmpty(value))
                {
                    return;
                }
                requiredFields.Add(new RequiredField
                                       {
                                           Name = hfName.Value,
                                           Type = hfType.Value,
                                           Value = value
                                       });
            }
            try
            {
                var ticketService = Services.GetTicketToolService();
                var result = AuthContext.Auth.Execute(() => 
                    ticketService.FinishTicketTask(_taskId, outcomeValue, requiredFields.ToArray()), ticketService.InnerChannel);                

                if (result.HasError)
                {
                    //Error
                    lError.Text = result.MessageText;
                    lError.Visible = true;
                }
                else
                {
                    lCommentError.Visible = false;
                    lError.Visible = false;
                    pContent.Visible = false;
                    pResult.Visible = true;
                    lResult.Text = string.Format("Спасибо! Задача выполнена.");
                }
            }
            catch (Exception exp)
            {
                lError.Text = "Проблема на стороне сервера или с соединением: " + exp;
                lError.Visible = true;
            }

        }
    }
}