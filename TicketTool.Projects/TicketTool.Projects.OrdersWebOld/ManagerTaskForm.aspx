﻿<%@ Page Language="C#" AutoEventWireup="true"
    CodeBehind="ManagerTaskForm.aspx.cs"
    Inherits="TicketTool.Projects.OrderTaskForm.ManagerTaskForm" 
    ValidateRequest="false"%>
<%@ Import Namespace="TicketTool.Projects.OrderTaskForm.Common" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Задача: <%= this.TaskName %></title>
    <%--<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet"/>--%>
    <link href="ManagerTaskForm/bootstrap-combined.min.css" rel="stylesheet"/>
    <meta http-equiv="cache-control" content="no-cache, no-store, max-age=0, must-revalidate"/>
    <meta http-equiv="expires" content="0"/>
    <meta name="viewport"  content="width=450, initial-scale=0.7, maximum-scale=0.7, user-scalable=0" />
    <!--, initial-scale=1, maximum-scale=1, user-scalable=0-->
    <style type="text/css">
        .modal {
            max-height: 90%;
            overflow-y: auto;

          
        }
        .btn-custom {
          background-color: hsl(214, 37%, 28%) !important;
          background-repeat: repeat-x;
          filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#7a99c1", endColorstr="#2c4361");
          background-image: -khtml-gradient(linear, left top, left bottom, from(#7a99c1), to(#2c4361));
          background-image: -moz-linear-gradient(top, #7a99c1, #2c4361);
          background-image: -ms-linear-gradient(top, #7a99c1, #2c4361);
          background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #7a99c1), color-stop(100%, #2c4361));
          background-image: -webkit-linear-gradient(top, #7a99c1, #2c4361);
          background-image: -o-linear-gradient(top, #7a99c1, #2c4361);
          background-image: linear-gradient(#7a99c1, #2c4361);
          border-color: #2c4361 #2c4361 hsl(214, 37%, 19.5%);
          color: #fff !important;
          text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.56);
          -webkit-font-smoothing: antialiased;
        }
    </style>
</head>
<body>
    <%--<script src="http://code.jquery.com/jquery-latest.js"></script>--%>
    <script src="ManagerTaskForm/jquery-latest.js"></script>
    <script src="Scripts/ManagerTaskForm.js"></script>
    <script src="Scripts/bootbox.min.js"></script>
    <%--<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js"></script>--%>
    <script src="ManagerTaskForm/bootstrap.min.js"></script>
    <script type="text/javascript">
        
        var EmptyState = '<%Response.Write(ReviewItemState.Empty); %>';
        var IncorrectPriceState = '<%Response.Write(ReviewItemState.IncorrectPrice); %>';
        var NotFoundState = '<%Response.Write(ReviewItemState.NotFound); %>';
        var ReservedState = '<%Response.Write(ReviewItemState.Reserved); %>';
        var ReservedDisplayItemState = '<%Response.Write(ReviewItemState.ReservedDisplayItem); %>';
        var ReservedPartState = '<%Response.Write(ReviewItemState.ReservedPart); %>';
        
        var ArticleConditionNewState = '<%Response.Write(ItemState.New); %>';
        var ArticleConditionDisplayItemState = '<%Response.Write(ItemState.DisplayItem); %>';
        var ArticleConditionRepairedState = '<%Response.Write(ItemState.Repaired); %>';

    // document.body.style.webkitTransformOrigin= "0% 0%";
       // document.body.style.webkitTransform = "scale(1.1)";
    </script>
    <form id="form1" runat="server"
        method="post" onsubmit="return checkform()">
        <% if (this.TaskExist)
           { %>
        <h4>Заказ № <%= this.OrderWWSOrderId %> (<%= this.OrderSapCode %> - <%= this.OrderId %>)</h4>
        <h5>Задача: <%= this.TaskName %></h5>
        <div class="row">
            <div class="span12">
                <table class="table table-striped">
                    <tr>
                        <td>Время на выполнение задачи</td>
                        <td><%= this.TaskTime %></td>
                    </tr>
                    <tr>
                        <td colspan="2"><%= this.TaskDescription %></td>
                    </tr>
                    <tr>
                        <td>Откуда поступил заказ</td>
                        <td><%= this.OrderSource %></td>
                    </tr>
                    <tr>
                        <td>Тип оплаты</td>
                         <% if (OrderPaymentShouldBeBold)
                            {%>
                            <td><b><%= this.OrderPaymentType %></b></td>
                         <%}%>
                         <%else
                           {%>
                            <td><%= this.OrderPaymentType %></td>
                          <%}%>
                    </tr>
                    <% if (false)
                       { %>
                    <tr>
                        <td>Статус</td>
                        <td><%= this.OrderStatusInfo %></td>
                    </tr>
                    <% } %>
                    <tr>
                        <td>Время обновления заказа (МСК)</td>
                        <td><%= this.OrderUpdateDate %></td>
                    </tr>
                    
                    <tr>
                        <td>Имя покупателя</td>
                        <td><%= this.OrderCustomerName %></td>
                    </tr>
                    <tr>
                        <td>Почта</td>
                        <td><a href="mailto:<%= this.OrderCustomerEmail %>"><%= this.OrderCustomerEmail %></a></td>
                    </tr>
                    <tr>
                        <td>Телефон</td>
                        <td><%= this.OrderCustomerTelephone %></td>
                    </tr>
                    <% if (HaveDelivery)
                       { %>
                    <tr>
                        <td>Город доставки</td>
                        <td><%=DeliveryCity %></td>
                    </tr>
                    <tr>
                        <td>Адрес доставки</td>
                        <td><%=DeliveryAddress %></td>
                    </tr>
                    <% } %>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="span12">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>№</th>
                            <th>Артикул</th>
                            <th>Цена</th>
                            <th>Кол-во</th>
                        </tr>
                    </thead>
                    <tbody>
                        <% foreach (var line in this.ReserveLines)
                           {
                        %>
                        <tr>
                            <td rowspan="1"><% Response.Write("" + line.Number); %></td>
                            <td colspan="3"><% Response.Write(string.IsNullOrEmpty(line.Title) ? "[название не известно]" : line.Title); %></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><% Response.Write(line.ArticleNum); %></td>
                            <td><% Response.Write(string.Format("<font {1}>{0}</font>",
                                                                line.Price.ToString(PriceFormat),
                                                                (line.ValidPrice ? "" : "color=\"#FF0000\""))); %></td>
                            <td><% Response.Write(string.Format("<font {1}>{0}</font>",
                                                                line.Qty,
                                                                (line.ValidQty ? "" : "color=\"#FF0000\""))); %></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="3">
                                <input type="hidden"
                                        name="<% Response.Write("rowLineId" + line.Number); %>"
                                        id="<% Response.Write("rowLineId" + line.Number); %>"
                                       value="<% Response.Write("" + line.LineId); %>"/>
                                <input type="hidden"
                                        name="<% Response.Write("rowArticle" + line.Number); %>"
                                        id="<% Response.Write("rowArticle" + line.Number); %>"
                                       value="<% Response.Write("" + line.ArticleNum); %>"/>
                                <input type="hidden"
                                        name="<% Response.Write("rowPrice" + line.Number); %>"
                                        id='<% Response.Write("rowPrice" + line.Number); %>'
                                        value="<% Response.Write(line.Price.ToString(PriceFormat)); %>"/>
                                <input type="hidden"
                                    name="<% Response.Write("rowQty" + line.Number); %>"
                                    id='<% Response.Write("rowQty" + line.Number); %>'
                                    value="<% Response.Write("" + line.Qty); %>"/>
                                <input type="hidden"
                                    name="<% Response.Write("rowCommentLog" + line.Number); %>"
                                    id="<% Response.Write("rowCommentLog" + line.Number); %>"
                                    value="<% Response.Write("" + line.CommentLog); %>"/>
                                <input type="hidden"
                                    name="<% Response.Write("rowTitle" + line.Number); %>"
                                    id="<% Response.Write("rowTitle" + line.Number); %>"
                                    value="<% Response.Write("" + line.Title); %>"/>
                                
                                <input type="hidden"
                                    name="<% Response.Write("WWSDepartmentNo" + line.Number); %>"
                                    id="<% Response.Write("WWSDepartmentNo" + line.Number); %>"
                                    value="<% Response.Write("" + line.WWSDepartmentNumber); %>"/>
                                <input type="hidden"
                                    name="<% Response.Write("WWSProductGroup" + line.Number); %>"
                                    id="<% Response.Write("WWSProductGroup" + line.Number); %>"
                                    value="<% Response.Write("" + line.WWSProductGroupName); %>"/>
                                <input type="hidden"
                                    name="<% Response.Write("WWSProductGroupNo" + line.Number); %>"
                                    id="<% Response.Write("WWSProductGroupNo" + line.Number); %>"
                                    value="<% Response.Write("" + line.WWSProductGroupNo); %>"/>
                                <input type="hidden"
                                    name="<% Response.Write("WWSStockNo" + line.Number); %>"
                                    id="<% Response.Write("WWSStockNo" + line.Number); %>"
                                    value="<% Response.Write("" + line.WWSStockNumber); %>"/>
                                <input type="hidden"
                                    name="<% Response.Write("WWSFreeQty" + line.Number); %>"
                                    id="<% Response.Write("WWSFreeQty" + line.Number); %>"
                                    value="<% Response.Write("" + line.WWSFreeQty); %>"/>
                                <input type="hidden"
                                    name="<% Response.Write("WWSReservedQty" + line.Number); %>"
                                    id="<% Response.Write("WWSReservedQty" + line.Number); %>"
                                    value="<% Response.Write("" + line.WWSReserverQuantity); %>"/>
                                <input type="hidden"
                                    name="<% Response.Write("WWSPriceOrig" + line.Number); %>"
                                    id="<% Response.Write("WWSPriceOrig" + line.Number); %>"
                                    value="<% Response.Write("" + line.WWSPriceOrig); %>"/>

                                <%
                               var prs = (line.Promotions == null || line.Promotions.Count == 0 ? "нет" : (string.Format("<font color=\"red\">{0}</font>", string.Join(", ", line.Promotions))));
                                    Response.Write(string.Format("Отдел <b>{0}</b>; ТГ <b>{1} ({3})</b>; Скл. № <b>{2}</b>; Акции: <b>{4}</b>", 
                                               line.WWSDepartmentNumber, line.WWSProductGroupNo, line.WWSStockNumber, line.WWSProductGroupName,
                                               prs)); %>
                            </td>
                            
                        </tr>
                        <tr style="display: none">
                            <td></td>
                            <td  style="background-color: lightgreen;">
                                <% Response.Write("" + ManagerUIExtension.GetItemStateDisplayName(line.ArticleConditionState)); %>
                            </td>
                            <td style="background-color: lightgreen;">
                                <% Response.Write("" + line.StockPrice); %>
                            </td>
                            <td style="background-color: lightgreen;">
                                <% Response.Write("" + line.StockQty); %>
                            </td>
                        </tr>
                        <tr id="<% Response.Write("rowConditionStateTr" + line.Number); %>"
                            <% Response.Write(line.ArticleConditionState != ItemState.New ? "" : "style=\"display: none\""); %>>
                            <td></td>
                            <td colspan="3">
                                Состояние товара:<br/>
                                <input type="hidden" 
                                    name="<% Response.Write("rowConditionState" + line.Number); %>"
                                    
                                    id="<% Response.Write("rowConditionState" + line.Number); %>"
                                    value="<% Response.Write(line.ArticleConditionState); %>">
                                    
                                </input>
                                <textarea rows="3" placeholder="Описание состояния…"
                                    readonly="readonly"
                                    id="<% Response.Write("rowConditionStateComment" + line.Number); %>"
                                    name="<% Response.Write("rowConditionStateComment" + line.Number); %>"><% Response.Write(line.ArticleCondition); %></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="3">Выберите ответ 
                                <select name="<% Response.Write("rowState" + line.Number); %>"
                                    id='<% Response.Write("rowState" + line.Number); %>'
                                    onchange="updateInput('<% Response.Write(line.Number); %>', this.value)"
                                    <% Response.Write(this.CanEdit ? "" : "disabled=\"disabled\""); %>>
                                    <option <% Response.Write(line.ReviewItemState == ReviewItemState.Empty ? "selected=\"selected\"" : ""); %>
                                        value="<% Response.Write(ReviewItemState.Empty); %>">
                                        <% Response.Write(ManagerUIExtension.GetReviewItemStateDisplayName(ReviewItemState.Empty)); %>
                                    </option>
                                    <option <% Response.Write(line.ReviewItemState == ReviewItemState.Reserved ? "selected=\"selected\"" : ""); %>
                                        value="<% Response.Write(ReviewItemState.Reserved); %>">
                                        <% Response.Write(ManagerUIExtension.GetReviewItemStateDisplayName(ReviewItemState.Reserved)); %>
                                    </option>
                                    <option <% Response.Write(line.ReviewItemState == ReviewItemState.NotFound ? "selected=\"selected\"" : ""); %>
                                        value="<% Response.Write(ReviewItemState.NotFound); %>">
                                        <% Response.Write(ManagerUIExtension.GetReviewItemStateDisplayName(ReviewItemState.NotFound)); %>
                                    </option>
                                    <option <% Response.Write(line.ReviewItemState == ReviewItemState.IncorrectPrice ? "selected=\"selected\"" : ""); %>
                                        value="<% Response.Write(ReviewItemState.IncorrectPrice); %>">
                                        <% Response.Write(ManagerUIExtension.GetReviewItemStateDisplayName(ReviewItemState.IncorrectPrice)); %>
                                    </option>
                                    <option <% Response.Write(line.ReviewItemState == ReviewItemState.ReservedPart ? "selected=\"selected\"" : ""); %>
                                        value="<% Response.Write(ReviewItemState.ReservedPart); %>">
                                        <% Response.Write(ManagerUIExtension.GetReviewItemStateDisplayName(ReviewItemState.ReservedPart)); %>
                                    </option>
                                    <option <% Response.Write(line.ReviewItemState == ReviewItemState.ReservedDisplayItem ? "selected=\"selected\"" : ""); %>
                                        value="<% Response.Write(ReviewItemState.ReservedDisplayItem); %>">
                                        <% Response.Write(ManagerUIExtension.GetReviewItemStateDisplayName(ReviewItemState.ReservedDisplayItem)); %>
                                    </option>
                                </select>
                            </td>
                        </tr>
                        <tr id="<% Response.Write("rowCorrectTr" + line.Number); %>"
                                style="display: none">
                            <td>
                                
                            </td>
                            <td colspan="3">
                                <span id="<% Response.Write("rowCorrectPriceTr" + line.Number); %>"
                                    style="display: none">
                                    Предлагаемая цена: 
                                    <label class="label"
                                    id='<% Response.Write("rowCorrectPrice" + line.Number); %>'>Default</label>
                                    <input type="hidden"
                                        id='<% Response.Write("hRowCorrectPrice" + line.Number); %>'
                                        name="<% Response.Write("hRowCorrectPrice" + line.Number); %>"
                                        value="<% Response.Write("" + line.Price.ToString(PriceFormat)); %>"/>;
                                </span>
                                <span id="<% Response.Write("rowCorrectQtyTr" + line.Number); %>"
                                      style="display: none">
                                    Доступное кол-во: 
                                    <label class="label"
                                        id='<% Response.Write("rowCorrectQty" + line.Number); %>'>Default</label>
                                    <input type="hidden"
                                        id='<% Response.Write("hRowCorrectQty" + line.Number); %>'
                                        name="<% Response.Write("hRowCorrectQty" + line.Number); %>"
                                        value="<% Response.Write("" + line.Qty); %>"/>
                                </span>
                            </td>
                        </tr>
                        <tr id="<% Response.Write("rowAltTr1" + line.Number); %>"
                            style="display: none">
                            <td>
                                
                            </td>
                            <td colspan="3">
                                Альтернатива:
                                <input type="hidden"
                                    id='<% Response.Write("hRowAltDisplayItem" + line.Number); %>'
                                    name="<% Response.Write("hRowAltDisplayItem" + line.Number); %>"/>
                                <span  id="<% Response.Write("rowAltTr2" + line.Number); %>"
                                       style="display: none">
                                    <label class="label"
                                        id='<% Response.Write("rowAltArticle" + line.Number); %>'>Default</label>
                                    (<label class="label"
                                        id='<% Response.Write("rowAltDisplayItem" + line.Number); %>'>Default</label>)
                                    <input type="hidden"
                                        id='<% Response.Write("hRowAltArticle" + line.Number); %>'
                                        name="<% Response.Write("hRowAltArticle" + line.Number); %>"/>
                                    ;
                                    Цена:
                                    <label class="label"
                                        id='<% Response.Write("rowAltPrice" + line.Number); %>'>Default</label>
                                    <input type="hidden"
                                        id='<% Response.Write("hRowAltPrice" + line.Number); %>'
                                        name="<% Response.Write("hRowAltPrice" + line.Number); %>"/>
                                    ;
                                    Кол-во:
                                    <label class="label"
                                        id='<% Response.Write("rowAltQty" + line.Number); %>'>Default</label>
                                    <input type="hidden"
                                        id='<% Response.Write("hRowAltQty" + line.Number); %>'
                                        name="<% Response.Write("hRowAltQty" + line.Number); %>"/>
                                    <label class="label"
                                        id='<% Response.Write("rowAltDisplayItemCondition" + line.Number); %>'>Default</label>
                                    <input type="hidden"
                                        id="<% Response.Write("hRowAltDisplayItemCondition" + line.Number); %>"
                                        name="<% Response.Write("hRowAltDisplayItemCondition" + line.Number); %>"/>
                                </span>

                            </td>
                        </tr>
                        <tr <% Response.Write(string.IsNullOrEmpty(line.CommentLog) ? "style=\"display: none\"" : ""); %>>
                            <td>
                                
                            </td>
                            <td colspan="3">
                                <div><% Response.Write(line.CommentLog); %></div>
                            </td>
                        </tr>
                        <tr id="<% Response.Write("rowCommentTr" + line.Number); %>"
                            style="display: none">
                            <td></td>
                            <td colspan="3">
                                <textarea rows="3" placeholder="Комментарий…" readonly="readonly"
                                    id="<% Response.Write("rowComment" + line.Number); %>"
                                    name="<% Response.Write("rowComment" + line.Number); %>"><% Response.Write(line.Comment); %></textarea>
                            </td>
                        </tr>
                        <tr class="success">
                            <td colspan="4"></td>
                        </tr>
                        <% } %>
                    </tbody>
                </table>
            </div>
        </div>
        <div style="display: none">
            <div class="row">
                <div class="span12">
                    <table class="table table-bordered">
                        <caption><h4>Поля</h4></caption>
                        <thead>
                            <tr>
                                <th>Название</th>
                                <th>Значение</th>
                            </tr>
                        </thead>
                        <tbody>
                            <% foreach (var field in this.RequiredFields)
                               {
                            %>
                            <tr>
                                <td><% Response.Write("" + field.Name); %></td>
                                <td>
                                    <input type="text" 
                                        placeholder="введите текст…">
                                
                                </td>
                            </tr>
                            <% } %>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <% if (HaveAnyRequiredField)
               { %> 
            
        <div class="span12 accordion" id="accordionOutcomes">
            <% foreach (var outcome in this.Outcomes)
               {
            %>
            <input type="hidden"
                   id="hOutcomeFieldsCount<% Response.Write(outcome.Number); %>"
                   name="hOutcomeFieldsCount<% Response.Write(outcome.Number); %>"
                    value="<%= outcome.Fields.Count + this.RequiredFields.Count %>"/>
          <div class="accordion-group">
            <div class="accordion-heading">
              <a class="accordion-toggle btn btn-custom" 
                  data-toggle="collapse" 
                  data-parent="#accordionOutcomes" 
                  href="#collapseOutcome<% Response.Write(outcome.Number); %>"
                  style="text-align: left">
                <i class="icon-chevron-down  icon-white"></i> <% Response.Write(string.Format("{0} (заполнить полей: {1})", outcome.Name, outcome.FieldsCount)); %>
              </a>
            </div>
            <div id="collapseOutcome<% Response.Write(outcome.Number); %>" class="accordion-body collapse">
              <div class="accordion-inner" style="background: lavender">
                  <table>
                      
                    <% int i = 0;
                       foreach (var field in this.RequiredFields.Union(outcome.Fields))
                       {
                           i++;
                    %>
                      <tr>
                          <td>
                              <% Response.Write("" + field.Name); %>
                              <input type="hidden"
                                   id="hOutcomeFieldType<% Response.Write("" + outcome.Number + "_" + i); %>"
                                   name="hOutcomeFieldType<% Response.Write("" + outcome.Number + "_" + i); %>"
                                   value="<%= field.Type %>"/>
                              <input type="hidden"
                                   id="hOutcomeFieldTypeName<% Response.Write("" + outcome.Number + "_" + i); %>"
                                   name="hOutcomeFieldTypeName<% Response.Write("" + outcome.Number + "_" + i); %>"
                                   value="<%= field.TypeName %>"/>
                              <input type="hidden"
                                   id="hOutcomeFieldName<% Response.Write("" + outcome.Number + "_" + i); %>"
                                   name="hOutcomeFieldName<% Response.Write("" + outcome.Number + "_" + i); %>"
                                   value="<%= field.Name %>"/>
                              <input type="hidden"
                                   id="hOutcomeFieldValue<% Response.Write("" + outcome.Number + "_" + i); %>"
                                   name="hOutcomeFieldValue<% Response.Write("" + outcome.Number + "_" + i); %>"
                                   value="<%= field.Value %>"/>
                              <input type="hidden"
                                   id="hOutcomeFieldTaskField<% Response.Write("" + outcome.Number + "_" + i); %>"
                                   name="hOutcomeFieldTaskField<% Response.Write("" + outcome.Number + "_" + i); %>"
                                   value="<%= field.TaskField %>"/>
                          </td>
                          <td>
                              <% if (field.Type == RequiredFieldType.Boolean)
                                 { %>
                                  <label class="checkbox">
                                      <input type="checkbox"
                                          id="rf_<% Response.Write("" + outcome.Number + "_" + i); %>"
                                          name="rf_<% Response.Write("" + outcome.Number + "_" + i); %>"/> выбрать
                                  </label>
                              <% }
                                 else if (field.Type == RequiredFieldType.Choice)
                                 { %>
                                <select
                                    id="rf_<% Response.Write("" + outcome.Number + "_" + i); %>"
                                    name="rf_<% Response.Write("" + outcome.Number + "_" + i); %>">
                                    <% foreach (var value in field.PredefinedValues)
                                       { %>
                                        <option value="<% Response.Write(string.Format("{0}", value)); %>"><% Response.Write(string.Format("{0}", value)); %></option>
                                       <% } %>
                                </select>
                              <% }
                                 else if (field.Type == RequiredFieldType.DateTime)
                                 { %>
                                  <input type="date" placeholder="введите дату…" class="input-medium"
                                    id="rf_<% Response.Write("" + outcome.Number + "_" + i); %>"
                                    name="rf_<% Response.Write("" + outcome.Number + "_" + i); %>"/>
                              <% }
                                 else if (field.Type == RequiredFieldType.Decimal)
                                 { %>
                                  <input type="text" placeholder="введите число…"
                                    id="rf_<% Response.Write("" + outcome.Number + "_" + i); %>"
                                    name="rf_<% Response.Write("" + outcome.Number + "_" + i); %>"/>
                              <% }
                                 else if (field.Type == RequiredFieldType.Double)
                                 { %>
                                  <input type="text" placeholder="введите число…"
                                      id="rf_<% Response.Write("" + outcome.Number + "_" + i); %>"
                                    name="rf_<% Response.Write("" + outcome.Number + "_" + i); %>"/>
                              <% }
                                 else if (field.Type == RequiredFieldType.Int32)
                                 { %>
                                  <input type="number" placeholder="введите целое число…"
                                      id="rf_<% Response.Write("" + outcome.Number + "_" + i); %>"
                                    name="rf_<% Response.Write("" + outcome.Number + "_" + i); %>"/>
                              <% }
                                 else if (field.Type == RequiredFieldType.String)
                                 { %>
                                  <input type="text" placeholder="введите текст…"
                                      id="rf_<% Response.Write("" + outcome.Number + "_" + i); %>"
                                    name="rf_<% Response.Write("" + outcome.Number + "_" + i); %>"/>
                              <% }
                                 else if (field.Type == RequiredFieldType.TimeSpan)
                                 { %>
                                <select class="input-small"
                                    id="rf_<% Response.Write("d_" + outcome.Number + "_" + i); %>"
                                    name="rf_<% Response.Write("d_" + outcome.Number + "_" + i); %>">
                                    <% for (int d = 0; d <= 31; d++)
                                       { %>
                                        <option value="<% Response.Write(string.Format("{0}", d)); %>"><% Response.Write(string.Format("{0} дней", d)); %></option>
                                       <% } %>
                                </select>
                                <select class="input-small"
                                    id="rf_<% Response.Write("h_" + outcome.Number + "_" + i); %>"
                                    name="rf_<% Response.Write("h_" + outcome.Number + "_" + i); %>">
                                    <% for (int d = 0; d <= 24; d++)
                                       { %>
                                        <option value="<% Response.Write(string.Format("{0}", d)); %>"><% Response.Write(string.Format("{0} часов", d)); %></option>
                                       <% } %>
                                </select>
                                <select class="input-small"
                                    id="rf_<% Response.Write("m_" + outcome.Number + "_" + i); %>"
                                    name="rf_<% Response.Write("m_" + outcome.Number + "_" + i); %>">
                                    <% for (int d = 0; d < 12; d++)
                                       { %>
                                        <option value="<% Response.Write(string.Format("{0}", d*5)); %>"><% Response.Write(string.Format("{0} минут", d*5)); %></option>
                                       <% } %>
                                 </select>
                              <% } %>
                          </td>
                      </tr>
                    <% } %>
                </table>
                <button class="btn btn-primary btn-large" type="submit" onclick="saveOutcome('<% Response.Write(outcome.Name); %>', <% Response.Write(outcome.IsSuccess ? "true" : "false"); %>, <% Response.Write(outcome.SkipTaskRequeriedFields ? "true" : "false"); %>, <% Response.Write(outcome.Number); %>)">Завершить</button>
              </div>
            </div>
          </div>
            <% } %>
        </div>
        <% } else { %>
            <div class="span12">
                <% foreach (var outcome in this.Outcomes)
                   {
                %>
                <input type="hidden"
                   id="hOutcomeFieldsCount<% Response.Write(outcome.Number); %>"
                   name="hOutcomeFieldsCount<% Response.Write(outcome.Number); %>"
                    value="<%= outcome.Fields.Count + this.RequiredFields.Count %>"/>
                <button class="btn btn-primary btn-large btn-block" type="submit" 
                    onclick="saveOutcome('<% Response.Write(outcome.Name); %>', <% Response.Write(outcome.IsSuccess ? "true" : "false"); %>, <% Response.Write(outcome.SkipTaskRequeriedFields ? "true" : "false"); %>, <% Response.Write(outcome.Number); %>)"><% Response.Write(outcome.Name); %></button>
                <% } %>
            </div>
        <% } %>
        </div>
        <div class="row">
            <div class="span12">
                <input type="hidden"
                   id="hCanEdit"
                   name="hCanEdit"
                    value="<%=this.CanEdit %>"/>
                <input type="hidden"
                   id="hOutcomeName"
                   name="hOutcomeName"/>
                <input type="hidden"
                   id="hOutcomeNumber"
                   name="hOutcomeNumber"/>
                <input type="hidden"
                   id="hOutcomeIsSuccess"
                   name="hOutcomeIsSuccess"/>
                <input type="hidden"
                   id="hOutcomeSkipTaskRequeriedFields"
                   name="hOutcomeSkipTaskRequeriedFields"/>
                <input type="hidden"
                   id="hRowsCount"
                   name="hRowsCount"/>
                <input type="hidden"
                   id="hOrderId"
                   name="hOrderId"
                    value="<% Response.Write(this.OrderId); %>"/>
                    <table width="100%">
                        <tr>
                            <td width="100%">
                                <button class="btn btn-danger btn-large btn-block" type="submit" onclick="saveOutcome('CancelOutcome', false, true, -1)">Завершить позднее</button>
                            </td>
                        </tr>
                    </table>
            </div>
        </div>
        <% } else { %>
            <div class="row">
                <div class="span6">
                    <h4><%= this.StatusMessage %></h4>
                    <a class="btn btn-danger btn-large btn-block" onclick="window.open('', '_self', '');window.close();">Закрыть</a>
                    <table class="table table-striped">
                        <tr>
                            <td ><%= this.ErrorMessage %></td>
                        </tr>
                    </table>
                </div>
            </div>
        <% } %>
        <!-- Modal NotFound-->
        <div id="modalNotFound" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-header">
            <a href="#"  class="close" onclick="closeModalNotFound(true)">×</a>
            <h6 id="myModalLabel">
                <label id="modalNotFoundCurrentArticle"></label>
            </h6>
          </div>
          <div class="modal-body">
            <table class="table table-condensed">
                <tr class="info">
                    <td colspan="2">
                        Если возможно предложите альтернативу
                    </td>
                </tr>
                <tr class="info">
                    <td>Артикул/Кол-во</td>
                    <td>
                        <input class="input-mini" type="text" placeholder="Артикул…"
                                       id="modalNotFoundAltArticle"/>
                        <input class="input-mini" type="text" placeholder="Кол-во…"
                                    id="modalNotFoundAltQty"/>
                    </td>
                </tr>
                <tr class="info">
                    <td>Цена</td>
                    <td>
                        <div class="input-append">
                            <input class="input-small" type="text" placeholder="Цена…"
                                id="modalNotFoundAltPrice"/>
                            <span class="add-on">.00</span>
                        </div>
                    </td>
                </tr>
                <tr class="info">
                    <td>Витринный экземпляр</td>
                    <td>
                        <input class="input-mini" type="checkbox" 
                                    id="modalNotFoundAltDisplayItem"
                            onchange="toggleRow('modalNotFoundAltDisplayItemConditionTr' , this.checked);"/>
                    </td>
                </tr>
                <tr class="info"
                    id="modalNotFoundAltDisplayItemConditionTr"
                    style="display: none">
                    <td>Описание состояния витрины</td>
                    <td>
                        <textarea rows="3" placeholder="Описание состояния витрины…"
                                    id="modalNotFoundAltDisplayItemCondition"></textarea>
                    </td>
                </tr>
                <tr>
                    <td>Комментарий</td>
                    <td>
                        <textarea rows="3" placeholder="Комментарий…"
                                    id="modalNotFoundComment"></textarea>
                    </td>
                </tr>
            </table>
          </div>
          <div class="modal-footer">
              <a href="#" class="btn" onclick="closeModalNotFound(false)">OK</a>
              <a href="#"  class="btn" onclick="closeModalNotFound(true)">Отмена</a>
          </div>
        </div>
        
        <!-- Modal IncorrectPrice-->
        <div id="modalIncorrectPrice" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-header">
            <a href="#"  class="close" onclick="closeModalIncorrectPrice(true)">×</a>
            <h6 id="H1">
                <label id="modalIncorrectPriceCurrentArticle"></label>
            </h6>
          </div>
          <div class="modal-body">
            <table class="table table-condensed">
                <tr>
                    <td>Цена</td>
                    <td>
                        <div class="input-append">
                            <input class="input-small" type="text" placeholder="Цена…"
                                id="modalIncorrectPriceCorrectPrice"/>
                            <span class="add-on">.00</span>
                        </div>
                        <span class="help-inline">
                            <label id="modalIncorrectPriceCurrentPrice"></label>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>Комментарий</td>
                    <td>
                        <textarea rows="3" placeholder="Комментарий…"
                                    id="modalIncorrectPriceComment"></textarea>
                    </td>
                </tr>
            </table>
          </div>
          <div class="modal-footer">
              <a href="#" class="btn" onclick="closeModalIncorrectPrice(false)">OK</a>
              <a href="#"  class="btn" onclick="closeModalIncorrectPrice(true)">Отмена</a>
          </div>
        </div>
        
        <!-- Modal ReservedPart-->
        <div id="modalReservedPart" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-header">
            <a href="#"  class="close" onclick="closeModalReservedPart(true)">×</a>
            <h6 id="H2">
                <label id="modalReservedPartCurrentArticle"></label>
            </h6>
          </div>
          <div class="modal-body">
            <table class="table table-condensed">
                <tr>
                    <td>Доступное кол-во</td>
                    <td>
                        <div class="input-append">
                            <input class="input-small" type="text" placeholder="кол-во…"
                                id="modalReservedPartCorrectQty"/>
                        </div>
                        <span class="help-inline">
                            <label id="modalReservedPartCurrentQty"></label>
                        </span>
                    </td>
                </tr>
                <tr class="info">
                    <td colspan="2">
                        Если возможно предложите альтернативу
                    </td>
                </tr>
                <tr class="info">
                    <td>Артикул/Кол-во</td>
                    <td>
                        <input class="input-mini" type="text" placeholder="Артикул…"
                                       id="modalReservedPartAltArticle"/>
                        <input class="input-mini" type="text" placeholder="Кол-во…"
                                    id="modalReservedPartAltQty"/>
                    </td>
                </tr>
                <tr class="info">
                    <td>Цена</td>
                    <td>
                        <div class="input-append">
                            <input class="input-small" type="text" placeholder="Цена…"
                                id="modalReservedPartAltPrice"/>
                            <span class="add-on">.00</span>
                        </div>
                    </td>
                </tr>
                <tr class="info">
                    <td>Витринный экземпляр</td>
                    <td>
                        <input class="input-mini" type="checkbox" 
                                    id="modalReservedPartAltDisplayItem"
                            onchange="toggleRow('modalReservedPartAltDisplayItemConditionTr' , this.checked);"/>
                    </td>
                </tr>
                <tr class="info"
                    id="modalReservedPartAltDisplayItemConditionTr"
                    style="display: none">
                    <td>Описание состояния витрины</td>
                    <td>
                        <textarea rows="3" placeholder="Описание состояния витрины…"
                                    id="modalReservedPartAltDisplayItemCondition"></textarea>
                    </td>
                </tr>
                <tr>
                    <td>Комментарий</td>
                    <td>
                        <textarea rows="3" placeholder="Комментарий…"
                                    id="modalReservedPartComment"></textarea>
                    </td>
                </tr>
            </table>
          </div>
          <div class="modal-footer">
              <a href="#" class="btn" onclick="closeModalReservedPart(false)">OK</a>
              <a href="#"  class="btn" onclick="closeModalReservedPart(true)">Отмена</a>
          </div>
        </div>
        
        <!-- Modal ReservedDisplayItem-->
        <div id="modalReservedDisplayItem" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-header">
            <a href="#"  class="close" onclick="closeModalReservedDisplayItem(true)">×</a>
            <h6 id="H3">
                <label id="modalReservedDisplayItemCurrentArticle"></label>
            </h6>
          </div>
          <div class="modal-body">
            <table class="table table-condensed">
                <tr>
                    <td>Описание состояния</td>
                    <td>
                        <textarea rows="3" placeholder="Описание состояния…"
                                    id="modalReservedDisplayItemRowConditionStateComment"></textarea>
                    </td>
                </tr>
                <tr>
                    <td>Предлагаемая цена</td>
                    <td>
                        <div class="input-append">
                            <input class="input-small" type="text" placeholder="цена…"
                                id="modalReservedDisplayItemCorrectPrice"/>
                        </div>
                        <span class="help-inline">
                            <label id="modalReservedDisplayItemCurrentPrice"></label>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>Доступное кол-во</td>
                    <td>
                        <div class="input-append">
                            <input class="input-small" type="text" placeholder="кол-во…"
                                id="modalReservedDisplayItemCorrectQty"/>
                        </div>
                        <span class="help-inline">
                            <label id="modalReservedDisplayItemCurrentQty"></label>
                        </span>
                    </td>
                </tr>
                <tr class="info">
                    <td colspan="2">
                        Если возможно предложите альтернативу
                    </td>
                </tr>
                <tr class="info">
                    <td>Артикул/Кол-во</td>
                    <td>
                        <input class="input-mini" type="text" placeholder="Артикул…"
                                       id="modalReservedDisplayItemAltArticle"/>
                        <input class="input-mini" type="text" placeholder="Кол-во…"
                                    id="modalReservedDisplayItemAltQty"/>
                    </td>
                </tr>
                <tr class="info">
                    <td>Цена</td>
                    <td>
                        <div class="input-append">
                            <input class="input-small" type="text" placeholder="Цена…"
                                id="modalReservedDisplayItemAltPrice"/>
                            <span class="add-on">.00</span>
                        </div>
                    </td>
                </tr>
                <tr class="info">
                    <td>Витринный экземпляр</td>
                    <td>
                        <input class="input-mini" type="checkbox" 
                                    id="modalReservedDisplayItemAltDisplayItem"
                            onchange="toggleRow('modalReservedDisplayItemAltDisplayItemConditionTr' , this.checked);"/>
                    </td>
                </tr>
                <tr class="info" 
                    id="modalReservedDisplayItemAltDisplayItemConditionTr"
                    style="display: none">
                    <td>Описание состояния витрины</td>
                    <td>
                        <textarea rows="3" placeholder="Описание состояния витрины…"
                                    id="modalReservedDisplayItemAltDisplayItemCondition"></textarea>
                    </td>
                </tr>
                <tr>
                    <td>Комментарий</td>
                    <td>
                        <textarea rows="3" placeholder="Комментарий…"
                                    id="modalReservedDisplayItemComment"></textarea>
                    </td>
                </tr>
            </table>
          </div>
          <div class="modal-footer">
              <a href="#" class="btn" onclick="closeModalReservedDisplayItem(false)">OK</a>
              <a href="#"  class="btn" onclick="closeModalReservedDisplayItem(true)">Отмена</a>
          </div>
        </div>
    </form>
    
    <script type="text/javascript">
        var currentRow = 0;
        var rowsCount = <%Response.Write("" + this.ReserveLines.Count); %>;
        initRowsEvents();
    </script>
    
    <script type="text/javascript">
        $('.accordion').on('show hide', function (n) {
            $(n.target).siblings('.accordion-heading').find('.accordion-toggle i').toggleClass('icon-chevron-up icon-chevron-down');
        });
    </script>
</body>
</html>
