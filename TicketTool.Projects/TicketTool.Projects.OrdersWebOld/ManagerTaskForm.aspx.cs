﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using TicketTool.Projects.Data;
using TicketTool.Projects.Data.Model;
using TicketTool.Projects.OrderTaskForm.Common;
using TicketTool.Projects.OrderTaskForm.Common.Configuration;

namespace TicketTool.Projects.OrderTaskForm
{
    public partial class ManagerTaskForm : Page
    {
        protected const string PriceFormat = "0.00";
        private const string DateTimeFormat = "yyyy-MM-dd HH:mm:ss";

        protected string ErrorMessage;
        protected string StatusMessage;
        //task properties
        protected bool TaskExist;
        protected string TaskName;
        protected string TaskTime;
        protected string TaskDescription;
        protected string OrderId;
        //order properties
        protected string OrderSapCode;
        protected string OrderSource;
        protected bool OrderPaymentShouldBeBold;
        protected string OrderPaymentType;
        protected string OrderStatusInfo;
        protected string OrderUpdateDate;
        protected string OrderWWSOrderId;
        protected string OrderCustomerName;
        protected string OrderCustomerEmail;
        protected string OrderCustomerTelephone;
        protected bool HaveDelivery;
        protected string DeliveryCity;
        protected string DeliveryAddress;

        protected bool CanEdit;
        protected bool HaveAnyRequiredField;

        protected List<RequiredFieldDto> RequiredFields = new List<RequiredFieldDto>();
        protected List<OurcomeDto> Outcomes = new List<OurcomeDto>();
        protected List<ReserveLineDto> ReserveLines = new List<ReserveLineDto>();

        private IDataFacade DataFacade { get; set; }
        public TicketTask Task { get; set; }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.DataFacade = new DataFacade(new HttpCache());
        }

        protected override void OnLoad(EventArgs e)
        {
            this.SetCacheSettings();

            TaskExist = true;

            if (!this.Login()) return;

            try
            {
                var taskResult = this.DataFacade.Tasks.GetTask(_taskId);
                if (taskResult.HasErrors)
                {
                    TaskExist = false;
                    StatusMessage = "Задачи нет, либо она завершена, либо у вас нет на нее прав!";
                    return;
                }
                this.Task = taskResult.Item;

                if (!IsPostBack)
                {
                    TaskName = this.Task.Name;
                    TaskDescription = UpdateDescription(this.Task.Description);

                    CanEdit = !(this.Task.RelatedContent == null || this.Task.RelatedContent.IsReadOnly);
                    OrderId = this.Task.WorkitemId;
                    if (string.IsNullOrEmpty(OrderId))
                    {
                        TaskExist = false;
                        StatusMessage = "Нет заказа у задачи! " + this.Task.Id;
                        return;
                    }
                    
                    var client = new OrderServiceClient();
                    var response = client.GerOrderDataByOrderId(OrderId);

                    OrderSapCode = response.OrderInfo.SapCode;
                    OrderSource = ManagerUIExtension.GetOrderSourceDisplayName(response.OrderInfo.OrderSource);
                    OrderPaymentType = ManagerUIExtension.GetPaymentTypeDisplayName(response.OrderInfo.PaymentType);
                    OrderPaymentShouldBeBold = response.OrderInfo.PaymentType == PaymentType.OnlineCredit;
                    OrderStatusInfo = ManagerUIExtension.GetInternalOrderStatusDisplayName(response.OrderInfo.StatusInfo.Status);
                    var updateDate = TimeZoneInfo.ConvertTimeFromUtc(response.OrderInfo.UpdateDate,
                                                                      TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time"));
                    OrderUpdateDate = updateDate.ToString(DateTimeFormat);
                    OrderWWSOrderId = response.OrderInfo.WWSOrderId;

                    if (response.ClientData != null)
                    {
                        OrderCustomerName = string.Format("{0} {1}", response.ClientData.Name, response.ClientData.Surname);
                        OrderCustomerEmail = response.ClientData.Email;
                        OrderCustomerTelephone = response.ClientData.Phone;
                    }
                    HaveDelivery = response.DeliveryInfo != null && response.DeliveryInfo.HasDelivery;
                    if (HaveDelivery)
                    {
                        DeliveryCity = response.DeliveryInfo.City;
                        DeliveryAddress = response.DeliveryInfo.Address;
                    }
                    ReserveLines = FillReserveLines(response);
                    //RequiredFields = OrderStatesExtension.GetTestRequiredFields();
                    RequiredFields = GetRequiredFieldDtos(task.RequiredFields, true);
                    int outcomeNumber = 1;
                    Outcomes = task.Outcomes.Select(x => new OurcomeDto
                                                             {
                                                                 Number = outcomeNumber++,
                                                                 Name = x.Value,
                                                                 IsSuccess = x.IsSuccess,
                                                                 SkipTaskRequeriedFields = x.SkipTaskRequeriedFields,
                                                                 Fields = GetRequiredFieldDtos(x.RequiredFields, false),
                                                                 FieldsCount = this.RequiredFields.Count + x.RequiredFields.Count()
                                                             }).ToList();
                    HaveAnyRequiredField = RequiredFields.Count > 0 || Outcomes.Any(o => o.Fields.Count > 0);
                }
                else
                {
                    this.CompleteTask();
                }
            }
            catch (Exception exp)
            {
                TaskExist = false;
                StatusMessage = "Ошибка!";
                ErrorMessage = exp.ToString();
            }
        }

        private bool Login()
        {
            var currentUserResult = this.DataFacade.Auth.GetCurrentUser();
            if (currentUserResult.HasErrors)
            {
                TaskExist = false;
                StatusMessage = currentUserResult.Message;
                return false;
            }
            if (!currentUserResult.Item.Authorized)
            {
                var loginResult = this.DataFacade.Auth.LoginByTokens(
                    Request.QueryString[Settings.ManagerUserName],
                    Request.QueryString[Settings.ManagerParameter],
                    Request.QueryString[Settings.TaskIdParameter],
                    Request.QueryString[Settings.KeyParameter]);
                if (loginResult.HasErrors)
                {
                    TaskExist = false;
                    StatusMessage = loginResult.Message;
                    return false;
                }
            }
            return true;
        }

        private void SetCacheSettings()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetMaxAge(new TimeSpan(0));
            Response.Cache.SetExpires(DateTime.Now);
            Response.Cache.SetNoStore();
            Response.Cache.SetRevalidation(HttpCacheRevalidation.None);
            Response.Cache.SetNoServerCaching();
        }

        private static List<RequiredFieldDto> GetRequiredFieldDtos(IEnumerable<RequiredField> requiredFields, bool taskField)
        {
            return requiredFields.Select(y => new RequiredFieldDto
                                                  {
                                                      DefaultValue = y.DefaultValue != null ? y.DefaultValue.ToString() : "",
                                                      Name = y.Name,
                                                      TypeName = y.Type,
                                                      Type = ManagerUIExtension.GetRequiredFieldTypeBySystemType(y.Type),
                                                      Value = y.Value != null ? y.Value.ToString() : "",
                                                      PredefinedValues =
                                                          (y is RequiredFieldValueList)
                                                              ? (y as RequiredFieldValueList).PredefinedValuesList.Select(z => z.ToString()).ToList() :
                                                              new List<string>(),
                                                      TaskField = taskField
                                                  }).ToList();
        }

        private void CompleteTask()
        {
            this.OrderId = this.Request.Form["hOrderId"];
            var outcome = this.Request.Form["hOutcomeName"];
            var outcomeIsSuccessStr = this.Request.Form["hOutcomeIsSuccess"];
            if (outcome == "CancelOutcome")
            {
                var result = this.DataFacade.Tasks.PostponeTask(this.Task, null);
                if (result.HasErrors)
                {
                    ErrorMessage = result.Message;
                }
                StatusMessage = "Задача отложена!";
            }
            else
            {
                bool.TryParse(this.Request.Form["hCanEdit"], out CanEdit);
                bool outcomeIsSuccess;
                bool.TryParse(outcomeIsSuccessStr, out outcomeIsSuccess);

                if (CanEdit)
                {
                    var articles = GetManagerArticlesFromForm();
                    var reserveLines = ManagerUIExtension.GetReserveLines(articles).ToArray();
                    var updateResult = this.DataFacade.Orders.UpdateOrder(
                        new UpdateOrderData {OrderId = OrderId},
                        new ReserveInfo {ReserveLines = reserveLines});

                    if (updateResult.HasErrors)
                    {
                        TaskExist = false;
                        ErrorMessage = "Проблемы с сохранением заказа! " + updateResult.Message;                        
                        StatusMessage = "Проблемы с сохранением заказа!";
                        return;
                    }
                }
                var reqFields = GetRequiredFieldFromForm();
                var taskOutcome = this.Task.Outcomes.FirstOrDefault(x => x.Value == outcome);
                if (taskOutcome == null)
                {
                    StatusMessage = "Проблема! Нет такого исхода: " + outcome;
                    ErrorMessage = "";
                    TaskExist = false;
                    return;
                }
                if (taskOutcome.RequiredFields.Count() + this.Task.RequestedFields.Count() != reqFields.Count)
                {
                    StatusMessage = "Проблема с обязательными полями для заполнения!";
                    ErrorMessage = "";
                    TaskExist = false;
                    return;
                }
                var fields = reqFields.Select(
                    f => new RequiredField
                        {
                            Name = f.TaskField ? f.Name : string.Format("{0};#{1}", outcome, f.Name),
                            Type = f.TypeName,
                            Value = ManagerUIExtension.GetRequiredFieldValue(f.Value, f.TypeName)
                        }).ToArray();
                //return;
                foreach (var f in reqFields)
                {
                    if (string.IsNullOrEmpty(f.Value) || !ManagerUIExtension.ValidateRequiredFieldValue(f.Value, f.TypeName))
                    {
                        StatusMessage = "Проблема со значение обязательного поля!";
                        ErrorMessage = string.Format("Название: '{0}'; 'Тип: {1}'; Значение: '{2}'", f.Name, f.TypeName, f.Value);
                        TaskExist = false;
                        return;
                    }
                }

                 AuthContext.Auth.Execute(() => ticketService.FinishTicketTask(_taskId, outcome, fields)
                var result = this.DataFacade.Tasks.CompleteTask(this.Task);
                if (result.HasErrors)
                {
                    StatusMessage = "Проблемы с завершением задачи!";
                    ErrorMessage = result.Message;
                    TaskExist = false;
                    return;
                }
                StatusMessage = "Задача завершена!";
            }
            TaskExist = false;
        }

        private List<ReserveLineDto> FillReserveLines(CustomerOrder order)
        {
            int number = 1;
            long l;            
            return order.ReserveLines
                .Select(x => new ReserveLineDto
                                {
                                    Number = number++,
                                    LineId = x.LineId,
                                    ArticleNum = x.ArticleNum,
                                    Title = string.IsNullOrEmpty(x.Title) ? "" : x.Title,
                                    Price = x.Price,
                                    Qty = x.Qty,
                                    ReviewItemState = x.ReviewItemState,
                                    Comment = "",
                                    CommentLog = x.Comment,
                                    StockPrice = x.StockPrice,
                                    StockQty = x.StockQty,
                                    ArticleConditionState = x.StockItemState,
                                    ArticleCondition = x.ArticleCondition,
                                    Promotions = x.ArticleData.Promotions,

                                    WWSDepartmentNumber = x.WWSDepartmentNumber,
                                    WWSProductGroupNo = x.WWSProductGroupNumber,
                                    WWSProductGroupName = x.WWSProductGroupName,
                                    WWSStockNumber = x.WWSStockNumber,
                                    WWSFreeQty = x.WWSFreeQty,
                                    WWSReserverQuantity = x.WWSReservedQty,
                                    WWSPriceOrig = x.WWSPriceOrig,

                                    ValidArticleNum = long.TryParse( x.ArticleData.ArticleNum, out l),
                                    ValidPrice = x.Price > 0 && x.Price == x.WWSPriceOrig,
                                    ValidQty = x.Qty > 0 && x.Qty <= x.WWSFreeQty
                                }).ToList();
        }

        private List<RequiredFieldDto> GetRequiredFieldFromForm()
        {
            var fields = new List<RequiredFieldDto>();

            var outcomeNumberStr = this.Request.Form["hOutcomeNumber"];
            var fieldsCountStr = this.Request.Form["hOutcomeFieldsCount" + outcomeNumberStr];
            int fieldsCount;
            Int32.TryParse(fieldsCountStr, out fieldsCount);
            for (int i = 1; i <= fieldsCount; i++)
            {
                var fieldType = this.Request.Form["hOutcomeFieldType" + outcomeNumberStr + "_" + i];
                var fieldTypeName = this.Request.Form["hOutcomeFieldTypeName" + outcomeNumberStr + "_" + i];
                var fieldName = this.Request.Form["hOutcomeFieldName" + outcomeNumberStr + "_" + i];
                var fieldValue = this.Request.Form["hOutcomeFieldValue" + outcomeNumberStr + "_" + i];
                var fieldTaskField = this.Request.Form["hOutcomeFieldTaskField" + outcomeNumberStr + "_" + i].ToLowerInvariant() == "true";
                var type = ManagerUIExtension.GetRequiredFieldType(fieldType);
                fields.Add(new RequiredFieldDto
                               {
                                   Name = fieldName,
                                   TaskField = fieldTaskField,
                                   Type = type.HasValue ? type.Value : RequiredFieldType.String,
                                   TypeName = fieldTypeName,
                                   Value = fieldValue
                               });
            }

            return fields;
        }

        private IEnumerable<ManagerArticle> GetManagerArticlesFromForm()
        {
            var rowsCountStr = this.Request.Form["hRowsCount"];
            int rowsCount;
            Int32.TryParse(rowsCountStr, out rowsCount);
            var articles = new List<ManagerArticle>();
            for (int number = 1; number <= rowsCount; number++)
            {
                var rowTitle = this.Request.Form["rowTitle" + number];
                var rowLineIdStr = this.Request.Form["rowLineId" + number];
                var articleStr = this.Request.Form["rowArticle" + number];
                var priceStr = this.Request.Form["rowPrice" + number];
                var qtyStr = this.Request.Form["rowQty" + number];
                var commentStr = this.Request.Form["rowComment" + number];
                var rowCommentLog = this.Request.Form["rowCommentLog" + number];

                var rowState = this.Request.Form["rowState" + number];

                var rowCorrectPrice = this.Request.Form["hRowCorrectPrice" + number];
                var rowCorrectQty = this.Request.Form["hRowCorrectQty" + number];
                var rowAltArticle = this.Request.Form["hRowAltArticle" + number];
                var rowAltPrice = this.Request.Form["hRowAltPrice" + number];
                var rowAltQty = this.Request.Form["hRowAltQty" + number];
                var rowAltDisplayItem = this.Request.Form["hRowAltDisplayItem" + number];
                var rowAltDisplayItemCondition = this.Request.Form["hRowAltDisplayItemCondition" + number];

                var rowConditionState = this.Request.Form["rowConditionState" + number];
                var rowConditionStateComment = this.Request.Form["rowConditionStateComment" + number];

                var WWSDepartmentNo = this.Request.Form["WWSDepartmentNo" + number];
                var WWSProductGroup = this.Request.Form["WWSProductGroup" + number];
                var WWSProductGroupNo = this.Request.Form["WWSProductGroupNo" + number];
                var WWSStockNo = this.Request.Form["WWSStockNo" + number];
                var WWSFreeQty = this.Request.Form["WWSFreeQty" + number];
                var WWSReservedQty = this.Request.Form["WWSReservedQty" + number];
                var WWSPriceOrig = this.Request.Form["WWSPriceOrig" + number];

                var state = ManagerUIExtension.GetReviewItemState(rowState);
                var conditionState = ManagerUIExtension.GetItemState(rowConditionState);

                var line = new ManagerArticle
                               {
                                   Title = rowTitle,
                                   Number = number,
                                   Article = articleStr,
                                   Price = decimal.Parse(priceStr),
                                   Qty = Int32.Parse(qtyStr),
                                   Comment = commentStr,
                                   CommentLog = rowCommentLog,
                                   RowState = state.HasValue ? state.Value : ReviewItemState.Empty,
                                   ConditionState = conditionState.HasValue ? conditionState.Value : ItemState.New,
                                   ConditionStateComment = rowConditionStateComment
                               };
                long l;
                line.LineId = long.TryParse(rowLineIdStr, out l) ? l : 0;
                decimal d;
                line.RowCorrectPrice = decimal.TryParse(rowCorrectPrice, out d) ? d : 0;
                int i;
                line.RowCorrectQty = Int32.TryParse(rowCorrectQty, out i) ? i : 0;
                line.RowAltArticle = rowAltArticle;
                line.RowAltPrice = decimal.TryParse(rowAltPrice, out d) ? d : 0;
                line.RowAltQty = Int32.TryParse(rowAltQty, out i) ? i : 0;
                line.RowDisplayItem = rowAltDisplayItem == "true";
                line.RowAltDisplayItemCondition = rowAltDisplayItemCondition;

                line.WWSDepartmentNo = Int32.TryParse(WWSDepartmentNo, out i) ? i : 0;
                line.WWSProductGroup = WWSProductGroup;
                line.WWSProductGroupNo = Int32.TryParse(WWSProductGroupNo, out i) ? i : 0;
                line.WWSStockNo = Int32.TryParse(WWSStockNo, out i) ? i : 0;
                line.WWSFreeQty = Int32.TryParse(WWSFreeQty, out i) ? i : 0;
                line.WWSReservedQty = Int32.TryParse(WWSReservedQty, out i) ? i : 0;
                line.WWSPriceOrig = decimal.TryParse(WWSPriceOrig, out d) ? d : 0;
                
                articles.Add(line);
            }
            return articles;
        }

        private static string UpdateDescription(string description)
        {
            var startPreffix = "<!--mediamarkt.description.start-->";
            var endPreffix = "<!--mediamarkt.description.end-->";
            var s1 = description.IndexOf(startPreffix);
            var e1 = description.IndexOf(endPreffix);
            if (s1 < e1 && s1 > 0)
            {
                var text = description.Substring(s1 + startPreffix.Length, e1 - s1 - startPreffix.Length);
                return text;
            }
            return description;
        }
    }
}