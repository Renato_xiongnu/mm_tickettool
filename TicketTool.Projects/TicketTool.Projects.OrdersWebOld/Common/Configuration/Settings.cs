﻿using System.Configuration;

namespace TicketTool.Projects.OrderTaskForm.Common.Configuration
{
    public static class Settings
    {      
        public static string DefaultManagerPassword
        {
            get { return ConfigurationManager.AppSettings["DefaultManagerPassword"]; }
        }

        public static string TaskIdParameter
        {
            get { return ConfigurationManager.AppSettings["TaskIdParameter"]; }
        }

        public static string KeyParameter
        {
            get { return ConfigurationManager.AppSettings["KeyParameter"]; }
        }

        public static string SapCodeParameter
        {
            get { return ConfigurationManager.AppSettings["SapCodeParameter"]; }
        }

        public static string ManagerParameter
        {
            get { return ConfigurationManager.AppSettings["ManagerParameter"]; }
        }

        public static string ManagerUserName
        {
            get { return ConfigurationManager.AppSettings["ManagerUserName"]; }
        }

        public static string OrderMarkerParam
        {
            get { return ConfigurationManager.AppSettings["OrderMarkerParam"]; }
        }

        public static string OrderIdParameter
        {
            get { return ConfigurationManager.AppSettings["OrderIdParameter"]; }
        }
    }
}