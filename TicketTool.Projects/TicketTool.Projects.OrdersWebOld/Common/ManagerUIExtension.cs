﻿using System;
using System.Collections.Generic;
using System.Globalization;
using TicketTool.Projects.Data.Proxy.OrderService.v1_3;

namespace TicketTool.Projects.OrderTaskForm.Common
{
    public class ManagerUIExtension
    {
        private const string DateFormat = "yyyy-MM-dd";

        public static string GetReviewItemStateDisplayName(ReviewItemState? state)
        {
            switch (state)
            {
                case ReviewItemState.Empty:
                    return "";
                case ReviewItemState.IncorrectPrice:
                    return "Неправильная цена";
                case ReviewItemState.NotFound:
                    return "Не найден";
                case ReviewItemState.Reserved:
                    return "Отложен";
                case ReviewItemState.ReservedDisplayItem:
                    return "Отложен витринный экземпляр";
                case ReviewItemState.ReservedPart:
                    return "Отложен частично";
                default:
                    // throw new ArgumentOutOfRangeException("state");
                    return "";
            }
        }

        public static string GetOrderSourceDisplayName(OrderSource orderSource)
        {
            switch (orderSource)
            {
                case OrderSource.CallCenter:
                    return "Call Center";
                case OrderSource.IPhone:
                    return "Приложение IPhone";
                case OrderSource.MobileWebSite:
                    return "Мобильный веб сайт";
                case OrderSource.WebSite:
                    return "Веб сайт";
                case OrderSource.Windows8:
                    return "Приложение Windows 8";
				case OrderSource.Avito:
                    return "АВИТО";				
                case OrderSource.Metro:
                    return "Метро";
                case OrderSource.MetroCallCenter:
                    return "Метро (Call Center)";
                case OrderSource.WebSiteQuickOrder:
                    return "Веб сайт (быстрый заказ)";
                default:
                    return orderSource.ToString();
            }
        }

        public static string GetPaymentTypeDisplayName(PaymentType paymentType)
        {
            switch (paymentType)
            {
                case PaymentType.Cash:
                    return "Наличные";
                case PaymentType.Online:
                    return "Online";
                case PaymentType.OnlineCredit:
                    return "Онлайн кредит";
                default:
                    throw new ArgumentOutOfRangeException("paymentType");
            }
        }

        public static string GetInternalOrderStatusDisplayName(InternalOrderStatus internalOrderStatus)
        {
            switch (internalOrderStatus)
            {
                case InternalOrderStatus.Blank:
                    return "Пустой";
                case InternalOrderStatus.Created:
                    return "Заказ создан";
                case InternalOrderStatus.Confirmed:
                    return "Заказ подтвержден";
                case InternalOrderStatus.GiftCertificateNumberReservedNotifyCustomer:
                    return "Купон отправлен покупателю";
                case InternalOrderStatus.Paid:
                    return "Заказ оплачен";
                case InternalOrderStatus.GiftCertificateCredited:
                    return "Сертификат аккредитован";
                case InternalOrderStatus.GiftCertificateCreditedNotifyCustomer:
                    return "Сертификат отправлен покупателю";
                case InternalOrderStatus.GiftCertificateCreditedError:
                    return "Ошибка в начислении денег на сертификат покупателя";
                case InternalOrderStatus.LifeCycleIgnored:
                    return "Данный заказ не обрабатывается системой";
                case InternalOrderStatus.Closed:
                    return "Заказ закрыт";
                case InternalOrderStatus.Rejected:
                    return "Заказ отклонен";
                case InternalOrderStatus.RejectedByCustomer:
                    return "Заказ отклонен пользователем";
                default:
                    throw new ArgumentOutOfRangeException("internalOrderStatus");
            }
        }

        public static string GetItemStateDisplayName(ItemState? state)
        {
            switch (state)
            {
                case ItemState.New:
                    return "Новый";
                case ItemState.DisplayItem:
                    return "Витринный экземпляр";
                case ItemState.Repaired:
                    return "ИЗ ремонта";
                default:
                    // throw new ArgumentOutOfRangeException("state");
                    return "";
            }
        }

        public static ItemState? GetItemState(string state)
        {
            ItemState? t = null;
            if (Enum.IsDefined(typeof(ItemState), state))
            {
                t = (ItemState)Enum.Parse(typeof(ItemState), state);
            }
            return t;
        }

        public static ReviewItemState? GetReviewItemState(string state)
        {
            ReviewItemState? t = null;
            if (Enum.IsDefined(typeof(ReviewItemState), state))
            {
                t = (ReviewItemState)Enum.Parse(typeof(ReviewItemState), state);
            }
            return t;
        }

        public static RequiredFieldType? GetRequiredFieldType(string state)
        {
            RequiredFieldType? t = null;
            if (Enum.IsDefined(typeof(RequiredFieldType), state))
            {
                t = (RequiredFieldType)Enum.Parse(typeof(RequiredFieldType), state);
            }
            return t;
        }

        public static object GetRequiredFieldValue(string value, string type)
        {
            int i32;
            double d;
            decimal dd;
            DateTime dt;
            TimeSpan ts;
            bool b;
            if (type == "System.Int32" && int.TryParse(value, out i32))
            {
                return i32;
            }
            if (type == "System.Double" && double.TryParse(value, out d))
            {
                return d;
            }
            if (type == "System.Decimal" && decimal.TryParse(value, out dd))
            {
                return dd;
            }
            if (type == "System.DateTime" && DateTime.TryParse(value,
                 new CultureInfo("ru-RU"),
                 DateTimeStyles.None, 
                out dt))
            {
                return dt;
            }
            if (type == "System.TimeSpan" && TimeSpan.TryParse(value, out ts))
            {
                return ts;
            }
            if (type == "System.Boolean" && bool.TryParse(value, out b))
            {
                return b;
            }
            return value;
        }

        public static bool ValidateRequiredFieldValue(string value, string type)
        {
            int i32;
            double d;
            decimal dd;
            DateTime dt;
            TimeSpan ts;
            bool b;
            if (type == "System.Int32" && int.TryParse(value, out i32))
            {
                return true;
            }
            if (type == "System.Double" && double.TryParse(value, out d))
            {
                return true;
            }
            if (type == "System.DateTime" && DateTime.TryParse(value, new CultureInfo("ru-RU"),
                 DateTimeStyles.None, out dt))
            {
                return true;
            }
            if (type == "System.TimeSpan" && TimeSpan.TryParse(value, out ts))
            {
                return true;
            }
            if (type == "System.Boolean" && bool.TryParse(value, out b))
            {
                return true;
            }
            if (type == "System.String" && !string.IsNullOrEmpty(value))
            {
                return true;
            }
            if (type == "System.Decimal" && decimal.TryParse(value, out dd))
            {
                return true;
            }
            if (type == "Choice" && !string.IsNullOrEmpty(value))
            {
                return true;
            }
            return false;
        }

        public static RequiredFieldType GetRequiredFieldTypeBySystemType(string type)
        {
            if (type == "System.Int32")
            {
                return RequiredFieldType.Int32;
            }
            if (type == "System.Double")
            {
                return RequiredFieldType.Double;
            }
            if (type == "System.DateTime")
            {
                return RequiredFieldType.DateTime;
            }
            if (type == "System.TimeSpan")
            {
                return RequiredFieldType.TimeSpan;
            }
            if (type == "System.Boolean")
            {
                return RequiredFieldType.Boolean;
            }
            if (type == "System.String")
            {
                return RequiredFieldType.String;
            }
            if (type == "System.Decimal")
            {
                return RequiredFieldType.Decimal;
            }
            if (type == "Choice")
            {
                return RequiredFieldType.Choice;
            }
            return RequiredFieldType.String;
        }

        public static List<RequiredFieldDto> GetTestRequiredFields()
        {
            return new List<RequiredFieldDto>
                       {
                           new RequiredFieldDto
                               {
                                   Name = "Дата доставки",
                                   DefaultValue = "",
                                   PredefinedValues = new List<string>(),
                                   TypeName = "System.DateTime",
                                   Type = RequiredFieldType.DateTime,
                                   Value = "",
                                   TaskField = true
                               },
                           new RequiredFieldDto
                               {
                                   Name = "Перезвонит мне через",
                                   DefaultValue = "",
                                   PredefinedValues = new List<string>(),
                                   TypeName = "System.TimeSpan",
                                   Type = RequiredFieldType.TimeSpan,
                                   Value = "",
                                   TaskField = true
                               },
                           new RequiredFieldDto
                               {
                                   Name = "Не сообщать номер резерва по телефону",
                                   DefaultValue = "",
                                   PredefinedValues = new List<string>(),
                                   TypeName = "System.Boolean",
                                   Type = RequiredFieldType.Boolean,
                                   Value = "",
                                   TaskField = true
                               },
                           new RequiredFieldDto
                               {
                                   Name = "Строка",
                                   DefaultValue = "",
                                   PredefinedValues = new List<string>(),
                                   TypeName = "System.String",
                                   Type = RequiredFieldType.String,
                                   Value = "",
                                   TaskField = true
                               },
                           new RequiredFieldDto
                               {
                                   Name = "Целое число",
                                   DefaultValue = "",
                                   PredefinedValues = new List<string>(),
                                   TypeName = "System.Int32",
                                   Type = RequiredFieldType.Int32,
                                   Value = "",
                                   TaskField = false
                               },
                           new RequiredFieldDto
                               {
                                   Name = "Число decimal",
                                   DefaultValue = "",
                                   PredefinedValues = new List<string>(),
                                   TypeName = "System.Decimal",
                                   Type = RequiredFieldType.Decimal,
                                   Value = "",
                                   TaskField = false
                               },
                           new RequiredFieldDto
                               {
                                   Name = "Число double",
                                   DefaultValue = "",
                                   PredefinedValues = new List<string>(),
                                   TypeName = "System.Double",
                                   Type = RequiredFieldType.Double,
                                   Value = "",
                                   TaskField = false
                               },
                           new RequiredFieldDto
                               {
                                   Name = "Выбор",
                                   DefaultValue = "",
                                   PredefinedValues = new List<string>
                                                          {
                                                              "Да",
                                                              "Нет",
                                                              "Может быть",
                                                              "Как то так"

                                                          },
                                   TypeName = "Choice",
                                   Type = RequiredFieldType.Choice,
                                   Value = "",
                                   TaskField = false

                               }
                       };
        }

        public static List<ReserveLine> GetReserveLines(IEnumerable<ManagerArticle> articles)
        {
            List<ReserveLine> lines = new List<ReserveLine>();
            foreach (var article in articles)
            {
                switch (article.RowState)
                {
                    case ReviewItemState.Empty:
                        {
                            break;
                        }
                    case ReviewItemState.Reserved:
                        {
                            AddLinesForReserved(article, lines);
                            break;
                        }
                    case ReviewItemState.ReservedPart:
                        {
                            AddLinesForReservedPart(article, lines);
                            break;
                        }
                    case ReviewItemState.ReservedDisplayItem:
                        {
                            AddLinesForReservedDisplayItem(article, lines);
                            break;
                        }
                    case ReviewItemState.NotFound:
                        {
                            AddLinesForNotFound(article, lines);
                            break;
                        }
                    case ReviewItemState.IncorrectPrice:
                        {
                            AddLinesForIncorrectPrice(article, lines);
                            break;
                        }
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            return lines;
        }

        private static void AddLinesForIncorrectPrice(ManagerArticle article, List<ReserveLine> lines)
        {
            if (article.Qty > 0)
            {
                var comment = string.Format("У товара была неверная цена! Исправлено с {0} на {1}",
                                            article.Price.ToString("0.##"),
                                            article.RowCorrectPrice.ToString("0.##"));

                var line = new ReserveLine
                {
                    ArticleData = new ArticleData
                    {
                        ArticleNum = article.Article,
                        Price = article.RowCorrectPrice,
                        Qty = article.Qty
                    },
                    LineId = article.LineId,
                    Comment = comment,
                    ReviewItemState = ReviewItemState.IncorrectPrice,
                    Title = article.Title,
                    StockPrice = 0,
                    StockQty = 0,
                    StockItemState = article.ConditionState,
                    ArticleCondition = article.ConditionStateComment,
                    WWSDepartmentNumber = article.WWSDepartmentNo,
                    WWSProductGroupNumber = article.WWSProductGroupNo,
                    WWSProductGroupName = article.WWSProductGroup,
                    WWSStockNumber = article.WWSStockNo,
                    WWSFreeQty = article.WWSFreeQty,
                    WWSReservedQty = article.WWSReservedQty,
                    WWSPriceOrig = article.WWSPriceOrig
                };
                if (!string.IsNullOrEmpty(article.Comment))
                {
                    comment += "<br/>" + article.Comment;
                }
                line.Comment = article.CommentLog + GetCommentHeader() + Encode(comment);
                lines.Add(line);
            }
        }

        private static void AddLinesForNotFound(ManagerArticle article, List<ReserveLine> lines)
        {
            ReserveLine line = null;
            var comment = "Товар не найден!";
            if (article.Qty > 0)
            {
                line = new ReserveLine
                {
                    ArticleData = new ArticleData
                    {
                        ArticleNum = article.Article,
                        Price = article.Price,
                        Qty = article.Qty
                    },
                    LineId = article.LineId,
                    Comment = comment,
                    ReviewItemState = ReviewItemState.NotFound,
                    Title = article.Title,
                    StockPrice = 0,
                    StockQty = 0,
                    StockItemState = article.ConditionState,
                    ArticleCondition = article.ConditionStateComment,
                    WWSDepartmentNumber = article.WWSDepartmentNo,
                    WWSProductGroupNumber = article.WWSProductGroupNo,
                    WWSProductGroupName = article.WWSProductGroup,
                    WWSStockNumber = article.WWSStockNo,
                    WWSFreeQty = article.WWSFreeQty,
                    WWSReservedQty = article.WWSReservedQty,
                    WWSPriceOrig = article.WWSPriceOrig
                };
                comment = article.CommentLog + GetCommentHeader() + comment;
                if (!string.IsNullOrEmpty(article.Comment))
                {
                    comment += "<br/>" + article.Comment;
                }
                lines.Add(line);
            }
            if (!string.IsNullOrEmpty(article.RowAltArticle))
            {
                comment += string.Format("<br/>Есть альтернатива: {0}", article.RowAltArticle);
                var altLine = new ReserveLine
                {
                    ArticleData = new ArticleData
                    {
                        ArticleNum = article.RowAltArticle,
                        Price = article.RowAltPrice,
                        Qty = article.RowAltQty
                    },
                    Comment =
                        GetCommentHeader() +
                        string.Format("Альтернатива для не найденного {0}", article.Article),
                    ReviewItemState =
                        article.RowDisplayItem
                            ? ReviewItemState.ReservedDisplayItem
                            : ReviewItemState.Reserved,
                    Title = "",
                    StockPrice = 0,
                    StockQty = 0,
                    StockItemState = article.RowDisplayItem ? ItemState.DisplayItem : ItemState.New,
                    ArticleCondition = article.RowAltDisplayItemCondition,
                    WWSDepartmentNumber = 0,
                    WWSProductGroupNumber = 0,
                    WWSProductGroupName = "",
                    WWSStockNumber = 0,
                    WWSFreeQty = 0,
                    WWSReservedQty = 0,
                    WWSPriceOrig = 0
                };
                lines.Add(altLine);
            }
            if (line != null)
            {
                line.Comment = Encode(comment);
            }
        }

        private static void AddLinesForReservedDisplayItem(ManagerArticle article, List<ReserveLine> lines)
        {
            ReserveLine line = null;
            var comment = "Товар отложен витринный!";
            if (article.RowCorrectQty > 0)
            {
                line = new ReserveLine
                {
                    ArticleData = new ArticleData
                    {
                        ArticleNum = article.Article,
                        Price = article.RowCorrectPrice,
                        Qty = article.RowCorrectQty
                    },
                    LineId = article.LineId,
                    Comment = comment,
                    ReviewItemState = ReviewItemState.ReservedDisplayItem,
                    Title = article.Title,
                    StockPrice = 0,
                    StockQty = 0,
                    StockItemState = ItemState.DisplayItem, //article.ConditionState,
                    ArticleCondition = article.ConditionStateComment,
                    WWSDepartmentNumber = article.WWSDepartmentNo,
                    WWSProductGroupNumber = article.WWSProductGroupNo,
                    WWSProductGroupName = article.WWSProductGroup,
                    WWSStockNumber = article.WWSStockNo,
                    WWSFreeQty = article.WWSFreeQty,
                    WWSReservedQty = article.WWSReservedQty,
                    WWSPriceOrig = article.WWSPriceOrig
                };
                if (article.Qty != article.RowCorrectQty)
                {
                    comment +=
                        string.Format(" Хотели {0}, в наличии {1}.", article.Qty, article.RowCorrectQty);
                }
                if (article.Price != article.RowCorrectPrice)
                {
                    comment +=
                        string.Format(" Цена исправлена с {0}, на {1}.", article.Price.ToString("0.##"),
                                      article.RowCorrectPrice.ToString("0.##"));
                }
                if (!string.IsNullOrEmpty(article.Comment))
                {
                    comment += "<br/>" + article.Comment;
                }
                lines.Add(line);
            }
            if (article.Qty - article.RowCorrectQty > 0)
            {
                var line2 = new ReserveLine
                {
                    ArticleData = new ArticleData
                    {
                        ArticleNum = article.Article,
                        Price = article.Price,
                        Qty = article.Qty - article.RowCorrectQty
                    },
                    Comment = "",
                    ReviewItemState = ReviewItemState.NotFound,
                    Title = article.Title,
                    StockPrice = 0,
                    StockQty = 0,
                    StockItemState = ItemState.New,
                    WWSDepartmentNumber = article.WWSDepartmentNo,
                    WWSProductGroupNumber = article.WWSProductGroupNo,
                    WWSProductGroupName = article.WWSProductGroup,
                    WWSStockNumber = article.WWSStockNo,
                    WWSFreeQty = article.WWSFreeQty,
                    WWSReservedQty = article.WWSReservedQty,
                    WWSPriceOrig = article.WWSPriceOrig
                };
                lines.Add(line2);
            }

            if (!string.IsNullOrEmpty(article.RowAltArticle))
            {
                comment += string.Format(" Есть альтернатива: {0}", article.RowAltArticle);
                var altLine = new ReserveLine
                {
                    ArticleData = new ArticleData
                    {
                        ArticleNum = article.RowAltArticle,
                        Price = article.RowAltPrice,
                        Qty = article.RowAltQty
                    },
                    Comment =
                        GetCommentHeader() +
                        string.Format("Альтернатива для витринного экземпляра {0}", article.Article),
                    ReviewItemState =
                        article.RowDisplayItem
                            ? ReviewItemState.ReservedDisplayItem
                            : ReviewItemState.Reserved,
                    Title = "",
                    StockPrice = 0,
                    StockQty = 0,
                    StockItemState = article.RowDisplayItem ? ItemState.DisplayItem : ItemState.New,
                    ArticleCondition = article.RowAltDisplayItemCondition,
                    WWSDepartmentNumber = 0,
                    WWSProductGroupNumber = 0,
                    WWSProductGroupName = "",
                    WWSStockNumber = 0,
                    WWSFreeQty = 0,
                    WWSReservedQty = 0,
                    WWSPriceOrig = 0
                };
                lines.Add(altLine);
            }
            if (line != null)
            {
                line.Comment = article.CommentLog + GetCommentHeader() + Encode(comment);
            }
        }

        private static void AddLinesForReservedPart(ManagerArticle article, List<ReserveLine> lines)
        {
            ReserveLine line = null;
            var comment = string.Format("Товар отложен частично! Хотели {0}, в наличии {1}.", article.Qty, article.RowCorrectQty);
            if (article.RowCorrectQty > 0)
            {
                line = new ReserveLine
                {
                    ArticleData = new ArticleData
                    {
                        ArticleNum = article.Article,
                        Price = article.Price,
                        Qty = article.RowCorrectQty
                    },
                    LineId = article.LineId,
                    Comment = comment,
                    ReviewItemState = ReviewItemState.Reserved,
                    Title = article.Title,
                    StockPrice = 0,
                    StockQty = 0,
                    StockItemState = article.ConditionState,
                    ArticleCondition = article.ConditionStateComment,
                    WWSDepartmentNumber = article.WWSDepartmentNo,
                    WWSProductGroupNumber = article.WWSProductGroupNo,
                    WWSProductGroupName = article.WWSProductGroup,
                    WWSStockNumber = article.WWSStockNo,
                    WWSFreeQty = article.WWSFreeQty,
                    WWSReservedQty = article.WWSReservedQty,
                    WWSPriceOrig = article.WWSPriceOrig
                };
                if (!string.IsNullOrEmpty(article.Comment))
                {
                    comment += "<br/>" + article.Comment;
                }
                lines.Add(line);
            }
            if (article.Qty - article.RowCorrectQty > 0)
            {
                var line2 = new ReserveLine
                {
                    ArticleData = new ArticleData
                    {
                        ArticleNum = article.Article,
                        Price = article.Price,
                        Qty = article.Qty - article.RowCorrectQty
                    },
                    Comment = "",
                    ReviewItemState = ReviewItemState.NotFound,
                    Title = article.Title,
                    StockPrice = 0,
                    StockQty = 0,
                    StockItemState = ItemState.New,
                    WWSDepartmentNumber = article.WWSDepartmentNo,
                    WWSProductGroupNumber = article.WWSProductGroupNo,
                    WWSProductGroupName = article.WWSProductGroup,
                    WWSStockNumber = article.WWSStockNo,
                    WWSFreeQty = article.WWSFreeQty,
                    WWSReservedQty = article.WWSReservedQty,
                    WWSPriceOrig = article.WWSPriceOrig
                };
                lines.Add(line2);
            }

            if (!string.IsNullOrEmpty(article.RowAltArticle))
            {
                if (line != null)
                {
                    comment += string.Format(" Есть альтернатива: {0}", article.RowAltArticle);
                }
                var altLine = new ReserveLine
                {
                    ArticleData = new ArticleData
                    {
                        ArticleNum = article.RowAltArticle,
                        Price = article.RowAltPrice,
                        Qty = article.RowAltQty
                    },
                    Comment =
                        GetCommentHeader() +
                        string.Format("Альтернатива для найденного частично {0}", article.Article),
                    ReviewItemState =
                        article.RowDisplayItem
                            ? ReviewItemState.ReservedDisplayItem
                            : ReviewItemState.Reserved,
                    Title = "",
                    StockPrice = 0,
                    StockQty = 0,
                    StockItemState = article.RowDisplayItem ? ItemState.DisplayItem : ItemState.New,
                    ArticleCondition = article.RowAltDisplayItemCondition,
                    WWSDepartmentNumber = 0,
                    WWSProductGroupNumber = 0,
                    WWSProductGroupName = "",
                    WWSStockNumber = 0,
                    WWSFreeQty = 0,
                    WWSReservedQty = 0,
                    WWSPriceOrig = 0
                };
                lines.Add(altLine);
            }
            if (line != null)
            {
                line.Comment = article.CommentLog + GetCommentHeader() + comment;
            }
        }

        private static void AddLinesForReserved(ManagerArticle article, List<ReserveLine> lines)
        {
            var line = new ReserveLine
            {
                ArticleData = new ArticleData
                {
                    ArticleNum = article.Article,
                    Price = article.Price,
                    Qty = article.Qty
                },
                LineId = article.LineId,
                Comment = "Отложен",
                ReviewItemState = ReviewItemState.Reserved,
                Title = article.Title,
                StockPrice = 0,
                StockQty = 0,
                StockItemState = article.ConditionState,
                ArticleCondition = article.ConditionStateComment,
                WWSDepartmentNumber = article.WWSDepartmentNo,
                WWSProductGroupNumber = article.WWSProductGroupNo,
                WWSProductGroupName = article.WWSProductGroup,
                WWSStockNumber = article.WWSStockNo,
                WWSFreeQty = article.WWSFreeQty,
                WWSReservedQty = article.WWSReservedQty,
                WWSPriceOrig = article.WWSPriceOrig
            };
            if (!string.IsNullOrEmpty(article.Comment))
            {
                line.Comment += "<br/>" + article.Comment;
            }
            line.Comment = article.CommentLog +
                           GetCommentHeader() + Encode(line.Comment);
            lines.Add(line);
        }

        private static string GetCommentHeader()
        {
            return string.Format("<br/><b>Магазин ({0}):</b><br/>", DateTime.Today.ToString(DateFormat));
        }

        private static string Encode(string str)
        {
            return str;
        }
    }
}
