﻿using System;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text.RegularExpressions;
using TicketTool.Projects.Data.Proxy.TicketToolAuth;

namespace TicketTool.Projects.OrderTaskForm.Common
{
    public class AuthWrapper
    {
        public string TicketCookie { get; private set; }

        public UserInfo GetCurrentUser(Func<UserInfo> method, IContextChannel serviceInnerChannel)
        {
            CheckArgs(method, serviceInnerChannel, false);

            using (new OperationContextScope(serviceInnerChannel))
            {
                var requestProperty = new HttpRequestMessageProperty();
                OperationContext.Current.OutgoingMessageProperties.Add(HttpRequestMessageProperty.Name, requestProperty);
                requestProperty.Headers.Add(HttpRequestHeader.Cookie, this.TicketCookie);
                var res = method();
                if (!res.IsAuthtorized)
                {
                    TicketCookie = null;
                }
                return res;
            }
        }

        public void Logout()
        {
            TicketCookie = null;
        }

        public bool Login(Func<bool> method, IContextChannel serviceInnerChannel)
        {
            CheckArgs(method, serviceInnerChannel, false);

            using (new OperationContextScope(serviceInnerChannel))
            {
                if (!method())
                {
                    TicketCookie = null;
                    return false;
                }
                var properties = OperationContext.Current.IncomingMessageProperties;
                var responseProperty = (HttpResponseMessageProperty)properties[HttpResponseMessageProperty.Name];
                var cookiesStr = responseProperty.Headers[HttpResponseHeader.SetCookie];
                TicketCookie = Regex.Match(cookiesStr, @".ASPXAUTH=[A-Z0-9]*").Captures[0].Value;
                return true;
            }
        }

        public T Execute<T>(Func<T> method, IContextChannel serviceInnerChannel)
        {
            CheckArgs(method, serviceInnerChannel);

            using (new OperationContextScope(serviceInnerChannel))
            {
                var requestProperty = new HttpRequestMessageProperty();
                OperationContext.Current.OutgoingMessageProperties.Add(HttpRequestMessageProperty.Name, requestProperty);
                requestProperty.Headers.Add(HttpRequestHeader.Cookie, this.TicketCookie);
                return method();
            }
        }
        

        public void ExecuteM(Action method, IContextChannel serviceInnerChannel)
        {
            CheckArgs(method, serviceInnerChannel);

            using (new OperationContextScope(serviceInnerChannel))
            {
                var requestProperty = new HttpRequestMessageProperty();
                OperationContext.Current.OutgoingMessageProperties.Add(HttpRequestMessageProperty.Name, requestProperty);
                requestProperty.Headers.Add(HttpRequestHeader.Cookie, this.TicketCookie);
                method();
            }
        }

        private void CheckArgs(object method, IContextChannel serviceInnerChannel, bool checkTicket = true)
        {
            if (method == null) throw new ArgumentNullException("method");
            if (serviceInnerChannel == null) throw new ArgumentNullException("serviceInnerChannel");
            if (checkTicket && String.IsNullOrWhiteSpace(this.TicketCookie))
            {
                throw new InvalidOperationException("Currently not logged in. Must Login before calling this method.");
            }
        }
    }
}