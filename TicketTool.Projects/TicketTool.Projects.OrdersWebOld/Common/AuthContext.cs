﻿using System.Web;

namespace TicketTool.Projects.OrderTaskForm.Common
{
    public class AuthContext
    {
        public static AuthWrapper Auth
        {
            get
            {
                var auth = (AuthWrapper)HttpContext.Current.Session["Auth"];
                if (auth == null)
                {
                    HttpContext.Current.Session["Auth"] = auth = new AuthWrapper();
                }
                return auth;
            }
        }

        private AuthContext()
        {
        }
    }
}