﻿using System.Web.Optimization;

namespace TicketTool.Projects.OrderForm.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery.inputmask/jquery.inputmask-{version}.js",
                        "~/Scripts/jquery.keyfilter.js",
                        "~/Scripts/jquery.maskedinput-{version}.js",
                        "~/Scripts/jquery-ui-{version}.full.js",
                        "~/Scripts/custom/jquery.datepicker.russian.js",
                        "~/Scripts/jquery.tinysort.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        "~/Scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/dialogs").IncludeDirectory("~/Scripts/dialogs/", "*.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/bootstrap.css",
                        "~/Content/bootstrap-responsive.css",
                        "~/Content/jquery-ui-1.10.3.full.css",
                        "~/Content/site.css"
                        ));

        }
    }
}