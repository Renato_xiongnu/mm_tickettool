﻿using System.Collections.Generic;
using System;
using System.Linq;
using System.Web.Mvc;
using TicketTool.Projects.Data;
using TicketTool.Projects.Data.Model;
using TicketTool.Projects.Data.Strategies.Contracts;

namespace TicketTool.Projects.OrderForm.Controllers
{
    public class HomeController : Controller
    {
        private static IOrderStrategy Orders
        {
            get { return new DataFacade(new HttpCache()).Orders; }
        }

        [NonAction]
        public CustomerOrder PrepareOrder(CustomerOrder order)
        {
            ModelState.Clear();
            order.IsApproved = false;
            this.RefreshArticles(order);
            return order;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View("Index", new CustomerOrder());
        }

        [HttpGet]
        public ActionResult Order(string orderId, string ro)
        {
            var result = Orders.GetOrder(orderId);
            if(result.HasErrors)
                throw new InvalidOperationException(result.Message);

            var order = PrepareOrder(result.Item);
            order.ReadOnly = ro == "1";
            return View(order.ReadOnly ? "Display" : "Index", order);
        }

        [HttpGet]
        public ActionResult Result(CustomerOrder order)
        {
            return View("Result", order);
        }

        [HttpGet]
        public ActionResult GeneratedOrder()
        {
            var order = Orders.CreateTestOrder().Item;
            this.RefreshArticles(order);
            return View("Index", order);
        }

        [HttpPost]
        public ActionResult Index(CustomerOrder order)
        {
            this.RefreshArticles(order);
            if (ModelState.IsValid
                && order.IsApproved
                && order.HasArticles
                && order.AllArticles.All(m => !m.IsDataWrong && !m.IsWrong))
            {
                UpdtateComments(order.BasketArticles);
                var result = Orders.UpdateOrder(order);
                if (result.HasErrors)
                    throw new InvalidOperationException(result.Message);
            }
            else
            {
                return View("Index", order);
            }
            return RedirectToAction("Order", new {orderId = order.OrderId});
            return View("Result", order);
        }

        [HttpPost]
        public ActionResult Validate(CustomerOrder order)
        {
            order = PrepareOrder(order);
            UpdtateComments(order.BasketArticles);
            return View("Index", order);
        }

        #region [ArticleActions]

        [HttpPost]
        public ActionResult AddArticle(CustomerOrder order)
        {
            order.NewArticles.Add(new ArticleLine{IsWrong = true});
            order = PrepareOrder(order);
            return View("Index", order);
        }

        [HttpPost]
        public ActionResult DeleteNewArticle(CustomerOrder order, int index)
        {
            order.NewArticles.RemoveAt(index);
            order = PrepareOrder(order);
            return View("Index", order);
        }

        [HttpPost]
        public ActionResult DeleteBasketArticle(CustomerOrder order, int index)
        {
            order.BasketArticles.RemoveAt(index);
            order = PrepareOrder(order);
            return View("Index", order);
        }

        [HttpPost]
        public ActionResult AddQty(CustomerOrder order, int index)
        {
            order.BasketArticles[index].Qty++;
            order = PrepareOrder(order);
            return View("Index", order);
        }

        [HttpPost]
        public ActionResult SubQty(CustomerOrder order, int index)
        {
            order.BasketArticles[index].Qty--;
            if (order.BasketArticles[index].Qty == 0)
                order.BasketArticles.RemoveAt(index);
            order = PrepareOrder(order);
            return View("Index", order);
        }

        #endregion

        private static void UpdtateComments(IEnumerable<ArticleLine> basketArticles)
        {
            var now = DateTime.Now;
            foreach (var article in basketArticles.Where(ba => !String.IsNullOrEmpty(ba.NewComment)))
            {
                var newComment = article.NewComment.Trim().Replace("<", "&lt;").Replace(">", "&gt;");
                article.Comments.Add(new Comment{Created = now, CreatedBy = "Call center", Text = newComment});
                article.NewComment = String.Empty;
            }
        }

        private void RefreshArticles(CustomerOrder order)
        {
            var articlesToRemove = new List<ArticleLine>();
            foreach (var newArticle in order.NewArticles)
            {
                var basketArticle = order.BasketArticles.FirstOrDefault(m => m.ArticleNum == newArticle.ArticleNum);
                if (basketArticle == null) continue;
                basketArticle.Qty += newArticle.Qty;
                articlesToRemove.Add(newArticle);
            }
            foreach (var removeArticle in articlesToRemove)
            {
                order.NewArticles.Remove(removeArticle);
            }

            order.IsApproved = true;

            foreach (var item in order.AllArticles)
            {
                item.IsWrong = false;
            }

            var newBasketArticles = order.NewArticles.Where(m => !m.IsDataWrong).ToList();
            order.BasketArticles.AddRange(newBasketArticles);
            foreach (var article in newBasketArticles)
            {
                order.NewArticles.Remove(article);
            }
        }
    }    
}
