﻿using System;
using System.Web.Mvc;

namespace TicketTool.Projects.OrderForm.Controllers
{
    public class ErrorsController : Controller
    {
        public ActionResult General(Exception exception)
        {
            return View("Error", exception);
        }

        public ActionResult Http404()
        {
            return View("Error", new Exception("404"));
        }

        public ActionResult Http403()
        {
            return View("Error", new Exception("403"));
        }

        public ActionResult Error(string message)
        {
            return View("Error", new Exception(message));
        }
    }
}
