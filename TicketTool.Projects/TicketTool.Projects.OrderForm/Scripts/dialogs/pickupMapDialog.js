﻿$(function () {
    makeDialog("#pickupMapDialog", {
        title: "Карта",
        width:700,
        open: function () {
            
        },
        buttons: {
            "OK": function () {
                $('select[id$="DeliveryPlace"]').val($(this).find('.pickupRadio:checked').val());
                $(this).dialog("close");
            },
            Cancel: function () {
            }
        }
    });
    $('.pickupMapLink').on('click', function () {
        $("#pickupMapDialog").dialog("open");
    });
});
function handleKilos(container, coords) {
    var destCoords = [$('.pointLatitude', container).val(), $('.pointLongitude', container).val()];
    var distance = (ymaps.coordSystem.geo.getDistance(coords, destCoords) / 1000).toFixed(1);
    var kilos = $('.pointKilos', container);
    kilos.html(distance + " км");
    container.attr('data-kilos', distance);
}