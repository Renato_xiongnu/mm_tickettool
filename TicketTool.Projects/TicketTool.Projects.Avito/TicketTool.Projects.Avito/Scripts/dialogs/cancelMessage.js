﻿$(function () {
    makeDialog("#cancelBoardMessageDialog", {
        title: "Удаление объявления",
        buttons: {
            "Удалить": function () {
                $("input#cancelOrderId", this).val($(this).data('orderId'));
                $('form', this).submit();
            },
            "Отмена": function () {
                $(this).dialog("close");
            }
        }
    });
    $(".cancelBoardMessageLink").click(function () {
        $("#cancelBoardMessageDialog").data('orderId', $(this).attr('data-orderId')).dialog("open");
    });
});