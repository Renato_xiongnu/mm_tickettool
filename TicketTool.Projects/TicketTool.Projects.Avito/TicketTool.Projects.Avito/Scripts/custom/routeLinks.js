﻿jQuery.routeLinks || (function ($, window, undefined) {
    $.routeLinks = {
        ShowBoardMessage: approot + '/BoardMessage/DialogDetails',
        CreateBoardMessage: approot + '/BoardMessage/DialogCreate',
        CheckName: approot + '/BoardMessage/CheckName',
        EditBoardMessage: approot + '/BoardMessage/DialogEdit',
        ReloadGrid: approot + '/BoardMessageGrid/BoardMessageXGrid',
        Error: approot + '/Errors/Error'
    };
})(jQuery, this);