﻿using System.Linq;
using DevExpress.Data;
using DevExpress.Data.Filtering;
using DevExpress.Data.Linq;
using DevExpress.Data.Linq.Helpers;
using DevExpress.Web.Mvc;
using TicketTool.Projects.Avito.Common;
using System;
using System.Web.Mvc;

namespace TicketTool.Projects.Avito.Controllers
{
    [Authorize]
    public class BoardMessageGridController : Controller
    {
        private readonly IDataFacade _dataFacade;

        public BoardMessageGridController(IDataFacade dataFacade)
        {
            _dataFacade = dataFacade;
        }

        public ActionResult BoardMessageXGrid()
        {
            var viewModel = GridViewExtension.GetViewModel("boardMessageGridView") ?? CreateGridViewModel();
            if (Request.Params["filterExpression"] != null) viewModel.FilterExpression = Request.Params["filterExpression"];
            return BoardMessageXGridCore(viewModel);
        }

        public ActionResult BoardMessageXGridPaging([ModelBinder(typeof(DevExpressEditorsBinder))]GridViewPagerState pager)
        {
            var viewModel = GridViewExtension.GetViewModel("boardMessageGridView");
            viewModel.Pager.Assign(pager);
            return BoardMessageXGridCore(viewModel);
        }

        public ActionResult BoardMessageXGridFiltering([ModelBinder(typeof(DevExpressEditorsBinder))]GridViewColumnState column)
        {
            var viewModel = GridViewExtension.GetViewModel("boardMessageGridView");
            viewModel.Columns[column.FieldName].Assign(column);
            return BoardMessageXGridCore(viewModel);
        }

        public ActionResult BoardMessageXGridSorting([ModelBinder(typeof(DevExpressEditorsBinder))]GridViewColumnState column, bool reset)
        {
            var viewModel = GridViewExtension.GetViewModel("boardMessageGridView");
            viewModel.SortBy(column, reset);
            return BoardMessageXGridCore(viewModel);
        }


        public PartialViewResult BoardMessageXGridCore(GridViewModel gridViewModel)
        {
            bool hideCanceled;
            if (!String.IsNullOrEmpty(Request.Params["hideCanceled"]) && bool.TryParse(Request.Params["hideCanceled"], out hideCanceled))
            {
                //string filter = GridViewExtensions.CreateFilter("OrderStatus", "<", Convert.ToInt16(OrderLink.InternalOrderStatus.Closed) + "s");
                //gridViewModel.FilterExpression = gridViewModel.FilterExpression.ClearFilter(filter);
                //if (hideCanceled)
                //{
                //    gridViewModel.FilterExpression = gridViewModel.FilterExpression.ApplyFilter(filter);
                //}
            }
            gridViewModel.ProcessCustomBinding(GetGridCount, GetGridData);
            return PartialView("BoardMessageControls/BoardMessageXGrid", gridViewModel);
        }

        private void GetGridCount(GridViewCustomBindingGetDataRowCountArgs e)
        {            
            e.DataRowCount = _dataFacade.Adverts.AsQueryable()
                              .AppendWhere(new CriteriaToExpressionConverter(), CriteriaOperator.Parse(e.State.FilterExpression))
                              .Count();
        }

        private void GetGridData(GridViewCustomBindingGetDataArgs e)
        {            
            var adverts = _dataFacade.Adverts.AsQueryable()
                                .AppendWhere(new CriteriaToExpressionConverter(), CriteriaOperator.Parse(e.State.FilterExpression));
            e.State.SortedColumns.Aggregate(adverts,
                                            (current, column) => adverts.MakeOrderBy(new CriteriaToExpressionConverter(), new ServerModeOrderDescriptor(new OperandProperty(column.FieldName), column.SortOrder == ColumnSortOrder.Descending)));
            e.Data = adverts
                .Skip(e.StartDataRowIndex)
                .Take(e.DataRowCount);
            //.Cast<CustomerOrder>()
            //.ToString().Select(o => o.ToModel());
        }

        [NonAction]
        private static GridViewModel CreateGridViewModel()
        {
            var viewModel = new GridViewModel {KeyFieldName = "ID"};
            viewModel.Columns.Add("ExternalNumber");
            viewModel.Columns.Add("SapCode");
            viewModel.Columns.Add("StoreName");
            viewModel.Columns.Add("OrderId");
            viewModel.Columns.Add("WWSOrderId");
            viewModel.Columns.Add("OrderStatus");
            viewModel.Pager.PageSize = 10;
            return viewModel;
        }

    }
}
