﻿using System;
using System.Linq;
using System.Web.Mvc;
using TicketTool.Projects.Avito.Common;
using TicketTool.Projects.Avito.Models;
using TicketTool.Projects.Avito.Common.Attributes;
using System.Web.Security;
using TicketTool.Projects.Avito.Proxy.OrdersService;
using TicketTool.Projects.Avito.Proxy.TicketToolAuth;

namespace TicketTool.Projects.Avito.Controllers
{
    [Authorize] 
    public class BoardMessageController : Controller
    {
        private readonly IDataFacade _dataFacade;

        public BoardMessageController(IDataFacade dataFacade)
        {
            _dataFacade = dataFacade;
        }          
        
        public ActionResult Index()
        {
            return View("Index");
        }

        #region [Create]
        
        public ActionResult Create()
        {
            ViewData["OpType"] = "Create";
            ViewData["ShowSubmit"] = true;
            return View(new BoardMessageItem {AvailableStores = _dataFacade.AvailableStores});
        }
        
        [HttpPost]
        public ActionResult Create(BoardMessageItem item)
        {
            ViewData["OpType"] = "Create";
            BoardMessageItem.ChangeErrors(ModelState, Roles.GetRolesForUser(), "Create");
            if (ModelState.IsValid)
            {
                _dataFacade.CreateAdvert(item);
                return RedirectToAction("Index");
            }
            ViewData["ShowSubmit"] = true;
            return View(item);
        }
        
        [ExceptionHandler]
        public ActionResult DialogCreate()
        {
            ViewData["OpType"] = "Create";
            return PartialView("BoardMessageControls/BoardMessageForm",
                               new BoardMessageItem {AvailableStores = _dataFacade.AvailableStores});
        }        

        #endregion

        #region [Edit]
        
        public ActionResult Edit(string orderId)
        {
            ViewData["OpType"] = "Edit";
            ViewData["ShowSubmit"] = true;
            var item = _dataFacade.Adverts.Single(a => a.OrderId == orderId);
            if (item.IsCanceled) throw new Exception("Invalid action");
            return View(item);
        }
        
        [ExceptionHandler]
        public ActionResult DialogEdit(string orderId)
        {
            ViewData["OpType"] = "Edit";
            var item = _dataFacade.Adverts.Single(a => a.OrderId == orderId);
            if (item.IsCanceled) throw new Exception("Invalid action");
            return PartialView("BoardMessageControls/BoardMessageForm", item);
        }
        
        [HttpPost]
        public ActionResult Edit(BoardMessageItem item)
        {
            ViewData["OpType"] = "Edit";
            BoardMessageItem.ChangeErrors(ModelState, Roles.GetRolesForUser());
            if (ModelState.IsValid)
            {
                _dataFacade.UpdateAdvert(item);
                return RedirectToAction("Index");
            }
            ViewData["ShowSubmit"] = true;
            return View(item);
        }

        #endregion

        #region [Details]
        
        [ExceptionHandler]
        public ActionResult DialogDetails(string orderId)
        {
            return PartialView("BoardMessageControls/DisplayForm", _dataFacade.Adverts.Single(a => a.OrderId == orderId));
        }
        
        public ActionResult Details(string orderId)
        {
            return View(_dataFacade.Adverts.Single(a => a.OrderId == orderId));
        }

        #endregion

        #region [Cancel]
        
        public ActionResult Cancel(string orderId)
        {
            var item = _dataFacade.Adverts.Single(a => a.OrderId == orderId);
            if (item.IsCanceled) throw new Exception("Invalid action");
            return View(item);
        }
        
        public JsonResult CheckName(string name)
        {
            return Json(this._dataFacade.Adverts.All(m => m.ExternalNumber != name), JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        public ActionResult Cancel(string cancelOrderId, string cancelReason)
        {
            var user = Membership.GetUser();
            if (!String.IsNullOrEmpty(cancelOrderId) && !String.IsNullOrEmpty(cancelReason))
            {
                var item = _dataFacade.Adverts.Single(a => a.OrderId == cancelOrderId);
                if (item.IsCanceled) throw new Exception("Invalid action");
                var client = new OrderServiceClient();
                client.CancelOrder(new CancelOrderRequest
                    {
                        CancelBy = user != null ? user.UserName : String.Empty, 
                        CancelReason = cancelReason,
                        OrderId = cancelOrderId
                    });
            }
            return RedirectToAction("Index");
        }

        #endregion

    }
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   