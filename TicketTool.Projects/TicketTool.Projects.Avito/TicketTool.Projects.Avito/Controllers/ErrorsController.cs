﻿using System;
using System.Web.Mvc;

namespace TicketTool.Projects.Avito.Controllers
{
    public class ErrorsController : Controller
    {
        public ActionResult General(Exception exception)
        {
            return View("Error", exception);
        }

        public ActionResult Http404()
        {
            return View("Error", new Exception("Not found"));
        }

        public ActionResult Http403()
        {
            return View("Error", new Exception("Forbidden"));
        }

        public ActionResult Error(string message)
        {
            return View("Error", new Exception(message));
        }
    }
}
