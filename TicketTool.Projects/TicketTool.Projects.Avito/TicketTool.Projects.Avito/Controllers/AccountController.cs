﻿using System;
using System.Web.Mvc;
using TicketTool.Projects.Avito.Models.Account;
using TicketTool.Projects.Avito.Proxy.TicketToolAuth;

namespace TicketTool.Projects.Avito.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult Login()
        {
            var loginModel = new LoginModel();
            return View(loginModel);
        }

        [HttpPost]
        public ActionResult Login(LoginModel loginModel)
        {
            var client = new AuthenticationServiceClient();
            if (client.LogIn(loginModel.Name, loginModel.Password))
            {
                if (!String.IsNullOrEmpty(loginModel.ReturnUrl))
                    return Redirect(loginModel.ReturnUrl);
                return RedirectToAction("BoardMessageXGrid", "BoardMessageGrid");
            }
            ModelState.AddModelError("","Комбинация имени и пароля не найдена");
            return View(loginModel);
        }

        public ActionResult Logout()
        {
            var client = new AuthenticationServiceClient();
            client.LogOut();
            return Redirect(Request.UrlReferrer.ToString());
        }

        public ActionResult GetHeader()
        {
            return PartialView("AccountControls/LoginHeader");
        }
    }
}
