﻿using log4net;

namespace TicketTool.Projects.Avito.Logging
{
    public class Log4LoggerFactory : ILoggerFactory
    {
        public ILogger GetLogger(string name)
        {
            return new Log4NetLogger(LogManager.GetLogger(name));
        }
    }
}