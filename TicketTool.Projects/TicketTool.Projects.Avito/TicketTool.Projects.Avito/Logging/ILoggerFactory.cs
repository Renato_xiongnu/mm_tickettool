﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicketTool.Projects.Avito.Logging
{
    public interface ILoggerFactory
    {
        ILogger GetLogger(string name);
    }
}