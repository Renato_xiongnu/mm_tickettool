﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using System.Web;
using log4net;
using TicketTool.Projects.Avito.Logging;
using TicketTool.Projects.Avito.Proxy.OrdersService;
using Timer = System.Timers.Timer;

namespace TicketTool.Projects.Avito.Data
{
    public class CustomerOrdersSync
    {
        private readonly CustomerOrdersRepository _customerOrdersRepository;

        private readonly Timer _timer;

        private readonly int _pageSize;

        private long _lastSequance = 0;

        private readonly ILogger _logger;

        public CustomerOrdersSync(
            TimeSpan pollingInterval,
            int pollingPageSize,
            CustomerOrdersRepository customerOrdersRepository,
            ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.GetLogger(GetType().Name);
            _customerOrdersRepository = customerOrdersRepository;
            _pageSize = pollingPageSize;
            _timer = new Timer
            {
                AutoReset = false,
                Interval = pollingInterval.TotalMilliseconds
            };
            _timer.Elapsed += Timer_Elapsed;
            _timer.Start();
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                Sync();
            }
            finally
            {
                _timer.Start();
            }
        }

        private void Sync()
        {
            using(var client = new OrderServiceClient())
            {
                int index;
                do
                {                    
                    _logger.ErrorFormat("Actualize start (LastSequance={0})", _lastSequance);
                    var response = client.GetOrders(_lastSequance, _pageSize);
                    if(response.ReturnCode == ReturnCode.Error)
                    {
                        _logger.ErrorFormat("Actualize error(LastSequance={0}): {1}", _lastSequance, response.Message);
                        break;
                    }
                    _customerOrdersRepository.Save(response.CustomerOrders);
                    index = response.CustomerOrders.Count;
                    _lastSequance = response.LastSequance;
                    _logger.ErrorFormat("Actualize complete (LastSequance={0})", _lastSequance);
                } while (_pageSize == index);
            }
        }
    }
}