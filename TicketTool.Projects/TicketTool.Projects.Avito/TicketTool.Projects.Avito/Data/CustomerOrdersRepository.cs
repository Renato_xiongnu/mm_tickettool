﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using TicketTool.Projects.Avito.Proxy.OrdersService;

namespace TicketTool.Projects.Avito.Data
{
    public class CustomerOrdersRepository
    {                
        private readonly ReaderWriterLockSlim _locker;

        private readonly Dictionary<string, CustomerOrder> _items;

        public CustomerOrdersRepository()
        {            
            _items = new Dictionary<string, CustomerOrder>();            
            _locker = new ReaderWriterLockSlim();
        }

        public IEnumerable<CustomerOrder> Items
        {
            get
            {               
                _locker.EnterReadLock();
                try
                {
                    return _items.Values;
                }
                finally
                {
                    _locker.ExitReadLock();
                }
            }
        }

        public CustomerOrder Save(CustomerOrder order)
        {
            ValidateOrder(order);
            _locker.EnterWriteLock();
            try
            {
                return InternalSave(order);
            }
            finally
            {
                _locker.ExitWriteLock();
            }
        }

        private CustomerOrder InternalSave(CustomerOrder order)
        {
            ValidateOrder(order);
            _items[order.OrderId] = order;
            return _items[order.OrderId];
        }

        private static CustomerOrder ValidateOrder(CustomerOrder order)
        {
            if(order == null)
            {
                throw new ArgumentNullException("order");
            }
            if(string.IsNullOrEmpty(order.OrderId))
            {
                throw new ArgumentException("Order.OrderId cannot be null or empty");
            }
            return order;
        }

        public IReadOnlyCollection<CustomerOrder> Save(IEnumerable<CustomerOrder> orders)
        {
            if(orders == null)
            {
                throw new ArgumentNullException("orders");
            }
            var list = orders.Select(ValidateOrder).ToList();
            _locker.EnterWriteLock();
            try
            {
                foreach (var customerOrder in list)
                {
                    InternalSave(customerOrder);
                }
                return list.AsReadOnly();
            }
            finally
            {
                _locker.ExitWriteLock();
            }
        }
    }
}