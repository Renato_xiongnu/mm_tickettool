﻿namespace TicketTool.Projects.Avito.Models
{
    public class StoreItem
    {
        public string SapCode { get; set; }
        public string Name { get; set; }
    }
}