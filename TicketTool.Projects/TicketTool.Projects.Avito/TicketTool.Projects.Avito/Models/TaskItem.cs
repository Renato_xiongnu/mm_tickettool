﻿using System;

namespace TicketTool.Projects.Avito.Models
{
    public class TaskItem
    {
        public string WWSOrderId { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime DeadlineDate { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public bool IsAssignedOnMe { get; set; }
    }
}