﻿using System.Collections.Generic;

namespace TicketTool.Projects.Avito.Models
{
    public class ManagerTasksData
    {
        public ManagerTasksData()
        {
            Tasks = new List<TaskItem>();
        }


        public string StoreId { get; set; }
        public List<StoreItem> Stores { get; set; }
        public List<TaskItem> Tasks { get; set; }
    }
}