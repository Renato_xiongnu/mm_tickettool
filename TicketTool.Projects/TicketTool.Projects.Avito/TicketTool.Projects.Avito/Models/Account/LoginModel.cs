﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TicketTool.Projects.Avito.Models.Account
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Обязательное поле")]
        [DisplayName("Имя")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [DataType(DataType.Password)]
        [DisplayName("Пароль")]
        public string Password { get; set; }

        public string ReturnUrl { get; set; }
    }
}