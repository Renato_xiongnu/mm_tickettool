﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace TicketTool.Projects.Avito.Models
{
    public class BoardMessageItem
    {
        public BoardMessageItem()
        {
            AvailableStores = new List<StoreItem>();
        }

        [Display(Name = "Номер заказа")]
        public string OrderId { get; set; }

        private static readonly DateTime ExpirationDateDefault = DateTime.Now.AddDays(30);

        private DateTime _expirationDate = ExpirationDateDefault;

        [Required(ErrorMessage = "Поле '{0}' обязательно.")]
        [Display(Name = "Номер объявления")]
        public string ExternalNumber { get; set; }

        [Required(ErrorMessage = "Поле '{0}' не заполнено.")]
        [Display(Name = "Номер артикула")]
        public string ArticleNum { get; set; }

        [Required(ErrorMessage = "Поле '{0}' не заполнено.")]
        [RegularExpression(@"^\d+$", ErrorMessage = "Неправильное значение поля '{0}'")]
        [Range(1, 1000, ErrorMessage = "Значение поля '{0}' должно являться целым числом и быть в диапазоне [1;1000]")]
        [Display(Name = "Количество")]
        public int? ArticleQty { get; set; }

        [DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Поле '{0}' не заполнено.")]
        [RegularExpression(@"^\d+(\.\d+)?$", ErrorMessage = "Неправильное значение поля '{0}'")]
        [Display(Name = "Цена продажи")]
        public decimal? ArticlePrice { get; set; }

        [Required(ErrorMessage = "Поле '{0}' не заполнено.")]
        [Display(Name = "Описание состояния")]
        public string Comment { get; set; }

        [Required(ErrorMessage = "Поле '{0}' не заполнено.")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd.MM.yyyy}")]
        [Display(Name = "Размещен до")]
        public DateTime ExpirationDate
        {
            get { return _expirationDate; }
            set { _expirationDate = value; }
        }

        [Required(ErrorMessage = "Поле '{0}' не заполнено.")]
        [Display(Name = "Фамилия")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Поле '{0}' не заполнено.")]
        [Display(Name = "Имя")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Поле '{0}' не заполнено.")]
        [Display(Name = "Телефон")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Поле '{0}' не заполнено.")]
        [RegularExpression(@"^[^<>\s\@]+(\@[^<>\s\@]+(\.[^<>\s\@]+)+)$", ErrorMessage = "Неверный email.")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Поле '{0}' не заполнено.")]
        [Display(Name = "Код магазина")]
        public string SapCode { get; set; }

        public IEnumerable<StoreItem> AvailableStores { get; set; }

        [Display(Name = "Название магазина")]
        public string StoreName { get; set; }

        [Display(Name = "Номер резерва WWS")]
        public string WWSOrderId { get; set; }

        [Display(Name = "Наименование товара")]
        public string ArticleTitle { get; set; }

        [Display(Name = "Статус")]
        public string Status { get; set; }

        public bool IsCanceled
        {
            get { return Status != null && Status.Equals("Отменено", StringComparison.InvariantCultureIgnoreCase); }
        }

        public string StatusInfo
        {
            get
            {
                if(String.IsNullOrEmpty(Status))
                {
                    return "в процессе создания";
                }
                return IsCanceled ? "отменено" : "активно";
            }
        }

        [Display(Name = "Создан")]
        public DateTime Created { get; set; }

        public static void ChangeErrors(ModelStateDictionary errors, string[] roles, string actionType = null)
        {
            if(actionType == "Create" || roles.Contains("AvitoManager"))
            {
                errors.Remove("Name");
                errors.Remove("Surname");
                errors.Remove("Phone");
                errors.Remove("Email");
            }
            else if(roles.Contains("Agent"))
            {
                errors.Remove("ExternalNumber");
                errors.Remove("Comment");
                errors.Remove("ArticleNum");
                errors.Remove("ArticleQty");
                errors.Remove("ArticlePrice");
                errors.Remove("ExpirationDate");
            }
        }
    }
}