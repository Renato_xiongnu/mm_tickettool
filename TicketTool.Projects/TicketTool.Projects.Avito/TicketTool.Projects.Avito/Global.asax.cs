﻿using System;
using System.Globalization;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using TicketTool.Projects.Avito.Common;
using TicketTool.Projects.Avito.Controllers;
using TicketTool.Projects.Avito.Logging;

namespace TicketTool.Projects.Avito
{
    public class MvcApplication : HttpApplication
    {
        private static ILogger Logger;

        public MvcApplication()
        {
        }

        protected void Application_Start()
        {
            var container = InitContainer();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            Logger = container.Resolve<ILoggerFactory>().GetLogger(MethodBase.GetCurrentMethod().DeclaringType.Name);

            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            BundleTable.EnableOptimizations = false;
            var binder = new DateTimeModelBinder("dd.MM.yyyy");
            ModelBinders.Binders.Add(typeof(DateTime), binder);
            ModelBinders.Binders.Add(typeof(DateTime?), binder);
        }

        private IContainer InitContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new PresentationLayer());
            var container = builder.Build();
            return container;
        }

        protected void Application_BeginRequest()
        {
            var newCulture = (CultureInfo) CultureInfo.CurrentCulture.Clone();
            newCulture.NumberFormat.CurrencyDecimalSeparator = ".";
            newCulture.NumberFormat.NumberDecimalSeparator = ".";
            newCulture.NumberFormat.PercentDecimalSeparator = ".";

            Thread.CurrentThread.CurrentCulture = newCulture;
            Thread.CurrentThread.CurrentUICulture = newCulture;
        }

        protected void Application_Error()
        {
            var exception = Server.GetLastError();
            var httpException = exception as HttpException;
            Response.Clear();
            Server.ClearError();
            var routeData = new RouteData();
            routeData.Values["controller"] = "Errors";
            routeData.Values["action"] = "General";
            routeData.Values["exception"] = exception != null && exception.InnerException != null
                ? exception.InnerException
                : exception;
            Logger.Error(exception);
            Response.StatusCode = 500;
            if(httpException != null)
            {
                Response.StatusCode = httpException.GetHttpCode();
                switch(Response.StatusCode)
                {
                    case 403:
                        routeData.Values["action"] = "Http403";
                        break;
                    case 404:
                        routeData.Values["action"] = "Http404";
                        break;
                }
            }

            IController errorsController = new ErrorsController();
            var rc = new RequestContext(new HttpContextWrapper(Context), routeData);
            errorsController.Execute(rc);
        }
    }
}