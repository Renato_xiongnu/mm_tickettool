﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TicketTool.Projects.Avito.Common
{
    public class DecimalModelBinder : DefaultModelBinder
    {
        private CultureInfo _cultureInfo = CultureInfo.InvariantCulture;


        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (value != null && !String.IsNullOrEmpty(value.AttemptedValue))
            {
                object actualValue = null;
                try
                {
                    actualValue = Convert.ToDecimal(value.AttemptedValue,_cultureInfo);
                    return actualValue;
                }
                catch (FormatException e)
                {
                    bindingContext.ModelState.AddModelError(
                        bindingContext.ModelName,
                        string.Format("{0} Невалидное значение числа", value.AttemptedValue)
                    );
                }
            }
            return base.BindModel(controllerContext, bindingContext);
        }
    }
}