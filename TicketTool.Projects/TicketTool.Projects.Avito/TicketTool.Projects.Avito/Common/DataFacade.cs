﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TicketTool.Projects.Avito.Models;
using TicketTool.Projects.Avito.Properties;
using TicketTool.Projects.Avito.Proxy.OrdersService;
using TicketTool.Projects.Avito.Proxy.TicketToolAuth;
using TicketTool.Projects.Avito.Proxy.TicketToolService;

namespace TicketTool.Projects.Avito.Common
{
    public class DataFacade : IDataFacade
    {
        private readonly AuthenticationServiceClient.Factory _authenticationServiceClientFactory;

        private readonly OrderServiceClient.Factory _orderServiceClientFactory;

        private const string OrdersCacheKey = "AVITO_orders";

        private List<BoardMessageItem> _adverts;

        private List<StoreItem> _stores;

        public DataFacade(
            AuthenticationServiceClient.Factory authenticationServiceClientFactory,
            OrderServiceClient.Factory orderServiceClientFactory)
        {
            _authenticationServiceClientFactory = authenticationServiceClientFactory;
            _orderServiceClientFactory = orderServiceClientFactory;
        }

        private static IEnumerable<CustomerOrder> Orders
        {
            get
            {
                return RefreshOrders().Orders;

                var ordersCache = HttpContext.Current.Cache[OrdersCacheKey] as OrdersCache;
                if(ordersCache == null || ordersCache.Orders == null)
                {
                    return RefreshOrders().Orders;
                }

                return ordersCache.Cached.AddSeconds(Settings.Default.OrdersTTL) < DateTime.Now
                    ? RefreshOrders().Orders
                    : ordersCache.Orders;
            }
        }

        private static OrdersCache RefreshOrders()
        {
            var client = new OrderServiceClient();
            var result = client.GetOrdersBySourceSystem(Settings.Default.Avito);

            if(result.ReturnCode == ReturnCode.Error)
            {
                throw new InvalidOperationException(result.Message);
            }

            var cache = new OrdersCache
            {
                Cached = DateTime.Now,
                Orders = result.CustomerOrders ?? new List<CustomerOrder>()
            };
            HttpContext.Current.Cache[OrdersCacheKey] = cache;
            return cache;
        }

        public IEnumerable<StoreItem> AvailableStores
        {
            get
            {
                return _stores ?? (_stores = new TicketToolServiceClient()
                    .GetCurrentPerformer()
                    .Parameters
                    .Select(p => new StoreItem
                    {
                        Name = p.Name,
                        SapCode = p.ParameterName
                    })
                    .ToList());
            }
        }

        public IEnumerable<BoardMessageItem> Adverts
        {
            get
            {
                using(var client = _authenticationServiceClientFactory())
                {
                    var sl = client.GetCurrentUser()
                        .Parameters
                        .Select(p => String.Concat(Settings.Default.SaleLocationShopPrefix, p));

                    return _adverts ?? (_adverts = Orders
                        .Where(o => sl.Contains(o.SaleLocation.SaleLocationId))
                        .OrderByDescending(o => o.OrderInfo.CreateData)
                        .Select(o => o.ToModel())
                        .ToList());
                }

               
            }
        }

        public void CreateAdvert(BoardMessageItem item)
        {
            using(var autClient = _authenticationServiceClientFactory())
            {
                using (var client = _orderServiceClientFactory())
                {
                    var userData = autClient.GetCurrentUser();
                    var customerOrder = new CustomerOrder
                    {
                        Customer = new Customer {Surname = Settings.Default.Avito},
                        OrderInfo = new OrderInfo
                        {
                            IsPreorder = true,
                            ExternalOrderId = item.ExternalNumber,
                            ExpirationDate = item.ExpirationDate,
                            CreatedBy = userData.LoginName,
                            CustomerComment = item.Comment,
                            ShouldBeProcessedInTT = true,
                            OrderSource = Settings.Default.CompanyName,
                            OrderSourceSystem = Settings.Default.Avito
                        },
                        SaleLocation = new SaleLocationInfo
                        {
                            SaleLocationId = String.Concat(Settings.Default.SaleLocationShopPrefix, item.SapCode)
                        },
                        OrderLines = new List<OrderLine>
                        {
                            new OrderLine
                            {
                                ArticleNo = Int32.Parse(item.ArticleNum),
                                ArticleTitle = item.ArticleTitle,
                                Price = item.ArticlePrice.HasValue ? item.ArticlePrice.Value : default(decimal),
                                Quantity = item.ArticleQty.HasValue ? item.ArticleQty.Value : default(int)
                            }
                        }
                    };
                    var result = client.CreateOrder(customerOrder);

                    if(result.ReturnCode == ReturnCode.Error)
                    {
                        throw new InvalidOperationException(result.Message);
                    }
                }
            }
        }

        public void UpdateAdvert(BoardMessageItem item)
        {
            using (var client = _orderServiceClientFactory())
            {
                var getOrderResult = client.GetOrder(item.OrderId);
                if (getOrderResult.ReturnCode == ReturnCode.Error)
                {
                    throw new InvalidOperationException(getOrderResult.Message);
                }

                var order = getOrderResult.CustomerOrder;

                var wwsOrder = order.WwsOrders.LastOrDefault();
                if (wwsOrder != null)
                {
                    var reserveLine = wwsOrder.Items.FirstOrDefault();
                    if (reserveLine != null)
                    {
                        if (item.ArticleQty.HasValue)
                        {
                            reserveLine.Quantity = item.ArticleQty.Value;
                        }
                        if (item.ArticlePrice.HasValue)
                        {
                            reserveLine.Price = item.ArticlePrice.Value;
                        }
                    }
                }

                order.OrderInfo.IsPreorder = 
                    String.IsNullOrEmpty(item.Name) 
                    || (String.IsNullOrEmpty(item.Surname) 
                    && item.Surname == Settings.Default.Avito);
                order.OrderInfo.CustomerComment = item.Comment;
                order.OrderInfo.ExpirationDate = item.ExpirationDate;
                order.Customer = new Customer
                {
                    FirstName = item.Name,
                    Surname = item.Surname,
                    Email = item.Email,
                    Phone = item.Phone
                };

                var result = client.UpdateOrder(order);
                if (result.ReturnCode == ReturnCode.Error)
                {
                    throw new InvalidOperationException(result.Message);
                }
            }
            
        }
    }
}