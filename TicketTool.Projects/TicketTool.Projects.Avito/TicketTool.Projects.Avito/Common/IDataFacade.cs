﻿using TicketTool.Projects.Avito.Models;
using System.Collections.Generic;

namespace TicketTool.Projects.Avito.Common
{
    public interface IDataFacade
    {
        IEnumerable<StoreItem> AvailableStores { get; }
        IEnumerable<BoardMessageItem> Adverts { get; }
        void CreateAdvert(BoardMessageItem item);
        void UpdateAdvert(BoardMessageItem item);
    }
}