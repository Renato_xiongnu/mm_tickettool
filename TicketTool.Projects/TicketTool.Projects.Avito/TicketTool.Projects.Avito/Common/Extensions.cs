﻿using System;
using System.Globalization;
using System.Linq;
using TicketTool.Projects.Avito.Models;
using TicketTool.Projects.Avito.Proxy.OrdersService;

namespace TicketTool.Projects.Avito.Common
{
    public static class Extensions
    {
        public static BoardMessageItem ToModel(this CustomerOrder order)
        {
            var item = new BoardMessageItem();
            if (order.OrderInfo.ExpirationDate.HasValue)
                item.ExpirationDate = TimeZoneInfo.ConvertTimeFromUtc(order.OrderInfo.ExpirationDate.Value, TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time"));
            item.OrderId = order.OrderId;
            item.SapCode = order.SaleLocation.SapCode;

            var wwsOrder = order.WwsOrders == null ? null : order.WwsOrders.LastOrDefault();
            if (wwsOrder != null) item.WWSOrderId = wwsOrder.Id;

            item.ExternalNumber = order.OrderInfo.ExternalOrderId;
            item.Comment = order.OrderInfo.CustomerComment;
            item.Status = order.OrderInfo.OrderState;            
            item.Created = order.OrderInfo.CreateData;
            item.Name = order.Customer.FirstName;
            item.Surname = order.Customer.Surname;
            item.Email = order.Customer.Email;
            item.Phone = order.Customer.Phone;
            item.StoreName = order.SaleLocation.Title;
            var orderLine = order.OrderLines.FirstOrDefault();
            if (orderLine != null)
            {
                item.ArticleTitle = orderLine.ArticleTitle;
                item.ArticleNum = orderLine.ArticleNo.ToString(CultureInfo.InvariantCulture);
                item.ArticlePrice = orderLine.Price;
                item.ArticleQty = orderLine.Quantity;
            }                        
            return item;
        }
    }
}