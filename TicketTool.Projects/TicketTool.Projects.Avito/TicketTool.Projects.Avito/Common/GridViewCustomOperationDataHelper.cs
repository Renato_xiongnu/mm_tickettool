﻿using System.Collections.Generic;
using System.Linq;
using DevExpress.Data;
using DevExpress.Data.Filtering;
using DevExpress.Data.Linq;
using DevExpress.Data.Linq.Helpers;
using DevExpress.Web.Mvc;

namespace TicketTool.Projects.Avito.Common
{
    public static class GridViewCustomOperationDataHelper
    {
        public static IQueryable ApplySorting1(this IQueryable query, IEnumerable<GridViewColumnState> sortedColumns)
        {
            return sortedColumns.Aggregate(query,
                                           (current, column) => query.MakeOrderBy(new CriteriaToExpressionConverter(), new ServerModeOrderDescriptor(new OperandProperty(column.FieldName), column.SortOrder == ColumnSortOrder.Descending)));

        }
    }
}