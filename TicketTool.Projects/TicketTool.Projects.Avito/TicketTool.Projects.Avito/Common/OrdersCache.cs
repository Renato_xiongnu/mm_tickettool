﻿using System;
using System.Collections.Generic;
using TicketTool.Projects.Avito.Proxy.OrdersService;

namespace TicketTool.Projects.Avito.Common
{
    internal class OrdersCache
    {
        public List<CustomerOrder> Orders { get; set; }
        public DateTime Cached { get; set; }
    }
}