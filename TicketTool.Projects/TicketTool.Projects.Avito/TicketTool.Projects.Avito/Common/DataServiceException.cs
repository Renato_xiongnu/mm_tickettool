﻿using System;

namespace TicketTool.Projects.Avito.Common
{
    /// <summary>
    /// Exception type which represents the DataServiceException thrown by the ADO.NET Data Service
    /// </summary>
    public class DataServiceException : Exception
    {
        public DataServiceException(string message, Exception internalException)
            : base(message, internalException)
        {
        }
    }
}