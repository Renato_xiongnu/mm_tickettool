﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TicketTool.Projects.Avito.Common
{
    public class DateTimeModelBinder : DefaultModelBinder
    {
        private string _customFormat;
        private CultureInfo _cultureInfo = CultureInfo.InvariantCulture;

        public DateTimeModelBinder(string customFormat)
        {
            _customFormat = customFormat;
        }
        public DateTimeModelBinder(string customFormat, CultureInfo cultureInfo)
        {
            _customFormat = customFormat;
            _cultureInfo = cultureInfo;
        }


        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (value != null && !String.IsNullOrEmpty(value.AttemptedValue))
            {
                DateTime date;
                if (DateTime.TryParseExact(value.AttemptedValue, _customFormat, _cultureInfo, DateTimeStyles.None, out date))
                {
                    return date;
                }
                else
                {
                    bindingContext.ModelState.AddModelError(
                        bindingContext.ModelName,
                        string.Format("{0} Невалидное значение даты", value.AttemptedValue)
                    );
                }
            }
            return base.BindModel(controllerContext, bindingContext);
        }
    }
}