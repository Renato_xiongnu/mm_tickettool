﻿using System;
using System.Xml.Linq;

namespace TicketTool.Projects.Avito.Common
{
    /// <summary>
    /// Helper class to de-serialize DataServiceExceptions thrown by an ADO.NET Data Service
    /// </summary>
    public static class DataServiceExceptionUtil
    {
        /// <summary>
        /// Parses the Exception object to determine if it contains a DataServiceException 
        /// and de-serializes the Exception message into a DataServiceException.
        /// </summary>
        /// <param name="dsRexception">The Exception thrown by the client library 
        /// in response to an Execute/SaveChanges call </param>
        /// <param name="dataServiceException">The DataServiceException thrown by the ADO.NET Data Service</param>
        /// <returns>true if we are able to parse the response into a DataServiceException,false if not</returns>
        public static bool TryParse(Exception dsRexception, out Exception dataServiceException)
        {
            bool parseSucceeded;
            try
            {
                Exception baseException = dsRexception.GetBaseException();
                XDocument xDoc = XDocument.Parse(baseException.Message);
                dataServiceException = ParseException(xDoc.Root, false);
                parseSucceeded = dataServiceException != null;
            }
            catch
            {
                dataServiceException = dsRexception;
                parseSucceeded = false;
            }
            return parseSucceeded;
        }

        #region Variables

        private static readonly string DataServicesMetadataNamespace = "http://schemas.microsoft.com/ado/2007/08/dataservices/metadata";
        private static readonly XName XnMessage = XName.Get("message", DataServicesMetadataNamespace);
        private static readonly XName XnInternalException = XName.Get("internalexception", DataServicesMetadataNamespace);
        private static readonly XName XnInnerError = XName.Get("innererror", DataServicesMetadataNamespace);

        #endregion

        private static DataServiceException ParseException(XElement errorElement, bool throwOnFailure)
        {
            switch (errorElement.Name.LocalName)
            {
                case "error":
                case "internalexception":
                    Exception internalEx = null;
                    if (errorElement.Element(XnInnerError) != null)
                    {
                        var e = errorElement.Element(XnInnerError).Element(XnInternalException);
                        if (e != null)
                            internalEx = ParseException(errorElement.Element(XnInnerError).Element(XnInternalException), throwOnFailure);

                    }
                    string message = errorElement.Element(XnMessage) != null ? errorElement.Element(XnMessage).Value : String.Empty;
                    return new DataServiceException(
                        message,
                        internalEx);
                default:
                    if (throwOnFailure)
                    {
                        throw new InvalidOperationException("Could not parse Exception");
                    }
                    return null;
            }
        }
    }
}