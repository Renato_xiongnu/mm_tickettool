﻿using System;
using System.Text.RegularExpressions;

namespace TicketTool.Projects.Avito.Common
{
    public static class GridViewExtensions
    {

        public static string CreateFilter(string fieldName, string expression, string value)
        {
            return string.Format("[{0}] {1} {2}", fieldName, expression, value);
        }

        public static string ClearFilter(this string source, string filter)
        {
            source = source.Replace(filter, "");
            const string regexTrim = @"(^(( )*And( )+))|((( )+And( )*)$)";
            const string regexDouble = @"(( )+And( )+And( )+)";
            source = Regex.Replace(source, regexTrim, "");
            source = Regex.Replace(source, regexDouble, " And ");
            return source;
        }

        public static string ApplyFilter(this string source, string filter)
        {
            return source + (!String.IsNullOrEmpty(source) ? " And " : "") + filter;
        }
    }

/*
    public static class Extensions
    {
        public static BoardMessageItem ToModel(this RouterDataLink.RoutableAvitoOrder orderData, TimeZoneInfo timeZone)
        {
            //var status = orderData.OrderStatus.ToEnum<OrderLink.InternalOrderStatus>();
            var item = new BoardMessageItem();
            if (orderData.ExpirationDate.HasValue)
                item.ExpirationDate = TimeZoneInfo.ConvertTimeFromUtc(orderData.ExpirationDate.Value.UtcDateTime, timeZone);
            item.OrderId = orderData.OrderId;
            item.SapCode = orderData.SapCode;
            item.WWSOrderId = orderData.WWSOrderId;
            item.ExternalNumber = orderData.ExternalNumber;
            item.Comment = orderData.Comment;
            item.Status = orderData.OrderStatus;
            item.CustomerIsDefault = orderData.CustomerIsDefault.HasValue ? orderData.CustomerIsDefault.Value : false;

            item.RoutableCreateDate = orderData.RoutableCreateDate;

            item.Name = item.CustomerIsDefault ? "" : orderData.CustomerName;
            item.Surname = item.CustomerIsDefault ? "" : orderData.CustomerSurname;
            item.Email = item.CustomerIsDefault ? "" : orderData.CustomerEmail;
            item.Phone = item.CustomerIsDefault ? "" : orderData.CustomerPhone;

            item.StoreName = orderData.StoreName;
            item.ArticleTitle = orderData.ArticleTitle;
            item.ArticleNum = orderData.ArticleNum;
            item.ArticlePrice = orderData.ArticlePrice;
            item.ArticleQty = orderData.ArticleQty;
            item.RoutableOrderId = orderData.RoutableOrderId;
            item.RoutableSapCode = orderData.RoutableSapCode;
            return item;
        }

        public static StoreItem ToModel(this OrderLink.StoreData storeData)
        {
            return new StoreItem
            {
                SapCode = storeData.SapCode,
                Name = storeData.Name
            };
        }

    }
*/
}