﻿using System.Web.Mvc;

namespace TicketTool.Projects.Avito.Common.Attributes
{
    public class ExceptionHandler : ActionFilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            filterContext.HttpContext.Response.StatusCode = 500;
            filterContext.ExceptionHandled = true;
            filterContext.Result = new JsonResult
            {
                Data = new { success = false, error = filterContext.Exception.Message.ToString() },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
    }

    //public class ExceptionHandler : FilterAttribute, IExceptionFilter
    //{
    //    public void OnException(ExceptionContext filterContext)
    //    {
    //        var response = filterContext.RequestContext.HttpContext.Response;
    //        response.Write(filterContext.Exception.Message);
    //        response.ContentType = MediaTypeNames.Text.Plain;
    //        response.StatusCode = (int)HttpStatusCode.InternalServerError;
    //        filterContext.ExceptionHandled = true;
    //    }
    //}
}