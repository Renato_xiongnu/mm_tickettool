﻿using System.Web;
using System.Web.Mvc;

namespace TicketTool.Projects.Avito.Common.Attributes
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            //AuthenticatorLink.AuthenticationServiceClient autClient = new AuthenticatorLink.AuthenticationServiceClient();
            //var isAuthorized = autClient.GetCurrentUser().IsAuthtorized;
            return true /*isAuthorized*/;
        }
    }
}