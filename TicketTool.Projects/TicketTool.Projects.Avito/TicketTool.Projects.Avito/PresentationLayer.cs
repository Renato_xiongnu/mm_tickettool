﻿using System.Reflection;
using Autofac;
using Autofac.Integration.Mvc;
using TicketTool.Projects.Avito.Common;
using TicketTool.Projects.Avito.Data;
using TicketTool.Projects.Avito.Logging;
using TicketTool.Projects.Avito.Proxy.OrdersService;
using TicketTool.Projects.Avito.Proxy.TicketToolAuth;
using Module = Autofac.Module;

namespace TicketTool.Projects.Avito
{
    public class PresentationLayer:Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Log4LoggerFactory>().As<ILoggerFactory>();
            builder.RegisterType<DataFacade>().As<IDataFacade>();
            builder.RegisterType<CustomerOrdersRepository>().SingleInstance();
            builder.RegisterType<CustomerOrdersSync>().SingleInstance();
            builder.RegisterType<AuthenticationServiceClient>();
            builder.RegisterType<OrderServiceClient>();

            builder.RegisterControllers(Assembly.GetExecutingAssembly());
        }
    }
}