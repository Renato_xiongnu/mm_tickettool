﻿using System.Activities;
using System.ComponentModel;
using System.Linq;
using Orders.Backend.Model;
using TicketTool.Projects.Avito.Activities.Design;

namespace TicketTool.Projects.Avito.Activities
{
    [Designer(typeof(CompareOrdersActivityDesigner))]
    public sealed class CompareOrders : NativeActivity<bool>
    {
        [Browsable(false)]
        [RequiredArgument]
        public InArgument<CustomerOrder> Order1 { get; set; }

        [Browsable(false)]
        [RequiredArgument]
        public InArgument<CustomerOrder> Order2 { get; set; }

        protected override void Execute(NativeActivityContext context)
        {
            var order1 = Order1.Get(context);
            var order2 = Order2.Get(context);
            if(order1 == null || order2 == null)
            {
                Result.Set(context, false);
                return;
            }

            var lines1 = order1.OrderLines == null
                ? new OrderLine[0]
                : order1.OrderLines.ToArray();
            var lines2 = order2.OrderLines == null
                ? new OrderLine[0]
                : order2.OrderLines.ToArray();
            if(lines1.Length != lines2.Length)
            {
                Result.Set(context, false);
                return;
            }

            if(lines1.Select((t, i) => Compare(t,lines2[i])).Any(c=>!c))
            {
                Result.Set(context, false);
                return;
            }

            Result.Set(context, true);
        }

        private bool Compare(OrderLine orderLine1, OrderLine orderLine2)
        {
            return orderLine1.ArticleNo == orderLine2.ArticleNo
                   && orderLine1.Quantity == orderLine2.Quantity
                   && orderLine1.Price == orderLine2.Price;
        }
        
    }
}