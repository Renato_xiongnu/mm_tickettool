﻿using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using Orders.Backend.Model;
using Orders.Backend.Notification.Model.AvitoOrder.SalesDocumentForManager;
using TicketTool.Projects.Orders.Activities.WWS;
using Address = Orders.Backend.Notification.Model.AvitoOrder.SalesDocumentForManager.Address;
using Bank = Orders.Backend.Notification.Model.AvitoOrder.SalesDocumentForManager.Bank;
using Requisite = Orders.Backend.Notification.Model.AvitoOrder.SalesDocumentForManager.Requisite;
using StoreInfo = Orders.Backend.Model.StoreInfo;
using WwsOrder = Orders.Backend.Notification.Model.AvitoOrder.SalesDocumentForManager.WwsOrder;
using WwsOrderItem = Orders.Backend.Notification.Model.AvitoOrder.SalesDocumentForManager.WwsOrderItem;
using WwsOrderTotalPrice = Orders.Backend.Notification.Model.AvitoOrder.SalesDocumentForManager.WwsOrderTotalPrice;

namespace TicketTool.Projects.Avito.Activities.NotificationExtensions
{
    public static class SalesDocumentForManagerNotification
    {
        public static object ToSalesDocumentForManagerNotificationDto(this CustomerOrder order, StoreInfo storeInfo)
        {
            var dto = new SalesDocumentForManagerNotificationDto()
            {
                OrderId = order.OrderId,
                CustomerInfo = new CustomerInfo
                {
                    FirstName = order.Customer.FirstName,
                    LastName = order.Customer.Surname
                },
                WwsOrder = ToWwsOrder(order.CurrentWwsOrder()),
                StoreInfo = ToStoreInfo(storeInfo)
            };
            return dto;
        }

        private static global::Orders.Backend.Notification.Model.AvitoOrder.SalesDocumentForManager.StoreInfo
            ToStoreInfo(StoreInfo storeInfo)
        {
            return new global::Orders.Backend.Notification.Model.AvitoOrder.SalesDocumentForManager.StoreInfo
            {
                ManagingDirector =
                    string.Join(",", storeInfo.Employees.Select(e => string.Join(" ", e.Name, e.Surname)).ToArray()),
                Name = storeInfo.Name,
                OutletId = storeInfo.OutletId.ToString(CultureInfo.InvariantCulture),
                SapCode = storeInfo.SapCode,
                Requisites = ToRequisites(storeInfo.Requisites)
            };
        }

        private static Requisite ToRequisites(global::Orders.Backend.Model.Requisite requisites)
        {
            return new Requisite
            {
                Bank = ToBank(requisites.Bank),
                ClassificationCode = requisites.ClassificationCode,
                Department = requisites.Department,
                INN = requisites.INN,
                KPP = requisites.KPP,
                LegalAddress = ToLegalAddress(requisites.LegalAddress),
                LegalName = requisites.LegalName,
                OGRN = requisites.OGRN,
                RegisterCourt = requisites.RegisterCourt
            };
        }

        private static Address ToLegalAddress(global::Orders.Backend.Model.Address addr)
        {
            return new Address
            {
                Country = addr.Country,
                Location = addr.Location,
                Phone = addr.Phone,
                Street = addr.Street,
                ZipCode = addr.ZipCode
            };
        }

        private static Bank ToBank(global::Orders.Backend.Model.Bank bank)
        {
            return new Bank
            {
                AccountNumber = bank.AccountNumber,
                BIC = bank.BIC,
                CorrespondingNumber = bank.CorrespondingNumber,
                Name = bank.Name
            };
        }

        private static WwsOrder ToWwsOrder(global::Orders.Backend.Model.WwsOrder wwsOrder)
        {
            return new WwsOrder
            {
                Id = wwsOrder.Id,
                CreationDate = wwsOrder.CreationDate.ToString("dd.MM.yyyy"),
                DocBarcode = wwsOrder.DocBarcode,
                DocBarcodeImage = wwsOrder.DocBarcodeImage,
                StoreManagerName = wwsOrder.StoreManagerName,
                LastUpdateTime =
                    wwsOrder.LastUpdateTime.HasValue
                        ? wwsOrder.LastUpdateTime.Value.ToString("dd.MM.yyyy")
                        : string.Empty,
                LeftoverTotalPrice = ToTotalPrice(wwsOrder.LeftoverTotalPrice),
                TotalPrice = ToTotalPrice(wwsOrder.TotalPrice),
                PrepaymentTotalPrice = ToTotalPrice(wwsOrder.PrepaymentTotalPrice),
                OriginalSum = PrintMoney(wwsOrder.OriginalSum),
                Items = new Collection<WwsOrderItem>(wwsOrder.Items.Select(ToWwsOrderItem).ToList())
            };
        }

        private static WwsOrderItem ToWwsOrderItem(global::Orders.Backend.Model.WwsOrderItem item)
        {
            return new WwsOrderItem
            {
                Article = item.Article.ToString(CultureInfo.InvariantCulture),
                DepartmentNumber = item.DepartmentNumber.ToString(CultureInfo.InvariantCulture),
                Description = item.Description,
                Quantity = item.Quantity.ToString(CultureInfo.InvariantCulture),
                FreeQuantity = item.FreeQuantity.ToString(CultureInfo.InvariantCulture),
                ReservedQuantity = item.ReservedQuantity.ToString(CultureInfo.InvariantCulture),
                PositionNumber = item.PositionNumber.ToString(CultureInfo.InvariantCulture),
                Price = PrintMoney(item.Price),
                PriceOrig = PrintMoney(item.PriceOrig),
                ProductGroup = item.ProductGroup.ToString(CultureInfo.InvariantCulture),
                ProductGroupName = item.ProductGroupName,
                ProductType = item.ProductType.ToString(),
                SerialNumber = item.SerialNumber,
                Title = item.Title,
                StockNumber = item.StockNumber.ToString(CultureInfo.InvariantCulture),
                StoreNumber = item.StoreNumber.ToString(CultureInfo.InvariantCulture),
                VAT = item.VAT.ToString(CultureInfo.InvariantCulture),
            };
        }

        private static WwsOrderTotalPrice ToTotalPrice(global::Orders.Backend.Model.WwsOrderTotalPrice price)
        {
            return new WwsOrderTotalPrice
            {
                GrossPrice = PrintMoney(price.GrossPrice),
                NETPrice = PrintMoney(price.NETPrice),
                VAT = PrintMoney(price.VAT),
                VATPrice = PrintMoney(price.VATPrice)
            };
        }

        private static string PrintMoney(decimal value)
        {
            return value.ToString("N0", CultureInfo.GetCultureInfo(0x0419));
        }
    }
}