using System;
using System.Linq;
using AutoMapper;
using System.Collections.Generic;
using Web.MVC.OrderProcessing.CatalogueService;
using Web.MVC.OrderProcessing.Obs;
using ReturnCode = Web.MVC.OrderProcessing.Obs.ReturnCode;

namespace Web.MVC.OrderProcessing
{
    public class OrderProcessing : IDisposable
    {
        private readonly OrderServiceClient _orderService;
        private readonly CatalogueServiceClient _catalogueService;
        private Win8Service.Win8Service _win8ServiceClient;

        public CatalogueServiceClient CatalogueService { get { return _catalogueService; } }

        public Win8Service.Win8Service Win8ServiceClient
        {
            get { return _win8ServiceClient ?? (_win8ServiceClient = new Win8Service.Win8Service()); }
        }

        public OrderProcessing()
        {
            _orderService = new OrderServiceClient();
            _catalogueService = new CatalogueServiceClient();
            MapTypes();
        }

        public CustomerOrder GetOrder(string orderId, out string message, out string sapCode)
        {
            var getOrderOperationResult = _orderService.GetOrder(orderId);
            message = getOrderOperationResult.Message;
            sapCode = null;
            return getOrderOperationResult.ReturnCode == ReturnCode.Ok ? Transform(getOrderOperationResult.CustomerOrder, out sapCode) : null;
        }

        public bool Update(CustomerOrder order, string sapCode, out string message)
        {
            var result = _orderService.UpdateOrder(Transform(order, sapCode));
            message = result.Message;
            return result.ReturnCode == ReturnCode.Ok;
        }

        public bool Create(CustomerOrder order, string sapCode, out string message)
        {
            var result = _orderService.CreateOrder(Transform(order, sapCode));
            message = result.Message;
            order.OrderId = result.OrderId;
            return result.ReturnCode == ReturnCode.Ok;
        }

        public static void MapTypes()
        {
            #region Internal services

            Mapper.CreateMap<CustomerOrder, Obs.CustomerOrder>();
            Mapper.CreateMap<Obs.CustomerOrder, CustomerOrder>();

            Mapper.CreateMap<DeliveryInfo, Obs.DeliveryInfo>();
            Mapper.CreateMap<Obs.DeliveryInfo, DeliveryInfo>();

            Mapper.CreateMap<OrderLine, Obs.OrderLine>();
            Mapper.CreateMap<Obs.OrderLine, OrderLine>();

            Mapper.CreateMap<Customer, Obs.Customer>()
                .ForMember(dst => dst.Mobile, e => e.MapFrom(src => src.Phone))
                .ForMember(dst => dst.Phone, e => e.MapFrom(src => src.OptionalPhones));
            Mapper.CreateMap<Obs.Customer, Customer>()
                .ForMember(dst => dst.Phone, e => e.MapFrom(src => src.Mobile))
                .ForMember(dst => dst.OptionalPhones, e => e.MapFrom(src => src.Phone));

            #endregion

            #region External services

            Mapper.CreateMap<OrderLine, BasketArticle>().ForMember(d => d.ArticleNum, o => o.MapFrom(s => s.ArticleNo));
            Mapper.CreateMap<BasketArticle, OrderLine>().ForMember(d => d.ArticleNo, o => o.MapFrom(s => s.ArticleNum));

            #endregion
        }

        public CustomerOrder Transform(Obs.CustomerOrder order, out string sapCode)
        {
            var customerOrder = Mapper.Map<CustomerOrder>(order);
            sapCode = order.SaleLocation != null ? order.SaleLocation.SapCode : null;
            Mapper.Map(order.Delivery, customerOrder.Delivery);
            Mapper.Map(order.OrderLines, customerOrder.OrderLines);
            Mapper.Map(order.Customer, customerOrder.Customer);
            var deliveryTypeNameResult = CatalogueService.GetDeliveryTypeDictionary();

            if (deliveryTypeNameResult.ReturnCode == MVC.CatalogueService.ReturnCode.Ok)
            {
                var pair = deliveryTypeNameResult.Dictionary.FirstOrDefault(kvp => kvp.Value == customerOrder.Delivery.DeliveryType);
                if (!pair.Equals(default(KeyValuePair<string, string>)))
                    customerOrder.Delivery.DeliveryType = pair.Key;
            }

            return customerOrder;
        }

        public Obs.CustomerOrder Transform(CustomerOrder order, string sapCode)
        {
            var customerOrder = new Obs.CustomerOrder();

            if (!string.IsNullOrEmpty(order.OrderId))
            {
                var storedOrderResponce = _orderService.GetOrder(order.OrderId);
                if (storedOrderResponce.ReturnCode == ReturnCode.Ok && storedOrderResponce.CustomerOrder != null)
                    Mapper.DynamicMap(storedOrderResponce.CustomerOrder, customerOrder);
            }

            customerOrder = Mapper.Map(order, customerOrder);
            customerOrder.OrderInfo = new OrderInfo();
            if (!string.IsNullOrEmpty(sapCode))
                customerOrder.SaleLocation = new SaleLocationInfo
                    {
                        SaleLocationId = "shop_" + sapCode,
                    };
            Mapper.Map(order.Delivery, customerOrder.Delivery);
            Mapper.Map(order.OrderLines, customerOrder.OrderLines);
            Mapper.Map(order.Customer, customerOrder.Customer);

            var deliveryTypeResult = CatalogueService.IsSelfCarryDeliveryTypeResult(order.Delivery.DeliveryType);
            var deliveryTypeNameResult = CatalogueService.GetDeliveryTypeDictionary();
            var isSelfCary = deliveryTypeResult.ReturnCode == MVC.CatalogueService.ReturnCode.Ok && deliveryTypeResult.IsSelfCarry;
            if (isSelfCary && order.Delivery.PickupPointId.HasValue)
            {
                var pointDictionary = _catalogueService.GetPickupPointDictionary();
                if (pointDictionary.ReturnCode == MVC.CatalogueService.ReturnCode.Ok && pointDictionary.Dictionary != null && pointDictionary.Dictionary.Any())
                {
                    var pickupPoint = pointDictionary.Dictionary.FirstOrDefault(p => p.Key == order.Delivery.PickupPointId.Value.ToString(System.Globalization.CultureInfo.InvariantCulture));
                    if(!pickupPoint.Equals(default(KeyValuePair<string, string>)))
                        customerOrder.Delivery.PickupPointName = pickupPoint.Value;
                }
            }

            if (deliveryTypeNameResult.ReturnCode == MVC.CatalogueService.ReturnCode.Ok)
            {
                var pair = deliveryTypeNameResult.Dictionary.FirstOrDefault(kvp => kvp.Key == customerOrder.Delivery.DeliveryType);
                if (!pair.Equals(default(KeyValuePair<string, string>)))
                    customerOrder.Delivery.DeliveryType = pair.Value;
            }

            if (order.Delivery.SubwayStationId.HasValue)
            {
                var subwayDictionary = _catalogueService.GetMetroStationDictionary();
                if (subwayDictionary.ReturnCode == MVC.CatalogueService.ReturnCode.Ok && subwayDictionary.Dictionary != null && subwayDictionary.Dictionary.Any())
                {
                    var subwayStation = subwayDictionary.Dictionary.FirstOrDefault(p => p.Key == order.Delivery.SubwayStationId.Value.ToString(System.Globalization.CultureInfo.InvariantCulture));
                    if(!subwayStation.Equals(default(KeyValuePair<string, string>)))
                        customerOrder.Delivery.SubwayStationName = subwayStation.Value;
                }
            }

            if (!string.IsNullOrWhiteSpace(order.SystemCommentNew))
                customerOrder.OrderInfo.SystemComment += "\r\n" + order.SystemCommentNew;

            return customerOrder;
        }

        public void Dispose()
        {
            if (_orderService != null)
                try
                {
                    _orderService.Close();
                }
                catch
                {
#if DEBUG
                    throw;
#endif
                }
        }

        public bool UpdateBasket(List<OrderLine> orderLines, string orderId, out string message)
        {
            var orderResponce = _orderService.GetOrder(orderId);
            message = orderResponce.Message;

            if (orderResponce.ReturnCode == ReturnCode.Error)
                return false;

            if (orderResponce.CustomerOrder == null)
            {
                message = string.Format("Order (ID:{0}) not found!", orderId);
                return false;
            }

            orderResponce.CustomerOrder.OrderLines = Mapper.Map<List<Obs.OrderLine>>(orderLines).ToArray();
            var updatedOrderResponce = _orderService.UpdateOrder(orderResponce.CustomerOrder);
            message = updatedOrderResponce.Message;

            return updatedOrderResponce.ReturnCode != ReturnCode.Error;
        }
    }
}