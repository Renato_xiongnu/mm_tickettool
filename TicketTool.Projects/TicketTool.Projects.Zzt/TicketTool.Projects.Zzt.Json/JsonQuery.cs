﻿using System;
using System.Globalization;
using System.Linq;
using Newtonsoft.Json;

namespace TicketTool.Projects.Zzt.Json
{
    public sealed class JsonQuery
    {
        private readonly Request _request;

        public JsonQuery(QueryType type, Content content)
        {
            this._request = new Request {type = type.ToString().ToLower(), content = content};
        }

        public static string GetJson(params JsonQuery[] queries)
        {
            var i = 0;
            foreach (var query in queries)
            {
                i++;
                query._request.num = i.ToString(CultureInfo.InvariantCulture);
            }
            return JsonConvert.SerializeObject(new {requests = queries.Select(q => q._request).ToList()}, new JsonSerializerSettings {NullValueHandling = NullValueHandling.Ignore});
        }

        internal static T ResponseToObject<T>(string response) where T : IJsonDeserializable, new()
        {
            try
            {
                var result = JsonConvert.DeserializeObject<RootObject<T>>(response);
                var reaction = result.reactions.FirstOrDefault();
                return reaction != null ? reaction.content.FirstOrDefault() : default(T);
            }
            catch (Exception ex)
            {
                IJsonDeserializable result = new T();
                try
                {
                    var exception = JsonConvert.DeserializeObject<RootObject<string>>(response);
                    var reaction = exception.reactions.FirstOrDefault();
                    if (reaction != null) result.DeserializationErrors = reaction.content.FirstOrDefault();
                }
                catch
                {
                    result.DeserializationErrors = ex.Message;
                }
                return (T) result;
            }
        }
    }
}