﻿namespace TicketTool.Projects.Zzt.Json
{
    public class SendContent : Content
    {
// ReSharper disable InconsistentNaming
        public EntitySet content { get; set; }
// ReSharper restore InconsistentNaming
    }
}