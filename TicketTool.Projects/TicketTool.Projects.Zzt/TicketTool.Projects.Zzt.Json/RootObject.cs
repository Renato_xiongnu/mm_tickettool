﻿using System.Collections.Generic;

namespace TicketTool.Projects.Zzt.Json
{
    public class RootObject<T>
    {
// ReSharper disable InconsistentNaming
        public List<Reaction<T>> reactions { get; set; }
// ReSharper restore InconsistentNaming
    }
}