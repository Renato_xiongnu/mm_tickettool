﻿namespace TicketTool.Projects.Zzt.Json
{
    public interface IJsonDeserializable
    {
        string DeserializationErrors { get; set; }
    }
}