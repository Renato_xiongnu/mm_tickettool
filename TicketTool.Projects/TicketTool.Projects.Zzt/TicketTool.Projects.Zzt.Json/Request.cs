﻿namespace TicketTool.Projects.Zzt.Json
{
    public class Request
    {
        public string num { get; set; }
        public string type { get; set; }
        public Content content { get; set; }
    }
}