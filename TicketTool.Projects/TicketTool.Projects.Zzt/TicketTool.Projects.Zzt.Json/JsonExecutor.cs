﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using TicketTool.Projects.Zzt.Json.Properties;

namespace TicketTool.Projects.Zzt.Json
{
    public sealed class JsonExecutor
    {
        private readonly string _queryUrl;
        private readonly string _certificatePath;
        private readonly List<JsonQuery> _queries;

        public JsonExecutor(string queryUrl, string certificatePath)
        {
            this._queryUrl = queryUrl;
            this._certificatePath = certificatePath;
            this._queries = new List<JsonQuery>();
        }

        public void AddQuery(JsonQuery query)
        {
            this._queries.Add(query);
        }

        public TResponse ExecuteQuery<TResponse>() where TResponse : IJsonDeserializable, new()
        {
            return JsonQuery.ResponseToObject<TResponse>(this.Execute());
        }

        private string Execute()
        {
            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, errors) => true;

            var request = (HttpWebRequest)WebRequest.Create(new Uri(this._queryUrl));
            request.Timeout = 1*1*Settings.Default.PostTimeout*1000;
            request.Credentials = new NetworkCredential("", "");
            request.PreAuthenticate = true;
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            var cert = X509Certificate.CreateFromCertFile(this._certificatePath);
            request.ClientCertificates.Add(cert);

            var byteArray = Encoding.UTF8.GetBytes(String.Format("request={0}", JsonQuery.GetJson(this._queries.ToArray())));            
            request.ContentLength = byteArray.Length;
            var dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            var stream = request.GetResponse().GetResponseStream();
            return stream != null ? new StreamReader(stream, new UTF8Encoding()).ReadToEnd() : null;
        }
    }
}