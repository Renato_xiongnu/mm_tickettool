﻿namespace TicketTool.Projects.Zzt.Json
{
    public class FilteredContent : Content
    {
// ReSharper disable InconsistentNaming
        public string filter { get; set; }
// ReSharper restore InconsistentNaming
    }
}