﻿namespace TicketTool.Projects.Zzt.Json
{
    public enum QueryType
    {
        Select,
        Send,
        Report
    }
}