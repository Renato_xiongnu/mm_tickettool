﻿using System.Collections.Generic;

namespace TicketTool.Projects.Zzt.Json
{
    public class Reaction<T> 
    {
// ReSharper disable InconsistentNaming
        public string num { get; set; }
        public List<T> content { get; set; }
        // ReSharper restore InconsistentNaming
    }
}