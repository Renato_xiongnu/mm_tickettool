﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Web.MVC.BusinessLogic.Test
{
    [TestClass]
    public class DataServiceTest
    {
        private OBS.OrderServiceClient _service;
        protected OBS.OrderServiceClient Client { get { return _service ?? (_service = new OBS.OrderServiceClient()); } }
        private OrderProcessing _processing;
        protected OrderProcessing Processing { get { return _processing ?? (_processing = new OrderProcessing()); } }

        [TestMethod]
        public void ConvertOrder()
        {
            string message, sapCode;
            var request = Processing.GetOrder("1234567", out message, out sapCode);

            Assert.IsNotNull(request, message);

            var transformedServiceOrder = Processing.Transform(Processing.Transform(request, sapCode), out sapCode);
            Assert.IsTrue(request.Equals(transformedServiceOrder));
        }
    }
}
