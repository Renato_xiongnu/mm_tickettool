﻿using System;
using System.Globalization;
using System.Linq;
using System.Collections.Generic;
using log4net;
using MediaMarkt.AssertExeptionExtention;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Orders.Integration.Dadata;
using Ploeh.AutoFixture;
using Zzt.Web.MVC.BusinessLogic;
using Zzt.Web.MVC.CatalogueService;
using Zzt.Web.MVC.Obs;
using Zzt.Web.MVC.TTAuth;
using CustomerOrder = Zzt.Web.MVC.Models.CustomerOrder;
using OrderLine = Zzt.Web.MVC.Obs.OrderLine;
using OrderStatusChange = Zzt.Web.MVC.Obs.OrderStatusChange;
using PaymentType = Zzt.Web.MVC.Obs.PaymentType;
using ReturnCode = Zzt.Web.MVC.CatalogueService.ReturnCode;

namespace Web.MVC.BusinessLogic
{
    /// <summary>
    /// Summary description for CustomerOrderConverterTest
    /// </summary>
    [TestClass]
    public class CustomerOrderConverterTest
    {
        [TestMethod]
        public void ToInternalCustomerOrderWrongArgumentsTest()
        {
            string sapCode;
            var catalogueService = new Mock<ICatalogueService>();
            catalogueService.Setup(x => x.GetDeliveryTypeDictionary()).Returns(() => new GetDictionaryOperationResult
            {
                ReturnCode = ReturnCode.Ok,
                Dictionary = new[]
                        {
                            new KeyValuePair<string, string>("11", "Самовывоз"),
                            new KeyValuePair<string, string>("12", "Барнаул, Улица Второго Мая, 12"),
                            new KeyValuePair<string, string>("13", "DeliveryType")
                        },
            });
            catalogueService.Setup(x => x.GetPickupPointDictionary()).Returns(() => new GetDictionaryOperationResult
            {
                ReturnCode = ReturnCode.Ok,
                Dictionary = new[]
                        {
                            new KeyValuePair<string, string>("11", "Абакан, Улица Первого Мая, 11"),
                            new KeyValuePair<string, string>("12", "Барнаул, Улица Второго Мая, 12"),
                            new KeyValuePair<string, string>("13", "PickupPointName")
                        },
            });
            catalogueService.Setup(x => x.GetCityDictionary()).Returns(() => new GetDictionaryOperationResult
            {
                ReturnCode = ReturnCode.Ok,
                Dictionary = new[]
                        {
                            new KeyValuePair<string, string>("11", "Абакан"),
                            new KeyValuePair<string, string>("12", "Барнаул"),
                            new KeyValuePair<string, string>("13", "City")
                        },
            });
            catalogueService.Setup(x => x.GetMetroStationDictionary()).Returns(() => new GetDictionaryOperationResult
            {
                ReturnCode = ReturnCode.Ok,
                Dictionary = new[]
                        {
                            new KeyValuePair<string, string>("11", "SADklsajdlk;as"),
                            new KeyValuePair<string, string>("12", "sal;,dksla;dkla"),
                            new KeyValuePair<string, string>("13", "SubwayStationName")
                        },
            });
            var customerOrder = new Zzt.Web.MVC.Obs.CustomerOrder
            {
                Customer = new Customer
                {
                    Account = "Account",
                    AgreedToProcessingPD = true,
                    BIC = "BIC",
                    BankName = "BankName",
                    Birthday = new DateTime(1980, 1, 1),
                    CorrespondingAccount = "CorrespondingAccount",
                    CustomerType = "CustomerType",
                    Email = "Email",
                    Fax = "Fax",
                    FirstName = "FirstName",
                    Gender = 'G',
                    HasSubscription = true,
                    INN = "INN",
                    KPP = "KPP",
                    LastName = "LastName",
                    Login = "Login",
                    Mobile = "Mobile",
                    OKONH = "OKONH",
                    OKPO = "OKPO",
                    Phone = "Phone",
                    ProcessingPDAgreementDate = new DateTime(2001, 1, 1),
                    RegistrationDate = new DateTime(2002, 1, 1),
                    Surname = "Surname",
                },
                Delivery = new DeliveryInfo
                {
                    ActualDeliveryFrom = new DateTime(2003, 1, 1),
                    ActualDeliveryTo = new DateTime(2004, 1, 1),
                    Address = "Address",
                    AddressComment = "AddressComment",
                    Building = "Building",
                    City = "City",
                    DeliveryCompany = "DeliveryCompany",
                    DeliveryPrice = 100,
                    DeliveryType = "DeliveryType",
                    Entrance = "Entrance",
                    EntranceCode = "EntranceCode",
                    ExpectedDeliveryFrom = new DateTime(2005, 1, 1),
                    ExpectedDeliveryTo = new DateTime(2006, 1, 1),
                    Flat = "Flat",
                    Floor = "Floor",
                    FloorTax = 200,
                    HasServiceLift = true,
                    House = "House",
                    Housing = "Housing",
                    Latitude = 1.00,
                    Longitude = 2.00,
                    PickupPointId = 9,
                    PickupPointName = "PickupPointName",
                    ProposedDeliveryFrom = new DateTime(2007, 1, 1),
                    ProposedDeliveryTo = new DateTime(2008, 1, 1),
                    Street = "Street",
                    SubwayStationId = 10,
                    SubwayStationName = "SubwayStationName",
                },
                OrderId = "OrderId",
                OrderInfo = new OrderInfo
                {
                    AdditionalAttribures = new Dictionary<string, string>
                                {
                                    {"1", "2"}, {"3", "4"}
                                },
                    CreateData = new DateTime(2009, 1, 1),
                    CreatedBy = "CreatedBy",
                    CustomerComment = "CustomerComment",
                    CustomerTypeExtended = "CustomerTypeExtended",
                    DiscountCardNo = "DiscountCardNo",
                    DiscountTypeCard = "DiscountTypeCard",
                    ExpirationDate = new DateTime(2010, 1, 1),
                    ExternalOrderId = "ExternalOrderId",
                    FinalPaymentType = "FinalPaymentType",
                    InTicketTool = true,
                    IsOnlineOrder = true,
                    IsOnlinePayment = true,
                    IsPreorder = true,
                    LastChangingSystem = "LastChangingSystem",
                    MinimalPriceForDiscount = 50000000,
                    OrderDiscount = 90000000,
                    OrderSource = "OrderSource",
                    OrderSourceSystem = "OrderSourceSystem",
                    OrderState = "OrderState",
                    OrderStatusHistory = new[]
                                {
                                    new OrderStatusChange
                                        {
                                            ChangedType = "ChangedType", FromStatus = "FromStatus", ReasonText = "ReasonText", ToStatus = "ToStatus", UpdateTime = new DateTime(2011, 1, 1), WhoChanged = "WhoChanged",
                                        }
                                },
                    PaymentType = new PaymentType
                    {
                        Code = "Code",
                        Id = 1,
                        Name = "Name",
                        NameRu = "NameRu",
                    },
                    PickupType = "PickupType",
                    ShouldBeProcessedInTT = true,
                    SystemComment = "SystemComment",
                    UpdateDate = new DateTime(2012, 1, 1),
                    UpdatedBy = "UpdatedBy",
                    UserQuality = 1,
                    UserQualityComment = "UserQualityComment",
                    UtmSourse = "UtmSourse",
                    WWSOrderId = "WWSOrderId",
                    WorkflowType = "WorkflowType",
                },
                OrderLines = new[]
                        {
                            new OrderLine
                                {
                                    ArticleNo = 2, ArticleTitle = "ArticleTitle", ArticleType = ArticleType.NonPhysycalGood, Disabled = true, DiscountPercentage = 3, IsSet = true, ItemState = "ItemState", LineType = OrderLineType.Article, LongTail = true, Price = 10000000, Quantity = 20, SerialNumber = "SerialNumber", SubOrderLines = new[]
                                        {
                                            new OrderLine
                                                {
                                                    ArticleNo = 3, ArticleTitle = "SubOrderLinesArticleTitle", ArticleType = ArticleType.PhysicalGood, Disabled = true, DiscountPercentage = 4, IsSet = true, ItemState = "SubOrderLinesItemState", LineType = OrderLineType.Set, LongTail = true, Price = 50000, Quantity = 1, SerialNumber = "SubOrderLinesSerialNumber",
                                                }
                                        },
                                    UmaxStatus = "UmaxStatus", WarrantedOrderLine = new OrderLine
                                        {
                                            ArticleNo = 4, ArticleTitle = "WarrantedOrderLineArticleTitle", ArticleType = ArticleType.PhysicalGood, Disabled = true, DiscountPercentage = 5, IsSet = true, ItemState = "WarrantedOrderLineItemState", LineType = OrderLineType.Set, LongTail = true, Price = 1000, Quantity = 2, SerialNumber = "WarrantedOrderLineSerialNumber",
                                        }
                                }
                        },
                SaleLocation = new SaleLocationInfo
                {
                    ChannelId = "ChannelId",
                    SaleLocationId = "SaleLocationId",
                    SapCode = "SapCode",
                    Title = "Title",
                },
            };
            AssertException.DosNotThrow(() => CustomerOrderConverter.Instance.ToInternalCustomerOrder(customerOrder, out sapCode, catalogueService.Object));

            sapCode = null;

            AssertException.Throws<ArgumentNullException>(() => CustomerOrderConverter.Instance.ToInternalCustomerOrder(customerOrder, out sapCode, null));
            AssertException.Throws<ArgumentNullException>(() => CustomerOrderConverter.Instance.ToInternalCustomerOrder(null, out sapCode, null));
            AssertException.Throws<ArgumentNullException>(() => CustomerOrderConverter.Instance.ToInternalCustomerOrder(null, out sapCode, catalogueService.Object));
        }

        [TestMethod]
        public void ToInternalCustomerOrderTest()
        {
            var catalogueService = new Mock<ICatalogueService>();
            catalogueService.Setup(x => x.GetDeliveryTypeDictionary()).Returns(() => new GetDictionaryOperationResult
            {
                ReturnCode = ReturnCode.Ok,
                Dictionary = new[]
                        {
                            new KeyValuePair<string, string>("11", "Самовывоз"),
                            new KeyValuePair<string, string>("12", "Барнаул, Улица Второго Мая, 12"),
                            new KeyValuePair<string, string>("13", "DeliveryType")
                        },
            });
            catalogueService.Setup(x => x.GetPickupPointDictionary()).Returns(() => new GetDictionaryOperationResult
            {
                ReturnCode = ReturnCode.Ok,
                Dictionary = new[]
                        {
                            new KeyValuePair<string, string>("11", "Абакан, Улица Первого Мая, 11"),
                            new KeyValuePair<string, string>("12", "Барнаул, Улица Второго Мая, 12"),
                            new KeyValuePair<string, string>("13", "PickupPointName")
                        },
            });
            catalogueService.Setup(x => x.GetCityDictionary()).Returns(() => new GetDictionaryOperationResult
            {
                ReturnCode = ReturnCode.Ok,
                Dictionary = new[]
                        {
                            new KeyValuePair<string, string>("11", "Абакан"),
                            new KeyValuePair<string, string>("12", "Барнаул"),
                            new KeyValuePair<string, string>("13", "City")
                        },
            });
            catalogueService.Setup(x => x.GetMetroStationDictionary()).Returns(() => new GetDictionaryOperationResult
            {
                ReturnCode = ReturnCode.Ok,
                Dictionary = new[]
                        {
                            new KeyValuePair<string, string>("11", "Абакан"),
                            new KeyValuePair<string, string>("12", "Барнаул"),
                            new KeyValuePair<string, string>("10", "SubwayStationName")
                        },
            });
            var random = new Random();
            Func<DateTime> randomDate = () => DateTime.Now.AddTicks(random.Next(-1000000, 1000000));

            var externalCustomerOrder = new Zzt.Web.MVC.Obs.CustomerOrder
            {
                Customer = new Customer
                {
                    Account = "Account",
                    AgreedToProcessingPD = true,
                    BIC = "BIC",
                    BankName = "BankName",
                    Birthday = randomDate(),
                    CorrespondingAccount = "CorrespondingAccount",
                    CustomerType = "CustomerType",
                    Email = "Email",
                    Fax = "Fax",
                    RecepientName = "RecepientName",
                    FirstName = "FirstName",
                    Gender = 'G',
                    HasSubscription = true,
                    INN = "INN",
                    KPP = "KPP",
                    LastName = "LastName",
                    Login = "Login",
                    Mobile = "Mobile",
                    OKONH = "OKONH",
                    OKPO = "OKPO",
                    Phone = "Phone",
                    ProcessingPDAgreementDate = randomDate(),
                    RegistrationDate = randomDate(),
                    Surname = "Surname",
                },
                Delivery = new DeliveryInfo
                {
                    ActualDeliveryFrom = randomDate(),
                    ActualDeliveryTo = randomDate(),
                    Address = "Address",
                    AddressComment = "AddressComment",
                    Building = "Building",
                    City = "City",
                    DeliveryCompany = "DeliveryCompany",
                    DeliveryPrice = 100,
                    DeliveryType = "DeliveryType",
                    Entrance = "Entrance",
                    EntranceCode = "EntranceCode",
                    ExpectedDeliveryFrom = randomDate(),
                    ExpectedDeliveryTo = randomDate(),
                    Flat = "Flat",
                    Floor = "Floor",
                    FloorTax = 200,
                    HasServiceLift = true,
                    House = "House",
                    Housing = "Housing",
                    Latitude = 1.00,
                    Longitude = 2.00,
                    PickupPointId = 9,
                    PickupPointName = "PickupPointName",
                    ProposedDeliveryFrom = randomDate(),
                    ProposedDeliveryTo = randomDate(),
                    Street = "Street",
                    SubwayStationId = 10,
                    SubwayStationName = "SubwayStationName",
                },
                OrderId = "OrderId",
                OrderInfo = new OrderInfo
                {
                    AdditionalAttribures = new Dictionary<string, string>
                                {
                                    {"1", "2"},
                                    {"3", "4"}
                                },
                    CreateData = randomDate(),
                    CreatedBy = "CreatedBy",
                    CustomerComment = "CustomerComment",
                    CustomerTypeExtended = "CustomerTypeExtended",
                    DiscountCardNo = "DiscountCardNo",
                    DiscountTypeCard = "DiscountTypeCard",
                    ExpirationDate = new DateTime(2010, 1, 1),
                    ExternalOrderId = "ExternalOrderId",
                    FinalPaymentType = "FinalPaymentType",
                    InTicketTool = true,
                    IsOnlineOrder = true,
                    IsOnlinePayment = true,
                    IsPreorder = true,
                    LastChangingSystem = "LastChangingSystem",
                    MinimalPriceForDiscount = 50000000,
                    OrderDiscount = 90000000,
                    OrderSource = "OrderSource",
                    OrderSourceSystem = "OrderSourceSystem",
                    OrderState = "OrderState",
                    OrderStatusHistory = new[]
                                {
                                    new OrderStatusChange
                                        {
                                            ChangedType = "ChangedType",
                                            FromStatus = "FromStatus",
                                            ReasonText = "ReasonText",
                                            ToStatus = "ToStatus",
                                            UpdateTime = randomDate(),
                                            WhoChanged = "WhoChanged",
                                        }
                                },
                    PaymentType = new PaymentType
                    {
                        Code = "Code",
                        Id = 1,
                        Name = "Name",
                        NameRu = "NameRu",
                    },
                    PickupType = "PickupType",
                    ShouldBeProcessedInTT = true,
                    SystemComment = "SystemComment",
                    UpdateDate = randomDate(),
                    UpdatedBy = "UpdatedBy",
                    UserQuality = 1,
                    UserQualityComment = "UserQualityComment",
                    UtmSourse = "UtmSourse",
                    WWSOrderId = "WWSOrderId",
                    WorkflowType = "WorkflowType",
                },
                OrderLines = new[]
                        {
                            new OrderLine
                                {
                                    ArticleNo = 2,
                                    ArticleTitle = "ArticleTitle",
                                    ArticleType = ArticleType.NonPhysycalGood,
                                    Disabled = true,
                                    DiscountPercentage = 3,
                                    IsSet = true,
                                    ItemState = "ItemState",
                                    LineType = OrderLineType.Article,
                                    LongTail = true,
                                    Price = 10000000,
                                    Quantity = 20,
                                    SerialNumber = "SerialNumber",
                                    SubOrderLines = new []
                                        {
                                            new OrderLine
                                                {
                                                    ArticleNo = 3,
                                                    ArticleTitle = "SubOrderLinesArticleTitle",
                                                    ArticleType = ArticleType.PhysicalGood,
                                                    Disabled = true,
                                                    DiscountPercentage = 4,
                                                    IsSet = true,
                                                    ItemState = "SubOrderLinesItemState",
                                                    LineType = OrderLineType.Set,
                                                    LongTail = true,
                                                    Price = 50000,
                                                    Quantity = 1,
                                                    SerialNumber = "SubOrderLinesSerialNumber",
                                                }
                                        },
                                    UmaxStatus = "UmaxStatus",
                                    WarrantedOrderLine = new OrderLine
                                        {
                                            ArticleNo = 4,
                                            ArticleTitle = "WarrantedOrderLineArticleTitle",
                                            ArticleType = ArticleType.PhysicalGood,
                                            Disabled = true,
                                            DiscountPercentage = 5,
                                            IsSet = true,
                                            ItemState = "WarrantedOrderLineItemState",
                                            LineType = OrderLineType.Set,
                                            LongTail = true,
                                            Price = 1000,
                                            Quantity = 2,
                                            SerialNumber = "WarrantedOrderLineSerialNumber",
                                        }
                                }
                        },
                SaleLocation = new SaleLocationInfo
                {
                    ChannelId = "ChannelId",
                    SaleLocationId = "SaleLocationId",
                    SapCode = "SapCode",
                    Title = "Title",
                },
            };

            string sapCode;
            var expectedResult = new CustomerOrder
            {
                Email = externalCustomerOrder.Customer.Email,
                FirstName = externalCustomerOrder.Customer.FirstName,
                FullName = externalCustomerOrder.Customer.LastName + ' ' + externalCustomerOrder.Customer.FirstName,
                HasSecondPhone = !string.IsNullOrEmpty(externalCustomerOrder.Customer.Mobile),
                OptionalPhones = externalCustomerOrder.Customer.Mobile,
                Phone = externalCustomerOrder.Customer.Phone,
                LastName = externalCustomerOrder.Customer.LastName,
                CustomerComment = externalCustomerOrder.OrderInfo.CustomerComment,
                AddressComment = externalCustomerOrder.Delivery.AddressComment,
                Building = externalCustomerOrder.Delivery.Building,
                City = externalCustomerOrder.Delivery.City,
                RecepientName = externalCustomerOrder.Customer.RecepientName,
                DeliveryPrice = externalCustomerOrder.Delivery.DeliveryPrice,
                DeliveryType = "13",
                Entrance = externalCustomerOrder.Delivery.Entrance,
                EntranceCode = externalCustomerOrder.Delivery.EntranceCode,
                ExpectedDeliveryFrom = externalCustomerOrder.Delivery.ExpectedDeliveryFrom,
                ExpectedDeliveryTo = externalCustomerOrder.Delivery.ExpectedDeliveryTo,
                Flat = externalCustomerOrder.Delivery.Flat,
                Floor = externalCustomerOrder.Delivery.Floor,
                FloorTax = externalCustomerOrder.Delivery.FloorTax,
                HasServiceLift = externalCustomerOrder.Delivery.HasServiceLift ?? false,
                House = externalCustomerOrder.Delivery.House,
                Housing = externalCustomerOrder.Delivery.Housing,
                PickupPointId = externalCustomerOrder.Delivery.PickupPointId,
                PickupPointName = externalCustomerOrder.Delivery.PickupPointName,
                ProposedDeliveryFrom = externalCustomerOrder.Delivery.ProposedDeliveryFrom,
                ProposedDeliveryTo = externalCustomerOrder.Delivery.ProposedDeliveryTo,
                Street = externalCustomerOrder.Delivery.Street,
                SubwayStationId = externalCustomerOrder.Delivery.SubwayStationId,
                SubwayStationName = externalCustomerOrder.Delivery.SubwayStationName,
                DiscountCardNo = externalCustomerOrder.OrderInfo.DiscountCardNo,
                OrderDiscount = externalCustomerOrder.OrderInfo.OrderDiscount,
                OrderId = externalCustomerOrder.OrderId,
                OrderSource =  externalCustomerOrder.OrderInfo.OrderSourceSystem,
                OrderLines = new List<Zzt.Web.MVC.Models.OrderLine>
                        {
                            new Zzt.Web.MVC.Models.OrderLine
                                {
                                    ArticleNo = 2,
                                    ArticleTitle = "ArticleTitle",
                                    DiscountPercentage = 3,
                                    LongTail = true,
                                    Price = 10000000,
                                    Quantity = 20,
                                    UmaxStatus = "UmaxStatus",
                                }
                        },
                OrderStatusHistory = new List<Zzt.Web.MVC.Models.OrderStatusChange>
                        {
                            new Zzt.Web.MVC.Models.OrderStatusChange
                                {
                                    ChangedType = "ChangedType",
                                    FromStatus = "FromStatus",
                                    ReasonText = "ReasonText",
                                    ToStatus = "ToStatus",
                                    UpdateTime = randomDate(),
                                    WhoChanged = "WhoChanged",
                                },
                        },
                SystemComment = externalCustomerOrder.OrderInfo.SystemComment,
            };

            var actualResult = CustomerOrderConverter.Instance.ToInternalCustomerOrder(externalCustomerOrder, out sapCode, catalogueService.Object);

            Assert.AreEqual(expectedResult.OrderId, actualResult.OrderId);
            Assert.AreEqual(expectedResult.OrderSource, actualResult.OrderSource);
            Assert.AreEqual(expectedResult.CustomerComment, actualResult.CustomerComment);
            Assert.AreEqual(expectedResult.SystemComment, actualResult.SystemComment);
            Assert.AreEqual(expectedResult.DiscountCardNo, actualResult.DiscountCardNo);
            Assert.AreEqual(expectedResult.OrderDiscount, actualResult.OrderDiscount);
            Assert.AreEqual(expectedResult.LastName, actualResult.LastName);
            Assert.AreEqual(expectedResult.FirstName, actualResult.FirstName);
            Assert.AreEqual(expectedResult.FullName, actualResult.FullName);
            Assert.AreEqual(expectedResult.RecepientName, actualResult.RecepientName);
            Assert.AreEqual(expectedResult.Phone, actualResult.Phone);
            Assert.AreEqual(expectedResult.OptionalPhones, actualResult.OptionalPhones);
            Assert.AreEqual(expectedResult.Email, actualResult.Email);
            Assert.AreEqual(expectedResult.HasSecondPhone, actualResult.HasSecondPhone);
            Assert.AreEqual(expectedResult.AddressComment, actualResult.AddressComment);
            Assert.AreEqual(expectedResult.Building, actualResult.Building);
            Assert.AreEqual(expectedResult.City, actualResult.City);
            Assert.AreEqual(expectedResult.DeliveryPrice, actualResult.DeliveryPrice);
            Assert.AreEqual(expectedResult.DeliveryType, actualResult.DeliveryType);
            Assert.AreEqual(expectedResult.Entrance, actualResult.Entrance);
            Assert.AreEqual(expectedResult.EntranceCode, actualResult.EntranceCode);
            Assert.AreEqual(expectedResult.ExpectedDeliveryFrom, actualResult.ExpectedDeliveryFrom);
            Assert.AreEqual(expectedResult.ExpectedDeliveryTo, actualResult.ExpectedDeliveryTo);
            Assert.AreEqual(expectedResult.Flat, actualResult.Flat);
            Assert.AreEqual(expectedResult.Floor, actualResult.Floor);
            Assert.AreEqual(expectedResult.FloorTax, actualResult.FloorTax);
            Assert.AreEqual(expectedResult.HasServiceLift, actualResult.HasServiceLift);
            Assert.AreEqual(expectedResult.House, actualResult.House);
            Assert.AreEqual(expectedResult.Housing, actualResult.Housing);
            Assert.AreEqual(expectedResult.PickupPointId, actualResult.PickupPointId);
            Assert.AreEqual(expectedResult.PickupPointName, actualResult.PickupPointName);
            Assert.AreEqual(expectedResult.ProposedDeliveryFrom, actualResult.ProposedDeliveryFrom);
            Assert.AreEqual(expectedResult.ProposedDeliveryTo, actualResult.ProposedDeliveryTo);
            Assert.AreEqual(expectedResult.Street, actualResult.Street);
            Assert.AreEqual(expectedResult.SubwayStationId, actualResult.SubwayStationId);
            Assert.AreEqual(expectedResult.SubwayStationName, actualResult.SubwayStationName);

            if (expectedResult.ProposedDeliveryFrom.HasValue)
            {
                Assert.AreEqual(expectedResult.ProposedDeliveryFrom.Value.Hour, actualResult.PropesedDeliveryFromHours);
                Assert.AreEqual(expectedResult.ProposedDeliveryFrom.Value.Minute, actualResult.ProposedDeliveryFromMinutes);
            }
            if (expectedResult.ProposedDeliveryTo.HasValue)
            {
                Assert.AreEqual(expectedResult.ProposedDeliveryTo.Value.Hour, actualResult.ProposedDeliveryToHours);
                Assert.AreEqual(expectedResult.ProposedDeliveryTo.Value.Minute, actualResult.ProposedDeliveryToMinutes);
            }

            Assert.AreEqual(sapCode, externalCustomerOrder.SaleLocation.SaleLocationId);
            foreach (var e in expectedResult.OrderStatusHistory)
            {
                Assert.IsTrue(actualResult.OrderStatusHistory.Any(a =>
                    a.ChangedType.Equals(e.ChangedType)
                    && a.FromStatus.Equals(e.FromStatus)
                    && a.ToStatus.Equals(e.ToStatus)
                    && a.ReasonText.Equals(e.ReasonText)
                    && a.UpdateTime.ToString(CultureInfo.InvariantCulture).Equals(e.UpdateTime.ToString(CultureInfo.InvariantCulture))
                    && a.WhoChanged.Equals(e.WhoChanged)));
            }
            foreach (var e in expectedResult.OrderLines)
            {
                Assert.IsTrue(actualResult.OrderLines.Any(a =>
                    a.LongTail.Equals(e.LongTail)
                    && a.ArticleNo.Equals(e.ArticleNo)
                    && a.ArticleTitle.Equals(e.ArticleTitle)
                    && a.DiscountPercentage.Equals(e.DiscountPercentage)
                    && a.DiscountPrice.Equals(e.DiscountPrice)
                    && a.Quantity.Equals(e.Quantity)
                    && a.UmaxStatus.Equals(e.UmaxStatus)
                    && a.Price.Equals(e.Price)));
            }
            catalogueService.Verify(x => x.GetCityDictionary());
            catalogueService.Verify(x => x.GetDeliveryTypeDictionary());
            catalogueService.Verify(x => x.GetMetroStationDictionary());
        }

        [TestMethod]
        public void ToExternalCustomerOrderTest()
        {
            var catalogueService = new Mock<ICatalogueService>();
            var authService = new Mock<IAuthenticationService>();
            var orderService = new Mock<IOrderService>();
            var dadataClient = new Mock<DadataClient>();
            var logger = new Mock<ILog>();
            catalogueService.Setup(x => x.GetDeliveryTypeDictionary()).Returns(() => new GetDictionaryOperationResult
                {
                    ReturnCode = ReturnCode.Ok,
                    Dictionary = new[]
                        {
                            new KeyValuePair<string, string>("11", "Самовывоз"),
                            new KeyValuePair<string, string>("12", "Барнаул, Улица Второго Мая, 12"),
                            new KeyValuePair<string, string>("13", "DeliveryType")
                        },
                });
            catalogueService.Setup(x => x.GetPickupPointDictionary()).Returns(() => new GetDictionaryOperationResult
                {
                    ReturnCode = ReturnCode.Ok,
                    Dictionary = new[]
                        {
                            new KeyValuePair<string, string>("11", "Абакан, Улица Первого Мая, 11"),
                            new KeyValuePair<string, string>("12", "Барнаул, Улица Второго Мая, 12"),
                            new KeyValuePair<string, string>("13", "PickupPointName")
                        },
                });
            catalogueService.Setup(x => x.GetCityDictionary()).Returns(() => new GetDictionaryOperationResult
                {
                    ReturnCode = ReturnCode.Ok,
                    Dictionary = new[]
                        {
                            new KeyValuePair<string, string>("11", "Абакан"),
                            new KeyValuePair<string, string>("12", "Барнаул"),
                            new KeyValuePair<string, string>("13", "City")
                        },
                });
            catalogueService.Setup(x => x.GetMetroStationDictionary()).Returns(() => new GetDictionaryOperationResult
                {
                    ReturnCode = ReturnCode.Ok,
                    Dictionary = new[]
                        {
                            new KeyValuePair<string, string>("11", "Абакан"),
                            new KeyValuePair<string, string>("12", "Барнаул"),
                            new KeyValuePair<string, string>("10", "SubwayStationName")
                        },
                });
            catalogueService.Setup(x => x.IsSelfCarryDeliveryTypeResult("self"))
                .Returns(() => new IsSelfCarryDeliveryTypeResult
                {
                    IsSelfCarry = true,
                    ReturnCode = ReturnCode.Ok
                });
            catalogueService.Setup(x => x.IsSelfCarryDeliveryTypeResult("DeliveryType")).Returns((string key) => new IsSelfCarryDeliveryTypeResult
            {
                ReturnCode = ReturnCode.Ok,
                IsSelfCarry = true,
            });
            authService.Setup(x => x.GetCurrentUser()).Returns(() => new UserInfo
            {
                IsAuthtorized = true,
                Email = "email@email.email",
                LoginName = "LoginName",
            });
            var random = new Random();
            Func<DateTime> randomDate = () => DateTime.Now.AddTicks(random.Next(-1000000, 1000000));

            var externalCustomerOrder = new Zzt.Web.MVC.Obs.CustomerOrder
            {
                Customer = new Customer
                {
                    Account = "Account",
                    AgreedToProcessingPD = true,
                    BIC = "BIC",
                    BankName = "BankName",
                    Birthday = randomDate.Invoke(),
                    CorrespondingAccount = "CorrespondingAccount",
                    CustomerType = "CustomerType",
                    Email = "Email",
                    RecepientName = "RecepientName",
                    Fax = "Fax",
                    FirstName = "FirstName",
                    Gender = 'G',
                    HasSubscription = true,
                    INN = "INN",
                    KPP = "KPP",
                    LastName = "LastName",
                    Login = "Login",
                    Mobile = "Mobile",
                    OKONH = "OKONH",
                    OKPO = "OKPO",
                    Phone = "Phone",
                    ProcessingPDAgreementDate = randomDate.Invoke(),
                    RegistrationDate = randomDate.Invoke(),
                    Surname = "Surname",
                },
                Delivery = new DeliveryInfo
                {
                    ActualDeliveryFrom = randomDate.Invoke(),
                    ActualDeliveryTo = randomDate.Invoke(),
                    Address = "Address",
                    AddressComment = "AddressComment",
                    Building = "Building",
                    City = "City",
                    DeliveryCompany = "DeliveryCompany",
                    DeliveryPrice = 100,
                    DeliveryType = "self",
                    Entrance = "Entrance",
                    EntranceCode = "EntranceCode",
                    ExpectedDeliveryFrom = randomDate.Invoke(),
                    ExpectedDeliveryTo = randomDate.Invoke(),
                    Flat = "Flat",
                    Floor = "Floor",
                    FloorTax = 200,
                    HasServiceLift = true,
                    House = "House",
                    Housing = "Housing",
                    Latitude = 1.00,
                    Longitude = 2.00,
                    PickupPointId = 13,
                    PickupPointName = "PickupPointName",
                    ProposedDeliveryFrom = randomDate.Invoke(),
                    ProposedDeliveryTo = randomDate.Invoke(),
                    Street = "Street",
                    SubwayStationId = 10,
                    SubwayStationName = "SubwayStationName",
                },
                OrderId = "OrderId",
                OrderInfo = new OrderInfo
                {
                    AdditionalAttribures = new Dictionary<string, string>
                                {
                                    {"1", "2"},
                                    {"3", "4"}
                                },
                    CreateData = randomDate.Invoke(),
                    CreatedBy = "CreatedBy",
                    CustomerComment = "CustomerComment",
                    CustomerTypeExtended = "CustomerTypeExtended",
                    DiscountCardNo = "DiscountCardNo",
                    DiscountTypeCard = "DiscountTypeCard",
                    ExpirationDate = randomDate.Invoke(),
                    ExternalOrderId = "ExternalOrderId",
                    FinalPaymentType = "FinalPaymentType",
                    InTicketTool = true,
                    IsOnlineOrder = true,
                    IsOnlinePayment = true,
                    IsPreorder = true,
                    LastChangingSystem = "LastChangingSystem",
                    MinimalPriceForDiscount = 50000000,
                    OrderDiscount = 90000000,
                    OrderSource = "OrderSource",
                    OrderSourceSystem = "OrderSourceSystem",
                    OrderState = "OrderState",
                    OrderStatusHistory = new[]
                                {
                                    new OrderStatusChange
                                        {
                                            ChangedType = "ChangedType",
                                            FromStatus = "FromStatus",
                                            ReasonText = "ReasonText",
                                            ToStatus = "ToStatus",
                                            UpdateTime = randomDate.Invoke(),
                                            WhoChanged = "WhoChanged",
                                        }
                                },
                    PaymentType = new PaymentType
                    {
                        Code = "Code",
                        Id = 1,
                        Name = "Name",
                        NameRu = "NameRu",
                    },
                    PickupType = "PickupType",
                    ShouldBeProcessedInTT = true,
                    SystemComment = "SystemComment",
                    UpdateDate = randomDate.Invoke(),
                    UpdatedBy = "UpdatedBy",
                    UserQuality = 1,
                    UserQualityComment = "UserQualityComment",
                    UtmSourse = "UtmSourse",
                    WWSOrderId = "WWSOrderId",
                    WorkflowType = "WorkflowType",
                },
                OrderLines = new[]
                        {
                            new OrderLine
                                {
                                    ArticleNo = 2,
                                    ArticleTitle = "ArticleTitle",
                                    ArticleType = ArticleType.NonPhysycalGood,
                                    Disabled = true,
                                    DiscountPercentage = 3,
                                    IsSet = true,
                                    ItemState = "ItemState",
                                    LineType = OrderLineType.Article,
                                    LongTail = true,
                                    Price = 10000000,
                                    Quantity = 20,
                                    SerialNumber = "SerialNumber",
                                    SubOrderLines = new []
                                        {
                                            new OrderLine
                                                {
                                                    ArticleNo = 3,
                                                    ArticleTitle = "SubOrderLinesArticleTitle",
                                                    ArticleType = ArticleType.PhysicalGood,
                                                    Disabled = true,
                                                    DiscountPercentage = 4,
                                                    IsSet = true,
                                                    ItemState = "SubOrderLinesItemState",
                                                    LineType = OrderLineType.Set,
                                                    LongTail = true,
                                                    Price = 50000,
                                                    Quantity = 1,
                                                    SerialNumber = "SubOrderLinesSerialNumber",
                                                }
                                        },
                                    UmaxStatus = "UmaxStatus",
                                    WarrantedOrderLine = new OrderLine
                                        {
                                            ArticleNo = 4,
                                            ArticleTitle = "WarrantedOrderLineArticleTitle",
                                            ArticleType = ArticleType.PhysicalGood,
                                            Disabled = true,
                                            DiscountPercentage = 5,
                                            IsSet = true,
                                            ItemState = "WarrantedOrderLineItemState",
                                            LineType = OrderLineType.Set,
                                            LongTail = true,
                                            Price = 1000,
                                            Quantity = 2,
                                            SerialNumber = "WarrantedOrderLineSerialNumber",
                                        }
                                }
                        },
                SaleLocation = new SaleLocationInfo
                {
                    ChannelId = "ChannelId",
                    SaleLocationId = "SaleLocationId",
                    SapCode = "SapCode",
                    Title = "Title",
                },
            };
            orderService.Setup(x => x.GetOrder("OrderId")).Returns((string oId) => new GetOrderOperationResult
            {
                ReturnCode = Zzt.Web.MVC.Obs.ReturnCode.Ok,
                CustomerOrder = externalCustomerOrder,
            });

            var sapCode = "BR001";
            var internalCustomerOrder = new CustomerOrder
            {
                Email = externalCustomerOrder.Customer.Email,
                FirstName = externalCustomerOrder.Customer.FirstName,
                FullName = externalCustomerOrder.Customer.LastName + ' ' +externalCustomerOrder.Customer.FirstName,
                HasSecondPhone = !string.IsNullOrEmpty(externalCustomerOrder.Customer.Mobile),
                OptionalPhones = externalCustomerOrder.Customer.Mobile,
                Phone = externalCustomerOrder.Customer.Phone,
                LastName = externalCustomerOrder.Customer.LastName,
                CustomerComment = externalCustomerOrder.OrderInfo.CustomerComment,
                AddressComment = externalCustomerOrder.Delivery.AddressComment,
                Building = externalCustomerOrder.Delivery.Building,
                City = "13",
                RecepientName = externalCustomerOrder.Customer.RecepientName,
                //CityName = externalCustomerOrder.Delivery.City,
                DeliveryPrice = externalCustomerOrder.Delivery.DeliveryPrice,
                DeliveryType = externalCustomerOrder.Delivery.DeliveryType,
                Entrance = externalCustomerOrder.Delivery.Entrance,
                EntranceCode = externalCustomerOrder.Delivery.EntranceCode,
                ExpectedDeliveryFrom = externalCustomerOrder.Delivery.ExpectedDeliveryFrom,
                ExpectedDeliveryTo = externalCustomerOrder.Delivery.ExpectedDeliveryTo,
                Flat = externalCustomerOrder.Delivery.Flat,
                Floor = externalCustomerOrder.Delivery.Floor,
                FloorTax = externalCustomerOrder.Delivery.FloorTax,
                HasServiceLift = externalCustomerOrder.Delivery.HasServiceLift ?? false,
                House = externalCustomerOrder.Delivery.House,
                Housing = externalCustomerOrder.Delivery.Housing,
                PickupPointId = externalCustomerOrder.Delivery.PickupPointId,
                PickupPointName = externalCustomerOrder.Delivery.PickupPointName,
                ProposedDeliveryFrom = externalCustomerOrder.Delivery.ProposedDeliveryFrom,
                ProposedDeliveryTo = externalCustomerOrder.Delivery.ProposedDeliveryTo,
                Street = externalCustomerOrder.Delivery.Street,
                SubwayStationId = externalCustomerOrder.Delivery.SubwayStationId,
                SubwayStationName = externalCustomerOrder.Delivery.SubwayStationName,
                DiscountCardNo = externalCustomerOrder.OrderInfo.DiscountCardNo,
                OrderDiscount = externalCustomerOrder.OrderInfo.OrderDiscount,
                OrderId = externalCustomerOrder.OrderId,
                OrderLines = new List<Zzt.Web.MVC.Models.OrderLine>
                         {
                             new Zzt.Web.MVC.Models.OrderLine
                                 {
                                     ArticleNo = 2,
                                     ArticleTitle = "ArticleTitle",
                                     DiscountPercentage = 3,
                                     LongTail = true,
                                     Price = 10000000,
                                     Quantity = 20,
                                     UmaxStatus = "UmaxStatus",
                                 }
                         },
                OrderStatusHistory = new List<Zzt.Web.MVC.Models.OrderStatusChange>
                         {
                             new Zzt.Web.MVC.Models.OrderStatusChange
                                 {
                                     ChangedType = "ChangedType",
                                     FromStatus = "FromStatus",
                                     ReasonText = "ReasonText",
                                     ToStatus = "ToStatus",
                                     UpdateTime = new DateTime(2011,1,1),
                                     WhoChanged = "WhoChanged",
                                 },
                         },
                SystemComment = externalCustomerOrder.OrderInfo.SystemComment,
            };

            var actualResult = CustomerOrderConverter.Instance.ToExternalCustomerOrder(internalCustomerOrder, sapCode, catalogueService.Object, orderService.Object, authService.Object, dadataClient.Object, logger.Object);

            Assert.AreEqual(externalCustomerOrder.Customer.Account, actualResult.Customer.Account);
            Assert.AreEqual(externalCustomerOrder.Customer.AgreedToProcessingPD, actualResult.Customer.AgreedToProcessingPD);
            Assert.AreEqual(externalCustomerOrder.Customer.BIC, actualResult.Customer.BIC);
            Assert.AreEqual(externalCustomerOrder.Customer.BankName, actualResult.Customer.BankName);
            Assert.AreEqual(externalCustomerOrder.Customer.Birthday, actualResult.Customer.Birthday);
            Assert.AreEqual(externalCustomerOrder.Customer.CorrespondingAccount, actualResult.Customer.CorrespondingAccount);
            Assert.AreEqual(externalCustomerOrder.Customer.CustomerType, actualResult.Customer.CustomerType);
            Assert.AreEqual(externalCustomerOrder.Customer.Email, actualResult.Customer.Email);
            Assert.AreEqual(externalCustomerOrder.Customer.Fax, actualResult.Customer.Fax);
            Assert.AreEqual(externalCustomerOrder.Customer.FirstName, actualResult.Customer.FirstName);
            Assert.AreEqual(externalCustomerOrder.Customer.Gender, actualResult.Customer.Gender);
            Assert.AreEqual(externalCustomerOrder.Customer.HasSubscription, actualResult.Customer.HasSubscription);
            Assert.AreEqual(externalCustomerOrder.Customer.INN, actualResult.Customer.INN);
            Assert.AreEqual(externalCustomerOrder.Customer.KPP, actualResult.Customer.KPP);
            Assert.AreEqual(externalCustomerOrder.Customer.LastName, actualResult.Customer.LastName);
            Assert.AreEqual(externalCustomerOrder.Customer.Login, actualResult.Customer.Login);
            Assert.AreEqual(externalCustomerOrder.Customer.Mobile, actualResult.Customer.Mobile);
            Assert.AreEqual(externalCustomerOrder.Customer.OKONH, actualResult.Customer.OKONH);
            Assert.AreEqual(externalCustomerOrder.Customer.OKPO, actualResult.Customer.OKPO);
            Assert.AreEqual(externalCustomerOrder.Customer.ProcessingPDAgreementDate, actualResult.Customer.ProcessingPDAgreementDate);
            Assert.AreEqual(externalCustomerOrder.Customer.RegistrationDate, actualResult.Customer.RegistrationDate);
            Assert.AreEqual(externalCustomerOrder.Customer.Surname, actualResult.Customer.Surname);

            Assert.AreEqual(externalCustomerOrder.Delivery.ActualDeliveryFrom, actualResult.Delivery.ActualDeliveryFrom);
            Assert.AreEqual(externalCustomerOrder.Delivery.ActualDeliveryTo, actualResult.Delivery.ActualDeliveryTo);

            if (externalCustomerOrder.Delivery.DeliveryType == "self")
                Assert.IsTrue(string.IsNullOrEmpty(actualResult.Delivery.Address));
            else
                Assert.AreEqual(externalCustomerOrder.Delivery.Address, actualResult.Delivery.Address);

            Assert.AreEqual(externalCustomerOrder.Delivery.AddressComment, actualResult.Delivery.AddressComment);

            if (externalCustomerOrder.Delivery.DeliveryType == "self")
                Assert.IsTrue(string.IsNullOrEmpty(actualResult.Delivery.Building));
            else 
                Assert.AreEqual(externalCustomerOrder.Delivery.Building, actualResult.Delivery.Building);

            Assert.AreEqual(externalCustomerOrder.Delivery.City, actualResult.Delivery.City);

            Assert.AreEqual(externalCustomerOrder.Delivery.DeliveryCompany, actualResult.Delivery.DeliveryCompany);
            Assert.AreEqual(externalCustomerOrder.Delivery.DeliveryPrice, actualResult.Delivery.DeliveryPrice);
            Assert.AreEqual(externalCustomerOrder.Delivery.DeliveryType, actualResult.Delivery.DeliveryType);

            if (externalCustomerOrder.Delivery.DeliveryType == "self")
                Assert.IsTrue(string.IsNullOrEmpty(actualResult.Delivery.Entrance));
            else
                Assert.AreEqual(externalCustomerOrder.Delivery.Entrance, actualResult.Delivery.Entrance);

            if (externalCustomerOrder.Delivery.DeliveryType == "self")
                Assert.IsTrue(string.IsNullOrEmpty(actualResult.Delivery.EntranceCode));
            else
                Assert.AreEqual(externalCustomerOrder.Delivery.EntranceCode, actualResult.Delivery.EntranceCode);

            Assert.AreEqual(externalCustomerOrder.Delivery.ExpectedDeliveryFrom, actualResult.Delivery.ExpectedDeliveryFrom);
            Assert.AreEqual(externalCustomerOrder.Delivery.ExpectedDeliveryTo, actualResult.Delivery.ExpectedDeliveryTo);

            if (externalCustomerOrder.Delivery.DeliveryType == "self")
                Assert.IsTrue(string.IsNullOrEmpty(actualResult.Delivery.Flat));
            else
                Assert.AreEqual(externalCustomerOrder.Delivery.Flat, actualResult.Delivery.Flat);

            if (externalCustomerOrder.Delivery.DeliveryType == "self")
                Assert.IsTrue(string.IsNullOrEmpty(actualResult.Delivery.Floor));
            else
                Assert.AreEqual(externalCustomerOrder.Delivery.Floor, actualResult.Delivery.Floor);

            if (externalCustomerOrder.Delivery.DeliveryType == "self")
                Assert.IsTrue(actualResult.Delivery.FloorTax == 0);
            else
                Assert.AreEqual(externalCustomerOrder.Delivery.FloorTax, actualResult.Delivery.FloorTax);

            Assert.AreEqual(externalCustomerOrder.Delivery.HasServiceLift, actualResult.Delivery.HasServiceLift);

            if (externalCustomerOrder.Delivery.DeliveryType == "self")
                Assert.IsTrue(string.IsNullOrEmpty(actualResult.Delivery.House));
            else
                Assert.AreEqual(externalCustomerOrder.Delivery.House, actualResult.Delivery.House);

            if (externalCustomerOrder.Delivery.DeliveryType == "self")
                Assert.IsTrue(string.IsNullOrEmpty(actualResult.Delivery.Housing));
            else
                Assert.AreEqual(externalCustomerOrder.Delivery.Housing, actualResult.Delivery.Housing);

            Assert.AreEqual(externalCustomerOrder.Delivery.Latitude, actualResult.Delivery.Latitude);
            Assert.AreEqual(externalCustomerOrder.Delivery.Longitude, actualResult.Delivery.Longitude);
            Assert.AreEqual(externalCustomerOrder.Delivery.PickupPointId, actualResult.Delivery.PickupPointId);
            Assert.AreEqual(externalCustomerOrder.Delivery.PickupPointName, actualResult.Delivery.PickupPointName);
            Assert.AreEqual(externalCustomerOrder.Delivery.ProposedDeliveryFrom, actualResult.Delivery.ProposedDeliveryFrom);
            Assert.AreEqual(externalCustomerOrder.Delivery.ProposedDeliveryTo, actualResult.Delivery.ProposedDeliveryTo);
            
            if (externalCustomerOrder.Delivery.DeliveryType == "self")
                Assert.IsTrue(string.IsNullOrEmpty(actualResult.Delivery.Street));
            else
                Assert.AreEqual(externalCustomerOrder.Delivery.Street, actualResult.Delivery.Street);

            if (externalCustomerOrder.Delivery.DeliveryType == "self")
                Assert.IsTrue(actualResult.Delivery.SubwayStationId == null);
            else
                Assert.AreEqual(externalCustomerOrder.Delivery.SubwayStationId, actualResult.Delivery.SubwayStationId);

            if (externalCustomerOrder.Delivery.DeliveryType == "self")
                Assert.IsTrue(string.IsNullOrEmpty(actualResult.Delivery.SubwayStationName));
            else
                Assert.AreEqual(externalCustomerOrder.Delivery.SubwayStationName, actualResult.Delivery.SubwayStationName);

            Assert.AreEqual(externalCustomerOrder.OrderId, actualResult.OrderId);

            Assert.AreEqual(externalCustomerOrder.OrderInfo.AdditionalAttribures, actualResult.OrderInfo.AdditionalAttribures);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.CreateData, actualResult.OrderInfo.CreateData);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.CreatedBy, actualResult.OrderInfo.CreatedBy);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.CustomerComment, actualResult.OrderInfo.CustomerComment);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.CustomerTypeExtended, actualResult.OrderInfo.CustomerTypeExtended);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.DiscountCardNo, actualResult.OrderInfo.DiscountCardNo);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.DiscountTypeCard, actualResult.OrderInfo.DiscountTypeCard);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.ExpirationDate, actualResult.OrderInfo.ExpirationDate);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.FinalPaymentType, actualResult.OrderInfo.FinalPaymentType);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.ExternalOrderId, actualResult.OrderInfo.ExternalOrderId);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.IsOnlineOrder, actualResult.OrderInfo.IsOnlineOrder);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.InTicketTool, actualResult.OrderInfo.InTicketTool);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.IsOnlinePayment, actualResult.OrderInfo.IsOnlinePayment);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.IsPreorder, actualResult.OrderInfo.IsPreorder);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.LastChangingSystem, actualResult.OrderInfo.LastChangingSystem);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.MinimalPriceForDiscount, actualResult.OrderInfo.MinimalPriceForDiscount);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.OrderDiscount, actualResult.OrderInfo.OrderDiscount);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.OrderSource, actualResult.OrderInfo.OrderSource);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.OrderSourceSystem, actualResult.OrderInfo.OrderSourceSystem);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.OrderState, actualResult.OrderInfo.OrderState);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.PaymentType, actualResult.OrderInfo.PaymentType);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.PickupType, actualResult.OrderInfo.PickupType);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.ShouldBeProcessedInTT, actualResult.OrderInfo.ShouldBeProcessedInTT);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.UpdateDate, actualResult.OrderInfo.UpdateDate);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.UpdatedBy, actualResult.OrderInfo.UpdatedBy);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.UserQuality, actualResult.OrderInfo.UserQuality);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.UserQualityComment, actualResult.OrderInfo.UserQualityComment);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.UtmSourse, actualResult.OrderInfo.UtmSourse);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.WWSOrderId, actualResult.OrderInfo.WWSOrderId);
            Assert.AreEqual(externalCustomerOrder.OrderInfo.WorkflowType, actualResult.OrderInfo.WorkflowType);

            Assert.AreEqual(externalCustomerOrder.SaleLocation.ChannelId, actualResult.SaleLocation.ChannelId);
            Assert.AreEqual(externalCustomerOrder.SaleLocation.SaleLocationId, actualResult.SaleLocation.SaleLocationId);
            Assert.AreEqual(externalCustomerOrder.SaleLocation.SapCode, actualResult.SaleLocation.SapCode);
            Assert.AreEqual(externalCustomerOrder.SaleLocation.Title, actualResult.SaleLocation.Title);

            foreach (var e in externalCustomerOrder.OrderInfo.OrderStatusHistory)
                Assert.IsNotNull(
                    actualResult.OrderInfo.OrderStatusHistory.FirstOrDefault(a => e.ChangedType == a.ChangedType
                                                                                  && e.FromStatus == a.FromStatus
                                                                                  && e.ReasonText == a.ReasonText
                                                                                  && e.ToStatus == a.ToStatus
                                                                                  && e.UpdateTime == a.UpdateTime
                                                                                  && e.WhoChanged == a.WhoChanged));

            foreach (var e in externalCustomerOrder.OrderLines)
                Assert.IsNotNull(actualResult.OrderLines.FirstOrDefault(a => a.Disabled == e.Disabled
                                                                             && a.IsSet == e.IsSet
                                                                             && a.LongTail == e.LongTail
                                                                             && a.ArticleNo == e.ArticleNo
                                                                             && a.ArticleTitle == e.ArticleTitle
                                                                             && a.ArticleType == e.ArticleType
                                                                             &&
                                                                             a.DiscountPercentage ==
                                                                             e.DiscountPercentage
                                                                             && a.ItemState == e.ItemState
                                                                             && a.LineType == e.LineType
                                                                             && a.Price == e.Price
                                                                             && a.Quantity == e.Quantity
                                                                             && a.SerialNumber == e.SerialNumber
                                                                             && a.UmaxStatus == e.UmaxStatus));

            catalogueService.Verify(x => x.GetDeliveryTypeDictionary());
            if (internalCustomerOrder.DeliveryType != "self")
                catalogueService.Verify(x => x.GetMetroStationDictionary());
        }
    }

    [TestClass]
    public class OrderProccessingTest
    {
        [TestMethod]
        public void TestWrongArguments()
        {
            var fixture = new Fixture();
            fixture.Behaviors.Clear();
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());

            string message, sapCode = "BR001";
            var basket = fixture.CreateMany<Zzt.Web.MVC.Models.OrderLine>(20).ToList();
            var fixtureMvcOrder = fixture.Create<CustomerOrder>();
            var mock = new Mock<OrderProcessing>();
            var noOrderId = fixtureMvcOrder;
            noOrderId.OrderId = null;
            AssertException.Throws<ArgumentNullException>(() => mock.Object.Create(fixtureMvcOrder, null, out message));
            AssertException.Throws<ArgumentNullException>(() => mock.Object.Create(null, sapCode, out message));
            AssertException.Throws<ArgumentNullException>(() => mock.Object.Create(null, null, out message));
            AssertException.Throws<ArgumentNullException>(() => mock.Object.Update(fixtureMvcOrder, null, out message));
            AssertException.Throws<ArgumentNullException>(() => mock.Object.Update(noOrderId, null, out message));
            AssertException.Throws<ArgumentNullException>(() => mock.Object.Update(null, sapCode, out message));
            AssertException.Throws<ArgumentNullException>(() => mock.Object.Update(null, null, out message));
            AssertException.Throws<ArgumentNullException>(() => mock.Object.GetOrder(null, out message, out sapCode));
            AssertException.Throws<ArgumentNullException>(() => mock.Object.UpdateBasket(basket, null, out message));
            basket.Clear();
            AssertException.Throws<ArgumentNullException>(() => mock.Object.UpdateBasket(basket, fixtureMvcOrder.OrderId, out message));
            AssertException.Throws<ArgumentNullException>(() => mock.Object.UpdateBasket(null, fixtureMvcOrder.OrderId, out message));
        }
    }
}
