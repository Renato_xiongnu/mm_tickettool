﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Web.Compilation;
using System.Web.Mvc;
using Zzt.Web.MVC.BusinessLogic;
using Zzt.Web.MVC.CatalogueService;
using Zzt.Web.MVC.Extensions;
using Zzt.Web.MVC.Models;
using Zzt.Web.MVC.ProductsCatalogue;
using CustomerOrder = Zzt.Web.MVC.Models.CustomerOrder;
using OrderLine = Zzt.Web.MVC.Models.OrderLine;

namespace Zzt.Web.MVC.Controllers
{
#if !DEBUG
    [Authorize]
#endif
    public class OrdersController : BaseController
    {
        private OrderProcessing _orderProcessing;
        private List<long> _defaultArticles;
        private List<long> _excludeArticles;

        private OrderProcessing OrderProcessing
        {
            get { return _orderProcessing ?? (_orderProcessing = new OrderProcessing()); }
        }

        private List<long> DefaultArticles
        {
            get { return this._defaultArticles ?? (this._defaultArticles = GetArticles("DefaultArticles")); }
        }

        private List<long> ExcludeArticles
        {
            get { return this._excludeArticles ?? (this._excludeArticles = GetArticles("ExcludeArticles")); }
        }

        protected override void Dispose(bool disposing)
        {
            if (OrderProcessing != null)
                OrderProcessing.Dispose();
            base.Dispose(disposing);
        }

        [HttpGet]
        public ActionResult Copy(string id)
        {
            CustomerOrder order = null;

            if (!string.IsNullOrEmpty(id))
            {
                string message, sapCode;
                order = OrderProcessing.GetOrder(id, out message, out sapCode);
                SapCode = sapCode ?? SapCode;
                if (order == null)
                    ErrorMessage(message);
                else
                    ValidateOrderLines(order.OrderLines);
            }
            var model = new EditOrder(order == null ? null : "Редактирование копии заказа")
            {
                Order = order ?? new CustomerOrder(),
            };
            model.Order.OrderId = null;
            model.Order.OrderStatusHistory = new List<OrderStatusChange>();
            model.Order.SystemComment = string.Empty;
            model.Order.OriginalOrderId = id;
            model.Order.CustomerComment = string.Empty;
            model.Order.CitySelectListItems(OrderProcessing.CatalogueService);
            model.Order.DeliveryTypeSelectListItems(OrderProcessing.CatalogueService);            
            return Index(this.InitDadataSettings(model));
        }

        [HttpGet]
        public ActionResult Index(string id)
        {
            CustomerOrder order = null;

            if (!string.IsNullOrEmpty(id))
            {
                string message, sapCode;
                order = OrderProcessing.GetOrder(id, out message, out sapCode);
                SapCode = sapCode ?? SapCode;
                if (order == null)
                    ErrorMessage(message);
                else
                    ValidateOrderLines(order.OrderLines);
            }
            var model = new EditOrder(order == null ? null : "Редактирование заказа")
            {
                Order = order ?? new CustomerOrder(),
            };
            model.Order.CitySelectListItems(OrderProcessing.CatalogueService);
            model.Order.DeliveryTypeSelectListItems(OrderProcessing.CatalogueService);

            return View(this.InitDadataSettings(model));
        }

        [HttpPost]
        public ActionResult Index(EditOrder model, bool? save = null)
        {
            save = save ?? false;

            if (model != null && model.Order != null)
            {
                ValidateOrderLines(model.Order.OrderLines);

                if (save.Value && ModelState.IsValid)
                {
                    string message;
                    if (string.IsNullOrEmpty(model.Order.OrderId)
                            ? OrderProcessing.Create(model.Order, SapCode, out message)
                            : OrderProcessing.Update(model.Order, SapCode, out message))
                        return RedirectToAction("Index",new {id=model.Order.OrderId});//return Index(model.Order.OrderId);
                    ErrorMessage(message);
                }
            }
            else
                ModelState.AddModelError("Order", "Заказ не найден");

            if(!save.Value)
                ModelState.Clear();

            if (ModelState.Any(s => s.Value.Errors.Any()))
            {
                ModelState
                    .Where(s => s.Key.LastIndexOf('.') > -1)
                    .Select(s => new {Error = s.Value.Errors.FirstOrDefault(), Property = typeof (CustomerOrder).GetProperty(s.Key.Substring(s.Key.LastIndexOf('.') + 1))})
                    .Where(x => x.Error != null)
                    .Select(x => new
                    {
                        Error = x.Error.ErrorMessage,
                        Attribute = x.Property == null ? null : x.Property.GetCustomAttributes(true)
                            .Where(a => a as DisplayAttribute != null)
                            .Cast<DisplayAttribute>()
                            .FirstOrDefault()
                    })
                    .Select(x => new {x.Error, Name = x.Attribute != null ? x.Attribute.Name : String.Empty})
                    .Select(x => String.IsNullOrEmpty(x.Name) ? x.Error : String.Concat(x.Name, ": ", x.Error))
                    .ToList()
                    .ForEach(ErrorMessage);
                //ErrorMessage("Заказ не был сохранён. Проверьте заполнение обязательных полей.");
            }

            if (model != null && model.Order != null && !string.IsNullOrEmpty(model.Order.OrderId))
                model.Header = "Редактирование заказа";
            
            if (model != null 
                && model.Order != null 
                && string.IsNullOrEmpty(model.Order.OrderId) 
                && !string.IsNullOrEmpty(model.Order.OriginalOrderId))
                model.Header = "Редактирование копии заказа";

            return View("Index", this.InitDadataSettings(model));
        }

        private void ValidateOrderLines(List<OrderLine> lines)
        {
            if (lines == null || !lines.Any())
            {
                ModelState.AddModelError("Order.OrderLines", "Добавьте хотя бы 1 позицию");
                return;
            }

            try
            {
                var getItemsResponse = OrderProcessing.OrdersBackendServiceClient.GetArticleStockStatus(
                    new GetItemsRequest
                        {
                            Articles = lines.Select(l => l.ArticleNo.ToString(CultureInfo.InvariantCulture)).Where(s => !string.IsNullOrWhiteSpace(s)).ToArray(),
                            SaleLocation = SapCode,
                            Channel = ((string)AppSettingsExpressionBuilder.GetAppSetting("ProductServiceChannel")),
                        });

                if (getItemsResponse.Items == null || !getItemsResponse.Items.Any())
                    return;

                if (getItemsResponse.Items != null && getItemsResponse.Items != null && getItemsResponse.Items.GroupBy(d => d.Article).Count() != lines.GroupBy(l => l.ArticleNo).Count())
                {
                    var notFound = lines.GroupBy(l => l.ArticleNo).Where(l => getItemsResponse.Items.All(d => d.Article != l.Key)).Select(l => l.Key);
                    foreach (var l in notFound)
                        ModelState.AddModelError("Order.OrderLines", string.Format("Артикул {0} не найден", l));
                }

                for (var j = 0; j < lines.Count; j++)
                {
                    var orderLine = lines[j];
                    var article = getItemsResponse.Items.FirstOrDefault(i => i.Article == orderLine.ArticleNo);
                    var orderLineErrorKey = string.Format("Order.OrderLines[{0}].ArticleNo", j);
                    if (article == null)
                    {
                        ModelState.AddModelError(orderLineErrorKey, string.Format("Артикул не найден"));
                        continue;
                    }

                    if (!this.ExcludeArticles.Contains(article.Article))
                        orderLine.ArticleTitle = article.Title;
                    else
                    {
                        if (String.IsNullOrEmpty(orderLine.ArticleTitle))
                            orderLine.ArticleTitle = article.Title;
                    }

                    orderLine.LongTail = article.IsLongTail;
                    if (!this.ExcludeArticles.Contains(article.Article) && 
                        (article.Qty == 0 || orderLine.Quantity > article.Qty) &&
                        !String.Equals(orderLine.UmaxStatus, "проведен", StringComparison.InvariantCultureIgnoreCase))
                    {
                        ModelState.AddModelError(orderLineErrorKey, string.Format("На складе {0} шт.", article.Qty));
                    }
                    if (this.DefaultArticles.Contains(article.Article))
                    {
                        orderLine.Price = article.Price.HasValue ? article.Price.Value : default(decimal);
                    }
                }
            }
            catch (Exception e)
            {
                Message(MessageType.Error, e.Message);
                Logger.Error(e.Message, e);
            }
        }

        [HttpGet]
        public ActionResult CreateOrder()
        {
            var basketArticles=new List<BasketArticle>();
            var orderId = string.Empty;
            object tempBasketObject = Session["createOrderRequest"];
            if (tempBasketObject != null)
            {
                var tempBasket = (Tuple<string, List<BasketArticle>>) tempBasketObject;
                basketArticles = tempBasket.Item2;
                orderId = tempBasket.Item1;
            }

            var editOrder = ConstructOrder(basketArticles, orderId);
            Session.Remove("createOrderRequest");

            return Index(editOrder);
        }

        private static List<long> GetArticles(string type)
        {
            var articlesConfig = ConfigurationManager.AppSettings[type];
            return String.IsNullOrEmpty(articlesConfig)
                ? new List<long>()
                : articlesConfig.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(a => Int64.Parse(a.Trim())).ToList();
        }

        public ActionResult CreateOrder(List<BasketArticle> basketArticles, string orderId)
        {
            var user = System.Web.HttpContext.Current.User;
            if (user == null || !user.Identity.IsAuthenticated)
            {
                Session["createOrderRequest"] = new Tuple<string, List<BasketArticle>>(orderId, basketArticles);
                var u = new UrlHelper(ControllerContext.RequestContext);
                var returnUrl = u.Action("CreateOrder", "Orders");
                return RedirectToAction("Index", "Account", new { ReturnUrl = returnUrl });
            }

            var editOrder = ConstructOrder(basketArticles, orderId);
            try
            {
                var request = System.Net.WebRequest.CreateHttp(Url.GetCallcenterUrl(new
                    {
                        sap_code = SapCode,
                        basket = UrlExtensions.CallcenterBasketJson(new List<OrderLine>()),
                    }));
                request.Method = HttpMethod.Post.Method;
                request.BeginGetResponse(a => {}, request);
            }
            catch (Exception e)
            {
                Logger.Error("Запрос обработан, но корзина не была очищена.", e);
            }

            Session.Remove("createOrderRequest");
            ValidateOrderLines(editOrder.Order.OrderLines);

            return Index(editOrder);
        }

        private EditOrder ConstructOrder(List<BasketArticle> basketArticles, string orderId)
        {
            EditOrder editOrder = null;
            basketArticles = basketArticles ?? new List<BasketArticle>();
            AutoMapper.Mapper.CreateMap<BasketArticle, OrderLine>()
                .ForMember(line => line.ArticleNo, a => a.MapFrom(art => art.ArticleNum));
            Func<BasketArticle, OrderLine> lineMapper = AutoMapper.Mapper.Map<OrderLine>;

            if (!string.IsNullOrEmpty(orderId))
            {
                string message;
                if (!OrderProcessing.UpdateBasket(basketArticles.Select(lineMapper).ToList(), orderId, out message))
                    ErrorMessage(message);
                string sapCode;
                var customerOrder = OrderProcessing.GetOrder(orderId, out message, out sapCode);
                editOrder = customerOrder == null ? null : new EditOrder("Редактирование заказа") {Order = customerOrder};
                SapCode = sapCode;
            }

            editOrder = editOrder ?? new EditOrder {Order = new CustomerOrder {OrderLines = basketArticles.Select(lineMapper).ToList()}};
            ViewBag.FromPHP = string.IsNullOrWhiteSpace(orderId);

            editOrder.Order.CitySelectList = editOrder.Order.CitySelectListItems(_orderProcessing.CatalogueService);
            editOrder.Order.DeliveryTypeList = editOrder.Order.DeliveryTypeSelectListItems(_orderProcessing.CatalogueService);
            editOrder.Order.TimeSlotList = editOrder.Order.TimeSlotSelectListItems(_orderProcessing.CatalogueService);

            foreach (var article in this.DefaultArticles)
            {
                editOrder.Order.OrderLines.Add(new OrderLine { ArticleNo = article, Quantity = 1 });
            }

            return this.InitDadataSettings(editOrder);
        }

        [HttpGet]
        public ActionResult GetPickUpPoints(string cityId, string selected, int req)
        {
            ViewBag.SelectedId = selected;
            var result = new PickupLocationV3[0];

            if (String.IsNullOrEmpty(cityId)) return View(result);

            var points = OrderProcessing.CatalogueService.GetPickupLocationV3OperationResult(cityId);
            if (points.ReturnCode == ReturnCode.Ok)
                result = points.PickupLocations;

            Response.AddHeader("req", req.ToString(CultureInfo.InvariantCulture));
            return View(result);
        }

        [HttpGet]
        public ActionResult GetMetroStations(int? cityId, int? selected, int req)
        {
            ViewBag.SelectedId = selected ?? 0;
            var result = new List<MetroStation>
                {
                    new MetroStation
                        {
                            Name = "...",
                            MetroStationId = 0,
                        }
                };

            var stations = OrderProcessing.CatalogueService.GetMetroStationOperationResult(cityId);
            if (stations.ReturnCode == ReturnCode.Ok)
                result.AddRange(stations.Result);

            Response.AddHeader("req", req.ToString(CultureInfo.InvariantCulture));

            return View(result.OrderBy(r => r.Name.ToLower()).ToArray());
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);            
            ViewBag.SelfCarryDictionary = SelfCarryDeliveryTypes;
        }

        private List<KeyValuePair<string, bool>> SelfCarryDeliveryTypes
        {
            get
            {
                var result = new List<KeyValuePair<string, bool>>();
                var deliveryTypeDictionaryResult = OrderProcessing.CatalogueService.GetDeliveryTypeDictionary();

                if(deliveryTypeDictionaryResult.ReturnCode == ReturnCode.Error)
                    return result;

                result.AddRange(from pair in deliveryTypeDictionaryResult.Dictionary
                                let isSelfCarryDeliveryTypeResult = OrderProcessing.CatalogueService.IsSelfCarryDeliveryTypeResult(pair.Key)
                                select new KeyValuePair<string, bool>(pair.Key, isSelfCarryDeliveryTypeResult.ReturnCode == ReturnCode.Ok && isSelfCarryDeliveryTypeResult.IsSelfCarry));
                return result;
            }
        }

        [HttpPost]
        public ActionResult LineAdd(EditOrder model, int? position)
        {
            if (!position.HasValue)
                return Index(model, false);

            if (model.Order.OrderLines.Count > position.Value)
                model.Order.OrderLines[position.Value].Quantity = ++model.Order.OrderLines[position.Value].Quantity;

            ModelState.Clear();

            return Index(model, false);
        }

        [HttpPost]
        public ActionResult LineDec(EditOrder model, int? position)
        {
            if (!position.HasValue)
                return Index(model, false);

            if (model.Order.OrderLines.Count > position.Value && model.Order.OrderLines[position.Value].Quantity > 0)
            {
                model.Order.OrderLines[position.Value].Quantity = --model.Order.OrderLines[position.Value].Quantity;
                if (model.Order.OrderLines[position.Value].Quantity == 0)
                    model.Order.OrderLines.RemoveAt(position.Value);
            }

            ModelState.Clear();

            return Index(model, false);
        }

        [HttpPost]
        public ActionResult LineDel(EditOrder model, int? position)
        {
            if (!position.HasValue)
                return Index(model, false);

            if (model.Order.OrderLines.Count > position.Value)
                model.Order.OrderLines.RemoveAt(position.Value);

            ModelState.Clear();

            return Index(model, false);
        }

        private EditOrder InitDadataSettings(EditOrder model)
        {
            model.DadataSettings = new DadataSettings
            {
                AuthToken = ConfigurationManager.AppSettings["AuthToken"] ?? "",
                SuggestionUrlFio = ConfigurationManager.AppSettings["Dadata:SuggestionUrlFio"] ?? "",
                SuggestionUrlAddress = ConfigurationManager.AppSettings["Dadata:SuggestionUrlAddress"] ?? "",
            };
            return model;
        }
    }
}
