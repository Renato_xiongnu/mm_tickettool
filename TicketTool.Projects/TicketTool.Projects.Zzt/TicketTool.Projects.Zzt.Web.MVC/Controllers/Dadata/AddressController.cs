using System.Web.Http;
using Newtonsoft.Json.Linq;
using Orders.Integration.Dadata;
using Orders.Integration.Dadata.Contracts;
using Zzt.Web.MVC.Models;

namespace Zzt.Web.MVC.Controllers
{
    public class ParseAddressRequest
    {
        public string Address { get; set; }

        public int Hash { get; set; }
    }

    public class AddressController : ApiController
    {
        private DadataClient _dadataClient;

        private DadataClient DadataClient
        {
            get
            {
                return _dadataClient ?? (_dadataClient = new DadataClient(Orders.Integration.Dadata.Configuration.Instance));
            }
        }

        public DadataAddressResponse Post(JObject request)
        {
            var addressRequest = request.ToObject<ParseAddressRequest>();
            var result = new DadataAddressResponse
                {
                    AddressData = new AddressData(),
                    Hash = addressRequest != null ? addressRequest.Hash : 0,
                };

            if (addressRequest == null || string.IsNullOrEmpty(addressRequest.Address))
                return result;

            var customer = DadataClient.VerifyCustomer(new CustomerRequest
                {
                    Address = addressRequest.Address,
                });

            if (customer.AddressData != null && customer.Succesfull)
            {
                result.AddressData = customer.AddressData;
            }

            return result;
        }
    }
}