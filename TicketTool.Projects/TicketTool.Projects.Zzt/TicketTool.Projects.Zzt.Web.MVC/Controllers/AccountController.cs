﻿using System;
using System.Web.Mvc;

namespace Zzt.Web.MVC.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult Index()
        {
            ViewData["ShowSubmit"] = true;
            ViewData["returnUrl"] = Request.QueryString["ReturnUrl"];
            return View();
        }

        public ActionResult Login(string name, string password, string returnUrl)
        {
            var ttService = new TTAuth.AuthenticationServiceClient();
            if (ttService.LogIn(name, password))
            {
                if (!String.IsNullOrEmpty(returnUrl))
                {
                    return Redirect(returnUrl);
                }
                return Redirect(Request.UrlReferrer.ToString());
            }
            return Redirect(Request.UrlReferrer.ToString());
        }

        public ActionResult Logout()
        {
            var ttService = new TTAuth.AuthenticationServiceClient();
            ttService.LogOut();
            return Redirect(Request.UrlReferrer.ToString());
        }
    }
}
