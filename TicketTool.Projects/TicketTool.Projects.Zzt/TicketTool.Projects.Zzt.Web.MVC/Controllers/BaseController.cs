﻿using System.Collections.Generic;
using System.Web.Mvc;
using Zzt.Web.MVC.Models;
using log4net;

namespace Zzt.Web.MVC.Controllers
{
    public class BaseController : Controller
    {
        private ILog _logger;
        private const string SapCodeKey = "sap_code";
        protected string SapCode { get; set; }

        protected ILog Logger
        {
            get { return _logger ?? (_logger = LogManager.GetLogger("rollingFile")); }
        }

        protected void Message(MessageType type, string message)
        {
            var messages = (ViewData["Messages"] as List<Message>) ?? new List<Message>();
            messages.Add(new Message(type, message));
            ViewData["Messages"] = messages;
        }

        protected void ErrorMessage(string message)
        {
            Message(MessageType.Error, message);
        }

        protected void InfoMessage(string message)
        {
            Message(MessageType.Info, message);
        }

        protected void WarningMessage(string message)
        {
            Message(MessageType.Warning, message);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            Logger.Error("OnException", filterContext.Exception);

            base.OnException(filterContext);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            SapCode = Request.Headers[SapCodeKey] ?? Request[SapCodeKey];

            if (string.IsNullOrEmpty(SapCode))
                SapCode = ((string) System.Web.Compilation.AppSettingsExpressionBuilder.GetAppSetting("Default_SapCode"));

            Response.AddHeader(SapCodeKey, SapCode);
            ViewBag.SapCode = SapCode;
            base.OnActionExecuting(filterContext);
        }

        public ActionResult TestCreateOrderIframe()
        {
            return View();
        }
    }
}