﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;
using Antlr.Runtime;
using Zzt.Web.MVC.CatalogueService;

namespace Zzt.Web.MVC.Models
{
    public class CustomerOrder
    {
        public CustomerOrder() : this(null, null, null)
        {
        }

        public CustomerOrder(List<SelectListItem> cities, List<SelectListItem> deliveryTypes, List<SelectListItem> timeSlots)
        {
            PaymentTypeLabel = new PaymentType
                {
                    Code = "Cash",
                    Name = "Cash",
                    NameRu = "Наличными при получении заказа",
                    Id = 1,
                };
            OrderId = "";
            OrderLines = new List<OrderLine>();
            Header = "";
            CitySelectList = cities;
            DeliveryTypeList = deliveryTypes;
            TimeSlotList = timeSlots;
        }

        [HiddenInput]
        [Display(Name = "Источник")]
        public string OrderSource { get; set; }

        [HiddenInput]
        [Display(Name = "№ заказа")]
        public string OrderId { get; set; }

        [Display(Name = "№ исходного заказа")]
        public string OriginalOrderId { get; set; }

        public string Header { get; set; }

        [Required(ErrorMessage = "Обязательное поле")]
        [Display(Name = "Фамилия и имя заказчика")]
        public string FullName { get; set; }

        [Display(Name = "Имя")]
        public string FirstName { get; set; }

        [Display(Name = "Фамилия")]
        public string LastName { get; set; }

        [Display(Name = "Получатель не заказчик")]
        public bool IsReceiverNotCustomer { get; set; }

        [Display(Name = "ФИО получателя")]
        [CustomValidation(typeof(CustomerOrder), "ValidateRecepientName")]
        public string RecepientName { get; set; }

        [Display(Name = "Основной телефон")]
        [Required(ErrorMessage = "Обязательное поле")]
        [Phone(ErrorMessage = "Неправильный формат")]
        public string Phone { get; set; }

        [Display(Name = "Дополнительные телефоны")]
        [CustomValidation(typeof(CustomerOrder), "ValidateOptionalPhones")]
        public string OptionalPhones { get; set; }

        [Display(Name = "Есть дополнительные номера телефона")]
        public bool HasSecondPhone { get; set; }

        [Display(Name = "E-mail")]
        [EmailAddress(ErrorMessage = "Неправильный формат")]
        public string Email { get; set; }

        [Display(Name = "Способ оплаты")]
        public PaymentType PaymentTypeLabel { get; set; }

        [Display(Name = "Корзина")]
        [UIHint("OrderLineView")]
        public List<OrderLine> OrderLines { get; set; }

        [HiddenInput]
        [Display(Name = "Желаемая дата доставки")]
        public DateTime? ExpectedDeliveryFrom { get; set; }

        [HiddenInput]
        [Display(Name = "Желаемая дата доставки")]
        public DateTime? ExpectedDeliveryTo { get; set; }

        [Display(Name = "Предложенная дата доставки")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Дата доставки обязательное поле")]
        public DateTime? ProposedDeliveryFrom { get; set; }

        [HiddenInput]
        [Display(Name = "Предложенная дата доставки")]
        public DateTime? ProposedDeliveryTo { get; set; }

        [Display]
        public int ProposedDeliveryToHours { get; set; }

        [Display]
        public int ProposedDeliveryToMinutes { get; set; }

        [Display]
        [Required(ErrorMessage = "Время доставки обязательное поле")]
        public int PropesedDeliveryFromHours { get; set; }

        [Display]
        public int ProposedDeliveryFromMinutes { get; set; }

        [Display(Name = "Город")]
        [Required(ErrorMessage = "Обязательное поле")]
        public string City { get; set; }

        [Display(Name = "Улица")]
        [CustomValidation(typeof(CustomerOrder), "ValidateDelivery")]
        public string Street { get; set; }

        [Display(Name = "Номер дома")]
        [CustomValidation(typeof(CustomerOrder), "ValidateDelivery")]
        public string House { get; set; }

        [Display(Name = "Корпус")]
        public string Housing { get; set; }

        [Display(Name = "Строение")]
        public string Building { get; set; }

        [Display(Name = "Квартира")]
        public string Flat { get; set; }

        [Display(Name = "Подъезд")]
        public string Entrance { get; set; }

        [Display(Name = "Код подъезда")]
        public string EntranceCode { get; set; }

        [Display(Name = "Этаж")]
        [DefaultValue(typeof(string), "")]
        public string Floor { get; set; }

        //[Display(Name = "Стоимость подъема на этаж")]
        //public decimal FloorTax { get; set; }

        [Display(Name = "Грузовой лифт")]
        public bool HasServiceLift { get; set; }

        [Display(Name = "Станция метро")]
        public string SubwayStationName { get; set; }

        [HiddenInput]
        public int? SubwayStationId { get; set; }

        [Display(Name = "Способ получения")]
        public string DeliveryType { get; set; }

        [Display(Name = "Точка самовывоза")]
        public string PickupPointId { get; set; }

        [Display(Name = "Точка самовывоза")]
        public string PickupPointName { get; set; }

        [Display(Name = "Стоимость доставки")]
        public decimal DeliveryPrice { get; set; }

        [Display(Name = "Стоимость подъема на этаж")]
        public decimal FloorTax { get; set; }

        [Display(Name = "Адрес")]
        public string Address { get; set; }

        [Display(Name = "Комментарии к доставке")]
        [AllowHtml]
        public string AddressComment { get; set; }

        [Display(Name = "Подъём на этаж")]
        public bool NeedLiftToFloor { get; set; }

        public List<SelectListItem> CitySelectList { get; set; }

        public string CityJsonList
        {
            get
            {
                var regions = ConfigurationManager.AppSettings["CityRegions"].Split(',').Select(r => r.ToLower());
                var prefixes = new DadataSettings().BORegionPrefixes.Select(p => p.prefix.ToLower()).ToList();
                return String.Join(", ", this.CitySelectList
                    .Select(c => c.Text.ToLower())
                    .Select(c =>
                    {
                        prefixes.ForEach(p => { c = c.Replace(p, String.Empty); });
                        return c;
                    })
                    .Select(c => regions.Contains(c) ? "{region: '" + c + "'}" : "{city: '" + c + "'}")
                    .ToArray());
            }
        }

        public List<SelectListItem> DeliveryTypeList { get; set; }

        public List<SelectListItem> TimeSlotList { get; set; }
        
        public List<TimeSlot> TimeSlots { get; set; }

        [Display(Name = "Итого")]
        public string TotalSummText
        {
            get { return string.Format("{0:0}", TotalSumm); }
        }

        [HiddenInput]
        [Display(Name = "Внутренний комментарий")]
        public string SystemComment { get; set; }

        [Display(Name = "Добавить во внутренний комментарий")]
        [AllowHtml]
        public string SystemCommentNew { get; set; }

        [Display(Name = "Комментарий покупателя")]
        [AllowHtml]
        public string CustomerComment { get; set; }

        [Display(Name = "Итого")]
        private decimal TotalSumm
        {
            get { return Math.Round(OrderLines == null || !OrderLines.Any() ? 0 : OrderLines.Sum(l => l.Price * l.Quantity), 0); }
        }

        [Display(Name = "С учётом скидки")]
        public string DiscountTotalSummText
        {
            get { return string.Format("{0:0}", DiscountTotalSumm); }
        }

        [Display(Name = "С учётом скидки")]
        public decimal DiscountTotalSumm
        {
            get { return Math.Round(OrderLines == null || !OrderLines.Any() ? 0 : OrderLines.Sum(l => l.DiscountPrice * l.Quantity) - OrderDiscount, 0); }
        }

        [Display(Name = "Номер дисконтной карты")]
        public string DiscountCardNo { get; set; }

        [Display(Name = "Тип дисконтной карты")]
        public string DiscountCardType { get; set; }

        [HiddenInput]
        public decimal OrderDiscount { get; set; }

        [Display(Name = "Скидка на заказ")]
        public string OrderDiscountText { get { return string.Format("{0:0} руб.", OrderDiscount); } }

        [HiddenInput]
        [UIHint("OrderStatusChange")]
        [Display(Name = "История смены статуса")]
        public List<OrderStatusChange> OrderStatusHistory { get; set; }

        public static ValidationResult ValidateRecepientName(string value, ValidationContext validationContext)
        {
            var customer = (CustomerOrder)validationContext.ObjectInstance;

            if (!customer.IsReceiverNotCustomer)
                return ValidationResult.Success;

            return !string.IsNullOrEmpty((value ?? "").Trim())
                       ? ValidationResult.Success
                       : new ValidationResult("Обязательное поле");
        }

        public static ValidationResult ValidateOptionalPhones(string value, ValidationContext validationContext)
        {
            var customer = (CustomerOrder)validationContext.ObjectInstance;

            if (!customer.HasSecondPhone || string.IsNullOrEmpty(value))
                return ValidationResult.Success;

            //Ignore all non-digit characters or defined separators
            //Require at least one number, contained one decimal character
            var @decimal = new Regex(@"[\d]+");
            return value.Split(';', ',').Select(v => new string(v.Where(c => c >= 48 && c <= 57).ToArray())).All(@decimal.IsMatch)
                       ? ValidationResult.Success
                       : new ValidationResult("Проверьте формат дополнительных телефонных нормеров");
        }

        public static ValidationResult ValidateDelivery(string value, ValidationContext context)
        {
            var delivery = (CustomerOrder)context.ObjectInstance;
            using (var service = new CatalogueService.CatalogueServiceClient())
            {
                var deliveryTypeResult = service.IsSelfCarryDeliveryTypeResult(delivery.DeliveryType);
                var isSelfCary = deliveryTypeResult.ReturnCode == CatalogueService.ReturnCode.Ok && deliveryTypeResult.IsSelfCarry;
                if (isSelfCary || !string.IsNullOrEmpty(value))
                    return ValidationResult.Success;
            }
            return new ValidationResult(string.Format(@"Обязательное поле"));
        }
    }

    public class OrderStatusChange
    {
        [Display(Name = "Дата")]
        public DateTime UpdateTime { get; set; }

        [Display(Name = "Кто изменил")]
        public string WhoChanged { get; set; }

        [Display(Name = "Комментарий")]
        public string ReasonText { get; set; }

        [Display(Name = "ChangedType")]
        public string ChangedType { get; set; }

        [Display(Name = "Статус до")]
        public string FromStatus { get; set; }

        [Display(Name = "Статус после")]
        public string ToStatus { get; set; }
    }

    public class PaymentType
    {
        [HiddenInput]
        public int Id { get; set; }

        [HiddenInput]
        public string Name { get; set; }

        [HiddenInput]
        public string NameRu { get; set; }

        [HiddenInput]
        public string Code { get; set; }
    }

    public class OrderLine
    {
        public OrderLine()
        {
            DiscountPercentage = 0;
        }

        [Display(Name = "Артикул")]
        [DisplayFormat(NullDisplayText = "")]
        public long ArticleNo { get; set; }

        [Display(Name = "Наименование")]
        [AllowHtml]
        public string ArticleTitle { get; set; }

        [Display(Name = "Цена")]
        [DisplayFormat(DataFormatString = "{0:0} руб", ApplyFormatInEditMode = true)]
        public decimal Price { get; set; }

        [Display(Name = "Количество")]
        public int Quantity { get; set; }

        [Display(Name = "Скидка, %")]
        [DisplayFormat(DataFormatString = "{0:0} %", ApplyFormatInEditMode = true)]
        public decimal DiscountPercentage { get; set; }

        [Display(Name = "LongTail")]
        public bool LongTail { get; set; }

        [Display(Name = "Скидка")]
        [Editable(false)]
        public string DiscountPercentageText { get { return DiscountPercentage+"%"; } }

        [Editable(false)]
        [Display(Name = "Цена со скидкой")]
        public decimal DiscountPrice { get { return DiscountPercentage > 0 ? Price * (100 - DiscountPercentage) / 100 : Price; } }

        [Editable(false)]
        [Display(Name = "Предзаказ")]
        public string UmaxStatus { get; set; }

        [Editable(false)]
        public string PriceText
        {
            get { return string.Format("{0:0}", Price); }
        }
    }
}