﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Zzt.Web.MVC.Models
{
    public class Message
    {
        public Message()
        {
            Type = MessageType.Disabled;
        }

        public Message(MessageType type = MessageType.Info, string message = "") : this()
        {
            Type = type;
            Text = message;
        }

        public MessageType Type { get; set; }

        [Display]
        public string Text { get; set; }

        public class MessageTypeStyle
        {
            public string IconStyle { get; set; }

            public string ContainerStyle { get; set; }
        }

        public static Dictionary<MessageType, MessageTypeStyle> Styles = new Dictionary<MessageType, MessageTypeStyle>
            {
                {
                    MessageType.Error,
                    new MessageTypeStyle
                        {
                            IconStyle = "ui-icon-alert",
                            ContainerStyle = "ui-state-error",
                        }
                },
                {
                    MessageType.Warning,
                    new MessageTypeStyle
                        {
                            IconStyle = "ui-icon-info",
                            ContainerStyle = "ui-state-highlight",
                        }
                },
                {
                    MessageType.Info,
                    new MessageTypeStyle
                        {
                            IconStyle = "ui-icon-info",
                            ContainerStyle = "ui-state-highlight",
                        }
                },
            };
    }
}