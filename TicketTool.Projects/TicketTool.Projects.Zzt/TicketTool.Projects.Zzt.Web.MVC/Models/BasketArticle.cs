﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Zzt.Web.MVC.Models
{
    [ModelBinder(typeof(BasketArticleModelBinder))]
    public class BasketArticle
    {
        public long ArticleNum { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
    }

    public class BasketData
    {
        public IEnumerable<BasketArticle> BasketArticles { get; set; }
    }

    public class BasketArticleModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType != typeof (BasketArticle))
                return null;

            if (!bindingContext.ValueProvider.ContainsPrefix(bindingContext.ModelName))
                return null;
            try
            {
                var result = Activator.CreateInstance(bindingContext.ModelType);

                bindingContext.ModelType.GetProperty("Quantity").SetValue(result, Convert.ToInt32(bindingContext.ValueProvider.GetValue(bindingContext.ModelName + "[quantity]").AttemptedValue));
                bindingContext.ModelType.GetProperty("ArticleNum").SetValue(result, Convert.ToInt64(bindingContext.ValueProvider.GetValue(bindingContext.ModelName + "[articleNum]").AttemptedValue));
                bindingContext.ModelType.GetProperty("Price").SetValue(result, Convert.ToDecimal(bindingContext.ValueProvider.GetValue(bindingContext.ModelName + "[price]").AttemptedValue));

                return result;
            }
            catch
            {
                return null;
            }
        }
    }
}