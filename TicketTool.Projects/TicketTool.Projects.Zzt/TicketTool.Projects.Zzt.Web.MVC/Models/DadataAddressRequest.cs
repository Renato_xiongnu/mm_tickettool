﻿namespace Zzt.Web.MVC.Models
{
    public class DadataAddressResponse
    {
        public Orders.Integration.Dadata.Contracts.AddressData AddressData { get; set; }
        public int Hash { get; set; }
    }
}