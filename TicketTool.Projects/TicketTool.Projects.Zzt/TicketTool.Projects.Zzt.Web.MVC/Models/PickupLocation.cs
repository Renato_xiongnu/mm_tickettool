﻿using System.Linq;
using System.Text;

namespace Zzt.Web.MVC.CatalogueService
{
    public partial class PickupLocationV3
    {
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append(Title);            
            if (LogisticProperties != null)
            {
                sb.Append("/");
                var length = LogisticProperties.LengthLimitCm;
                var width = LogisticProperties.WidthLimitCm;
                var height = LogisticProperties.HeightLimitCm;
                var weight = LogisticProperties.WeightLimitKg;
                if (length.HasValue && length.Value != 0) sb.AppendFormat("Длина: {0} см", length.Value);
                if (width.HasValue && width.Value != 0) sb.AppendFormat(" Ширина: {0} см", width.Value);
                if (height.HasValue && height.Value != 0) sb.AppendFormat(" Высота: {0} см", height.Value);
                if (weight.HasValue && weight.Value != 0) sb.AppendFormat(" Вес: {0} кг", weight.Value);
            }
            if (PaymentOptions != null)
            {
                sb.Append("/");
                var option = PaymentOptions.FirstOrDefault(p => p.Id == "COD");
                if (option != null && option.Limits != null && option.Enable && option.Limits.MaxOrderPrice.HasValue && option.Limits.MaxOrderPrice.Value != 0)
                {
                    sb.AppendFormat("Максимальная стоимость заказа {0}", option.Limits.MaxOrderPrice.Value);
                }
            }
            return sb.ToString();
        }
    }
}