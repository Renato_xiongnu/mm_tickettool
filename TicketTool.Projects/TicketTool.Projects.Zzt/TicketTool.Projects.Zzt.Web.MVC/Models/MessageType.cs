﻿using System.ComponentModel.DataAnnotations;

namespace Zzt.Web.MVC.Models
{
    public enum MessageType
    {
        Disabled = -1,
        [Display(Name = "Ошибка")]
        Error = 1,
        [Display(Name = "Предупреждение")]
        Warning = 2,
        [Display(Name = "Информация")]
        Info = 3,
    }
}