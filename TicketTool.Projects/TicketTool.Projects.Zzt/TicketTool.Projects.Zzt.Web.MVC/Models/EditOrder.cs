﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Orders.Integration.Dadata;
using Orders.Integration.Dadata.Contracts;
using Zzt.Web.MVC.CatalogueService;
using Zzt.Web.MVC.Extensions;

namespace Zzt.Web.MVC.Models
{
    public class EditOrder
    {
        public EditOrder() : this("Создание заказа")
        {
        }

        public EditOrder(string header = null, List<SelectListItem> cities = null, List<SelectListItem> deliveryTypes = null, List<SelectListItem> timeSlots = null)
        {
            Header = header ?? "Создание заказа";
            Order = new CustomerOrder(cities, deliveryTypes, timeSlots);
        }

        public string Header { get; set; }

        public DadataSettings DadataSettings { get; set; }

        [UIHint("OrderView")]
        public CustomerOrder Order { get; set; }
    }

    public class DadataSettings
    {
        private List<Prefix> _prefixes;

        public string SuggestionUrlFio { get; set; }
        public string SuggestionUrlAddress { get; set; }
        public string AuthToken { get; set; }

        public List<Prefix> BORegionPrefixes
        {
            get { return this._prefixes ?? (this._prefixes = LoadPrefixes()); }
        }

        private static List<Prefix> LoadPrefixes()
        {
            string json = File.ReadAllText(HttpContext.Current.Server.MapPath("~/App_Data/regions.json"));
            return JsonConvert.DeserializeObject<List<Prefix>>(json);
        }
    }

    public class Prefix
    {
        public string prefix { get; set; }
        public string region { get; set; }
    }

    public class EditOrderModelBinder : DefaultModelBinder
    {
        protected override object CreateModel(ControllerContext controllerContext, ModelBindingContext bindingContext, Type modelType)
        {
            if (modelType != typeof(EditOrder))
                return base.CreateModel(controllerContext, bindingContext, modelType);

            using (var catalogueService = new CatalogueServiceClient())
            {
                var model = (EditOrder)base.CreateModel(controllerContext, bindingContext, modelType);
                model.Order.CitySelectList = model.Order.CitySelectListItems(catalogueService);
                model.Order.DeliveryTypeList = model.Order.DeliveryTypeSelectListItems(catalogueService);
                model.Order.TimeSlotList = model.Order.TimeSlotSelectListItems(catalogueService);
                return model;
            }
        }
    }

    public class CustomerOrderModelBinder : DefaultModelBinder
    {
        protected override void BindProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor)
        {
            var model = (CustomerOrder)bindingContext.Model;
            switch (propertyDescriptor.Name)
            {
                case "FullName":
                    var attemptedValue = bindingContext.ValueProvider.GetValue("Order.FullName").AttemptedValue;
                    if (attemptedValue == null)
                        return;
                    var dadataClient = new DadataClient(Configuration.Instance);
                    var dadataResponce = dadataClient.VerifyCustomer(new CustomerRequest
                    {
                        FIO = attemptedValue,
                    });
                    model.FullName = attemptedValue;
                    if (dadataResponce.Succesfull && dadataResponce.CustomerData != null)
                    {
                        model.LastName = dadataResponce.CustomerData.Surname;
                        model.FirstName = dadataResponce.CustomerData.Name;
                    }
                    break;
                default:
                    base.BindProperty(controllerContext, bindingContext, propertyDescriptor);
                    break;
            }
        }
    }
}