﻿
function generateLoader(container, optImgUrl, optWidth, optHeight) {
    if (!optImgUrl)
        optImgUrl = 'Content/themes/base/images/ajax-loader.gif';
    var height = optHeight === undefined || optHeight === null ? $(container).height() : optHeight;
    var width = optWidth === undefined || optWidth === null ? $(container).width() : optWidth;
    if (height === 0) height = 50;
    if (width === 0) width = 50;
    return '<div name="loader" style="text-align:center;width:' + width + 'px;height:' + height + 'px;"><img src="' + optImgUrl + '"></div>';
}
String.prototype.hashCode = function () {
    var hash = 0, i, c, l;
    if (this.length == 0) return hash;
    for (i = 0, l = this.length; i < l; i++) {
        c = this.charCodeAt(i);
        hash = ((hash << 5) - hash) + c;
        hash |= 0;
    }
    return hash;
};