﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Zzt.Web.MVC.BusinessLogic;
using Zzt.Web.MVC.CatalogueService;
using Zzt.Web.MVC.Models;

namespace Zzt.Web.MVC.Extensions
{
    public static class HtmlExtensions
    {
        public static IHtmlString EditorForCollection<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, IList<TProperty>>> expression)
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            if (string.IsNullOrEmpty(metadata.TemplateHint))
            {
                return html.EditorFor(expression);
            }

            var collection = (IList<TProperty>)metadata.Model;

            var sb = new StringBuilder();
            for (var i = 0; i < collection.Count; i++)
            {
                var indexExpression = Expression.Constant(i, typeof(int));
                var itemGetter = expression.Body.Type.GetProperty("Item", new[] { typeof(int) }).GetGetMethod();
                var methodCallExpression = Expression.Call(expression.Body, itemGetter, indexExpression);
                var itemExpression = Expression.Lambda<Func<TModel, TProperty>>(methodCallExpression, expression.Parameters[0]);
                var result = html.EditorFor(itemExpression, metadata.TemplateHint, new { index = i }).ToHtmlString();
                sb.AppendLine(result);
            }
            return new HtmlString(sb.ToString());
        }

        public static IHtmlString DisplayForCollection<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, IList<TProperty>>> expression)
        {
            var metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            if (string.IsNullOrEmpty(metadata.TemplateHint))
            {
                return html.DisplayFor(expression);
            }

            var collection = (IList<TProperty>)metadata.Model;

            var sb = new StringBuilder();
            if (collection == null)
                return new HtmlString(sb.ToString());

            for (var i = 0; i < collection.Count; i++)
            {
                var indexExpression = Expression.Constant(i, typeof(int));
                var itemGetter = expression.Body.Type.GetProperty("Item", new[] { typeof(int) }).GetGetMethod();
                var methodCallExpression = Expression.Call(expression.Body, itemGetter, indexExpression);
                var itemExpression = Expression.Lambda<Func<TModel, TProperty>>(methodCallExpression, expression.Parameters[0]);
                var result = html.DisplayFor(itemExpression, metadata.TemplateHint, new { index = i }).ToHtmlString();
                sb.AppendLine(result);
            }
            return new HtmlString(sb.ToString());
        }

        public static List<SelectListItem> DeliveryTypeSelectListItems(this CustomerOrder customerOrder, ICatalogueService service)
        {
            var dictionary = service.GetDeliveryTypeDictionary();

            var result = new List<SelectListItem>();
            if (dictionary.ReturnCode == ReturnCode.Ok)
                result.AddRange(dictionary.Dictionary.Select(kvp => new SelectListItem
                {
                    Selected = customerOrder != null && customerOrder.DeliveryType == kvp.Value,
                    Text = kvp.Value,
                    Value = kvp.Key,
                }));
            return customerOrder.DeliveryTypeList = result;
        }

        public static List<SelectListItem> TimeSlotSelectListItems(this CustomerOrder customerOrder, ICatalogueService service)
        {
            var dictionary = service.GetTimeSlotResult();

            var result = new List<SelectListItem>();
            if (dictionary.ReturnCode == ReturnCode.Ok)
            {
                result.AddRange(dictionary.TimeSlots.Select(ts => new SelectListItem
                {
                    Text = String.Format("{0} — {1}", ts.From, ts.To), 
                    Value = ts.From.ToString(CultureInfo.InvariantCulture),
                    Selected = ts.From == (customerOrder.ProposedDeliveryFrom.HasValue ? customerOrder.ProposedDeliveryFrom.Value.Hour : 0)                    
                }));
                customerOrder.TimeSlots = dictionary.TimeSlots.ToList();
            }
            
            return customerOrder.TimeSlotList = result;
        }

        public static List<SelectListItem> CitySelectListItems(this CustomerOrder customerOrder, ICatalogueService service)
        {
            var cityDictionary = service.GetCityDictionary();

            var citySelectList = new List<SelectListItem>();
            var city = customerOrder != null ? customerOrder.City : "";
            if (cityDictionary.ReturnCode == ReturnCode.Ok)
                citySelectList.AddRange(
                    cityDictionary.Dictionary.OrderBy(kvp => kvp.Value.ToLower()).Select(kvp => new SelectListItem
                    {
                        Selected = city == kvp.Key,
                        Text = kvp.Value,
                        Value = kvp.Key,
                    }));
            return customerOrder.CitySelectList = citySelectList;
        }

        public static IHtmlString ExtenstionErrorMessage<TModel>(this HtmlHelper<TModel> helper, Message message)
        {
            if (message.Type == MessageType.Disabled)
                return new MvcHtmlString("");

            return new MvcHtmlString(HtmlTemplate.Err_MessageBox.FormatWith(
                new
                    {
                        style_container = Message.Styles[message.Type].ContainerStyle,
                        text = message.Text.Replace("\r\n", "<br/>"),
                        message = message.Type.DisplayName(),
                        style_icon = Message.Styles[message.Type].IconStyle,
                    }));
        }
    }
}