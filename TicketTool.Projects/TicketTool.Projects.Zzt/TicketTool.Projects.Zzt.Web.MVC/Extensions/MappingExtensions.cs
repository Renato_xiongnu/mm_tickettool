using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Zzt.Web.MVC.Models;
using Zzt.Web.MVC.Obs;
using CustomerOrder = Zzt.Web.MVC.Models.CustomerOrder;
using OrderLine = Zzt.Web.MVC.Models.OrderLine;
using OrderStatusChange = Zzt.Web.MVC.Models.OrderStatusChange;
using PaymentType = Zzt.Web.MVC.Models.PaymentType;

namespace Zzt.Web.MVC.Extensions
{
    public static class MappingExtensions
    {
        public static void MapTypes()
        {
            #region Internal services

            Mapper.CreateMap<OrderInfo, CustomerOrder>()
                .ForMember(dst => dst.PaymentTypeLabel, e => e.MapFrom(src => src.PaymentType))
                .ForMember(dst => dst.OrderSource, e => e.MapFrom(src => src.OrderSourceSystem))
                .ForMember(dst => dst.CustomerComment, a => a.MapFrom(src => src.CustomerComment));

            Mapper.CreateMap<CustomerOrder, Obs.CustomerOrder>();

            Mapper.CreateMap<Obs.CustomerOrder, CustomerOrder>()
                .ForMember(dst => dst.CustomerComment, e => e.MapFrom(src => src.OrderInfo.CustomerComment))
                .ForMember(dst => dst.SystemComment, e => e.MapFrom(src => src.OrderInfo.SystemComment))
                .ForMember(dst => dst.OrderDiscount, e => e.MapFrom(src => src.OrderInfo.OrderDiscount))
                .ForMember(dst => dst.DiscountCardNo, e => e.MapFrom(src => src.OrderInfo.DiscountCardNo))
                .ForMember(dst => dst.DiscountCardType, e => e.MapFrom(src => src.OrderInfo.DiscountTypeCard));

            Mapper.CreateMap<CustomerOrder, OrderInfo>()
                .ForMember(dst => dst.CustomerComment, e => e.MapFrom(src => src.CustomerComment))
                .ForMember(dst => dst.PaymentType, e => e.MapFrom(src => src.PaymentTypeLabel))
                .ForMember(dst => dst.OrderStatusHistory, e => e.MapFrom(src => src.OrderStatusHistory))
                .ForMember(dst => dst.OrderSource, e => e.MapFrom(src => src.OrderSource))
                .ForMember(dst => dst.OrderSource, e => e.Ignore())
                .ForMember(dst => dst.OrderSourceSystem, e => e.Ignore())
                .ForMember(dst => dst.SystemComment, e => e.Ignore())
                .AfterMap((order, info) =>
                {
                    if (!string.IsNullOrWhiteSpace(order.SystemCommentNew))
                    {
                        info.SystemComment += (string.IsNullOrEmpty(info.SystemComment) ? "" : "\r\n") + order.SystemCommentNew;
                    }

                    info.OrderSource = System.Web.Compilation.AppSettingsExpressionBuilder.GetAppSetting("DefaultOrderSource") as string;
                    info.OrderSourceSystem = System.Web.Compilation.AppSettingsExpressionBuilder.GetAppSetting("DefaultOrderSourceSystem") as string;
                });

            Mapper.CreateMap<OrderLine, Obs.OrderLine>();
            Mapper.CreateMap<Obs.OrderLine, OrderLine>();

            Mapper.CreateMap<OrderLine, Obs.OrderLine>();
            Mapper.CreateMap<Obs.OrderLine, OrderLine>();

            Mapper.CreateMap<PaymentType, Obs.PaymentType>();
            Mapper.CreateMap<Obs.PaymentType, PaymentType>();

            Mapper.CreateMap<Customer, CustomerOrder>()
                .ForMember(customerOrder => customerOrder.OptionalPhones, a => a.MapFrom(customer => customer.Mobile))
                .ForMember(customerOrder => customerOrder.RecepientName, a => a.MapFrom(customer => customer.RecepientName))
                .ForMember(customerOrder => customerOrder.IsReceiverNotCustomer, a => a.MapFrom(c => IsReceiver(c)));

            Mapper.CreateMap<CustomerOrder, Customer>()
                .ForMember(customer => customer.Mobile, a => a.MapFrom(customerOrder => customerOrder.OptionalPhones))
                .ForMember(customer => customer.RecepientName, a => a.MapFrom(customerOrder => customerOrder.RecepientName));

            Mapper.CreateMap<DeliveryInfo, CustomerOrder>()
                .ForMember(customerOrder => customerOrder.City, a => a.MapFrom(delivery => delivery.City))
                .ForMember(customerOrder => customerOrder.FloorTax, a => a.MapFrom(delivery => delivery.FloorTax))
                .ForMember(customerOrder => customerOrder.NeedLiftToFloor, a => a.MapFrom(delivery => String.Equals(delivery.RequestedDeliveryServiceOption, "ApartamentWithoutLift", StringComparison.InvariantCultureIgnoreCase) || String.Equals(delivery.RequestedDeliveryServiceOption, "ApartmentWithLift", StringComparison.InvariantCultureIgnoreCase)))
                .ForMember(customerOrder => customerOrder.Address, a => a.Ignore())
                .ForMember(customerOrder => customerOrder.PropesedDeliveryFromHours, a => a.Ignore())
                .ForMember(customerOrder => customerOrder.ProposedDeliveryToHours, a => a.Ignore())
                .AfterMap((delivery, customerOrder) =>
                {
                    if (delivery.ProposedDeliveryFrom.HasValue)
                    {
                        customerOrder.PropesedDeliveryFromHours = delivery.ProposedDeliveryFrom.Value.Hour;
                        customerOrder.ProposedDeliveryFromMinutes = delivery.ProposedDeliveryFrom.Value.Minute;
                    }

                    if (delivery.ProposedDeliveryTo.HasValue)
                    {
                        customerOrder.ProposedDeliveryToHours = delivery.ProposedDeliveryTo.Value.Hour;
                        customerOrder.ProposedDeliveryToMinutes = delivery.ProposedDeliveryTo.Value.Minute;
                    }

                    var addressParts = new List<string> {delivery.City, delivery.Street, delivery.House, delivery.Housing}
                        .Where(s => !string.IsNullOrEmpty(s));
                    customerOrder.Address = string.Join(" ", addressParts);
                });
            Mapper.CreateMap<CustomerOrder, DeliveryInfo>()
                .ForMember(delivery => delivery.City, a => a.MapFrom(customerOrder => customerOrder.City))
                .ForMember(delivery => delivery.ProposedDeliveryFrom, a => a.Ignore())
                .ForMember(delivery => delivery.ProposedDeliveryTo, a => a.Ignore())
                .ForMember(delivery => delivery.FloorTax, a => a.MapFrom(customerOrder => customerOrder.FloorTax))
                .ForMember(delivery => delivery.RequestedDeliveryServiceOption, a => a.MapFrom(order => String.Equals(order.DeliveryType, "�������� ��������", StringComparison.InvariantCultureIgnoreCase) ? order.NeedLiftToFloor ? order.HasServiceLift ? "ApartmentWithLift" : "ApartmentWithoutLift" : "DoorstepEmbatted" : "None"))
                .AfterMap((customerOrder, delivery) =>
                {
                    if (!customerOrder.ProposedDeliveryFrom.HasValue) return;

                    var date = customerOrder.ProposedDeliveryFrom.Value.Date;
                    delivery.ProposedDeliveryFrom = date.AddHours(customerOrder.PropesedDeliveryFromHours);
                    delivery.ProposedDeliveryTo = date.AddHours(customerOrder.ProposedDeliveryToHours);
                    //delivery.ProposedDeliveryFrom = delivery.ProposedDeliveryFrom.Value.AddHours(-delivery.ProposedDeliveryFrom.Value.Hour + );
                    //delivery.ProposedDeliveryTo = delivery.ProposedDeliveryTo.Value.AddHours(-delivery.ProposedDeliveryTo.Value.Hour + customerOrder.ProposedDeliveryToHours);
                });

            Mapper.CreateMap<OrderStatusChange, Obs.OrderStatusChange>();
            Mapper.CreateMap<Obs.OrderStatusChange, OrderStatusChange>();

            #endregion

            #region External services

            Mapper.CreateMap<OrderLine, BasketArticle>().ForMember(d => d.ArticleNum, o => o.MapFrom(s => s.ArticleNo));
            Mapper.CreateMap<BasketArticle, OrderLine>().ForMember(d => d.ArticleNo, o => o.MapFrom(s => s.ArticleNum));

            #endregion
        }

        private static bool IsReceiver(Customer c)
        {
            return c.RecepientName != String.Join(" ", new[] { ToFullNamePart(c.LastName), ToFullNamePart(c.FirstName), ToFullNamePart(c.Surname) }).Trim();
        }

        private static string ToFullNamePart(string firstName)
        {
            return !String.IsNullOrEmpty(firstName) ? firstName.Trim() : String.Empty;
        }
    }
}