﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Zzt.Web.MVC.Extensions
{
    public static class EnumExtensions
    {
        public static IEnumerable<SelectListItem> ToSelectList<T>(T? selected = null, List<T> ignoreItems = null) where T : struct
        {
            var t = typeof (T);

            if(!t.IsEnum)
                yield return new SelectListItem();

            foreach (var value in t.GetEnumValues())
            {
                var displayAttribute = t.GetField(t.GetEnumName(value)).GetCustomAttributes(typeof (System.ComponentModel.DataAnnotations.DisplayAttribute), false).OfType<System.ComponentModel.DataAnnotations.DisplayAttribute>().FirstOrDefault();

                if (ignoreItems != null && ignoreItems.Contains((T)value))
                    continue;

                yield return new SelectListItem
                    {
                        Text = displayAttribute == null ? t.GetEnumName(value) : displayAttribute.Name,
                        Value = value.ToString(),
                        Selected = value.Equals(selected),
                    };
            }
        }

        public static string DisplayName<T>(this T? @enum) where T: struct
        {
            return @enum.HasValue ? @enum.Value.DisplayName() : "";
        }

        public static string DisplayName<T>(this T @enum) where T: struct
        {
            var t = typeof (T);

            if (!t.IsEnum)
                return @enum.ToString();

            var displayAttribute = t.GetField(t.GetEnumName(@enum)).GetCustomAttributes(typeof(System.ComponentModel.DataAnnotations.DisplayAttribute), false).OfType<System.ComponentModel.DataAnnotations.DisplayAttribute>().FirstOrDefault();
            return displayAttribute == null ? t.GetEnumName(@enum) : displayAttribute.Name;
        }

        public static string ShortName<T>(this T? @enum) where T: struct
        {
            return !@enum.HasValue ? "" : @enum.Value.ShortName();
        }

        public static string ShortName<T>(this T @enum) where T: struct
        {
            var t = typeof (T);

            if (!t.IsEnum)
                return @enum.ToString();

            var displayAttribute = t.GetField(t.GetEnumName(@enum)).GetCustomAttributes(typeof(System.ComponentModel.DataAnnotations.DisplayAttribute), false).OfType<System.ComponentModel.DataAnnotations.DisplayAttribute>().FirstOrDefault();
            return displayAttribute == null ? t.GetEnumName(@enum) : displayAttribute.GetShortName();
        }
    }
}