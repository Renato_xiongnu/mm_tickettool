﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Zzt.Web.MVC.Models;

namespace Zzt.Web.MVC.Extensions
{
    public static class UrlExtensions
    {
        public static string GetAbsoluteUrl(this UrlHelper helper, string relativeUrl)
        {
            if (helper.RequestContext.HttpContext.Request.Url == null)
                throw new ArgumentNullException("helper");

            return string.Format("{0}://{1}{2}{3}"
                                 , helper.RequestContext.HttpContext.Request.Url.Scheme
                                 , helper.RequestContext.HttpContext.Request.Url.Host
                                 , helper.RequestContext.HttpContext.Request.Url.IsDefaultPort
                                       ? ""
                                       : ":" + helper.RequestContext.HttpContext.Request.Url.Port
                                 , relativeUrl);
        }

        public static string GetCallcenterUrl(this UrlHelper helper, object routeData)
        {
            if (helper.RequestContext.HttpContext.Request.Url == null)
                throw new ArgumentNullException("helper");

            return string.Format("{0}?{1}", System.Web.Compilation.AppSettingsExpressionBuilder.GetAppSetting("CallcenterURL"), BuildQueryString(routeData));
        }

        public static string CallcenterBasketJson(IEnumerable<OrderLine> items)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(new
                {
                    BasketArticles = items.Select(i => new
                        {
                            ArticleNum = i.ArticleNo,
                            price = i.Price,
                            count = i.Quantity,
                            discount = i.DiscountPercentage,
                        }).ToArray(),
                });
        }

        public static MvcHtmlString BuildQueryString(object routeData)
        {
            return new MvcHtmlString(string.Join("&", routeData.GetType()
                                             .GetProperties()
                                             .Select(fi =>
                                                 {
                                                     var value = fi.GetValue(routeData);
                                                     return new KeyValuePair<string, string>(fi.Name, value == null
                                                                                                          ? string.Empty
                                                                                                          : value.ToString());
                                                 })
                                             .Select(p => string.Format("{0}={1}", p.Key, System.Web.HttpUtility.UrlEncode(p.Value)))
                                             .ToArray()));
        }
    }
}