﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using AutoMapper;
using Orders.Integration.Dadata;
using Orders.Integration.Dadata.Contracts;
using Zzt.Web.MVC.CatalogueService;
using Zzt.Web.MVC.Extensions;
using Zzt.Web.MVC.Obs;
using Zzt.Web.MVC.ProductsCatalogue;
using Zzt.Web.MVC.TTAuth;
using log4net;
using CustomerOrder = Zzt.Web.MVC.Models.CustomerOrder;
using OrderLine = Zzt.Web.MVC.Models.OrderLine;
using OrderStatusChange = Zzt.Web.MVC.Models.OrderStatusChange;
using ReturnCode = Zzt.Web.MVC.Obs.ReturnCode;

namespace Zzt.Web.MVC.BusinessLogic
{
    [ContractClass(typeof(CodeContracts.CustomerOrderConverter))]
    public interface ICustomerOrderConverter
    {
        CustomerOrder ToInternalCustomerOrder(Obs.CustomerOrder customerOrder, out string sapCode, ICatalogueService catalogueService);
        Obs.CustomerOrder ToExternalCustomerOrder(CustomerOrder customerOrder, string sapCode, ICatalogueService catalogueService, IOrderService orderService, IAuthenticationService authenticationService, DadataClient dadataClient, ILog logger);
    }

    [ContractClass(typeof(CodeContracts.OrderProcessing))]
    public interface IOrderProcessing : IDisposable
    {
        bool Update(CustomerOrder order, string sapCode, out string message);
        bool Create(CustomerOrder order, string sapCode, out string message);
        CustomerOrder GetOrder(string orderId, out string message, out string sapCode);
        bool UpdateBasket(List<OrderLine> orderLines, string orderId, out string message);
    }

    public class CustomerOrderConverter : ICustomerOrderConverter
    {
        public static readonly ICustomerOrderConverter Instance = new CustomerOrderConverter();

        private CustomerOrderConverter()
        {
            MappingExtensions.MapTypes();
        }

        public CustomerOrder ToInternalCustomerOrder(Obs.CustomerOrder customerOrder, out string sapCode, ICatalogueService catalogueService)
        {
            var result = Mapper.Map<CustomerOrder>(customerOrder);            
            Mapper.Map(customerOrder.Customer, result);
            Mapper.Map(customerOrder.Delivery, result);
            Mapper.Map(customerOrder.OrderInfo, result);
            sapCode = customerOrder.SaleLocation != null ? customerOrder.SaleLocation.SaleLocationId : null;
            Mapper.Map(customerOrder.OrderLines, result.OrderLines);
            if (customerOrder.OrderInfo.OrderStatusHistory != null)
                result.OrderStatusHistory = Mapper.Map<List<OrderStatusChange>>(customerOrder.OrderInfo.OrderStatusHistory.ToList());
            var deliveryTypeNameResult = catalogueService.GetDeliveryTypeDictionary();

            result.CitySelectList = result.CitySelectListItems(catalogueService);
            result.DeliveryTypeList = result.DeliveryTypeSelectListItems(catalogueService);
            result.TimeSlotList = result.TimeSlotSelectListItems(catalogueService);
            var surname = (customerOrder.Customer != null ? customerOrder.Customer.Surname ?? "" : "");
            result.FullName = string.Join(" ", new[] {result.LastName ?? "", string.IsNullOrEmpty(result.FirstName) ? "" : result.FirstName, surname});

            if (!string.IsNullOrEmpty(customerOrder.Delivery.City))
            {
                var cityDictionaryResult = catalogueService.GetCityDictionary();
                if (cityDictionaryResult.ReturnCode == CatalogueService.ReturnCode.Ok && cityDictionaryResult.Dictionary != null && cityDictionaryResult.Dictionary.Any())
                {
                    var city = cityDictionaryResult.Dictionary.FirstOrDefault(p => p.Value == customerOrder.Delivery.City);
                    if (!city.Equals(default(KeyValuePair<string, string>)))
                        result.City = city.Key;
                }
            }

            if (deliveryTypeNameResult.ReturnCode == CatalogueService.ReturnCode.Ok)
            {
                var pair = deliveryTypeNameResult.Dictionary.FirstOrDefault(kvp => kvp.Value == result.DeliveryType);
                if (!pair.Equals(default(KeyValuePair<string, string>)))
                    result.DeliveryType = pair.Key;
            }

            if (customerOrder.Delivery.SubwayStationId.HasValue)
            {
                var subwayDictionary = catalogueService.GetMetroStationDictionary();
                if (subwayDictionary.ReturnCode == CatalogueService.ReturnCode.Ok && subwayDictionary.Dictionary != null && subwayDictionary.Dictionary.Any())
                {
                    var subwayStation = subwayDictionary.Dictionary.FirstOrDefault(p => p.Key == customerOrder.Delivery.SubwayStationId.Value.ToString(System.Globalization.CultureInfo.InvariantCulture));
                    if (!subwayStation.Equals(default(KeyValuePair<string, string>)))
                        result.SubwayStationName = subwayStation.Value;
                }
            }

            result.HasSecondPhone = !string.IsNullOrEmpty(result.OptionalPhones);

            return result;
        }

        public Obs.CustomerOrder ToExternalCustomerOrder(CustomerOrder customerOrder, string sapCode, ICatalogueService catalogueService, IOrderService orderService, IAuthenticationService authenticationService, DadataClient dadataClient, ILog logger)
        {
            if (!customerOrder.IsReceiverNotCustomer)
                customerOrder.RecepientName = null;

            var result = new Obs.CustomerOrder
            {
                Customer = new Customer(),
                Delivery = new DeliveryInfo(),
                OrderInfo = new OrderInfo(),
                OrderLines = new Obs.OrderLine[0],
            };

            if (!string.IsNullOrEmpty(customerOrder.OrderId))
            {
                var storedOrderResponce = orderService.GetOrder(customerOrder.OrderId);
                if (storedOrderResponce.ReturnCode == ReturnCode.Ok && storedOrderResponce.CustomerOrder != null)
                    result = storedOrderResponce.CustomerOrder;
            }
            
            Mapper.Map(customerOrder, result);
            Mapper.Map(customerOrder, result.Customer);
            Mapper.Map(customerOrder, result.Delivery);

            result.Delivery.RequestedDeliveryServiceOption = String.Equals(customerOrder.DeliveryType, "courier", StringComparison.InvariantCultureIgnoreCase) ? customerOrder.NeedLiftToFloor ? customerOrder.HasServiceLift ? "ApartmentWithLift" : "ApartmentWithoutLift" : "DoorstepEmbatted" : "None";

            if (!string.IsNullOrEmpty(sapCode))
                result.SaleLocation = new SaleLocationInfo{SaleLocationId = sapCode,};
            Mapper.Map(customerOrder.OrderLines, result.OrderLines);
            var addressRequest = dadataClient.VerifyCustomer(new CustomerRequest {FIO = customerOrder.FullName,});

            if (addressRequest.Succesfull)
            {
                result.Customer.FirstName = addressRequest.CustomerData.Name;
                result.Customer.LastName = addressRequest.CustomerData.Surname;
            }
            else
                result.Customer.LastName = customerOrder.FullName;

            var deliveryTypeResult = catalogueService.IsSelfCarryDeliveryTypeResult(customerOrder.DeliveryType);
            var deliveryTypeNameResult = catalogueService.GetDeliveryTypeDictionary();
            var isSelfCary = deliveryTypeResult.ReturnCode == CatalogueService.ReturnCode.Ok && deliveryTypeResult.IsSelfCarry;
            if (isSelfCary && !String.IsNullOrEmpty(customerOrder.PickupPointId))
            {
                var pointDictionary = catalogueService.GetPickupLocationV3Dictionary();
                if (pointDictionary.ReturnCode == CatalogueService.ReturnCode.Ok && pointDictionary.PickupLocations != null && pointDictionary.PickupLocations.Any())
                {
                    var pickupPoint = pointDictionary.PickupLocations.FirstOrDefault(p => p.PickupLocationId == customerOrder.PickupPointId);
                    if (!pickupPoint.Equals(default(KeyValuePair<string, string>)))
                        result.Delivery.PickupPointName = pickupPoint.Title;
                }
                result.Delivery.Address
                    = result.Delivery.Building
                        = result.Delivery.House
                            = result.Delivery.Entrance
                                = result.Delivery.EntranceCode
                                    = result.Delivery.Flat
                                        = result.Delivery.Floor
                                            = result.Delivery.Housing
                                                = result.Delivery.Street
                                                    = result.Delivery.SubwayStationName
                                                        = "";
                result.Delivery.FloorTax = 0;
                result.Delivery.SubwayStationId = null;
            }
            else
            {
                result.Delivery.PickupPointId = "0";
                result.Delivery.PickupPointName = "";
            }

            if (deliveryTypeNameResult.ReturnCode == CatalogueService.ReturnCode.Ok)
            {
                var pair = deliveryTypeNameResult.Dictionary.FirstOrDefault(kvp => kvp.Key == result.Delivery.DeliveryType);
                if (!pair.Equals(default(KeyValuePair<string, string>)))
                    result.Delivery.DeliveryType = pair.Value;
            }

            if (result.Delivery.SubwayStationId.HasValue)
            {
                var subwayDictionary = catalogueService.GetMetroStationDictionary();
                if (subwayDictionary.ReturnCode == CatalogueService.ReturnCode.Ok && subwayDictionary.Dictionary != null && subwayDictionary.Dictionary.Any())
                {
                    var subwayStation = subwayDictionary.Dictionary.FirstOrDefault(p => p.Key == result.Delivery.SubwayStationId.Value.ToString(System.Globalization.CultureInfo.InvariantCulture));
                    if (!subwayStation.Equals(default(KeyValuePair<string, string>)))
                        result.Delivery.SubwayStationName = subwayStation.Value;
                }
            }

            if (!string.IsNullOrEmpty(customerOrder.City))
            {
                var cityDictionaryResult = catalogueService.GetCityDictionary();
                if (cityDictionaryResult.ReturnCode == CatalogueService.ReturnCode.Ok && cityDictionaryResult.Dictionary != null && cityDictionaryResult.Dictionary.Any())
                {
                    var city = cityDictionaryResult.Dictionary.FirstOrDefault(p => p.Key == customerOrder.City);
                    if (!city.Equals(default(KeyValuePair<string, string>)))
                        result.Delivery.City = city.Value;
                }
            }
            result.OrderInfo = result.OrderInfo ?? new OrderInfo();
            Mapper.Map(customerOrder, result.OrderInfo);
            try
            {
                var currentUser = authenticationService.GetCurrentUser();
                if (currentUser != null)
                {
                    result.OrderInfo.UpdatedBy = currentUser.LoginName;
                }
            }
            catch (Exception e)
            {
                logger.Error(e);
#if DEBUG
                throw;
#endif
            }

            return result;
        }
    }

    public class OrderProcessing : IOrderProcessing
    {
        private readonly OrderServiceClient _orderService;
        private readonly CatalogueServiceClient _catalogueService;
        private readonly DadataClient _dadataClient;
        private OrdersBackendServiceClient _ordersBackendServiceClient;
        private AuthenticationServiceClient _authenticationServiceClient;

        public CatalogueServiceClient CatalogueService { get { return _catalogueService; } }

        public OrdersBackendServiceClient OrdersBackendServiceClient
        {
            get { return _ordersBackendServiceClient ?? (_ordersBackendServiceClient = new OrdersBackendServiceClient()); }
        }

        private AuthenticationServiceClient AuthenticationService
        {
            get { return _authenticationServiceClient ?? (_authenticationServiceClient = new AuthenticationServiceClient()); }
        }

        public OrderProcessing()
        {
            _orderService = new OrderServiceClient();
            _catalogueService = new CatalogueServiceClient();
            _dadataClient = new DadataClient(Configuration.Instance);
        }

        public CustomerOrder GetOrder(string orderId, out string message, out string sapCode)
        {
            var getOrderOperationResult = _orderService.GetOrder(orderId);
            message = getOrderOperationResult.Message;
            sapCode = null;
            return getOrderOperationResult.ReturnCode == ReturnCode.Ok ? Transform(getOrderOperationResult.CustomerOrder, out sapCode) : null;
        }

        public bool Update(CustomerOrder order, string sapCode, out string message)
        {
            var result = _orderService.UpdateOrder(Transform(order, sapCode));
            message = result.Message;
            return result.ReturnCode == ReturnCode.Ok;
        }

        public bool Create(CustomerOrder order, string sapCode, out string message)
        {
            Contract.Assume(order != null);
            var result = _orderService.CreateOrder(Transform(order, sapCode));
            message = result.Message;
            order.OrderId = result.OrderId;
            return result.ReturnCode == ReturnCode.Ok;
        }

        private CustomerOrder Transform(Obs.CustomerOrder order, out string sapCode)
        {
            return CustomerOrderConverter.Instance.ToInternalCustomerOrder(order, out sapCode, CatalogueService);
        }

        private Obs.CustomerOrder Transform(CustomerOrder order, string sapCode)
        {
            return CustomerOrderConverter.Instance.ToExternalCustomerOrder(order, sapCode, CatalogueService, _orderService, AuthenticationService, _dadataClient, Logger);
        }

        public void Dispose()
        {
            if (_orderService != null)
                try
                {
                    _orderService.Close();
                }
                catch (Exception e)
                {
                    Logger.Error(e);
#if DEBUG
                    throw;
#endif
                }
            if (_catalogueService != null)
                try
                {
                    _catalogueService.Close();
                }
                catch (Exception e)
                {
                    Logger.Error(e);
#if DEBUG
                    throw;
#endif
                }
            if (_ordersBackendServiceClient != null)
                try
                {
                    _ordersBackendServiceClient.Close();
                }
                catch (Exception e)
                {
                    Logger.Error(e);
#if DEBUG
                    throw;
#endif
                }
        }

        private ILog Logger
        {
            get { return LogManager.GetLogger(GetType()); }
        }

        public bool UpdateBasket(List<OrderLine> orderLines, string orderId, out string message)
        {
            var orderResponce = _orderService.GetOrder(orderId);
            message = orderResponce.Message;

            if (orderResponce.ReturnCode == ReturnCode.Error)
                return false;

            if (orderResponce.CustomerOrder == null)
            {
                message = string.Format("Order (ID:{0}) not found!", orderId);
                return false;
            }

            orderResponce.CustomerOrder.OrderLines = Mapper.Map<List<Obs.OrderLine>>(orderLines).ToArray();
            var updatedOrderResponce = _orderService.UpdateOrder(orderResponce.CustomerOrder);
            message = updatedOrderResponce.Message;

            return updatedOrderResponce.ReturnCode != ReturnCode.Error;
        }
    }
}