﻿using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Zzt.Web.MVC.Models;

namespace Zzt.Web.MVC
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : HttpApplication
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Scripts/Dadata")
                            .Include("~/Scripts/Dadata.js")
                            .Include("~/Scripts/keypress-2.0.1.min.js")
                            .Include("~/Scripts/suggestions-jquery-4.1.min.js")
                            );
            bundles.Add(new ScriptBundle("~/Scripts/jquery")
                            .Include("~/Scripts/jquery-1.9.1.min.js"
                                     , "~/Scripts/jquery-{version}.intellisense.js"
                                     , "~/Scripts/jquery.ui/jquery-ui.min.js"
                                     , "~/Scripts/jquery.validate.js"
                                     , "~/Scripts/jquery.validate.unobtrusive.js"
                                     , "~/Scripts/jquery-migrate-1.2.1.min.js"
                                     , "~/Scripts/globalize.js"
                                     , "~/Scripts/globalize.culture.ru-RU.js"));
            bundles.Add(new StyleBundle("~/Content/jquery-ui")
                            .Include("~/Content/themes/base/jquery-ui.css"
                                     , "~/Content/themes/base/jquery.ui.*"));
            bundles.Add(new StyleBundle("~/Content/bootstrap")
                            .Include("~/Content/themes/base/bootstrap-*"
                                     , "~/Content/themes/base/bootstrap.css"));
        }

        private static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        private static void RegisterRoutes(RouteCollection routes)
        {
            GlobalConfiguration.Configuration.Routes.MapHttpRoute("API", "api/{controller}/{id}", new {id = UrlParameter.Optional});

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute("Copy", "Copy/{id}", new { controller = "Orders", action = "Copy", id = UrlParameter.Optional });
            routes.MapRoute("Default", "{controller}/{action}/{id}", new { controller = "Orders", action = "Index", id = UrlParameter.Optional });
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            RegisterBundles(BundleTable.Bundles);
            BundleTable.EnableOptimizations = false;
            ModelBinders.Binders.Add(typeof(CustomerOrder), new CustomerOrderModelBinder());
            ModelBinders.Binders.Add(typeof(EditOrder), new EditOrderModelBinder());
            ModelBinders.Binders.Add(typeof(BasketArticle), new BasketArticleModelBinder());
            log4net.Config.XmlConfigurator.Configure();
        }
    }
}