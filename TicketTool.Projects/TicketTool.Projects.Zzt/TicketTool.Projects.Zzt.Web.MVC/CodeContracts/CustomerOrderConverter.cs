﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using Orders.Integration.Dadata;
using Zzt.Web.MVC.BusinessLogic;
using Zzt.Web.MVC.CatalogueService;
using Zzt.Web.MVC.Obs;
using Zzt.Web.MVC.TTAuth;
using log4net;
using CustomerOrder = Zzt.Web.MVC.Models.CustomerOrder;
using OrderLine = Zzt.Web.MVC.Models.OrderLine;

namespace Zzt.Web.MVC.CodeContracts
{
    [ContractClassFor(typeof(ICustomerOrderConverter)) ]
    public abstract class CustomerOrderConverter : ICustomerOrderConverter
    {
        public CustomerOrder ToInternalCustomerOrder(Obs.CustomerOrder customerOrder, out string sapCode, ICatalogueService catalogueService)
        {
            Contract.Requires<ArgumentNullException>(customerOrder != null);
            Contract.Requires<ArgumentNullException>(catalogueService != null);
            Contract.Ensures(Contract.ValueAtReturn(out sapCode) != string.Empty);
            sapCode = string.Empty;
            return new CustomerOrder();
        }

        public Obs.CustomerOrder ToExternalCustomerOrder(CustomerOrder customerOrder, string sapCode, ICatalogueService catalogueService, IOrderService orderService, IAuthenticationService authenticationService, DadataClient dadataClient, ILog logger)
        {
            Contract.Requires<ArgumentNullException>(customerOrder != null);
            Contract.Requires<ArgumentNullException>(dadataClient != null);
            Contract.Requires<ArgumentNullException>(!string.IsNullOrEmpty(sapCode));
            Contract.Requires<ArgumentNullException>(catalogueService != null);
            Contract.Requires<ArgumentNullException>(orderService != null);
            Contract.Requires<ArgumentNullException>(authenticationService != null);
            return new Obs.CustomerOrder();
        }
    }

    [ContractClassFor(typeof(IOrderProcessing))]
    public abstract class OrderProcessing : IOrderProcessing
    {
        public bool Update(CustomerOrder order, string sapCode, out string message)
        {
            Contract.Requires<ArgumentNullException>(order != null);
            Contract.Requires<ArgumentNullException>(!string.IsNullOrEmpty(order.OrderId));
            Contract.Requires<ArgumentNullException>(!string.IsNullOrEmpty(sapCode));

            message = default(string);
            return default(bool);
        }

        public bool Create(CustomerOrder order, string sapCode, out string message)
        {
            Contract.Requires<ArgumentNullException>(order != null);
            Contract.Requires<ArgumentNullException>(!string.IsNullOrEmpty(sapCode));

            message = default(string);
            return default(bool);
        }

        public CustomerOrder GetOrder(string orderId, out string message, out string sapCode)
        {
            Contract.Requires<ArgumentNullException>(!string.IsNullOrEmpty(orderId));

            sapCode = default(string);
            message = default(string);
            return null;
        }

        public bool UpdateBasket(List<OrderLine> orderLines, string orderId, out string message)
        {
            Contract.Requires<ArgumentNullException>(orderLines != null);
            Contract.Requires<ArgumentNullException>(!string.IsNullOrEmpty(orderId));
            Contract.Requires<ArgumentNullException>(orderLines.Any());

            message = default(string);
            return default(bool);
        }

        public void Dispose()
        {
        }
    }
}