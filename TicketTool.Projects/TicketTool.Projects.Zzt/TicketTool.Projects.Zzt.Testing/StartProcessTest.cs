﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TicketTool.Projects.Zzt.Testing.Services.ZztOrderService;

namespace TicketTool.Projects.Zzt.Testing
{
    [TestClass]
    public class StartProcessTest
    {
        [TestMethod]
        public void StartProcess_ForExistingOrder_StartsCorrectly()
        {
            var client = new ZztOrderProcessingServiceClient();

            client.StartProcess(new StartProcess() {WorkItemId = "R006-3711-5095", ParameterName = "R006"});
        }
    }
}
