﻿using System.Collections.Generic;
using TicketTool.Projects.Zzt.BO.Entities;
using TicketTool.Projects.Zzt.Json;

namespace TicketTool.Projects.Zzt.BO.Json
{
    public class OrdersContent : EntitySet
    {
// ReSharper disable InconsistentNaming
        public List<Order> orders { get; set; }
// ReSharper restore InconsistentNaming
    }
}
