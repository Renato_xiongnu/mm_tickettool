﻿namespace TicketTool.Projects.Zzt.BO.Entities
{
    public class User
    {
// ReSharper disable InconsistentNaming
        public string user_id { get; set; }
        public string user_type_id { get; set; }
        public string login { get; set; }
        public string passwd { get; set; }
        public string cookie { get; set; }
        public string email { get; set; }
        public string nospam { get; set; }
        public string allshops_card { get; set; }
        public string last_name { get; set; }
        public string first_name { get; set; }
        public string middle_name { get; set; }
        public string efirst_name { get; set; }
        public string elast_name { get; set; }
        public string birthday { get; set; }
        public string sex { get; set; }
        public string fio { get; set; }
        public string vip { get; set; }
        public string disabled_delivery_type_ids { get; set; }
        public string enabled_delivery_type_ids { get; set; }
        public string disabled_payment_ids { get; set; }
        public string personal_discount { get; set; }
        public string user_group_id { get; set; }
        public string channels { get; set; }
        public string rapida { get; set; }
        public string ipaddress { get; set; }
        public string bank_phone { get; set; }
        public string is_super_expert { get; set; }
        public UserAddress user_address { get; set; }
        public Organization organization { get; set; }
// ReSharper restore InconsistentNaming
    }
}
