﻿using System;
using System.Collections.Generic;
using TicketTool.Projects.Zzt.BO.Json;
using TicketTool.Projects.Zzt.Json;

namespace TicketTool.Projects.Zzt.BO.Entities
{
    public class Order : EntityBase<Order>
    {
        // ReSharper disable InconsistentNaming
        public string order_id { get; set; }
        public string shop_id { get; set; }
        public string shop_order_id { get; set; }
        public DateTime xtime { get; set; }
        public string discount { get; set; }
        public string discount_type_id { get; set; }
        public string stat_id { get; set; }
        public string order_char { get; set; }
        public string delivery_id { get; set; }
        public string payment_id { get; set; }
        public string vars { get; set; }
        public string delivery_type_id { get; set; }
        public string notes { get; set; }
        public string discount_card_id { get; set; }
        public string ipaddress { get; set; }
        public string office_id { get; set; }
        public string order_status { get; set; }
        public List<Paid> paid { get; set; }
        public List<Carrier> carrier { get; set; }
        public List<Product> products { get; set; }
        public User user { get; set; }
        public string delivery_cost { get; set; }
        public DateTime? del_xtime { get; set; }
        public DateTime? del_xtime2 { get; set; }
        // ReSharper restore InconsistentNaming
        protected override EntitySet Content
        {
            get
            {
                var content = new OrdersContent {orders = new List<Order>()};
                content.orders.Add(this);
                return content;
            }
        }

        protected override string BOEntityName {  get { return "order"; } }        
    }
}
