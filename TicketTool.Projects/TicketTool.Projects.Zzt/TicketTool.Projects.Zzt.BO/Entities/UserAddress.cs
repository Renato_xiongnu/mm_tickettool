﻿namespace TicketTool.Projects.Zzt.BO.Entities
{
    public class UserAddress
    {
// ReSharper disable InconsistentNaming
        public string country_id { get; set; }
        public string postcode { get; set; }
        public string city { get; set; }
        public string street { get; set; }
        public string building { get; set; }
        public string stroenie { get; set; }
        public string block { get; set; }
        public string entrance { get; set; }
        public string floor { get; set; }
        public string apartament { get; set; }
        public string phone { get; set; }
        public string fax { get; set; }
        public string metro { get; set; }
        public string notes { get; set; }
        public string region_id { get; set; }
        public string city_id { get; set; }
        public string code { get; set; }
        public string address { get; set; }
        public string vars { get; set; }
        public string recipient { get; set; }
        public string area { get; set; }
        public string region { get; set; }
        public string is_have_lift { get; set; }
        public string kladr_region_id { get; set; }
        public string kladr_area_id { get; set; }
        public string kladr_city_id { get; set; }
// ReSharper restore InconsistentNaming
    }
}
