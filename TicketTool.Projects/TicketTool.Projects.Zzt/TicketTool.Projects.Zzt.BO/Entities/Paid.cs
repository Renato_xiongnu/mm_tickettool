﻿namespace TicketTool.Projects.Zzt.BO.Entities
{
    public class Paid
    {
// ReSharper disable InconsistentNaming
        public string timestamp { get; set; }
        public string is_canceled { get; set; }
// ReSharper restore InconsistentNaming
    }
}
