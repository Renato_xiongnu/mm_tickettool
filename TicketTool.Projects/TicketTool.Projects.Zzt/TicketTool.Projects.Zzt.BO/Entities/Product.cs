﻿namespace TicketTool.Projects.Zzt.BO.Entities
{
    public class Product
    {
// ReSharper disable InconsistentNaming
        public string product_id { get; set; }
        public string price { get; set; }
        public string quantity { get; set; }
        public string product_name { get; set; }
        public string discount { get; set; }
        public string discount_id { get; set; }
        public string wws_id { get; set; }
// ReSharper restore InconsistentNaming
    }
}
