﻿using System;
using TicketTool.Projects.Zzt.BO.Properties;
using TicketTool.Projects.Zzt.Json;

namespace TicketTool.Projects.Zzt.BO.Entities
{
    public abstract class EntityBase<T> : IJsonDeserializable where T : IJsonDeserializable, new()
    {
        public T Update()
        {
            var executor = new JsonExecutor(Settings.Default.BOUrl, Settings.Default.CertPath);
            executor.AddQuery(this.GetQuery());
            return executor.ExecuteQuery<T>();
        }

        public string ToJson()
        {
            return String.Format("request={0}", JsonQuery.GetJson(this.GetQuery()));
        }

        private JsonQuery GetQuery()
        {
            return new JsonQuery(QueryType.Send, new SendContent {name = this.BOEntityName, content = this.Content});
        }

        protected abstract EntitySet Content { get; }
        protected abstract string BOEntityName { get; }
        string IJsonDeserializable.DeserializationErrors { get; set; }
    }
}
