﻿namespace TicketTool.Projects.Zzt.BO.Entities
{
    public class Organization
    {
// ReSharper disable InconsistentNaming
        public string name { get; set; }
        public string address { get; set; }
        public string inn { get; set; }
        public string kpp { get; set; }
        public string account { get; set; }
        public string bank { get; set; }
        public string bik { get; set; }
        public string okpo { get; set; }
        public string okonh { get; set; }
        public string vars { get; set; }
        public string comment { get; set; }
        public string director { get; set; }
        public string url { get; set; }
        public string corr_acc { get; set; }
// ReSharper restore InconsistentNaming
    }
}
