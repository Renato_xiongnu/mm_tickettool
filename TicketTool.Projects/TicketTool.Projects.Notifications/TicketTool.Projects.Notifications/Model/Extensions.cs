﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using TicketTool.Projects.Notifications.Proxy.Notifications;
using TicketTool.Projects.Notifications.Proxy.Notifications.Metadata;

namespace TicketTool.Projects.Notifications.Model
{
    internal static class Extensions
    {
        public static Event ToModelEvent(this EventInfo eventInfo)
        {
            return new Event
                {
                    Name = eventInfo.EventName,
                    Provider = eventInfo.EventProviderName,
                    Deliveries =
                        new Collection<Delivery>(
                            eventInfo.DeliveryChannels.Select(dch => dch.ToModelDelivery()).ToList())
                };
        }

        public static Delivery ToModelDelivery(this DeliveryInfo deliveryInfo)
        {
            return new Delivery
                {
                    Type = deliveryInfo.DeliveryType.ToModelDeliveryType(),
                    Parameters =
                        new Collection<DeliveryParameter>(
                            deliveryInfo.Parameters.Select(p => p.ToModelDeliveryParameter()).ToList())
                };
        }

        public static DeliveryType ToModelDeliveryType(this Proxy.Notifications.Metadata.DeliveryType deliveryType)
        {
            switch (deliveryType)
            {
                case Proxy.Notifications.Metadata.DeliveryType.Site:
                    return DeliveryType.Site;
                case Proxy.Notifications.Metadata.DeliveryType.EMail:
                    return DeliveryType.EMail;
                case Proxy.Notifications.Metadata.DeliveryType.SMS:
                    return DeliveryType.SMS;
                case Proxy.Notifications.Metadata.DeliveryType.IPhone:
                    return DeliveryType.IPhone;
                default:
                    throw new ArgumentOutOfRangeException("deliveryType");
            }
        }

        public static DeliveryParameter ToModelDeliveryParameter(this DeliveryTypeParameter parameter)
        {
            return new DeliveryParameter
                {
                    Description = parameter.Description,
                    Name = parameter.Name
                };
        }

        public static SendSettings ToSendSettings(this IEnumerable<Delivery> deliveries)
        {
            return new SendSettings
                {
                    DeliveryChannels = deliveries.Select(d => d.ToDeliveryChannel()).ToArray()
                };
        }

        public static DeliveryChannel ToDeliveryChannel(this Delivery delivery)
        {
            return new DeliveryChannel
                {
                    DeliveryType = delivery.Type.ToDeliveryType(),
                    Parameters = delivery.Parameters.ToDictionary(k => k.Name, v => v.Value)
                };
        }

        public static Proxy.Notifications.DeliveryType ToDeliveryType(this DeliveryType deliveryType)
        {
            switch (deliveryType)
            {
                case DeliveryType.Site:
                    return Proxy.Notifications.DeliveryType.Site;
                case DeliveryType.EMail:
                    return Proxy.Notifications.DeliveryType.EMail;
                case DeliveryType.SMS:
                    return Proxy.Notifications.DeliveryType.SMS;
                case DeliveryType.IPhone:
                    return Proxy.Notifications.DeliveryType.IPhone;
                default:
                    throw new ArgumentOutOfRangeException("deliveryType");
            }
        }
    }
}