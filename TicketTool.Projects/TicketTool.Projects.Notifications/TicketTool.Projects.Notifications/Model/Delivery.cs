﻿using System.Collections.ObjectModel;

namespace TicketTool.Projects.Notifications.Model
{
    public class Delivery
    {
        public Delivery()
        {
            this.Parameters = new Collection<DeliveryParameter>();
        }

        public DeliveryType Type { get; set; }
        public Collection<DeliveryParameter> Parameters { get; set; }
    }
}