﻿namespace TicketTool.Projects.Notifications.Model
{
    public class DeliveryParameter
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
    }
}