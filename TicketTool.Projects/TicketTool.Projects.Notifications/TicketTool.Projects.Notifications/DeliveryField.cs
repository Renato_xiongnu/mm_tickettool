﻿using System.Activities;
using System.ComponentModel;
using TicketTool.Projects.Notifications.Model;

namespace TicketTool.Projects.Notifications
{
    public sealed class DeliveryField : CodeActivity
    {
        [Browsable(false)]
        [RequiredArgument]
        public InArgument<Model.Delivery> Delivery { get; set; }

        [Browsable(false)]
        [RequiredArgument]
        public InArgument<string> Value { get; set; }

        [Browsable(false)]
        public string Name { get; set; }

        [Browsable(false)]
        public string Description { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            if (Value == null) return;
            Delivery.Get(context).Parameters.Add(
                new DeliveryParameter {Name = Name, Value = Value.Get(context)});
        }
    }
}