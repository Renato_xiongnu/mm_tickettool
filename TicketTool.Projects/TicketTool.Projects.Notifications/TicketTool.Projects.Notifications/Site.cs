﻿using System.Activities.Presentation;
using System.Windows;
using TicketTool.Projects.Notifications.Model;

namespace TicketTool.Projects.Notifications
{
    public sealed class Site : Delivery, IActivityTemplateFactory<Site>
    {
        public Site Create(DependencyObject target, IDataObject dataObject)
        {
            return new Site
                {
                    DeliveryType = DeliveryType.Site,
                    DeliveryFields = GetDeliveryFields(target, DeliveryType.Site)
                };
        }
    }
}