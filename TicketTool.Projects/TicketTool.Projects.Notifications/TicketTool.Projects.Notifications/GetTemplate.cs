﻿using System;
using System.Activities;
using System.Activities.Presentation;
using System.Activities.Statements;
using System.ComponentModel;
using System.Windows;
using Newtonsoft.Json;
using TicketTool.Activities.Common;
using TicketTool.Projects.Notifications.Design;
using TicketTool.Projects.Notifications.Model;
using TicketTool.Projects.Notifications.Proxy.Notifications;

namespace TicketTool.Projects.Notifications
{
    [Designer(typeof (GetTemplateActivityDesigner))]
    public sealed class GetTemplate : NativeActivity, IActivityTemplateFactory
    {
        private ActivityFunc<object, string> Flow { get; set; }

        [Browsable(false)]
        [RequiredArgument]
        public InArgument<object> Data { get; set; }

        [Browsable(false)]
        [RequiredArgument]
        public OutArgument<string> Document { get; set; }

        [Browsable(false)]
        public Event Event { get; set; }

        [Browsable(false)]
        public string Format { get; set; }

        public string DesignUrl { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            this.CacheArguments(metadata);
            this.CacheFlow(metadata);
        }

        private void CacheFlow(NativeActivityMetadata metadata)
        {
            var data = new DelegateInArgument<object>("Data");
            var document = new DelegateOutArgument<string>("Document");
            var json = new Variable<string>("Json");
            var id = new Variable<long[]>("Id");

            this.Flow = new ActivityFunc<object, string>
                {
                    Argument = data,
                    Result = document,
                    Handler = new Sequence
                        {
                            Variables = {json, id},
                            Activities =
                                {
                                    new Assign
                                        {
                                            To = new OutArgument<string>(json),
                                            Value =
                                                new InArgument<string>(c => JsonConvert.SerializeObject(data.Get(c)))
                                        },
                                    new Failover
                                        {
                                            Delegate = new InvokeDelegate
                                                {
                                                    Delegate = new ActivityAction
                                                        {
                                                            Handler = new Sequence
                                                                {
                                                                    Activities =
                                                                        {
                                                                            new Assign
                                                                                {
                                                                                    To = new OutArgument<long[]>(id),
                                                                                    Value = new InArgument<long[]>(c =>
                                                                                                                   new NotificationServiceClient
                                                                                                                       ()
                                                                                                                       .SendObject
                                                                                                                       (
                                                                                                                           this
                                                                                                                               .Event
                                                                                                                               .Provider,
                                                                                                                           this
                                                                                                                               .Event
                                                                                                                               .Name,
                                                                                                                           new SendSettings
                                                                                                                               {
                                                                                                                                   DeliveryChannels
                                                                                                                                       =
                                                                                                                                       new DeliveryChannel
                                                                                                                               [
                                                                                                                               ]
                                                                                                                                           {
                                                                                                                                           }
                                                                                                                               },
                                                                                                                           json
                                                                                                                               .Get
                                                                                                                               (c)))
                                                                                },
// ReSharper disable ImplicitlyCapturedClosure
                                                                            new If(c => id.Get(c).Length == 0)
// ReSharper restore ImplicitlyCapturedClosure
                                                                                {
                                                                                    Then = new Throw
                                                                                        {
                                                                                            Exception =
                                                                                                new InArgument
                                                                                                    <Exception>(
                                                                                                    c => new Exception(
                                                                                                             String
                                                                                                                 .Format
                                                                                                                 ("No Id returned from Notification Service. Provider: {0}, Name: {1}, Data: {2}",
                                                                                                                  this
                                                                                                                      .Event
                                                                                                                      .Provider,
                                                                                                                  this
                                                                                                                      .Event
                                                                                                                      .Name,
                                                                                                                  json
                                                                                                                      .Get
                                                                                                                      (c))))
                                                                                        }
                                                                                }
                                                                        }
                                                                }
                                                        }
                                                }
                                        },
                                    new DoWhile(c => String.IsNullOrEmpty(document.Get(c)))
                                        {
                                            Body = new Sequence
                                                {
                                                    Activities =
                                                        {
                                                            new Failover
                                                                {
                                                                    Delegate = new InvokeDelegate
                                                                        {
                                                                            Delegate = new ActivityAction
                                                                                {
                                                                                    Handler = new Assign
                                                                                        {
                                                                                            To =
                                                                                                new OutArgument<string>(
                                                                                                    document),
                                                                                            Value =
                                                                                                new InArgument<string>(
                                                                                                    c =>
                                                                                                    new NotificationServiceClient
                                                                                                        ()
                                                                                                        .GetDescriptionValueForSendNotification
                                                                                                        (
                                                                                                            id.Get(c)[0],
                                                                                                            this.Format))
                                                                                        }
                                                                                }
                                                                        }
                                                                },
                                                            new If(c => String.IsNullOrEmpty(document.Get(c)))
                                                                {
                                                                    Then = new Delay
                                                                        {
                                                                            Duration =
                                                                                new InArgument<TimeSpan>(
                                                                                    TimeSpan.FromSeconds(5))
                                                                        }
                                                                }
                                                        }
                                                }
                                        }
                                }
                        }
                };
            metadata.AddDelegate(this.Flow);
        }

        private void CacheArguments(NativeActivityMetadata metadata)
        {
            RuntimeArgument data = new RuntimeArgument("Data", typeof (object),
                                                       ArgumentDirection.In, true);
            metadata.Bind(this.Data, data);
            metadata.AddArgument(data);

            RuntimeArgument document = new RuntimeArgument("Document", typeof (string),
                                                           ArgumentDirection.Out, true);
            metadata.Bind(this.Document, document);
            metadata.AddArgument(document);
        }

        protected override void Execute(NativeActivityContext context)
        {
            context.ScheduleFunc(this.Flow, this.Data.Get(context), this.OnCompleted);
        }

        private void OnCompleted(NativeActivityContext context, ActivityInstance completedinstance, string result)
        {
            if (completedinstance.State == ActivityInstanceState.Canceled) return;
            this.Document.Set(context, result);
        }

        public Activity Create(DependencyObject target)
        {
            return new GetTemplate {DesignUrl = Settings.Default.NotificationsDesignUrl};
        }
    }
}