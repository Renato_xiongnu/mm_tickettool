﻿using System.Activities.Presentation;
using System.Windows;
using TicketTool.Projects.Notifications.Model;

namespace TicketTool.Projects.Notifications
{
    public sealed class Email : Delivery, IActivityTemplateFactory<Email>
    {
        public Email Create(DependencyObject target, IDataObject dataObject)
        {
            return new Email
                {
                    DeliveryType = DeliveryType.EMail,
                    DeliveryFields = GetDeliveryFields(target, DeliveryType.EMail)
                };
        }
    }
}