﻿using System;
using System.Activities;
using System.Activities.Presentation;
using System.Activities.Statements;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Newtonsoft.Json;
using TicketTool.Activities.Common;
using TicketTool.Projects.Notifications.Design;
using TicketTool.Projects.Notifications.Model;
using TicketTool.Projects.Notifications.Proxy.Notifications;

namespace TicketTool.Projects.Notifications
{
    [Designer(typeof (NotificationActivityDesigner))]
    public sealed class Notification : NativeActivity, IActivityTemplateFactory<Notification>
    {
        private Collection<Delivery> _deliveryChannels;
        private ActivityAction<object> Flow { get; set; }

        [Browsable(false)]
        public Collection<Delivery> DeliveryChannels
        {
            get { return this._deliveryChannels ?? (this._deliveryChannels = new Collection<Delivery>()); }
// ReSharper disable ValueParameterNotUsed
            set { _deliveryChannels = value; }
// ReSharper restore ValueParameterNotUsed
        }

        [Browsable(false)]
        [RequiredArgument]
        public InArgument<object> Data { get; set; }

        [Browsable(false)]
        public Event Event { get; set; }

        public string DesignUrl { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            this.CacheArguments(metadata);
            this.Validate(metadata);
            this.CacheFlow(metadata);
        }

        private void CacheFlow(NativeActivityMetadata metadata)
        {
            var channels = new Variable<Collection<Model.Delivery>>("DeliveryChannels");
            var sendResult = new Variable<long[]>("SendResult");
            var json = new Variable<string>("Json");
            var objectArg = new DelegateInArgument<object>("TemplateObject");

            Sequence deliveries = new Sequence();
            foreach (var delivery in this.DeliveryChannels)
            {
                delivery.DeliveryChannels = channels;
                deliveries.Activities.Add(delivery);
            }

            this.Flow = new ActivityAction<object>
                {
                    Argument = objectArg,
                    Handler = new Sequence
                        {
                            Variables = {sendResult, json},
                            Activities =
                                {
                                    new Assign
                                        {
                                            To = new OutArgument<Collection<Model.Delivery>>(channels),
                                            Value =
                                                new InArgument<Collection<Model.Delivery>>(
                                                    c => new Collection<Model.Delivery>())
                                        },
                                    deliveries,
                                    new Assign
                                        {
                                            To = new OutArgument<string>(json),
                                            Value =
                                                new InArgument<string>(
                                                    c => JsonConvert.SerializeObject(objectArg.Get(c)))
                                        },
                                    new Failover
                                        {
                                            Delegate = new InvokeDelegate
                                                {
                                                    Delegate = new ActivityAction
                                                        {
                                                            Handler = new Assign
                                                                {
                                                                    To = new OutArgument<long[]>(sendResult),
                                                                    Value =
                                                                        new InArgument<long[]>(
                                                                            c =>
                                                                            SendObject(
                                                                                this.Event.Provider, this.Event.Name,
                                                                                channels.Get(c).ToSendSettings(),
                                                                                json.Get(c)))
                                                                }
                                                        }
                                                }
                                        }
                                }
                        }
                };
            metadata.AddVariable(channels);
            metadata.AddDelegate(this.Flow);
        }

        private void CacheArguments(NativeActivityMetadata metadata)
        {
            RuntimeArgument data = new RuntimeArgument("Data", typeof (object),
                                                       ArgumentDirection.In, true);
            metadata.Bind(this.Data, data);
            metadata.AddArgument(data);
        }

        private void Validate(NativeActivityMetadata metadata)
        {
            if (this.Event == null)
            {
                metadata.AddValidationError("Template must be chosen");
            }
            else
            {
                if (this.Event.Deliveries.Count == 0)
                {
                    metadata.AddValidationError("No suitable delivery channels were found for chosen event");
                }
                else
                {
                    if (this.DeliveryChannels.Count == 0)
                    {
                        metadata.AddValidationError("At least one delivery channel is required");
                    }
                    else
                    {
                        if (!this.DeliveryChannels.All(d => this.Event.Deliveries.Any(d1 => d1.Type == d.DeliveryType)))
                        {
                            var message = "Only {0} channel is allowed for chosen event";
                            metadata.AddValidationError(String.Format(message,
                                                                      String.Join(", ",
                                                                                  this.Event.Deliveries.Select(
                                                                                      d => d.Type))));
                        }
                    }
                }
            }
        }

        protected override void Execute(NativeActivityContext context)
        {
            context.ScheduleAction(this.Flow, this.Data.Get(context));
        }

        public Notification Create(DependencyObject target, IDataObject dataObject)
        {
            return new Notification {DesignUrl = Settings.Default.NotificationsDesignUrl};
        }

        private long[] SendObject(string eventProvider, string eventName, SendSettings sendSettings, string data)
        {
            var client = new NotificationServiceClient();
            var ids = client.SendObject(
                eventProvider, eventName,
                sendSettings,
                data);
            return ids;
        }
    }
}