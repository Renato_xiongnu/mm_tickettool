﻿using System.Activities.Presentation;
using System.Windows;
using TicketTool.Projects.Notifications.Model;

namespace TicketTool.Projects.Notifications
{
    public sealed class Sms : Delivery, IActivityTemplateFactory<Sms>
    {
        public Sms Create(DependencyObject target, IDataObject dataObject)
        {
            return new Sms
                {
                    DeliveryType = DeliveryType.SMS,
                    DeliveryFields = GetDeliveryFields(target, DeliveryType.SMS)
                };
        }
    }
}