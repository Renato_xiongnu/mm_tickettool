﻿using System;
using TicketTool.Projects.Notifications.Model;

namespace TicketTool.Projects.Notifications.Design
{
    // Interaction logic for DeliveryActivityDesigner.xaml
    public partial class DeliveryActivityDesigner
    {
        public DeliveryActivityDesigner()
        {
            InitializeComponent();
        }

        public string[] Channels
        {
            get { return Enum.GetNames(typeof (DeliveryType)); }
        }
    }
}