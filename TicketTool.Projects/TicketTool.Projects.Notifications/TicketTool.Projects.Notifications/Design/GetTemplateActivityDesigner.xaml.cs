﻿using System.Windows;

namespace TicketTool.Projects.Notifications.Design
{
    // Interaction logic for GetTemplateActivityDesigner.xaml
    public partial class GetTemplateActivityDesigner
    {
        private NotificationEventStrategy _eventStrategy;

        public GetTemplateActivityDesigner()
        {
            InitializeComponent();
            this.Loaded += this.Refresh;
        }

        public NotificationEventStrategy EventStrategy
        {
            get { return _eventStrategy ?? (this._eventStrategy = new NotificationEventStrategy(this)); }
        }

        private void Refresh(object sender, RoutedEventArgs e)
        {
            this.EventStrategy.RefreshEvents();
            this.EventStrategy.RefreshFormats();
        }

        private void RefreshFormats(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            this.EventStrategy.RefreshFormats();
        }
    }
}