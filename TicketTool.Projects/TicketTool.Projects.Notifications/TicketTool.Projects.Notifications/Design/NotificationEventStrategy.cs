﻿using System;
using System.Activities.Presentation;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.Windows;
using TicketTool.Projects.Notifications.Model;
using TicketTool.Projects.Notifications.Proxy.Notifications.Metadata;

namespace TicketTool.Projects.Notifications.Design
{
    public sealed class NotificationEventStrategy : INotifyPropertyChanged
    {
        private readonly ActivityDesigner _designer;
        private ObservableCollection<Event> _events;
        private ObservableCollection<string> _formats;
        private Event _event;
        private string _format;

        public NotificationEventStrategy(ActivityDesigner designer)
        {
            this._designer = designer;
        }

        public ObservableCollection<Event> Events
        {
            get { return this._events; }
            set
            {
                this._events = value;
                this.OnPropertyChanged();
            }
        }

        public ObservableCollection<string> Formats
        {
            get { return this._formats; }
            set
            {
                this._formats = value;
                this.OnPropertyChanged();
            }
        }

        public Event Event
        {
            get { return this._event; }
            set { this.SetValue(ref this._event, value, "Event"); }
        }

        public string Format
        {
            get { return this._format; }
            set { this.SetValue(ref this._format, value, "Format"); }
        }

        private void SetValue<T>(ref T oldValue, T newValue, string propertyName) where T : class
        {
            if (newValue == oldValue) return;
            oldValue = newValue;
            var property = this._designer.ModelItem.Properties[propertyName];
            if (property != null) property.SetValue(newValue);
            this.OnPropertyChanged(propertyName);
        }

        private string DesignTimeServiceUrl
        {
            get
            {
                var property = this._designer.ModelItem.Properties["DesignUrl"];
                if (property != null)
                {
                    if (property.Value != null)
                    {
                        return (string) property.Value.GetCurrentValue();
                    }
                }
                return null;
            }
        }

        internal void RefreshEvents()
        {
            var url = this.DesignTimeServiceUrl;
            if (String.IsNullOrEmpty(url)) return;
            try
            {
                var basicHttpBinding = new BasicHttpBinding
                {
                    MaxBufferPoolSize = 2147483647,
                    MaxReceivedMessageSize = 2147483647,
                    MaxBufferSize = 2147483647
                };
                var client = new RegisterMetaDataServiceClient(basicHttpBinding, new EndpointAddress(url));
                var events = client.GetEvents()
                                   .Select(i => i.ToModelEvent())
                                   .OrderBy(i => i.Provider);
                this.Events = new ObservableCollection<Event>(events);
                this.Event = this.GetSelectedEvent();
            }
            catch (Exception ex)
            {
                if (this.Events != null) this.Events.Clear();
                MessageBox.Show(ex.Message);
            }
        }

        internal void RefreshFormats()
        {
            if (this.Event == null) return;
            var url = this.DesignTimeServiceUrl;
            if (String.IsNullOrEmpty(url)) return;
            try
            {
                var client = new RegisterMetaDataServiceClient(new BasicHttpBinding(), new EndpointAddress(url));
                var formats = client.GetEventPatterns(this.Event.Provider, this.Event.Name).OrderBy(f => f);
                this.Formats = new ObservableCollection<string>(formats);
                this.Format = GetSelectedFormat();
            }
            catch (Exception ex)
            {
                if (this.Formats != null) this.Formats.Clear();
                MessageBox.Show(ex.Message);
            }
        }

        private string GetSelectedFormat()
        {
            var property = this._designer.ModelItem.Properties["Format"];
            if (property == null || property.Value == null) return this.Formats.FirstOrDefault();
            return (string) property.Value.GetCurrentValue();
        }

        private Event GetSelectedEvent()
        {
            var property = this._designer.ModelItem.Properties["Event"];
            if (property == null || property.Value == null) return this.Events.FirstOrDefault();
            var eventInfo = (Event) property.Value.GetCurrentValue();
            return this.Events.FirstOrDefault(e => e.Name == eventInfo.Name && e.Provider == eventInfo.Provider);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}