﻿using System.Windows;

namespace TicketTool.Projects.Notifications.Design
{
    // Interaction logic for NotificationActivityDesigner.xaml
    public partial class NotificationActivityDesigner
    {
        private NotificationEventStrategy _eventStrategy;

        public NotificationActivityDesigner()
        {
            InitializeComponent();
            this.Loaded += this.Refresh;
        }

        public NotificationEventStrategy EventStrategy
        {
            get { return _eventStrategy ?? (this._eventStrategy = new NotificationEventStrategy(this)); }
        }

        private void Refresh(object sender, RoutedEventArgs e)
        {
            this.EventStrategy.RefreshEvents();
        }
    }
}