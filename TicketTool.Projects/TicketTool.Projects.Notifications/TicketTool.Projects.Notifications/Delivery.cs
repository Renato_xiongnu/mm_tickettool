﻿using System;
using System.Activities;
using System.Activities.Statements;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using TicketTool.Projects.Notifications.Design;
using TicketTool.Projects.Notifications.Model;

namespace TicketTool.Projects.Notifications
{
    [Designer(typeof (DeliveryActivityDesigner))]
    public class Delivery : NativeActivity
    {
        protected Collection<DeliveryField> DeliveryFields;
        public ActivityAction<Model.Delivery> Flow { get; set; }

        [Browsable(false)]
        public DeliveryType DeliveryType { get; set; }

        [Browsable(false)]
        public string DeliveryTypeString
        {
            get { return this.DeliveryType.ToString(); }
            set
            {
                if (!Enum.IsDefined(typeof (DeliveryType), value)) return;
                this.DeliveryType = (DeliveryType) Enum.Parse(typeof (DeliveryType), value);
            }
        }

        [Browsable(false)]
        [RequiredArgument]
        public InArgument<Collection<Model.Delivery>> DeliveryChannels { get; set; }

        public Collection<DeliveryField> Fields
        {
            get { return this.DeliveryFields ?? (this.DeliveryFields = new Collection<DeliveryField>()); }
// ReSharper disable ValueParameterNotUsed
            set { }
// ReSharper restore ValueParameterNotUsed
        }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            this.CacheArguments(metadata);
            this.CacheFlow(metadata);
        }

        private void CacheFlow(NativeActivityMetadata metadata)
        {
            var delivery = new DelegateInArgument<Model.Delivery>();
            Sequence fields = new Sequence();
            foreach (var field in this.Fields)
            {
                field.Delivery = delivery;
                fields.Activities.Add(field);
            }
            this.Flow = new ActivityAction<Model.Delivery>
                {
                    Argument = delivery,
                    Handler = fields
                };
            metadata.AddDelegate(this.Flow);
        }

        private void CacheArguments(NativeActivityMetadata metadata)
        {
            var deliveryChannels = new RuntimeArgument("DeliveryChannels",
                                                                   typeof (
                                                                       Collection
                                                                       <
                                                                       Model.Delivery>), ArgumentDirection.In, true);
            metadata.Bind(this.DeliveryChannels, deliveryChannels);
            metadata.AddArgument(deliveryChannels);
        }

        protected override void Execute(NativeActivityContext context)
        {
            var delivery = new Model.Delivery {Type = this.DeliveryType};
            DeliveryChannels.Get(context).Add(delivery);
            context.ScheduleAction(this.Flow, delivery);
        }

        protected static Collection<DeliveryField> GetDeliveryFields(DependencyObject target, DeliveryType deliveryType)
        {
            if (target.DependencyObjectType.Name != typeof (NotificationActivityDesigner).Name)
                return new Collection<DeliveryField>();
            var property = ((NotificationActivityDesigner) target).ModelItem.Properties["Event"];
            if (property != null)
            {
                if (property.Value != null)
                {
                    var delivery =
                        ((Event) property.Value.GetCurrentValue()).Deliveries.FirstOrDefault(d => d.Type == deliveryType);
                    if (delivery != null)
                    {
                        return
                            new Collection<DeliveryField>(
                                delivery.Parameters.Select(
                                    p => new DeliveryField {Name = p.Name, Description = p.Description}).ToList());
                    }
                }
            }
            return new Collection<DeliveryField>();
        }
    }
}