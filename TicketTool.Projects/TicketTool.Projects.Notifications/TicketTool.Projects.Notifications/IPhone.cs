﻿using System.Activities.Presentation;
using System.Windows;
using TicketTool.Projects.Notifications.Model;

namespace TicketTool.Projects.Notifications
{
// ReSharper disable InconsistentNaming
    public sealed class IPhone : Delivery, IActivityTemplateFactory<IPhone>
// ReSharper restore InconsistentNaming
    {
        public IPhone Create(DependencyObject target, IDataObject dataObject)
        {
            return new IPhone
                {
                    DeliveryType = DeliveryType.IPhone,
                    DeliveryFields = GetDeliveryFields(target, DeliveryType.IPhone)
                };
        }
    }
}