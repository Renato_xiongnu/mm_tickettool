﻿$(function () {
    makeDialog("#editBoardMessageDialog", {
        title: 'Объявление',
        position: { my: "top", at: "top" },
        open: function () {
            var self = $(this);
            $(this).parent().find(":button:contains('Сохранить')")
                .prop("disabled", false).removeClass('ui-state-disabled');
            var orderId = $(this).data('orderId');
            $(this).html(generateLoader($(this), null, 50));
            $(this).load(orderId === '' ? $.routeLinks.CreateBoardMessage : $.routeLinks.EditBoardMessage, { orderId: orderId }, function () {
                $.validator.unobtrusive.parse("#editBoardMessageDialog");
                $('form input[id$="ExternalNumber"]', self).rules("add", {
                    remote: {
                        url: $.routeLinks.CheckName,
                        type: "get",
                        data: {
                            name: function () {
                                return $('form input[id$="ExternalNumber"]').val();
                            }
                        }
                    },
                    messages: {
                        remote: "Такой номер объявления уже существует."
                    }
                });
                $('.datepicker', this).datepicker({
                    minDate: new Date(),
                    dateFormat: 'dd.mm.yy',
                    buttonImage: approot + '/Content/Images/calendar.gif',
                    buttonImageOnly: true,
                    showOn: 'both',
                    buttonText: "" 
                });
            });
            
        },
        buttons: {
            "Сохранить": function () {
                var $form = $('form', this);
                if ($form.valid()) {
                    $(this).parent().find(":button:contains('Сохранить')")
                        .prop("disabled", true).addClass('ui-state-disabled');
                    $form.submit();
                }
            },
            "Отмена": function () {
                $(this).dialog("close");
            }
        }
    });
    $(".createBoardMessageLink").click(function () {
        $("#editBoardMessageDialog").data('orderId', '').dialog("open");
    });
    $(".editBoardMessageLink").click(function () {
        var orderId = $(this).attr('data-orderId');
        $("#editBoardMessageDialog").data('orderId', orderId).dialog("open");
    });
});