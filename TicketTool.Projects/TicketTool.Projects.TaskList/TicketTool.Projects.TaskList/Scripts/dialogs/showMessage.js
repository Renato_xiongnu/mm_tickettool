﻿$(function () {
    makeDialog("#showBoardMessageDialog", {
        title: 'Объявление',
        position: { my: "top", at: "top" },
        open: function (event, ui) {
            $(this).html(generateLoader($(this), null, 50));
            $(this).load($.routeLinks.ShowBoardMessage, { orderId: $(this).data('orderId') });
            var isCanceled = $(this).data('isCanceled').toLowerCase() === 'true';
            if (!isCanceled) {
                var buttons = $(this).dialog("option", "buttons");
                $.extend(buttons, {
                    "Редактировать": function () {
                        $(this).dialog("close");
                        $("#editBoardMessageDialog").data('orderId', $(this).data('orderId')).dialog("open");
                    }
                });
                $(this).dialog("option", "buttons", buttons);
            }
            else {
                var buttons = $(this).dialog("option", "buttons");
                delete buttons['Редактировать'];
                $(this).dialog("option", "buttons", buttons);
            }
        },
        buttons: { 
            "OK": function () {
                $(this).dialog("close");
            }
        }
    });
    $(".showBoardMessageLink").click(function () {
        $("#showBoardMessageDialog").data({ 'orderId': $(this).attr('data-orderId'), 'isCanceled': $(this).attr('data-isCanceled') }).dialog("open");
    });
});