﻿function gridInit() {
    refreshGrid();
}
function gridEndCallback() {
    refreshGrid();
}
function gridBeginCallback(s, e) {
    e.customArgs['hideCanceled'] = $('[id$="HideCanceled"]').is(':checked');
}
function refreshGrid() {
    $(".cancelBoardMessageLink").click(function () {
        $("#cancelBoardMessageDialog").data('orderId', $(this).attr('data-orderId')).dialog("open");
    });
    $(".editBoardMessageLink").click(function () {
        var orderId = $(this).attr('data-orderId');
        $("#editBoardMessageDialog").data('orderId', orderId).dialog("open");
    });
    $(".showBoardMessageLink").click(function () {
        $("#showBoardMessageDialog").data({ 'orderId': $(this).attr('data-orderId'), 'isCanceled': $(this).attr('data-isCanceled') }).dialog("open");
    });
    $('.javascriptLink').removeAttr('href');
}
