﻿$(function () {

    $('#clearFilters').on('click', function () {
        $('[id$="HideCanceled"]').attr('checked', false);
        boardMessageGridView.PerformCallback({ filterExpression: null });
    });
    $('[id$="HideCanceled"]').on('click', function () {
        boardMessageGridView.PerformCallback();
    });
});