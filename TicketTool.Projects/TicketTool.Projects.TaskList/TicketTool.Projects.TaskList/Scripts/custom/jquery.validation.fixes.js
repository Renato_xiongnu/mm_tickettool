﻿$.validator.setDefaults({
	ignore: ':hidden, [readonly=readonly]'
});
$.validator.addMethod("date", function (value, element) {
    var shortDateFormat = "dd.MM.yyyy";
	var res = true;
	try {
		res = isValidDate(value);
	} catch (error) {
		res = false;
	}
	return res;
});

$.validator.methods.range = function (value, element, param) {
	var globalizedValue = value.replace(",", ".");
	return this.optional(element) || (globalizedValue >= param[0] && globalizedValue <= param[1]);
};

$.validator.methods.number = function (value, element) {
	return this.optional(element) || /^-?(?:\d+|\d{1,3}(?:[\s\.,]\d{3})+)(?:[\.,]\d+)?$/.test(value);
};