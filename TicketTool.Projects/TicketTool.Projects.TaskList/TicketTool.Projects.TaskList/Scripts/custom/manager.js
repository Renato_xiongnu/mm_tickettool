﻿function generateLoader(container) {
    var height = $(container).height();
    var width = $(container).width()
    if (height === 0) height = 50;
    if (width === 0) width = 50;
    return '<div style="text-align:center;width:' + width + 'px;height:' + height + 'px;"><img src="' + approot + '/Content/Images/ajax-loader.gif"></div>';
}


$(function () {
    $('#StoreSelector').on('change', function () {
        var $container = $(".taskContainer");
        $.ajax({
            type: "get",
            url: $.routeLinks.ManagerReload,
            beforeSend: function () {
                $container.html(generateLoader($($container)));
            },
            data: { id: $(this).val() },
            success: function (response) {
                $container.html(response);
            }
        });
    });
});