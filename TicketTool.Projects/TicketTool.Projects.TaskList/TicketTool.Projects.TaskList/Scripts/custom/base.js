﻿function generateLoader(container, optWidth, optHeight) {
    var height = optHeight === undefined || optHeight === null ? $(container).height() : optHeight;
    var width = optWidth === undefined || optWidth === null ? $(container).width() : optWidth;
    if (height === 0) height = 50;
    if (width === 0) width = 50;
    return '<div style="text-align:center;width:' + width + 'px;height:' + height + 'px;"><img src="' + approot + '/Content/Images/ajax-loader.gif"></div>';
}


String.prototype.endsWith = function (suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};
function cleanInputs(container)
{
    if (container !== undefined) {
        $('input[type!=checkbox]', container).removeAttr('disabled').removeAttr('readonly').val('');
        $('input[type=checkbox]', container).removeAttr('disabled').removeAttr('readonly').removeAttr('checked');
        $('select', container).removeAttr('disabled').removeAttr('readonly').val('');
    }
}

function stringToDate(customDate) {
    if (customDate === undefined || customDate === null) return null;
    var dateParts = customDate.split(".");
    if (dateParts.length != 3) return null;
    return new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);
}
function isValidDate(stringDate) {
    if (stringDate === undefined || stringDate === null) return false;
    var dateParts = stringDate.split(".");
    if (dateParts.length != 3) return false;
    var d = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);
    return d.getFullYear() == dateParts[2] && d.getDate() == dateParts[0];
}


$(function () {
    $('.javascriptLink').removeAttr('href');
});

$.ajaxSetup({
    error: function (event, request, settings) {
        try {
            var jsonText = jQuery.parseJSON(event.responseText);
            window.location.replace($.routeLinks.Error + '?message=' + jsonText.error);
        } catch (e) {
            window.location.replace($.routeLinks.Error + '?message=' + 'javascript error');
        }
    }
});