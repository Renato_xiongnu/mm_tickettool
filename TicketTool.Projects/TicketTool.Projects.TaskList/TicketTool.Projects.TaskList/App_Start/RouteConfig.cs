﻿using System.Web.Mvc;
using System.Web.Routing;

namespace TicketTool.Projects.TaskList
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute("Default", "{controller}/{action}/{id}", new {controller = "Manager", action = "Index", id = UrlParameter.Optional}, new[] {"TicketTool.Projects.RichUI.Controllers"});
        }
    }
}