﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using TicketTool.Projects.Data.Model;

namespace TicketTool.Projects.TaskList.Models.Manager
{
    public class ManagerIndexModel
    {
        private ICollection<Parameter> _parameters;

        public string ParameterName { get; set; }

        public ICollection<Parameter> Parameters
        {
            get { return _parameters??(_parameters = new Collection<Parameter>()); }
            set { _parameters = value; }
        }

        public ICollection<TaskItem> TaskItems { get; set; }
    }
}