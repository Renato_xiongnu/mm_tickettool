﻿using System;
using System.Linq;
using System.Web.Mvc;
using TicketTool.Projects.Data;
using TicketTool.Projects.Data.Model;
using TicketTool.Projects.TaskList.Models.Manager;

namespace TicketTool.Projects.TaskList.Controllers
{
    public class ManagerController : Controller
    {
        private readonly IDataFacade _dataFacade;

        public ManagerController(IDataFacade dataFacade)
        {
            _dataFacade = dataFacade;
        }        

        public ActionResult Index(ManagerIndexModel model)
        {
            var getParametersResult = _dataFacade.Tasks.GetParameters();
            if (getParametersResult.HasErrors)
                throw new InvalidOperationException(getParametersResult.Message);
            model.Parameters = getParametersResult.Item;
            string[] parameters = null;
            if (!string.IsNullOrEmpty(model.ParameterName))
            {                
                model.ParameterName = model.Parameters.Any(p => p.Name == model.ParameterName)
                    ? model.ParameterName
                    : null;
                parameters = new[] { model.ParameterName };
            }                        
            var getTasksResult = _dataFacade.Tasks.GetTasksInProgress(parameters);
            if(getTasksResult.HasErrors)
                throw new InvalidOperationException(getTasksResult.Message);
            model.TaskItems = getTasksResult.Item.ToArray();            
            return View(model);
        }
    }
}
