﻿using System;
using System.Linq;
using System.Web.Mvc;
using TicketTool.Projects.Data;
using TicketTool.Projects.Data.Model;
using TicketTool.Projects.Data.Strategies.Contracts;

namespace TicketTool.Projects.TaskList.Controllers
{
    public class AccountController : Controller
    {
        private readonly IDataFacade _dataFacade;

        public AccountController(IDataFacade dataFacade )
        {
            _dataFacade = dataFacade;
        }

        private IAuthStrategy Auth
        {
            get { return _dataFacade.Auth; }
        }

        [HttpPost]
        public ActionResult Login(string name,string password )
        {
            var result = Auth.Login(name, password, true);
            if(result.HasErrors)
                throw new InvalidOperationException(result.Message);

            return result.Item 
                ? RedirectToAction("CheckUser")
                : (ActionResult)Redirect(Request.UrlReferrer == null ? "/" : Request.UrlReferrer.AbsoluteUri);
        }

        public ActionResult CheckUser()
        {
            var result = Auth.GetCurrentUser();
            if (result.HasErrors)
                throw new InvalidOperationException(result.Message);

            if (result.Item.Roles.All(r => r == "Agent"))
                throw new InvalidOperationException("Воспользуйтесь другим интерфейсом получения задач");

            return RedirectToAction("Index", "Manager");
        }

        public ActionResult Logout()
        {
            Auth.Logout();
            return RedirectToAction("Index", "Manager");
        }
    }
}
