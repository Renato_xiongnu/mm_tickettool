﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TT.LoadTester.Agents;
using TT.LoadTester.Agents.Config;
using TT.LoadTester.Agents.Infrastructure;
using TT.LoadTester.Agents.Orders;

namespace TT.LoadTester
{
    class Program
    {
        internal static void Main()
        {
            log4net.Config.BasicConfigurator.Configure();

            var credentials = new CredentialsProvider().GetCredentials();
            var agentConfig = new AgentsConfigurationProvider().GetAgentConfiguration();
            
            ThrowOnInvalidSetting(credentials,agentConfig);

            var agents =
                credentials.Select(
                    credential => AgenеFactory.Create(credential.Login, credential.Password, credential.Role))
                    .ToList();

            Parallel.ForEach(agents, t => t.Login());

            //Curent params

            foreach (var skillSetConfig in agentConfig.SkillSetConfigs)
            {
                var agentsFor = agents.Take(skillSetConfig.AgentsCount).ToList();
                SkillSetConfig config = skillSetConfig;
                Parallel.ForEach(agentsFor, t => t.LoginToSkillset(config.SkillSetName));
                foreach (var behavior in skillSetConfig.Behaviors)
                {
                    var agentsOutc = agentsFor.Take(behavior.Freq).ToList();
                    Behavior behavior1 = behavior;
                    Parallel.ForEach(agentsOutc,
                        t =>
                        {
                            t.SetBeforeTaskComplete(BeforeTaskCompleteFactory.GetBeforeTaskComplete(behavior1.ActionName));
                            t.StartProcessing(behavior1.Outcome, behavior1.DelayBeforeCompleteTask,
                                behavior1.OutcomeRequiredFields);
                        });
                    agentsFor = agentsFor.Skip(behavior.Freq).ToList();
                }
                agents = agents.Skip(skillSetConfig.AgentsCount).ToList();
            }

            //agents.ForEach(t => t.Logout());
        }

        private static void ThrowOnInvalidSetting(IList<Credentials> credentials, AgentConfiguration agentConfiguration)
        {
            var agentsCount = agentConfiguration.SkillSetConfigs.Sum(t => t.AgentsCount);
            if (credentials.Count != agentsCount)
            {
                throw new ConfigurationErrorsException(string.Format("Credentials count ({0}) must be the same as total agents on skill set count ({1})",
                    credentials.Count, agentsCount));
            }

            var agentsOnSkillSetErrorMessage = new StringBuilder();
            foreach (var skillSetConfig in agentConfiguration.SkillSetConfigs)
            {
                var agentsOnBehavior = skillSetConfig.Behaviors.Sum(t => t.Freq);
                if (skillSetConfig.AgentsCount != agentsOnBehavior)
                {
                    agentsOnSkillSetErrorMessage.AppendLine(
                        string.Format(
                            "Agents on skill set {0} count ({1}) must be the same as total agents on each behavior ({2})",
                            skillSetConfig.SkillSetName, skillSetConfig.AgentsCount, agentsOnBehavior));
                }
            }

            var errorMessage = agentsOnSkillSetErrorMessage.ToString();
            if (!string.IsNullOrEmpty(errorMessage))
            {
                throw new ConfigurationErrorsException(errorMessage);
            }
        }
    }
}
