using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using TT.LoadTester.Agents.Infrastructure;
using TT.LoadTester.Agents.Orders;
using TT.LoadTester.Agents.Proxy.Tt;
using TT.LoadTester.Agents.Proxy.TtAdm;
using TT.LoadTester.Agents.Proxy.TtAuth;
using Cluster = TT.LoadTester.Agents.Proxy.Tt.Cluster;
using SkillSet = TT.LoadTester.Agents.Proxy.Tt.SkillSet;

namespace TT.LoadTester.Agents
{
    public abstract class AgentBase
    {
        protected string _login;
        protected string _password;
        protected TicketToolServiceClient _ticketToolService;
        protected AuthenticationServiceClient _authenticationService;
        protected ITicketToolRestCaller _ticketToolRestCaller;
        protected IBeforeTaskComplete _beforeTaskComplete;
        protected AuthWrapper _auth;
        protected string _currentTaskId { get; set; }
        protected string _skillSetName;
        protected ILog Log;

        protected AgentBase()
        {
            Log = LogManager.GetLogger(GetType().Name);
        }

        public void SetBeforeTaskComplete(IBeforeTaskComplete beforeTaskComplete)
        {
            _beforeTaskComplete = beforeTaskComplete;
        }

        public void Login()
        {
            ExecuteWithTiming("Login",()=>_auth.Login(() => _authenticationService.LogIn(_login, _password, true),_authenticationService.InnerChannel));
        }

        public void Logout()
        {
            ExecuteWithTiming("Logout",
                () => _auth.Execute(() => _authenticationService.LogOut(), _authenticationService.InnerChannel));
        }

        public void LoginToSkillset(string skillSetName)
        {
            var clusters = new TicketToolAdminClient().GetAllClusters();
            _skillSetName = skillSetName;
            ExecuteWithTiming("LoginToSkillSet", () => _auth.Execute(() => _ticketToolService.LogOutAndLogInToSkillSets(
                new[] {new SkillSet {Name = skillSetName}},
                clusters.Select(el => new Cluster {Name = el.Name}).ToArray()), _ticketToolService.InnerChannel));
        }

        public void OpenStatistics()
        {
            while (true)
            {
                var actions=Enumerable.Repeat<Action>(() => 
                ExecuteWithTiming("OpenStatistics",
                    () =>
                        _auth.Execute(() => _ticketToolRestCaller.OpenStatistics(_auth.TicketCookie),
                            _ticketToolService.InnerChannel)),4).ToArray();
                Parallel.Invoke(actions);
                Thread.Sleep(TimeSpan.FromSeconds(60));
            }
        }

        public abstract void StartProcessing(string outcome, double sleepBeforeTask, IDictionary<string,object> requiredFields);

        protected void ExecuteWithTiming(string action, Action func)
        {
            var sw = new Stopwatch();
            LogBefore(action);
            sw.Start();
            try
            {
                func();
                sw.Stop();
                LogAfter(action, true, sw.Elapsed);
            }
            catch (Exception e) 
            {
                sw.Stop();
                LogAfter(action+e.Message,false,sw.Elapsed);
            }
        }

        protected T ExecuteWithTiming<T>(string action, Func<T> func)
        {
            var sw = new Stopwatch();
            LogBefore(action);
            sw.Start();
            T result = default(T);
            try
            {
                result=func();
                sw.Stop();
                LogAfter(action, true, sw.Elapsed);
            }
            catch (Exception e)
            {
                sw.Stop();
                LogAfter(action + e.Message, false, sw.Elapsed);
            }

            return result;
        }

        protected void LogBefore(string action)
        {
            //Log.InfoFormat("{0}: Start executing {1} {2}", _login,action, DateTime.Now);
        }

        protected void LogAfter(string action, bool result, TimeSpan duration)
        {
            //Log.InfoFormat("{0}: Finish executing {1} success:{2} duration:{3} {4}", _login, action, result, duration,
            //    DateTime.Now);
        }
    }
}