﻿using System;
using System.ServiceModel.Description;

namespace TT.LoadTester.Agents.TrackingOperations
{
    public class TrackingOperationBehaviorAttribute : Attribute, IOperationBehavior
    {
        public void AddBindingParameters(OperationDescription operationDescription, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
        {

        }

        public void ApplyClientBehavior(OperationDescription operationDescription, System.ServiceModel.Dispatcher.ClientOperation clientOperation)
        {
            clientOperation.ParameterInspectors.Add(new StopwatchParameterInspector());
        }

        public void ApplyDispatchBehavior(OperationDescription operationDescription, System.ServiceModel.Dispatcher.DispatchOperation dispatchOperation)
        {
            //TrackingOperationInvoker invoker = new TrackingOperationInvoker(dispatchOperation.Invoker);
            //invoker.OnInvokeEnded += (sender, args) => { /* Log the call and execution time */ };
        }

        public void Validate(OperationDescription operationDescription)
        {

        }
    }
}
