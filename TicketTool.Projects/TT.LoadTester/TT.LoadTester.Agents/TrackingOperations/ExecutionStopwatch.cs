﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace TT.LoadTester.Agents.TrackingOperations
{ 
   
    public sealed class ThreadSafeStopwatch : IDisposable
    {
        private Int64 _ticks;

        [ThreadStatic] 
        private static Dictionary<WeakReference, Stopwatch> _threadWatches;

        [ThreadStatic] 
        private static int _cacheCounter;


        public Int64 ElapsedTicks
        {
            get { return _ticks; }
        }

        public Single ElapsedMilliseconds
        {
            get { return (Single)_ticks / TimeSpan.TicksPerMillisecond; }
        }

        
        ~ThreadSafeStopwatch()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        } 

        private void Dispose(Boolean disposing)
        {
            if (!Environment.HasShutdownStarted)
            {
                var watches = _threadWatches;

                var thisRef = new WeakReference(this);

                if (watches != null && watches.ContainsKey(thisRef))
                {
                    watches.Remove(thisRef);
                }
            }
        }

        private const int ThreadCachePruneCount = 100;

        private Stopwatch GetWatch()
        {
            if (_threadWatches == null)
            {
                _threadWatches = new Dictionary<WeakReference, Stopwatch>(WeakRefEqualityComparer.Comparer);
            }
            else if(++_cacheCounter%ThreadCachePruneCount == 0)
            {
                PruneCache();
                _cacheCounter = 0;
            }

            Stopwatch sw; 
            return _threadWatches.TryGetValue(new WeakReference(this), out sw) ? sw : null;
        }



        private static void PruneCache()
        {
            List<WeakReference> toRemoveWrefs = null;

            foreach (WeakReference wr in _threadWatches.Keys)
            {
                if (!wr.IsAlive)
                {
                    if (toRemoveWrefs == null)
                        toRemoveWrefs = new List<WeakReference>();
                    toRemoveWrefs.Add(wr);
                }
            }

            if (toRemoveWrefs != null)
            {
                foreach (var wr in toRemoveWrefs)
                {
                    _threadWatches.Remove(wr);
                }
            }
        }



        [Conditional("DEBUG")]
        public void Start()
        {
            Thread.BeginThreadAffinity();

            Stopwatch sw = GetWatch();

            if (sw == null)
            {

                sw = new Stopwatch();
                _threadWatches.Add(new WeakReference(this), sw);
            }

            if (!sw.IsRunning)
            {
                sw.Start();
            }
        }



        [Conditional("DEBUG")]
        public void Reset()
        { 
            var sw = GetWatch();
            if (sw != null)
                sw.Reset();
            _ticks = 0;
        }



        [Conditional("DEBUG")]
        public void Stop()
        {
            var sw = GetWatch();
            if (sw != null && sw.IsRunning)
            {
                Interlocked.Add(ref _ticks, sw.ElapsedTicks);
                sw.Reset();
                Thread.EndThreadAffinity();
            }
        }



        private class WeakRefEqualityComparer : IEqualityComparer<WeakReference>
        {

            internal static readonly WeakRefEqualityComparer Comparer = new WeakRefEqualityComparer();

            public bool Equals(WeakReference wr1, WeakReference wr2)
            {
                var o1 = wr1.Target;
                var o2 = wr2.Target;
                if (!wr1.IsAlive || !wr2.IsAlive)
                    return false;
                Debug.Assert(o1 != null && o2 != null);
                return o1.Equals(o2); 
            }



            public int GetHashCode(WeakReference wr)
            { 
                object o = wr.Target;
                if (!wr.IsAlive)
                    return 0;
                Debug.Assert(o != null);
                return o.GetHashCode();
            }

        }
    }

}
