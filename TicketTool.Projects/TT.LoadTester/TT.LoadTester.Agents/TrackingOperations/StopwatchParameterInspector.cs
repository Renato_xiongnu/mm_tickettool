﻿using System.ServiceModel.Dispatcher;
using log4net;

namespace TT.LoadTester.Agents.TrackingOperations
{
    public class StopwatchParameterInspector : IParameterInspector
    {
        
        private readonly ILog _logger = LogManager.GetLogger("Timing");

        #region IParameterInspector Members

        public void AfterCall(string operationName, object[] outputs, object returnValue, object correlationState)
        {
            var result = (ThreadSafeStopwatch)correlationState;
            result.Stop();
            _logger.InfoFormat("Finish {0}, elapsed {1}", operationName, result.ElapsedMilliseconds);
            result.Dispose();
        }

        public object BeforeCall(string operationName, object[] inputs)
        {
            var stopwatch = new ThreadSafeStopwatch();
            stopwatch.Start();
            _logger.InfoFormat("Start {0}", operationName);
            return stopwatch;
          
        }

        #endregion
    }                                     
}
