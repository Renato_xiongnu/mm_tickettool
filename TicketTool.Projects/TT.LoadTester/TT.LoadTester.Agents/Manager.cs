﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using TT.LoadTester.Agents.Config;
using TT.LoadTester.Agents.Infrastructure;
using TT.LoadTester.Agents.Orders;
using TT.LoadTester.Agents.Proxy.Tt;
using TT.LoadTester.Agents.Proxy.TtAuth;

namespace TT.LoadTester.Agents
{
    public class Manager : AgentBase
    {
        public Manager(string login, string password, TicketToolServiceClient ticketToolService,
            AuthenticationServiceClient authenticationService, ITicketToolRestCaller ticketToolRestCaller)
        {
            _login = login;
            _password = password;
            _ticketToolService = ticketToolService;
            _authenticationService = authenticationService;
            _ticketToolRestCaller = ticketToolRestCaller;
            _auth = new AuthWrapper();
        }

        public override void StartProcessing(string outcome, double sleepBeforeTask,
            IDictionary<string, object> requiredFields)
        {
            var thread = new Thread(() => Process(outcome, sleepBeforeTask, requiredFields));
            thread.Start();
        }

        private void Process(string outcome, double sleepBeforeTask, IDictionary<string, object> requiredFields)
        {
            while (true)
            {
                Thread.Sleep(TimeSpan.FromMilliseconds(TtConfig.SleepBetweenNextTask));
                var tasks = ExecuteWithTiming("GetManagerTasks",()=>_auth.Execute(() => _ticketToolService.GetMyTasksInProgress(_ticketToolService.GetMyParameters().Select(el=>el.ParameterName).ToArray(),
                    new[] { _skillSetName }), _ticketToolService.InnerChannel));
                
                if (tasks != null && tasks.Any())
                {
                    var taskInProgress =
                        ExecuteWithTiming("GetAllManagerOrders",()=>new Proxy.Orders13.OrderServiceClient().GerOrderDataByOrderIds(
                            tasks.Select(el => el.WorkitemId).ToArray()));

                    var taskListItem =
                        tasks.First();
                    var uri = new Uri(taskListItem.Url);
                    var taskId = HttpUtility.ParseQueryString(uri.Query).Get("taskId");
                    var task = ExecuteWithTiming("GetTask",
                        () => _auth.Execute(() => _ticketToolService.GetTask(taskId), _ticketToolService.InnerChannel));
                    var assigned = ExecuteWithTiming("AssignTaskForMe",
                        () => _auth.Execute(() => _ticketToolService.AssignTaskForMe(taskId),
                            _ticketToolService.InnerChannel)
                        );

                    _currentTaskId = task.TaskId;
                    Thread.Sleep(TimeSpan.FromMilliseconds(sleepBeforeTask));
                    ExecuteWithTiming("CompleteTask",()=>_auth.Execute(() => _ticketToolService.CompleteTask(task.TaskId, outcome,
                        requiredFields != null
                            ? requiredFields.Select(el => new RequiredField
                            {
                                Name = el.Key,
                                Value = el.Value
                            }).ToArray()
                            : null), _ticketToolService.InnerChannel));
                }
            }
        }
    }
}
