﻿using System;
using TT.LoadTester.Agents.Infrastructure;
using TT.LoadTester.Agents.Proxy.Tt;
using TT.LoadTester.Agents.Proxy.TtAuth;
using TT.LoadTester.Agents.TrackingOperations;

namespace TT.LoadTester.Agents
{
    public class AgenеFactory
    {
        public static AgentBase Create(string login,string password,string role)
        {
            switch (role)
            {
                case "Agent":
                    var client = new TicketToolServiceClient();
                    AddOperationBehavior(client);
                    return new Agent(login,password,client,new AuthenticationServiceClient(),new TicketToolRestCaller());
                case "Manager":
                    var client1 = new TicketToolServiceClient();
                    AddOperationBehavior(client1);
                    return new Manager(login, password, client1, new AuthenticationServiceClient(),
                        new TicketToolRestCaller());
                default:
                    throw new ArgumentOutOfRangeException("role");
            }
        }

        private static void AddOperationBehavior(TicketToolServiceClient client)
        {
            foreach (var item in client.ChannelFactory.Endpoint.Contract.Operations)
            {
                item.Behaviors.Add(new TrackingOperationBehaviorAttribute());
            }
        }
    }
}
