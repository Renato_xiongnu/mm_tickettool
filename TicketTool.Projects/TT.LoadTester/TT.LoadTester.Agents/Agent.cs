﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using TT.LoadTester.Agents.Config;
using TT.LoadTester.Agents.Infrastructure;
using TT.LoadTester.Agents.Proxy.Tt;
using TT.LoadTester.Agents.Proxy.TtAuth;

namespace TT.LoadTester.Agents
{
    public class Agent : AgentBase
    {
        public Agent(string login, string password, TicketToolServiceClient ticketToolService,
            AuthenticationServiceClient authenticationService, ITicketToolRestCaller ticketToolRestCaller)
        {
            _login = login;
            _password = password;
            _ticketToolService = ticketToolService;
            _authenticationService = authenticationService;
            _ticketToolRestCaller = ticketToolRestCaller;
            _auth=new AuthWrapper();
        }

        public override void StartProcessing(string outcome, double sleepBeforeTask, IDictionary<string,object> requiredFields)
        {
            var executionThread = new Thread(() => Process(outcome, sleepBeforeTask, requiredFields));
            executionThread.Start();
        }
                                                                       
        private void Process(string outcome, double sleepBeforeTask, IEnumerable<KeyValuePair<string, object>> requiredFields)
        {
            while (true)
            {       
                Thread.Sleep(TimeSpan.FromMilliseconds(TtConfig.SleepBetweenNextTask));
                var task = ExecuteWithTiming("GetNextTask",
                    () => _auth.Execute(() => _ticketToolService.GetNextTask(), _ticketToolService.InnerChannel));
                 
                if (task != null)
                {
                    _currentTaskId = task.TaskId;
                    ExecuteWithTiming("GetOrder", () => _beforeTaskComplete.GetOrderById(task.WorkItemId));
                    Thread.Sleep(TimeSpan.FromMilliseconds(sleepBeforeTask));
                    ExecuteWithTiming("SaveOrder", () => _beforeTaskComplete.UpdateOrder(task.WorkItemId));
                    ExecuteWithTiming("CompleteTask",
                        () => _auth.Execute(() => _ticketToolService.CompleteTask(task.TaskId, outcome,
                            requiredFields != null
                                ? requiredFields.Select(el => new RequiredField
                                {
                                    Name = el.Key,
                                    Value = el.Value
                                }).ToArray()
                                : null), _ticketToolService.InnerChannel));
                }
            }
        }
    }
}
