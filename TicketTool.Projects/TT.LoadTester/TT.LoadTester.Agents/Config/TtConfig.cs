﻿using System.Configuration;

namespace TT.LoadTester.Agents.Config
{
    class TtConfig
    {
        public static double PulseCheck { get { return double.Parse(ConfigurationManager.AppSettings["PulseCheck"]); } }
        public static double SleepBetweenNextTask { get { return double.Parse(ConfigurationManager.AppSettings["SleepBetweenNextTask"]); } }
        public static double CkeckTask { get { return double.Parse(ConfigurationManager.AppSettings["CkeckTask"]); } }
    }
}
