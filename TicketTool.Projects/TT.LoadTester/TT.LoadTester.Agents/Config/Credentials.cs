﻿namespace TT.LoadTester.Agents.Infrastructure
{
    public class Credentials
    {
        public Credentials(string login, string password,string role)
        {
            Login = login;
            Password = password;
            Role = role;
        }

        public string Login { get; private set; }

        public string Password { get; private set; }

        public string Role { get; private set; }
    }
}