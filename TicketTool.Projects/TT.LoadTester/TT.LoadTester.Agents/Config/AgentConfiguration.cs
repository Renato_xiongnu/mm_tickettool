﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TT.LoadTester.Agents.Config
{
    public class AgentConfiguration
    {
        public AgentConfiguration()
        {
            SkillSetConfigs=new List<SkillSetConfig>();
        }

        public IList<SkillSetConfig> SkillSetConfigs { get; set; }
    }

    public class SkillSetConfig
    {
        public SkillSetConfig()
        {
            Behaviors=new List<Behavior>();
        }

        public short AgentsCount { get; set; }

        public string SkillSetName { get; set; }

        public IList<Behavior> Behaviors { get; set; } 
    }

    public class Behavior
    {
        public string Outcome { get; set; }

        public IDictionary<string, object> OutcomeRequiredFields { get; set; } 

        public short Freq { get; set; }

        public double DelayBeforeCompleteTask { get; set; }

        public string ActionName { get; set; }
    }
}
