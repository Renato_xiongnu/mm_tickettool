﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TT.LoadTester.Agents.Orders
{
    public class BeforeTaskCompleteFactory
    {
        public static IBeforeTaskComplete GetBeforeTaskComplete(string actionName)
        {
            switch (actionName)
            {
                case "OrdersBackend13":
                    return new OrdersBackend13();
                case "ManagerOrders":
                    return new ManagerOrders();
                case "OrdersBackend20":
                    return new OrdersBackend20();
                default:
                    return new NoOpBeforeTaskComplete();
            }
        }
    }
}
