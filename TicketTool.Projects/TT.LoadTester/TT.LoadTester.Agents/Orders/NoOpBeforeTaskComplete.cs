﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TT.LoadTester.Agents.Orders
{
    public class NoOpBeforeTaskComplete:IBeforeTaskComplete
    {
        public void GetOrderById(string orderId)
        {
        }

        public void UpdateOrder(string orderId)
        {
        }
    }
}
