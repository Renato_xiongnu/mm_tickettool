﻿namespace TT.LoadTester.Agents.Orders
{
    public interface IBeforeTaskComplete
    {
        void GetOrderById(string orderId);
        void UpdateOrder(string orderId);
    }
}