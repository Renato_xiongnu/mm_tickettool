﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Schema;
using TT.LoadTester.Agents.Proxy.Orders13;

namespace TT.LoadTester.Agents.Orders
{
    public class ManagerOrders : IBeforeTaskComplete
    {
        public void GetOrderById(string orderId)
        {
            var ordersClient = new OrderServiceClient();
            ordersClient.GerOrderDataByOrderId(orderId);
        }

        public void UpdateOrder(string orderId)
        {
            var ordersClient = new Proxy.Orders13.OrderServiceClient();
            var order = ordersClient.GerOrderDataByOrderId(orderId);
            var updateOrderData = new UpdateOrderData
            {
                OrderId = orderId,
                Comment = "ManagerComment",
            };
            var reserveLines = order.ReserveInfo.ReserveLines.ToList();
            reserveLines.ForEach(el =>
            {
                el.ReviewItemState = ReviewItemState.Reserved;
                el.Comment += "ManagerComment";
            });
            var reserveInfo = new ReserveInfo
            {
                ReserveLines = reserveLines.ToArray()
            };

            ordersClient.UpdateOrder(updateOrderData, reserveInfo);
        }
    }
}
