﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TT.LoadTester.Agents.Proxy.Orders13;

namespace TT.LoadTester.Agents.Orders
{
    public class OrdersBackend13 : IBeforeTaskComplete
    {
        public void GetOrderById(string orderId)
        {
            var ordersClient = new Proxy.Orders13.OrderServiceClient();
            ordersClient.GerOrderDataByOrderId(orderId);
        }

        public void UpdateOrder(string orderId)
        {
            var ordersClient = new Proxy.Orders13.OrderServiceClient();
            var updateOrderData = new UpdateOrderData
            {
                ClientData = new ClientData
                {
                    Birthday = DateTime.Now,
                    Email = "test@test.ru",
                    Fax = "123",
                    Gender = "M",
                    LastName = "Иванов",
                    Name = "Иван",
                    Phone = "123",
                    Phone2 = "1234",
                    Surname = "123",
                },
                OrderId = orderId,
                Comment = "Comment",
                DeliveryInfo = new DeliveryInfo
                {
                    HasDelivery = false
                },
                SapCode = GetRandomSapCode()
            };
            
            ordersClient.UpdateOrder(updateOrderData, new ReserveInfo());
        }

        private string GetRandomSapCode()
        {
            var random = new Random();
            switch (random.Next(3))
            {
                case 0:
                    return "R201";
                case 1:
                    return "R202";
                case 2:
                    return "R242";
                default:
                    return "R207";
            }
        }
    }
}
