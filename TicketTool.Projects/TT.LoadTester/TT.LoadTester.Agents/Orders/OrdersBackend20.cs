﻿using System;
using System.Runtime.CompilerServices;
using TT.LoadTester.Agents.Proxy.Orders20;

namespace TT.LoadTester.Agents.Orders
{
    public class OrdersBackend20 : IBeforeTaskComplete
    {
        public void GetOrderById(string orderId)
        {
            var result = new OrderServiceClient().GetOrder(orderId);
            if(result.ReturnCode == ReturnCode.Ok)
                Console.WriteLine(result.OrderId);
        }

        public void UpdateOrder(string orderId)
        {
            var client = new OrderServiceClient();
            var result = client.GetOrder(orderId);
            if (result.ReturnCode == ReturnCode.Ok)
            {
                client.ConfirmOrder(new ConfirmOrderRequest {ConfirmedBy = "LoadTester", OrderId = orderId});
            }
        }
    }
}