﻿using TT.LoadTester.Agents.Proxy.Tt;

namespace TT.LoadTester.Agents
{
    public interface ITicketToolRestCaller
    {
        void Pulse(string taskId, string cookie);
        TicketTask CheckTask(string taskId, string cookie);
        void OpenStatistics(string cookie);
    }
}