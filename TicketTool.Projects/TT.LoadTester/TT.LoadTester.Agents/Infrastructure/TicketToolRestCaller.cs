﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TT.LoadTester.Agents.Proxy.Tt;

namespace TT.LoadTester.Agents.Infrastructure
{
    public class TicketToolRestCaller:ITicketToolRestCaller
    {
        public void Pulse(string taskId, string cookie)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://zztapp-load.mediasaturnrussia.ru/Web/Simple/Pulse");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                client.GetAsync(string.Format("{{taskId={0}}}",taskId)).Wait();
            }
        }

        public TicketTask CheckTask(string taskId,string cookie)
        {
            using (var client = new HttpClient())
            {
                var handler = new HttpClientHandler { CookieContainer = new CookieContainer() };
                var realCookie = cookie.Split(new[] { "=" }, StringSplitOptions.RemoveEmptyEntries);
                handler.CookieContainer.Add(
                    new Uri("http://zztapp-load.mediasaturnrussia.ru/Web/"),
                    new Cookie(realCookie[0], realCookie[1])); // Adding a Cookie

                client.BaseAddress = new Uri("http://zztapp-load.mediasaturnrussia.ru/Web/Simple/CheckTask");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = client.GetAsync(string.Format("{{taskId={0}}}", taskId)).Result;
                TicketTask task = null;
                if (response.IsSuccessStatusCode)
                {
                    task = JsonConvert.DeserializeObject<TicketTask>(response.Content.ReadAsStringAsync().Result);
                }

                return task;
            }
        }

        public async void OpenStatistics(string cookie)
        {
            var handler = new HttpClientHandler {CookieContainer = new CookieContainer()};
            var realCookie = cookie.Split(new[] {"="}, StringSplitOptions.RemoveEmptyEntries);
            handler.CookieContainer.Add(
                new Uri("http://zztapp-load.mediasaturnrussia.ru/Web/"),
                new Cookie(realCookie[0], realCookie[1])); // Adding a Cookie

            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri("http://zztapp-load.mediasaturnrussia.ru/Web/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var currentDate = DateTimeOffset.Now;
                
                var result = await client.GetAsync("");
                Console.WriteLine("Elapsed {0}",  result.Headers.Date.Value.Subtract(currentDate));

                if (!result.IsSuccessStatusCode) throw new Exception(result.ReasonPhrase);
            }
        }
    }
}
