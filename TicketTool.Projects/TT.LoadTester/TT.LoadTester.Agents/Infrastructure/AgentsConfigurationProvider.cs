﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TT.LoadTester.Agents.Config;

namespace TT.LoadTester.Agents.Infrastructure
{
    public class AgentsConfigurationProvider
    {
        private static readonly string _delimeter = "---";

        public AgentConfiguration GetAgentConfiguration()
        {
            var file = File.ReadAllLines("skillset.txt");

            var agentConfiguration = new AgentConfiguration();

            while (file.Any())
            {
                var skillSetConfigLine = file.TakeWhile(el => el != _delimeter).ToArray();
                var skillSetAgensCount = skillSetConfigLine[0].Split(new[] {":"},
                    StringSplitOptions.RemoveEmptyEntries);
                var skillSet = new SkillSetConfig
                {
                    AgentsCount = short.Parse(skillSetAgensCount[1]),
                    SkillSetName = skillSetAgensCount[0]
                };
                agentConfiguration.SkillSetConfigs.Add(skillSet);
                foreach (var configLine in skillSetConfigLine.Skip(1))
                {
                    var scenario = configLine.Trim().Split(new[] {";"}, StringSplitOptions.RemoveEmptyEntries);
                    skillSet.Behaviors.Add(
                        new Behavior
                        {
                            Outcome = scenario[0],
                            Freq = short.Parse(scenario[1]),
                            DelayBeforeCompleteTask = double.Parse(scenario[2]),
                            ActionName = scenario.Length > 3 ? scenario[3] : string.Empty,
                            OutcomeRequiredFields =
                                scenario.Length > 4
                                    ? new Dictionary<string, object>
                                    {
                                        {
                                            scenario[4].Split(new[] {":"}, StringSplitOptions.RemoveEmptyEntries)[0].Replace("##",";#"),
                                            scenario[4].Split(new[] {":"}, StringSplitOptions.RemoveEmptyEntries)[1]
                                        }
                                    }
                                    : null
                        });
                }
                file = file.SkipWhile(el => el != _delimeter).Skip(1).ToArray();
            }

            return agentConfiguration;
        }
    }
}
