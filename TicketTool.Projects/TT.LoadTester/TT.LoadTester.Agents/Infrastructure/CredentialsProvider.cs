﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TT.LoadTester.Agents.Infrastructure
{
    public class CredentialsProvider
    {
        public IList<Credentials> GetCredentials()
        {
            var file = File.ReadAllLines("agents.txt");

            return (from agent in file
                select agent.Trim()
                into agent
                select agent.Split(new string[] {":"}, StringSplitOptions.RemoveEmptyEntries)
                into credentialsString
                select new Credentials(credentialsString[0], credentialsString[1],credentialsString[2])).ToArray();
        }
    }
}
