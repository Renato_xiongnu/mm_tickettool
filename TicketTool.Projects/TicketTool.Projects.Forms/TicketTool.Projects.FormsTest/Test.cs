﻿using System.Globalization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TicketTool.Projects.FormsTest.Proxy.OrdersBackend;

namespace TicketTool.Projects.FormsTest
{
    [TestClass]
    public class Test
    {
        [TestMethod]
        public void TestCreateOrder()
        {
            var result = new OrderServiceClient().CreateOrder(new CustomerOrder
                {
                    Customer = new Customer
                        {
                            FirstName = "Михаил Евграфович",
                            Surname = "Салтыков-Щедрин",
                            Email = "mikhail@saltykov-schedrin.info",
                            Mobile = "+79653222322",
                            Phone = "4993332233"
                        },
                    Delivery = new DeliveryInfo
                        {
                            Address = "Новодмитровская 23к4 стр. 8",
                            City = "Москоу",
                            DeliveryType = "Доставка"
                        },
                    SaleLocation = new SaleLocationInfo {SaleLocationId = "shop_r201"},
                    OrderLines = new[]
                        {
                            new OrderLine
                                {
                                    ArticleNo = 1111111,
                                    OrderLineComment = "Коммент1",
                                    Price = 10,
                                    Quantity = 1
                                },
                            new OrderLine
                                {
                                    ArticleNo = 1111111,
                                    OrderLineComment = "Коммент2",
                                    Price = 20,
                                    Quantity = 2
                                }
                        },
                    WwsOrders = new[]
                        {
                            new WwsOrder
                                {
                                    Id = "2342152363456",
                                    Items = new[]
                                        {
                                            new WwsOrderItem
                                                {
                                                    Article = 1111111,
                                                    Title = "iPhone1",
                                                    Price = 12,
                                                    ReservedQuantity = 1,
                                                },
                                            new WwsOrderItem
                                                {
                                                    Article = 1111111,
                                                    Title = "iPhone2",
                                                    Price = 22,
                                                    ReservedQuantity = 2,
                                                }
                                        }
                                }
                        },
                    OrderInfo = new OrderInfo {InTicketTool = true}
                });

            Assert.AreEqual(result.ReturnCode, ReturnCode.Ok);
        }
    }
}
