﻿using System;
using System.Collections.Generic;
using System.Management.Instrumentation;
using System.Web;
using TicketTool.Projects.Data.Properties;

namespace TicketTool.Projects.Data
{
    public class HttpCache : ICache
    {
        private readonly Dictionary<string, object> _actions;
        private readonly Dictionary<string, DateTime> _cachedTime;

        public HttpCache()
        {
            this._actions = new Dictionary<string, object>();
            this._cachedTime = new Dictionary<string, DateTime>();
        }

        public void Cache<T>(Func<T> action, string key, int ttl)
        {
            this._actions[key] = action;
            this.CacheInternal<T>(key, ttl);
        }

        private T CacheInternal<T>(string key, int ttl)
        {            
            this._cachedTime[key] = DateTime.Now;
            var objectToCache = ((Func<T>)this._actions[key])();
            HttpContext.Current.Cache[key] = new CachedObject<T> { TTL = ttl, Item = objectToCache }; 
            return objectToCache;
        }

        public T GetCachedObject<T>(string key)
        {
            var result = HttpContext.Current.Cache[key] as CachedObject<T>;
            if (result == null)
            {
                if (this._actions.ContainsKey(key))
                {
                    return this.CacheInternal<T>(key, Settings.Default.TTL);
                }
                throw new InstanceNotFoundException();
            }
            return this._cachedTime[key].AddSeconds(result.TTL) < DateTime.Now 
                ? this.CacheInternal<T>(key, result.TTL) 
                : result.Item;
        }
    }
}