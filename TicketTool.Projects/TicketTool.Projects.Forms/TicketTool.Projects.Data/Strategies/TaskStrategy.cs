﻿using System;
using System.Collections.Generic;
using System.Linq;
using TicketTool.Projects.Data.Model;
using TicketTool.Projects.Data.Properties;
using TicketTool.Projects.Data.Proxy.OrderService;
using TicketTool.Projects.Data.Proxy.TicketToolService;
using TicketTool.Projects.Data.Strategies.Contracts;
using Parameter = TicketTool.Projects.Data.Model.Parameter;
using TicketTask = TicketTool.Projects.Data.Model.TicketTask;

namespace TicketTool.Projects.Data.Strategies
{
    public class TaskStrategy : StrategyBase, ITaskStrategy
    {
        private const string TasksInProgressKey = "TicketTool.Projects.Data.Strategies.TasksInProgress";

        public TaskStrategy(IDataFacade dataFacade)
            : base(dataFacade)
        {
            //this.DataFacade.Cache.Cache(() => GetTasksInProgressInternal(), TasksInProgressKey, Settings.Default.TTL_TasksInProgress);
        }

        public OperationResult<TicketTask> GetTask(string id)
        {
            return Try(() => new TicketToolServiceClient().GetTask(id).ToModelTicketTask());
        }

        public OperationResult<bool> PostponeTask(TicketTask task, TimeSpan? ts)
        {
            var result = new TicketToolServiceClient().PostponeTask(task.Id, ts);
            return result.HasError ? new OperationResult<bool>(false, result.MessageText) : new OperationResult<bool>(true);
        }

        public OperationResult<bool> CompleteTask(TicketTask task)
        {
            var result = new TicketToolServiceClient().CompleteTask(task.Id, task.NewOutcome, task.RequestedFields.Select(f => f.ToServerField()).ToArray());
            return result.HasError ? new OperationResult<bool>(false, result.MessageText) : new OperationResult<bool>(true);
        }

        public OperationResult<IEnumerable<TaskItem>> GetTasksInProgress(string[] parameters)
        {
            return GetTasksInProgressInternal(parameters);
            // return this.DataFacade.Cache.GetCachedObject<OperationResult<IEnumerable<TaskItem>>>(TasksInProgressKey);
        }

        public OperationResult<Parameter[]> GetParameters()
        {
            var result = Try(InternalGetMyParameters);
            return result;
        }

        private static Parameter[] InternalGetMyParameters()
        {
            return new TicketToolServiceClient()
                .GetMyParameters()
                .Select(p => new Parameter {Name = p.ParameterName, Title = p.Name})
                .ToArray();
        }

        private  OperationResult<IEnumerable<TaskItem>> GetTasksInProgressInternal(string[] parameters)
        {
            var moscow = TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time");

            var tasksResult = Try(() => new TicketToolServiceClient().GetMyTasksInProgress(parameters, null));
            if (tasksResult.HasErrors)
                return new OperationResult<IEnumerable<TaskItem>>(null, tasksResult.Message);

            var ordersResult = DataFacade.Orders.GetOrdersByIds(tasksResult.Item.Select(m => m.WorkitemId).ToArray());
            if (ordersResult.HasErrors)
                return new OperationResult<IEnumerable<TaskItem>>(null, ordersResult.Message);

            var taskItems = tasksResult.Item.Select(t => new {Task = t, Order = ordersResult.Item.SingleOrDefault(z => z.OrderId == t.WorkitemId)})
                .Where(t=>t.Order!=null)
                                       .Select(x => new TaskItem
                                           {
                                               Name = x.Task.Name,
                                               CreationDate = TimeZoneInfo.ConvertTimeFromUtc(x.Task.CreationDate, moscow),
                                               DeadlineDate = TimeZoneInfo.ConvertTimeFromUtc(x.Task.CreationDate + (x.Task.MaxRunTime ?? new TimeSpan()), moscow),
                                               Url = x.Task.Url,
                                               IsAssignedOnMe = x.Task.IsAssignedOnMe,
                                               AssignedOn = x.Task.AssignedOn,
                                               Escaleted = x.Task.Escalated,
                                               OrderId = x.Order.OrderId, 
                                               WWSOrderId = x.Order != null ? x.Order.WwsOrderId : String.Empty,
                                               Store = x.Order != null ? new StoreItem
                                               {
                                                   Name = x.Order.PickupInfo.SapCode, 
                                                   SapCode = x.Order.PickupInfo.SapCode
                                               } : new StoreItem()
                                           })
                                       .OrderBy(t => t.CreationDate)
                                       .ToList();

            return new OperationResult<IEnumerable<TaskItem>>(taskItems);
        }
    }
}