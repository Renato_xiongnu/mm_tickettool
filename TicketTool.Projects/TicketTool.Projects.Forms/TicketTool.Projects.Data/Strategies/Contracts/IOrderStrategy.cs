﻿using System.Collections.Generic;
using TicketTool.Projects.Data.Model;

namespace TicketTool.Projects.Data.Strategies.Contracts
{
    public interface IOrderStrategy
    {
        OperationResult<CustomerOrder> GetOrder(string id);
        OperationResult<IEnumerable<CustomerOrder>> GetOrdersByIds(string[] id);
        OperationResult<CustomerOrder> UpdateOrder(CustomerOrder order);
        OperationResult<CustomerOrder> CreateTestOrder();
    }
}
