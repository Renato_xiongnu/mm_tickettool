﻿using TicketTool.Projects.Data.Model;

namespace TicketTool.Projects.Data.Strategies.Contracts
{
    public interface IAuthStrategy
    {
        OperationResult<bool> Login(string userName, string password, bool remeberMe);
        OperationResult<bool> LoginByTokens(string userName, string userToken, string taskId, string taskToken);
        OperationResult<User> GetCurrentUser();
        void Logout();
    }
}
