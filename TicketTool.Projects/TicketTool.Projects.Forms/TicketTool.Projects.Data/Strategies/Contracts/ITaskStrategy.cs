﻿using System;
using System.Collections.Generic;
using TicketTool.Projects.Data.Model;

namespace TicketTool.Projects.Data.Strategies.Contracts
{
    public interface ITaskStrategy
    {
        OperationResult<TicketTask> GetTask(string id);
        OperationResult<bool> PostponeTask(TicketTask task, TimeSpan? ts);
        OperationResult<bool> CompleteTask(TicketTask task);

        OperationResult<IEnumerable<TaskItem>> GetTasksInProgress(string[] parameters);

        OperationResult<Parameter[]> GetParameters();
    }    
}
