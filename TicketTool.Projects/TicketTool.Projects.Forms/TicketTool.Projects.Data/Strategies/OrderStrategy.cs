﻿using System;
using System.Collections.Generic;
using System.Linq;
using TicketTool.Projects.Data.Model;
using TicketTool.Projects.Data.Proxy.OrderService;
using TicketTool.Projects.Data.Strategies.Contracts;
using CustomerOrder = TicketTool.Projects.Data.Model.CustomerOrder;

namespace TicketTool.Projects.Data.Strategies
{
    public class OrderStrategy : StrategyBase, IOrderStrategy
    {
        public OrderStrategy(IDataFacade dataFacade) : base(dataFacade)
        {
        }

        public OperationResult<CustomerOrder> GetOrder(string id)
        {
            var result = new OrderServiceClient().GetOrder(id);
            return result.ReturnCode == ReturnCode.Error
                       ? new OperationResult<CustomerOrder>(result.CustomerOrder.ToModelOrder(), result.Message)
                       : new OperationResult<CustomerOrder>(result.CustomerOrder.ToModelOrder());
        }

        public OperationResult<IEnumerable<CustomerOrder>> GetOrdersByIds(string[] id)
        {
            var result = new OrderServiceClient().GetOrdersByIds(id);
            return result.ReturnCode == ReturnCode.Error
                       ? new OperationResult<IEnumerable<CustomerOrder>>(result.CustomerOrders.Select(o => o.ToModelOrder()), result.Message)
                       : new OperationResult<IEnumerable<CustomerOrder>>(result.CustomerOrders.Select(o => o.ToModelOrder()));
        }

        public OperationResult<CustomerOrder> UpdateOrder(CustomerOrder order)
        {
            var serviceOrder = order.ToServiceOrder();
            var client = new OrderServiceClient();
            var result = String.IsNullOrEmpty(order.OrderId) ? client.CreateOrder(serviceOrder) : client.UpdateOrder(serviceOrder);
            return (result.ReturnCode == ReturnCode.Error) 
                ? new OperationResult<CustomerOrder>(order, result.Message)
                : new OperationResult<CustomerOrder>(order);
        }

        public OperationResult<CustomerOrder> CreateTestOrder()
        {
            return new OperationResult<CustomerOrder>(
                new CustomerOrder
                    {
                        BasketArticles = new List<ArticleLine>
                            {
                                new ArticleLine
                                    {
                                        Description = "",
                                        ArticleNum = "1161629",
                                        Price = 5199,
                                        Qty = 1
                                    },
                                new ArticleLine
                                    {
                                        Description = "",
                                        ArticleNum = "1188761",
                                        Price = 27999,
                                        Qty = 1
                                    }
                            }
                    });
        }
    }
}