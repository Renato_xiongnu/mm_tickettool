﻿using TicketTool.Projects.Data.Model;
using TicketTool.Projects.Data.Proxy.TicketToolAuth;
using TicketTool.Projects.Data.Strategies.Contracts;

namespace TicketTool.Projects.Data.Strategies
{
    internal class AuthStrategy : StrategyBase, IAuthStrategy
    {
        public AuthStrategy(IDataFacade dataFacade) : base(dataFacade)
        {
        }

        public OperationResult<bool> Login(string userName, string password, bool remeberMe)
        {
            return Try(() => new AuthenticationServiceClient().LogIn(userName, password, remeberMe));
        }

        public OperationResult<bool> LoginByTokens(string userName, string userToken, string taskId, string taskToken)
        {
            return Try(() => new AuthenticationServiceClient().LoginByToken(userName, userToken, taskId, taskToken));
        }

        public OperationResult<User> GetCurrentUser()
        {
            return Try(() => new AuthenticationServiceClient().GetCurrentUser().ToModelUser());
        }

        public void Logout()
        {
            new AuthenticationServiceClient().LogOut();
        }
    }
}