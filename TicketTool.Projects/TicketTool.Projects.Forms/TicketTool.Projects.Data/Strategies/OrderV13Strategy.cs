using System;
using System.Collections.Generic;
using System.Linq;
using TicketTool.Projects.Data.Model;
using TicketTool.Projects.Data.Proxy.OrderService.v1_3;
using TicketTool.Projects.Data.Strategies.Contracts;
using CustomerOrder = TicketTool.Projects.Data.Model.CustomerOrder;
using OrderServiceClient =TicketTool.Projects.Data.Proxy.OrderService.v1_3.OrderServiceClient;
using ReserveLine = TicketTool.Projects.Data.Model.ReserveLine;

namespace TicketTool.Projects.Data.Strategies
{
    public class OrderV13Strategy : StrategyBase, IOrderStrategy
    {
        public OrderV13Strategy(IDataFacade dataFacade)
            : base(dataFacade)
        {
        }

        public OperationResult<CustomerOrder> GetOrder(string id)
        {
            using(var client = new OrderServiceClient())
            {
                var result = client.GerOrderDataByOrderId(id);                
                return result.ReturnCode == ReturnCode.Error
                    ? new OperationResult<CustomerOrder>(ToModelOrder(result), result.ErrorMessage)
                    : new OperationResult<CustomerOrder>(ToModelOrder(result));
            }
        }

        public OperationResult<IEnumerable<CustomerOrder>> GetOrdersByIds(string[] ids)
        {
            using (var client = new OrderServiceClient())
            {
                var result = client.GerOrderDataByOrderIds(ids);
                return new OperationResult<IEnumerable<CustomerOrder>>(result.Select(ToModelOrder));                
            }
                      
        }

        public OperationResult<CustomerOrder> UpdateOrder(CustomerOrder order)
        {
            throw new NotImplementedException();
        }

        public OperationResult<CustomerOrder> CreateTestOrder()
        {
            throw new NotImplementedException();
        }
       

        public static CustomerOrder ToModelOrder(GerOrderDataResult result)
        {
            if (result == null)
                return new CustomerOrder();
            var orderInfo = result.OrderInfo;
            var deliveryInfo = result.DeliveryInfo;
            var clientData = result.ClientData;
            var wwsOrder = result.ReserveInfo;

            return new CustomerOrder
            {
                OrderId = orderInfo.OrderId,
                Customer =
                {
                    FirstName = clientData.Name,
                    Surname = clientData.Surname,
                    Email = clientData.Email,
                    MainPhone = clientData.Phone,
                    OptionalPhones = clientData.Phone2
                },
                PickupInfo =
                {
                    Address = deliveryInfo.Address,
                    City = deliveryInfo.City,
                    SapCode = String.IsNullOrEmpty(orderInfo.SapCode) ? String.Empty : orderInfo.SapCode.ToUpper(),                    
                    PickupType = new PickupInfo().PickupTypes.First(t => deliveryInfo.HasDelivery ? t.Value == "1" : t.Value == "2") .Value                   
                },
                ReadOnly = false, //TODO: Update when service will support permission assigning
                BasketArticles = result.ArticleLines
                                      .Select(l => new ArticleLine
                                      {
                                          ArticleNum = l.ArticleNum,                                          
                                          Price = l.Price ,
                                          Qty = l.Qty,
                                      })
                                      .ToList(),
                WwsOrderId = orderInfo.WWSOrderId,
                ReserveLines = wwsOrder == null
                                   ? new List<ReserveLine>()
                                   : wwsOrder.ReserveLines.Select(i => new ReserveLine
                                   {
                                       ArticleNum = i.ArticleData.ArticleNum,                                       
                                       Qty = i.WWSReservedQty,
                                       Price = i.ArticleData.Price,
                                       Title = i.Title,
                                       Department = i.WWSDepartmentNumber,
                                       FreeQty = i.WWSFreeQty,
                                       HasShippedFromStock = i.HasShippedFromStock,
                                       Position = i.LineNumber,
                                       PriceOrig = i.WWSPriceOrig,
                                       ProductGroup = i.WWSPositionNumber,
                                       ProductGroupName = i.WWSProductGroupName
                                   }).ToList()
            };
        }
    }
}