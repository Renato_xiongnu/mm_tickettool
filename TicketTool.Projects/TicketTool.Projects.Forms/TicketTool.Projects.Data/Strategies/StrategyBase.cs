﻿using System;

namespace TicketTool.Projects.Data.Strategies
{
    public abstract class StrategyBase
    {
        protected StrategyBase(IDataFacade dataFacade)
        {
            DataFacade = dataFacade;
        }

        public IDataFacade DataFacade { get; private set; }

        protected static OperationResult<T> Try<T>(Func<T> func)
        {
            try
            {
                return new OperationResult<T>(func());
            }
            catch (Exception e)
            {
                return new OperationResult<T>(default(T), e.Message);
            }
        }
    }
}
