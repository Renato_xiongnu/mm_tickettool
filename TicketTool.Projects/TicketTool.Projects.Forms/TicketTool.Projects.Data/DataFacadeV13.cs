﻿using TicketTool.Projects.Data.Strategies;
using TicketTool.Projects.Data.Strategies.Contracts;

namespace TicketTool.Projects.Data
{
    public class DataFacadeV13 : IDataFacade
    {
        private ITaskStrategy _tasks;

        private IOrderStrategy _orders;

        private IAuthStrategy _auth;

        public DataFacadeV13(ICache cache)
        {
            Cache = cache;
        }

        public ICache Cache { get; private set; }

        public ITaskStrategy Tasks
        {
            get { return _tasks ?? (_tasks = new TaskStrategy(this)); }
        }

        public IOrderStrategy Orders
        {
            get { return _orders ?? (_orders = new OrderV13Strategy(this)); }
        }

        public IAuthStrategy Auth
        {
            get { return _auth ?? (_auth = new AuthStrategy(this)); }
        }
    }
}