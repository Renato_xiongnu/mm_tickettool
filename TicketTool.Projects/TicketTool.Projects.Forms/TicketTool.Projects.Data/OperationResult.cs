﻿namespace TicketTool.Projects.Data
{
    public class OperationResult<T>
    {
        public OperationResult(T item)
        {
            this.Item = item;
        }

        public OperationResult(T item, string message)
            : this(item)
        {
            this.HasErrors = true;
            this.Message = message;
        }

        public bool HasErrors { get; private set; }
        public string Message { get; private set; }
        public T Item { get; private set; }
    }
}