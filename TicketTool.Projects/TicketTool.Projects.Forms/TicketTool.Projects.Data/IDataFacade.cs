﻿using TicketTool.Projects.Data.Strategies.Contracts;

namespace TicketTool.Projects.Data
{
    public interface IDataFacade
    {
        ICache Cache { get; }
        ITaskStrategy Tasks { get; }
        IOrderStrategy Orders { get; }
        IAuthStrategy Auth { get; }
    }
}