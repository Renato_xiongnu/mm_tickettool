﻿using System.ComponentModel.DataAnnotations;

namespace TicketTool.Projects.Data.Model
{
    public class Customer
    {
        [Display(Name = "Фамилия заказчика")]
        [Required(ErrorMessage = "Поле '{0}' не заполнено.")]
        public string Surname { get; set; }

        [Display(Name = "Имя заказчика")]
        [Required(ErrorMessage = "Поле '{0}' не заполнено.")]
        public string FirstName { get; set; }

        [Display(Name = "E-mail")]
        [Required(ErrorMessage = "Поле '{0}' не заполнено.")]
        [RegularExpression(@"^[^<>\s\@]+(\@[^<>\s\@]+(\.[^<>\s\@]+)+)$", ErrorMessage = "Неверный email.")]
        public string Email { get; set; }

        [Display(Name = "Основной телефон")]
        [Required(ErrorMessage = "Поле '{0}' не заполнено.")]
        public string MainPhone { get; set; }

        [Display(Name = "Дополнительные телефоны")]
        public string OptionalPhones { get; set; }
    }
}