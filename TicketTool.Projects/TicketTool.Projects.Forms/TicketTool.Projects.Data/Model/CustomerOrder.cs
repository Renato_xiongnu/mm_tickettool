﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace TicketTool.Projects.Data.Model
{
    public class CustomerOrder
    {
        public CustomerOrder()
        {
            PickupInfo = new PickupInfo();
            Customer = new Customer();
            BasketArticles = new List<ArticleLine>();
            NewArticles = new List<ArticleLine>();
        }
        
        [UIHint("Controls/DeliveryInfo")]
        public PickupInfo PickupInfo { get; private set; }
        
        [UIHint("Controls/Customer")]
        public Customer Customer { get; private set; }

        [UIHint("Controls/BasketArticleLine")]
        public List<ArticleLine> BasketArticles { get; set; }

        [UIHint("Controls/ReserveLine")]
        public List<ReserveLine> ReserveLines { get; set; }

        [UIHint("Controls/ArticleLine")]
        public List<ArticleLine> NewArticles { get; private set; }

        public IEnumerable<ArticleLine> AllArticles
        {
            get
            {
                return BasketArticles.Concat(NewArticles);
            }
        }

        [Display(Name = "Номер заказа")]
        public string OrderId { get; set; }

        public bool HasArticles
        {
            get
            {
                return AllArticles.Any();
            }
        }

        public bool IsApproved { get; set; }

        public decimal TotalPrice
        {
            get { return BasketArticles.Sum(m => m.Price*m.Qty); }
        }

        public decimal TotalReservePrice
        {
            get { return !String.IsNullOrEmpty(this.WwsOrderId) ? this.ReserveLines.Sum(i => i.Price*i.Qty) : default(decimal); }
        }

        public bool ReadOnly { get; set; }

        public string WwsOrderId { get; set; }
    }
}