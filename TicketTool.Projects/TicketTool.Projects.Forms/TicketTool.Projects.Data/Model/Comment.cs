﻿using System;

namespace TicketTool.Projects.Data.Model
{
    public class Comment
    {
        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public string Text { get; set; }
    }
}