﻿using System.Collections.Generic;

namespace TicketTool.Projects.Data.Model
{
    public class TicketTask
    {
        public string Id { get; set; }
        public string WorkitemId { get; set; }
        public string NewOutcome { get; set; }
        public List<RequiredField> RequestedFields { get; set; }
        public List<Outcome> Outcomes { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}