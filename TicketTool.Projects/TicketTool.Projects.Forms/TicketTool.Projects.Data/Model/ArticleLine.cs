﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace TicketTool.Projects.Data.Model
{
    public class ArticleLine
    {
        public ArticleLine()
        {
            Qty = 1;
            this.Comments = new List<Comment>();
        }

        [Display(Name="Артикул")]
        public string ArticleNum { get; set; }

        [Display(Name = "Цена")]
        public decimal Price { get; set; }

        [Display(Name = "Количество")]
        public int Qty { get; set; }

        [Display(Name = "Комментарии")]
        public string Comment
        {
            get
            {
                return JsonConvert.SerializeObject(this.Comments);
            }
            set
            {
                try
                {
                    this.Comments = JsonConvert.DeserializeObject<List<Comment>>(value);
                }
                // ReSharper disable EmptyGeneralCatchClause
                catch
                // ReSharper restore EmptyGeneralCatchClause
                {                    
                }

            }
        }

        [Display(Name = "Новый комментарий")]
        [DataType(DataType.MultilineText)]
        public string NewComment { get; set; }

        public bool IsWrong { get; set; }

        public bool IsDataWrong
        {
            get
            {
                return String.IsNullOrEmpty(ArticleNum) || Price <= 0 || Qty <= 0;
            }
        }

        public string ErrorMessage
        {
            get
            {
                if (IsDataWrong)
                {
                    if (String.IsNullOrEmpty(ArticleNum))
                        return "Не задан номер артикула";
                    if (Qty <= 0)
                        return "Не задано количество";
                }
                else if (IsWrong)
                {
                    return "Неправильный артикул";
                }
                return String.Empty;
            }
        }

        public string Description { get; set; }

        public List<Comment> Comments { get; private set; }
        public string Title { get; set; }
    }
}