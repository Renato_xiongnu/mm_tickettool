﻿using System.Web.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TicketTool.Projects.Data.Common;

namespace TicketTool.Projects.Data.Model
{
    public class PickupInfo
    {
        [Display(Name = "Адрес")]
        [RequiredIf("HasDelivery", true, ErrorMessage = "Поле 'Адрес' не заполнено")] //TODO RequiredIf не делает корректный Format
        public string Address { get; set; }

        [Display(Name = "Город")]
        [RequiredIf("HasDelivery", true, ErrorMessage = "Поле 'Город' не заполнено")]
        public string City { get; set; }

        [Display(Name = "Дата доставки")]
        public DateTime DeliveryDate { get; set; }

        [Display(Name = "Способ получения")]
        public string PickupType { get; set; }

        public IEnumerable<SelectListItem> PickupTypes
        {
            get
            {
                var selectListItems = new List<SelectListItem>
                    {
                        new SelectListItem{Text = "Самовывоз",Value = "1",Selected = !HasDelivery},
                        new SelectListItem{Text = "Доставка",Value = "2",Selected = HasDelivery}
                    };

                return selectListItems;
            }
        }

        [Display(Name="Магазин")]
        public string SapCode { get; set; }

        public bool HasDelivery
        {
            get { return PickupType == "2"; }
        }
    }
}