﻿namespace TicketTool.Projects.Data.Model
{
    public class User
    {
        public string Login { get; set; }
        public string Email { get; set; }
        public string[] Roles { get; set; }
        public string[] Parameters { get; set; }
        public bool Authorized { get; set; }
    }
}