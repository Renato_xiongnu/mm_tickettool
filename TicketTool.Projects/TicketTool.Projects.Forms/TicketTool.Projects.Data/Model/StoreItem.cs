﻿namespace TicketTool.Projects.Data.Model
{
    public class StoreItem
    {
        public string SapCode { get; set; }
        public string Name { get; set; }
    }
}