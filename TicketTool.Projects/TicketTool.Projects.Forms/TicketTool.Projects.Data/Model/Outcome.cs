﻿using System.Collections.Generic;

namespace TicketTool.Projects.Data.Model
{
    public class Outcome
    {
        public bool IsSuccess { get; set; }
        public string Value { get; set; }
        public List<RequiredField> RequiredFields { get; set; }
        public bool SkipTaskRequeriedFields { get; set; }
    }
}