﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using TicketTool.Projects.Data.Proxy.OrderService;

namespace TicketTool.Projects.Data.Model
{
    internal static class Extensions
    {
        public static TicketTask ToModelTicketTask(this Proxy.TicketToolService.TicketTask serviceTask)
        {
            return new TicketTask
                {
                    Id = serviceTask.TaskId,
                    WorkitemId = serviceTask.WorkItemId,
                    RequestedFields = serviceTask.RequiredFields.Select(f => f.ToModelField()).ToList(),
                    Outcomes = serviceTask.Outcomes.Select(o => o.ToModelOutcome()).ToList(),
                    Name = serviceTask.Name,
                    Description = serviceTask.Description
                };
        }

        private static Outcome ToModelOutcome(this Proxy.TicketToolService.Outcome serviceOutcome)
        {
            return new Outcome
                {
                    IsSuccess = serviceOutcome.IsSuccess,
                    Value = serviceOutcome.Value,
                    RequiredFields = serviceOutcome.RequiredFields.Select(f => f.ToModelField()).ToList(),
                    SkipTaskRequeriedFields = serviceOutcome.SkipTaskRequeriedFields
                };
        }

        private static RequiredField ToModelField(this Proxy.TicketToolService.RequiredField serviceField)
        {
            return new RequiredField
                {
                    Name = serviceField.Name,
                    Type = serviceField.Type,
                    DefaultValue = serviceField.DefaultValue
                };
        }

        public static Proxy.TicketToolService.RequiredField ToServerField(this RequiredField field)
        {
            return new Proxy.TicketToolService.RequiredField();
        }

        public static User ToModelUser(this Proxy.TicketToolAuth.UserInfo user)
        {
            return new User
                {
                    Login = user.LoginName,
                    Email = user.Email,
                    Roles = user.Roles,
                    Parameters = user.Parameters,
                    Authorized = user.IsAuthtorized
                };
        }

        public static CustomerOrder ToModelOrder(this Proxy.OrderService.CustomerOrder order)
        {
            if (order == null) return new CustomerOrder();

            var wwsOrder = order.WwsOrders.LastOrDefault();

            return new CustomerOrder
                {
                    OrderId = order.OrderId,
                    Customer =
                        {
                            FirstName = order.Customer.FirstName,
                            Surname = order.Customer.Surname,
                            Email = order.Customer.Email,
                            MainPhone = order.Customer.Mobile,
                            OptionalPhones = order.Customer.Phone
                        },
                    PickupInfo =
                        {
                            Address = order.Delivery.Address,
                            City = order.Delivery.City,
                            SapCode = String.IsNullOrEmpty(order.SaleLocation.SapCode) ? String.Empty : order.SaleLocation.SapCode.ToUpper(),
                            PickupType = new PickupInfo().PickupTypes
                                                         .Where(pt => pt.Text == order.Delivery.DeliveryType)
                                                         .Select(pt => pt.Value)
                                                         .FirstOrDefault()
                        },
                    ReadOnly = false, //TODO: Update when service will support permission assigning
                    BasketArticles = order.OrderLines
                                          .Select(l => new ArticleLine
                                              {
                                                  ArticleNum = l.ArticleNo.ToString(CultureInfo.InvariantCulture),
                                                  //Comment = l.OrderLineComment,
                                                  Price = l.Price.HasValue ? l.Price.Value : default(decimal),
                                                  Qty = l.Quantity,
                                              })
                                          .ToList(),
                    WwsOrderId = wwsOrder != null ? wwsOrder.Id : String.Empty,
                    ReserveLines = wwsOrder == null
                                       ? new List<ReserveLine>()
                                       : wwsOrder.Items.Select(i => new ReserveLine
                                           {
                                               ArticleNum = i.Article.ToString(CultureInfo.InvariantCulture),
                                               Description = i.Description,
                                               Qty = i.ReservedQuantity,
                                               Price = i.Price,
                                               Title = i.Title,
                                               Department = i.DepartmentNumber,
                                               FreeQty = i.FreeQuantity,
                                               HasShippedFromStock = i.HasShippedFromStock,
                                               Position = i.PositionNumber,
                                               PriceOrig = i.PriceOrig,         
                                               ProductGroup = i.ProductGroup,
                                               ProductGroupName = i.ProductGroupName
                                           }).ToList()
                };

            //Proxy.OrderService.WwsOrderItem wwsOrderItem;
            //wwsOrderItem.
        }

        public static Proxy.OrderService.CustomerOrder ToServiceOrder(this CustomerOrder order)
        {
            return new Proxy.OrderService.CustomerOrder
            {
                OrderId = order.OrderId,
                OrderInfo = new OrderInfo(),
                Customer = new Proxy.OrderService.Customer
                {
                    FirstName = order.Customer.FirstName,
                    Surname = order.Customer.Surname,
                    Email = order.Customer.Email,
                    Mobile = order.Customer.MainPhone,
                    Phone = order.Customer.OptionalPhones
                },
                Delivery = new DeliveryInfo
                {
                    Address = order.PickupInfo.Address,
                    City = order.PickupInfo.City,
                    DeliveryType = order.PickupInfo.PickupTypes.First(pt => pt.Value == order.PickupInfo.PickupType).Text
                },
                SaleLocation = new SaleLocationInfo
                {
                    SaleLocationId = String.Concat("shop_", order.PickupInfo.SapCode.ToLower())
                },
                OrderLines = order.BasketArticles.Select(i => new OrderLine
                {
                    ArticleNo = Int32.Parse(i.ArticleNum),
                    //OrderLineComment = i.Comment,
                    Price = i.Price,
                    Quantity = i.Qty
                }).ToArray()
            };
        }
    }
}
