﻿using System;

namespace TicketTool.Projects.Data.Model
{
    public class TaskItem
    {
        public string WWSOrderId { get; set; }
        public string OrderId { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime DeadlineDate { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public bool IsAssignedOnMe { get; set; }
        public string AssignedOn { get; set; }
        public bool Escaleted { get; set; }
        public StoreItem Store { get; set; }        
    }
}