﻿namespace TicketTool.Projects.Data.Model
{
    public class RequiredField
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public object DefaultValue { get; set; }
        public object Value { get; set; }
    }
}