﻿namespace TicketTool.Projects.Data.Model
{
    public class ReserveLine : ArticleLine
    {
        public int Department { get; set; }
        public int FreeQty { get; set; }
        public bool HasShippedFromStock { get; set; }
        public int Position { get; set; }
        public decimal PriceOrig { get; set; }
        public int ProductGroup { get; set; }
        public string ProductGroupName { get; set; }
    }
}