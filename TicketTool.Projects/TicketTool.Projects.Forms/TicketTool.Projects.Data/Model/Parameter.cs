﻿namespace TicketTool.Projects.Data.Model
{
    public class Parameter
    {        
        public string Name { get; set; }

        public string Title { get; set; }

        public string DisplayName
        {
            get { return string.Format("{0} - {1}", Name, Title); }
        }
    }
}