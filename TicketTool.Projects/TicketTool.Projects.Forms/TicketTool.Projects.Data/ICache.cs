﻿using System;

namespace TicketTool.Projects.Data
{
    public interface ICache
    {
        void Cache<T>(Func<T> action, string key, int ttl);
        T GetCachedObject<T>(string key);
    }
}
