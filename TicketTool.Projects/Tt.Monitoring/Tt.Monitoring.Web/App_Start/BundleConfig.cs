﻿using System.Web.Optimization;

namespace TicketTool.Monitoring.Web.App_Start
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            RegisterScripts(bundles);
            RegisterStyles(bundles);
        }

        private static void RegisterStyles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css")
                .Include("~/Content/site.css")
                .Include("~/Content/alertify/alertify.core.css")
                .Include("~/Content/alertify/alertify.bootstrap.css")
                .IncludeDirectory("~/Content/themes/base", "*.css", false)
                );

            bundles.Add(new StyleBundle("~/Content/themes/base/css")
                .Include("~/Content/site.css")
                .IncludeDirectory("~/Content/themes/base", "*.css", false)
                );

            bundles.Add(new StyleBundle("~/Content/kendo/css")
                .Include("~/Content/kendo/kendo.common.css")
                .Include("~/Content/kendo/kendo.metro.css")
                .Include("~/Content/kendo.css")
                );
        }

        private static void RegisterScripts(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Scripts/jquery")
                .Include("~/Scripts/jquery-{version}.js")
                .Include("~/Scripts/jquery.cookie.js")
                .Include("~/Scripts/alertify/alertify.js")
                .Include("~/Scripts/jquery-ui-1.10.3.js")
                );

            bundles.Add(new ScriptBundle("~/Scripts/kendoui/")
                .Include("~/Scripts/kendo/kendo.core.js")
                .Include("~/Scripts/kendo/kendo.data.odata.js")
                .Include("~/Scripts/kendo/kendo.data.xml.js")
                .Include("~/Scripts/kendo/kendo.data.js")
                .Include("~/Scripts/kendo/kendo.validator.js")
                .Include("~/Scripts/kendo/kendo.binder.js")
                .Include("~/Scripts/kendo/kendo.editable.js")
                .Include("~/Scripts/kendo/kendo.fx.js")
                .Include("~/Scripts/kendo/kendo.popup.js")
                .Include("~/Scripts/kendo/kendo.list.js")
                .Include("~/Scripts/kendo/kendo.calendar.js")
                .Include("~/Scripts/kendo/kendo.combobox.js")
                .Include("~/Scripts/kendo/kendo.datepicker.js")
                .Include("~/Scripts/kendo/kendo.numerictextbox.js")
                .Include("~/Scripts/kendo/kendo.dropdownlist.js")
                .Include("~/Scripts/kendo/kendo.filtermenu.js")
                .Include("~/Scripts/kendo/kendo.menu.js")
                .Include("~/Scripts/kendo/kendo.columnmenu.js")
                .Include("~/Scripts/kendo/kendo.selectable.js")
                .Include("~/Scripts/kendo/kendo.groupable.js")
                .Include("~/Scripts/kendo/kendo.pager.js")
                .Include("~/Scripts/kendo/kendo.sortable.js")
                .Include("~/Scripts/kendo/kendo.reorderable.js")
                .Include("~/Scripts/kendo/kendo.userevents.js")
                .Include("~/Scripts/kendo/kendo.draganddrop.js")
                .Include("~/Scripts/kendo/kendo.resizable.js")
                .Include("~/Scripts/kendo/kendo.window.js")
                .Include("~/Scripts/kendo/kendo.tabstrip.js")
                .Include("~/Scripts/kendo/kendo.tooltip.js")
                .Include("~/Scripts/kendo/kendo.grid.js")
                .Include("~/Scripts/kendo/kendo.button.js")
                .Include("~/Scripts/kendo/kendo.progressbar.js")
                .Include("~/Scripts/kendo/kendo.autocomplete.js")
                .Include("~/Scripts/kendo/cultures/kendo.culture.ru-RU.js")
                .Include("~/Scripts/App/kendo.ui.culture.ru-RU.js")
                .Include("~/Scripts/App/kendo.ui.culture.js")
                .Include("~/Scripts/App/kendo.ui.multiselectbox.js")
                .Include("~/Scripts/App/kendo.filtermenu.js")
                );

            bundles.Add(new ScriptBundle("~/Scripts/OrderMonitoring")
                .IncludeDirectory("~/Scripts/App/OrderMonitoring", "*.js", true)
                );

            bundles.Add(new ScriptBundle("~/Scripts/jqueryui").Include(
                "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/Scripts/jqueryval").Include(
                "~/Scripts/jquery.unobtrusive*",
                "~/Scripts/jquery.validate*"));
        }
    }
}