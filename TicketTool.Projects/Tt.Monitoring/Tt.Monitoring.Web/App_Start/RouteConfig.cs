﻿using System.Web.Mvc;
using System.Web.Routing;

namespace TicketTool.Monitoring.Web
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapMvcAttributeRoutes();

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // Main
            routes.MapRoute("Home.Index", "", new { controller = "Home", action = "Index" });

            // Account
            routes.MapRoute("Account.Login", "account/login/", new { controller = "Account", action = "Login" });
            routes.MapRoute("Account.LogOff", "account/logoff/", new { controller = "Account", action = "LogOff" });

            // OrderMonitoring
            routes.MapRoute("OrderMonitoring.Index", "order-monitoring/", new { controller = "OrderMonitoring", action = "Index" });
            routes.MapRoute("OrderMonitoring.IndexKendo", "order-monitoring/kendo/", new { controller = "OrderMonitoring", action = "IndexKendo" });
            routes.MapRoute("OrderMonitoring.Orders", "order-monitoring/orders/", new { controller = "OrderMonitoring", action = "Orders" });
            routes.MapRoute("OrderMonitoring.Get", "order-monitoring/order/", new { controller = "OrderMonitoring", action = "Get" });
            routes.MapRoute("OrderMonitoring.OrderLines", "order-monitoring/order-lines/", new { controller = "OrderMonitoring", action = "OrderLines" });
            routes.MapRoute("OrderMonitoring.OrderCopies", "order-monitoring/order-copies/", new { controller = "OrderMonitoring", action = "OrderCopies" });
            routes.MapRoute("OrderMonitoring.OrderWorkflow", "order-monitoring/order-workflow/", new { controller = "OrderMonitoring", action = "OrderWorkflow" });
            routes.MapRoute("OrderMonitoring.OrderTaks", "order-monitoring/order_tasks/", new { controller = "OrderMonitoring", action = "OrderTasks" });
            routes.MapRoute("OrderMonitoring.LastOrderTask", "order-monitoring/last-order-task/", new { controller = "OrderMonitoring", action = "LastOrderTask" });
            routes.MapRoute("OrderMonitoring.OrderStatusHistory", "order-monitoring/order-status-history/", new { controller = "OrderMonitoring", action = "OrderStatusHistory" });
            routes.MapRoute("OrderMonitoring.ExportOrdersToExcel", "order-monitoring/export-orders-to-excel/", new { controller = "OrderMonitoring", action = "ExportOrdersToExcel" });
            routes.MapRoute("OrderMonitoring.CancelOrder", "order-monitoring/cancel-order/", new { controller = "OrderMonitoring", action = "CancelOrder" });
            routes.MapRoute("OrderMonitoring.ConfirmOrder", "order-monitoring/confirm-order/", new { controller = "OrderMonitoring", action = "ConfirmOrder" });                        
            routes.MapRoute("OrderMonitoring.StopOrder", "order-monitoring/stop-order/", new { controller = "OrderMonitoring", action = "StopOrder" });
            routes.MapRoute("OrderMonitoring.ChangeState", "order-monitoring/Change-State/", new { controller = "OrderMonitoring", action = "ChangeState" });                        
        }
    }
}