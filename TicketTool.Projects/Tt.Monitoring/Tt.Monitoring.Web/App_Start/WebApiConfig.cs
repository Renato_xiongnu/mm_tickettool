﻿using System.Web.Http;

namespace TicketTool.Monitoring.Web.App_Start
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //Catalogue
            var routes = config.Routes;
            routes.MapHttpRoute("Catalogue.GetPaymentTypes", "api/catalogue/payment-types",
                new {controller = "Catalogue", action = "GetPaymentTypes"});
            routes.MapHttpRoute("Catalogue.GetOrderSources", "api/catalogue/order-sources",
                new {controller = "Catalogue", action = "GetOrderSources"});
            routes.MapHttpRoute("Catalogue.GetOrderStates", "api/catalogue/order-states",
                new {controller = "Catalogue", action = "GetOrderStates"});
            routes.MapHttpRoute("Catalogue.GetPickupTypes", "api/catalogue/pickup-types",
                new {controller = "Catalogue", action = "GetPickupTypes"});
            routes.MapHttpRoute("Catalogue.GetCancelOrderReasonsPriorToShipment",
                "api/catalogue/cancel-order-reasons-prior-to-shipment",
                new {controller = "Catalogue", action = "GetCancelOrderReasonsPriorToShipment"});
            routes.MapHttpRoute("Catalogue.GetCancelOrderReasonsAfterShipment",
                "api/catalogue/cancel-order-after-shipment",
                new {controller = "Catalogue", action = "GetCancelOrderReasonsAfterShipment"});
            routes.MapHttpRoute("Catalogue.GetChangableOrderStates",
                "api/catalogue/changable-order-states",
                new { controller = "Catalogue", action = "GetChangableOrderStates" });
            routes.MapHttpRoute("Catalogue.GetOrderChangeReasons",
                "api/catalogue/order-change-reasons",
                new { controller = "Catalogue", action = "GetOrderChangeReasons" });
        }
    }
}