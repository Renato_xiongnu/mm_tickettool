﻿using System.Threading;
using log4net;
using TicketTool.Monitoring.Web.Data;
using TicketTool.Monitoring.Web.Logging;

namespace TicketTool.Monitoring.Web
{
    public class PrewarmApp : System.Web.Hosting.IProcessHostPreloadClient
    {
        private ILogger _logger;

        public void Preload(string[] parameters)
        {
            _logger = new Log4NetLogger(LogManager.GetLogger("Tt.Monitoring.Web")); 
            _logger.Info("Start Preload");
            var ordersRepository = OrdersRepository.Instance;
            if(ordersRepository!=null)
            {
                while (!ordersRepository.Actualized)
                {
                    Thread.Sleep(5000);
                }
            }
            _logger.Info("Finish Preload");
        }
    }
}