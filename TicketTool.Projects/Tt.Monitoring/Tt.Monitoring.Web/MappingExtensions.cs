﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AutoMapper;
using TicketTool.Monitoring.Web.Models.Catalogue;
using TicketTool.Monitoring.Web.Models.OrderMonitoring;
using TicketTool.Monitoring.Web.OrderServiceProxy;
using TicketTool.Monitoring.Web.TtService;

namespace TicketTool.Monitoring.Web
{
    public static class MappingExtensions
    {
        static MappingExtensions()
        {
            ConfigureMapping();
        }

        public static void ConfigureMapping()
        {
            Mapper.CreateMap<OrderLine, OrderLineModel>();
            Mapper.CreateMap<Customer, CustomerModel>();
            Mapper.CreateMap<SaleLocationInfo, SaleLocationInfoModel>();
            Mapper.CreateMap<DeliveryInfo, DeliveryInfoModel>();
            Mapper.CreateMap<PaymentType, PaymentTypeModel>()
                .ForMember(m=>m.Id,m=>m.MapFrom(o=>o.Id.HasValue?o.Id.Value.ToString(CultureInfo.InvariantCulture):""));
            Mapper.CreateMap<OrderInfo, OrderInfoModel>();
            Mapper.CreateMap<OrderStatusChange, OrderStatusChangeModel>();
            CreateMapForCustomerOrderListItemModel();
            Mapper.CreateMap<TicketTask, TicketTaskModel>()
                .ForMember(d => d.FormattedFilledForm, o => o.Ignore())
                .ForMember(t => t.SkillSetName, m => m.MapFrom(t1 => t1.SkillSet.Name))
                .ForMember(t => t.FilledForm,
                    m =>
                        m.MapFrom(t1 => t1.Response == null ? new Dictionary<string, object>() : t1.Response.FilledForm));
            Mapper.AssertConfigurationIsValid();
        }

        private static void CreateMapForCustomerOrderListItemModel()
        {
            Mapper.CreateMap<CustomerOrder, CustomerOrderListItemModel>()
                .ForMember(m => m.OriginaOrderId, m => m.MapFrom(c => c.OriginalOrderId))
                .ForMember(m => m.Price,
                    m =>
                        m.MapFrom(
                            c =>
                                c.OrderLines == null
                                    ? 0
                                    : c.OrderLines.Sum(ol => ol.Price.HasValue ? ol.Price.Value : 0)))
                .ForMember(m => m.DiscountCardNo,
                    m =>
                        m.MapFrom(
                            c =>
                                c.OrderInfo == null
                                    ? string.Empty
                                    : c.OrderInfo.DiscountCardNo))
                .ForMember(m => m.CustomerFullName,
                    m =>
                        m.MapFrom(
                            c =>
                                c.Customer == null
                                    ? string.Empty
                                    : string.Format("{0} {1} {2}", c.Customer.FirstName, c.Customer.LastName,
                                        c.Customer.Surname)))
                .ForMember(m => m.CustomerEmail,
                    m => m.MapFrom(c => c.Customer == null ? string.Empty : c.Customer.Email))
                .ForMember(m => m.CustomerPhone,
                    m => m.MapFrom(c => c.Customer == null ? string.Empty : c.Customer.Phone ?? c.Customer.Mobile))
                .ForMember(m => m.OrderInfoCreateData,
                    m => m.MapFrom(c => c.OrderInfo == null ? default(DateTime) : c.OrderInfo.CreateData))
                .ForMember(m => m.OrderInfoOrderSource,
                    m => m.MapFrom(c => c.OrderInfo == null ? string.Empty : c.OrderInfo.OrderSource))
                .ForMember(m => m.OrderInfoOrderState,
                    m => m.MapFrom(c => c.OrderInfo == null ? string.Empty : c.OrderInfo.OrderState))
                .ForMember(m => m.DeliveryDeliveryType,
                    m => m.MapFrom(c => c.Delivery == null ? string.Empty : c.Delivery.DeliveryType))
                .ForMember(m => m.OrderInfoPaymentType,
                    m =>
                        m.MapFrom(
                            c =>
                                c.OrderInfo == null || c.OrderInfo.PaymentType == null
                                    ? new PaymentTypeModel { Id = "", NameRu = "Не указан" }
                                    : c.OrderInfo.PaymentType.ConvertTo<PaymentTypeModel>()))
                .ForMember(m => m.DeliveryAddress, m => m.MapFrom(c => ToFullAddress(c)))                
                .ForMember(m => m.Prepay, m => m.MapFrom(o => IsPrepaid(o)));
        }

        private static bool IsPrepaid(CustomerOrder order)
        {
            if(order == null || order.OrderInfo == null || order.OrderInfo.PaymentType == null ||
               order.OrderInfo.OrderStatusHistory == null)
            {
                return false;
            }
            return order.OrderInfo.PaymentType.NameRu.ToLower() != "наличными при получении заказа" ||
                   order.OrderInfo.OrderStatusHistory.Any(el => el.ToStatus.ToLower() == "деньги пришли");
        }

        private static string ToFullAddress(CustomerOrder c)
        {
            var info = c.Delivery;
            return info == null
                ? string.Empty
                : string.Join(" ", info.City, info.Street, info.House, info.Housing, info.Building, info.Flat,
                    info.Entrance);
        }

        public static T ConvertTo<T>(this object obj)
        {
            return Mapper.Map<T>(obj);
        }
    }
}