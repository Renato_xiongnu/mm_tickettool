﻿using System;
using System.Web.Mvc;
using System.Web.Security;
using TicketTool.Monitoring.Web.AuthProxy;
using TicketTool.Monitoring.Web.Models.Account;

namespace TicketTool.Monitoring.Web.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }        

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if(ModelState.IsValid)
            {
                using(var client = new AuthenticationServiceClient())
                {
                    if(client.LogIn(model.UserName, model.Password))
                    {                        
                        return RedirectToLocal(returnUrl);
                    }
                }
                ModelState.AddModelError("", "The user name or password provided is incorrect.");
            }            
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            using(var client = new AuthenticationServiceClient())
            {
                client.LogOut();                
            }
            return RedirectToAction("Index", "Home");
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if(Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }
    }
}