﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Kendo.Mvc.Grid.CRUD.Models;
using Newtonsoft.Json;
using TicketTool.Monitoring.Web.Data;
using TicketTool.Monitoring.Web.Infastructure;
using TicketTool.Monitoring.Web.Models.OrderMonitoring;
using TicketTool.Monitoring.Web.OrderServiceProxy;
using TicketTool.Monitoring.Web.Proxy.OrderSecurityService;
using TicketTool.Monitoring.Web.Reports;
using TicketTool.Monitoring.Web.TtService;
using Filter = Kendo.Mvc.Grid.CRUD.Models.Filter;
using OperationResult = TicketTool.Monitoring.Web.Proxy.OrderSecurityService.OperationResult;
using ReturnCode = TicketTool.Monitoring.Web.OrderServiceProxy.ReturnCode;
using TicketTool.Monitoring.Web.WFManagementServiceProxy;

namespace TicketTool.Monitoring.Web.Controllers
{
    [Authorize]
    public class OrderMonitoringController : BaseController
    {
        private readonly CustomerOrdersReport _customerOrdersReport;

        public OrderMonitoringController()
        {
            _customerOrdersReport = new CustomerOrdersReport();
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Get(string orderId)
        {
            var order = OrdersRepository.Instance.GetOrderListItemModelByOrderId(orderId);
            return Json(order, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Orders(int? take, int? skip, IEnumerable<Sort> sort, Filter filter)
        {
            AdjustDateTime(filter);
            ProcessOrdersSort(sort);            
            var result = OrdersRepository.Instance
                .GetOrderListItemModels()
                .AsQueryable()
                .ToDataSourceResult(take.HasValue ? take.Value : 1000, skip.HasValue ? skip.Value : 0, sort, filter);

            if (result.Total == 0)
            {
                if (filter.Filters != null)
                {
                    var f = filter.Filters.FirstOrDefault();
                    if (result.Total == 0 && f != null && f.Field.Equals("OrderId", StringComparison.InvariantCultureIgnoreCase) && f.Operator.Equals("contains", StringComparison.InvariantCultureIgnoreCase))
                    {
                        var order = OrdersRepository.GetOrderByIdNoCache(f.Value as string);
                        if (order != null)
                        {
                            return Json(new {Data = new List<CustomerOrderListItemModel> {order}, Total = 1, Filter = filter}, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            return Json(new
            {
                result.Data, 
                result.Total,
                Filter = filter                
            }, JsonRequestBehavior.AllowGet);
        }

        [Route("can-be-edited")]
        public ActionResult CanBeEdited(string orderId)
        {
            using (var client = new OrderSecurityServiceClient())
            {
                try
                {
                    var response = client.CanBeEdited(orderId);
                    return Json(response, JsonRequestBehavior.AllowGet); 
                }
                catch(Exception ex)
                {                    
                    return Json(new OperationResult()
                    {
                        ReturnCode = Proxy.OrderSecurityService.ReturnCode.Error,
                        Message="Произошла ошибка. Попробуйте позже"
                    }, JsonRequestBehavior.AllowGet);
                }               
            }
        }        

        private static void ProcessOrdersSort(IEnumerable<Sort> sort)
        {
            if(sort != null)
            {
                foreach(var sort1 in sort)
                {
                    if(sort1.Field == "OrderInfoCreateDay")
                    {
                        sort1.Field = "OrderInfoCreateData";
                    }
                }
            }
        }

        [HttpPost]
        public ActionResult CancelOrder(string orderId, string reasonText)
        {
            using(var client = new OrderServiceClient())
            {
                var response = client.GetOrder(orderId);
                if(response.ReturnCode != ReturnCode.Ok)
                {
                    return Json(response);
                }
                var order = response.CustomerOrder;
                var orderStatus = (order.OrderInfo.OrderState ?? "").ToLower();
                OrderServiceProxy.OperationResult result;
                switch(orderStatus)
                {
                    case "упакован":
                    case "доставляется клиенту":
                    case "передано на ТС":
                    case "доставлен":
                    case "доставлено":
                    case "отмена":
                        result = new CancelOrderOperationResult
                        {
                            ReturnCode = ReturnCode.Error,
                            Message = string.Format("Заказ в статусе \"{0}\" отменить нельзя!", orderStatus)
                        };
                        break;
                    default:
                        result = client.CancelOrder(new CancelOrderRequest
                        {
                            OrderId = orderId,
                            CancelReason = reasonText,
                            CancelBy = User.Identity.Name
                        });
                        break;
                }
                return Json(result);
            }
        }        

        [HttpPost]
        public ActionResult ConfirmOrder(string orderId)
        {
            using (var client = new OrderServiceClient())
            {
                var response = client.GetOrder(orderId);
                if (response.ReturnCode != OrderServiceProxy.ReturnCode.Ok)
                {
                    return Json(response);
                }
                var order = response.CustomerOrder;
                var orderStatus = (order.OrderInfo.OrderState ?? "").ToLower();
                OrderServiceProxy.OperationResult result;
                switch (orderStatus)
                {
                    case "новый заказ":
                    case "новый - назначен":
                    case "новый - просмотрен":
                    case "уточняется":
                    case "оплачивается":
                    case "деньги пришли":
                    case "оплачен":
                    case "позиция удалена":
                        var request = new ConfirmOrderRequest { OrderId = orderId, ConfirmedBy = User.Identity.Name };
                        result = client.ConfirmOrder(request);
                        return Json(result);
                    default:
                        result = new OrderServiceProxy.OperationResult
                        {
                            ReturnCode = OrderServiceProxy.ReturnCode.Error,
                            Message = string.Format("Заказ в статусе \"{0}\" подтвердить нельзя!", orderStatus)
                        };
                        return Json(result);
                }
            }
        }

        [HttpPost]
        public ActionResult StopOrder(string orderId)
        {
            JsonResult result = null;

            using (var ttService = new TicketToolServiceClient())
            {
                try
                {
                    var ticketId = ttService.GetTicket(orderId);

                    if (!String.IsNullOrEmpty(ticketId))
                        ttService.CloseTicket(ticketId);

                    using (var wfService = new WorkflowManagementClient())
                    {
                        var workflowId = wfService.GetWorkflowId(orderId);

                        wfService.DeleteWorkflow(workflowId);

                        result = Json(new OrderServiceProxy.OperationResult { ReturnCode = ReturnCode.Ok, OrderId = orderId });
                    }
                }
                catch (Exception err)
                {
                    result = Json(new OrderServiceProxy.OperationResult { ReturnCode = ReturnCode.Error, Message = err.Message, OrderId = orderId });
                }
            }


            return result;
        }

        public ActionResult ChangeState(string orderId, string newState, string reasonText)
        {
            using (var client = new OrderServiceClient())
            {
                return Json(client.ChangeState(orderId, newState, reasonText, User.Identity.Name));
            }
        }

        public JsonResult OrderLines(string orderId, int? take, int? skip, IEnumerable<Sort> sort, Filter filter)
        {
            AdjustDateTime(filter);
            var result = OrdersRepository.Instance
                .GetOrderLineModelsByOrderId(orderId)
                .AsQueryable()
                .ToDataSourceResult(take.HasValue ? take.Value : 1000, skip.HasValue ? skip.Value : 0, sort, filter);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult OrderStatusHistory(string orderId, int? take, int? skip, IEnumerable<Sort> sort, Filter filter)
        {
            AdjustDateTime(filter);
            var result = OrdersRepository.Instance
                .GetOrderStatusChangeModelsByOrderId(orderId)
                .AsQueryable()
                .ToDataSourceResult(take.HasValue ? take.Value : 1000, skip.HasValue ? skip.Value : 0, sort, filter);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult OrderTasks(string orderId, int? take, int? skip, IEnumerable<Sort> sort, Filter filter)
        {
            AdjustDateTime(filter);
            using(var client = new TicketToolServiceClient())
            {
                var tasksByWorkitemId = client
                    .GetTasksByWorkitemId(orderId);
                var ticketTaskModels = tasksByWorkitemId
                    .Select(t => t.ConvertTo<TicketTaskModel>())
                    .ToArray();
                var result = ticketTaskModels
                    .AsQueryable()
                    .ToDataSourceResult(take.HasValue ? take.Value : 1000, skip.HasValue ? skip.Value : 0, sort, filter);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult OrderCopies(string orderId, int? take, int? skip, IEnumerable<Sort> sort, Filter filter)
        {
            AdjustDateTime(filter);
            var result = OrdersRepository.Instance
                .GetOrderCopyModelsByOrderId(orderId)
                .AsQueryable()
                .ToDataSourceResult(take.HasValue ? take.Value : 1000, skip.HasValue ? skip.Value : 0, sort, filter);
            return Json(result, JsonRequestBehavior.AllowGet);            
        }

        public JsonResult OrderWorkflow(string orderId, int? take, int? skip, IEnumerable<Sort> sort, Filter filter)
        {
            AdjustDateTime(filter);

            WorkflowModel model = null;

            using (var wfService = new WorkflowManagementClient())
            {
                var wf = wfService.GetWorkflow(orderId);
                if (wf != null)
                {
                    model = new WorkflowModel()
                    {
                        WorkflowId = wf.WorkflowId,
                        Status = wf.Status,
                        Exception = wf.Exception,
                        IsSuspended = wf.IsSuspended.ToString()
                    };
                }
            }

            var result = new List<WorkflowModel> { model }
                .AsQueryable()
                .ToDataSourceResult(take.HasValue ? take.Value : 1000, skip.HasValue ? skip.Value : 0, sort, filter);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LastOrderTask(string orderId)
        {
            using(var client = new TicketToolServiceClient())
            {
                var task = client
                    .GetTasksByWorkitemId(orderId)
                    .OrderBy(t => t.Created)
                    .LastOrDefault();
                if(task == null)
                {
                    return Json(new
                    {
                        HasError = true,
                        MessageText = "Нет ни одной задачи"
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new
                {
                    HasError = false,
                    Data = task
                },JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ExportOrdersToExcel(int? take, int? skip, FormCollection collection)
        {
            var sortParam = collection.Get("sort");
            var sort = sortParam != null
                ? JsonConvert.DeserializeObject<Sort[]>(sortParam)
                : null;
            ProcessOrdersSort(sort);
            var filterParam = collection.Get("filter");
            var filter = DeserializeFilter(filterParam);            
            var result = OrdersRepository.Instance
                .GetOrderListItemModels()
                .ToDataSourceResult(take.HasValue ? take.Value : Int16.MaxValue, skip.HasValue ? skip.Value : 0, sort,
                    filter);

            return new ExcelResult("CustomerOrders.xlsx", _customerOrdersReport.Create(result.Data));
        }

        private static Filter DeserializeFilter(string filterParam)
        {
            var filter = filterParam != null
                ? JsonConvert.DeserializeObject<Filter>(filterParam, new JsonSerializerSettings
                {
                    DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                })
                : null;
            AdjustDateTime(filter);
            return filter;
        }

        private static void AdjustDateTime(Filter filter)
        {
            if (filter == null)
            {
                return;
            }
            if (filter.Value is DateTime)
            {
                filter.Value = ((DateTime)filter.Value)                    
                    .ToApplicationDateTime();
            }
            if (filter.Filters == null)
            {
                return;
            }
            foreach (var filter1 in filter.Filters)
            {
                AdjustDateTime(filter1);
            }
        }
    }

}