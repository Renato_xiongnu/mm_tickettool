﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TicketTool.Monitoring.Web.CatalogueService;
using TicketTool.Monitoring.Web.Data;
using TicketTool.Monitoring.Web.Infastructure;
using TicketTool.Monitoring.Web.Models.Catalogue;

namespace TicketTool.Monitoring.Web.Controllers
{
    [Authorize]
    public class CatalogueController : ApiController
    {
        /// <summary>
        /// Причины отказа от заказа до отгрузки
        /// </summary>
        private static readonly string[] CancelOrderReasonsPriorToShipment =
            StringResources.GetStrings("CancelOrderReasonsPriorToShipment");

        /// <summary>
        /// Причины отказа от заказа после отгрузки
        /// </summary>
        private static readonly string[] CancelOrderReasonsAfterShipment =
            StringResources.GetStrings("CancelOrderReasonsAfterShipment");

        [HttpGet]
        public PaymentTypeModel[] GetPaymentTypes()
        {
            using(var client = new CatalogueServiceClient())
            {
                var result = client.GetPaymentTypeDictionary();
                if(result.ReturnCode != ReturnCode.Ok)
                {
                    throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(result.Message),
                        ReasonPhrase = "CatalogueService return error"
                    });
                }
                var results = result.Dictionary
                    .Select(kv => new PaymentTypeModel()
                    {
                        Id = kv.Key,
                        Name = kv.Value
                    })
                    .OrderBy(pt => pt.Name)
                    .ToArray();
                return results;
            }
        }

        [HttpGet]
        public string[] GetOrderSources()
        {
            return OrdersRepository.Instance
                .GetOrderSources()
                .OrderBy(s => s)
                .ToArray();
        }

        [HttpGet]
        public string[] GetOrderStates()
        {
            return OrdersRepository.Instance
                .GetOrderStates()
                .OrderBy(s => s)
                .ToArray();
        }

        [HttpGet]
        public string[] GetPickupTypes()
        {
            return OrdersRepository.Instance
                .GetPickupTypes()
                .OrderBy(s => s)
                .ToArray();
        }

        [HttpGet]
        public string[] GetCancelOrderReasonsPriorToShipment()
        {
            return CancelOrderReasonsPriorToShipment
                .OrderBy(s => s)
                .ToArray();
        }

        [HttpGet]
        public string[] GetCancelOrderReasonsAfterShipment()
        {
            return CancelOrderReasonsAfterShipment
                .OrderBy(s => s)
                .ToArray();
        }

        [HttpGet]
        public string[] GetChangableOrderStates()
        {
            return new[] {"Отмена КЦ"};
        }

        [HttpGet]
        public string[] GetOrderChangeReasons()
        {
            return new[] {"1", "2"};
        }
    }
}