﻿using System.Web.Mvc;

namespace TicketTool.Monitoring.Web.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return RedirectToAction("Index", "OrderMonitoring");
        }
    }
}