﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Timers;
using log4net;
using TicketTool.Monitoring.Web.Logging;
using TicketTool.Monitoring.Web.Models.OrderMonitoring;
using TicketTool.Monitoring.Web.OrderServiceProxy;
using Timer = System.Timers.Timer;

namespace TicketTool.Monitoring.Web.Data
{
    public class OrdersRepository
    {
        private readonly ReaderWriterLockSlim _locker = new ReaderWriterLockSlim();

        private readonly Dictionary<string, Tuple<CustomerOrderListItemModel, OrderLineModel[], OrderStatusChangeModel[], OrderCopyModel[]>> _customOrders =
                new Dictionary<string, Tuple<CustomerOrderListItemModel, OrderLineModel[], OrderStatusChangeModel[], OrderCopyModel[]>>();

        private readonly Dictionary<string, string> _orderSources = new Dictionary<string, string>();

        private readonly Dictionary<string, string> _orderStates = new Dictionary<string, string>();

        private readonly Dictionary<string, string> _pickupTypes = new Dictionary<string, string>();

        public bool Actualized
        {
            get { return _actualized; }            
        }

        private readonly Timer _timer;

        private long _lastSequance = int.Parse(ConfigurationManager.AppSettings["OrdersLastSequance"]);

        private readonly int _pageSize;

        private OrdersRepository()
        {
            _logger = new Log4NetLogger(LogManager.GetLogger("Tt.Monitoring.Web"));
            _locker = new ReaderWriterLockSlim();
            _pageSize = 100;
            _timer = new Timer
            {
                AutoReset = false,
                Interval = TimeSpan.FromSeconds(1).TotalMilliseconds
            };
            _timer.Elapsed += Timer_Elapsed;            
            try
            {
                _actualized = false;
                Actualize();
            }            
            finally
            {
                _actualized = true;
                _timer.Start();
            }
        }

        static OrdersRepository()
        {
            Instance = new OrdersRepository();
        }

        public static readonly OrdersRepository Instance;

        private readonly ILogger _logger;

        private volatile bool _actualized;

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                Actualize();
            }
            finally
            {
                _timer.Start();
            }
        }

        private void Actualize()
        {
            using(var client = new OrderServiceClient())
            {
                do
                {
                    _logger.InfoFormat("Actualize start (LastSequance={0})", _lastSequance);
                    var response = client.GetOrders(_lastSequance, _pageSize);
                    if (response.ReturnCode == ReturnCode.Error)
                    {
                        _logger.ErrorFormat("Actualize error(LastSequance={0}): {1}", _lastSequance, response.Message);
                        break;
                    }
                    var index = 0;
                    _locker.EnterWriteLock();
                    try
                    {                        
                        foreach(var customerOrder in response.CustomerOrders)
                        {
                            _customOrders[customerOrder.OrderId] =
                                new Tuple<CustomerOrderListItemModel, OrderLineModel[], OrderStatusChangeModel[],OrderCopyModel[]>(
                                    customerOrder.ConvertTo<CustomerOrderListItemModel>(),
                                    customerOrder.OrderLines == null ? new OrderLineModel[0] : customerOrder.OrderLines.Select(ol => ol.ConvertTo<OrderLineModel>()).ToArray(),
                                    customerOrder.OrderInfo.OrderStatusHistory == null ? new OrderStatusChangeModel[0] : customerOrder.OrderInfo.OrderStatusHistory.Select(ol => ol.ConvertTo<OrderStatusChangeModel>()).ToArray(),
                                    customerOrder.CopyIds == null ? new OrderCopyModel[0] : customerOrder.CopyIds.Select(oid => new OrderCopyModel {OrderId = oid}).ToArray());
                            if(customerOrder.OrderInfo != null)
                            {
                                var orderSource = customerOrder.OrderInfo.OrderSource;
                                if(!string.IsNullOrEmpty(orderSource))
                                {
                                    _orderSources[orderSource] = orderSource;
                                }
                                var orderState = customerOrder.OrderInfo.OrderState;
                                if(!string.IsNullOrEmpty(orderState))
                                {
                                    _orderStates[orderState] = orderState;
                                }                                
                            }
                            if (customerOrder.Delivery!=null)
                            {
                                var pickupType = customerOrder.Delivery.DeliveryType;
                                if (!string.IsNullOrEmpty(pickupType))
                                {
                                    _pickupTypes[pickupType] = pickupType;
                                }
                            }                            
                            index++;
                        }
                        _lastSequance = response.LastSequance;
                        _logger.InfoFormat("Actualize complete (LastSequance={0})", _lastSequance);
                    }
                    catch(Exception ex)
                    {
                        _logger.ErrorFormat("Actualize exception(LastSequance={0}): {1}", _lastSequance, ex.Message);
                    }
                    finally
                    {
                        _locker.ExitWriteLock();
                    }
                    if(_pageSize > index)
                    {
                        break;
                    }
                } while(true);
            }
        }

        public IQueryable<CustomerOrderListItemModel> GetOrderListItemModels()
        {
            _locker.EnterReadLock();
            try
            {
                return _customOrders.Values.Select(v => v.Item1).ToArray().AsQueryable();
            }
            finally
            {
                _locker.ExitReadLock();
            }
        }

        public CustomerOrderListItemModel GetOrderListItemModelByOrderId(string orderId)
        {
            _locker.EnterReadLock();
            try
            {
                return _customOrders[orderId].Item1;
            }
            finally
            {
                _locker.ExitReadLock();
            }
        }

        public OrderLineModel[] GetOrderLineModelsByOrderId(string orderId)
        {
            _locker.EnterReadLock();
            try
            {
                return _customOrders[orderId].Item2;
            }
            finally
            {
                _locker.ExitReadLock();
            }
        }

        public OrderCopyModel[] GetOrderCopyModelsByOrderId(string orderId)
        {
            _locker.EnterReadLock();
            try
            {
                return _customOrders[orderId].Item4;
            }
            finally
            {
                _locker.ExitReadLock();
            }
        }

        public OrderStatusChangeModel[] GetOrderStatusChangeModelsByOrderId(string orderId)
        {
            _locker.EnterReadLock();
            try
            {
                return _customOrders[orderId].Item3;
            }
            finally
            {
                _locker.ExitReadLock();
            }
        }

        public string[] GetOrderSources()
        {
            _locker.EnterReadLock();
            try
            {
                return _orderSources.Values.ToArray();
            }
            finally
            {
                _locker.ExitReadLock();
            }
        }

        public string[] GetOrderStates()
        {
            _locker.EnterReadLock();
            try
            {
                return _orderStates.Values.ToArray();
            }
            finally
            {
                _locker.ExitReadLock();
            }
        }

        public string[] GetPickupTypes()
        {
            _locker.EnterReadLock();
            try
            {
                return _pickupTypes.Values.ToArray();
            }
            finally
            {
                _locker.ExitReadLock();
            }
        }

        public static CustomerOrderListItemModel GetOrderByIdNoCache(string orderId)
        {
            if (String.IsNullOrEmpty(orderId)) return null;

            using (var client = new OrderServiceClient())
            {
                var result = client.GetOrder(orderId);
                if (result.ReturnCode == ReturnCode.Ok)
                {
                    return result.CustomerOrder.ConvertTo<CustomerOrderListItemModel>();
                }
            }
            return null;
        }
    }
}