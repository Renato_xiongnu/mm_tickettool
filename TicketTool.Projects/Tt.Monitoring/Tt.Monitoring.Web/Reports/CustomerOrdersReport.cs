﻿using System.Collections.Generic;
using System.IO;
using ClosedXML.Excel;
using TicketTool.Monitoring.Web.Models.OrderMonitoring;

namespace TicketTool.Monitoring.Web.Reports
{
    public class CustomerOrdersReport
    {
        public byte[] Create(ICollection<CustomerOrderListItemModel> customerOrders)
        {
            var workbook = new XLWorkbook();
            var ws = workbook.Worksheets.Add("Заказы");            
            ws.Cell(1, 1).Value = "Создан";
            ws.Cell(1, 2).Value = "Канал";
            ws.Cell(1, 3).Value = "Номер заказа";
            ws.Cell(1, 4).Value = "Состояние заказа";
            ws.Cell(1, 5).Value = "Клиент";
            ws.Cell(1, 6).Value = "Телефон";
            ws.Cell(1, 7).Value = "Email";
            ws.Cell(1, 8).Value = "Способ получения";
            ws.Cell(1, 9).Value = "Адрес доставки";
            ws.Cell(1, 10).Value = "Тип оплаты";
            //ws.Cell(1, 11).Value = "Статус обработки";
            ws.Cell(1, 11).Value = "Сумма";
            var row = 2;
            foreach(var order in customerOrders)
            {
                ws.Cell(row, 1).Value = order.OrderInfoCreateData;
                ws.Cell(row, 2).Value = order.OrderInfoOrderSource;
                ws.Cell(row, 3).Value = order.OrderId;
                ws.Cell(row, 4).Value = order.OrderInfoOrderState;
                ws.Cell(row, 5).Value = order.CustomerFullName;
                ws.Cell(row, 6).Value = order.CustomerPhone;
                ws.Cell(row, 7).Value = order.CustomerEmail;
                ws.Cell(row, 8).Value = order.DeliveryDeliveryType;
                ws.Cell(row, 9).Value = order.DeliveryAddress;
                ws.Cell(row, 10).Value = order.OrderInfoPaymentType.NameRu;
                ws.Cell(row, 11).Value = order.Price;
                row++;
                if(row==3)
                {
                    ws.Columns().AdjustToContents();
                }
            }
            ws.RangeUsed().Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            ws.RangeUsed().Style.Border.OutsideBorder = XLBorderStyleValues.Medium;
            const int lastCellColumn = 12;
            var rngTable = ws.Range(1, 1, row, lastCellColumn);
            var rngHeaders = rngTable.Range(1, 1, 1, lastCellColumn);
            rngHeaders.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            rngHeaders.Style.Font.Bold = true;
            rngHeaders.Style.Fill.BackgroundColor = XLColor.LightGray;            
            using(var ms = new MemoryStream())
            {
                workbook.SaveAs(ms);
                return ms.ToArray();
            }
        }
    }
}