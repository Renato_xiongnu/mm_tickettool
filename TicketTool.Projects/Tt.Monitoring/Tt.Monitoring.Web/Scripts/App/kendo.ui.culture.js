﻿kendo.ui = kendo.ui || {};
kendo.ui.cultures = kendo.ui.cultures || [];

(function ($, cultureName) {    
	kendo.culture(cultureName);
	kendo.ui.currentCulture = kendo.ui.cultures["ru-RU"];
})(jQuery, 'ru-RU');
