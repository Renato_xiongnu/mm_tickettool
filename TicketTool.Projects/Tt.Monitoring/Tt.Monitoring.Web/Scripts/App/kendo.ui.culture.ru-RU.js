﻿window.kendo.ui = window.kendo.ui || {};
kendo.ui.cultures = kendo.ui.cultures || [];

(function() {
    kendo.ui.cultures["ru-RU"] = {
        grid: {
            filterable: {
                messages: {
                    info: "Фильтр:", // sets the text on top of the filter menu
                    filter: "Применить", // sets the text for the "Filter" button
                    clear: "Очистить", // sets the text for the "Clear" button

                    // when filtering boolean numbers
                    isTrue: "Истина", // sets the text for "isTrue" radio button
                    isFalse: "Ложь", // sets the text for "isFalse" radio button

                    //changes the text of the "And" and "Or" of the filter menu
                    and: "и",
                    or: "или"
                },
                operators: {
                    //filter menu for "string" type columns
                    string: {
                        eq: "Равно",
                        neq: "Не равно",
                        startswith: "Начинается с",
                        contains: "Содержит",
                        endswith: "Заканчивается на"
                    },
                    //filter menu for "number" type columns
                    number: {
                        eq: "Равно",
                        neq: "Не равно",
                        gte: "Больше или равно",
                        gt: "Больше",
                        lte: "Меньше или равно",
                        lt: "Меньше"
                    },
                    //filter menu for "date" type columns
                    date: {
                        eq: "Равно",
                        neq: "Не равно",
                        gte: "Больше или равно",
                        gt: "Больше",
                        lte: "Меньше или равно",
                        lt: "Меньше"
                    },
                    //filter menu for foreign key values
                    enums: {
                        eq: "Равно",
                        neq: "Не равно"
                    }
                }
            },
            pageable:
            {                
                messages: {
                    display: "{0} - {1} из {2}", //{0} is the index of the first record on the page, {1} - index of the last record on the page, {2} is the total amount of records
                    empty: "Нет ни одного элемента",
                    page: "Страница",
                    of: "из {0}", //{0} is total amount of pages
                    itemsPerPage: "элементов на странице",
                    first: "Превая страница",
                    previous: "Предыдущая страница",
                    next: "Следующая страница",
                    last: "Последняя страница",
                    refresh: "Обновить"
                }
            }
        }
    };
})();