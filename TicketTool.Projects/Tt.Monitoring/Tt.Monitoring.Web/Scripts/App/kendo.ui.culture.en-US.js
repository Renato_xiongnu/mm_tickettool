﻿window.kendo.ui = window.kendo.ui || {};
kendo.ui.cultures = kendo.ui.cultures || [];

(function() {
    var EQ = "Is equal to",
        NEQ = "Is not equal to";
    kendo.ui.cultures["en-US"] = {
        grid: {
            filterable: {
                messages: {
                    info: "Show items with value that:",
                    isTrue: "is true",
                    isFalse: "is false",
                    filter: "Filter",
                    clear: "Clear",
                    and: "And",
                    or: "Or",
                    selectValue: "-Select value-",
                    operator: "Operator",
                    value: "Value",
                    cancel: "Cancel"
                },
                operators: {
                    string: {
                        eq: EQ,
                        neq: NEQ,
                        startswith: "Starts with",
                        contains: "Contains",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with",
                        emp: "Is empty",
                        nemp: "Is not empty",
                    },
                    number: {
                        eq: EQ,
                        neq: NEQ,
                        gte: "Is greater than or equal to",
                        gt: "Is greater than",
                        lte: "Is less than or equal to",
                        lt: "Is less than",
                        emp: "Is empty",
                        nemp: "Is not empty",
                    },
                    date: {
                        eq: EQ,
                        neq: NEQ,
                        gte: "Is after or equal to",
                        gt: "Is after",
                        lte: "Is before or equal to",
                        lt: "Is before",
                        emp: "Is empty",
                        nemp: "Is not empty",
                    },
                    enums: {
                        eq: EQ,
                        neq: NEQ
                    }
                },
                pageable:
                {
                    messages: {
                        display: "{0} - {1} of {2} items",
                        empty: "No items to display",
                        page: "Page",
                        of: "of {0}",
                        itemsPerPage: "items per page",
                        first: "Go to the first page",
                        previous: "Go to the previous page",
                        next: "Go to the next page",
                        last: "Go to the last page",
                        refresh: "Refresh"
                    }
                }
            }
        }
    };
})();