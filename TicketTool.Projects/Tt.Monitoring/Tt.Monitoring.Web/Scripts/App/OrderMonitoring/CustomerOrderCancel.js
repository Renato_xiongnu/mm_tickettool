﻿// <reference path="_references.js" />
/// <reference path="kendo.ui.culture.js" />

window.Mms = window.Mms || {};
Mms.OrderMonitoring = Mms.OrderMonitoring || {};

Mms.OrderMonitoring.CustomerOrderCancel = function (opts) {
    this.opts = $.extend(true, {           
        CancelOrderUrl: '',        
        CancelWindowName: 'Отмена заказа №',
        $Container: $('body'),        
        CancelOrderReasonPriorToShipmentDataSource: true,
        CancelOrderReasonAfterShipmentDataSource: true,
        DropDownOptionLabel: '--Выберите--'
    }, opts);
    this.opts.$Container.append(this.$window);
};

Mms.OrderMonitoring.CustomerOrderCancel.prototype = {
    Cancel: function (cancelParameters) {        
        this.cancelParameters = cancelParameters;        
        if (this.cancelOrderWindow) {            
            this._updateCancelOrderWindow();
            this.cancelOrderWindow.open();
            this.cancelOrderWindow.center();
            return;
        }
        this.cancelOrderWindow = this._createCancelOrderWindow();
        this.cancelOrderWindow.open();
        this.cancelOrderWindow.center();
    },
    _updateCancelOrderWindow: function () {
        this.cancelOrderWindow.setOptions({
            title: this.opts.CancelWindowName + this.cancelParameters.orderId,
        });
        this.cancelOrderWindow.refresh({
            url: this.opts.EditOrderUrl,
            data: { orderId: this.cancelParameters.orderId }
        });
    },
    _createCancelOrderWindow: function () {
        var self = this;
        var $window = $('<div />');
        this.opts.$Container.append($window);
        return $window
            .kendoWindow({
                modal: true,
                content: {
                    template:
                        '<ul>\
                            <li>\
                                <label for="reason" class="required">Причина:</label>\
                                <select  class="dropDown" name="reason" required/>\
                                <span class="k-invalid-msg" data-for="reason"></span>\
                            </li>\
                         </ul>\
                         <p></p>\
                         <div class="toolbar">\
                            <button class="okBtn k-button">Отменить заказ</button>\
                            <button class="cancelBtn k-button">Не отменять</button>\
                          </div>'
                },
                iframe: false,
                position: { top: 100 },
                width: "550px",
                height: '100px',
                actions: [],
                resizable: false,
                title: this.opts.CancelWindowName + this.cancelParameters.orderId,
                open: $.proxy(function (e) {                    
                    var self = this;
                    var el = e.sender.element;                    
                    this.reasons = el.find('.dropDown').kendoDropDownList({
                        dataSource: self._getCancelOrderReasonsDataSource(),
                        index: 0,
                        optionLabel: this.opts.DropDownOptionLabel,
                        change: function() {
                            self.validator.validate();
                        }
                    }).data("kendoDropDownList");                    
                    $(this.reasons.list).width("auto");
                    el.find('.okBtn').click($.proxy(this._cancelOrder, this));
                    el.find('.cancelBtn').click($.proxy(this._cancel, this));
                    this.validator = $(el).kendoValidator({
                        messages: {                            
                            required: "Обязательное поле",
                        },
                    }).data("kendoValidator");
                }, this)
            }).data("kendoWindow");
    },    
    _cancelOrder: function (parameters) {
        var reason = this.reasons.value();
        if (this.validator.validate()) {
            $.post(
                this.opts.CancelOrderUrl,
                {
                    orderId: this.cancelParameters.orderId,
                    reasonText: reason
                },
                $.proxy(this._onCancelOrderSuccess, this)
            );
        }
    },
    _cancel: function (parameters) {
        this.cancelOrderWindow.close();
        if (this.cancelParameters.cancelCallback) {
            this.cancelParameters.cancelCallback({ ok: false });
        }        
    },
    _getCancelOrderReasonsDataSource: function (parameters) {        
        var orderStatus = this.cancelParameters.orderStatus;
        switch (orderStatus.toLowerCase()) {            
            case 'разукомплектовано':
            case 'доставляется клиенту':
            case 'хранение':
            case 'передано на тс':
            case 'доставлено':                
                return this.opts.CancelOrderReasonAfterShipmentDataSource ;
            default:                
            return this.opts.CancelOrderReasonPriorToShipmentDataSource;
        }
    },
    _onCancelOrderSuccess: function (data, e) {        
        switch (data.ReturnCode) {
            case 0:                
                alertify.success('Успешно отменили');
                this.cancelOrderWindow.close();
                this.cancelParameters.cancelCallback({ ok: true });                
            break;
            case 1:
                alertify.alert('<b>Ошибка при выполнении операции:</b></br>' + data.Message);
            break;        
        }
        if (data.ReturnCode == 1) {
            return;
        }
    }
}