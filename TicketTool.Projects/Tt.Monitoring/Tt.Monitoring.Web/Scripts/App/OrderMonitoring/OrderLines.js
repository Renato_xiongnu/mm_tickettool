﻿/// <reference path="_references.js" />
/// <reference path="kendo.ui.culture.js" />

window.Mms = window.Mms || {};
Mms.OrderMonitoring = Mms.OrderMonitoring || {};

Mms.OrderMonitoring.OrderLines = function (opts) {
    this.opts = $.extend(true, {
        DataSource: true,
        $Container: true        
    }, opts);
    this.currentCulture = kendo.ui.currentCulture;
    this.Show();
};

Mms.OrderMonitoring.OrderLines.prototype = {
    Show: function () {
        var filterable = this.currentCulture.grid.filterable;
        var pageable = this.currentCulture.grid.pageable;
        this.grid = this.opts
            .$Container.kendoGrid({
                dataSource: this.opts.DataSource,
                scrollable: true,
                sortable: true,                
                filterable: false,
                pageable: false,                
                columns: [
                    { field: "ArticleNo", title: "Артикул", width: 100 },
                    { field: "ArticleTitle", title: "Название", width: 550 },
                    { field: "Quantity", title: "Кол-во", width: 100 },
                    { field: "Price", title: "Цена", width: 80 },
                    { field: "DiscountPercentage", title: "Скидка", template: "#= DiscountPercentage == 0 ? 'нет' : DiscountPercentage #", width: 80 },
                    { field: "LongTail", title: "Под заказ", template: "#= LongTail ? 'да' : 'нет' #" }
                ]
            }).data('kendoGrid');
    },
    Refresh: function () {
        this.grid.dataSource.fetch();
    }
};

Mms.OrderMonitoring.OrderLinesFactory = function (opts) {
    this.opts = $.extend(true, {
        DataSourceFactory: true,        
    }, opts);
};

Mms.OrderMonitoring.OrderLinesFactory.prototype = {
    Create: function(detailRow, orderId) {
        return new Mms.OrderMonitoring.OrderLines({
            DataSource: this.opts.DataSourceFactory.Create(orderId),
            $Container: detailRow
        });
    }
};