﻿/// <reference path="_references.js" />
/// <reference path="kendo.ui.culture.js" />

window.Mms = window.Mms || {};
Mms.OrderMonitoring = Mms.OrderMonitoring || {};

Mms.OrderMonitoring.OrderWorkflow = function (opts) {
    this.opts = $.extend(true, {
        DataSource: true,
        $Container: true
    }, opts);
    this.currentCulture = kendo.ui.currentCulture;
    this.Show();
};

Mms.OrderMonitoring.OrderWorkflow.prototype = {
    Show: function () {
        var filterable = this.currentCulture.grid.filterable;
        var pageable = this.currentCulture.grid.pageable;
        this.grid = this.opts
            .$Container.kendoGrid({
                dataSource: this.opts.DataSource,
                scrollable: false,
                sortable: true,
                filterable: false,
                pageable: false,
                columns: [
                    { field: "WorkflowId", title: "ИД рабочего процесса", width: 250 },
                    { field: "Status", title: "Статус", width: 150 },
                    { field: "IsSuspended", title: "Остановлен", width: 150 },
                    { field: "Exception", title: "Ошибка", width: 400 }
                ]
            }).data('kendoGrid');
    },
    Refresh: function () {
        this.grid.dataSource.fetch();
    },
    GetLastTask: function () {
        debugger;
        return this.grid.dataSource.data();
    }
};

Mms.OrderMonitoring.OrderWorkflowFactory = function (opts) {
    this.opts = $.extend(true, {
        DataSourceFactory: true,
    }, opts);
};

Mms.OrderMonitoring.OrderWorkflowFactory.prototype = {
    Create: function (detailRow, orderId) {
        return new Mms.OrderMonitoring.OrderWorkflow({
            DataSource: this.opts.DataSourceFactory.Create(orderId),
            $Container: detailRow
        });
    }
};