﻿// <reference path="_references.js" />
/// <reference path="kendo.ui.culture.js" />

window.Mms = window.Mms || {};
Mms.OrderMonitoring = Mms.OrderMonitoring || {};

Mms.OrderMonitoring.CustomerOrderStop = function (opts) {
    this.opts = $.extend(true, {
        StopOrderUrl: '',
        WindowName: 'Остановить обработку заказа №',
        $Container: $('body')
    }, opts);
    this.opts.$Container.append(this.$window);
};

Mms.OrderMonitoring.CustomerOrderStop.prototype = {
    Stop: function (stopParameters) {
        this.stopParameters = stopParameters;
        if (this.window) {
            this._updateStopOrderWindow();
            this.window.open();
            this.window.center();
            return;
        }
        this.window = this._createStopOrderWindow();
        this.window.open();
        this.window.center();
    },
    _updateStopOrderWindow: function () {
        this.window.setOptions({
            title: this.opts.WindowName + this.stopParameters.orderId,
        });
        this.window.refresh({
            url: this.opts.EditOrderUrl,
            data: { orderId: this.stopParameters.orderId }
        });
    },
    _createStopOrderWindow: function () {
        var self = this;
        var $window = $('<div />');
        this.opts.$Container.append($window);
        return $window
            .kendoWindow({
                modal: true,
                content: {
                    template:
                        '<ul>\
                            <li>\
                                Вы действительно хотите остановить обработку заказа?\
                            </li>\
                         </ul>\
                         <p></p>\
                         <div class="toolbar">\
                            <button class="okBtn k-button">Остановить обработку</button>\
                            <button class="cancelBtn k-button">Отмена</button>\
                          </div>'
                },
                iframe: false,
                position: { top: 100 },
                width: "450px",
                height: '100px',
                actions: [],
                resizable: false,
                title: this.opts.WindowName + this.stopParameters.orderId,
                open: $.proxy(function (e) {
                    var self = this;
                    var el = e.sender.element;
                    el.find('.okBtn').click($.proxy(this._stopOrder, this));
                    el.find('.cancelBtn').click($.proxy(this._cancel, this));
                }, this)
            }).data("kendoWindow");
    },
    _stopOrder: function (parameters) {
        $.post(
            this.opts.StopOrderUrl,
            {
                orderId: this.stopParameters.orderId,
            },
            $.proxy(this._onStopOrderSuccess, this)
        );
    },
    _cancel: function (parameters) {
        this.window.close();
        if (this.stopParameters.cancelCallback) {
            this.stopParameters.cancelCallback({ ok: false });
        }
    },
    _onStopOrderSuccess: function (data, e) {
        switch (data.ReturnCode) {
            case 0:
                alertify.success('Обработка заказа в ТТ успешно остановлена');
                this.window.close();
                this.stopParameters.cancelCallback({ ok: true });
                break;
            case 1:
                alertify.alert('<b>Ошибка при выполнении операции:</b></br>' + data.Message);
                break;
        }
        if (data.ReturnCode == 1) {
            return;
        }
    }
}