﻿// <reference path="_references.js" />
/// <reference path="kendo.ui.culture.js" />
/// <reference path="..\..\alertify\alertify.js" />
/// <reference path="~/Scripts/jquery-2.0.3.js" />
window.Mms = window.Mms || {};
Mms.OrderMonitoring = Mms.OrderMonitoring || {};

Mms.OrderMonitoring.CustomerOrderEdit = function (opts) {
    this.opts = $.extend(true, {
        GetOrderUrl: '',
        CanBeEditedUrl: '',
        EditOrderUrl: '',
        EditWindowName: 'Редактирование заказа №',
    }, opts);
    this.opts.$Container.append(this.$window);
};

Mms.OrderMonitoring.CustomerOrderEdit.prototype = {
    Edit: function (orderId, closeCallback) {
        var self = this;        
        this.closeCallback = closeCallback;
        self._checkCanBeEdit(orderId,
            function (data) {                
                self._edit(orderId, $.proxy(self._onClose, self));
            },
            function (data) {
                closeCallback(data);
            });
    },
    _edit: function (orderId, closeCallback) {        
        if (this.editWindow) {
            this._updateEditWindow(orderId, closeCallback);
            this.editWindow.open();
            this.editWindow.center();
            return;
        }
        this.editWindow = this._createEditWindow(orderId, closeCallback);
        this._updateSrc(orderId);
        this.editWindow.open();
        this.editWindow.center();
    },
    _updateSrc: function (orderId) {
        var $element = $(this.editWindow.element);
        var iframe = $element.find('iframe');
        iframe.attr('src', this.opts.EditOrderUrl + '?id=' + orderId);
    },
    _checkCanBeEdit: function (orderId, canEditCallback, canNotEditCallback) {
        var self = this;        
        if (self.opts.CanBeEditedUrl=='') {
            canEditCallback({ ReturnCode: 'OK' });
            return;
        }
        $.getJSON(self.opts.CanBeEditedUrl, { orderId: orderId })
            .success(function (data) {
                data.ReturnCode = data.ReturnCode === 0 ? "OK" : "Error";
                if (data.ReturnCode == 'OK') {
                    canEditCallback(data);
                    return;
                }
                data.Message = data.Message ? data.Message : "Заказ редактировать нельзя";
                canNotEditCallback(data);
            })
            .error(function (data, s, p) {
                debugger;
                canNotEditCallback({
                    ReturnCode: "Error",
                    Message: 'Ошибка при выполнении запроса'
                });
            });
    },
    _updateEditWindow: function (orderId, closeCallBack) {
        this.editWindow.setOptions({
            title: this.opts.EditWindowName + orderId,
            close: closeCallBack
        });
        this.editWindow.refresh({
            height: $(window).height() - 200,
            url: this.opts.EditOrderUrl + '?id=' + orderId,
            data: { orderId: orderId }
        });
        this._updateSrc(orderId);
    },
    _createEditWindow: function (orderId, closeCallBack) {
        var self = this;
        var $window = $('#orderEditWindow');
        return $window
            .kendoWindow({
                modal: true,
                position: { top: 100 },
                width: "850px",
                height: $(window).height() - 200,
                actions: ["Close"],
                title: this.opts.EditWindowName + orderId,
                open: function (parameters) {
                    var $element = $(parameters.sender.element);
                    $element.addClass('iframeContainer');
                    $element.css({ 'overflow': 'hidden' });
                    var iframe = $element.find('iframe');
                    iframe.hide();
                    kendo.ui.progress($element, true);
                    iframe.load($.proxy(function (e) {
                        kendo.ui.progress($element, false);
                        iframe.show();
                    }, self));
                },
                refresh: function (parameters) {
                    $(parameters.sender.element).css({ 'overflow': 'hidden' });
                },
                close: closeCallBack
            }).data("kendoWindow");
    },
    _onClose: function (parameters) {
        var orderId = '';
        var iframe = $(this.editWindow.element).find('iframe');
        if (iframe.length==1) {
            var iframeInner = $(iframe).contents();
            orderId = $(iframeInner).find('#Order_OrderId').val();
        }                       
        this.closeCallback({
            ReturnCode: 'OK',
            OrderId: orderId
        });
    }
}