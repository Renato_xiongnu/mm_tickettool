﻿// <reference path="_references.js" />
/// <reference path="kendo.ui.culture.js" />

window.Mms = window.Mms || {};
Mms.OrderMonitoring = Mms.OrderMonitoring || {};

Mms.OrderMonitoring.CustomerOrderChangeState = function (opts) {
    this.opts = $.extend(true, {
        ChangeOrderUrl: '',
        WindowName: 'Изменить состояние заказа №',
        $Container: $('body'),
        ChangeOrderStateDataSource: true,
        ChangeOrderStateReasonsDataSource: true
    }, opts);
    this.opts.$Container.append(this.$window);
};

Mms.OrderMonitoring.CustomerOrderChangeState.prototype = {
    ChangeState: function (changeParameters) {
        this.changeParameters = changeParameters;
        if (this.window) {
            this._updateChangeOrderWindow();
            this.window.open();
            this.window.center();
            return;
        }
        this.window = this._createChangeOrderWindow();
        this.window.open();
        this.window.center();
    },
    _updateChangeOrderWindow: function () {
        this.window.setOptions({
            title: this.opts.WindowName + this.changeParameters.orderId,
        });
    },
    _createChangeOrderWindow: function () {
        var $window = $('<div />');
        this.opts.$Container.append($window);
        return $window
            .kendoWindow({
                modal: true,
                content: {
                    template:
                        '<ul>\
                            <li>\
                                <label for="newstate" class="required">Новое состояние:</label>\
                                <select  class="newState" name="newstate" required/>\
                                <span class="k-invalid-msg" data-for="newstate"></span>\
                            </li>\
                            <li>\
                                <label for="reason" class="required">Причина:</label>\
                                <select  class="ddlReason" name="reason" required/>\
                                <span class="k-invalid-msg" data-for="reason"></span>\
                            </li>\
                         </ul>\
                         <p></p>\
                         <div class="toolbar">\
                            <button class="okBtn k-button">Да</button>\
                            <button class="cancelBtn k-button">Нет</button>\
                          </div>'
                },
                iframe: false,
                position: { top: 100 },
                width: "500px",
                height: '115px',
                actions: [],
                resizable: false,
                title: this.opts.WindowName + this.changeParameters.orderId,
                open: $.proxy(function (e) {
                    var self = this;
                    var el = e.sender.element;

                    this.states = el.find('.newState').kendoDropDownList({
                        dataSource: self._getChangeOrderStateDataSource(),
                        index: 0
                    }).data("kendoDropDownList");
                    $(this.states.list).width("auto");

                    this.reasons = el.find('.ddlReason').kendoDropDownList({
                        dataSource: this.opts.ChangeOrderStateReasonsDataSource,
                        index: 0
                    }).data("kendoDropDownList");
                    $(this.reasons.list).width("auto");

                    el.find('.okBtn').click($.proxy(this._changeOrder, this));
                    el.find('.cancelBtn').click($.proxy(this._cancel, this));
                }, this)
            }).data("kendoWindow");
    },
    _getChangeOrderStateDataSource: function (parameters) {
        return this.opts.ChangeOrderStateDataSource;
    },
    _changeOrder: function (parameters) {
        var value = this.states.value();
        var reason = this.reasons.value();
        $.post(            
            this.opts.ChangeOrderUrl,
            {
                orderId: this.changeParameters.orderId,
                newState: value,
                reasonText: reason
            },
            $.proxy(this._onChangeOrderSuccess, this)
        );
    },
    _cancel: function (parameters) {
        this.window.close();
        if (this.changeParameters.cancelCallback) {
            this.changeParameters.cancelCallback({ ok: false });
        }
    },
    _onChangeOrderSuccess: function (data, e) {
        switch (data.ReturnCode) {
            case 0:
                alertify.success('Состояние заказа изменено');
                this.window.close();
                this.changeParameters.cancelCallback({ ok: true });
                break;
            case 1:
                alertify.alert('<b>Ошибка при выполнении операции:</b></br>' + data.Message);
                break;
        }
        if (data.ReturnCode == 1) {
            return;
        }
    }
}