﻿/// <reference path="_references.js" />
/// <reference path="kendo.ui.culture.js" />

window.Mms = window.Mms || {};
Mms.OrderMonitoring = Mms.OrderMonitoring || {};

Mms.OrderMonitoring.OrderStatusHistory = function(opts) {
    this.opts = $.extend(true, {
        DataSource: true,
        $Container: true        
    }, opts);
    this.currentCulture = kendo.ui.currentCulture;
    this.Show();
};

Mms.OrderMonitoring.OrderStatusHistory.prototype = {    
    Show: function () {
        var filterable = this.currentCulture.grid.filterable;
        var pageable = this.currentCulture.grid.pageable;
        this.grid = this.opts
            .$Container.kendoGrid({
                dataSource: this.opts.DataSource,
            scrollable: false,
            sortable: true,
            filterable: false,
            pageable: false,            
            columns: [
                {
                    field: "UpdateTime",
                    title: "Дата",
                    width: 150,
                    template: '#= kendo.toString(UpdateTime, "dd.MM.yyyy HH:mm:ss")#'
                },
                { field: "ToStatus", title: "Статус", width: 250 },
                { field: "ReasonText", title: "Комментарий", width: 350 },
                { field: "WhoChanged", title: "Пользователь", width: 250 },
                {}
            ]
        }).data('kendoGrid');
    },
    Refresh: function () {
        this.grid.dataSource.fetch();
    }
};

Mms.OrderMonitoring.OrderStatusHistoryFactory = function(opts) {
    this.opts = $.extend(true, {
        DataSourceFactory: true,        
    }, opts);
};

Mms.OrderMonitoring.OrderStatusHistoryFactory.prototype = {
    Create: function(detailRow, orderId) {
        return new Mms.OrderMonitoring.OrderStatusHistory({
            DataSource: this.opts.DataSourceFactory.Create(orderId),
            $Container: detailRow
        });
    }
};