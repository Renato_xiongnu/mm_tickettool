﻿/// <reference path="_references.js" />
/// <reference path="..\..\kendo\kendo.core.js" />
/// <reference path="..\kendo.ui.culture.js" />
/// <reference path="..\..\alertify\alertify.js" />
/// <reference path="OrderDetail.js" />
/// <reference path="OrderLines.js" />
/// <reference path="OrderTasks.js" />
/// <reference path="OrderStatusHistory.js" />
window.Mms = window.Mms || {};
Mms.OrderMonitoring = Mms.OrderMonitoring || {};

Mms.OrderMonitoring.App = function(opts) {
    this.currentCulture = kendo.ui.currentCulture;
    this.opts = $.extend(true, {
        $Container: $('body'),
        ExportToExcelBtnSelector: '#exportToExcelBtn',
        RefreshBtnSelector: '#refreshBtn',
        ResetFiltersBtnSelector: '#resetFiltersBtn',
        CustomerOrdersDataSource: false,
        GetLasOrderTaskByIdUrl: true,
        DetailTemplate: true,
        ToolbarTemplate: true,
        ExportToExcelUrl: true,
        GetOrderByIdUrl: true,
        OrderEditor: false,
        OrderCancel: false,
        OrderChangeState: false,
        OrderConfirm: false,
        OrderStop: false,
        OrderCopy: false,
        OrderLinesFactory: true,
        OrderStatusHistoryFactory: true,
        OrderTasksFactory: true,
        OrderCopiesFactory: true,
        OrderWorkflowFactory: true,
        OrderStatesDataSource: [
            "новый заказ"
        ],
        OrderSourcesDataSource: [
            "TicketTool",
            "WebSite"
        ],
        PickupTypesDataSource: [
            "Доставка",
            "Самовывоз"
        ],
        PaymentTypesDataSource: [
            "Наличные",
            "Карта",
            "Отнлайн кредит"
        ]
    }, opts);
};

Mms.OrderMonitoring.App.prototype = {
    Render: function() {
        var filterable = this.currentCulture.grid.filterable;
        var pageable = this.currentCulture.grid.pageable;
        this.$grid = this.opts.$Container
            .kendoGrid({
                scrollable: {
                    virtual: false
                },
                dataSource: this.opts.CustomerOrdersDataSource,
                //height: 700,
                detailTemplate: this.opts.DetailTemplate,
                detailInit: $.proxy(this._detailInit, this),
                dataBound: $.proxy(this._dataBound, this),
                filterable: $.extend(true, { extra: false }, filterable),
                sortable: true,
                toolbar: kendo.template($("#toolbarTemplate").html()),
                pageable: $.extend(true, { pageSizes: [5, 10, 20, 50, 100], refresh: true }, pageable),
                filterMenuInit: $.proxy(this._filterMenuInit, this),
                columns:
                [
                    {
                        field: "OrderInfoCreateData",
                        title: "Создан",
                        template: '#= kendo.toString(OrderInfoCreateData, "dd.MM.yyyy <br /> HH:mm:ss")#',
                        width: 90,
                        filterable: {
                            extra: true,
                            ui: "datepicker"
                        }
                    },
                    {
                        field: "OrderInfoOrderSource",
                        title: "Канал",
                        width: 100,
                        filterable: { extra: false, ui: $.proxy(this._orderSourcesFilter, this) }
                    },
                    {
                        field: "OrderId",
                        title: "№ заказа(сайт)",
                        width: 150,
                        filterable: { extra: false, ui: $.proxy(this._orderIdFilter, this) }
                    },
                    {
                        field: "OrderInfoOrderState",
                        title: "Состояние заказа",
                        width: 170,
                        filterable: { extra: false, ui: $.proxy(this._orderStatesFilter, this) }
                    },
                    {
                        field: "OriginaOrderId",
                        title: "Скопирован с",
                        width: 150,
                        filterable: { extra: false, ui: $.proxy(this._orderIdFilter, this) }
                    },
                    {
                        field: "CustomerFullName",
                        title: "Клиент",
                        width: 200,
                        filterable: { extra: false, ui: $.proxy(this._customerFullNameFilter, this) }
                    },
                    {
                        field: "CustomerPhone",
                        title: "Телефон",
                        width: 120,
                        filterable: { extra: false, ui: $.proxy(this._customerPhoneFilter, this) }
                    },
                    {
                        field: "CustomerEmail",
                        title: "Email",
                        width: 200,
                        filterable: { extra: false, ui: $.proxy(this._customerEmailFilter, this) }
                    },
                    {
                        field: "DeliveryDeliveryType",
                        title: "Способ получения",
                        width: 200,
                        filterable: { extra: false, ui: $.proxy(this._pickupTypesFilter, this) }
                    },
                    {
                        field: "DeliveryAddress",
                        title: "Адрес доставки",
                        width: 300,
                        filterable: { extra: false, ui: $.proxy(this._deliveryAddressFilter, this) }
                    },
                    //{ field: "Delivery.Email", title: "Дата доставки", width: 100, filterable: true },
                    {
                        field: "OrderInfoPaymentType.Id",
                        title: "Тип оплаты",
                        width: 200,
                        template: '#= OrderInfoPaymentType.NameRu#',
                        filterable: { extra: false, ui: $.proxy(this._paymentTypesFilter, this) }
                    },
                    { field: "Price", title: "Сумма", width: 100, filterable: true },
                    { field: "DiscountCardNo", title: "№ cертификата", width: 200, filterable: { extra: false, ui: $.proxy(this._stringFilter, this) } }
                    //{ field: "Delivery.Email", title: "Статус обработки", width: 100, filterable: true },
                ]
            });
        this.grid = this.$grid.data('kendoGrid');
        var self = this;
        this.grid.bind("dataBinding", $.proxy(this._distinguishFilteredColumn, this));
        this._bindToolbarButtons();
        this._distinguishFilteredColumn();
    },
    _distinguishFilteredColumn: function(parameters) {
        var filter = this.grid.dataSource.filter();
        var anyFilterChanged = false;
        for (var i = 0; i < this.grid.columns.length; i++) {
            var columnField = this.grid.columns[i].field;
            var columnEl = $('#grid th[data-field="' + columnField + '"]');
            var filterChanged = false;
            if (filter && filter.filters) {
                for (var j = 0; j < filter.filters.length; j++) {
                    var filterItem = filter.filters[j];
                    if (columnField == filterItem.field) {
                        filterChanged = true;
                        anyFilterChanged = true;
                    }
                }
            }
            columnEl.toggleClass('filterChanged', filterChanged);
        }
        $(this.opts.ResetFiltersBtnSelector).toggleClass('none', !anyFilterChanged);
        if (anyFilterChanged) {
            $(this.opts.ResetFiltersBtnSelector).show();
        } else {
            $(this.opts.ResetFiltersBtnSelector).hide();
        }
    },
    _bindToolbarButtons: function() {
        $(this.opts.RefreshBtnSelector).kendoButton({
            spriteCssClass: "k-icon k-i-refresh"
        });
        $(this.opts.ResetFiltersBtnSelector).kendoButton({
            spriteCssClass: "k-icon k-i-funnel-clear"
        });
        $(this.opts.ExportToExcelBtnSelector).click($.proxy(this.ExportToExcel, this));
        $(this.opts.RefreshBtnSelector).click($.proxy(this.Refersh, this));
        $(this.opts.ResetFiltersBtnSelector).click($.proxy(this.ResetFilters, this));
    },
    _detailInit: function(e) {
        var detailRow = e.detailRow;
        var orderId = e.data.OrderId;
        //var detail = new Mms.OrderMonitoring.OrderDetail({
        //    DataSource: this.grid.dataSource,
        //    Order: e.data,
        //    Container: e.detailRow
        //});
        this._initTabStrip(e.detailRow, orderId);
        var orderStatusHistoryTable = this.opts.OrderStatusHistoryFactory.Create(detailRow.find(".history"), orderId);
        var orderLinesTable = this.opts.OrderLinesFactory.Create(detailRow.find(".orders"), orderId);
        var orderTasksTable = this.opts.OrderTasksFactory.Create(detailRow.find(".tasks"), orderId);        
        var orderCopiesTable = this.opts.OrderCopiesFactory.Create(detailRow.find(".copies"), orderId);
        var orderWorkflowTable = this.opts.OrderWorkflowFactory.Create(detailRow.find(".workflow"), orderId);
        this._initDetailCommandButtons(detailRow, orderId, [
            orderStatusHistoryTable,
            orderLinesTable,
            orderTasksTable,
            orderCopiesTable,
            orderWorkflowTable
        ]);


    },
    _initTabStrip: function(detailRow, orderId) {
        detailRow.find(".tabstrip").kendoTabStrip({
            animation: {
                open: { effects: "fadeIn" }
            }
        });
    },
    _initDetailCommandButtons: function(detailRow, orderId, detailTables) {
        var self = this;
        kendo.init($(detailRow));

        var orderState = self.grid.dataSource.get(orderId).OrderInfoOrderState.toLowerCase();
        detailRow.find("button").each(function() {
            var states = $(this).attr('data-states').split(',');
            if (states[0] != "" && $.inArray(orderState, states) == -1) {
                $(this).css('visibility', 'hidden');
            } else {
                $(this).css('visibility', 'visible');
            }
        });

        detailRow.find('.refreshOrderBtn').click(function(e) {
            self._refreshDetailRow(detailTables);
        });
        detailRow.find('.abuseOrderBtn').click(function(e) {
            alertify.alert("Не реализовано");
        });
        detailRow.find('.changeStateBtn').click(
            $.proxy(function(e) {
                self.opts.OrderChangeState.ChangeState({
                    orderId: orderId,
                    cancelCallback:
                        function(params) {
                            if (params.ok) {
                                self._refreshDetailRow(detailTables);
                            }
                        }
                });
            }, self));
        detailRow.find('.cancelOrderBtn').click(
            $.proxy(function(e) {
                var dataSource = self.grid.dataSource;
                var order = dataSource.get(orderId);
                var orderStatus = order.OrderInfoOrderState ? order.OrderInfoOrderState.toLowerCase() : '';
                self.opts.OrderCancel.Cancel({
                    orderId: orderId,
                    orderStatus: orderStatus,
                    cancelCallback:
                        function(params) {
                            if (params.ok) {
                                kendo.ui.progress(self.$grid.find('.k-grid-content'), true);
                                self._refreshDetailRow(detailTables);
                                $.getJSON(self.opts.GetOrderByIdUrl, { orderId: orderId })
                                    .success(function(data) {
                                        kendo.ui.progress(self.$grid.find('.k-grid-content'), false);
                                        var orderUpdated = false;
                                        for (var propertyName in data) {
                                            if (!data.hasOwnProperty(propertyName)) {
                                                continue;
                                            }
                                            if (order.get(propertyName) !== data[propertyName]) {
                                                orderUpdated = true;
                                            }
                                            order.set(propertyName, data[propertyName]);
                                        }
                                        debugger;
                                        if (data.OrderInfoOrderState == "отказ") {
                                            if (data.Prepay == 'true') {
                                                alertify.alert("Заказ был отгружен в ТК. Уведомьте ТК, в которую был отгружен заказ, об отказе клиента. В том случае, если заказ был оплачен клиентом банковской картой на сайте, сообщите об отказе в отдел по работе с претензиями.");
                                            } else {
                                                alertify.alert("Заказ был отгружен в ТК. Уведомьте ТК, в которую был отгружен заказ, об отказе клиента.");
                                            }
                                        } else {
                                            alertify.success("Заказ отменен");
                                        }                                                                    
                                        self.grid.expandRow('[data-uid="' + order.uid + '"]');                                        
                                    })
                                    .error(function(data) {
                                        debugger;
                                    });

                                order.set('OrderInfoOrderState', 'отмена');
                                self.grid.expandRow('[data-uid="' + order.uid + '"]');
                                self._refreshDetailRow(detailTables);
                            }
                        }
                });
            }, self));
        detailRow.find('.editOrderBtn').click(
            $.proxy(function(e) {
                var dataSource = self.grid.dataSource;
                var order = dataSource.get(orderId);
                self.opts.OrderEditor.Edit(orderId, function(result) {
                    if (result.ReturnCode != "OK") {
                        alertify.error(result.Message);
                        return;
                    }
                    kendo.ui.progress(self.$grid.find('.k-grid-content'), true);
                    self._refreshDetailRow(detailTables);
                    $.getJSON(self.opts.GetOrderByIdUrl, { orderId: orderId })
                        .success(function(data) {
                            kendo.ui.progress(self.$grid.find('.k-grid-content'), false);
                            var orderUpdated = false;
                            for (var propertyName in data) {
                                if (!data.hasOwnProperty(propertyName)) {
                                    continue;
                                }
                                if (order.get(propertyName) !== data[propertyName]) {
                                    orderUpdated = true;
                                }
                                order.set(propertyName, data[propertyName]);
                            }
                            self.grid.expandRow('[data-uid="' + order.uid + '"]');
                            if (orderUpdated) {
                                alertify.success("Заказ обновлен");
                            }
                        })
                        .error(function(data) {
                            debugger;
                        });
                });
            }, self));
        detailRow.find('.confirmOrderBtn').click(
            $.proxy(function(e) {
                var dataSource = self.grid.dataSource;
                var order = dataSource.get(orderId);
                var orderStatus = order.OrderInfoOrderState.toLowerCase();
                self.opts.OrderConfirm.Confirm({
                    orderId: orderId,
                    orderStatus: orderStatus,
                    cancelCallback:
                        function(params) {
                            if (params.ok) {
                                self._refreshDetailRow(detailTables);
                            }
                        }
                });

            }, self));
        detailRow.find('.stopOrderBtn').click(
            $.proxy(function(e) {
                var dataSource = self.grid.dataSource;
                var order = dataSource.get(orderId);
                var orderStatus = order.OrderInfoOrderState.toLowerCase();
                self.opts.OrderStop.Stop({
                    orderId: orderId,
                    orderStatus: orderStatus,
                    cancelCallback:
                        function(params) {
                            if (params.ok) {
                                self._refreshDetailRow(detailTables);
                            }
                        }
                });

            }, self));
        detailRow.find('.copyOrderBtn').click(
            $.proxy(function(e) {
                var dataSource = self.grid.dataSource;
                var order = dataSource.get(orderId);
                var orderStatus = order.OrderInfoOrderState.toLowerCase();
                self.opts.OrderCopy.Copy({
                    orderId: orderId,
                    orderStatus: orderStatus,
                    callback:
                        function(result) {
                            if (result.ReturnCode != "OK") {
                                alertify.error(result.Message);
                                return;
                            }
                            alertify.alert("Заказа №" + orderId + " скопирован. Копии присвоен номер " + result.OrderId);
                            kendo.ui.progress(self.$grid.find('.k-grid-content'), true);
                            self._refreshDetailRow(detailTables);
                            $.getJSON(self.opts.GetOrderByIdUrl, { orderId: orderId })
                                .success(function(data) {
                                    kendo.ui.progress(self.$grid.find('.k-grid-content'), false);
                                    var orderUpdated = false;
                                    for (var propertyName in data) {
                                        if (!data.hasOwnProperty(propertyName)) {
                                            continue;
                                        }
                                        if (order.get(propertyName) !== data[propertyName]) {
                                            orderUpdated = true;
                                        }
                                        order.set(propertyName, data[propertyName]);
                                    }
                                    self.grid.expandRow('[data-uid="' + order.uid + '"]');
                                    if (orderUpdated) {
                                        alertify.success("Заказ обновлен");
                                    }
                                })
                                .error(function(data) {
                                    debugger;
                                });
                        }
                });

            }, self));
    },
    _dataBound: function(e) {

    },
    _filterMenuInit: function(e) {
        switch (e.field) {
        case "OrderInfoCreateDay":
        case "OrderInfoCreateData":
            this._initOrderInfoCreateDayFilter(e);
            break;
        case "CustomerFullName":
            this._initCustomerFullNameFilter(e);
            break;
        case "CustomerEmail":
            this._initCustomerEmailFilter(e);
            break;
        case "CustomerPhone":
            this._initCustomerPhoneFilter(e);
            break;
        case "DeliveryAddress":
            this._initDeliveryAddressFilter(e);
            break;
        case "OrderId":
            this._orderIdFilterFilter(e);
            break;
            case "DiscountCardNo":                
                this._initStringFilter(e, "contains");
            break;
        default:
        }
    },
    _initOrderInfoCreateDayFilter: function(e) {
        kendo.ui.OnInitDayFilter(e);
    },
    _initCustomerFullNameFilter: function(e) {
        this._initStringFilter(e, "startswith");
    },

    _initCustomerEmailFilter: function(e) {
        this._initStringFilter(e, "startswith");
    },

    _initCustomerPhoneFilter: function(e) {
        this._initStringFilter(e, "contains");
    },

    _orderIdFilterFilter: function(e) {
        this._initStringFilter(e, "contains");
    },

    _initDeliveryAddressFilter: function(e) {
        this._initStringFilter(e, "contains");
    },
    _initStringFilter: function(e, operation) {
        var dropDown = e.container
            .find('select[data-bind="value: filters[0].operator"]')
            .data("kendoDropDownList");
        dropDown.value(operation);
        dropDown.trigger('change');
        dropDown.element.trigger('change');
    },
    _refreshDetailRow: function(detailTables) {
        for (var i = 0; i < detailTables.length; i++) {
            detailTables[i].Refresh();
        }
    },

    ExportToExcel: function() {
        var exportRequest = {
            filter: this.grid.dataSource.filter() || {},
            sort: this.grid.dataSource.sort() || []
        };
        var formTemplate =
            '<form id="exportToXls" action="' + this.opts.ExportToExcelUrl + '" method="POST" target="_blank">\
                    <input type="hidden" name="filter"/>\
                    <input type="hidden" name="sort"/>\
                </form>';
        var $form = $(formTemplate);
        $("body").append($form); //$form.offset({ top: -30000, left: -30000 });
        $form.submit(function(parameters) {
            if (exportRequest.filter) {
                $('input[name="filter"]', $form).val(kendo.stringify(exportRequest.filter));
            }
            if (exportRequest.sort) {
                $('input[name="sort"]', $form).val(kendo.stringify(exportRequest.sort));
            }
        });
        $form.submit();
    },

    ResetFilters: function() {
        this.grid.dataSource.filter({});
        this.grid.dataSource.fetch();
    },

    Refersh: function() {
        this.grid.dataSource.fetch();
    },

    _orderStatesFilter: function(element) {
        var parent = element.parent();
        $('[data-bind="value: filters[0].operator"]:first', parent).remove();
        element.kendoDropDownList({
            dataSource: this.opts.OrderStatesDataSource,
            optionLabel: "--Выберите--"
        });
    },

    _orderIdFilter: function(element) {
        this._stringFilter(element);
    },

    _customerFullNameFilter: function(element) {
        this._stringFilter(element);
    },

    _customerEmailFilter: function(element) {
        this._stringFilter(element);
    },

    _customerPhoneFilter: function(element) {
        this._stringFilter(element);
    },

    _deliveryAddressFilter: function(element) {
        this._stringFilter(element);
    },

    _stringFilter: function(element) {
        var parent = element.parent();
        $('[value="neq"]', parent).remove();
        $('[value="eq"]', parent).remove();
        $('input').addClass('k-textbox');
    },

    _orderSourcesFilter: function(element) {
        var parent = element.parent();
        $('[data-bind="value: filters[0].operator"]:first', parent).remove();
        element.kendoDropDownList({
            dataSource: this.opts.OrderSourcesDataSource,
            optionLabel: "--Выберите--"
        });
    },
    _pickupTypesFilter: function(element) {
        var parent = element.parent();
        $('[data-bind="value: filters[0].operator"]:first', parent).remove();
        element.kendoDropDownList({
            dataSource: this.opts.PickupTypesDataSource,
            optionLabel: "--Выберите--"
        });
    },
    _paymentTypesFilter: function(element) {
        var parent = element.parent();
        $('[data-bind="value: filters[0].operator"]:first', parent).remove();
        element.kendoDropDownList({
            dataTextField: "Name",
            dataValueField: "Id",
            dataSource: this.opts.PaymentTypesDataSource,
            optionLabel: "--Выберите--"
        });
    }
};