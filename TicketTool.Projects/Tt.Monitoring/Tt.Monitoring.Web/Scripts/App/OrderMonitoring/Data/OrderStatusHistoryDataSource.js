﻿/// <reference path="jquery-2.0.3.js" />

window.Mms = window.Mms || {};
Mms.OrderMonitoring = Mms.OrderMonitoring || {};
Mms.OrderMonitoring.Data = Mms.OrderMonitoring.Data || {};

Mms.OrderMonitoring.Data.OrderStatusHistoryDataSource = function (opts) {
    var dataSource = $.extend(true, {
        type: "json",
        transport: {
            read:
            {
                url: '',
                type: "POST",
                dataType: "json",
                contentType: "application/json; charset=utf-8"
            },
            parameterMap: function (options) {
                return JSON.stringify(options);
            }
        },
        schema: {
            total: "Total",
            data: "Data",
            model: {
                fields: {
                    "UpdateTime": { type: "date" },
                    "WhoChanged": { type: "string" },
                    "ReasonText": { type: "string" },
                    "ChangedType": { type: "string" },
                    "FromStatus": { type: "string" },
                    "ToStatus": { type: "string" },
                }
            }
        },
        sort: [{ field: "UpdateTime", dir: "desc" }],
        serverPaging: false,
        serverSorting: false,
        serverFiltering: true
    }, opts);
    return dataSource;
}