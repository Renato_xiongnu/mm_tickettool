﻿/// <reference path="../../../jquery-2.0.3.js" />

window.Mms = window.Mms || {};
Mms.OrderMonitoring = Mms.OrderMonitoring || {};
Mms.OrderMonitoring.Data = Mms.OrderMonitoring.Data || {};

Mms.OrderMonitoring.Data.CustomerOrdersDataSource = function (opts) {    
    var dataSource = $.extend(true,{
        type: "json",
        transport: {
            read:
            {
                url: '',
                type: "POST",
                dataType: "json",
                contentType: "application/json; charset=utf-8"
            },
            parameterMap: function (options) {
                return JSON.stringify(options);
            }
        },
        schema: {
            total: "Total",
            data: "Data",            
            model: {
                id: "OrderId",
                fields: {
                    OrderId: { type: "string" },
                    OriginaOrderId: { type: "string" },
                    'OrderInfoOrderSource': { type: "string" },
                    'OrderInfoOrderState': { type: "string" },
                    'CustomerFullName': { type: "string" },
                    'CustomerPhone': { type: "string" },
                    'CustomerEmail': { type: "string" },
                    'OrderInfoCreateDay': { type: "date" },
                    'OrderInfoCreateData': { type: "date" },                    
                    'OrderInfoPaymentType.Id': { type: "string" },
                    'DeliveryAddress': { type: "string" },
                    'DeliveryDeliveryType': { type: "string" },
                    'Price': { type: "number" },
                    'DiscountCardNo': { type: "string" },
                    'Prepay': { type: "string" },
                }
            }
        },
        sort: [{ field: "OrderInfoCreateDay", dir: "desc" }],
        pageSize: 20,
        serverPaging: true,
        serverFiltering: true,
        serverGrouping: true,
        serverSorting: true,
    }, opts);
    return dataSource;
}