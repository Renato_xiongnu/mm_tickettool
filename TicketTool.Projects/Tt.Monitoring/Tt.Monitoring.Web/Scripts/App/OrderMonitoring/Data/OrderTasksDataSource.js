﻿/// <reference path="jquery-2.0.3.js" />

window.Mms = window.Mms || {};
Mms.OrderMonitoring = Mms.OrderMonitoring || {};
Mms.OrderMonitoring.Data = Mms.OrderMonitoring.Data || {};

Mms.OrderMonitoring.Data.OrderTasksDataSource = function (opts) {
    var dataSource = $.extend(true, {
        type: "json",
        transport: {
            read:
            {
                url: '',
                type: "POST",
                dataType: "json",
                contentType: "application/json; charset=utf-8"
            },
            parameterMap: function (options) {
                return JSON.stringify(options);
            }
        },
        schema: {
            total: "Total",
            data: "Data",
            model: {
                fields: {
                    "Name": { type: "string" },
                    "Description": { type: "string" },
                    "Outcome": { type: "string" },
                    "Created": { type: "date" },
                    "Completed": { type: "date" },
                    "Escalated": { type: "boolean" },
                    "Performer": { type: "string" },
                    "SkillSetName": { type: "string" },
                }
            }
        },
        sort: [{ field: "Created", dir: "desc" }],
        serverPaging: false,
        serverSorting: false,
        serverFiltering: true,        
    }, opts);
    return dataSource;
}