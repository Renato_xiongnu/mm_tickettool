﻿/// <reference path="../../../jquery-2.0.3.js" />
/// <reference path="OrderLinesDataSource.js" />

window.Mms = window.Mms || {};
Mms.OrderMonitoring = Mms.OrderMonitoring || {};
Mms.OrderMonitoring.Data = Mms.OrderMonitoring.Data || {};

Mms.OrderMonitoring.Data.OrderLinesDataSourceFactory = function(opts) {
    this.opts = $.extend(true, {
        readUrl: ''
    }, opts);
    return this;
};

Mms.OrderMonitoring.Data.OrderLinesDataSourceFactory.prototype = {
    Create: function(orderId) {
        return new Mms.OrderMonitoring.Data.OrderLinesDataSource({
            transport: {
                read:
                {
                    url: this.opts.readUrl,
                    data: { OrderId: orderId },
                },
            }
        });
    }
}