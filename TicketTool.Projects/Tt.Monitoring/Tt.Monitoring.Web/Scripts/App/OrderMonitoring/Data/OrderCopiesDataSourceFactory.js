﻿/// <reference path="../../../jquery-2.0.3.js" />
/// <reference path="OrderLinesDataSource.js" />

window.Mms = window.Mms || {};
Mms.OrderMonitoring = Mms.OrderMonitoring || {};
Mms.OrderMonitoring.Data = Mms.OrderMonitoring.Data || {};

Mms.OrderMonitoring.Data.OrderCopiesDataSourceFactory = function (opts) {
    this.opts = $.extend(true, {
        readUrl: ''
    }, opts);
    return this;
};

Mms.OrderMonitoring.Data.OrderCopiesDataSourceFactory.prototype = {
    Create: function(orderId) {
        return new Mms.OrderMonitoring.Data.OrderCopiesDataSource({
            transport: {
                read:
                {
                    url: this.opts.readUrl,
                    data: { OrderId: orderId },
                },
            }
        });
    }
}