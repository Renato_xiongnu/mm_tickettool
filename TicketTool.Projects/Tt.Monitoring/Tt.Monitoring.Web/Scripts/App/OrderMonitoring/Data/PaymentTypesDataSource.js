﻿/// <reference path="../../../jquery-2.0.3.js" />

window.Mms = window.Mms || {};
Mms.OrderMonitoring = Mms.OrderMonitoring || {};
Mms.OrderMonitoring.Data = Mms.OrderMonitoring.Data || {};

Mms.OrderMonitoring.Data.PaymentTypesDataSource = function (opts) {
    var dataSource = $.extend(true, {
        type: "json",
        transport: {
            read:
            {
                url: '',
                type: "Get",
                dataType: "json",
                contentType: "application/json; charset=utf-8"
            },
            parameterMap: function (options) {
                return JSON.stringify(options);
            }
        },
        schema: {            
            model: {
                fields: {
                    Id: { type: "string" },
                    'Name': { type: "string" }                    
                }
            }
        },        
        serverPaging: false,        
        serverGrouping: false,
        serverFiltering: true
    }, opts);
    return  dataSource;
}