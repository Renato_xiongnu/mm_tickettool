﻿/// <reference path="../../../jquery-2.0.3.js" />
/// <reference path="OrderWorkflowDataSource.js" />

window.Mms = window.Mms || {};
Mms.OrderMonitoring = Mms.OrderMonitoring || {};
Mms.OrderMonitoring.Data = Mms.OrderMonitoring.Data || {};

Mms.OrderMonitoring.Data.OrderWorkflowDataSourceFactory = function (opts) {
    this.opts = $.extend(true, {
        readUrl: ''
    }, opts);
    return this;
};

Mms.OrderMonitoring.Data.OrderWorkflowDataSourceFactory.prototype = {
    Create: function (orderId) {
        return new Mms.OrderMonitoring.Data.OrderWorkflowDataSource({
            transport: {
                read:
                {
                    url: this.opts.readUrl,
                    data: { OrderId: orderId },
                },
            }
        });
    }
}