﻿/// <reference path="../../../jquery-2.0.3.js" />
/// <reference path="OrderStatusHistoryDataSource.js" />

window.Mms = window.Mms || {};
Mms.OrderMonitoring = Mms.OrderMonitoring || {};
Mms.OrderMonitoring.Data = Mms.OrderMonitoring.Data || {};

Mms.OrderMonitoring.Data.OrderStatusHistoryDataSourceFactory = function (opts) {    
    this.opts = $.extend(true, {
        readUrl: ''
    }, opts);    
};

Mms.OrderMonitoring.Data.OrderStatusHistoryDataSourceFactory.prototype = {
    Create: function (orderId) {        
        return new Mms.OrderMonitoring.Data.OrderStatusHistoryDataSource({
            transport: {
                read:
                {
                    url: this.opts.readUrl,
                    data: { OrderId: orderId },
                },
            }
        });
    }
}