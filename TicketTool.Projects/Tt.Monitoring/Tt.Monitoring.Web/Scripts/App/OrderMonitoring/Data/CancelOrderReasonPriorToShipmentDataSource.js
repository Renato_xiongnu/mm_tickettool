﻿/// <reference path="../../../jquery-2.0.3.js" />

window.Mms = window.Mms || {};
Mms.OrderMonitoring = Mms.OrderMonitoring || {};
Mms.OrderMonitoring.Data = Mms.OrderMonitoring.Data || {};

Mms.OrderMonitoring.Data.CancelOrderReasonPriorToShipmentDataSource = function (opts) {
    var dataSource = $.extend(true, {
        type: "json",
        transport: {
            read:
            {
                url: '',
                type: "Get",
                dataType: "json",
                contentType: "application/json; charset=utf-8"
            },
        }
    }, opts);
    return dataSource;
}