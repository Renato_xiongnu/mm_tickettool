﻿/// <reference path="../../../jquery-2.0.3.js" />
/// <reference path="OrderTasksDataSource.js" />

window.Mms = window.Mms || {};
Mms.OrderMonitoring = Mms.OrderMonitoring || {};
Mms.OrderMonitoring.Data = Mms.OrderMonitoring.Data || {};

Mms.OrderMonitoring.Data.OrderTasksDataSourceFactory = function (opts) {
    this.opts = $.extend(true, {
        readUrl: ''
    }, opts);
    return this;
};

Mms.OrderMonitoring.Data.OrderTasksDataSourceFactory.prototype = {
    Create: function (orderId) {
        return new Mms.OrderMonitoring.Data.OrderTasksDataSource({
            transport: {
                read:
                {
                    url: this.opts.readUrl,
                    data: { OrderId: orderId },
                },
            }
        });
    }
}