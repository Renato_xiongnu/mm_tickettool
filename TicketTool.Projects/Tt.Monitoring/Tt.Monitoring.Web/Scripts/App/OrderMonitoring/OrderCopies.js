﻿/// <reference path="_references.js" />
/// <reference path="kendo.ui.culture.js" />

window.Mms = window.Mms || {};
Mms.OrderMonitoring = Mms.OrderMonitoring || {};

Mms.OrderMonitoring.OrderCopies = function (opts) {
    this.opts = $.extend(true, {
        DataSource: true,
        $Container: true        
    }, opts);
    this.currentCulture = kendo.ui.currentCulture;
    this.Show();
};

Mms.OrderMonitoring.OrderCopies.prototype = {
    Show: function () {        
        this.grid = this.opts
            .$Container.kendoGrid({
                dataSource: this.opts.DataSource,
                scrollable: true,
                sortable: true,                
                filterable: false,
                pageable: false,                
                columns: [
                    { field: "OrderId", title: "№ заказа(сайт)", width: 150 }                   
                ]
            }).data('kendoGrid');
    },
    Refresh: function () {
        this.grid.dataSource.fetch();
    }
};

Mms.OrderMonitoring.OrderCopiesFactory = function (opts) {
    this.opts = $.extend(true, {
        DataSourceFactory: true,        
    }, opts);
};

Mms.OrderMonitoring.OrderCopiesFactory.prototype = {
    Create: function(detailRow, orderId) {
        return new Mms.OrderMonitoring.OrderCopies({
            DataSource: this.opts.DataSourceFactory.Create(orderId),
            $Container: detailRow
        });
    }
};