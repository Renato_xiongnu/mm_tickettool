﻿// <reference path="_references.js" />
/// <reference path="kendo.ui.culture.js" />

window.Mms = window.Mms || {};
Mms.OrderMonitoring = Mms.OrderMonitoring || {};

Mms.OrderMonitoring.CustomerOrderCopy = function (opts) {
    this.opts = $.extend(true, {
        OrderEditCopy: false,
        CancelOrderUrl: '',
        WindowName: 'Копировать заказ №',
        $Container: $('body'),
        CancelOrderReasonPriorToShipmentDataSource: true,
        CancelOrderReasonAfterShipmentDataSource: true,
        DropDownOptionLabel: '--Выберите--'
    }, opts);
    this.opts.$Container.append(this.$window);
};

Mms.OrderMonitoring.CustomerOrderCopy.prototype = {
    Copy: function (copyParameters) {
        this.copyParameters = copyParameters;
        if (this.window) {
            this._updateCopyOrderWindow();
            this.window.open();
            this.window.center();
            return;
        }
        this.window = this._createConfirmOrderWindow();
        this.window.open();
        this.window.center();
    },
    _updateCopyOrderWindow: function () {
        this.window.setOptions({
            title: this.opts.WindowName + this.copyParameters.orderId,
        });
        this.window.refresh({
            url: this.opts.EditOrderUrl,
            data: { orderId: this.copyParameters.orderId }
        });
    },
    _createConfirmOrderWindow: function () {
        var self = this;
        var $window = $('<div />');
        this.opts.$Container.append($window);
        return $window
            .kendoWindow({
                modal: true,
                content: {
                    template:
                        '<ul>\
                            <li>\
                                <label>\
                                    Вы действительно хотите сделать копию заказа?\
                                </label>\
                            </li>\
                            <li >\
                                <label style="margin-top: 15px;">\
                                    <input type="checkbox" id="cancelOrder" />\
                                    Отменить заказ перед копированием?\
                                </label>\
                            </li>\
                            <li id="reasonContainer" style="display: none;">\
                                <label for="reason" class="required">Причина отмены:</label>\
                                <select  class="dropDown" name="reason" />\
                                <span class="k-invalid-msg" data-for="reason"></span>\
                            </li>\
                         </ul>\
                         <p></p>\
                         <div class="toolbar">\
                            <button class="okBtn k-button">Копировать</button>\
                            <button class="cancelBtn k-button">Отмена</button>\
                          </div>'
                },
                iframe: false,
                position: { top: 100 },
                width: "450px",                
                actions: [],
                resizable: false,
                title: this.opts.WindowName + this.copyParameters.orderId,
                open: $.proxy(function (e) {
                    var self = this;
                    var el = e.sender.element;
                    this.reasons = el.find('.dropDown').kendoDropDownList({
                        dataSource: self._getCancelOrderReasonsDataSource(),
                        index: 0,
                        optionLabel: this.opts.DropDownOptionLabel,
                        change: function () {
                            self.validator.validate();
                        }
                    }).data("kendoDropDownList");
                    $(this.reasons.list).width("auto");
                    el.find('.okBtn').click($.proxy(this._copyOrder, this));
                    self.cancelOrderChk = el.find('#cancelOrder');
                    el.find('#cancelOrder').on('click', $.proxy(function(e) {
                        var checked = self.cancelOrderChk.prop('checked');
                        if (checked) {
                            el.find('#reasonContainer').show();
                            el.find('#reasonContainer').attr("required","true");
                        } else {
                            el.find('#reasonContainer').hide();
                            el.find('#reasonContainer').removeAttr("required");
                        }
                    }, this));
                    el.find('.cancelBtn').click($.proxy(this._cancel, this));
                    this.validator = $(el).kendoValidator({
                        rules: {
                            customRequired: function (input) {
                                var cancelOrder = self.cancelOrderChk.prop('checked');                                
                                if (input.is("[name=reason]")) {                                    
                                    return !cancelOrder || input.val() != "";                                    
                                }

                                return true;
                            }
                        },
                        messages: {
                            required: "Обязательное поле",
                            customRequired: "Обязательное поле",
                        },
                    }).data("kendoValidator");
                }, this)
            }).data("kendoWindow");
    },
    _copyOrder: function(parameters) {
        if (this.validator.validate()) {
            var cancelOrder = this.cancelOrderChk.prop('checked');
            var reason = this.reasons.value();
            if (cancelOrder) {
                $.post(
                    this.opts.CancelOrderUrl,
                    {
                        orderId: this.copyParameters.orderId,
                        reasonText: reason
                    },
                    $.proxy(this._onCancelOrderSuccess, this)
                );
            } else {
                this.window.close();
                this._editCopyOrder();
                return;
            }
        }
    },
    _cancel: function (parameters) {
        this.window.close();
        if (this.copyParameters.callback) {
            this.copyParameters.callback({ ok: false });
        }
    },
    _getCancelOrderReasonsDataSource: function (parameters) {
        var orderStatus = this.copyParameters.orderStatus;
        switch (orderStatus.toLowerCase()) {
            case 'разукомплектовано':
            case 'доставляется клиенту':
            case 'хранение':
            case 'передано на тс':
            case 'доставлено':
                return this.opts.CancelOrderReasonAfterShipmentDataSource;
            default:
                return this.opts.CancelOrderReasonPriorToShipmentDataSource;
        }
    },
    _onCancelOrderSuccess: function (data, e) {        
        switch (data.ReturnCode) {
            case 0:
                alertify.success('Успешно отменили заказ');
                this.window.close();
                this._editCopyOrder();
                break;
            case 1:
                alertify.alert('<b>Ошибка при выполнении операции:</b></br>' + data.Message);
                break;
        }
        if (data.ReturnCode == 1) {
            return;
        }
    },
    _editCopyOrder: function () {        
        this.opts.OrderEditCopy.Edit(this.copyParameters.orderId, $.proxy(this._onCopied, this));
    },
    _onCopied: function (parameters) {        
        if (parameters.ReturnCode != 'OK' || !parameters.OrderId) {
            this.copyParameters.callback({
                ReturnCode: 'Error',
                Message:'Заказ не был скопирован'
            });
            return;
        }        
        this.copyParameters.callback(parameters);
    }
}