﻿(function () {
    var offset = new Date();
    $.cookie("TimeOffset", offset.getTimezoneOffset()*(-1), { expires: 365 });
})()