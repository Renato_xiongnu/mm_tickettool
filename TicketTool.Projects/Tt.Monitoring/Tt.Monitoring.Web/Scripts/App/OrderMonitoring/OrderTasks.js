﻿/// <reference path="_references.js" />
/// <reference path="kendo.ui.culture.js" />

window.Mms = window.Mms || {};
Mms.OrderMonitoring = Mms.OrderMonitoring || {};

Mms.OrderMonitoring.OrderTasks = function(opts) {
    this.opts = $.extend(true, {
        DataSource: true,
        $Container: true
    }, opts);
    this.currentCulture = kendo.ui.currentCulture;
    this.Show();
};

Mms.OrderMonitoring.OrderTasks.prototype = {
    Show: function() {
        var filterable = this.currentCulture.grid.filterable;
        var pageable = this.currentCulture.grid.pageable;
        this.grid = this.opts
            .$Container.kendoGrid({
                dataSource: this.opts.DataSource,                
                scrollable: false,
                sortable: true,
                filterable:  false,
                pageable: false,
                columns: [
                    { field: "Name", title: "Название", width: 250 },                    
                    { field: "Outcome", title: "Исход", width: 150 },
                    {
                        field: "FormattedFilledForm",
                        title: "Параметры",
                        template:
                            '# if(FormattedFilledForm) { #\
                                # for(var key in FormattedFilledForm) { #\
                                    # if(!FormattedFilledForm.hasOwnProperty(key)) continue; #\
                                    # if(key=="_events"||key=="uid"||key =="parent") continue; #\
                                    <p style="margin:0px"><b>#= key #</b>\
                                    </br>#= FormattedFilledForm[key] # </p>\
                                # } #\
                             # } #',
                        width: 200
                    },
                    {
                        field: "Created",
                        title: "Создана",
                        template: '#= kendo.toString(Created, "dd.MM.yyyy <br /> HH:mm:ss")#',
                        width: 90
                    },
                    {
                        field: "Completed",
                        title: "Завершена",
                        template: '#= Completed?kendo.toString(Completed, "dd.MM.yyyy <br /> HH:mm:ss"):"Не завершена"#',
                        width: 100
                    },
                    {
                        field: "Escalated",
                        title: "Эскалация",
                        template: "#= Escalated ? 'да' : '' #",
                        width: 95
                    },
                    { field: "Performer", title: "Исполнитель", width: 150 },
                    { field: "SkillSetName", title: "Скилсет", width: 150 },                    
                    { field: "Description", title: "Описание"},
                    
                ]
            }).data('kendoGrid');
    },
    Refresh: function() {
        this.grid.dataSource.fetch();
    },
    GetLastTask: function() {
        debugger;
        return this.grid.dataSource.data();
    }
};

Mms.OrderMonitoring.OrderTasksFactory = function(opts) {
    this.opts = $.extend(true, {
        DataSourceFactory: true,
    }, opts);
};

Mms.OrderMonitoring.OrderTasksFactory.prototype = {
    Create: function(detailRow, orderId) {
        return new Mms.OrderMonitoring.OrderTasks({
            DataSource: this.opts.DataSourceFactory.Create(orderId),
            $Container: detailRow
        });
    }
};