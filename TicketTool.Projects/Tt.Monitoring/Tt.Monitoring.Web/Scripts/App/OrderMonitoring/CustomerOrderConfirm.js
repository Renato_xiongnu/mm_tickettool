﻿// <reference path="_references.js" />
/// <reference path="kendo.ui.culture.js" />

window.Mms = window.Mms || {};
Mms.OrderMonitoring = Mms.OrderMonitoring || {};

Mms.OrderMonitoring.CustomerOrderConfirm = function (opts) {
    this.opts = $.extend(true, {
        ConfirmOrderUrl: '',
        WindowName: 'Подтверждение заказа №',
        $Container: $('body')        
    }, opts);
    this.opts.$Container.append(this.$window);
};

Mms.OrderMonitoring.CustomerOrderConfirm.prototype = {
    Confirm: function (cancelParameters) {
        this.cancelParameters = cancelParameters;
        if (this.window) {
            this._updateConfirmOrderWindow();
            this.window.open();
            this.window.center();
            return;
        }
        this.window = this._createConfirmOrderWindow();
        this.window.open();
        this.window.center();
    },
    _updateConfirmOrderWindow: function () {
        this.window.setOptions({
            title: this.opts.WindowName + this.cancelParameters.orderId,
        });
        this.window.refresh({
            url: this.opts.EditOrderUrl,
            data: { orderId: this.cancelParameters.orderId }
        });
    },
    _createConfirmOrderWindow: function () {
        var self = this;
        var $window = $('<div />');
        this.opts.$Container.append($window);
        return $window
            .kendoWindow({
                modal: true,
                content: {
                    template:
                        '<ul>\
                            <li>\
                                Вы действительно хотите подтвердить заказ?\
                            </li>\
                         </ul>\
                         <p></p>\
                         <div class="toolbar">\
                            <button class="okBtn k-button">Подтвердить</button>\
                            <button class="cancelBtn k-button">Отмена</button>\
                          </div>'
                },
                iframe: false,
                position: { top: 100 },
                width: "450px",
                height: '100px',
                actions: [],
                resizable: false,
                title: this.opts.WindowName + this.cancelParameters.orderId,
                open: $.proxy(function (e) {
                    var self = this;
                    var el = e.sender.element;                    
                    el.find('.okBtn').click($.proxy(this._cancelOrder, this));
                    el.find('.cancelBtn').click($.proxy(this._cancel, this));                    
                }, this)
            }).data("kendoWindow");
    },
    _cancelOrder: function (parameters) {
        $.post(
            this.opts.ConfirmOrderUrl,
            {
                orderId: this.cancelParameters.orderId,
            },
            $.proxy(this._onCancelOrderSuccess, this)
        );
    },
    _cancel: function (parameters) {
        this.window.close();
        if (this.cancelParameters.cancelCallback) {
            this.cancelParameters.cancelCallback({ ok: false });
        }
    },    
    _onCancelOrderSuccess: function (data, e) {        
        switch (data.ReturnCode) {
            case 0:
                alertify.success('Успешно подтвердили');
                this.window.close();
                this.cancelParameters.cancelCallback({ ok: true });
                break;
            case 1:
                alertify.alert('<b>Ошибка при выполнении операции:</b></br>' + data.Message);
                break;
        }
        if (data.ReturnCode == 1) {
            return;
        }
    }
}