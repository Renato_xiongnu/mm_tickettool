﻿/// <reference path="_references.js" />
/// <reference path="kendo.ui.culture.js" />

window.Mms = window.Mms || {};
Mms.OrderMonitoring = Mms.OrderMonitoring || {};

Mms.OrderMonitoring.OrderDetail = function (opts) {
    this.opts = $.extend(true, {
        DataSource: true,
        $Container: true
    }, opts);
    this.currentCulture = kendo.ui.currentCulture;
    this.Show();
};

Mms.OrderMonitoring.OrderDetail.prototype = {
    Show: function () {
        
    },
    Refresh: function () {
        this.grid.dataSource.fetch();
    },
    GetLastTask: function () {
        debugger;
        return this.grid.dataSource.data();
    }
};

Mms.OrderMonitoring.OrderDetailFactory = function (opts) {
    this.opts = $.extend(true, {
        DataSourceFactory: true,
    }, opts);
};

Mms.OrderMonitoring.OrderDetailFactory.prototype = {
    Create: function (detailRow, orderId) {
        return new Mms.OrderMonitoring.OrderTasks({
            DataSource: this.opts.DataSourceFactory.Create(orderId),
            $Container: detailRow
        });
    }
};