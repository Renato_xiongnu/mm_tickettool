﻿(function () {
    function removeFiltersForField(expression, field) {
        if (expression.filters) {
            expression.filters = $.grep(expression.filters, function (filter) {
                removeFiltersForField(filter, field);
                if (filter.filters) {
                    return filter.filters.length;
                } else {
                    return filter.field != field;
                }
            });
        }
    }

    kendo.ui.FilterMenu.prototype._merge = function (expression) {
        //debugger;
        var that = this,
            logic = expression.logic || "and",
            filters = expression.filters,
            filter,
            result = that.dataSource.filter() || { filters: [], logic: "and" },
            idx,
            length;
        removeFiltersForField(result, that.field);
        var array = {};
        if (filters.length == 2 && (filters[0].operator == 'emp' || filters[0].operator == 'nemp')) {            
            if (filters[0].operator == 'emp') {
                logic = expression.logic = "or";
                filters[0].operator = "eq";
                filters[0].value = null;
                filters[1].operator = "eq";
                filters[1].value = "";
            }
            if (filters[0].operator == 'nemp') {
                logic = expression.logic = "and";
                filters[0].operator = "neq";
                filters[0].value = null;
                filters[1].operator = "neq";
                filters[1].value = "";
            }
        } else {
            filters = $.grep(filters, function (filter) {                
                var r = filter.value !== "" && filter.value != null || (filter.operator == "eq" || filter.operator == "neq");
                if (r && (filter.value == "" || filter.value == null)) {
                    var key = filter.operator + '_' + filter.field + '_' + filter.value;
                    if (!array[key]) {
                        array[key] = filter;
                        return false;
                    }
                }
                //if (r && (filter.operator == "eq" || filter.operator == "neq") && filter.value == 'null') {
                //    filter.value = null;
                //}
                return r;
            });
        }

        for (idx = 0, length = filters.length; idx < length; idx++) {
            filter = filters[idx];
            filter.value = that._parse(filter.value);
        }

        if (filters.length) {
            if (result.filters.length) {
                expression.filters = filters;

                if (result.logic !== "and") {
                    result.filters = [{ logic: result.logic, filters: result.filters }];
                    result.logic = "and";
                }

                if (filters.length > 1) {
                    result.filters.push(expression);
                } else {
                    result.filters.push(filters[0]);
                }
            } else {
                result.filters = filters;
                result.logic = logic;
            }
        }

        return result;
    };

    kendo.ui.OnInitDayFilter = function(e) {
        var firstDatePicker = e.container
            .find('input[data-bind="value:filters[0].value"]')
            .data("kendoDatePicker");
        var firstDropDown = e.container
            .find('select[data-bind="value: filters[0].operator"]')
            .data("kendoDropDownList");
        $('.k-dropdown', e.container).hide();
        var firstDateInput = e.container
            .find('input[data-bind="value:filters[0].value"]');
        var secondDatePicker = e.container
            .find('input[data-bind="value: filters[1].value"]')
            .data("kendoDatePicker");
        var secondDropDown = e.container
            .find('select[data-bind="value: filters[1].operator"]')
            .data("kendoDropDownList");
        var menuVisible = false;
        var filterVisibled = function(parameters) {
            var firstDate = firstDatePicker.value();
            var secondDate = secondDatePicker.value();
            if (!firstDate && !secondDate) {
                return;
            }
            var firstOperation = firstDropDown.value();
            if (firstDate && !secondDate && firstOperation === 'lte') {
                firstDatePicker.value(null);
                firstDatePicker.trigger('change');
                secondDatePicker.value(firstDate);
                secondDatePicker.trigger('change');
            }
        };
        var visibilityCheck = function(p) {
            if (!menuVisible && e.container.is(':visible')) {
                menuVisible = true;
                filterVisibled();
                return;
            }
            if (menuVisible && !e.container.is(':visible')) {
                menuVisible = false;
                return;
            }
        };
        var dateChanged = function() {
            var firstDate = firstDatePicker.value();
            var secondDate = secondDatePicker.value();
            if (secondDate) {
                var hours = secondDate.getHours();
                if (hours != 23) {
                    //secondDate.setUTCDate(secondDate.getUTCDate() + 1);
                    secondDate.setHours(23, 59, 59, 999);
                    secondDatePicker.value(secondDate);
                    secondDatePicker.trigger('change');
                }
            }
            if (firstDate && secondDate) {
                firstDropDown.value('gte');
                firstDropDown.trigger('change');
                secondDropDown.value('lte');
                secondDropDown.trigger('change');
                return;
            }
            if (firstDate && !secondDate) {
                firstDropDown.value('gte');
                firstDropDown.trigger('change');
                secondDropDown.value('eq');
                secondDropDown.trigger('change');
                return;
            }
            if (!firstDate && secondDate) {
                firstDropDown.value('eq');
                firstDropDown.trigger('change');
                secondDropDown.value('lte');
                secondDropDown.trigger('change');
                return;
            }
        };
        firstDatePicker.bind('change', dateChanged);
        secondDatePicker.bind('change', dateChanged);
        window.setInterval($.proxy(visibilityCheck, this), 100);
    };

    kendo.ui.OnInitEmptyFilter = function (e, operation) {
        var dropDown = e.container
            .find('select[data-bind="value: filters[0].operator"]')
            .data("kendoDropDownList");
        if (!dropDown)
            return;
        dropDown.value(operation);
        dropDown.trigger('change');
        dropDown.element.trigger('change');
        var menuVisible = true;
        var ds = e.sender.dataSource;
        var findFiltersByField = function (filter, field, filters) {            
            if (filter.filters) {
                for (var i = 0; i < filter.filters.length; i++) {
                    findFiltersByField(filter.filters[i], field, filters);
                }
                return;
            }
            if (filter.field) {                
                if (filter.field == field) {
                    filters.push(filter);
                }
            }
        };
        var filterVisibled = function (parameters) {
            var filter = ds.filter();
            if (!filter) {
                return;
            }            
            var filters = [];            
            findFiltersByField(filter, e.field, filters);
            if (filters.length!=2) {
                return;
            }
            var first = filters[0];
            var last = filters[1];
            if (first.operator != 'eq' && first.operator != 'neq' && first.value != '' && first.value != null) {
                return;
            }
            if (last.operator != 'eq' && last.operator != 'neq' && last.value != '' && last.value != null) {
                return;
            }
            if (first.operator != last.operator) {
                return;
            }
            if (first.operator == 'eq') {
                dropDown.value('emp');
                dropDown.trigger('change');
                dropDown.element.trigger('change');
            }
            if (first.operator == 'neq') {
                dropDown.value('nemp');
                dropDown.trigger('change');
                dropDown.element.trigger('change');
            }
        };
        var visibilityCheck = function (p) {
            if (!menuVisible && e.container.is(':visible')) {
                menuVisible = true;
                filterVisibled();
                return;
            }
            if (menuVisible && !e.container.is(':visible')) {
                menuVisible = false;
                return;
            }
        };
        window.setInterval($.proxy(visibilityCheck, this), 100);
    };

})();