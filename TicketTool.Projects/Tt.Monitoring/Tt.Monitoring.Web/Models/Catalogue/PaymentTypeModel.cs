﻿namespace TicketTool.Monitoring.Web.Models.Catalogue
{
    public class PaymentTypeModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string NameRu { get; set; }

        public string Code { get; set; }
    }
}