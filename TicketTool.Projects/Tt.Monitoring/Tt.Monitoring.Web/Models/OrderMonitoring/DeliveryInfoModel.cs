﻿using System;

namespace TicketTool.Monitoring.Web.Models.OrderMonitoring
{
    public class DeliveryInfoModel
    {
        public DateTime? ExpectedDeliveryFrom { get; set; }

        public DateTime? ExpectedDeliveryTo { get; set; }

        public DateTime? ProposedDeliveryFrom { get; set; }

        public DateTime? ProposedDeliveryTo { get; set; }

        public DateTime? ActualDeliveryFrom { get; set; }

        public DateTime? ActualDeliveryTo { get; set; }

        //FullfilmentOrder?

        public string Address { get; set; }

        public string City { get; set; }

        public string Street { get; set; }

        public string House { get; set; }

        public string Housing { get; set; }

        public string Building { get; set; }

        public string Flat { get; set; }

        public string Entrance { get; set; }

        public string EntranceCode { get; set; }

        public string Floor { get; set; }

        public decimal FloorTax { get; set; }

        public bool? HasServiceLift { get; set; }

        public string SubwayStationName { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        public string DeliveryType { get; set; }

        public decimal DeliveryPrice { get; set; }

        public string AddressComment { get; set; }

        public string DeliveryCompany { get; set; }

        public string PickupPointId { get; set; }

        //TODO Use correct field
    }
}