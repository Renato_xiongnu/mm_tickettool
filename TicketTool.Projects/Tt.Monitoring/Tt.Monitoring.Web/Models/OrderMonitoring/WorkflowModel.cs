﻿namespace TicketTool.Monitoring.Web.Models.OrderMonitoring
{
    public class WorkflowModel
    {
        public string WorkflowId { get; set; }
        public string Status { get; set; }
        public string IsSuspended { get; set; }
        public string Exception { get; set; }
    }
}