﻿using System;
using TicketTool.Monitoring.Web.Models.Catalogue;
using TicketTool.Monitoring.Web.OrderServiceProxy;

namespace TicketTool.Monitoring.Web.Models.OrderMonitoring
{
    public class CustomerOrderListItemModel
    {
        public string OrderId { get; set; }

        public string CustomerFullName { get; set; }

        public string CustomerEmail { get; set; }

        public string CustomerPhone { get; set; }

        public DateTime OrderInfoCreateData { get; set; }

        public DateTime OrderInfoCreateDay
        {
            get { return OrderInfoCreateData.Date; }
        }

        public string OrderInfoOrderSource { get; set; }

        public string OrderInfoOrderState { get; set; }

        public string DeliveryDeliveryType { get; set; }

        public PaymentTypeModel OrderInfoPaymentType { get; set; }

        public string DeliveryAddress { get; set; }

        public string DiscountCardNo { get; set; }

        public decimal Price { get; set; }

        public string OriginaOrderId { get; set; }

        public bool Prepay { get; set; }
    }
}