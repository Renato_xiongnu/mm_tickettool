﻿using System;
using System.Collections.Generic;
using System.Linq;
using TicketTool.Monitoring.Web.TtService;

namespace TicketTool.Monitoring.Web.Models.OrderMonitoring
{
    public class TicketTaskModel
    {
        private Dictionary<string, object> _filledForm;

        public string TaskId { get; set; }

        public string TicketId { get; set; }

        public string WorkItemId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Type { get; set; }

        public string Outcome { get; set; }

        public bool Escalated { get; set; }

        public TaskState State { get; set; }

        public string Performer { get; set; }

        public string SkillSetName { get; set; }

        public DateTime Created { get; set; }

        public DateTime? Completed { get; set; }

        public Dictionary<string, object> FilledForm
        {
            get { return _filledForm ?? (_filledForm = new Dictionary<string, object>()); }
            set { _filledForm = value; }
        }

        public Dictionary<string, object> FormattedFilledForm
        {
            get
            {
                var result = new Dictionary<string, object>();
                foreach (var field in this.FilledForm)
                {
                    if (field.Value is TimeSpan)
                    {
                        var ts = (TimeSpan) field.Value;
                        var tsSections = new List<string>();
                        if (ts.Days > 0)
                        {
                            tsSections.Add(ts.ToString(@"d\д"));
                        }
                        if (ts.Hours > 0)
                        {
                            tsSections.Add(ts.ToString(@"h\ч"));
                        }
                        if (ts.Minutes > 0)
                        {
                            tsSections.Add(ts.ToString(@"m\м"));
                        }
                        result.Add(field.Key, String.Join(" ", tsSections));
                        continue;
                    }
                    result.Add(field.Key, field.Value);
                }
                return result;
            }
        }
    }
}