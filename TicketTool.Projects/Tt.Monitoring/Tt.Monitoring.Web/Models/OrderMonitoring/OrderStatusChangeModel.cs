﻿using System;

namespace TicketTool.Monitoring.Web.Models.OrderMonitoring
{
    public class OrderStatusChangeModel
    {
        public DateTime UpdateTime { get; set; }

        public string WhoChanged { get; set; }

        public string ReasonText { get; set; }

        public string ChangedType { get; set; }

        public string FromStatus { get; set; }

        public string ToStatus { get; set; }
    }
}