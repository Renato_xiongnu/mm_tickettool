﻿namespace TicketTool.Monitoring.Web.Models.OrderMonitoring
{
    public class SaleLocationInfoModel
    {

        public string SaleLocationId { get; set; }
        
        public string SapCode { get; set; }
    }
}