﻿using System;
using TicketTool.Monitoring.Web.Models.Catalogue;

namespace TicketTool.Monitoring.Web.Models.OrderMonitoring
{
    public class OrderInfoModel
    {
        public string CreatedBy { get; set; }

        public string UpdatedBy { get; set; }

        public bool IsPreorder { get; set; }

        public string WWSOrderId { get; set; }

        public DateTime CreateData { get; set; }

        public DateTime CreateDay
        {
            get { return CreateData.Date; }
        }

        public DateTime UpdateDate { get; set; }

        public string OrderSource { get; set; }

        public PaymentTypeModel PaymentType { get; set; }

        public string FinalPaymentType { get; set; }

        public string OrderState { get; set; }        

        public string CustomerComment { get; set; }

        public string SystemComment { get; set; }

        public string ExternalOrderId { get; set; }

        public DateTime? ExpirationDate { get; set; }        

        /// <summary>
        /// признак - создан ли из онлайна
        /// </summary> 
        public bool IsOnlineOrder { get; set; }

        /// <summary>
        /// Способ получения
        /// </summary>
        public string PickupType { get; set; }

        public bool InTicketTool { get; set; }
    }
}