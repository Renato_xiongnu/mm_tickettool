﻿using TicketTool.Monitoring.Web.OrderServiceProxy;

namespace TicketTool.Monitoring.Web.Models.OrderMonitoring
{
    public class OrderLineModel
    {
        public int ArticleNo { get; set; }

        public int Quantity { get; set; }

        public string ArticleTitle { get; set; }

        public decimal? Price { get; set; }

        public string SerialNumber { get; set; }

        public string ItemState { get; set; }

        public decimal DiscountPercentage { get; set; }

        public bool IsSet { get; set; }

        public bool LongTail { get; set; }

        public ArticleType ArticleType { get; set; }

        public OrderLineType LineType { get; set; }
    }
}