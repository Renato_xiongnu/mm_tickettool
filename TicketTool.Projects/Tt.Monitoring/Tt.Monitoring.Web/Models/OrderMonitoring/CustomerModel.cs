﻿namespace TicketTool.Monitoring.Web.Models.OrderMonitoring
{
    public class CustomerModel
    {
        public string Email { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string LastName { get; set; }

        public string FullName
        {
            get { return string.Format("{0} {1} {2}", FirstName, Surname, LastName); }
        }

        public char? Gender { get; set; }

        public string Phone { get; set; }

        public string Mobile { get; set; }
        
    }
}