﻿using System.ComponentModel.DataAnnotations;

namespace TicketTool.Monitoring.Web.Models.Account
{
    public class LoginModel
    {
        [Required]
        [Display(Name = "Логин пользователя")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Display(Name = "Запомнить меня")]
        public bool? RememberMe { get; set; }
    }
}