﻿using System;
using System.Web;

namespace TicketTool.Monitoring.Web.Infastructure
{
    public static class DateTimeHelper
    {
        public static int GetClientTimeOffset()
        {
            var timeOffset = 0;
            var timeOffsetCookie = HttpContext.Current.Request.Cookies["TimeOffset"];
            if (timeOffsetCookie != null)
            {
                timeOffset += Convert.ToInt32(timeOffsetCookie.Value);
            }

            return timeOffset;
        }
    }
}