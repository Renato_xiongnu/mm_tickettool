﻿using System;
using System.Configuration;

namespace TicketTool.Monitoring.Web.Infastructure
{
    public static class DateTimeExtensions
    {
        public static DateTime ToApplicationDateTime(this DateTime value)
        {
            return ToApplicationDateTime((DateTime?) value) ?? value;
        }

        public static DateTime? ToApplicationDateTime(this DateTime? value)
        {
            if(!value.HasValue)
            {
                return null;
            }

            var timeZoneName = ConfigurationManager.AppSettings["AbsoluteTimeZone"];

            if(string.IsNullOrEmpty(timeZoneName))
            {
                return value;
            }

            var tz = TimeZoneInfo.FindSystemTimeZoneById(timeZoneName);
            var dateTime = value.Value;
            var clientTimeOffset = DateTimeHelper.GetClientTimeOffset();
            dateTime = dateTime.AddMinutes(clientTimeOffset - tz.BaseUtcOffset.TotalMinutes);
            return TimeZoneInfo.ConvertTime(dateTime, tz);
        }        
    }
}