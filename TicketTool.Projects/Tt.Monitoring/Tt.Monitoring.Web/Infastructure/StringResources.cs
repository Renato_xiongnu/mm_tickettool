﻿using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using Newtonsoft.Json;

namespace TicketTool.Monitoring.Web.Infastructure
{
    public static class StringResources
    {
        private static readonly Dictionary<string, string[]> Stringses = new Dictionary<string, string[]>();

        static StringResources()
        {
            Stringses = new Dictionary<string, string[]>();
            LoadStringResources();
        }

        private static void LoadStringResources()
        {
            var stringResourcesDirectoryRelativePath = ConfigurationManager.AppSettings["StringResourcesPath"];
            var stringResourcesDirectoryAbsolutePath =
                HttpContext.Current.Server.MapPath(stringResourcesDirectoryRelativePath);
            var directory = new DirectoryInfo(stringResourcesDirectoryAbsolutePath);
            if(!directory.Exists)
            {
                return;
            }
            var fileNameRegexp = new Regex(@"(?<Name>.+)\.json");
            var files = directory.GetFiles("*.json");
            foreach(var file in files)
            {
                var math = fileNameRegexp.Match(file.Name);
                if(!math.Success)
                {
                    continue;
                }
                var name = math.Groups["Name"].Value;
                string[] dictionary;
                using(var sr = file.OpenText())
                {
                    var json = sr.ReadToEnd();
                    dictionary = string.IsNullOrEmpty(json)
                        ? new string[0]
                        : JsonConvert.DeserializeObject<string[]>(json);
                }
                Stringses[name] = Stringses.ContainsKey(name)
                    ? Stringses[name]
                    : dictionary;
            }
        }

        public static string[] GetStrings(string key)
        {
            return Stringses.ContainsKey(key) ? Stringses[key] : new string[0];
        }
    }
}