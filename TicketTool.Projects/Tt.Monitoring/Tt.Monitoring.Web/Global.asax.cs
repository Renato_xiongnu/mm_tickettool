﻿using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using log4net;
using Newtonsoft.Json;
using TicketTool.Monitoring.Web.App_Start;
using TicketTool.Monitoring.Web.Binders;
using TicketTool.Monitoring.Web.Data;
using TicketTool.Monitoring.Web.Logging;

namespace TicketTool.Monitoring.Web
{
    public class MvcApplication : HttpApplication
    {
        private static Log4NetLogger _logger;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);                        
            MappingExtensions.ConfigureMapping();


            log4net.Config.XmlConfigurator.Configure();
            _logger = new Log4NetLogger(LogManager.GetLogger("Tt.Monitoring.Web"));            
            if (OrdersRepository.Instance==null)
            {
                throw new ApplicationException("OrdersRepository instance is null");
            }

            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                //DateTimeZoneHandling = DateTimeZoneHandling.Utc,
                DateFormatHandling = DateFormatHandling.IsoDateFormat
            };

            RegisterBynders();
            //BundleTable.EnableOptimizations = true;
            _logger.Info("Application started");
        }

        private void RegisterBynders()
        {
            ModelBinders.Binders.DefaultBinder = new DefaultModelBinder();            
            ModelBinders.Binders.Add(typeof(Filter), new FilterBinder());
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var exception = Server.GetLastError();
            _logger.ErrorFormat("Application Error: {1}", exception.Message);
        }
    }
}