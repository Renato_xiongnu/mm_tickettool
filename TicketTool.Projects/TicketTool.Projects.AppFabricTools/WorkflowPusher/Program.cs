﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Threading;
using log4net;
using log4net.Config;
using Microsoft.ApplicationServer.StoreManagement.Control;
using Microsoft.ApplicationServer.StoreManagement.Query;
using Microsoft.ApplicationServer.StoreManagement.Sql.Control;
using Microsoft.ApplicationServer.StoreManagement.Sql.Query;

namespace WorkflowPusher
{
    internal static class Program
    {
        private static readonly ILog Logger = LogManager.GetLogger("WorkflowPusher");

        internal static void Main()
        {
            XmlConfigurator.Configure();
            var connection = new NameValueCollection {{"connectionString", ConfigurationManager.ConnectionStrings["AppFabricPersistenceContext"].ConnectionString}};

            var controlProvider = new SqlInstanceControlProvider();
            controlProvider.Initialize(Settings.Default.Name, connection);
            var control = controlProvider.CreateInstanceControl();
            var commands = GetOverdueInstances(connection).Select(instanceInfo =>
                new InstanceCommand {CommandType = CommandType.Resume, InstanceId = instanceInfo.InstanceId, ServiceIdentifier = instanceInfo.HostInfo.HostMetadata});

            int n = 0, err = 0;
            foreach (var cmd in commands)
            {
                cmd.ServiceIdentifier["VirtualPath"] = Settings.Default.Path;
                try
                {
                    var commandAsyncResult = control.CommandSend.BeginSend(cmd, Settings.Default.Timeout, null, null);
                    control.CommandSend.EndSend(commandAsyncResult);
                    n++;
                }
                catch (Exception ex)
                {
                    err++;
                    Logger.Error(cmd.InstanceId, ex);
                }                
                Console.CursorLeft = 0;
                Console.Write("Completed {0}, error occured {1}", n, err);
                Thread.Sleep(Settings.Default.DelayBetweenKicks * 1000);
            }
        }

        private static IEnumerable<InstanceInfo> GetOverdueInstances(NameValueCollection connection)
        {
            var provider = new SqlInstanceQueryProvider();
            provider.Initialize(Settings.Default.Name, connection);

            var query = provider.CreateInstanceQuery();
            var asyncResult = query.BeginExecuteQuery(new InstanceQueryExecuteArgs {InstanceId = GetOverdueDbInstances(), Count = Settings.Default.Count}, Settings.Default.Timeout, null, null);
            var result = query.EndExecuteQuery(asyncResult).ToList();
            Logger.Info(result.Count);
            Console.WriteLine(result.Count);
            return result;
        }

        private static List<Guid> GetOverdueDbInstances()
        {
            var created = DateTime.Now.AddMinutes(-30);
            var overdure = DateTime.Now.AddMinutes(-20);
            using (var ctx = new AppFabricPersistenceContext())
            {
                return ctx.InstancesTables
                    .Where(i => i.ComplexDataProperties != null)
                    .Where(i => i.CreationTime < created)
                    .Where(i => i.PendingTimer != null && i.PendingTimer < overdure)
                    .Where(i => i.ExecutionStatus == "Idle")
                    .Where(i => i.IsSuspended == null || i.IsSuspended == false)
                    .OrderBy(i => i.PendingTimer)
                    .Select(i => i.Id)
                    .Take(Settings.Default.Count)
                    .ToList();
            }
        }
    }
}
