using System;
using System.ServiceModel;

namespace TicketTool.Projects.VoxImplant.Scheduler
{
    public interface IAuthContext
    {
        bool Login(Func<bool> method, IContextChannel serviceInnerChannel);
        T Execute<T>(Func<T> method, IContextChannel serviceInnerChannel);
    }
}