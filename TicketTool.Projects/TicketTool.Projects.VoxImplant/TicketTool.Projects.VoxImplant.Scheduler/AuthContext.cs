﻿using System;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text.RegularExpressions;

namespace TicketTool.Projects.VoxImplant.Scheduler
{
    internal class AuthContext : IAuthContext
    {
        private string AuthCookie { get; set; }

        public bool Login(Func<bool> method, IContextChannel serviceInnerChannel)
        {
            CheckArgs(method, serviceInnerChannel, false);

            using (new OperationContextScope(serviceInnerChannel))
            {
                if (!method())
                {
                    AuthCookie = null;
                    return false;
                }
                var properties = OperationContext.Current.IncomingMessageProperties;
                var responseProperty = (HttpResponseMessageProperty)properties[HttpResponseMessageProperty.Name];
                var cookiesStr = responseProperty.Headers[HttpResponseHeader.SetCookie];
                AuthCookie = Regex.Match(cookiesStr, @".ASPXAUTH=[A-Z0-9]*").Captures[0].Value;
                return true;
            }
        }

        public T Execute<T>(Func<T> method, IContextChannel serviceInnerChannel)
        {
            CheckArgs(method, serviceInnerChannel);

            using (new OperationContextScope(serviceInnerChannel))
            {
                var requestProperty = new HttpRequestMessageProperty();
                OperationContext.Current.OutgoingMessageProperties.Add(HttpRequestMessageProperty.Name, requestProperty);
                requestProperty.Headers.Add(HttpRequestHeader.Cookie, AuthCookie);
                return method();
            }
        }

        private void CheckArgs(object method, IContextChannel serviceInnerChannel, bool checkTicket = true)
        {
            if (method == null) throw new ArgumentNullException("method");
            if (serviceInnerChannel == null) throw new ArgumentNullException("serviceInnerChannel");
            if (checkTicket && String.IsNullOrWhiteSpace(AuthCookie))
            {
                throw new InvalidOperationException("Currently not logged in. Must Login before calling this method.");
            }
        }
    }
}
