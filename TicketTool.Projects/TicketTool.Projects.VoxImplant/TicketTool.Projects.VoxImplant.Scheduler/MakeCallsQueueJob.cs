﻿using System;
using System.Linq;
using Quartz;
using TicketTool.Projects.VoxImplant.Dal;
using TicketTool.Projects.VoxImplant.Scheduler.ProxyTT;

namespace TicketTool.Projects.VoxImplant.Scheduler
{
    [DisallowConcurrentExecution]
    public class MakeCallsQueueJob : TTJobBase
    {
        public override void Execute(IJobExecutionContext context)
        {
            try
            {
                this.Log.Info("Started!");
                var tt = new TicketToolServiceClient();
                var authCtx = this.Login();
                tt.LogInToAvailableSkillSets();
                if (authCtx == null) return;

                using (var ctx = new TTVoxImplantContext())
                {
                    var task = authCtx.Execute(tt.GetNextTaskSkipAssigned, tt.InnerChannel);
                    while (task != null)
                    {
                        var scenario = ctx.Scenarios.FirstOrDefault(s => s.TaskType == task.Type);
                        if (scenario != null)
                        {
                            var call = new Call();
                            ctx.Calls.Add(call);
                            call.Scenario = scenario;
                            call.TaskId = task.TaskId;
                            call.TextToSpeech = task.Description;
                            call.WorkItemId = task.WorkItemId;
                            call.Parameter = task.Parameter;
                            ctx.SaveChanges();
                        }
                        else
                        {
                            this.Log.ErrorFormat("Cant find scenario for task type {0}. Task Id is {1}", task.Type, task.TaskId);
                        }
                        task = authCtx.Execute(tt.GetNextTaskSkipAssigned, tt.InnerChannel);
                    }
                }
                this.Log.Info("Completed!");
            }
            catch (Exception e)
            {
                this.Log.Error(e);
            }
        }
    }
}
