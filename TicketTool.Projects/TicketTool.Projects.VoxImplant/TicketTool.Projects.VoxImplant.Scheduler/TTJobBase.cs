﻿using TicketTool.Projects.VoxImplant.Scheduler.Properties;
using TicketTool.Projects.VoxImplant.Scheduler.ProxyTTAuth;

namespace TicketTool.Projects.VoxImplant.Scheduler
{
    public abstract class TTJobBase : JobBase
    {
        protected IAuthContext Login()
        {
            var auth = new AuthenticationServiceClient();
            var authCtx = new AuthContext();

            if (authCtx.Login(() => auth.LogIn(Settings.Default.Login, Settings.Default.Password, true), auth.InnerChannel)) 
                return authCtx;
            this.Log.ErrorFormat("Login user fail {0}", Settings.Default.Login);
            return null;
        }
    }
}
