﻿using System;
using System.Linq;
using System.Runtime.Caching;
using System.Text.RegularExpressions;
using TicketTool.Projects.VoxImplant.Scheduler.Properties;

namespace TicketTool.Projects.VoxImplant.Scheduler
{
    public class TtAdminProxy
    {
        private static MemoryCache _memoryCache = new MemoryCache("TicketTool.Projects.VoxImplant");
        private const string ParameterCache = "Parameter";

        public string GetTimeZoneOrDefault(string parameterName)
        {
            ProxyTTAdm.Parameter[] parameters;
            if (_memoryCache[ParameterCache] == null)
            {
                var client = new ProxyTTAdm.TicketToolAdminClient();

                parameters = client.GetAllParameters();
                _memoryCache.Add(ParameterCache, parameters,
                    DateTimeOffset.Now.Add(Settings.Default.ParameterCacheLifiteme));
            }
            else
            {
                parameters = (ProxyTTAdm.Parameter[])_memoryCache[ParameterCache];
            }
            var targetParameter = parameters.FirstOrDefault(el => el.ParameterName == parameterName);
            if (targetParameter == null || string.IsNullOrEmpty(targetParameter.TimeZoneOffset))
            {
                var timeZone = TimeZoneInfo.FindSystemTimeZoneById(Settings.Default.CallCenterTimeZone);
                return TimeSpanAsOffset(timeZone.BaseUtcOffset.ToString());
            }

            return TimeSpanAsOffset(targetParameter.TimeZoneOffset);
        }

        private static string TimeSpanAsOffset(string ts)
        {
            ts = ts.Substring(0, 2); //Take only hours 
            if (ts.StartsWith("0")) ts = ts.Remove(0, 1); //It's time to learn regexp
            return string.Format("UTC+{0}", ts);
        }
    }
}
