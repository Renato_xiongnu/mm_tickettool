﻿using log4net;
using Quartz;

namespace TicketTool.Projects.VoxImplant.Scheduler
{
    public abstract class JobBase : IJob
    {
        private ILog _log;

        protected ILog Log
        {
            get { return this._log ?? (this._log = LogManager.GetLogger(this.GetType().Name)); }
        }

        public abstract void Execute(IJobExecutionContext context);
    }
}
