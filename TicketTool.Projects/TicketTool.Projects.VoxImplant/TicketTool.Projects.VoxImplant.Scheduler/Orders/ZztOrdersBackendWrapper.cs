﻿using System;
using TicketTool.Projects.VoxImplant.Scheduler.ProxyOB;

namespace TicketTool.Projects.VoxImplant.Scheduler.Orders
{
    class ZztOrdersBackendWrapper:OrdersBackendWrapperBase
    {
        protected override string GetPhone(string orderId)
        {
            using (var ob = new OrderServiceClient())
            {
                var result = ob.GetOrder(orderId);
                if (result.ReturnCode == ReturnCode.Error) throw new Exception(result.Message);

                return result.CustomerOrder.Customer.Phone;
            }
        }
    }
}
