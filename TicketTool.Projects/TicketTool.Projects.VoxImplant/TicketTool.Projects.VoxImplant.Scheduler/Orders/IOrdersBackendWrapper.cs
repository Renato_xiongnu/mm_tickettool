﻿namespace TicketTool.Projects.VoxImplant.Scheduler.Orders
{
    interface IOrdersBackendWrapper
    {
        OrderInfo GetOrderInfo(string orderId);
    }
}
