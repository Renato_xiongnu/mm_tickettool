﻿using System;
using TicketTool.Projects.VoxImplant.Scheduler.ProxyOBMM;

namespace TicketTool.Projects.VoxImplant.Scheduler.Orders
{
    class MMOrdersBackendWrapper:OrdersBackendWrapperBase
    {
        protected override string GetPhone(string orderId)
        {
            using (var client = new OrderServiceClient())
            {
                var result = client.GerOrderDataByOrderId(orderId);
                if (result.ReturnCode == ReturnCode.Error) throw new Exception(result.ErrorMessage);

                return result.ClientData.Phone;
            }
        }
    }
}
