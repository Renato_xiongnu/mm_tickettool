﻿using System;
using System.Text.RegularExpressions;
using log4net;
using TicketTool.Projects.VoxImplant.Scheduler.Properties;

namespace TicketTool.Projects.VoxImplant.Scheduler.Orders
{
    abstract class OrdersBackendWrapperBase:IOrdersBackendWrapper
    {
        private ILog _log;

        protected ILog Log
        {
            get { return _log ?? (_log = LogManager.GetLogger(GetType().Name)); }
        }

        public static IOrdersBackendWrapper Create()
        {
            switch (Settings.Default.OrdersSource)
            {
                case "ZZT":
                {
                    return new ZztOrdersBackendWrapper();
                }
                case "MM":
                {
                    return new MMOrdersBackendWrapper();
                }
                default:
                {
                    throw new ArgumentOutOfRangeException("", "Only MM and ZZT sources supported");
                }
            }
        }

        public OrderInfo GetOrderInfo(string orderId)
        {
            return new OrderInfo
            {
                OrderId = orderId,
                CustomerPhone = FormatPhone(orderId,GetPhone(orderId))
            };
        }

        private string FormatPhone(string orderId,string phoneRaw)
        {
            var phone = Regex.Replace(phoneRaw, @"\D+", String.Empty);
            phone = Regex.Replace(phone, @"^8", "7");
            Log.Info(String.Format("The phone number was formatted to {0}. Order id is {1}", phone, orderId));
            return phone;
        }

        protected abstract string GetPhone(string orderId);
    }
}
