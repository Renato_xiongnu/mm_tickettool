﻿namespace TicketTool.Projects.VoxImplant.Scheduler.Orders
{
    class OrderInfo
    {
        internal string OrderId { get; set; }

        internal string CustomerPhone { get; set; }
    }
}