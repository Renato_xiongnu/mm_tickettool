﻿using System;
using System.Linq;
using System.Net;
using System.Web;
using Newtonsoft.Json;
using Quartz;
using TicketTool.Projects.VoxImplant.Dal;
using TicketTool.Projects.VoxImplant.Scheduler.Orders;
using TicketTool.Projects.VoxImplant.Scheduler.Properties;

namespace TicketTool.Projects.VoxImplant.Scheduler
{
    [DisallowConcurrentExecution]
    public class InvokeVoxImplantJob : TTJobBase
    {
        public override void Execute(IJobExecutionContext context)
        {
            this.Log.Info("Started!");
            using (var ctx = new TTVoxImplantContext())
            {
                var calls = ctx.Calls.Include("Scenario").Where(c => c.Invoked == null).ToList();
                foreach (var call in calls)
                {
                    try
                    {
                        using (var client = new WebClient())
                        {
                            call.VoxImplantResponse = client.DownloadString(this.GetVoxImplantUri(call));
                            call.Invoked = DateTime.UtcNow;
                        }
                    }
                    catch (Exception e)
                    {
                        this.Log.ErrorFormat("An error occured while VoxImpland was invoked with call Id {0}. Details: {1}", call.Id, e.Message);
                    }
                    ctx.SaveChanges();
                }
            }
            this.Log.Info("Completed!");
        }

        private string GetVoxImplantUri(Call call)
        {
            var ob = OrdersBackendWrapperBase.Create();
            var orderInfo = ob.GetOrderInfo(call.WorkItemId);
            var ttAdminProxy = new TtAdminProxy();

            var voxImplantRequest = new
            {
                number = orderInfo.CustomerPhone,
                order_id = call.WorkItemId,
                tts_text = call.TextToSpeech,
                call_id = call.Id,
                offset = ttAdminProxy.GetTimeZoneOrDefault(call.Parameter)
            };
            string json =
                HttpUtility.UrlEncode(JsonConvert.SerializeObject(voxImplantRequest));
            return String.Format(Settings.Default.ExternalCallingApiUrl, Settings.Default.VoxImplantAccountId,
                Settings.Default.VoxImplantApiKey, call.Scenario.Id, json);
        }
    }
}
