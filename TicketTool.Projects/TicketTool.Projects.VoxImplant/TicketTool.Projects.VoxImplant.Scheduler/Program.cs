﻿using System;
using System.ServiceModel;
using log4net;
using log4net.Config;
using Quartz;
using Quartz.Impl;
using TicketTool.Projects.VoxImplant.Scheduler.Properties;

namespace TicketTool.Projects.VoxImplant.Scheduler
{
    static class Program
    {
        private static void Main()
        {
            var cron = String.Format("0/30 * {0}-{1} * * ?", ConvertFromCCHour(Settings.Default.CCStart),
                ConvertFromCCHour(Settings.Default.CCEnd));
            XmlConfigurator.Configure();
            var log = LogManager.GetLogger("QuartzScheduler");
            try
            {
                var scheduler = StdSchedulerFactory.GetDefaultScheduler();
                scheduler.Start();

                AddJob<MakeCallsQueueJob>(scheduler, cron);
                AddJob<InvokeVoxImplantJob>(scheduler, cron);
                AddJob<CompleteTasksJob>(scheduler, cron);

                log.Info("Scheduler has been started");
                while (true) System.Threading.Thread.Sleep(1000);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message, ex);
            }
        }

        private static void AddJob<T>(IScheduler scheduler, string cron) where T : IJob
        {
            var name = typeof (T).Name;
            var job = JobBuilder.Create<T>().WithIdentity(name).Build();                        
            var trigger = TriggerBuilder.Create().WithIdentity(String.Concat(name, "Trigger")).WithCronSchedule(cron).Build();
            scheduler.ScheduleJob(job, trigger);
        }

        private static int ConvertFromCCHour(int hours)
        {
            var ccStart = TimeZoneInfo.ConvertTimeToUtc(new DateTime(2014, 1, 1, hours, 0, 0), TimeZoneInfo.FindSystemTimeZoneById(Settings.Default.CallCenterTimeZone));
            return TimeZoneInfo.ConvertTime(ccStart, TimeZoneInfo.Local).Hour;
        }
    }
}
