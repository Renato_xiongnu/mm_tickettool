﻿using System;
using System.Linq;
using Quartz;
using TicketTool.Projects.VoxImplant.Dal;
using TicketTool.Projects.VoxImplant.Scheduler.ProxyTT;

namespace TicketTool.Projects.VoxImplant.Scheduler
{
    [DisallowConcurrentExecution]
    public class CompleteTasksJob : TTJobBase
    {
        public override void Execute(IJobExecutionContext context)
        {
            this.Log.Info("Started!");

            var tt = new TicketToolServiceClient();
            var authCtx = this.Login();
            if (authCtx == null) return;

            try
            {
                using (var ctx = new TTVoxImplantContext())
                {
                    var completedCalls = ctx.Calls.Include("Scenario").Where(c => c.ResultReceived != null && c.Completed == null).ToList();
                    foreach (var call in completedCalls)
                    {
                        var innerCall = call;
                        var completionResult = authCtx.Execute(() =>
                        {
                            var task = tt.GetTask(innerCall.TaskId);
                            return task == null || task.State >= TaskState.Completed
                                ? new OperationResult {HasError = true, MessageText = String.Format("Cant complete task with id {0}", innerCall.TaskId)}
                                : tt.CompleteTask(innerCall.TaskId, innerCall.Result, new RequiredField[] {});
                        }, tt.InnerChannel);

                        if (completionResult.HasError)
                        {
                            var queue = new CallCompletionQueue {Call = call, HasError = true, Message = completionResult.MessageText, TicketToolInvoked = DateTime.UtcNow};
                            ctx.CallCompletionQueues.Add(queue);
                            this.Log.ErrorFormat("An error occured while invoking TicketTool.CompleteTask with taskId {0}. Details: {1}", call.TaskId, completionResult.MessageText);
                        }
                        else
                        {
                            call.Completed = DateTime.UtcNow;
                            this.Log.InfoFormat("Call was completed with taskId {0} successfully", call.TaskId);
                        }
                        ctx.SaveChanges();
                    }
                }
                this.Log.Info("Completed!");
            }
            catch (Exception e)
            {
                this.Log.Error(e);
            }
        }
    }
}


                    