﻿using System.Data.Entity;

namespace TicketTool.Projects.VoxImplant.Dal
{
    public class TTVoxImplantContext : DbContext
    {
        public TTVoxImplantContext() : base("TicketTool_VoxImplant")
        { }

        public DbSet<Scenario> Scenarios { get; set; }
        public DbSet<Call> Calls { get; set; }
        public DbSet<CallCompletionQueue> CallCompletionQueues { get; set; }
    }
}
