namespace TicketTool.Projects.VoxImplant.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CallCompletionQueues",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TicketToolInvoked = c.DateTime(nullable: false),
                        HasError = c.Boolean(nullable: false),
                        Message = c.String(),
                        Call_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Calls", t => t.Call_Id)
                .Index(t => t.Call_Id);
            
            CreateTable(
                "dbo.Calls",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TaskId = c.String(nullable: false),
                        TextToSpeech = c.String(nullable: false),
                        WorkItemId = c.String(nullable: false),
                        Invoked = c.DateTime(),
                        ResultReceived = c.DateTime(),
                        Completed = c.DateTime(),
                        Result = c.String(),
                        VoxImplantResponse = c.String(),
                        Scenario_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Scenarios", t => t.Scenario_Id, cascadeDelete: true)
                .Index(t => t.Scenario_Id);
            
            CreateTable(
                "dbo.Scenarios",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        TaskType = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CallCompletionQueues", "Call_Id", "dbo.Calls");
            DropForeignKey("dbo.Calls", "Scenario_Id", "dbo.Scenarios");
            DropIndex("dbo.Calls", new[] { "Scenario_Id" });
            DropIndex("dbo.CallCompletionQueues", new[] { "Call_Id" });
            DropTable("dbo.Scenarios");
            DropTable("dbo.Calls");
            DropTable("dbo.CallCompletionQueues");
        }
    }
}
