namespace TicketTool.Projects.VoxImplant.Dal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _12 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Calls", "Parameter", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Calls", "Parameter");
        }
    }
}
