﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TicketTool.Projects.VoxImplant.Dal
{
    public class CallCompletionQueue
    {
        [Key]
        public int Id { get; set; }
        public Call Call { get; set; }
        public DateTime TicketToolInvoked { get; set; }
        public bool HasError { get; set; }
        [MaxLength]
        public string Message { get; set; }
    }
}
