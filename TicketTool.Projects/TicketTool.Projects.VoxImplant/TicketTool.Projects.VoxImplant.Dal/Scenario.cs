﻿using System.ComponentModel.DataAnnotations;

namespace TicketTool.Projects.VoxImplant.Dal
{
    public class Scenario
    {
        [Key]
        public string Id { get; set; }
        [Required]
        public string TaskType { get; set; }
    }
}
