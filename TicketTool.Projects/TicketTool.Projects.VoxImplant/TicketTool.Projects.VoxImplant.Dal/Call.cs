﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TicketTool.Projects.VoxImplant.Dal
{
    public class Call
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public Scenario Scenario { get; set; }
        [Required]
        public string TaskId { get; set; }
        [Required]
        public string TextToSpeech { get; set; }
        [Required]
        public string WorkItemId { get; set; }
        public DateTime? Invoked { get; set; }
        public DateTime? ResultReceived { get; set; }        
        public DateTime? Completed { get; set; }
        public string Result { get; set; }
        [MaxLength]
        public string VoxImplantResponse { get; set; }
        public string Parameter { get; set; }
    }
}
