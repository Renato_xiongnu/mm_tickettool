﻿using System;
using System.Configuration;
using System.Web;

namespace TicketTool.Projects.VoxImplant.LoadBalancing
{
    public class Balancer : IBalancer
    {
        private const string VoxImplantStatKey = "VoxImplantStat";

        public bool ShouldProcessByRobot()
        {
            int trottle;
            if (!Int32.TryParse(ConfigurationManager.AppSettings["Trottle"], out trottle))
            {
                trottle = 100;
            }
            var stat = HttpContext.Current.Application[VoxImplantStatKey] as Stat ?? new Stat();
            bool result = (double)stat.ProcessedByRobot/(double)stat.Total*100 < trottle;
            if (result) stat.ProcessedByRobot++;
            stat.Total++;
            HttpContext.Current.Application[VoxImplantStatKey] = stat;
            return result;
        }
    }
}
