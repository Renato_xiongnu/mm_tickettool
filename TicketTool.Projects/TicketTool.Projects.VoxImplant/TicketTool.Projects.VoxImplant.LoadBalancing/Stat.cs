﻿namespace TicketTool.Projects.VoxImplant.LoadBalancing
{
    public class Stat
    {
        public int ProcessedByRobot { get; set; }
        public int Total { get; set; }        
    }
}
