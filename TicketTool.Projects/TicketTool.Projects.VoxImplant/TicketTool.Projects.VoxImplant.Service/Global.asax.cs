﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using log4net;

namespace TicketTool.Projects.VoxImplant.Service
{
    public class Global : System.Web.HttpApplication
    {
        private readonly ILog Log = LogManager.GetLogger("Service");

        protected void Application_Start(object sender, EventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();
            Log.InfoFormat("Application started");
        }
    }
}