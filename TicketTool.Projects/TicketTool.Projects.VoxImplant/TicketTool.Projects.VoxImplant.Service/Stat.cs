﻿namespace TicketTool.Projects.VoxImplant.Service
{
    public class Stat
    {
        public int ProcessedByRobot { get; set; }
        public int Total { get; set; }        
    }
}
