﻿using System.Runtime.Serialization;

namespace TicketTool.Projects.VoxImplant.Service
{
    [DataContract]
    public class CallCompletedResponse
    {
        [DataMember(Name = "result")]
        public string Result { get; set; }
    }
}