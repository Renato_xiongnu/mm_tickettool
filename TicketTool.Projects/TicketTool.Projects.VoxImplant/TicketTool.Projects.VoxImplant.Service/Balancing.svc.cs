﻿using System;
using System.Configuration;
using System.Web;

namespace TicketTool.Projects.VoxImplant.Service
{
    public class Balancing : IBalancing
    {
        private const string VoxImplantStatKey = "VoxImplantStat";

        public bool ShouldProcessByRobot()
        {
            int trottle;
            if (!Int32.TryParse(ConfigurationManager.AppSettings["Trottle"], out trottle))
            {
                trottle = 100;
            }
            var stat = HttpContext.Current.Application[VoxImplantStatKey] as Stat ?? new Stat();
            stat.Total++;
            bool result = (double)stat.ProcessedByRobot / (double)stat.Total * 100 < trottle;
            if (result) stat.ProcessedByRobot++;            
            HttpContext.Current.Application[VoxImplantStatKey] = stat;
            return result;
        }
    }
}
