﻿using System.ServiceModel;
using System.ServiceModel.Web;

namespace TicketTool.Projects.VoxImplant.Service
{
    [ServiceContract]
    public interface ICallResultReceiver
    {
        [OperationContract]
        [WebInvoke(UriTemplate = "/mark_call_completed",
           Method = "POST",
           ResponseFormat = WebMessageFormat.Json,
           RequestFormat = WebMessageFormat.Json,
           BodyStyle = WebMessageBodyStyle.Bare)]
        CallCompletedResponse MarkCallCompleted(CallCompletedRequest call);
    }
}
