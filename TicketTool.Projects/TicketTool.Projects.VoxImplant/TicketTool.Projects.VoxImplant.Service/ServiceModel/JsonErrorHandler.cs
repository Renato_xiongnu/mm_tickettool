﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.Serialization.Json;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;

namespace TicketTool.Projects.VoxImplant.Service.ServiceModel
{
    public class JsonErrorHandler : IErrorHandler
    {
        public bool HandleError(Exception error)
        {
            return true;
        }

        public void ProvideFault(Exception error, MessageVersion version, ref Message fault)
        {
            fault = this.GetJsonFaultMessage(version, error);

            this.ApplyJsonSettings(ref fault);
            this.ApplyHttpResponseSettings(ref fault, HttpStatusCode.BadRequest, "BadRequest");
        }

        protected virtual void ApplyJsonSettings(ref Message fault)
        {
            var jsonFormatting = new WebBodyFormatMessageProperty(WebContentFormat.Json);
            fault.Properties.Add(WebBodyFormatMessageProperty.Name, jsonFormatting);
        }

        protected virtual void ApplyHttpResponseSettings(
            ref Message fault, HttpStatusCode statusCode,
            string statusDescription)
        {
            var httpResponse = new HttpResponseMessageProperty
            {
                StatusCode = statusCode,
                StatusDescription = statusDescription
            };
            httpResponse.Headers[HttpResponseHeader.ContentType] = "application/json";
            fault.Properties.Add(HttpResponseMessageProperty.Name, httpResponse);
        }

        protected virtual Message GetJsonFaultMessage(MessageVersion version, Exception error)
        {
            var code = error.GetType().Name;
            var message = error.Message;

            var jsonFault = new Dictionary<string, string> { { code, message } };

            var ser = new DataContractJsonSerializer(jsonFault.GetType(), new DataContractJsonSerializerSettings
            {
                UseSimpleDictionaryFormat = true
            });
            var faultMessage = Message.CreateMessage(version, "", jsonFault, ser);
            return faultMessage;
        }
    }
}