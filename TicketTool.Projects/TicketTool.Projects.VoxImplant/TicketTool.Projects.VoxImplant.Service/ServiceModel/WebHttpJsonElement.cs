﻿using System;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Web;

namespace TicketTool.Projects.VoxImplant.Service.ServiceModel
{
    public class WebHttpJsonElement : BehaviorExtensionElement
    {
        public class WebHttpJsonBehavior : WebHttpBehavior
        {
            protected override IDispatchMessageFormatter GetReplyDispatchFormatter(OperationDescription operationDescription, ServiceEndpoint endpoint)
            {
                return new JsonMessageFormatter();
            }

            protected override void AddServerErrorHandlers(
                                        ServiceEndpoint endpoint,
                                        EndpointDispatcher endpointDispatcher)
            {
                endpointDispatcher.ChannelDispatcher.ErrorHandlers.Clear();
                endpointDispatcher.ChannelDispatcher.ErrorHandlers.Add(new JsonErrorHandler());
            }
        }

        public WebHttpJsonElement() { }

        public override Type BehaviorType
        {
            get
            {
                return typeof(WebHttpJsonBehavior);
            }
        }

        protected override object CreateBehavior()
        {
            var behavior = new WebHttpJsonBehavior();
            behavior.DefaultBodyStyle = WebMessageBodyStyle.Bare;
            behavior.DefaultOutgoingResponseFormat = WebMessageFormat.Json;
            behavior.AutomaticFormatSelectionEnabled = false;
            return behavior;
        }
    }
}