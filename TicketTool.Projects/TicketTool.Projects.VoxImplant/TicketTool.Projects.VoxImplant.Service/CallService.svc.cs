﻿using System;
using System.Linq;
using log4net;
using TicketTool.Projects.VoxImplant.Dal;

namespace TicketTool.Projects.VoxImplant.Service
{
    public class CallResultReceiver : ICallResultReceiver
    {
        private ILog _log;

        protected ILog Log
        {
            get { return this._log ?? (this._log = LogManager.GetLogger(this.GetType().Name)); }
        }

        public CallCompletedResponse MarkCallCompleted(CallCompletedRequest call)
        {
            if (call == null)
            {

                Log.Error("Call is null!");
                return new CallCompletedResponse
                {
                    Result = "ArgumentNullException: Value cannot be null. Parameter name: call"
                };
            }

            Log.InfoFormat("Start MarkCallCompleted for call {0}, result: {1}", call.CallId, call.Result);

            int id;
            Int32.TryParse(call.CallId, out id);
            using (var ctx = new TTVoxImplantContext())
            {
                var storedCall = ctx.Calls.Include("Scenario").FirstOrDefault(c => c.Id == id);
                if (storedCall != null)
                {
                    storedCall.ResultReceived = DateTime.UtcNow;
                    storedCall.Result = call.Result;
                    ctx.SaveChanges();

                    Log.InfoFormat("Finish MarkCallCompleted for call {0}, result: {1}", call.CallId, call.Result);
                }
                else
                {

                    Log.InfoFormat(
                        "Error MarkCallCompleted for call {0}, result: {1}. Error: Value doesn't fall within the expected range.",
                        call.CallId, call.Result);
                    return new CallCompletedResponse {Result = "Value doesn't fall within the expected range."};
                }
            }
            return new CallCompletedResponse {Result = "ok"};
        }
    }
}
