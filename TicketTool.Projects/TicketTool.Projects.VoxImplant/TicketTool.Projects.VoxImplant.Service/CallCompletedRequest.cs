﻿using System.Runtime.Serialization;

namespace TicketTool.Projects.VoxImplant.Service
{
    [DataContract]
    public class CallCompletedRequest
    {
        [DataMember(Name = "call_id")]
        public string CallId { get; set; }

        [DataMember(Name = "result")]
        public string Result { get; set; }
    }
}