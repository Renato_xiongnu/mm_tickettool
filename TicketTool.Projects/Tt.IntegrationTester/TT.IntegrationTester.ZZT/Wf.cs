﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TT.IntegrationTester.ZZT.Proxy.OrderService;
using TT.IntegrationTester.ZZT.Proxy.WF;
using CustomerOrder = TT.IntegrationTester.ZZT.Proxy.OrderService.CustomerOrder;

namespace TT.IntegrationTester.ZZT
{
    public class Wf : WfBase, IZztWf
    {
        private OrderServiceClient _client;

        private TicketToolServiceClient _ttClient;

        private ZztOrderProcessingServiceClient _wfClient;

        public CustomerOrder Order { get; private set; }
        
        protected override object GetWfService()
        {
            return new ZztOrderProcessingServiceClient();
        }

        public Wf()
        {
            Order = new CustomerOrder
                {
                    OrderId = Guid.NewGuid().ToString(),
                    OrderInfo = new OrderInfo
                        {
                            OrderState = "новый заказ",
                            PaymentType = new PaymentType
                                {
                                    NameRu = "Наличными при получении заказа"
                                },
                            ExpirationDate = DateTime.Now.AddDays(1),
                            OrderStatusHistory = new List<OrderStatusChange>()
                        },
                    OrderLines = new List<OrderLine>(),
                    Customer =
                        new Customer
                            {
                                Surname = "test",
                                FirstName = "test",
                                Phone = "+79111111111",
                                Email = "test@test.ru"
                            },
                    Delivery = new DeliveryInfo
                        {
                            //ExpectedDeliveryTo = DateTime.Now.AddMinutes(-10),
                            //ProposedDeliveryTo = DateTime.Now.AddMinutes(-10)
                        },
                    SaleLocation = new SaleLocationInfo {SaleLocationId = "shop_br100", ChannelId = "ZZT"}
                };
            OrderId = Order.OrderId;

            Caller = new WfCaller(new ZztOrderProcessingServiceClient(), Order.OrderId);
            _client = new OrderServiceClient();
            _ttClient=new TicketToolServiceClient();
            _wfClient = new ZztOrderProcessingServiceClient();
        }
        
        public void CallChangeOrderStatus(string toStatus)
        {
            if (!CallChangeOrderStatusInternal(toStatus))
            {
                throw new ApplicationException(string.Format("Cant change order status to {0}", toStatus));
            }
            Thread.Sleep(Configuration.DelayAfterChangeStatus);
        }

        public override void CallCancel()
        {
            Order.OrderInfo.OrderState = "отмена";
            var req = new UpdateOrderRequest(Order);
            var updateResult = _client.UpdateOrder(req);
            if (updateResult.UpdateOrderResult.ReturnCode != ReturnCode.Ok)
            {
                throw new ApplicationException(string.Format("Error during canceling order: {0}", updateResult.UpdateOrderResult.Message));
            }
            if (!UpdateOrder())
            {
                throw new ApplicationException("Error duting notifing workflow");
            }
        }

        public void CheckOrderState(string state)
        {
            RefreshOrder();
            if (String.CompareOrdinal(Order.OrderInfo.OrderState, state) != 0)
            {
                Assert.Fail("Expected state: {0}. Actual state: {1}", state, Order.OrderInfo.OrderState);
            }
        }

        public void CheckOrderState(string state, TimeSpan delay)
        {
            var taskCompare = Task.Factory.StartNew(() =>
                {
                    while (!string.Equals(Order.OrderInfo.OrderState, state,StringComparison.InvariantCultureIgnoreCase))
                    {
                        RefreshOrder();
                        Thread.Sleep(Configuration.DelayBetweenCheckStatusRepeats);
                    }
                });
            var taskWait = Task.Factory.StartNew(() => Thread.Sleep(delay));

            Task.WaitAny(taskCompare, taskWait);
            if(taskWait.IsCompleted)
            {
                throw new ApplicationException(string.Format("Expected state: {0}. Actual state: {1}", state,
                                                             Order.OrderInfo.OrderState));
            }
        }
        
        public void AddArticle(int article, string umaxState)
        {
            Order.OrderLines.Add(new OrderLine
                {
                    ArticleNo = article,
                    ArticleTitle = "Test",
                    LongTail = true,
                    Price = 15000,
                    UmaxStatus = umaxState
                });

            UpdateOrder();
        }

        public void ChangeUmaxStatus(int articleNum, string newState)
        {
            if (Order.OrderLines.Count() < articleNum)
            {
                throw new ApplicationException(string.Format("Ошибка в тесте - позиции заказа с номером {0} нет",
                                                             articleNum));
            }

            var article = Order.OrderLines.ElementAt(articleNum - 1); //Для Саши
            //if (!article.LongTail)
            //{
            //    throw new ApplicationException(string.Format("Ошибка в тесте - позиция с номером {0} не longtail",
            //                                                 articleNum));
            //}

            article.UmaxStatus = newState;

            UpdateOrder();
            Thread.Sleep(Configuration.DelayAfterChangeStatus);
        }

        public void CheckNewOrderCreation(bool shouldBeCreated = true)
        {
            var req=new GetOrdersRequest(0,0);
            if (shouldBeCreated)
                if (!
                    _client.GetOrders(req).GetOrdersResult
                           .CustomerOrders.Any(
                               el =>
                               el.OrderInfo.SystemComment != null && el.OrderInfo.SystemComment.Contains(Order.OrderId)))
                    //TODO
                {
                    throw new ApplicationException("Заказ не был пересоздан");
                }
            if (!shouldBeCreated)
                if (_client.GetOrders(req).GetOrdersResult
                           .CustomerOrders.Any(
                               el =>
                               el.OrderInfo.SystemComment != null &&
                               el.OrderInfo.SystemComment.Contains(Order.OrderId)))
                {
                    throw new ApplicationException("Заказ был пересоздан");
                }
        }

        public override void AddArticle(int article)
        {
            Order.OrderLines.Add(new OrderLine
                {
                    ArticleNo = article,
                    ArticleTitle = "Test",
                    LongTail = false,
                    Price = 15000
                });

            UpdateOrder();
        }

        private void RefreshOrder()
        {
            var req = new GetOrderRequest(Order.OrderId);
            Order = _client.GetOrder(req).GetOrderResult.CustomerOrder;
        }


        public void SetOrderSource(string orderSource)
        {
            Order.OrderInfo.OrderSourceSystem = orderSource;
            var req = new UpdateOrderRequest(Order);
            _client.UpdateOrder(req);
        }

        public void SetOrderState(string orderState)
        {
            Order.OrderInfo.OrderState = orderState;
            var req = new UpdateOrderRequest(Order);
            _client.UpdateOrder(req); 
        }
        
        public void SetOrderState(string newState, string changedBy)
        {
            if (Order.OrderInfo.OrderStatusHistory == null)
                Order.OrderInfo.OrderStatusHistory = new List<OrderStatusChange>();
            Order.OrderInfo.OrderStatusHistory.Add(
                new OrderStatusChange
                {
                    FromStatus = Order.OrderInfo.OrderState,
                    ToStatus = newState,
                    WhoChanged = changedBy
                });

            Order.OrderInfo.OrderState = newState;

            var req = new UpdateOrderRequest(Order);
            _client.UpdateOrder(req);
        }


        public void SetPaymentType(string paymentType)
        {
            Order.OrderInfo.PaymentType.NameRu = paymentType;
            var req = new UpdateOrderRequest(Order);
            _client.UpdateOrder(req);
        }

        public void CheckTransition(string fromState, string toState, TimeSpan delay)
        {
            var taskCompare = Task.Factory.StartNew(() =>
            {
                while (
                    string.Equals(fromState, Order.OrderInfo.OrderState,
                        StringComparison.InvariantCultureIgnoreCase) ||
                    string.Equals(toState, Order.OrderInfo.OrderState, StringComparison.InvariantCultureIgnoreCase))
                {
                    RefreshOrder();
                    Thread.Sleep(Configuration.DelayBetweenCheckStatusRepeats);
                }
            });

            taskCompare.Wait(delay);
            if (taskCompare.IsCompleted)
            {
                throw new ApplicationException(string.Format("Expected state: {0}. Actual state: {1}", toState,
                                                             Order.OrderInfo.OrderState));
            }
        }

        public void AreEqual<T>(Func<T> expected, T actual)
        {
            RefreshOrder();
            Assert.AreEqual(expected.Invoke(), actual);
        }

        public void CallChangeOrderStatus(string toStatus, string whoChanged)
        {
            if (!CallChangeOrderStatusInternal(toStatus))
            {
                throw new ApplicationException(string.Format("Cant change order status to {0}", toStatus));
            }
            Thread.Sleep(Configuration.DelayAfterChangeStatus);
        }

        private bool CallChangeOrderStatusInternal(string toStatus)
        {
            if (string.IsNullOrEmpty(toStatus)) { throw new ArgumentOutOfRangeException("toStatus"); }

            return _wfClient.NotifyOrderChange(Caller.TicketId, Caller.OrderId, new CustomerOrderChangeDto
            {
                NewStatus = toStatus,
                OrderId = Caller.OrderId
            });
        }

        public bool UpdateOrder()
        {
            var req = new UpdateOrderRequest(Order);
            if (_client.UpdateOrder(req).UpdateOrderResult.ReturnCode != ReturnCode.Ok)
            {
                throw new ApplicationException("Error during updating order");
            }
            if (Caller.TicketId != null) //Ticket not created
                return _wfClient.NotifyOrderUpdate(Caller.TicketId, Caller.OrderId);

            return true;
        }
    }
}
