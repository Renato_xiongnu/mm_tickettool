﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

//wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "");
//wf.CallCompleteTask("Check_Online_Payment", "Подтвердить заказ и уточнить статус оплаты","");
//wf.CallCompleteTask("Return_Payment", "Вернуть оплату","");
//wf.CallCompleteTask("Approve_Umax", "Позвонить клиенту и уточнить заказ","");
//wf.CallCompleteTask("Cancel_Umax", "Позвонить клиенту и уточнить заказ","");
//wf.CallCompleteTask("Approve_Problem_Umax", "Узнать статус Umax в ЦО","");
//wf.CallCompleteTask("DispatchOrder", "Маршрутизировать заказ","");
//wf.CallCompleteTask("Send_Order", "Отправить заказ","");
//wf.CallCompleteTask("Fix_delivery", "Уточнить данные заказа","");
//wf.CallCompleteTask("Fix_Broken_Trans", "Уточнить данные заказа","");
//wf.CallCompleteTask("Recreate_Order_Failed", "Пересоздать заказ","");
//wf.CallCompleteTask("Fix_Broken_Payment", "Проверить оплату","");
//wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества","");
using Orders.Backend.Model;
using TT.IntegrationTester;
using TT.IntegrationTester.ZZT;


namespace Orders.Backend.IntegrationTester
{
    [TestClass]
    public class ZztTest
    {
        [TestMethod]
        public void NewMethod2()
        {
            var wf = new Wf { Configuration = { DelayBetweenCheckStatusRepeats = TimeSpan.FromMilliseconds(100) } };
            GetAction(wf, 3, 1).Invoke();
        }


        [TestMethod]
        private void NewTest()
        {
            //bool isFail = false;
            //for (var i = 10; i < 12; i++)
            //    for (var j = 0; j < 1; j++)
            //    {
            //        var wf = new Wf { Configuration = { DelayBetweenCheckStatusRepeats = TimeSpan.FromMilliseconds(100) } };
            //        try
            //        {
            //            switch (i)
            //            {
            //                case 1: Before_1_Happy(wf); break;
            //                case 2: Before_2_HP_CGT(wf); break;
            //                case 3: Before_3_CGT(wf); break;
            //                case 4: Before_4_CustomerRecall(wf); break;
            //                case 5: Before_5_FromCallCenter(wf); break;
            //                case 6: Before_6_UmaxHappy(wf); break;
            //                case 7: Before_7_Umax_NA(wf); break;
            //                case 8: Before_8_Umax_Done(wf); break;
            //                case 9: Before_9_NA_Done(wf); break;
            //                case 10: Before_10_WM_Paid(wf); break;
            //                case 11: Before_11_WM_Paid_CGT(wf); break;
            //                case 12: Before_12_WM_Paid_CGT(wf); break;
            //                case 13: Before_13_WM_ToPay(wf); break;
            //                case 14: Before_14_WM_ToPay_CGT(wf); break;
            //                case 15: Before_15_WM_PaidLater(wf); break;
            //                case 16: Before_16_WM_FixPayment(wf); break;
            //                case 17: Before_17_Auto_CloseApproveOrder(wf); break;
            //            }
            //            switch (i-10)
            //            {
            //                case 0: After_0(wf); break;
            //                case 1: After_1(wf); break;
            //                case 2: After_2(wf); break;
            //                case 3: After_3(wf); break;
            //                case 4: After_4(wf); break;
            //                case 5: After_5(wf); break;
            //                case 6: After_6(wf); break;
            //                case 7: After_7(wf); break;
            //                case 8: After_Refined(wf); After_0(wf); break;
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            isFail = true;
            //            Console.WriteLine("{0} {1} {2}", i, j, ex);
            //        }
            //    }
            //Assert.IsFalse(isFail);
        }

        [TestMethod]
        public void ZZT_NewTest_1()
        {
            bool isFail = false;
            var actions = new List<Action>();
            for (var i = 1; i < 18; i++)
            {

                var wf = new Wf { Configuration = { DelayBetweenCheckStatusRepeats = TimeSpan.FromMilliseconds(100) } };
                actions.Add(GetAction(wf, i, 1));
            }

            Parallel.Invoke(new ParallelOptions { MaxDegreeOfParallelism = 1 }, actions.ToArray());
            Assert.IsFalse(isFail);
        }

        [TestMethod]
        public void ZZT_NewTest_2()
        {
            bool isFail = false;
            var actions = new List<Action>();
                for (var j = 0; j < 8; j++)
                {

                    var wf = new Wf {Configuration = {DelayBetweenCheckStatusRepeats = TimeSpan.FromMilliseconds(100)}};
                    actions.Add(GetAction(wf, 1, j));
                }

            Parallel.Invoke(new ParallelOptions{MaxDegreeOfParallelism = 1},actions.ToArray());
            Assert.IsFalse(isFail);
        }

        private Action GetAction(Wf wf,int i, int j)
        {
            return () =>
                {
                    try
                    {
                    switch (i)
                    {
                        case 1:
                            Before_1_Happy(wf);
                            break;
                        case 2:
                            Before_2_HP_CGT(wf);
                            break;
                        case 3:
                            Before_3_CGT(wf);
                            break;
                        case 4:
                            Before_4_CustomerRecall(wf);
                            break;
                        case 5:
                            Before_5_FromCallCenter(wf);
                            break;
                        case 6:
                            Before_6_UmaxHappy(wf);
                            break;
                        case 7:
                            Before_7_Umax_NA(wf);
                            break;
                        case 8:
                            Before_8_Umax_Done(wf);
                            break;
                        case 9:
                            Before_9_NA_Done(wf);
                            break;
                        case 10:
                            Before_10_WM_Paid(wf);
                            break;
                        case 11:
                            Before_11_WM_Paid_CGT(wf);
                            break;
                        case 12:
                            Before_12_WM_Paid_CGT(wf);
                            break;
                        case 13:
                            Before_13_WM_ToPay(wf);
                            break;
                        case 14:
                            Before_14_WM_ToPay_CGT(wf);
                            break;
                        case 15:
                            Before_15_WM_PaidLater(wf);
                            break;
                        case 16:
                            Before_16_WM_FixPayment(wf);
                            break;
                        case 17:
                            Before_17_Auto_CloseApproveOrder(wf);
                            break;
                        case 18:
                            //
                            break;
                        case 19:
                            //
                            break;
                    }
                    Console.WriteLine("{0} {1} begin passed ", i, j);
                    switch (j)
                    {
                        case 0:
                            After_0(wf);
                            break;
                        case 1:
                            After_1(wf);
                            break;
                        case 2:
                            After_2(wf);
                            break;
                        case 3:
                            After_3(wf);
                            break;
                        case 4:
                            After_4(wf);
                            break;
                        case 5:
                            After_5(wf);
                            break;
                        case 6:
                            After_6(wf);
                            break;
                        case 7:
                            After_7(wf);
                            break;
                        case 8:
                            After_Refined(wf);
                            After_0(wf);
                            break;
                        case 9:
                            After_CantDispatch_Fix_In_BO_9(wf);
                            break;
                        case 10:
                            After_1(wf);
                            break;
                        case 11:
                            After_1(wf);
                            break;
                        case 12:
                            After_1(wf);
                            break;
                        case 13:
                            After_1(wf);
                            break;
                        case 14:
                            After_1(wf);
                            break;
                        case 15:
                            After_1(wf);
                            break;
                        case 16:
                            After_1(wf);
                            break;
                    }

                        Console.WriteLine("{0} {1} all passed", i, j);
                    }
                    catch (Exception e)
                    {
                        Console.Error.WriteLine("{0} {1} fail: {2}", i, j, e);
                    }
                };
        }

  
        [TestMethod]
        public void ZZT_Cancel_Approve()
        {
            var wf = new Wf { Configuration = { DelayBetweenCheckStatusRepeats = TimeSpan.FromMilliseconds(100) } };

            CreateOrder(wf);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Отклонено покупателем",
                                new Dictionary<string, object>()
                                    {
                                        {"Причина отказа", "Передумал/Change of mind"}
                                    });
            
            wf.CheckOrderState("отмена", TimeSpan.FromMinutes(0.5));
            wf.AreEqual(()=>wf.Order.OrderInfo.OrderStatusHistory.Last().ReasonText, "Передумал/Change of mind");

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void ZZT_Cancel_Approve_WillBeProcessInBO()
        {
            var wf = new Wf { Configuration = { DelayBetweenCheckStatusRepeats = TimeSpan.FromMilliseconds(100) } };

            CreateOrder(wf);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Будет обрабатываться в БО");

            wf.CheckOrderState("новый заказ", TimeSpan.FromMinutes(0.5));

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void ZZT_Cancel_Approve_CGT_NA()
        {
            var wf = new Wf { Configuration = { DelayBetweenCheckStatusRepeats = TimeSpan.FromMilliseconds(100) } };

            CreateOrder_NotInStock(wf);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");

            wf.CheckOrderState("отмена", TimeSpan.FromMinutes(0.5));

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void ZZT_Cancel_CGT()
        {
            var wf = new Wf { Configuration = { DelayBetweenCheckStatusRepeats = TimeSpan.FromMilliseconds(100) } };

            CreateOrder(wf);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CheckOrderState("уточняется", TimeSpan.FromMinutes(0.5));

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");

            Thread.Sleep(wf.Configuration.DelayLongOperationTimeOut);

            wf.CheckOrderState("отмена", TimeSpan.FromMinutes(0.5));

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void ZZT_Cancel_Umax_Approve()
        {
            var wf = new Wf { Configuration = { DelayBetweenCheckStatusRepeats = TimeSpan.FromMilliseconds(100) } };

            CreateOrder_Umax(wf);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Подтверждено");

            Thread.Sleep(wf.Configuration.DelayLongOperation);

            wf.ChangeUmaxStatus(1, "проведен");

            wf.CallCompleteTask("Approve_Umax", "Позвонить клиенту и уточнить заказ", "Отклонено покупателем",
                                    new Dictionary<string, object>()
                                    {
                                        {"Причина отказа", "Передумал/Change of mind"}
                                    });

            wf.CheckOrderState("отмена", TimeSpan.FromMinutes(0.5));

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void ZZT_Cancel_Umax_CGT()
        {
            var wf = new Wf { Configuration = { DelayBetweenCheckStatusRepeats = TimeSpan.FromMilliseconds(100) } };

            CreateOrder_Umax(wf);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Подтверждено");

            Thread.Sleep(wf.Configuration.DelayLongOperation);

            wf.ChangeUmaxStatus(1, "проведен");

            wf.CallCompleteTask("Approve_Umax", "Позвонить клиенту и уточнить заказ", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Umax", "Позвонить клиенту и уточнить заказ", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Umax", "Позвонить клиенту и уточнить заказ", "Не дозвонился");

            wf.CheckOrderState("отмена", TimeSpan.FromMinutes(0.5));

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();
        }


        [TestMethod]
        public void ZZT_Cancel_Umax_NA()
        {
            var wf = new Wf { Configuration = { DelayBetweenCheckStatusRepeats = TimeSpan.FromMilliseconds(100) } };

            CreateOrder_Umax(wf);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Подтверждено");

            Thread.Sleep(wf.Configuration.DelayLongOperation);

            wf.ChangeUmaxStatus(1, "недоступен");

            wf.CallCompleteTask("Approve_Problem_Umax", "Узнать статус Umax в ЦО", "Отклонено покупателем",
                                    new Dictionary<string, object>()
                                    {
                                        {"Причина отказа", "Передумал/Change of mind"}
                                    });

            wf.CheckOrderState("отмена", TimeSpan.FromMinutes(0.5));

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void ZZT_Cancel_Umax_NA_CGT()
        {
            var wf = new Wf { Configuration = { DelayBetweenCheckStatusRepeats = TimeSpan.FromMilliseconds(100) } };

            CreateOrder_Umax(wf);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Подтверждено");

            Thread.Sleep(wf.Configuration.DelayLongOperation);

            wf.ChangeUmaxStatus(1, "недоступен");

            wf.CallCompleteTask("Approve_Problem_Umax", "Узнать статус Umax в ЦО", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Problem_Umax", "Узнать статус Umax в ЦО", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Problem_Umax", "Узнать статус Umax в ЦО", "Не дозвонился");

            wf.CheckOrderState("отмена", TimeSpan.FromMinutes(0.5));

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void ZZT_Cancel_Umax_Cancel()
        {
            var wf = new Wf { Configuration = { DelayBetweenCheckStatusRepeats = TimeSpan.FromMilliseconds(100) } };

            CreateOrder_Umax(wf);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Подтверждено");
            
            Thread.Sleep(wf.Configuration.DelayLongOperationTimeOut);

            wf.CallCompleteTask("Cancel_Umax", "Позвонить клиенту и уточнить заказ", "Отклонено покупателем",
                                    new Dictionary<string, object>()
                                    {
                                        {"Причина отказа", "Передумал/Change of mind"}
                                    });

            wf.CheckOrderState("отмена", TimeSpan.FromMinutes(0.5));

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void ZZT_Cancel_Umax_Cancel_CGT()
        {
            var wf = new Wf { Configuration = { DelayBetweenCheckStatusRepeats = TimeSpan.FromMilliseconds(100) } };

            CreateOrder_Umax(wf);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Подтверждено");
            
            Thread.Sleep(wf.Configuration.DelayLongOperationTimeOut);

            wf.CallCompleteTask("Cancel_Umax", "Позвонить клиенту и уточнить заказ", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Cancel_Umax", "Позвонить клиенту и уточнить заказ", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Cancel_Umax", "Позвонить клиенту и уточнить заказ", "Не дозвонился");

            wf.CheckOrderState("отмена", TimeSpan.FromMinutes(0.5));

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void ZZT_Cancel_WM_Paid_Approve()   //Деньги пришли
        {
            var wf = new Wf { Configuration = { DelayBetweenCheckStatusRepeats = TimeSpan.FromMilliseconds(100) } };

            CreateOrder_WMPaid(wf);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Отклонено покупателем",
                                new Dictionary<string, object>()
                                    {
                                        {"Причина отказа", "Передумал/Change of mind"}
                                    });

            wf.CallCompleteTask("Return_Payment", "Верните оплату", "Ок");

            wf.CheckOrderState("отмена", TimeSpan.FromMinutes(0.5));

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void ZZT_Cancel_WM_Paid_CGT()     //Деньги пришли
        {
            var wf = new Wf { Configuration = { DelayBetweenCheckStatusRepeats = TimeSpan.FromMilliseconds(100) } };

            wf.AddArticle(1111111);
            wf.AddArticle(2222222);
            wf.SetOrderState("Деньги пришли", "robot_move");

            wf.StartProcess();

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");

            Thread.Sleep(wf.Configuration.DelayLongOperationTimeOut); 
            
            wf.CheckOrderState("отмена", TimeSpan.FromMinutes(0.5));

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void ZZT_Cancel_WM_Paid_CGT2()     //Деньги пришли
        {
            var wf = new Wf { Configuration = { DelayBetweenCheckStatusRepeats = TimeSpan.FromMilliseconds(100) } };

            CreateOrder_WMPaid(wf);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            
            wf.CallCompleteTask("Return_Payment", "Верните оплату", "Ок");

            wf.CheckOrderState("отмена", TimeSpan.FromMinutes(0.5));

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void ZZT_Cancel_WM_PaidLater_Approve()     //Оплачивается <30 мин
        {
            var wf = new Wf { Configuration = { DelayBetweenCheckStatusRepeats = TimeSpan.FromMilliseconds(100) } };

            CreateOrder_WMPaidLater(wf);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Отклонено покупателем",
                                new Dictionary<string, object>()
                                    {
                                        {"Причина отказа", "Передумал/Change of mind"}
                                    });

            wf.CheckOrderState("отмена", TimeSpan.FromMinutes(0.5));

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void ZZT_Cancel_14()     //Оплачивается <30 мин
        {
            var wf = new Wf { Configuration = { DelayBetweenCheckStatusRepeats = TimeSpan.FromMilliseconds(100) } };

            wf.AddArticle(1111111);
            wf.AddArticle(2222222);
            wf.SetOrderState("Оплачивается");

            wf.StartProcess();

            Thread.Sleep(wf.Configuration.DelayLongOperation);

            wf.CallChangeOrderStatus("Деньги пришли", "robot_move");

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");

            wf.CheckOrderState("отмена", TimeSpan.FromMinutes(0.5));

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void ZZT_Cancel_WM_PaidLater_CGT()     //Оплачивается <30 мин
        {
            var wf = new Wf { Configuration = { DelayBetweenCheckStatusRepeats = TimeSpan.FromMilliseconds(100) } };

            CreateOrder_WMPaidLater(wf);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CheckOrderState("уточняется", TimeSpan.FromMinutes(0.5));

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");

            wf.CheckOrderState("отмена", TimeSpan.FromMinutes(0.5));

            Thread.Sleep(TimeSpan.FromSeconds(17));
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void ZZT_Cancel_WM_Cancelled()     //Оплачивается <30 мин
        {
            var wf = new Wf { Configuration = { DelayBetweenCheckStatusRepeats = TimeSpan.FromMilliseconds(100) } };

            CreateOrder_WMToPay(wf);

            Thread.Sleep(wf.Configuration.DelayLongOperationTimeOut);

            wf.CallCancel();

            wf.CheckOrderState("отмена", TimeSpan.FromMinutes(0.5));

            Thread.Sleep(TimeSpan.FromSeconds(27));
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void ZZT_Cancel_WM_ToPay_Approve()     //Оплачивается > 30 мин
        {
            var wf = new Wf { Configuration = { DelayBetweenCheckStatusRepeats = TimeSpan.FromMilliseconds(100) } };

            CreateOrder_WMToPay(wf);

            Thread.Sleep(wf.Configuration.DelayLongOperationTimeOut);

            wf.CallCompleteTask("Check_Online_Payment", "Подтвердить заказ и уточнить статус оплаты", "Отклонено покупателем",
                                new Dictionary<string, object>()
                                    {
                                        {"Причина отказа", "Передумал/Change of mind"}
                                    });

            wf.CallCompleteTask("Return_Payment", "Верните оплату", "Ок");

            wf.CheckOrderState("отмена", TimeSpan.FromMinutes(0.5));

            Thread.Sleep(TimeSpan.FromSeconds(17));
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void ZZT_Cancel_WM_ToPay_CGT()     //Оплачивается > 30 мин
        {
            var wf = new Wf { Configuration = { DelayBetweenCheckStatusRepeats = TimeSpan.FromMilliseconds(100) } };

            CreateOrder_WMToPay(wf);

            Thread.Sleep(wf.Configuration.DelayLongOperationTimeOut);

            wf.CallCompleteTask("Check_Online_Payment", "Подтвердить заказ и уточнить статус оплаты", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Check_Online_Payment", "Подтвердить заказ и уточнить статус оплаты", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Check_Online_Payment", "Подтвердить заказ и уточнить статус оплаты", "Не дозвонился");

            wf.CallCompleteTask("Return_Payment", "Верните оплату", "Ок");

            wf.CheckOrderState("отмена", TimeSpan.FromMinutes(0.5));

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void ZZT_Cancel_WM_ToPay_Fix_CGT()
        {
            var wf = new Wf { Configuration = { DelayBetweenCheckStatusRepeats = TimeSpan.FromMilliseconds(100) } };

            CreateOrder_WMToPay(wf);

            Thread.Sleep(wf.Configuration.DelayLongOperationTimeOut);

            wf.CallCompleteTask("Check_Online_Payment", "Подтвердить заказ и уточнить статус оплаты", "Оплачено");

            Thread.Sleep(wf.Configuration.DelayBetweenAsserts);

            wf.CallCompleteTask("Fix_Broken_Payment", "Проверить оплату", "Сообщил");

            Thread.Sleep(wf.Configuration.DelayLongOperationTimeOut);

            wf.CallCompleteTask("Return_Payment", "Верните оплату", "Ок");

            wf.CheckOrderState("отмена", TimeSpan.FromMinutes(0.5));

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();
        }

        public void ZZT_Cancel_Rejection()
        {
            var wf = new Wf { Configuration = { DelayBetweenCheckStatusRepeats = TimeSpan.FromMilliseconds(100) } };

            CreateOrder(wf);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Подтверждено");

            wf.CheckOrderState("уточнен", TimeSpan.FromMinutes(0.5));

            wf.ChangeUmaxStatus(1, "проведен");
            wf.ChangeUmaxStatus(2, "проведен");

            wf.CheckOrderState("сформирован для логистики", TimeSpan.FromMinutes(0.5));

            wf.CallChangeOrderStatus("диспетчеризирован");

            wf.CallChangeOrderStatus("передано на тс");

            Thread.Sleep(wf.Configuration.DelayLongOperation);

            wf.CallChangeOrderStatus("отказ");
            
            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();
        }


        //***********************************************
        private void Before_1_Happy(Wf wf)     //HappyFlow
        {
            CreateOrder(wf);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Подтверждено");

            wf.CheckOrderState("уточнен", TimeSpan.FromMinutes(0.5));

            wf.ChangeUmaxStatus(1, "проведен");
            wf.ChangeUmaxStatus(2, "проведен"); 

            wf.CheckOrderState("сформирован для логистики", TimeSpan.FromMinutes(0.5));
        }

        private void Before_2_HP_CGT(Wf wf)     //HappyFlow with CantGetThrough
        {
            CreateOrder(wf);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Подтверждено");

            wf.CheckOrderState("уточнен", TimeSpan.FromMinutes(0.5));

            wf.ChangeUmaxStatus(1, "проведен");
            wf.ChangeUmaxStatus(2, "проведен"); 

            wf.CheckOrderState("сформирован для логистики", TimeSpan.FromMinutes(0.5));
        }

        private void Before_3_CGT(Wf wf)
        {
            CreateOrder(wf);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CheckOrderState("уточняется", TimeSpan.FromMinutes(0.5));

            Thread.Sleep(wf.Configuration.DelayLongOperation);

            wf.CallChangeOrderStatus("уточнен");

            wf.ChangeUmaxStatus(1, "проведен");
            wf.ChangeUmaxStatus(2, "проведен"); 

            Thread.Sleep(wf.Configuration.DelayLongOperation);

            wf.CheckOrderState("сформирован для логистики", TimeSpan.FromMinutes(0.5));
        }

        private void Before_4_CustomerRecall(Wf wf)
        {
            CreateOrder(wf);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Покупатель перезвонит сам");

            wf.CheckOrderState("уточняется", TimeSpan.FromMinutes(0.5));

            wf.CallChangeOrderStatus("уточнен");

            wf.ChangeUmaxStatus(1, "проведен");
            wf.ChangeUmaxStatus(2, "проведен"); 

            wf.CheckOrderState("сформирован для логистики", TimeSpan.FromMinutes(0.5));
        }

        private void Before_5_FromCallCenter(Wf wf)
        {
            CreateOrder_CallCenter(wf);

            wf.CheckOrderState("уточнен", TimeSpan.FromMinutes(0.5));

            wf.ChangeUmaxStatus(1, "проведен");
            wf.ChangeUmaxStatus(2, "проведен"); 

            wf.CheckTransition("уточнен", "сформирован для логистики", TimeSpan.FromMinutes(0.5));
            wf.CheckOrderState("сформирован для логистики", TimeSpan.FromMinutes(0.5));
        }

        private void Before_6_UmaxHappy(Wf wf)
        {
            CreateOrder_Umax(wf);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Подтверждено");
            
            Thread.Sleep(wf.Configuration.DelayLongOperation);

            wf.ChangeUmaxStatus(1, "Проведен");
            wf.ChangeUmaxStatus(2, "проведен");

            wf.CallCompleteTask("Approve_Umax", "Позвонить клиенту", "Подтверждено");

            //wf.CheckOrderState("уточнен", TimeSpan.FromMinutes(0.5));

            wf.CheckOrderState("сформирован для логистики", TimeSpan.FromMinutes(0.5));
        }

        private void Before_7_Umax_NA(Wf wf)
        {
            CreateOrder_Umax(wf);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Подтверждено");
            
            Thread.Sleep(wf.Configuration.DelayLongOperation);

            wf.ChangeUmaxStatus(1, "недоступен");

            wf.CallCompleteTask("Approve_Problem_Umax", "Узнать статус Umax в ЦО", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Problem_Umax", "Узнать статус Umax в ЦО", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Problem_Umax", "Узнать статус Umax в ЦО", "Подтверждено");

            wf.CheckOrderState("уточнен", TimeSpan.FromMinutes(0.5));

            wf.ChangeUmaxStatus(1, "проведен");
            wf.ChangeUmaxStatus(2, "проведен"); 

            wf.CheckOrderState("сформирован для логистики", TimeSpan.FromMinutes(0.5));
        }

        private void Before_8_Umax_Done(Wf wf)
        {
            CreateOrder_Umax(wf);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Подтверждено");
            
            Thread.Sleep(wf.Configuration.DelayLongOperationTimeOut);

            wf.CallCompleteTask("Cancel_Umax", "Позвонить клиенту и уточнить заказ", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Cancel_Umax", "Позвонить клиенту и уточнить заказ", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.ChangeUmaxStatus(1, "Проведен");
            wf.ChangeUmaxStatus(2, "проведен"); 

            wf.CallCompleteTask("Cancel_Umax", "Позвонить клиенту и уточнить заказ", "Подтверждено");

            //wf.CheckOrderState("уточнен", TimeSpan.FromMinutes(0.5));

            wf.CheckOrderState("сформирован для логистики", TimeSpan.FromMinutes(0.5));
        }

        private void Before_9_NA_Done(Wf wf)
        {
            CreateOrder_Umax(wf);

            wf.ChangeUmaxStatus(1, "не доступен");

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Подтверждено");

            Thread.Sleep(wf.Configuration.DelayLongOperationTimeOut);
            wf.CallCompleteTask("Cancel_Umax", "Позвонить клиенту и уточнить заказ", "Не дозвонился");

            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Cancel_Umax", "Позвонить клиенту и уточнить заказ", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.ChangeUmaxStatus(1, "Проведен");
            wf.ChangeUmaxStatus(2, "проведен"); 

            wf.CallCompleteTask("Cancel_Umax", "Позвонить клиенту и уточнить заказ", "Подтверждено");

            //wf.CheckOrderState("уточнен", TimeSpan.FromMinutes(0.5));

            wf.CheckOrderState("сформирован для логистики", TimeSpan.FromMinutes(0.5));
        }

        private void Before_10_WM_Paid(Wf wf)    //Деньги пришли
        {
            CreateOrder_WMPaid(wf);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Подтверждено");

            wf.CheckOrderState("уточнен", TimeSpan.FromMinutes(0.5));

            wf.ChangeUmaxStatus(1, "проведен");
            wf.ChangeUmaxStatus(2, "проведен"); 

            wf.CheckOrderState("сформирован для логистики", TimeSpan.FromMinutes(0.5));
        }

        private void Before_11_WM_Paid_CGT(Wf wf)    //Деньги пришли
        {
            CreateOrder_WMPaid(wf);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Подтверждено");

            wf.CheckOrderState("уточнен", TimeSpan.FromMinutes(0.5));

            wf.ChangeUmaxStatus(1, "проведен");
            wf.ChangeUmaxStatus(2, "проведен"); 

            wf.CheckOrderState("сформирован для логистики", TimeSpan.FromMinutes(0.5));
        }

        private void Before_12_WM_Paid_CGT(Wf wf)    //Деньги пришли
        {
            CreateOrder_WMPaid(wf);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Approve_Order", "Позвонить клиенту", "Не дозвонился");

            wf.CheckOrderState("уточняется", TimeSpan.FromMinutes(0.5));
            
            Thread.Sleep(wf.Configuration.DelayLongOperation);

            wf.CallChangeOrderStatus("уточнен");

            wf.CheckOrderState("уточнен", TimeSpan.FromMinutes(0.5));

            wf.ChangeUmaxStatus(1, "проведен");
            wf.ChangeUmaxStatus(2, "проведен"); 

            wf.CheckOrderState("сформирован для логистики", TimeSpan.FromMinutes(0.5));
        }

        private void Before_13_WM_ToPay(Wf wf)    //Деньги пришли
        {
            CreateOrder_WMToPay(wf);

            Thread.Sleep(wf.Configuration.DelayLongOperationTimeOut);

            wf.SetOrderState("Деньги пришли", "robot_move");

            wf.CallCompleteTask("Check_Online_Payment", "Позвонить клиенту", "Оплачено");

            Thread.Sleep(wf.Configuration.DelayLongOperation);
            
            wf.CheckOrderState("уточнен", TimeSpan.FromMinutes(0.5));

            wf.ChangeUmaxStatus(1, "проведен");
            wf.ChangeUmaxStatus(2, "проведен"); 

            wf.CheckOrderState("сформирован для логистики", TimeSpan.FromMinutes(0.5));
        }

        private void Before_14_WM_ToPay_CGT(Wf wf)    //Деньги пришли
        {
            CreateOrder_WMToPay(wf);

            Thread.Sleep(wf.Configuration.DelayLongOperationTimeOut);

            wf.CallCompleteTask("Check_Online_Payment", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.CallCompleteTask("Check_Online_Payment", "Позвонить клиенту", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);

            wf.SetOrderState("Деньги пришли", "robot_move");

            wf.CallCompleteTask("Check_Online_Payment", "Позвонить клиенту", "Оплачено");

            Thread.Sleep(wf.Configuration.DelayLongOperation); 
            
            wf.CheckOrderState("уточнен", TimeSpan.FromMinutes(0.5));

            wf.ChangeUmaxStatus(1, "проведен");
            wf.ChangeUmaxStatus(2, "проведен"); 

            wf.CheckOrderState("сформирован для логистики", TimeSpan.FromMinutes(0.5));
        }

        private void Before_15_WM_PaidLater(Wf wf)    //Оплачивается > 30
        {
            CreateOrder_WMToPay(wf);

            Thread.Sleep(wf.Configuration.DelayLongOperationTimeOut);

            wf.SetOrderState("Деньги пришли", "robot_move");

            wf.CallCompleteTask("Check_Online_Payment", "Подтвердить заказ и уточнить статус оплаты", "Оплачено");

            Thread.Sleep(wf.Configuration.DelayLongOperation);
            Thread.Sleep(wf.Configuration.DelayLongOperation);
            Thread.Sleep(wf.Configuration.DelayLongOperation); 

            wf.CheckOrderState("уточнен", TimeSpan.FromMinutes(0.5));

            wf.ChangeUmaxStatus(1, "проведен");
            wf.ChangeUmaxStatus(2, "проведен"); 

            wf.CheckOrderState("сформирован для логистики", TimeSpan.FromMinutes(0.5));

        }

        private void Before_16_WM_FixPayment(Wf wf)    //Оплачивается > 30
        {
            CreateOrder_WMToPay(wf);

            Thread.Sleep(wf.Configuration.DelayLongOperationTimeOut);

            wf.CallCompleteTask("Check_Online_Payment", "Подтвердить заказ и уточнить статус оплаты", "Оплачено");

            Thread.Sleep(wf.Configuration.DelayLongOperationTimeOut);

            wf.CallCompleteTask("Fix_Broken_Payment", "Проверить оплату", "Сообщил");

            wf.SetOrderState("Деньги пришли", "robot_move");

            wf.CheckOrderState("уточнен", TimeSpan.FromMinutes(0.5));

            wf.ChangeUmaxStatus(1, "проведен");
            wf.ChangeUmaxStatus(2, "проведен"); 

            wf.CheckOrderState("сформирован для логистики", TimeSpan.FromMinutes(0.5));

        }

        private void Before_17_Auto_CloseApproveOrder(Wf wf)    //Первая задача закрыта не в TT
        {
            CreateOrder(wf);

            wf.CallChangeOrderStatus("уточнен");

            Thread.Sleep(wf.Configuration.DelayLongOperation);

            wf.CheckOrderState("уточнен", TimeSpan.FromMinutes(0.5));

            wf.ChangeUmaxStatus(1, "проведен");
            wf.ChangeUmaxStatus(2, "проведен"); 

            wf.CheckOrderState("сформирован для логистики", TimeSpan.FromMinutes(0.5));
        }

        private void CreateOrder_WMPaidLater(Wf wf)
        {
            wf.AddArticle(1111111);
            wf.AddArticle(1111112);
            wf.SetOrderState("Оплачивается");

            wf.StartProcess();

            Thread.Sleep(wf.Configuration.DelayLongOperation);

            wf.CallChangeOrderStatus("Деньги пришли", "robot_move"); 
        }

        private void CreateOrder(Wf wf)
        {
            wf.AddArticle(1111111);
            wf.AddArticle(1111112);

            wf.StartProcess();
        }

        private void CreateOrder_NotInStock(Wf wf)
        {
            wf.AddArticle(1111111);
            wf.AddArticle(2222222);

            wf.StartProcess();
        }

        private void CreateOrder_CallCenter(Wf wf)
        {
            wf.AddArticle(1111111);
            wf.AddArticle(2222222);
            wf.SetOrderSource("TT CallCenter");

            wf.StartProcess();
        }

        private void CreateOrder_Umax(Wf wf)
        {
            wf.AddArticle(1111111, "");
            wf.AddArticle(1111112);

            wf.StartProcess();
        }

        private void CreateOrder_WMPaid(Wf wf)
        {
            wf.AddArticle(1111111);
            wf.AddArticle(1111112);
            wf.SetPaymentType("онлайн-оплата");
            wf.SetOrderState("Деньги пришли", "robot_move");

            wf.StartProcess();
        }

        private void CreateOrder_WMToPay(Wf wf)
        {
            wf.AddArticle(1111111);
            wf.AddArticle(1111112);
            wf.SetOrderState("Оплачивается");
            wf.SetPaymentType("онлайн-оплата");
            
            wf.StartProcess();
        }

        private void After_0(Wf wf)
        {
            wf.CallChangeOrderStatus("диспетчеризирован");
            //Console.WriteLine("диспетчеризирован");
            wf.CallChangeOrderStatus("передано на тс");
            //Console.WriteLine("передано на тс");
            //wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Качество оценено",
            //                    new Dictionary<string, object>()
            //                        {
            //                            {"Дата фактической доставки", DateTime.Now},
            //                            {"Комментарий", "Все хорошо"},
            //                            {"Оценка", "5"}
            //                        });
            
            Thread.Sleep(wf.Configuration.DelayLongOperation);

            wf.CallChangeOrderStatus("доставлен");
            //Console.WriteLine("доставлен");
            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();

        }

        private void After_1(Wf wf)
        {
            wf.CallChangeOrderStatus("диспетчеризирован");
            //Console.WriteLine("диспетчеризирован ");
            wf.CallChangeOrderStatus("доставляется клиенту");
            //Console.WriteLine("доставляется клиенту ");
            //wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Качество оценено",
            //                    new Dictionary<string, object>()
            //                        {
            //                            {"Дата фактической доставки", DateTime.Now},
            //                            {"Комментарий", "Все хорошо"},
            //                            {"Оценка", "5"}
            //                        });
            
            Thread.Sleep(wf.Configuration.DelayLongOperation);

            wf.CallChangeOrderStatus("доставлен");
            //Console.WriteLine("доставлен");
            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();
        }

        private void After_2(Wf wf)
        {
            wf.CallChangeOrderStatus("диспетчеризирован");
            //Console.WriteLine("диспетчеризирован ");
            wf.CallChangeOrderStatus("комплектация невозможна");
            //Console.WriteLine("комплектация невозможна ");
            wf.CallCompleteTask("Fix_delivery", "Уточнить данные заказа", "Покупатель согласен");
            //Console.WriteLine("Fix_delivery ");
            wf.CheckOrderState("отмена", TimeSpan.FromMinutes(0.5));
            //Console.WriteLine("отмена ");
            wf.CheckNewOrderCreation();
            //Console.WriteLine("CheckNewOrderCreation");
            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();
        }

        private void After_3(Wf wf)
        {
            wf.CallChangeOrderStatus("диспетчеризирован");
            //Console.WriteLine("диспетчеризирован");
            wf.CallChangeOrderStatus("комплектация невозможна");
            //Console.WriteLine("комплектация невозможна");
            wf.CallCompleteTask("Fix_delivery", "Уточнить данные заказа", "Покупатель отказался",
                                    new Dictionary<string, object>()
                                    {
                                        {"Покупатель отказался", "НЕ понравился товар /Did not like item"}
                                    });
            //Console.WriteLine("Fix_delivery");
            wf.CheckOrderState("отмена", TimeSpan.FromMinutes(0.5));
            //Console.WriteLine("отмена");
            wf.CheckNewOrderCreation(false);
            //Console.WriteLine("CheckNewOrderCreation");
            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();
        }

        private void After_4(Wf wf)
        {
            wf.CallChangeOrderStatus("диспетчеризация невозможна");
            //Console.WriteLine("диспетчеризация невозможна");
            wf.CallCompleteTask("Fix_dispatch", "Уточнить данные заказа", "Покупатель согласен");
            //Console.WriteLine("Fix_dispatch");
            wf.CheckOrderState("сформирован для логистики");
            //Console.WriteLine("сформирован для логистики");
            wf.CallChangeOrderStatus("диспетчеризирован");
            //Console.WriteLine("диспетчеризирован");
            wf.CallChangeOrderStatus("доставляется клиенту");
            //Console.WriteLine("доставляется клиенту");
            //wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Качество оценено",
            //                    new Dictionary<string, object>()
            //                        {
            //                            {"Дата фактической доставки", DateTime.Now},
            //                            {"Комментарий", "Все хорошо"},
            //                            {"Оценка", "5"}
            //                        });

            Thread.Sleep(wf.Configuration.DelayLongOperation);

            wf.CallChangeOrderStatus("доставлен");
            //Console.WriteLine("доставлен");
            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();
        }

        private void After_5(Wf wf)
        {
            wf.CallChangeOrderStatus("диспетчеризация невозможна");
            //Console.WriteLine("диспетчеризация невозможна");
            wf.CallCompleteTask("Fix_dispatch", "Уточнить данные заказы", "Покупатель отказался",
                                    new Dictionary<string, object>
                                    {
                                        {"Покупатель отказался", "Покупатель не доступен/Customer was not available"}
                                    });
            //Console.WriteLine("Fix_dispatch");
            wf.CheckOrderState("отмена", TimeSpan.FromMinutes(0.5));
            //Console.WriteLine("отмена");
            wf.CheckNewOrderCreation(false);
            //Console.WriteLine("CheckNewOrderCreation");
            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();
        }


        private void After_6(Wf wf)
        {
            wf.CallChangeOrderStatus("уточнен");
            //Console.WriteLine("уточнен");
            wf.CallCompleteTask("Fix_Broken_Trans", "Уточнить данные заказа", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);
            //Console.WriteLine("Fix_Broken_Trans");
            wf.CallCompleteTask("Fix_Broken_Trans", "Уточнить данные заказа", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);
            //Console.WriteLine("Fix_Broken_Trans");
            wf.CallCompleteTask("Fix_Broken_Trans", "Уточнить данные заказа", "Не дозвонился");
            //Console.WriteLine("Fix_Broken_Trans");
            wf.CheckOrderState("отмена", TimeSpan.FromMinutes(0.5));
            //Console.WriteLine("отмена");
            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();

        }

        private void After_7(Wf wf)
        {
            wf.CallChangeOrderStatus("уточнен");
            //Console.WriteLine("уточнен");
            wf.CallCompleteTask("Fix_Broken_Trans", "Уточнить данные заказа", "Отклонено покупателем",
                             new Dictionary<string, object>()
                                    {
                                        {"Причина отмены", "Переоформлено"}
                                    });
            //Console.WriteLine("Fix_Broken_Trans");
            wf.CheckOrderState("отмена", TimeSpan.FromMinutes(0.5));
            //Console.WriteLine("отмена");
            wf.CheckNewOrderCreation(false);
            //Console.WriteLine("CheckNewOrderCreation");
            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();
        }

        private void After_Refined(Wf wf)
        {
            wf.CallChangeOrderStatus("уточнен");
            //Console.WriteLine("уточнен");
            wf.CallCompleteTask("Fix_Broken_Trans", "Уточнить данные заказа", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);
            //Console.WriteLine("Fix_Broken_Trans");
            wf.CallCompleteTask("Fix_Broken_Trans", "Уточнить данные заказа", "Не дозвонился");
            Thread.Sleep(wf.Configuration.DelayAfterCantGetThrough);
            //Console.WriteLine("Fix_Broken_Trans");
            wf.CallCompleteTask("Fix_Broken_Trans", "Уточнить данные заказа", "Исправлено");
            //Console.WriteLine("Fix_Broken_Trans");
            wf.CheckOrderState("уточнен", TimeSpan.FromMinutes(0.5));
            //Console.WriteLine("уточнен");
            wf.CheckOrderState("сформирован для логистики", TimeSpan.FromMinutes(0.5));
            //Console.WriteLine("сформирован для логистики");
        }

        private void After_CantDispatch_Fix_In_BO_9(Wf wf)
        {
            wf.CallChangeOrderStatus("диспетчеризация невозможна");
            //Console.WriteLine("диспетчеризация невозможна");
            wf.CallChangeOrderStatus("сформирован для логистики");
            //Console.WriteLine("сформирован для логистики");
            wf.CallChangeOrderStatus("диспетчеризирован");
            //Console.WriteLine("диспетчеризирован");
            wf.CallChangeOrderStatus("доставляется клиенту");
            //Console.WriteLine("доставляется клиенту");
            //wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Качество оценено",
            //                    new Dictionary<string, object>()
            //                        {
            //                            {"Дата фактической доставки", DateTime.Now},
            //                            {"Комментарий", "Все хорошо"},
            //                            {"Оценка", "5"}
            //                        });

            Thread.Sleep(wf.Configuration.DelayLongOperation);

            wf.CallChangeOrderStatus("доставлен");
            //Console.WriteLine("доставлен");
            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();
        }
    }
}

