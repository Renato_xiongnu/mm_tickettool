﻿using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TT.IntegrationTester.ZZT;

namespace TT.IntegrationTester.ZZTIntegrationTest
{
    [TestClass]
    public class SimpleTest
    {
        [TestMethod]
        public void SimpleTester_Test()
        {
            var wf = new Wf();

            wf.Order.Delivery.ExpectedDeliveryTo = DateTime.Now.AddMinutes(-10);
            wf.StartProcess();
            Thread.Sleep(TimeSpan.FromMinutes(.5));

            wf.CallCompleteTask("Approve_Order", "Подтверждено");
            Thread.Sleep(TimeSpan.FromMinutes(8));

            wf.CallChangeOrderStatus("диспетчеризирован");
            Thread.Sleep(TimeSpan.FromMinutes(1));

            wf.CallChangeOrderStatus("комплектация невозможна");
            Thread.Sleep(TimeSpan.FromMinutes(1));

            wf.CallCompleteTask("Fix_delivery", "Покупатель отказался",
                                new Dictionary<string, object>
                                    {
                                        {
                                            "Покупатель отказался",
                                            "Не устроило качество товара / BAD  quality"
                                        }
                                    });
        }

        [TestMethod]
        public void SimpleTester_UserCancelOrder_Test()
        {
            var wf = new Wf();

            wf.Order.Delivery.ExpectedDeliveryTo = DateTime.Now.AddMinutes(-10);
            wf.StartProcess();
            Thread.Sleep(TimeSpan.FromMinutes(.5));

            wf.CallCompleteTask("Approve_Order", "Отклонено покупателем",
                                new Dictionary<string, object>
                                    {
                                        {
                                            "Причина отказа",
                                            "Хотел доставку в тот же день/Want same day delivery"
                                        }
                                    });
        }

        [TestMethod]
        public void SimpleTester_UserCancel_Test()
        {
            var wf = new Wf();

            wf.Order.Delivery.ExpectedDeliveryTo = DateTime.Now.AddMinutes(-10);
            wf.StartProcess();
            Thread.Sleep(TimeSpan.FromMinutes(.5));

            //wf.CallCompleteTask("Approve_Order", "Подтверждено");
            //Thread.Sleep(TimeSpan.FromMinutes(1));
            wf.CallCancel();
        }
    }
}
