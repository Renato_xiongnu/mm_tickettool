using System.Collections.Generic;

namespace TT.IntegrationTester
{
    public interface IWf
    {
        void CallCompleteTask(string taskName, string task, string outcome);
        void CallCompleteTask(string taskName, string task, string outcome,Dictionary<string,object> requiredFields);
        void CallCompleteTask(string taskName, string outcome);
        void CallCompleteTask(string taskName, string outcome,
            Dictionary<string, object> requiredFields);

        string StartProcess();
        void CallCancel();
        void AssertTicketClosed();

        void AddArticle(int article);
    }
}