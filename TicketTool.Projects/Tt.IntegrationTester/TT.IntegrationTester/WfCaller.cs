﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TT.IntegrationTester
{
    public class WfCaller : IWfCaller
    {
        private string _ticketId;
        private string _orderId;
        private bool _processStarted;
        private object _service;

        public WfCaller(object service, string orderId)
        {
            if (service == null)
            {
                throw new ArgumentNullException("service");
            }
            if (string.IsNullOrWhiteSpace(orderId))
            {
                throw new ArgumentOutOfRangeException("orderId");
            }

            _service = service;
            _orderId = orderId;
        }

        public bool CallCancel()
        {
            ThrowIfProcessNotStarted();

            var serviceType = _service.GetType();
            var methodName = "Cancel";
            var method = serviceType.GetMethod(methodName);
            if (method == null)
            {
                throw new ApplicationException(string.Format("Method {0} not found", methodName));
            }

            var parameters = new object[] 
            {
                OrderId, "Покупатель отказался", "admin2"
            };

            var result = method.Invoke(_service, parameters);
            return (bool)result;
        }

        public bool CallApprove()
        {
            ThrowIfProcessNotStarted();

            var serviceType = _service.GetType();
            var methodName = "ApproveOrder_External";
            var method = serviceType.GetMethod(methodName);
            if (method == null)
            {
                throw new ApplicationException(string.Format("Method {0} not found", methodName));
            }
            
            var parameters = new object[] {  "admin2", OrderId };

            method.Invoke(_service, parameters);
            return true;
        }

        public bool CallCompleteTask(string taskName, string outcome, Dictionary<string, object> requestedFields,
                                 string performer = "")
        {
            ThrowIfProcessNotStarted();

            if (string.IsNullOrEmpty(taskName)){ throw new ArgumentOutOfRangeException("taskName"); }
            if (string.IsNullOrEmpty(outcome)) { throw new ArgumentOutOfRangeException("taskName"); }

            var serviceType = _service.GetType();
            var methodName = string.Format("{0}{1}", taskName, "Continue");
            var method = serviceType.GetMethod(methodName);
            if (method == null)
            {
                throw new ApplicationException(string.Format("Method {0} not found", methodName));
            }

            requestedFields = ProcessRequestFields(outcome,requestedFields);
            var parameters = new object[] {_ticketId, outcome, requestedFields, performer};
            var result = method.Invoke(_service, parameters);
            return (bool)result.GetType().GetProperty("CompleteSuccessful").GetValue(result);
        }

        private Dictionary<string, object> ProcessRequestFields(string outcome,
                                                                Dictionary<string, object> requestedFields)
        {
            return
                requestedFields.ToDictionary(
                    requestedField => string.Format("{0};#{1}", outcome, requestedField.Key),
                    requestedField => requestedField.Value);
        }

        public void StartProcess(string sapCode)
        {
            _processStarted = true;
            _ticketId = (string) _service.GetType().GetMethod("StartProcess").Invoke(_service,
                new object[] { _orderId, sapCode });
        }

        public string TicketId  
        {
            get { return _ticketId; }
        }

        public string OrderId
        {
            get { return _orderId; }
        }

        public void ProlongReserve()
        {
            ThrowIfProcessNotStarted();

            var serviceType = _service.GetType();
            var methodName = "UpdateOrderExpiration_External";
            var method = serviceType.GetMethod(methodName);
            if (method == null)
            {
                throw new ApplicationException(string.Format("Method {0} not found", methodName));
            }

            var parameters = new object[] { "admin2", _orderId };

            method.Invoke(_service, parameters);
        }

        private void ThrowIfProcessNotStarted()
        {
            if (!_processStarted)
            {
                throw new ApplicationException("Method StartProcess must be called before execution");
            }
        }
    }
}
