﻿using System;

namespace TT.IntegrationTester
{
    public class WfTestConfiguration
    {
        public TimeSpan DelayBetweenCheckStatusRepeats = TimeSpan.FromMilliseconds(100);
        public TimeSpan DelayBetweenAsserts = TimeSpan.FromSeconds(2);
        public TimeSpan DelayAfterChangeStatus = TimeSpan.FromSeconds(30);
        public TimeSpan DelayLongOperation = TimeSpan.FromSeconds(30);
        public TimeSpan DelayLongOperationTimeOut = TimeSpan.FromSeconds(75);
        public TimeSpan DelayAfterCantGetThrough = TimeSpan.FromSeconds(45);
    }
}
