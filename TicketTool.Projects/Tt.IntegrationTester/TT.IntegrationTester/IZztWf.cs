using System;

namespace TT.IntegrationTester
{
    public interface IZztWf
    {
        void CallChangeOrderStatus(string toStatus);
        void CheckOrderState(string state);
        void CheckOrderState(string state, TimeSpan delay);
        void ChangeUmaxStatus(int articleNum, string newState);
        void CheckNewOrderCreation(bool shouldBeCreated = true);
        void SetOrderSource(string orderSource);
        void SetOrderState(string orderState);
        void SetOrderState(string newState, string changedBy);
        void SetPaymentType(string paymentType);
        void CheckTransition(string fromState, string toState, TimeSpan delay);
        void AreEqual<T>(Func<T> expected, T actual);
        void CallChangeOrderStatus(string toStatus, string whoChanged);
        void AddArticle(int article, string umaxState);
    }
}