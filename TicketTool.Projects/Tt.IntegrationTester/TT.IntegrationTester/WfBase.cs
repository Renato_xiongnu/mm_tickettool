﻿using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TT.IntegrationTester
{
    public abstract class WfBase:IWf
    {
        protected IWfCaller Caller;
        private readonly TicketToolServiceClient _ttClient;
        public WfTestConfiguration Configuration { get; private set; }
        protected abstract object GetWfService();
        protected string OrderId { get; set; }

        protected WfBase()
        {
            _ttClient = new TicketToolServiceClient();
            Configuration = new WfTestConfiguration();
        }

        public void CallCompleteTask(string taskName, string task, string outcome)
        {
            CallCompleteTask(taskName, outcome, new Dictionary<string, object>());
        }

        public void CallCompleteTask(string taskName, string task, string outcome, Dictionary<string, object> requiredFields)
        {
            CallCompleteTask(taskName, outcome, requiredFields);
        }

        /// <summary>
        /// Завершить задачу
        /// </summary>
        /// <param name="taskName">Название задачи</param>
        /// <param name="outcome">Исход</param>
        public void CallCompleteTask(string taskName, string outcome)
        {
            CallCompleteTask(taskName, outcome, new Dictionary<string, object>());
        }

        /// <summary>
        /// Завершить задачу
        /// </summary>
        /// <param name="taskName">Название задачи</param>
        /// <param name="outcome">Исход</param>
        /// <param name="requiredFields">Словарь дополнительных атрибутов (причина отмены, номер документа)</param>
        public void CallCompleteTask(string taskName, string outcome,
                                 Dictionary<string, object> requiredFields)
        {
            if (!Caller.CallCompleteTask(taskName, outcome, requiredFields))
            {
                throw new ApplicationException(string.Format("Complete task failed: {0}", taskName));
            }
            Thread.Sleep(Configuration.DelayBetweenAsserts);
        }

        /// <summary>
        /// Запустить процесс
        /// </summary>
        /// <returns></returns>
        public string StartProcess()
        {
            if (OrderId == null)
            {
                throw new ApplicationException("OrderId must be set before start process!");
            }
            Caller = new WfCaller(GetWfService(), OrderId);
            Caller.StartProcess("R006");
            
            return OrderId;
        }
        
        public abstract void CallCancel();

        /// <summary>
        /// Проверить, что Ticket закрыт
        /// </summary>
        public void AssertTicketClosed()
        {
            var exceptionCatched = false;
            try
            {
                _ttClient.GetTicket(Caller.OrderId); //TODO
            }
            catch (Exception)
            {
                exceptionCatched = true;
            }

            if (!exceptionCatched)
            {
                Assert.Fail("Ticket wasn't closed");
            }
        }

        public abstract void AddArticle(int article);
    }
}
