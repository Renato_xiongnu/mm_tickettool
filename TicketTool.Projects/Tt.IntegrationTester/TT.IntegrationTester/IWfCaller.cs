﻿using System.Collections.Generic;

namespace TT.IntegrationTester
{
    public interface IWfCaller
    {
        bool CallCompleteTask(string taskName, string outcome, Dictionary<string, object> requestedFields,
                          string performer = "");

        void StartProcess(string sapCode);

        bool CallCancel();

        bool CallApprove();
        
        string TicketId { get; }

        string OrderId { get; }
        void ProlongReserve();
    }
}