﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TT.IntegrationTester
{
    public interface IWfMm
    {
        void AddArticle(int articleId, int count);
        void ChangeToDelivery();//удалить адрес при этом, SapCode - под вопросом
        void ChangeToPickup();//добавить адрес
        void SetArticleState(int index, string state);//Мок резервов + битый артикул (все 2)
            //wf.SetArticleState(0, "Альтернатива");
            //wf.SetArticleState(1, "Отложен");
    }
}
