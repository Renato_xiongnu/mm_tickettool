﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Orders.Backend.IntegrationTester
{
    [TestClass]
    public class CloseTicketUtil
    {
        [TestMethod]
        public void CloseTicket_Util()
        {
            var auth = new Proxy.TTAuth.AuthenticationServiceClient();
            auth.LogIn("admin2", "2@admin2");

            var client = new Proxy.TT.TicketToolServiceClient();

            client.CloseTicket("7054577");
        }
    }
}
