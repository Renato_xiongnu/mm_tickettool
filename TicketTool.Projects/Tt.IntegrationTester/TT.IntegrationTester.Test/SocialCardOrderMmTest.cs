﻿using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Orders.Backend.Model;
using TT.IntegrationTester.MM;
using TT.IntegrationTester.MM.Proxy.OldOrders;
using PaymentType = TT.IntegrationTester.MM.Proxy.OldOrders.PaymentType;
using System.Diagnostics;

namespace Orders.Backend.IntegrationTester
{
    [TestClass]
    public class SocialCardOrderMmTest
    {
        private string OrderId;

        [TestInitialize]
        public void Init()
        {
            OrderId = null;
        }

        [TestMethod]
        public void SocCardCase_01()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 20000,
                        Qty = 1
                    }
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                true);

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(10));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено"
                //,
                //new Dictionary<string, object>
                //{
                //    {"Не сообщать номер резерва по телефону", false}
                //}
                );

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Отложен");

            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "ОК");

            wf.CallCompleteTask("Delivery_Approval", "Согласуйте дату доставки", "OK",
                new Dictionary<string, object>
                {
                    {"Дата доставки", DateTime.UtcNow.AddMinutes(5)}
                });

            wf.PayReserve();

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Опрос",
                                new Dictionary<string, object>()
                                    {
                                        {"Оцените по 5 балльной шкале работу колл – центра", "2"},
                                        {"Причина 1 (работа колл-центра)", "Поздно  позвонили для подтверждения заказа"},
                                        {"Причина 2 (работа колл-центра)", "Сотрудник грубил"}
                                    });

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderClosed();
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void SocCardCase_02()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 10000,
                        Qty = 1
                    },
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                true);

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Отклонено покупателем",
                new Dictionary<string, object>
                {
                    {"Причина отказа", "Нашел дешевле"}
                });

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderRejected("Нашел дешевле");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void SocCardCase_03()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 10000,
                        Qty = 1
                    },
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                true);

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено",
                new Dictionary<string, object>
                {
                    {"Не сообщать номер резерва по телефону", false}
                });

            //wf.CallCompleteTask("Check_And_Correct_Order", "Проверьте артикулы", "Проверил");

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Review_Reserve", "Проверьте заказ", "Отклонено покупателем",
                new Dictionary<string, object>
                {
                    {"Причина отказа", "Нашел дешевле"}
                });

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderRejected("Нашел дешевле");
            wf.AssertTicketClosed();
        }

        

        [TestMethod]
        public void SocCardCase_05()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 20000,
                        Qty = 3
                    },
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                true);

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено",
                 new Dictionary<string, object>
                {
                    {"Не сообщать номер резерва по телефону", false}
                });

            wf.CallCompleteTask("Review_Reserve", "Проверьте заказ", "Отклонено покупателем",
                new Dictionary<string, object>
                {
                    {"Причина отказа", "Ошибка WWS"}
                });

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderRejected("Ошибка WWS");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void SocCardCase_06()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {

                    new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 20000,
                        Qty = 3
                    },
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                true,
                "ОТМЕНЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено",
                new Dictionary<string, object>
                {
                    {"Не сообщать номер резерва по телефону", true}
                });

            Thread.Sleep(TimeSpan.FromSeconds(10));
            wf.SetArticleState(0, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Bad_Reserve_Approval", "Проверьте резерв", "Покупатель согласен");

            wf.CallCompleteTask("Delivery_Approval", "Согласуйте дату доставки", "OK",
                new Dictionary<string, object>
                {
                    {"Дата доставки", DateTime.Today}
                });

            wf.CancelReserve();
            Thread.Sleep(TimeSpan.FromSeconds(10));
            wf.CallCompleteTask("Cancel_Order_In_Process_And_Return_Articles", "Верните артикулы", "OK");

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderRejected("Резерв отменен в WWS сотрудником магазина");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void SocCardCase_07()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1159035",
                        Price = 20000,
                        Qty = 3
                    },
                  
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                true);

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено",
                new Dictionary<string, object>
                {
                    {"Не сообщать номер резерва по телефону", false}
                });

            Thread.Sleep(TimeSpan.FromSeconds(7));
            
            wf.SetArticleState(0, "Отложен");

            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Bad_Reserve_Approval", "Проверьте резерв", "Покупатель согласен");

            wf.CallCompleteTask("Check_Reserved_Items", "Замените товар в резерве", "Готово");

            wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "ОК");

            wf.CallCompleteTask("Delivery_Approval", "Согласуйте дату доставки", "Отклонено покупателем",
                new Dictionary<string, object>
                {
                    {"Причина отказа", "Не устроили условия доставки"}
                });

            wf.CallCompleteTask("Return_Articles", "Верните артикулы", "Вернул");

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderRejected("Не устроили условия доставки");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void SocCardCase_08()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1159035",
                        Price = 20000,
                        Qty = 5
                    },
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                false);

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено",
                new Dictionary<string, object>
                {
                    {"Не сообщать номер резерва по телефону", false}
                });

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.SetArticleState(0, "Отложен");

            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Bad_Reserve_Approval", "Проверьте резерв", "Покупатель согласен");

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.RemoveArticle(1);
            wf.CallCompleteTask("Bad_Reserve_Approval", "Проверьте резерв", "Покупатель согласен");

            wf.CallCompleteTask("Review_Reserve", "Проверьте заказ", "OK");

            Thread.Sleep(TimeSpan.FromSeconds(7)); 
            wf.RemoveArticle(1); 
            wf.CallCompleteTask("Review_Reserve", "Проверьте заказ", "OK");


            wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "Отклонено покупателем",
                new Dictionary<string, object>
                {
                    {"Причина отказа", "Нашел дешевле"}
                });

            wf.CallCompleteTask("Return_Articles", "Верните артикулы", "Вернул");

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderRejected("Нашел дешевле");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void SocCardCallCancel_01()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1159035",
                        Price = 10000,
                        Qty = 2
                    },
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                false);

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCancel();

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderRejected("Покупатель отказался");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void SocCardCallCancel_02()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "_222222",
                        Price = 20000,
                        Qty = 3
                    },
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                true);

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено",
                 new Dictionary<string, object>
                {
                    {"Не сообщать номер резерва по телефону", false}
                });

            wf.CallCancel();

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderRejected("Покупатель отказался");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void SocCardCallCancel_03()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 20000,
                        Qty = 3
                    }
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                true);

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено",
                 new Dictionary<string, object>
                {
                    {"Не сообщать номер резерва по телефону", false}
                });

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Review_Reserve", "Проверьте заказ", "OK");

            Thread.Sleep(TimeSpan.FromSeconds(10));
            wf.SetArticleState(0, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");
            wf.CallCancel();
            wf.CallCompleteTask("Cancel_Order_and_Return_Articles", "Верните артикулы", "Вернул");
            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderRejected("Покупатель отказался");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void SocCardCallCancel_04()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 20000,
                        Qty = 3
                    },
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                true);

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено",
                 new Dictionary<string, object>
                {
                    {"Не сообщать номер резерва по телефону", false}
                });

            Thread.Sleep(TimeSpan.FromSeconds(7));

         //   wf.CallCompleteTask("Review_Reserve", "Проверьте заказ", "OK");

            Thread.Sleep(TimeSpan.FromSeconds(10));
            wf.SetArticleState(0, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "ОК");
            wf.CallCancel();
            wf.CallCompleteTask("Cancel_Order_and_Return_Articles", "Верните артикулы", "Вернул");
            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderRejected("Покупатель отказался");
            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertTicketClosed();
        }


        //Дальше не прогоняла
        [TestMethod]
        public void SocCardCallCancel_05()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 20000,
                        Qty = 3
                    },
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                true);

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено",
                 new Dictionary<string, object>
                {
                    {"Не сообщать номер резерва по телефону", false}
                });

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.SetArticleState(0, "Не найден");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCancel();

            wf.CallCompleteTask("Cancel_Order_and_Return_Articles", "Верните артикулы", "Вернул");

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderRejected("Покупатель отказался");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void SocCardCallCancel_06()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 20000,
                        Qty = 3
                    },
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                true);

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено",
                 new Dictionary<string, object>
                {
                    {"Не сообщать номер резерва по телефону", false}
                });

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.SetArticleState(0, "Отложен");
            wf.AddArticle(2222222, 2);
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Bad_Reserve_Approval", "Проверьте резерв", "Покупатель согласен");

            wf.CallCompleteTask("Review_Reserve", "Проверьте заказ", "OK");

            wf.CallCancel();

            wf.CallCompleteTask("Cancel_Order_and_Return_Articles", "Верните артикулы", "Вернул");

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderRejected("Покупатель отказался");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void SocCardCallCancel_07()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 20000,
                        Qty = 3
                    },
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                true);

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено",
                 new Dictionary<string, object>
                {
                    {"Не сообщать номер резерва по телефону", false}
                });

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.SetArticleState(0, "Неправильная цена");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Bad_Reserve_Approval", "Проверьте резерв", "Покупатель согласен");

            wf.CallCancel();

            wf.CallCompleteTask("Cancel_Order_and_Return_Articles", "Верните артикулы", "Вернул");

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderRejected("Покупатель отказался");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void SocCardCallCancel_08()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 20000,
                        Qty = 3
                    },
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                true);

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено",
                 new Dictionary<string, object>
                {
                    {"Не сообщать номер резерва по телефону", false}
                });

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.SetArticleState(0, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCancel();

            wf.CallCompleteTask("Cancel_Order_and_Return_Articles", "Верните артикулы", "Вернул");

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderRejected("Покупатель отказался");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void SocCardCallCancel_09()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 20000,
                        Qty = 3
                    },
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                true);

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено",
                 new Dictionary<string, object>
                {
                    {"Не сообщать номер резерва по телефону", true}
                });

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.SetArticleState(0, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCancel();

            wf.CallCompleteTask("Cancel_Order_and_Return_Articles", "Верните артикулы", "Вернул");

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderRejected("Покупатель отказался");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void SocCardCallCancel_10()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 20000,
                        Qty = 3
                    },
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                true);

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено",
                 new Dictionary<string, object>
                {
                    {"Не сообщать номер резерва по телефону", true}
                });

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.SetArticleState(0, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Delivery_Approval", "Согласуйте дату доставки", "OK",
                new Dictionary<string, object>
                    {
                        {"Дата доставки", DateTime.Today}
                    });

            Thread.Sleep(TimeSpan.FromMinutes(1.1));

            wf.CallCancel();

            wf.CallCompleteTask("Cancel_Order_and_Return_Articles", "Верните артикулы", "Вернул");

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderRejected("Покупатель отказался");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void SocCardCallCancel_11()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 20000,
                        Qty = 3
                    },
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                false);

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено",
                 new Dictionary<string, object>
                {
                    {"Не сообщать номер резерва по телефону", false}
                });

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.SetArticleState(0, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "Не дозвонился");

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCancel();

            wf.CallCompleteTask("Cancel_Order_and_Return_Articles", "Верните артикулы", "Вернул");

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderRejected("Покупатель отказался");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void SocCardCallCancel_12()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 20000,
                        Qty = 3
                    },
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                false);

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено",
                 new Dictionary<string, object>
                {
                    {"Не сообщать номер резерва по телефону", false}
                });

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.SetArticleState(0, "Не найден");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Bad_Reserve_Approval", "Проверьте резерв", "Перезвонить позже",
                 new Dictionary<string, object>
                {
                    {"Перезвоните мне через", TimeSpan.FromMinutes(5)}
                });

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCancel();

            wf.CallCompleteTask("Cancel_Order_and_Return_Articles", "Верните артикулы", "Вернул");

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderRejected("Покупатель отказался");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void SocCardChangedReserve_01()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 20000,
                        Qty = 3
                    },
                    new ArticleData
                    {
                        ArticleNum = "1159035",
                        Price = 20000,
                        Qty = 3
                    }
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                false,
                "ИЗМЕНЕННЫЙ И ОТМЕНЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено",
                 new Dictionary<string, object>
                {
                    {"Не сообщать номер резерва по телефону", false}
                });

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.TransformReserve();
            wf.CancelReserve();
            
            wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "ОК");

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.CallCompleteTask("Return_Articles", "Верните артикулы", "Вернул");

            Thread.Sleep(TimeSpan.FromSeconds(17));
            wf.AssertOrderRejected("Резерв отменен в WWS сотрудником магазина");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void SocCardChangedReserve_02()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 20000,
                        Qty = 3
                    },
                    new ArticleData
                    {
                        ArticleNum = "1159035",
                        Price = 20000,
                        Qty = 3
                    }
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                false,
                "ИЗМЕНЕННЫЙ И ОПЛАЧЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено",
                 new Dictionary<string, object>
                {
                    {"Не сообщать номер резерва по телефону", false}
                });

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.TransformReserve();
            wf.PayReserve();

            wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "ОК");

            Thread.Sleep(TimeSpan.FromSeconds(37));

            wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Не участвует в выборке");

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderClosed();
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void SocCardChangedReserve_03()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 20000,
                        Qty = 3
                    },
                    new ArticleData
                    {
                        ArticleNum = "1159035",
                        Price = 20000,
                        Qty = 3
                    }
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                false,
                "ИЗМЕНЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено",
                 new Dictionary<string, object>
                {
                    {"Не сообщать номер резерва по телефону", false}
                });

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");

            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.TransformReserve();

            wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "ОК");

            Thread.Sleep(TimeSpan.FromMinutes(1));

            wf.CallCompleteTask("Check_Payment", "Проверьте получение заказа", "Покупатель получил заказ");

            wf.CallCompleteTask("Cancel_Order_In_Process_And_Return_Articles", "Верните артикулы", "OK");

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderRejected("Уже купил в ММ");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void SocCardChangedReserve_04()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 20000,
                        Qty = 3
                    },
                    new ArticleData
                    {
                        ArticleNum = "1159035",
                        Price = 20000,
                        Qty = 3
                    }
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                false,
                "ИЗМЕНЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено",
                 new Dictionary<string, object>
                {
                    {"Не сообщать номер резерва по телефону", false}
                });

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.TransformReserve();

            wf.CallCancel();

            wf.CallCompleteTask("Cancel_Order_and_Return_Articles", "Верните артикулы", "Вернул");

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderRejected("Покупатель отказался");
            wf.AssertTicketClosed();
        }

  

        // НЕ ДОЗВОНИЛИСЬ

        [TestMethod]
        public void SocCardCGT_01()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 20000,
                        Qty = 3
                    },
                    new ArticleData
                    {
                        ArticleNum = "2111111",
                        Price = 20000,
                        Qty = 3
                    }
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                true);

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Не дозвонился");
            Thread.Sleep(TimeSpan.FromMinutes(0.5));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Не дозвонился");
            Thread.Sleep(TimeSpan.FromMinutes(0.5));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Не дозвонился");
            Thread.Sleep(TimeSpan.FromMinutes(1));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Не дозвонился");

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderRejected("Невозможно связаться с покупателем");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void SocCardCGT_02()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 20000,
                        Qty = 3
                    },
                    new ArticleData
                    {
                        ArticleNum = "2111111",
                        Price = 20000,
                        Qty = 3
                    }
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                false);

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Не дозвонился");
            Thread.Sleep(TimeSpan.FromMinutes(0.5));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Не дозвонился");
            Thread.Sleep(TimeSpan.FromMinutes(0.5));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Не дозвонился");
            Thread.Sleep(TimeSpan.FromMinutes(1));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Не дозвонился");

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderRejected("Невозможно связаться с покупателем");
            wf.AssertTicketClosed();
        }
        
        [TestMethod]
        public void SocCardCGT_03()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 20000,
                        Qty = 3
                    },
                    new ArticleData
                    {
                        ArticleNum = "1159035",
                        Price = 2000,
                        Qty = 3
                    },
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                true);

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено",
                 new Dictionary<string, object>
                {
                    {"Не сообщать номер резерва по телефону", false}
                });

            Thread.Sleep(TimeSpan.FromSeconds(10));

            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "Не дозвонился");
            Thread.Sleep(TimeSpan.FromMinutes(0.5));

            wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "Не дозвонился");
            Thread.Sleep(TimeSpan.FromMinutes(0.5));

            wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "Не дозвонился");

            Thread.Sleep(TimeSpan.FromSeconds(20));
            wf.CallCompleteTask("Return_Articles", "Верните артикулы", "Вернул");

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderRejected("Невозможно связаться с покупателем");
            wf.AssertTicketClosed();
            
        }

        [TestMethod]
        public void SocCardCGT_04()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 20000,
                        Qty = 3
                    },
                    new ArticleData
                    {
                        ArticleNum = "1159035",
                        Price = 20000,
                        Qty = 3
                    }
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                false);

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено",
                 new Dictionary<string, object>
                {
                    {"Не сообщать номер резерва по телефону", false}
                });

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "ОК");

            Thread.Sleep(TimeSpan.FromMinutes(1));

            wf.CallCompleteTask("Check_Payment", "Проверьте оплату", "Не дозвонился");
            Thread.Sleep(TimeSpan.FromMinutes(0.5));

            wf.CallCompleteTask("Check_Payment", "Проверьте оплату", "Не дозвонился");
            Thread.Sleep(TimeSpan.FromMinutes(0.5));

            wf.CallCompleteTask("Check_Payment", "Проверьте оплату", "Не дозвонился");
            Thread.Sleep(TimeSpan.FromMinutes(0.5));

            wf.CallCompleteTask("Check_Payment", "Проверьте оплату", "Не дозвонился");
            Thread.Sleep(TimeSpan.FromMinutes(0.5));

            wf.CallCompleteTask("Check_Payment", "Проверьте оплату", "Не дозвонился");

            wf.CallCompleteTask("Cancel_Order_In_Process_And_Return_Articles", "Верните артикулы", "OK");

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderRejected("Истек срок резерва");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void SocCardCheckPayment_Paid()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 20000,
                        Qty = 3
                    },
                    new ArticleData
                    {
                        ArticleNum = "1159035",
                        Price = 20000,
                        Qty = 3
                    }
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                false);

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено",
                new Dictionary<string, object>
                {
                    {"Не сообщать номер резерва по телефону", false}
                });

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "ОК");

            Thread.Sleep(TimeSpan.FromMinutes(1));

            wf.PayReserve();
            wf.CallCompleteTask("Check_Payment", "Проверьте получение заказа", "Покупатель не получил заказ",
                 new Dictionary<string, object>
                {
                    {"Причина", "Нашел дешевле"}
                });

            wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Не участвует в выборке");

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderClosed();
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void SocCardCheckPayment_Cancelled()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 20000,
                        Qty = 3
                    },
                    new ArticleData
                    {
                        ArticleNum = "1159035",
                        Price = 20000,
                        Qty = 3
                    }
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                false);

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено",
                new Dictionary<string, object>
                {
                    {"Не сообщать номер резерва по телефону", false}
                });

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "ОК");

            Thread.Sleep(TimeSpan.FromMinutes(1));

            wf.CancelReserve();
            wf.CallCompleteTask("Check_Payment", "Проверьте получение заказа", "Покупатель получил заказ");

            wf.CallCompleteTask("Cancel_Order_In_Process_And_Return_Articles", "Верните артикулы", "OK");

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderRejected("Резерв отменен в WWS сотрудником магазина");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void SocCardCheckPayment_TransformedPaid()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 20000,
                        Qty = 3
                    },
                    new ArticleData
                    {
                        ArticleNum = "1159035",
                        Price = 20000,
                        Qty = 3
                    }
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                false);

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено",
                new Dictionary<string, object>
                {
                    {"Не сообщать номер резерва по телефону", false}
                });

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "ОК");

            Thread.Sleep(TimeSpan.FromMinutes(1));

            wf.CallCompleteTask("Check_Payment", "Проверьте получение заказа", "Не дозвонился");
            Thread.Sleep(TimeSpan.FromMinutes(0.5)); 
            
            wf.TransformReserve();
            wf.PayReserve();

            wf.CallCompleteTask("Check_Payment", "Проверьте получение заказа", "Покупатель не получил заказ",
                 new Dictionary<string, object>
                {
                    {"Причина", "Нашел дешевле"}
                });

            wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Не участвует в выборке");

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderClosed();
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void SocCardCheckPayment_TransformedCancelled()
        {
            var wf = new SocialCardOrderWfMm();
            wf.CreateWith(
                new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1111111",
                        Price = 20000,
                        Qty = 3
                    },
                    new ArticleData
                    {
                        ArticleNum = "1159035",
                        Price = 20000,
                        Qty = 3
                    }
                },
                OrderSource.WebSite,
                PaymentType.SocialCard,
                false);

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено",
                new Dictionary<string, object>
                {
                    {"Не сообщать номер резерва по телефону", false}
                });

            Thread.Sleep(TimeSpan.FromSeconds(7));

            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "ОК");

            Thread.Sleep(TimeSpan.FromMinutes(1));

            wf.CallCompleteTask("Check_Payment", "Проверьте получение заказа", "Не дозвонился");
            Thread.Sleep(TimeSpan.FromMinutes(0.5));

            wf.TransformReserve();
            wf.CancelReserve();
            wf.CallCompleteTask("Check_Payment", "Проверьте получение заказа", "Покупатель получил заказ");

            wf.CallCompleteTask("Cancel_Order_In_Process_And_Return_Articles", "Верните артикулы", "OK");

            Thread.Sleep(TimeSpan.FromSeconds(7));
            wf.AssertOrderRejected("Резерв отменен в WWS сотрудником магазина");
            wf.AssertTicketClosed();
        }
    }
}
