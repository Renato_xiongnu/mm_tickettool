﻿using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TT.IntegrationTester.MM;
using TT.IntegrationTester.MM.Proxy.OldOrders;
using PaymentType = TT.IntegrationTester.MM.Proxy.OldOrders.PaymentType;
using System.Diagnostics;
using TT.IntegrationTester.MM.Proxy.LipsMockAutoTestHelper;

namespace Orders.Backend.IntegrationTester
{
    [TestClass]
    public class SmartStartCash2MmTest
    {
        private string OrderId;

        [TestInitialize]
        public void Init()
        {
            OrderId = null;
        }

        [TestMethod]
        public void PickupNotTransferPaid()//Самовывоз из магазина в котором есть товар, резерв оплачен
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 20000,
                    Qty = 3
                },
                new ArticleData
                {
                    ArticleNum = "1000201",
                    Price = 20000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            false,
            "pickup_R201",
            "ОПЛАЧЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");

            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            //wf.PayReserve();

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderClosed();
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void PickupLateCanceled()//Самовывоз из магазина, не собрали вовремя, клиент отказался
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 20000,
                    Qty = 3
                },
                new ArticleData
                {
                    ArticleNum = "1000201",
                    Price = 20000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            false,
            "pickup_R201");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            //Thread.Sleep(TimeSpan.FromSeconds(75));
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Время истекло");

            wf.CallCompleteTask("Bad_Reserve_Approval", "Подтвердите заказ", "Отклонено покупателем",
                new Dictionary<string, object>
                {
                    {"Причина отказа", "Нашел дешевле"}
                });

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.AssertOrderRejected("Нашел дешевле");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void PickupLateChangeShopPushTransfer()//Самовывоз из магазина, не собрали вовремя, изменил магазин
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 20000,
                    Qty = 3
                },
                new ArticleData
                {
                    ArticleNum = "1201601",
                    Price = 20000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            false,
            "pickup_R201");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            //Thread.Sleep(TimeSpan.FromSeconds(75));
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Время истекло");

            wf.ChangePickupPoint("R202");
            //wf.RemoveArticle(1);
            //wf.AddArticle(1000601, 3);
            wf.CallCompleteTask("Bad_Reserve_Approval", "Подтвердите заказ", "ОК");

            wf.CallCompleteTask("Create_Push_Transfer", "Создайте PUSH трансфер", "Сделал трансфер", new Dictionary<string, object>
            {
                {"Магазин(SapCode)","R601"},
                {"Номер заказ-трансфера","12345678"},
                {"Номер выхода трансфера","12345667"}
            });

            //Thread.Sleep(TimeSpan.FromSeconds(10));
            wf.LipsSendTransferShippedProcessingInfoResponse();

            Thread.Sleep(TimeSpan.FromSeconds(5));
            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Confirm_Transfer", "Подтверди получение трансфера", "Готово");

            wf.PayReserve();

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderClosed();
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void PickupLateChangeDatePushTransfer()//Самовывоз из магазина, не собрали вовремя, изменил дату доставки
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 20000,
                    Qty = 3
                },
                new ArticleData
                {
                    ArticleNum = "1201601",
                    Price = 20000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            false,
            "pickup_R201",
            "ОПЛАЧЕННЫЙ РЕЗЕРВ"
            );

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            //Thread.Sleep(TimeSpan.FromSeconds(75));
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Время истекло");

            wf.ChangePickupPoint("R202");
            //wf.RemoveArticle(1);
            //wf.AddArticle(1000601, 3);
            wf.CallCompleteTask("Bad_Reserve_Approval", "Подтвердите заказ", "ОК");

            wf.CallCompleteTask("Create_Push_Transfer", "Создайте PUSH трансфер", "Невозможно создать push трансфер");

            wf.CallCompleteTask("Create_Transfer", "Сделай трансфер", "Сделал трансфер", new Dictionary<string, object>
            {
                {"Магазин(SapCode)", "R201"},
                {"Номер заказ-трансфера", "12345678"}
            });

            wf.SetArticleState(0, "Трансфер");
            wf.SetArticleState(1, "Трансфер");
            wf.CallCompleteTask("Collect_Transfer", "Собери трансфер", "Готово"); 
           
            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Confirm_Transfer", "Подтверди получение трансфера", "Готово");

            //wf.PayReserve();

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderClosed();
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void PickupDisplayItemCanceled()//Самовывоз из магазина, проблемные позиции, клиент отказался
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 20000,
                    Qty = 3
                },
                new ArticleData
                {
                    ArticleNum = "1000201",
                    Price = 20000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            false,
            "pickup_R201");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен витринный экземпляр");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Bad_Reserve_Approval", "Подтвердите заказ", "Отклонено покупателем",
                new Dictionary<string, object>
                {
                    {"Причина отказа", "Нашел дешевле"}
                });

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.AssertOrderRejected("Нашел дешевле");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void PickupDisplayItemChangeShopPushTransfer()//Самовывоз из магазина, проблемные позиции, изменил магазин
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 20000,
                    Qty = 3
                },
                new ArticleData
                {
                    ArticleNum = "1201601",
                    Price = 20000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            false,
            "pickup_R201");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен витринный экземпляр");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.ChangePickupPoint("R202");
            //wf.RemoveArticle(1);
            //wf.AddArticle(1000601, 3);
            wf.CallCompleteTask("Bad_Reserve_Approval", "Подтвердите заказ", "ОК");

            wf.CallCompleteTask("Create_Push_Transfer", "Создайте PUSH трансфер", "Сделал трансфер", new Dictionary<string, object>
            {
                {"Магазин(SapCode)","R201"},
                {"Номер заказ-трансфера","12345678"},
                {"Номер выхода трансфера","12345667"}
            });

            //Thread.Sleep(TimeSpan.FromSeconds(10));
            wf.LipsSendTransferShippedProcessingInfoResponse();

            Thread.Sleep(TimeSpan.FromSeconds(5));
            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Confirm_Transfer", "Подтверди получение трансфера", "Готово");

            wf.PayReserve();

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderClosed();
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void PickupDisplayItemChangeDatePushTransfer() //Самовывоз из магазина, проблемные позиции, изменил дату доставки
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 20000,
                    Qty = 3
                },
                new ArticleData
                {
                    ArticleNum = "1201601",
                    Price = 20000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            false,
            "pickup_R201",
            "ОПЛАЧЕННЫЙ РЕЗЕРВ"
            );

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен витринный экземпляр");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.ChangePickupPoint("R202");
            //wf.RemoveArticle(1);
            //wf.AddArticle(1000601, 3);
            wf.CallCompleteTask("Bad_Reserve_Approval", "Подтвердите заказ", "ОК");

            wf.CallCompleteTask("Create_Push_Transfer", "Создайте PUSH трансфер", "Невозможно создать push трансфер");

            wf.CallCompleteTask("Create_Transfer", "Сделай трансфер", "Сделал трансфер", new Dictionary<string, object>
            {
                {"Магазин(SapCode)", "R201"},
                {"Номер заказ-трансфера", "12345678"}
            });

            wf.SetArticleState(0, "Трансфер");
            wf.SetArticleState(1, "Трансфер");
            wf.CallCompleteTask("Collect_Transfer", "Собери трансфер", "Готово");

            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Confirm_Transfer", "Подтверди получение трансфера", "Готово");

            //wf.PayReserve();

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderClosed();
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void PickupWWSErrorCanceled() //Самовывоз из магазина, в наличии, ошибка создания резерва, отмена
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 20000,
                    Qty = 2
                },
                new ArticleData
                {
                    ArticleNum = "3000000",  // notsaleable
                    Price = 20000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            false,
            "pickup_R201"
            );

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            //wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

            wf.CallCompleteTask("Review_Reserve", "Проверьте заказ", "Ошибка WWS");

            wf.CallCompleteTask("Unable_to_complete_order", "Невозможность комплектации", "OK");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderRejected("Ошибка WWS");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void PickupInStockNotPaid()//Самовывоз из магазина в котором есть товар, резерв не оплачен (надо настоить)
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 20000,
                    Qty = 3
                },
                new ArticleData
                {
                    ArticleNum = "1000201",
                    Price = 20000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            false,
            "pickup_R201"
            );

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");

            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            //TODO продлить резерв
            //wf.ProlongReserve();
            Thread.Sleep(TimeSpan.FromMinutes(4));
            wf.CallCompleteTask("Auto_Order_Expiration_Check", "Напоминание про срок резерва", "Продлить резерв");

            Thread.Sleep(TimeSpan.FromSeconds(60));
            wf.CallCompleteTask("Return_Articles", "Верните артикулы", "Вернул");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderRejected("Истек резерв");
            wf.AssertTicketClosed();
        }


        [TestMethod]
        public void FirstCallCancel()//первый звонок, клиент отказался
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1111111",
                    Price = 20000,
                    Qty = 3
                },
                new ArticleData
                {
                    ArticleNum = "1159035",
                    Price = 20000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            true
            );

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Отклонено покупателем",
                new Dictionary<string, object>
                {
                    {"Причина отказа", "Нашел дешевле"}
                });

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderRejected("Нашел дешевле");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void FirstCallNoAnswer()//Первый звонок, не дозвонились, клиент не перезвонил
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData> 
            {
                new ArticleData
                {
                    ArticleNum = "1111111",
                    Price = 20000,
                    Qty = 3
                },
                new ArticleData
                {
                    ArticleNum = "1159035",
                    Price = 20000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            true
            );

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Не дозвонился");

            Thread.Sleep(TimeSpan.FromSeconds(30));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Не дозвонился");

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.AssertOrderRejected("Невозможно связаться с покупателем");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void FirstCallCallBackPaid()//Первый звонок, недозвонились до клиента, клиент перезвонил, резерв оплачен 		
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData> 
            {
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 10000,
                    Qty = 3
                }, 
                new ArticleData
                {
                    ArticleNum = "1000202",
                    Price = 10000,
                    Qty = 3
                }

            },
            OrderSource.WebSite,
            PaymentType.Cash,
            true,
            null,
            "ОПЛАЧЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Не дозвонился");
            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.ApproveOrder();

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Delivery_Approval", "Согласуйте дату доставки", "OK",
                new Dictionary<string, object>
                {
                    {"Дата доставки", DateTime.UtcNow.AddMinutes(1)}
                });

           // wf.LipsSendDeliverResponse(true);
            
            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.AssertOrderClosed();
            wf.AssertTicketClosed();

        }

        [TestMethod]
        public void FirstCallRecallPaid()//Первый звонок, перезвонить, оплатил резерв
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData> 
            {
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 10000,
                    Qty = 3
                }, 
                new ArticleData
                {
                    ArticleNum = "1000202",
                    Price = 10000,
                    Qty = 3
                }

            },
            OrderSource.WebSite,
            PaymentType.Cash,
            true,
            null,
            "ОПЛАЧЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Перезвонить позже", new Dictionary<string, object> 
            {
                {"Перезвоните мне через", TimeSpan.FromMinutes(1)}
            });
            Thread.Sleep(TimeSpan.FromMinutes(1));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Delivery_Approval", "Согласуйте дату доставки", "OK",
                new Dictionary<string, object>
                {
                    {"Дата доставки", DateTime.UtcNow.AddMinutes(1)}
                });

            Thread.Sleep(TimeSpan.FromSeconds(10));

            wf.AssertOrderClosed();
            wf.AssertTicketClosed();

        }

        [TestMethod]
        public void PickupPushTransferPaid()// Самовывоз создание Push трансфера Резерв оплачен
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 10000,
                    Qty = 3
                },

                new ArticleData
                {
                    ArticleNum = "1000601",
                    Price = 10000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            false,
            "pickup_R201",
            "ОПЛАЧЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(5));

            wf.CallCompleteTask("Create_Push_Transfer", "Создайте PUSH трансфер", "Сделал трансфер", new Dictionary<string, object>
            {
                {"Магазин(SapCode)","R601"},
                {"Номер заказ-трансфера","12345678"},
                {"Номер выхода трансфера","12345667"}
            });

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.LipsSendTransferShippedProcessingInfoResponse();

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Confirm_Transfer", "Подтверди получение трансфера", "Готово");

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.AssertOrderClosed();
            wf.AssertTicketClosed();

        }

        [TestMethod]
        public void PickupPushTransferChangeDatePaid()//Самовывоз, ошибка LIPS, изменить дату, создать Push Трансфер, оплачено
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1000601",
                        Price = 10000,
                        Qty = 3
                    },

                    new ArticleData
                    {
                        ArticleNum = "1000000",
                        Price = 10000,
                        Qty = 3
                    }
                },
            OrderSource.WebSite,
            PaymentType.Cash,
            false,
            "pickup_R201",
            "ОПЛАЧЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.CallCompleteTask("Create_Push_Transfer", "Создайте PUSH трансфер", "Сделал трансфер", new Dictionary<string, object>
            {
                {"Магазин(SapCode)","R601"},
                {"Номер заказ-трансфера","12345678"},
                {"Номер выхода трансфера","12345667"}
            });

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.LipsSendTransferResponse(false);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.CallCompleteTask("Transfer_Order_Incidents_Approval", "Внеси измненения в заказ (PUSH трансфер)", "ОК",
                new Dictionary<string, object>
                {
                    {"Дата доставки", DateTime.UtcNow.AddMinutes(1)}
                });

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.CallCompleteTask("Create_Push_Transfer", "Создайте PUSH трансфер", "Сделал трансфер", new Dictionary<string, object>
            {
                {"Магазин(SapCode)","R601"},
                {"Номер заказ-трансфера","12345678"},
                {"Номер выхода трансфера","12345667"}
            });

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.LipsSendTransferShippedProcessingInfoResponse();

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Confirm_Transfer", "Подтверди получение трансфера", "Готово");

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.AssertOrderClosed();
            wf.AssertTicketClosed();

        }

        [TestMethod]
        public void PickupPushTransferRejected()//Самовывоз, ошибка липс, выполнение невозможно
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
                {
                    new ArticleData
                    {
                        ArticleNum = "1000601",
                        Price = 10000,
                        Qty = 3
                    },
                    new ArticleData
                    {
                        ArticleNum = "1000000",
                        Price = 10000,
                        Qty = 3
                    }
                },
            OrderSource.WebSite,
            PaymentType.Cash,
            false,
            "pickup_R202",
            "ОПЛАЧЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.CallCompleteTask("Create_Push_Transfer", "Создайте PUSH трансфер", "Сделал трансфер", new Dictionary<string, object>
            {
                {"Магазин(SapCode)","R601"},
                {"Номер заказ-трансфера","12345678"},
                {"Номер выхода трансфера","12345667"}
            });

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.LipsSendTransferShippedProcessingInfoResponse("Received",
                new[] {new MMS.Cloud.Commands.FORD.Incident{Code = 10080, Description = "sdfhsdfhdfg"}
                });

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.CallCompleteTask("Transfer_Order_Incidents_Approval", "Внеси измненения в заказ (PUSH трансфер)", "Выполнение невозможно");

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.AssertOrderRejected("");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void PickupPushTransferChangeArticlePaid()//Самовывоз, ошибка LIPS, изменили артикульный состав, оплачен
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData> 
            {
                new ArticleData
                {
                    ArticleNum = "1000601",
                    Price = 10000,
                    Qty =3
                },

                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 10000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            false,
            "pickup_R202",
            "ОПЛАЧЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.CallCompleteTask("Create_Push_Transfer", "Создайте PUSH трансфер", "Сделал трансфер", new Dictionary<string, object>
            {
                {"Магазин(SapCode)","R601"},
                {"Номер заказ-трансфера","12345678"},
                {"Номер выхода трансфера","12345667"}
            });

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.LipsSendTransferShippedProcessingInfoResponse("Received",
                new[] {new MMS.Cloud.Commands.FORD.Incident{Code = 10080, Description = "sdfhsdfhdfg"}
                });

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.RemoveArticle(1000601);
            wf.CallCompleteTask("Transfer_Order_Incidents_Approval", "Внеси измненения в заказ (PUSH трансфер)", "ОК");

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.SetArticleState(0, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.AssertOrderClosed();
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void PickupPushTransferNotDelivery()//Самовывоз, Push трансфер не прибыл
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData> 
            {
                new ArticleData
                {
                    ArticleNum = "1000601",
                    Price = 10000,
                    Qty = 3
                },
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 10000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            false,
            "pickup_R201",
            "ОПЛАЧЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.CallCompleteTask("Create_Push_Transfer", "Создайте PUSH трансфер", "Сделал трансфер", new Dictionary<string, object>
            {
                {"Магазин(SapCode)","R601"},
                {"Номер заказ-трансфера","12345678"},
                {"Номер выхода трансфера","12345667"}
            });

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.LipsSendTransferShippedProcessingInfoResponse();

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Confirm_Transfer", "Подтверди получение трансфера", "Отменить заказ");

            wf.CallCompleteTask("Unable_to_complete_order", "Невозможность комплектации", "OK");

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.AssertOrderRejected("Трансфер не удался");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void PickupPushTransferNotPaid()//Самовывоз из магазина, создать Push трансфер, Резерв не оплачен		
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 10000,
                    Qty = 3
                },

                new ArticleData
                {
                    ArticleNum = "1000601",
                    Price = 10000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            false,
            "pickup_R201"
            );

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(5));

            wf.CallCompleteTask("Create_Push_Transfer", "Создайте PUSH трансфер", "Сделал трансфер", new Dictionary<string, object>
            {
                {"Магазин(SapCode)","R601"},
                {"Номер заказ-трансфера","12345678"},
                {"Номер выхода трансфера","12345667"}
            });

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.LipsSendTransferShippedProcessingInfoResponse();

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Confirm_Transfer", "Подтверди получение трансфера", "Готово");

            wf.CallCompleteTask("Auto_Order_Expiration_Check", "Напоминание про срок резерва", "Нет",
                new Dictionary<string, object>
                {
                    {"Причина", "Нашел дешевле"}
                });
            wf.CallCompleteTask("Cancel_Order_In_Process_And_Return_Articles", "Верните артикулы", "OK");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderRejected("Нашел дешевле");
            wf.AssertTicketClosed();
        }
       
        [TestMethod]
        public void PickupTransferPaid()//Самовывоз, создание трансфера, резерв оплачен
        {
            var random = new Random((int)DateTime.Now.Ticks % 100000);
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData> 
             {
                 new ArticleData
                 {
                     ArticleNum = "1000000",
                     Price = 10000,
                     Qty = 3
                 },
                 new ArticleData
                 {
                     ArticleNum = "1000202",
                     Price = 3,
                     Qty = 3
                 }
             },
             OrderSource.WebSite,
             PaymentType.Cash,
             false,
             "pickup_R201",
             "ОПЛАЧЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.CallCompleteTask("Create_Transfer", "Сделал трансфер", new Dictionary<string, object>
            {
                {"Магазин(SapCode)", "R202"},
                {"Номер заказ-трансфера", random.Next(0,int.MaxValue).ToString()}
            });

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Трансфер");
            wf.SetArticleState(1, "Трансфер");
            wf.CallCompleteTask("Collect_Transfer", "Собери трансфер", "Готово");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Confirm_Transfer", "Подтверди получение трансфера", "Готово");

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.AssertOrderClosed();
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void PickupPushTransferTransferPaid()//Самовывоз, Push трансфер невозможен, создать трансфер, оплаченный резерв
        {
            var wf = new SmartStartCashOrder2WfMM();
            var random = new Random((int)DateTime.Now.Ticks % 100000);
            wf.CreateWith(
            new List<ArticleData> 
             {
                new ArticleData
                {
                    ArticleNum = "1201601",
                    Price = 10000,
                    Qty = 3
                }
             },
             OrderSource.WebSite,
             PaymentType.Cash,
             false,
             "pickup_R202",
             "ОПЛАЧЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.CallCompleteTask("Create_Push_Transfer", "Создайте PUSH трансфер", "Невозможно создать push трансфер");//дописать исход Невозможности создать трансфер

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.CallCompleteTask("Create_Transfer", "Сделал трансфер", new Dictionary<string, object>
            {
                {"Магазин(SapCode)", "R201"},
                {"Номер заказ-трансфера", random.Next(0,int.MaxValue).ToString()}
            });

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Трансфер");
            wf.CallCompleteTask("Collect_Transfer", "Собери трансфер", "Готово");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Отложен");
            wf.CallCompleteTask("Confirm_Transfer", "Подтверди получение трансфера", "Готово");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderClosed();
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void PickupTransferTimeoutPaid()//Самовывоз, неуспели вовремя собрать трансфер, резерв выкуплен
        {
            var wf = new SmartStartCashOrder2WfMM();
            var random = new Random((int)DateTime.Now.Ticks % 100000);
            wf.CreateWith(
            new List<ArticleData> 
             {
                 new ArticleData
                 {
                     ArticleNum = "1000000",
                     Price = 10000,
                     Qty = 3
                 },
                 new ArticleData
                 {
                     ArticleNum = "1000206",
                     Price = 10000,
                     Qty = 3
                 }
             },
             OrderSource.WebSite,
             PaymentType.Cash,
             false,
             "pickup_R202",
             "ОПЛАЧЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.CallCompleteTask("Create_Transfer", "Сделал трансфер", new Dictionary<string, object>
            {
                {"Магазин(SapCode)", "R206"},
                {"Номер заказ-трансфера", random.Next(0,int.MaxValue).ToString()}
            });

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.CallCompleteTask("Collect_Transfer", "Собери трансфер", "Время истекло");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.CallCompleteTask("Create_Transfer", "Сделал трансфер", new Dictionary<string, object>
            {
                {"Магазин(SapCode)", "R206"},
                {"Номер заказ-трансфера", random.Next(0,int.MaxValue).ToString()}
            });

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Трансфер");
            wf.SetArticleState(1, "Трансфер");
            wf.CallCompleteTask("Collect_Transfer", "Собери трансфер", "Готово");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Confirm_Transfer", "Подтверди получение трансфера", "Готово");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderClosed();
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void PickupTransferProblemArticlePaid()// Самовывоз, Трансфер есть проблемные позиции, резерв оплачен
        {
            var wf = new SmartStartCashOrder2WfMM();
            var random = new Random((int)DateTime.Now.Ticks % 100000);
            wf.CreateWith(
            new List<ArticleData> 
             {
                 new ArticleData
                 {
                     ArticleNum = "1000000",
                     Price = 10000,
                     Qty = 3
                 },
                 new ArticleData
                 {
                     ArticleNum = "1000206",
                     Price  = 10000,
                     Qty = 3
                 }
             },
             OrderSource.WebSite,
             PaymentType.Cash,
             false,
             "pickup_R202",
             "ОПЛАЧЕННЫЙ РЕЗЕРВ"
                );
            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.CallCompleteTask("Create_Transfer", "Сделал трансфер", new Dictionary<string, object>
            {
                {"Магазин(SapCode)", "R206"},
                {"Номер заказ-трансфера", random.Next(0,int.MaxValue).ToString()}
            });

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Не найден");
            wf.SetArticleState(1, "Трансфер");
            wf.CallCompleteTask("Collect_Transfer", "Собери трансфер", "Готово");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.CallCompleteTask("Create_Transfer", "Сделай трансфер", "Сделал трансфер", new Dictionary<string, object>
            {
                {"Магазин(SapCode)", "R206"},
                {"Номер заказ-трансфера", random.Next(0,int.MaxValue).ToString()}
            });

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Трансфер");
            wf.SetArticleState(1, "Не найден");
            wf.CallCompleteTask("Collect_Transfer", "Собери трансфер", "Готово");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.CallCompleteTask("Create_Transfer", "Сделай трансфер", "Сделал трансфер", new Dictionary<string, object>
            {
                {"Магазин(SapCode)", "R206"},
                {"Номер заказ-трансфера", random.Next(0,int.MaxValue).ToString()}
            });

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Трансфер");
            wf.SetArticleState(1, "Трансфер");
            wf.CallCompleteTask("Collect_Transfer", "Собери трансфер", "Готово");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Confirm_Transfer", "Подтверди получение трансфера", "Готово");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderClosed();
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void PickupTransferNotDelivery()//Самовывоз Трансфер не прибыл
        {
            var wf = new SmartStartCashOrder2WfMM();
            var random = new Random((int)DateTime.Now.Ticks % 100000);
            wf.CreateWith(
            new List<ArticleData> 
             {
                 new ArticleData
                 {
                     ArticleNum = "1000000",
                     Price = 10000,
                     Qty = 3
                 },
                 new ArticleData
                 {
                     ArticleNum = "1000201",
                     Price = 10000,
                     Qty = 3
                 }
             },
             OrderSource.WebSite,
             PaymentType.Cash,
             false,
             "pickup_R202",
             "ОПЛАЧЕННЫЙ РЕЗЕРВ"
                );
            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.CallCompleteTask("Create_Transfer", "Сделал трансфер", new Dictionary<string, object>
            {
                {"Магазин(SapCode)", "R206"},
                {"Номер заказ-трансфера", random.Next(0,int.MaxValue).ToString()}
            });

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Трансфер");
            wf.SetArticleState(1, "Трансфер");
            wf.CallCompleteTask("Collect_Transfer", "Собери трансфер", "Готово");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Confirm_Transfer", "Подтверди получение трансфера", "Отменить заказ");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.CallCompleteTask("Unable_to_complete_order", "Невозможность комплектации", "OK");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderRejected("Трансфер не удался");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void PickupImpossibleCreateTransfer()//Самовывоз, невозможно создать трансфер
        {
            var wf = new SmartStartCashOrder2WfMM();
            var random = new Random((int)DateTime.Now.Ticks % 100000);
            wf.CreateWith(
            new List<ArticleData> 
             {
                 new ArticleData
                 {
                     ArticleNum = "1000000",
                     Price = 10000,
                     Qty = 3
                 },
                 new ArticleData
                 {
                     ArticleNum = "1201601",
                     Price = 10000,
                     Qty = 3
                 }
             },
             OrderSource.WebSite,
             PaymentType.Cash,
             false,
             "pickup_R202",
             "ОПЛАЧЕННЫЙ РЕЗЕРВ"
                );
            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.CallCompleteTask("Create_Push_Transfer", "Невозможно создать push трансфер");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.CallCompleteTask("Create_Transfer", "Сделал трансфер", new Dictionary<string, object>
            {
                {"Магазин(SapCode)", "R201"},
                {"Номер заказ-трансфера", random.Next(0,int.MaxValue).ToString()}
            });

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Не найден");
            wf.SetArticleState(1, "Трансфер");
            wf.CallCompleteTask("Collect_Transfer", "Собери трансфер", "Готово");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.CallCompleteTask("Create_Transfer", "Сделал трансфер", new Dictionary<string, object>
            {
                {"Магазин(SapCode)", "R201"},
                {"Номер заказ-трансфера", random.Next(0,int.MaxValue).ToString()}
            });

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Трансфер");
            wf.SetArticleState(1, "Витрина");
            wf.CallCompleteTask("Collect_Transfer", "Собери трансфер", "Готово");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.CallCompleteTask("Create_Transfer", "Отменить заказ", new Dictionary<string, object>
                {
                    {"Причина", "Невозможно сделать трансфер"}
                });

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderRejected("Невозможно сделать трансфер");
            wf.AssertTicketClosed();
        }


        [TestMethod]
        public void Delivery601Paid()//Стандартная доставка из 601, оплачен
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 20000,
                    Qty = 3
                },
                new ArticleData
                {
                    ArticleNum = "1000601",
                    Price = 20000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            true,
            null,
            "ОПЛАЧЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

            Thread.Sleep(TimeSpan.FromSeconds(4)); 
            wf.LipsSendDeliverResponse(true);

            //wf.PayReserve();

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderClosed();
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void DeliveryNot601Paid()//Стандартная доставка, нет в 601, оплачен
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1111111",
                    Price = 20000,
                    Qty = 1
                },
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 20000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            true,
            null,
            "ОПЛАЧЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Delivery_Approval", "Согласуйте дату доставки", "OK",
                new Dictionary<string, object>
                {
                    {"Дата доставки", DateTime.UtcNow.AddMinutes(1)}
                });

            //wf.PayReserve();

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderClosed();
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void Delivery601ChangeDeliveryDatePaid() //Стандартная доставка из 601, изменили дату доставки, оплачен
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1111111",
                    Price = 20000,
                    Qty = 3
                },
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 20000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            true,
            null,
            "ОПЛАЧЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.CallCompleteTask("Approve_Order", "Подтверждено");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.LipsSendDeliverResponse(false);

            wf.CallCompleteTask("Deliver_Order_Incidents_Approval", "ОК");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.LipsSendDeliverResponse();

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.AssertOrderClosed();
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void Delivery601ExecImpossible() //Стандартная доставка из 601, Выполнение невозможно
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1111111",
                    Price = 20000,
                    Qty = 3
                },
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 20000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            true,
            null,
            "ОПЛАЧЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.CallCompleteTask("Approve_Order", "Подтверждено");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.LipsSendDeliverResponse(false);

            wf.CallCompleteTask("Deliver_Order_Incidents_Approval", "Выполнение невозможно");

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.AssertOrderRejected("Выполнение невозможно");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void Delivery601NotPaid() //Стандартная доставка из 601, не оплачен
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1111111",
                    Price = 20000,
                    Qty = 3
                },
                new ArticleData
                {
                    ArticleNum = "1000601",
                    Price = 20000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            true
            );

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.LipsSendDeliverResponse(true);

            Thread.Sleep(TimeSpan.FromMinutes(16));

            wf.CallCompleteTask("Check_Payment", "Проверьте получение заказа", "Покупатель не получил заказ",
                new Dictionary<string, object>
                {
                    {"Причина", "Нашел дешевле"}
                });

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderRejected("Нашел дешевле");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void DeliveryNot601NotComplected()//Стандартная доставка, нет в 601, ошибка создания резерва
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "3000000",
                    Price = 20000,
                    Qty = 9999
                },
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 20000,
                    Qty = 9999999
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            true
            );

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

            wf.CallCompleteTask("Unable_to_complete_order", "Невозможность комплектации", "OK");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderRejected("Нет в наличии");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void DeliveryNot601LatePaid()
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1111111",
                    Price = 20000,
                    Qty = 1
                },
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 20000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            true,
            null,
            "ОПЛАЧЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4)); 
            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Время истекло");

            Thread.Sleep(TimeSpan.FromSeconds(4)); 
            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Delivery_Approval", "Согласуйте дату доставки", "OK",
                new Dictionary<string, object>
                {
                    {"Дата доставки", DateTime.UtcNow.AddMinutes(1)}
                });

            //wf.PayReserve();

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderClosed();
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void DeliveryNot601ProblemPositionPaid()
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 20000,
                    Qty = 2
                },

            },
            OrderSource.WebSite,
            PaymentType.Cash,
            true,
            null,
            "ОПЛАЧЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

            Thread.Sleep(TimeSpan.FromSeconds(4)); 
            wf.SetArticleState(0, "Не найден");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

//            wf.CallCompleteTask("Bad_Reserve_Approval", "Подтвердите заказ", "ОК");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Delivery_Approval", "Согласуйте дату доставки", "OK",
                new Dictionary<string, object>
                {
                    {"Дата доставки", DateTime.UtcNow.AddMinutes(1)}
                });

            //wf.PayReserve();

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderClosed();
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void DeliveryNot601CancelOnDeliveryApproval()
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1111111",
                    Price = 20000,
                    Qty = 1
                },
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 20000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            true,
            null,
            "ОПЛАЧЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Delivery_Approval", "Согласуйте дату доставки", "Отклонено покупателем",
                new Dictionary<string, object>
                {
                    {"Причина отказа", "Нашел дешевле"}
                });


            wf.CallCompleteTask("Return_Articles", "Верните артикулы", "Вернул");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderRejected("Нашел дешевле");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void DeliveryNot601NotPaid()
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1111111",
                    Price = 20000,
                    Qty = 1
                },
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 20000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            true,
            null,
            "НЕОПЛАЧЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Delivery_Approval", "Согласуйте дату доставки", "OK",
                new Dictionary<string, object>
                {
                    {"Дата доставки", DateTime.UtcNow.AddMinutes(1)}
                });

            //wf.PayReserve();

            Thread.Sleep(TimeSpan.FromMinutes(15));

            wf.CallCompleteTask("Check_Payment", "Проверьте получение заказа", "Покупатель не получил заказ",
                new Dictionary<string, object> 
                {
                    {"Причина", "Уже купил в ММ"}
                });

            wf.CallCompleteTask("Cancel_Order_In_Process_And_Return_Articles", "Верните артикулы", "OK");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderRejected("Уже купил в ММ");
            wf.AssertTicketClosed();
        }


        [TestMethod]
        public void DeliveryNoStore()//Доставка, невозможно выбрать магазин
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData> 
             {
                 new ArticleData
                 {
                     ArticleNum = "2000000",
                     Price = 10000,
                     Qty = 3
                 },
                 new ArticleData
                 {
                     ArticleNum = "1000000",
                     Price = 10000,
                     Qty = 3
                 }
             },
             OrderSource.WebSite,
             PaymentType.Cash,
             true,
             null
                );

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.CallCompleteTask("Unable_to_complete_order", "Невозможность комплектации", "OK");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderRejected("Нет в наличии");
            wf.AssertTicketClosed();

        }

        
        [TestMethod]
        public void PickupExt601Paid()//Доставка ext.PuP, товар в R601, резерв оплачен.
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 20000,
                    Qty = 3
                },
                new ArticleData
                {
                    ArticleNum = "1201601",
                    Price = 20000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            false,
            "PuP-001414",
            "ОПЛАЧЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.LipsSendDeliverResponse(true);

            //wf.PayReserve();

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderClosed();
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void PickupExtNot601() //Самовывоз из магазина, в наличии, ошибка создания резерва, отмена
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "3000000",
                    Price = 20000,
                    Qty = 2
                },
                new ArticleData
                {
                    ArticleNum = "1000201",
                    Price = 20000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            false,
            "PuP-001414"
            );

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            //wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

            wf.CallCompleteTask("Review_Reserve", "Проверьте заказ", "Ошибка WWS");

            wf.CallCompleteTask("Unable_to_complete_order", "Невозможность комплектации", "OK");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderRejected("Ошибка WWS");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void PickupExt601ChangeDeliveryDatePaid() //Стандартная доставка из 601, изменили дату доставки, оплачен
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1111111",
                    Price = 20000,
                    Qty = 3
                },
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 20000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            false,
            "PuP-001414",
            "ОПЛАЧЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.LipsSendDeliverResponse(false);

            wf.CallCompleteTask("Deliver_Order_Incidents_Approval", "ОК");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.LipsSendDeliverResponse();

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.AssertOrderClosed();
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void PickupExt601ExecImpossible() //Стандартная доставка из 601, Выполнение невозможно
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1111111",
                    Price = 20000,
                    Qty = 3
                },
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 20000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            false,
            "PuP-001414",
            "ОПЛАЧЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.LipsSendDeliverResponse(false);

            wf.CallCompleteTask("Deliver_Order_Incidents_Approval", "Выполнение невозможно");

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.AssertOrderRejected("Выполнение невозможно");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void PickupExt601NotPaid() //Стандартная доставка из 601, не оплачен
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1111111",
                    Price = 20000,
                    Qty = 3
                },
                new ArticleData
                {
                    ArticleNum = "1159035",
                    Price = 20000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            false,
            "PuP-001414"
            );

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.LipsSendDeliverResponse(true);

            Thread.Sleep(TimeSpan.FromMinutes(1));

            wf.CallCompleteTask("Check_Payment", "Проверьте получение заказа", "Покупатель не получил заказ",
                new Dictionary<string, object>
                {
                    {"Причина", "Нашел дешевле"}
                });

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderRejected("Нашел дешевле");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void PickupExtNot601NotComplected()//Стандартная доставка, нет в 601, ошибка создания резерва
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "3000000",
                    Price = 20000,
                    Qty = 9999
                },
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 20000,
                    Qty = 9999999
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            false,
            "PuP-001414"
            );

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.CallCompleteTask("Review_Reserve", "Проверьте заказ", "Ошибка WWS");

            wf.CallCompleteTask("Unable_to_complete_order", "Невозможность комплектации", "OK");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderRejected("Ошибка WWS");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void PickupExtNoStore()//Самовывоз из внешней точки, магазин не найден
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData> 
             {
                 new ArticleData
                 {
                     ArticleNum = "2000000",
                     Price = 10000,
                     Qty = 3
                 },
                 new ArticleData
                 {
                     ArticleNum = "1000000",
                     Price = 10000,
                     Qty = 3
                 }
             },
             OrderSource.WebSite,
             PaymentType.Cash,
             false,
             "PuP-001414"
                );

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.CallCompleteTask("Unable_to_complete_order", "Невозможность комплектации", "OK");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderRejected("Нет в наличии");
            wf.AssertTicketClosed();

        }

        [TestMethod]
        public void PickupExtNot601LatePaid()
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1111111",
                    Price = 20000,
                    Qty = 1
                },
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 20000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            false,
            "PuP-001414",
            "ОПЛАЧЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Время истекло");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Delivery_Approval", "Согласуйте дату доставки", "OK",
                new Dictionary<string, object>
                {
                    {"Дата доставки", DateTime.UtcNow.AddMinutes(1)}
                });

            //wf.PayReserve();

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderClosed();
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void PickupExtNot601ProblemPositionPaid()
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 20000,
                    Qty = 2
                },

            },
            OrderSource.WebSite,
            PaymentType.Cash,
            false,
            "PuP-001414",
            "ОПЛАЧЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));

            wf.SetArticleState(0, "Не найден");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            //            wf.CallCompleteTask("Bad_Reserve_Approval", "Подтвердите заказ", "ОК");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Delivery_Approval", "Согласуйте дату доставки", "OK",
                new Dictionary<string, object>
                {
                    {"Дата доставки", DateTime.UtcNow.AddMinutes(1)}
                });

            //wf.PayReserve();

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderClosed();
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void PickupExtNot601CancelOnDeliveryApproval()//Доставка ext.PuP, товар не в R601, при подтверждении даты доставки Отказ
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 20000,
                    Qty = 1
                },
                new ArticleData
                {
                    ArticleNum = "1000201",
                    Price = 20000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            false,
            "PuP-001414",
            "ОПЛАЧЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Delivery_Approval", "Согласуйте дату доставки", "Отклонено покупателем",
                new Dictionary<string, object>
                {
                    {"Причина отказа", "Нашел дешевле"}
                });


            wf.CallCompleteTask("Return_Articles", "Верните артикулы", "Вернул");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderRejected("Нашел дешевле");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void PickupExtNot601NotPaid()
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData>
            {
                new ArticleData
                {
                    ArticleNum = "1111111",
                    Price = 20000,
                    Qty = 1
                },
                new ArticleData
                {
                    ArticleNum = "1000000",
                    Price = 20000,
                    Qty = 3
                }
            },
            OrderSource.WebSite,
            PaymentType.Cash,
            false,
            "PuP-001414",
            "НЕОПЛАЧЕННЫЙ РЕЗЕРВ");

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Delivery_Approval", "Согласуйте дату доставки", "OK",
                new Dictionary<string, object>
                {
                    {"Дата доставки", DateTime.UtcNow.AddMinutes(1)}
                });

            //wf.PayReserve();

            wf.CallCompleteTask("Check_Payment", "Проверьте получение заказа", "Покупатель получил заказ");

            wf.CallCompleteTask("Cancel_Order_In_Process_And_Return_Articles", "Верните артикулы", "OK");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderRejected("Уже купил в ММ");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void CancelOrderInOrderMonitor()// Отмена заказа через ОМ
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData> 
             {
                 new ArticleData
                 {
                     ArticleNum = "1000000",
                     Price = 10000,
                     Qty = 3
                 },
                 new ArticleData
                 {
                     ArticleNum = "1000601",
                     Price = 10000,
                     Qty = 3
                 }
             },
             OrderSource.WebSite,
             PaymentType.Cash,
             true,
             null,
             "ОПЛАЧЕННЫЙ РЕЗЕРВ"
                );

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.CallCancel();

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderRejected("Покупатель отказался");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void PickupChangeReservePaid()// самовывоз, Изменить номер резерва на оплаченный
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData> 
             {
                 new ArticleData
                 {
                     ArticleNum = "1000000",
                     Price = 10000,
                     Qty = 3
                 },
                 new ArticleData
                 {
                     ArticleNum = "1000201",
                 }
             },
             OrderSource.WebSite,
             PaymentType.Cash,
             false,
             "pickup_R201",
             "ИЗМЕНЕННЫЙ И ОПЛАЧЕННЫЙ РЕЗЕРВ"
                );

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderClosed();
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void DeliveryChangeReservePaid()// Доставка, Изменить номер резерва на оплаченный
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData> 
             {
                 new ArticleData
                 {
                     ArticleNum = "1000000",
                     Price = 10000,
                     Qty = 3
                 },
                 new ArticleData
                 {
                     ArticleNum = "1000201",
                 }
             },
             OrderSource.WebSite,
             PaymentType.Cash,
             true,
             null,
             "ИЗМЕНЕННЫЙ И ОПЛАЧЕННЫЙ РЕЗЕРВ"
                );

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Delivery_Approval", "Согласуйте дату доставки", "OK",
    new Dictionary<string, object>
                {
                    {"Дата доставки", DateTime.UtcNow.AddMinutes(1)}
                });

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderClosed();
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void PickupChangeReserveCancel()// самовывоз, Изменить номер резерва на отмененный
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData> 
             {
                 new ArticleData
                 {
                     ArticleNum = "1000000",
                     Price = 10000,
                     Qty = 3
                 },
                 new ArticleData
                 {
                     ArticleNum = "1000201",
                 }
             },
             OrderSource.WebSite,
             PaymentType.Cash,
             false,
             "pickup_R201",
             "ИЗМЕНЕННЫЙ И ОТМЕНЕННЫЙ РЕЗЕРВ"
                );

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Return_Articles", "Верните отложенный товар в зал продаж", "Вернул");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderRejected("Резерв отменен в WWS сотрудником магазина");
            wf.AssertTicketClosed();
        }

        [TestMethod]
        public void DeliveryChangeReserveCancel()// Доставка, Изменить номер резерва на отмененный
        {
            var wf = new SmartStartCashOrder2WfMM();
            wf.CreateWith(
            new List<ArticleData> 
             {
                 new ArticleData
                 {
                     ArticleNum = "1000000",
                     Price = 10000,
                     Qty = 3
                 },
                 new ArticleData
                 {
                     ArticleNum = "1000201",
                 }
             },
             OrderSource.WebSite,
             PaymentType.Cash,
             true,
             null,
             "ИЗМЕНЕННЫЙ И ОТМЕНЕННЫЙ РЕЗЕРВ"
                );

            OrderId = wf.StartProcess();
            Debug.WriteLine(OrderId);

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

            wf.CallCompleteTask("Delivery_Approval", "Согласуйте дату доставки", "OK");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.SetArticleState(0, "Отложен");
            wf.SetArticleState(1, "Отложен");
            wf.CallCompleteTask("Cancel_Order_In_Process_And_Return_Articles", "Верните отложенный товар в зал продаж", "OK");

            Thread.Sleep(TimeSpan.FromSeconds(4));
            wf.AssertOrderRejected("Резерв отменен в WWS сотрудником магазина");
            wf.AssertTicketClosed();
        }





        //[TestMethod]
        //public void Standart_Test1()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //    new List<ArticleData>
        //    {
        //        new ArticleData
        //        {
        //            ArticleNum = "1111111",
        //            Price = 20000,
        //            Qty = 3
        //        },
        //        new ArticleData
        //        {
        //            ArticleNum = "1159035",
        //            Price = 20000,
        //            Qty = 3
        //        }
        //    },
        //    OrderSource.WebSite,
        //    PaymentType.Cash,
        //    true,
        //    "ОПЛАЧЕННЫЙ РЕЗЕРВ");

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.CallCompleteTask("Approve_Order", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.LipsSendDeliverResponse();

        //    Thread.Sleep(TimeSpan.FromSeconds(20));
        //    wf.AssertOrderClosed();
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void Standart_Test2()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //    new List<ArticleData>
        //    {
        //        new ArticleData
        //        {
        //            ArticleNum = "1111111",
        //            Price = 20000,
        //            Qty = 3
        //        },
        //        new ArticleData
        //        {
        //            ArticleNum = "1159035",
        //            Price = 20000,
        //            Qty = 3
        //        }
        //    },
        //    OrderSource.WebSite,
        //    PaymentType.Cash,
        //    true,
        //    "ОПЛАЧЕННЫЙ РЕЗЕРВ");

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.ChangeToPickup();
        //    wf.CallCompleteTask("Approve_Order", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Check_Reserved_Items", "Готово");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderClosed();
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void Standart_Test3()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //    new List<ArticleData>
        //    {
        //        new ArticleData
        //        {
        //            ArticleNum = "1111111",
        //            Price = 20000,
        //            Qty = 3
        //        },
        //        new ArticleData
        //        {
        //            ArticleNum = "1159035",
        //            Price = 20000,
        //            Qty = 3
        //        }
        //    },
        //    OrderSource.WebSite,
        //    PaymentType.Cash,
        //    true,
        //    "ОПЛАЧЕННЫЙ РЕЗЕРВ");

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.CallCompleteTask("Approve_Order", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.LipsSendDeliverResponse(false);

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.CallCompleteTask("Deliver_Order_Incidents_Approval", "ОК");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.LipsSendDeliverResponse();

        //    Thread.Sleep(TimeSpan.FromSeconds(10));

        //    wf.AssertOrderClosed();
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void Standart_Test4()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //    new List<ArticleData>
        //    {
        //        new ArticleData
        //        {
        //            ArticleNum = "1111111",
        //            Price = 20000,
        //            Qty = 3
        //        },
        //        new ArticleData
        //        {
        //            ArticleNum = "1159035",
        //            Price = 20000,
        //            Qty = 3
        //        }
        //    },
        //    OrderSource.WebSite,
        //    PaymentType.Cash,
        //    true,
        //    "ОПЛАЧЕННЫЙ РЕЗЕРВ");

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.ChangeToPickup();
        //    wf.ChangePickupPoint("R005");
        //    wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.CallCompleteTask("Create_Push_Transfer", "Сделал трансфер", new Dictionary<string, object>
        //    {
        //        {"Магазин(SapCode)","R601"},
        //        {"Номер заказ-трансфера","12345678"},
        //        {"Номер выхода трансфера","12345667"}
        //    });

        //    Thread.Sleep(TimeSpan.FromSeconds(10));
        //    wf.LipsSendTransferShippedProcessingInfoResponse();

        //    Thread.Sleep(TimeSpan.FromSeconds(20));
        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Confirm_Transfer", "Подтверди получение трансфера", "Готово");
        //    Thread.Sleep(TimeSpan.FromSeconds(60));

        //    wf.AssertOrderClosed();
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void Standart_Test5()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //    new List<ArticleData>
        //    {
        //        new ArticleData
        //        {
        //            ArticleNum = "1111111",
        //            Price = 20000,
        //            Qty = 3
        //        },
        //        new ArticleData
        //        {
        //            ArticleNum = "1159035",
        //            Price = 20000,
        //            Qty = 3
        //        }
        //    },
        //    OrderSource.WebSite,
        //    PaymentType.Cash,
        //    false,
        //    "ОПЛАЧЕННЫЙ РЕЗЕРВ");

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    Thread.Sleep(TimeSpan.FromSeconds(20));

        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Не найден");
        //    wf.CallCompleteTask("Check_Reserved_Items", "123", "Готово");

        //    Thread.Sleep(TimeSpan.FromSeconds(20));

        //    wf.CallCompleteTask("Bad_Reserve_Approval", "Подтвердите заказ", "Отклонено покупателем",
        //        new Dictionary<string, object>
        //        {
        //            {"Причина отказа", "Нашел дешевле"}
        //        });

        //    Thread.Sleep(TimeSpan.FromSeconds(20));
        //    wf.AssertOrderRejected("Нашел дешевле");
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void Standart_Incorrect_Test()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {

        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        }, OrderSource.WebSite, PaymentType.Cash, true);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.ChangeToPickup();
        //    wf.ChangeToDelivery();

        //    wf.AddArticle(1111111, 2);
        //    wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен витринный экземпляр");
        //    wf.SetArticleState(2, "Отложен");
        //    wf.CallCompleteTask("Check_Reserved_Items", "123", "Готово");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен витринный экземпляр");
        //    wf.SetArticleState(2, "Отложен");
        //    wf.CallCompleteTask("Check_Reserved_Items", "123", "Готово");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.CallCompleteTask("Unable_to_complete_order", "123", "OK");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertTicketClosed();

        //}

        //[TestMethod]
        //public void Standart_Pickup_Test()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {

        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        }, OrderSource.WebSite, PaymentType.Cash, false);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    //Thread.Sleep(TimeSpan.FromSeconds(4));

        //    //wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Check_Reserved_Items", "123", "Готово");

        //    wf.CallCompleteTask("Notify_Customer", "123", "ОК");

        //    wf.PayReserve();

        //    ////wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Опрос",
        //    //                    new Dictionary<string, object>()
        //    //                        {
        //    //                            {"Оцените по 5 балльной шкале работу колл – центра", "2"},
        //    //                            {"Причина 1 (работа колл-центра)", "Поздно  позвонили для подтверждения заказа"},
        //    //                            {"Причина 2 (работа колл-центра)", "Сотрудник грубил"}
        //    //                        });

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderClosed();
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void Standart_WrongArticle_Test()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {

        //                ArticleNum = "2111111",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        }, OrderSource.WebSite, PaymentType.Cash, true, "ОПЛАЧЕННЫЙ РЕЗЕРВ");

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.ChangeToPickup();
        //    wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.RemoveArticle(1);
        //    wf.CallCompleteTask("Review_Reserve", "123", "OK");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.SetArticleState(0, "Отложен");
        //    wf.CallCompleteTask("Check_Reserved_Items", "123", "Готово");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.CallCompleteTask("Notify_Customer", "123", "ОК");

        //    wf.PayReserve();

        //    ////wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Опрос",
        //    //                    new Dictionary<string, object>()
        //    //                        {
        //    //                            {"Оцените по 5 балльной шкале работу колл – центра", "2"},
        //    //                            {"Причина 1 (работа колл-центра)", "Поздно  позвонили для подтверждения заказа"},
        //    //                            {"Причина 2 (работа колл-центра)", "Сотрудник грубил"}
        //    //                        });

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderClosed();
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCancel_Test()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "2111111",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        true);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.CallCancel();


        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderRejected("Покупатель отказался");
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCGT_01()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        true);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Отклонено покупателем",
        //        new Dictionary<string, object>
        //        {
        //            {"Причина отказа", "Нашел дешевле"}
        //        });

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderRejected("Нашел дешевле");
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCGT_02()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "2111111",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        true);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Не дозвонился");
        //    Thread.Sleep(TimeSpan.FromMinutes(0.5));

        //    wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Не дозвонился");
        //    Thread.Sleep(TimeSpan.FromMinutes(0.5));

        //    wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Не дозвонился");
        //    Thread.Sleep(TimeSpan.FromMinutes(1));

        //    wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Не дозвонился");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderRejected("Невозможно связаться с покупателем");
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCase3()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 6
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 6
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        true);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Не дозвонился");
        //    Thread.Sleep(TimeSpan.FromMinutes(0.5));

        //    wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Не дозвонился");
        //    Thread.Sleep(TimeSpan.FromMinutes(0.5));

        //    wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    wf.CallCompleteTask("Unable_to_complete_order", "Невозможность комплектации", "Не дозвонился");
        //    Thread.Sleep(TimeSpan.FromMinutes(0.5));

        //    wf.CallCompleteTask("Unable_to_complete_order", "Невозможность комплектации", "Не дозвонился");
        //    Thread.Sleep(TimeSpan.FromMinutes(0.5));

        //    wf.CallCompleteTask("Unable_to_complete_order", "Невозможность комплектации", "OK");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderRejected("Нет в наличии");
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCGT_03()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        false);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    //Thread.Sleep(TimeSpan.FromSeconds(4));

        //    //wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    //wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "Не дозвонился");
        //    //Thread.Sleep(TimeSpan.FromMinutes(0.5));

        //    //wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "Не дозвонился");
        //    //Thread.Sleep(TimeSpan.FromMinutes(0.5));

        //    //wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "Не дозвонился");

        //    wf.CallCompleteTask("Return_Articles", "Верните артикулы", "Вернул");

        //    Thread.Sleep(TimeSpan.FromSeconds(17));
        //    wf.AssertOrderRejected("Невозможно связаться с покупателем");
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCase5()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        false);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    //Thread.Sleep(TimeSpan.FromSeconds(4));

        //    //wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    //wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "Отклонено покупателем",
        //    //    new Dictionary<string, object>
        //    //    {
        //    //        {"Причина отказа", "Нашел дешевле"}
        //    //    });

        //    wf.CallCompleteTask("Return_Articles", "Верните артикулы", "Вернул");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderRejected("Нашел дешевле");
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCase6()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        true);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    wf.CallCompleteTask("Delivery_Approval", "Согласуйте дату доставки", "Отклонено покупателем",
        //        new Dictionary<string, object>
        //        {
        //            {"Причина отказа", "Нашел дешевле"}
        //        });

        //    wf.CallCompleteTask("Return_Articles", "Верните артикулы", "Вернул");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderRejected("Нашел дешевле");
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCase7()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        false);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    //Thread.Sleep(TimeSpan.FromSeconds(4));

        //    //wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    ////wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "ОК");

        //    wf.CancelReserve();

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.CallCompleteTask("Return_Articles", "Верните артикулы", "Вернул");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderRejected("Резерв отменен в WWS сотрудником магазина");
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCase8()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        false);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    //Thread.Sleep(TimeSpan.FromSeconds(4));

        //    //wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    //wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "ОК");

        //    // НАДО БОЛЬШЕ ВРЕМЕНИ
        //    Thread.Sleep(TimeSpan.FromMinutes(1));

        //    wf.CallCompleteTask("Auto_Order_Expiration_Check", "Напоминание про срок резерва", "Нет");
        //    //,
        //    //    new Dictionary<string, object>
        //    //    {
        //    //        {"Причина", "Нашел дешевле"}
        //    //    });

        //    wf.CallCompleteTask("Cancel_Order_In_Process_And_Return_Articles", "Верните артикулы", "OK");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderRejected("Нашел дешевле");
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCase9()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        true);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    wf.CallCompleteTask("Delivery_Approval", "Согласуйте дату доставки", "OK",
        //        new Dictionary<string, object>
        //        {
        //            {"Дата доставки", DateTime.UtcNow.AddMinutes(1)}
        //        });

        //    Thread.Sleep(TimeSpan.FromMinutes(1));

        //    wf.CallCompleteTask("Check_Payment", "Проверьте получение заказа", "Покупатель получил заказ");

        //    wf.CallCompleteTask("Cancel_Order_In_Process_And_Return_Articles", "Верните артикулы", "OK");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderRejected("Уже купил в ММ");
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCase10_()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        true);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    wf.CallCompleteTask("Delivery_Approval", "Согласуйте дату доставки", "OK",
        //        new Dictionary<string, object>
        //        {
        //            {"Дата доставки", DateTime.UtcNow.AddMinutes(1)}
        //        });

        //    Thread.Sleep(TimeSpan.FromMinutes(1));

        //    wf.CallCompleteTask("Check_Payment", "Проверьте получение заказа", "Изменилась дата доставки",
        //        new Dictionary<string, object>
        //        {
        //            {"Новая дата доставки", DateTime.UtcNow.AddMinutes(1)}
        //        });

        //    Thread.Sleep(TimeSpan.FromMinutes(1));

        //    wf.CallCompleteTask("Check_Payment", "Проверьте получение заказа", "Изменилась дата доставки",
        //        new Dictionary<string, object>
        //        {
        //            {"Новая дата доставки", DateTime.UtcNow.AddMinutes(1)}
        //        });

        //    Thread.Sleep(TimeSpan.FromMinutes(0.5));
        //    wf.PayReserve();

        //    ////wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Опрос",
        //    //        new Dictionary<string, object>()
        //    //                        {
        //    //                            {"Оцените по 5 балльной шкале работу колл – центра", "2"},
        //    //                            {"Причина 1 (работа колл-центра)", "Поздно  позвонили для подтверждения заказа"},
        //    //                            {"Причина 2 (работа колл-центра)", "Сотрудник грубил"}
        //    //                        });

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderClosed();
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCase10()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        true,
        //        "ОПЛАЧЕННЫЙ РЕЗЕРВ");

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    wf.CallCompleteTask("Delivery_Approval", "Согласуйте дату доставки", "OK",
        //        new Dictionary<string, object>
        //        {
        //            {"Дата доставки", DateTime.UtcNow.AddMinutes(5)}
        //        });

        //    wf.PayReserve();

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    ////wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Опрос",
        //    //                    new Dictionary<string, object>()
        //    //                        {
        //    //                            {"Оцените по 5 балльной шкале работу колл – центра", "2"},
        //    //                            {"Причина 1 (работа колл-центра)", "Поздно  позвонили для подтверждения заказа"},
        //    //                            {"Причина 2 (работа колл-центра)", "Сотрудник грубил"}
        //    //                        });

        //    //Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderClosed();
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCase11()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        false,
        //        "ОПЛАЧЕННЫЙ РЕЗЕРВ");

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    //Thread.Sleep(TimeSpan.FromSeconds(4));
        //    //wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    ////wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "ОК");

        //    //wf.PayReserve();

        //    ////wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Не участвует в выборке");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderClosed();
        //    wf.AssertTicketClosed();
        //}


        //[TestMethod]
        //public void SmartCase14()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        false);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    //Thread.Sleep(TimeSpan.FromSeconds(4));

        //    //wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    Thread.Sleep(TimeSpan.FromSeconds(60));
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    ////wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "ОК");

        //    wf.PayReserve();

        //    ////wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Не участвует в выборке");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderClosed();
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCase15()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        false);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    //Thread.Sleep(TimeSpan.FromSeconds(4));

        //    //wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    Thread.Sleep(TimeSpan.FromSeconds(90));
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    ////wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "ОК");

        //    wf.PayReserve();

        //    ////wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Не участвует в выборке");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderClosed();
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCancel_NotProceed()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 6
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 5
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        true);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(75));

        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Время истекло");

        //    wf.CallCompleteTask("Unable_to_complete_order", "Невозможность комплектации", "OK");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderRejected("Заказ не был обработан магазином");
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCancel_NotProceed2()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 5
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        false);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(75));

        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Время истекло");

        //    wf.ChangePickupPoint("R005");
        //    wf.CallCompleteTask("Bad_Reserve_Approval", "Подтвердите заказ", "ОК");

        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    wf.PayReserve();

        //    ////wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Опрос",
        //    //        new Dictionary<string, object>()
        //    //                        {
        //    //                            {"Оцените по 5 балльной шкале работу колл – центра", "2"},
        //    //                            {"Причина 1 (работа колл-центра)", "Поздно  позвонили для подтверждения заказа"},
        //    //                            {"Причина 2 (работа колл-центра)", "Сотрудник грубил"}
        //    //                        });

        //    Thread.Sleep(TimeSpan.FromSeconds(60));
        //    wf.AssertOrderClosed();
        //    wf.AssertTicketClosed();

        //}

        //[TestMethod]
        //public void SmartCase16()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        true);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Не найден");
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    wf.CallCompleteTask("Delivery_Approval", "Согласуйте дату доставки", "OK",
        //        new Dictionary<string, object>
        //        {
        //            {"Дата доставки", DateTime.UtcNow.AddMinutes(5)}
        //        });

        //    wf.PayReserve();

        //    ////wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Не участвует в выборке");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderClosed();
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCase17()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        true);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Не найден");
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    //пересоздание резерва в другом магазине
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    wf.CallCompleteTask("Delivery_Approval", "Согласуйте дату доставки", "OK",
        //        new Dictionary<string, object>
        //        {
        //            {"Дата доставки", DateTime.UtcNow.AddMinutes(5)}
        //        });

        //    wf.PayReserve();

        //    ////wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Не участвует в выборке");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderClosed();
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCase18()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        false);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    wf.ChangePickupPoint("R001");
        //    //wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.CallCompleteTask("Check_And_Correct_Order", "Проверьте заказ", "Проверил");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.ChangePickupPoint("R006");
        //    wf.CallCompleteTask("Check_And_Correct_Order", "Проверьте заказ", "Проверил");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    //пересоздание резерва в другом магазине
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    ////wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "ОК");

        //    wf.PayReserve();

        //    ////wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Не участвует в выборке");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderClosed();
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCase19()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash, false);

        //    wf.StartProcess();
        //    Console.WriteLine(OrderId);

        //    wf.ChangePickupPoint("R001");
        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    //wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    wf.CallCompleteTask("Check_And_Correct_Order", "Проверьте заказ", "Проверил");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.ChangePickupPoint("R006");
        //    wf.CallCompleteTask("Check_And_Correct_Order", "Проверьте заказ", "Проверил");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    //пересоздание резерва в другом магазине
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    ////wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "ОК"); 

        //    wf.PayReserve();

        //    ////wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Не участвует в выборке");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderClosed();
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCase20()
        //{
        //    var random = new Random((int)DateTime.Now.Ticks % 100000);
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 5
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        false);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    wf.ChangePickupPoint("R001");
        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    //wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    wf.CallCompleteTask("Check_And_Correct_Order", "Проверьте заказ", "Проверил");

        //    wf.ChangePickupPoint("R005");
        //    wf.CallCompleteTask("Check_And_Correct_Order", "Проверьте заказ", "Проверил");

        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Не найден");
        //    //пересоздание резерва в другом магазине
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    wf.ChangePickupPoint("R001");
        //    wf.ChangeTransferAgreementWithCustomer(true);
        //    wf.CallCompleteTask("Bad_Reserve_Approval", "Подтвердите заказ", "ОК");

        //    wf.ChangePickupPoint("R006");
        //    wf.CallCompleteTask("Check_And_Correct_Order", "Проверьте заказ", "Проверил");

        //    wf.CallCompleteTask("Create_Transfer", "Сделай трансфер", "Сделал трансфер", new Dictionary<string, object>
        //    {
        //        {"Магазин(SapCode)", "R005"},
        //        {"Номер заказ-трансфера", random.Next(0,int.MaxValue).ToString()}
        //    });

        //    wf.SetArticleState(0, "Трансфер");
        //    wf.SetArticleState(1, "Трансфер");
        //    wf.CallCompleteTask("Collect_Transfer", "Собери трансфер", "Готово");

        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Confirm_Transfer", "Подтверди получение трансфера", "Готово");

        //    wf.PayReserve();

        //    ////wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Не участвует в выборке");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderClosed();
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCase21()
        //{
        //    var random = new Random((int)DateTime.Now.Ticks % 100000);
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 5
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1274599",
        //                Price = 0,
        //                Qty = 1
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        false);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    wf.ChangeTransferAgreementWithCustomer(true);
        //    wf.ChangePickupPoint("R001");
        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    //wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    wf.CallCompleteTask("Check_And_Correct_Order", "Проверьте заказ", "Проверил");

        //    wf.ChangePickupPoint("R007");
        //    wf.CallCompleteTask("Check_And_Correct_Order", "Проверьте заказ", "Проверил");

        //    wf.CallCompleteTask("Create_Transfer", "Сделай трансфер", "Сделал трансфер", new Dictionary<string, object>
        //    {
        //        {"Магазин(SapCode)", "R005"},
        //        {"Номер заказ-трансфера", random.Next(0,int.MaxValue).ToString()}
        //    });

        //    wf.SetArticleState(0, "Трансфер");
        //    wf.CallCompleteTask("Collect_Transfer", "Собери трансфер", "Готово");

        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Confirm_Transfer", "Подтверди получение трансфера", "Готово");

        //    wf.PayReserve();

        //    //wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Не участвует в выборке");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderClosed();
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartChangedReserve_01()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        false,
        //        "ИЗМЕНЕННЫЙ И ОПЛАЧЕННЫЙ РЕЗЕРВ");

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    //Thread.Sleep(TimeSpan.FromSeconds(4));

        //    //wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    wf.TransformReserve();
        //    wf.PayReserve();

        //    ////wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "ОК");

        //    //wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Не участвует в выборке");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderClosed();
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartChangedReserve_02()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        false,
        //        "ИЗМЕНЕННЫЙ И ОТМЕНЕННЫЙ РЕЗЕРВ");

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    //Thread.Sleep(TimeSpan.FromSeconds(4));

        //    //wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    wf.TransformReserve();
        //    wf.CancelReserve();

        //    ////wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "ОК");

        //    wf.CallCompleteTask("Return_Articles", "Верните артикулы", "Вернул");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderRejected("Резерв отменен в WWS сотрудником магазина");
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartChangedReserve_03()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        false,
        //        "ИЗМЕНЕННЫЙ РЕЗЕРВ");

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    //Thread.Sleep(TimeSpan.FromSeconds(4));

        //    //wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    ////wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "ОК");

        //    wf.TransformReserve();

        //    Thread.Sleep(TimeSpan.FromMinutes(1));

        //    wf.PayReserve();

        //    wf.CallCompleteTask("Auto_Order_Expiration_Check", "Напоминание про срок резерва", "Да");

        //    //wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Не участвует в выборке");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderClosed();
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartChangedReserve_04()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        false,
        //        "ИЗМЕНЕННЫЙ РЕЗЕРВ");

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    //Thread.Sleep(TimeSpan.FromSeconds(4));

        //    //wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    wf.TransformReserve();

        //    wf.CallCancel();

        //    wf.CallCompleteTask("Cancel_Order_and_Return_Articles", "Верните артикулы", "Вернул");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderRejected("Покупатель отказался");
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCase22()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        false);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.CallCancel();

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderRejected("Покупатель отказался");
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCase23()
        //{
        //    var random = new Random((int)DateTime.Now.Ticks % 100000);
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 5
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1274599",
        //                Price = 0,
        //                Qty = 1
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        false);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    wf.ChangeTransferAgreementWithCustomer(true);
        //    wf.ChangePickupPoint("R001");
        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    //wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    wf.CallCompleteTask("Check_And_Correct_Order", "Проверьте заказ", "Отменить заказ");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderRejected("Невозможно связаться с покупателем");
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCGT_04()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        false);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    //Thread.Sleep(TimeSpan.FromSeconds(4));

        //    //wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    ////wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "ОК");

        //    Thread.Sleep(TimeSpan.FromMinutes(1));

        //    wf.CallCompleteTask("Auto_Order_Expiration_Check", "Напоминание про срок резерва", "Не дозвонился");
        //    Thread.Sleep(TimeSpan.FromMinutes(0.5));

        //    wf.CallCompleteTask("Auto_Order_Expiration_Check", "Напоминание про срок резерва", "Не дозвонился");
        //    Thread.Sleep(TimeSpan.FromMinutes(0.5));

        //    wf.CallCompleteTask("Auto_Order_Expiration_Check", "Напоминание про срок резерва", "Не дозвонился");
        //    Thread.Sleep(TimeSpan.FromMinutes(0.5));

        //    wf.CallCompleteTask("Auto_Order_Expiration_Check", "Напоминание про срок резерва", "Не дозвонился");
        //    Thread.Sleep(TimeSpan.FromMinutes(0.5));

        //    wf.CallCompleteTask("Auto_Order_Expiration_Check", "Напоминание про срок резерва", "Не дозвонился");

        //    wf.CallCompleteTask("Cancel_Order_In_Process_And_Return_Articles", "Верните артикулы", "OK");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderRejected("Невозможно связаться с покупателем");
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCGT_05()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        false);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    //Thread.Sleep(TimeSpan.FromSeconds(4));

        //    //wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    ////wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "ОК");

        //    Thread.Sleep(TimeSpan.FromMinutes(1));

        //    wf.CallCompleteTask("Auto_Order_Expiration_Check", "Напоминание про срок резерва", "Не дозвонился");
        //    Thread.Sleep(TimeSpan.FromMinutes(0.5));

        //    wf.CallCompleteTask("Auto_Order_Expiration_Check", "Напоминание про срок резерва", "Не дозвонился");
        //    Thread.Sleep(TimeSpan.FromMinutes(0.5));

        //    wf.CallCompleteTask("Auto_Order_Expiration_Check", "Напоминание про срок резерва", "Не дозвонился");
        //    Thread.Sleep(TimeSpan.FromMinutes(0.5));

        //    wf.PayReserve();

        //    wf.CallCompleteTask("Auto_Order_Expiration_Check", "Напоминание про срок резерва", "Не дозвонился");
        //    Thread.Sleep(TimeSpan.FromMinutes(0.5));

        //    wf.CallCompleteTask("Auto_Order_Expiration_Check", "Напоминание про срок резерва", "Не дозвонился");

        //    //wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Не участвует в выборке");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderClosed();
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCase24()
        //{
        //    var random = new Random((int)DateTime.Now.Ticks % 100000);
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 5
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        false);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    //Thread.Sleep(TimeSpan.FromSeconds(4));

        //    //wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    wf.SetArticleState(0, "Не найден");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    wf.ChangePickupPoint("R005");
        //    wf.ChangeTransferAgreementWithCustomer(true);
        //    wf.CallCompleteTask("Bad_Reserve_Approval", "Подтвердите заказ", "ОК");

        //    wf.CallCompleteTask("Create_Transfer", "Сделай трансфер", "Сделал трансфер", new Dictionary<string, object>
        //    {
        //        {"Магазин(SapCode)", "R005"},
        //        {"Номер заказ-трансфера", random.Next(0,int.MaxValue).ToString()}
        //    });

        //    wf.SetArticleState(0, "Трансфер");
        //    wf.SetArticleState(1, "Трансфер");
        //    wf.CallCompleteTask("Collect_Transfer", "Собери трансфер", "Готово");

        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Confirm_Transfer", "Подтверди получение трансфера", "Готово");

        //    Thread.Sleep(TimeSpan.FromMinutes(1));

        //    wf.CallCompleteTask("Auto_Order_Expiration_Check", "Напоминание про срок резерва", "Нет");
        //    //,
        //    //     new Dictionary<string, object>
        //    //     {
        //    //         {"Причина", "Нашел дешевле"}
        //    //     });

        //    wf.CallCompleteTask("Cancel_Order_In_Process_And_Return_Articles", "Верните артикулы", "OK");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderRejected("Нашел дешевле");
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCase25()
        //{
        //    var random = new Random((int)DateTime.Now.Ticks % 100000);
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 5
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        false);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    //Thread.Sleep(TimeSpan.FromSeconds(4));

        //    //wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    wf.SetArticleState(0, "Не найден");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    wf.ChangePickupPoint("R005");
        //    wf.ChangeTransferAgreementWithCustomer(true);
        //    wf.CallCompleteTask("Bad_Reserve_Approval", "Подтвердите заказ", "ОК");

        //    wf.CallCompleteTask("Create_Transfer", "Сделай трансфер", "Сделал трансфер", new Dictionary<string, object>
        //    {
        //        {"Магазин(SapCode)", "R005"},
        //        {"Номер заказ-трансфера", random.Next(0,int.MaxValue).ToString()}
        //    });

        //    Thread.Sleep(TimeSpan.FromSeconds(75));

        //    wf.CallCompleteTask("Collect_Transfer", "Собери трансфер", "Время истекло");

        //    wf.CallCompleteTask("Create_Transfer", "Сделай трансфер", "Сделал трансфер", new Dictionary<string, object>
        //    {
        //        {"Магазин(SapCode)", "R004"},
        //        {"Номер заказ-трансфера", random.Next(0,int.MaxValue).ToString()}
        //    });

        //    wf.SetArticleState(0, "Не найден");
        //    wf.SetArticleState(1, "Трансфер");
        //    wf.CallCompleteTask("Collect_Transfer", "Собери трансфер", "Готово");

        //    wf.CallCompleteTask("Create_Transfer", "Сделай трансфер", "Сделал трансфер", new Dictionary<string, object>
        //    {
        //        {"Магазин(SapCode)", "R007"},
        //        {"Номер заказ-трансфера", random.Next(0,int.MaxValue).ToString()}
        //    });

        //    wf.SetArticleState(0, "Трансфер");
        //    wf.SetArticleState(1, "Трансфер");
        //    wf.CallCompleteTask("Collect_Transfer", "Собери трансфер", "Готово");

        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Confirm_Transfer", "Подтверди получение трансфера", "Готово");

        //    Thread.Sleep(TimeSpan.FromMinutes(1));

        //    wf.CallCompleteTask("Auto_Order_Expiration_Check", "Напоминание про срок резерва", "Нет");
        //    //,
        //    //     new Dictionary<string, object>
        //    //     {
        //    //         {"Причина", "Нашел дешевле"}
        //    //     });

        //    wf.CallCompleteTask("Cancel_Order_In_Process_And_Return_Articles", "Верните артикулы", "OK");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderRejected("Нашел дешевле");
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCase26()
        //{
        //    var random = new Random((int)DateTime.Now.Ticks % 100000);
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 5
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        false);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    //Thread.Sleep(TimeSpan.FromSeconds(4));

        //    //wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    wf.SetArticleState(0, "Не найден");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    wf.ChangePickupPoint("R005");
        //    wf.ChangeTransferAgreementWithCustomer(true);
        //    wf.CallCompleteTask("Bad_Reserve_Approval", "Подтвердите заказ", "ОК");

        //    wf.CallCompleteTask("Create_Transfer", "Сделай трансфер", "Сделал трансфер", new Dictionary<string, object>
        //    {
        //        {"Магазин(SapCode)", "R005"},
        //        {"Номер заказ-трансфера", random.Next(0,int.MaxValue).ToString()}
        //    });

        //    wf.SetArticleState(0, "Трансфер");
        //    wf.SetArticleState(1, "Трансфер");
        //    wf.CallCompleteTask("Collect_Transfer", "Собери трансфер", "Готово");

        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Confirm_Transfer", "Подтверди получение трансфера", "Отменить заказ");

        //    wf.CallCompleteTask("Unable_to_complete_order", "Невозможность комплектации", "OK");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderRejected("Трансфер не удался");
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartLIPS_Paid()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1135370",
        //                Price = 1999,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        true);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.LipsSendCommandDeliverOrderProcessingInfo("Received", "1фигня всякая",
        //        new[] {new Incident{Code = 10080, Description = "sdfhsdfhdfg"}
        //       });  // текущее состояние доставки

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.LipsSendCommandDeliverOrderProcessingInfo("Generated", "2фигня всякая",
        //        new[] {new Incident{Code = 10080, Description = "sdfhsdfhdfg"}
        //       });  // текущее состояние доставки

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.LipsSendCommandDeliverOrderProcessingInfo("Scheduled", "3фигня всякая",
        //        new[] {new Incident{Code = 10080, Description = "sdfhsdfhdfg"}
        //       });  // текущее состояние доставки

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.LipsSendCommandDeliverOrderProcessingInfo("Approved", "4фигня всякая",
        //        new[] {new Incident{Code = 10080, Description = "sdfhsdfhdfg"}
        //       });  // текущее состояние доставки

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.LipsSendCommandDeliverOrderProcessingInfo("Started", "5фигня всякая",
        //        new[] {new Incident{Code = 10080, Description = "sdfhsdfhdfg"}
        //       });  // текущее состояние доставки

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.LipsSendCommandDeliverOrderProcessingInfo("Started", "5фигня всякая",
        //        new[] {new Incident{Code = 10080, Description = "sdfhsdfhdfg"}
        //       });  // текущее состояние доставки

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.LipsSendCommandDeliverOrderProcessingInfo("Started", "5фигня всякая",
        //        new[] {new Incident{Code = 10080, Description = "sdfhsdfhdfg"}
        //       });  // текущее состояние доставки

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.LipsSendCommandDeliverOrderProcessingInfo("Ready for shipment", "6фигня всякая",
        //        new[] {new Incident{Code = 10080, Description = "sdfhsdfhdfg"}
        //       });  // текущее состояние доставки

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.LipsSendCommandDeliverOrderProcessingInfo("Shipped", "7фигня всякая",
        //        new[] {new Incident{Code = 10080, Description = "sdfhsdfhdfg"}
        //       });  // текущее состояние доставки

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.LipsSendCommandDeliverOrderProcessingInfo("Last Mile Reached", "8фигня всякая",
        //        new[] {new Incident{Code = 10080, Description = "sdfhsdfhdfg"}
        //       });  // текущее состояние доставки

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.LipsSendCommandDeliverOrderProcessingInfo("Customer Reached", "9фигня всякая",
        //        new[] {new Incident{Code = 10080, Description = "sdfhsdfhdfg"}
        //       });  // текущее состояние доставки

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.LipsSendCommandDeliverOrderProcessingInfo("Returned by LSP", "10фигня всякая",
        //        new[] {new Incident{Code = 10080, Description = "sdfhsdfhdfg"}
        //       });  // текущее состояние доставки

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.LipsSendCommandDeliverOrderProcessingInfo("Return Received", "11фигня всякая",
        //        new[] {new Incident{Code = 10080, Description = "sdfhsdfhdfg"}
        //       });  // текущее состояние доставки

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.LipsSendCommandDeliverOrderProcessingInfo("Closed", "12фигня всякая",
        //        new[] {new Incident{Code = 10080, Description = "sdfhsdfhdfg"}
        //       });  // текущее состояние доставки

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    // финальный статус запроса на отмену доставки: дон - отменен, фейл - не удалось отменить
        //    // ининцируется отменой заказа из ОМ
        //    //wf.LipsSendCommandResponseCancelDelivery(TT.IntegrationTester.MM.Proxy.LipsMockAutoTestHelper.Result.Done, "hfdhf");

        //    //финальный статус доставки
        //    wf.LipsSendCommandResponseDeliverOrder(TT.IntegrationTester.MM.Proxy.LipsMockAutoTestHelper.Result.Done,
        //        new[] {
        //         new Incident1{
        //            Code = 10080, 
        //            Description = "sdfhsdfhdfg"}
        //       });

        //    Thread.Sleep(TimeSpan.FromMinutes(1));

        //    wf.PayReserve();
        //    wf.CallCompleteTask("Check_Payment", "Проверьте получение заказа", "Покупатель не получил заказ",
        //         new Dictionary<string, object>
        //        {
        //            {"Причина", "Нашел дешевле"}
        //        });

        //    //wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Не участвует в выборке");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderClosed();
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCheckPayment_Paid()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        false);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    //Thread.Sleep(TimeSpan.FromSeconds(4));

        //    //wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    ////wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "ОК");

        //    Thread.Sleep(TimeSpan.FromMinutes(1));

        //    wf.PayReserve();
        //    wf.CallCompleteTask("Auto_Order_Expiration_Check", "Напоминание про срок резерва", "Нет");
        //    //,
        //    //     new Dictionary<string, object>
        //    //     {
        //    //         {"Причина", "Нашел дешевле"}
        //    //     });

        //    //wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Не участвует в выборке");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderClosed();
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void SmartCheckPayment_Cancelled()
        //{
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //        new List<ArticleData>
        //        {
        //            new ArticleData
        //            {
        //                ArticleNum = "1111111",
        //                Price = 20000,
        //                Qty = 3
        //            },
        //            new ArticleData
        //            {
        //                ArticleNum = "1159035",
        //                Price = 20000,
        //                Qty = 3
        //            }
        //        },
        //        OrderSource.WebSite,
        //        PaymentType.Cash,
        //        false);

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    //Thread.Sleep(TimeSpan.FromSeconds(4));

        //    //wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    ////wf.CallCompleteTask("Notify_Customer", "Подтвердите резерв", "ОК");

        //    Thread.Sleep(TimeSpan.FromMinutes(1));

        //    wf.CancelReserve();
        //    wf.CallCompleteTask("Auto_Order_Expiration_Check", "Напоминание про срок резерва", "Да");

        //    wf.CallCompleteTask("Cancel_Order_In_Process_And_Return_Articles", "Верните артикулы", "OK");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderRejected("Резерв отменен в WWS сотрудником магазина");
        //    wf.AssertTicketClosed();
        //}


        //[TestMethod]
        //public void Standart_Transfer_Test()
        //{
        //    var random = new Random((int)DateTime.Now.Ticks % 100000);
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //    new List<ArticleData>
        //    {
        //        new ArticleData
        //        {
        //            ArticleNum = "1111111",
        //            Price = 20000,
        //            Qty = 3
        //        },
        //        new ArticleData
        //        {
        //            ArticleNum = "1159035",
        //            Price = 20000,
        //            Qty = 3
        //        }
        //    },
        //    OrderSource.WebSite,
        //    PaymentType.Cash,
        //    true,
        //    "Тест");

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.ChangeToPickup();
        //    wf.ChangeTransferAgreementWithCustomer(true);
        //    wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.CallCompleteTask("Create_Transfer", "Сделай трансфер", "Сделал трансфер", new Dictionary<string, object>
        //    {
        //        {"Магазин(SapCode)", "R004"},
        //        {"Номер заказ-трансфера", random.Next(0,int.MaxValue).ToString()}
        //    });
        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.SetArticleState(0, "Трансфер");
        //    wf.SetArticleState(1, "Трансфер");
        //    wf.CallCompleteTask("Collect_Transfer", "123", "Готово");
        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Confirm_Transfer", "123", "Готово");
        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.PayReserve();

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    //wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Не участвует в выборке");

        //    Thread.Sleep(TimeSpan.FromSeconds(4));
        //    wf.AssertOrderClosed();
        //    wf.AssertTicketClosed();
        //}

        //[TestMethod]
        //public void Standart_Transfer_Test1()
        //{
        //    var random = new Random((int)DateTime.Now.Ticks % 100000);
        //    var wf = new SmartStartCashOrder2WfMM();
        //    wf.CreateWith(
        //    new List<ArticleData>
        //    {
        //        new ArticleData
        //        {
        //            ArticleNum = "1111111",
        //            Price = 20000,
        //            Qty = 3
        //        },
        //        new ArticleData
        //        {
        //            ArticleNum = "1159035",
        //            Price = 20000,
        //            Qty = 3
        //        }
        //    },
        //    OrderSource.WebSite,
        //    PaymentType.Cash,
        //    true,
        //    "Тест");

        //    OrderId = wf.StartProcess();
        //    Debug.WriteLine(OrderId);

        //    Thread.Sleep(TimeSpan.FromSeconds(4));

        //    wf.ChangeToPickup();
        //    wf.CallCompleteTask("Approve_Order", "Подтвердите заказ", "Подтверждено");

        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Не найден");
        //    wf.CallCompleteTask("Check_Reserved_Items", "Соберите интернет-заказ", "Готово");

        //    wf.ChangeTransferAgreementWithCustomer(true);
        //    wf.CallCompleteTask("Bad_Reserve_Approval", "Подтвердите заказ", "ОК");

        //    wf.CallCompleteTask("Create_Transfer", "Сделай трансфер", "Сделал трансфер", new Dictionary<string, object>
        //    {
        //        {"Магазин(SapCode)", "R004"},
        //        {"Номер заказ-трансфера", random.Next(0,int.MaxValue).ToString()}
        //    });

        //    wf.SetArticleState(0, "Трансфер");
        //    wf.SetArticleState(1, "Трансфер");
        //    wf.CallCompleteTask("Collect_Transfer", "Собери трансфер", "Готово");

        //    wf.SetArticleState(0, "Отложен");
        //    wf.SetArticleState(1, "Отложен");
        //    wf.CallCompleteTask("Confirm_Transfer", "Подтверди получение трансфера", "Готово");

        //    wf.PayReserve();


        //    //wf.CallCompleteTask("Check_Quality", "Обзвон проверки качества", "Не участвует в выборке");

        //    wf.AssertOrderClosed();
        //    wf.AssertTicketClosed();
        //}





    }
}

