using System;

namespace Orders.Backend.IntegrationTester
{
    public class Randomizer
    {
        public static int Next(int minValue, int maxValue)
        {
            var random = new Random((int)DateTime.Now.Ticks % 100000);
            return random.Next(minValue, maxValue);
        }        
    }
}