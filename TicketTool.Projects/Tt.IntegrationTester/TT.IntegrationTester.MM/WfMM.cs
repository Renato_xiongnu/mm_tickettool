﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using Newtonsoft.Json;
using TT.IntegrationTester.MM.Proxy.OldOrders;
using TT.IntegrationTester.MM.Proxy.WfSmartStart2;
using TT.IntegrationTester.MM.Proxy.WWS;
using PaymentType = TT.IntegrationTester.MM.Proxy.OldOrders.PaymentType;
using ReturnCode = TT.IntegrationTester.MM.Proxy.OldOrders.ReturnCode;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Key = TT.IntegrationTester.MM.Proxy.WfSmartStart2.Key;
using ReceiverCommand = TT.IntegrationTester.MM.Proxy.WfSmartStart2.ReceiverCommand;
using ReceiverCommandResponse = TT.IntegrationTester.MM.Proxy.WfSmartStart2.ReceiverCommandResponse;

namespace TT.IntegrationTester.MM
{
    public abstract class WfMm : WfBase, IWfMm
    {
        /// <summary>
        /// Создает заказ с возможностью изменения некоторых атрибутов
        /// </summary>
        /// <param name="articlesData">Артикульный состав</param>
        /// <param name="orderSource">Источник заказа</param>
        /// <param name="paymentType">Тип оплаты</param>
        /// <param name="hasDeilvery">Доставка если true/самовывоз если false</param>
        /// <param name="surname">Фамилия (нужна для эмуляции оплаты)</param>
        /// <param name="prepay">Предоплата</param>
        /// <param name="socialCardNumber">Номер социалльной карты</param>
        public void CreateWith(
            List<ArticleData> articlesData, 
            OrderSource orderSource, 
            PaymentType paymentType,
            bool hasDeilvery, 
            string pickupLocationId = "pickup_R201",
            string surname = "Тест",
            decimal prepay = 0.0M,
            string socialCardNumber = null
            )
        {
            var deliveryInfo = hasDeilvery
                ? new DeliveryInfo
                {
                    City = "Москва",
                    Address = "Проспект Мира дом 5 кв. 47 Онлайн кредит",
                    HasDelivery = true,
                    //DeliveryDate = DateTime.Now.AddDays(2)
                }
                : new DeliveryInfo
                {
                    HasDelivery = false,
                    PickupLocationId = pickupLocationId,
                    DeliveryDate = DateTime.Now.AddDays(2)
                };

            var clientData = new ClientData
            {
                Name = "Test",
                Email = "test@test.ru",
                Phone = "+79111111111",
                Surname = surname,                    
            };            
            var createOrderData = new CreateOrderData
            {
                OrderId = new Random().Next(20000000).ToString(CultureInfo.InvariantCulture),
                StoreInfo = new StoreInfo {SapCode = "R002"},
                OrderSource = orderSource,
                IsPreorder = false,
                PaymentType = paymentType,
                InitialSapCode = "R002"
            };
            if (paymentType == PaymentType.SocialCard)
            {
                clientData.SocialCard = new CardInfo
                {
                    Number = socialCardNumber
                };
                createOrderData.Prepay = prepay;
            }
            var createResult = new OrderServiceClient().CreateOrder(
                clientData,
                createOrderData,
                deliveryInfo,
                articlesData.ToArray());

            OrderId = createResult.OrderId;
        }

        public override void CallCancel()
        {
            Caller.CallCancel();
        }

        public override void AddArticle(int article)
        {
            throw new NotImplementedException();
        }

        public void AddArticle(int articleId, int count)
        {
            var ordersClient = new OrderServiceClient();
            var order =
                ordersClient.GerOrderDataByOrderId(Caller.OrderId);

            var newLines = new List<ReserveLine>();
            for(var i = 0; i < order.ReserveInfo.ReserveLines.Count(); i++)
            {
                var article = order.ReserveInfo.ReserveLines[i];
                newLines.Add(article);
            }

            newLines.Add(new ReserveLine
            {
                LineId = articleId,
                ArticleData = new ArticleData
                {
                    ArticleNum = articleId.ToString(),
                    Price = 10000,
                    Qty = count,
                }
            });

            var result = new OrderServiceClient().UpdateOrder(new UpdateOrderData
            {
                OrderId = Caller.OrderId
            }, new ReserveInfo
            {
                ReserveLines = newLines.ToArray()
            });

            if(result.ReturnCode == ReturnCode.Error)
            {
                throw new ApplicationException(string.Format("Не удалось обновить заказ: {0}", result.ErrorMessage));
            }
        }

        public void RemoveArticle(int index)
        {
            var ordersClient = new OrderServiceClient();
            var order =
                ordersClient.GerOrderDataByOrderId(Caller.OrderId);

            var newLines = new List<ReserveLine>();
            for(var i = 0; i < order.ReserveInfo.ReserveLines.Count(); i++)
            {
                var article = order.ReserveInfo.ReserveLines[i];
                if(i != index)
                {
                    newLines.Add(article);
                }
            }

            var result = new OrderServiceClient().UpdateOrder(new UpdateOrderData
            {
                OrderId = Caller.OrderId
            }, new ReserveInfo
            {
                ReserveLines = newLines.ToArray()
            });

            if(result.ReturnCode == ReturnCode.Error)
            {
                throw new ApplicationException(string.Format("Не удалось обновить заказ: {0}", result.ErrorMessage));
            }
        }

        public void ChangeToDelivery()
        {
            var ordersClient = new OrderServiceClient();
            var order =
                ordersClient.GerOrderDataByOrderId(Caller.OrderId);
            if(order.DeliveryInfo.HasDelivery)
            {
                throw new ApplicationException("Order already delivery");
            }

            order.DeliveryInfo.HasDelivery = true;
            order.DeliveryInfo.City = "Москва";
            order.DeliveryInfo.Address = "Какая-то улица в Москве";
            var result = ordersClient.UpdateOrder(new UpdateOrderData
            {
                OrderId = order.OrderInfo.OrderId,
                DeliveryInfo = order.DeliveryInfo,
            }, new ReserveInfo
            {
                ReserveLines = order.ReserveInfo.ReserveLines
            });

            if(result.ReturnCode == ReturnCode.Error)
            {
                throw new ApplicationException(string.Format("Не удалось поменять на доставку: {0}", result.ErrorMessage));
            }
        }

        /// <summary>
        /// Поменять тип с доставки на самовывоз в магазин
        /// </summary>
        public void ChangeToPickup()
        {
            var ordersClient = new OrderServiceClient();
            var order =
                ordersClient.GerOrderDataByOrderId(Caller.OrderId);
            if(!order.DeliveryInfo.HasDelivery)
            {
                throw new ApplicationException("Order already pickup");
            }

            order.DeliveryInfo.HasDelivery = false;
            order.DeliveryInfo.Address = string.Empty;
            order.DeliveryInfo.PickupLocationId = "pickup_R006";
            var result = ordersClient.UpdateOrder(new UpdateOrderData
            {
                OrderId = order.OrderInfo.OrderId,
                DeliveryInfo = order.DeliveryInfo,
            }, new ReserveInfo
            {
                ReserveLines = order.ReserveInfo.ReserveLines
            });

            if(result.ReturnCode == ReturnCode.Error)
            {
                throw new ApplicationException(string.Format("Не удалось поменять на самовывоз: {0}",
                    result.ErrorMessage));
            }
        }
        
        /// <summary>
        /// Поменять точку самовывоза. Пока поддерживаются только PickupPoint
        /// </summary>
        /// <param name="sapCode">SAP код точки, если это магазин</param>
        public void ChangePickupPoint(string sapCode)
        {
            var ordersClient = new OrderServiceClient();
            var order =
                ordersClient.GerOrderDataByOrderId(Caller.OrderId);

            var updateOrderData = new UpdateOrderData
            {
                OrderId = order.OrderInfo.OrderId,
                DeliveryInfo = order.DeliveryInfo,
                SapCode = sapCode,
            };
            updateOrderData.DeliveryInfo.PickupLocationId = string.Format("pickup_{0}", sapCode);
            var result = ordersClient.UpdateOrder(updateOrderData, new ReserveInfo
            {
                ReserveLines = order.ReserveInfo.ReserveLines
            });

            if(result.ReturnCode == ReturnCode.Error)
            {
                throw new ApplicationException(string.Format("Не удалось поменять магазин: {0}", result.ErrorMessage));
            }
        }

        /// <summary>
        /// Установить ответ по артикулу
        /// </summary>
        /// <param name="index">Порядковый номер артикула в заказе (начиная с 0)</param>
        /// <param name="state">Ответ ("Отложен","Отложен частично", "Трансфер" и т.д.)</param>
        public void SetArticleState(int index, string state)
        {
            var ordersClient = new OrderServiceClient();
            var order =
                ordersClient.GerOrderDataByOrderId(Caller.OrderId);
            if(index > order.ReserveInfo.ReserveLines.Count())
            {
                throw new ArgumentOutOfRangeException("index", "There only {0} order lines");
            }

            var newLines = new List<ReserveLine>();
            for(var i = 0; i < order.ReserveInfo.ReserveLines.Count(); i++)
            {
                var article = order.ReserveInfo.ReserveLines[i];
                newLines.Add(article);

                if(i == index)
                {
                    switch(state)
                    {
                        case "Отложен":
                            article.ReviewItemState = ReviewItemState.Reserved;
                            break;
                        case "Не найден":
                            article.ReviewItemState = ReviewItemState.NotFound;
                            break;
                        case "Отложен частично":
                            article.ReviewItemState = ReviewItemState.Reserved;
                            article.ArticleData.Qty = 2;
                            break;
                        case "Отложен витринный экземпляр":
                            article.ReviewItemState = ReviewItemState.ReservedDisplayItem;
                            article.Comment = "Что-тоне так";
                            break;
                        case "Неправильная цена":
                            article.ReviewItemState = ReviewItemState.IncorrectPrice;
                            article.ReviewItemState = ReviewItemState.Reserved;
                            article.ArticleData.Price = article.ArticleData.Price - 1;
                            article.Comment = "Что-тоне так";
                            break;
                        case "Витрина":
                            article.ReviewItemState = ReviewItemState.DisplayItem;
                            break;
                        case "Трансфер":
                            article.ReviewItemState = ReviewItemState.Transfer;
                            article.Comment = "Трансфер";
                            article.TransferSapCode = "R001";
                            article.TransferNumber = 123456;
                            break;
                        default:
                            throw new NotImplementedException();
                    }
                }
            }

            new OrderServiceClient().UpdateOrder(new UpdateOrderData
            {
                OrderId = Caller.OrderId
            }, new ReserveInfo
            {
                ReserveLines = newLines.ToArray()
            });
        }

        public void ChangeTransferAgreementWithCustomer(bool transferAgreementWithCustomer)
        {
            var ordersClient = new OrderServiceClient();
            var order =
                ordersClient.GerOrderDataByOrderId(Caller.OrderId);
            
            var result = new OrderServiceClient().UpdateOrder(new UpdateOrderData
            {
                OrderId = Caller.OrderId,
                DeliveryInfo = new DeliveryInfo
                {
                    TransferAgreementWithCustomer = transferAgreementWithCustomer
                }
            }, null);

            if (result.ReturnCode == ReturnCode.Error)
            {
                throw new ApplicationException(string.Format("Не удалось обновить заказ: {0}", result.ErrorMessage));
            }
        }

        public GetSalesOrderResult CancelReserve()
        {
            using(var ordersClient = new OrderServiceClient())
            {
                var order = ordersClient.GerOrderDataByOrderId(Caller.OrderId);
                if(string.IsNullOrEmpty(order.ReserveInfo.WWSOrderId))
                {
                    throw new ApplicationException("Order not reserved");
                }
                return CancelReserve(order.ReserveInfo.SapCode, order.ReserveInfo.WWSOrderId);
            }
        }

        private GetSalesOrderResult CancelReserve(string sapCode, string orderNumber)
        {
            using(var wwsClient = new SalesServiceClient())
            {
                wwsClient.CancelSalesOrder(new OrderNumberInfo
                {
                    OrderNumber = orderNumber,
                    SapCode = sapCode
                });
                return wwsClient.GetSalesOrder(new OrderNumberInfo
                {
                    OrderNumber = orderNumber,
                    SapCode = sapCode
                });
            }
        }

        public GetSalesOrderResult ToPayReserve()
        {
            using(var ordersClient = new OrderServiceClient())
            {
                var order = ordersClient.GerOrderDataByOrderId(Caller.OrderId);
                if(string.IsNullOrEmpty(order.ReserveInfo.WWSOrderId))
                {
                    throw new ApplicationException("Order not reserved");
                }
                return ToPayReserve(order.ReserveInfo.SapCode, order.ReserveInfo.WWSOrderId);
            }
        }

        private GetSalesOrderResult ToPayReserve(string sapCode, string orderNumber)
        {
            using(var wwsClient = new SalesServiceClient())
            {
                return wwsClient.GetSalesOrder(new OrderNumberInfo()
                {
                    OrderNumber = string.Format("{0}_Команда_КОплате", orderNumber),
                    SapCode = sapCode
                });
            }
        }

        public GetSalesOrderResult PrepaidReserve()
        {
            using(var ordersClient = new OrderServiceClient())
            {
                var order = ordersClient.GerOrderDataByOrderId(Caller.OrderId);
                if(string.IsNullOrEmpty(order.ReserveInfo.WWSOrderId))
                {
                    throw new ApplicationException("Order not reserved");
                }
                return PrepaidReserve(order.ReserveInfo.SapCode, order.ReserveInfo.WWSOrderId);
            }
        }

        private GetSalesOrderResult PrepaidReserve(string sapCode, string orderNumber)
        {
            using(var wwsClient = new SalesServiceClient())
            {
                return wwsClient.GetSalesOrder(new OrderNumberInfo()
                {
                    OrderNumber = string.Format("{0}_Команда_Предоплата", orderNumber),
                    SapCode = sapCode
                });
            }
        }

        public GetSalesOrderResult PayReserve()
        {
            using(var ordersClient = new OrderServiceClient())
            {
                var order = ordersClient.GerOrderDataByOrderId(Caller.OrderId);
                if(string.IsNullOrEmpty(order.ReserveInfo.WWSOrderId))
                {
                    throw new ApplicationException("Order not reserved");
                }
                return PayReserve(order.ReserveInfo.SapCode, order.ReserveInfo.WWSOrderId);
            }
        }

        private GetSalesOrderResult PayReserve(string sapCode, string orderNumber)
        {
            using(var wwsClient = new SalesServiceClient())
            {
                return wwsClient.GetSalesOrder(new OrderNumberInfo()
                {
                    OrderNumber = string.Format("{0}_Команда_Оплатить", orderNumber),
                    SapCode = sapCode
                });
            }
        }

        public GetSalesOrderResult TransformReserve()
        {
            using(var ordersClient = new OrderServiceClient())
            {
                var order = ordersClient.GerOrderDataByOrderId(Caller.OrderId);
                if(string.IsNullOrEmpty(order.ReserveInfo.WWSOrderId))
                {
                    throw new ApplicationException("Order not reserved");
                }
                return TransformReserve(order.ReserveInfo.SapCode, order.ReserveInfo.WWSOrderId);
            }
        }

        private GetSalesOrderResult TransformReserve(string sapCode, string orderNumber)
        {
            using(var wwsClient = new SalesServiceClient())
            {
                return wwsClient.GetSalesOrder(new OrderNumberInfo()
                {
                    OrderNumber = string.Format("{0}_Команда_Трансформировать", orderNumber),
                    SapCode = sapCode
                });
            }
        }

        public GerOrderDataResult GetOrder()
        {
            using(var ordersClient = new OrderServiceClient())
            {
                var order = ordersClient.GerOrderDataByOrderId(Caller.OrderId);
                return order;
            }
        }

        public GerOrderStatusHistoryResult GerOrderStatusHistory()
        {
            using (var ordersClient = new OrderServiceClient())
            {
                return ordersClient.GetOrderStatusHistoryByOrderId(Caller.OrderId);
            }
        }

        public void AssertOrderRejected(string rejectReason)
        {
            var order = GetOrder();
            Assert.AreEqual(InternalOrderStatus.Rejected,order.OrderInfo.StatusInfo.Status);
            var status = GerOrderStatusHistory().OrderStatusHistory.OrderBy(osh => osh.UpdateDate).Last();
            Assert.AreEqual(rejectReason, status.ReasonText);
        }

        public void AssertOrderRejectedByCustomer(string rejectReason)
        {
            var order = GetOrder();
            Assert.AreEqual(InternalOrderStatus.RejectedByCustomer, order.OrderInfo.StatusInfo.Status);
            var status = GerOrderStatusHistory().OrderStatusHistory.OrderBy(osh=>osh.UpdateDate).Last();
            Assert.AreEqual(rejectReason, status.ReasonText);
        }

        /// <summary>
        /// Проверить, находится ли заказ в статусе "Order is closed"
        /// </summary>
        public void AssertOrderClosed()
        {
            var order = GetOrder();
            Assert.AreEqual(InternalOrderStatus.Closed, order.OrderInfo.StatusInfo.Status);
        }

        // Продление времени ожидания оплаты резерва
        public void ProlongReserve(int minutes=1)
        {
            var ordersClient = new OrderServiceClient();
            var order =
                ordersClient.GerOrderDataByOrderId(Caller.OrderId);

            var updateOrderResult = ordersClient.UpdateOrder(new UpdateOrderData
            {
                OrderId = Caller.OrderId,
                ExpirationDate = order.OrderInfo.ExpirationDate.Value.Add(TimeSpan.FromMinutes(minutes))
            }, null);
            if (updateOrderResult.ReturnCode == ReturnCode.Error) throw new Exception(updateOrderResult.ErrorMessage);

            Caller.ProlongReserve();
        }

        // Подтверждение заказа из ОрдерМониторинга
        public void ApproveOrder()
        {
            Caller.CallApprove();
        }

        // Апрув задачи на обработку инцидента из LIPS заказа из ОрдерМониторинга
        public void FixOrder()
        {
            throw new NotImplementedException();
        }

        #region LIPS

        /// <summary>
        /// Эмулирует ответ от LIPS на команду DeliverOrder
        /// </summary>
        /// <param name="done">Если true - результат Done, иначе Failed</param>
        public void LipsSendDeliverResponse(bool done=true)
        {
            using (var client=new SmartStart2CashOrderProcessingServiceClient())
            {
                client.SendCommandDeliverOrderDeliverOrderResponseContinue(new ReceiverCommandResponseRequest
                {
                    TicketId = Caller.TicketId,
                    ReceiverCommandResponse = new ReceiverCommandResponse
                    {
                        SerializedData = JsonConvert.SerializeObject(new MMS.Cloud.Commands.LIPS.DeliverOrderResponse()
                        {
                            Result = done ? MMS.Cloud.Commands.LIPS.Result.Done : MMS.Cloud.Commands.LIPS.Result.Failed,
                            Incidents = new HashSet<MMS.Cloud.Commands.LIPS.Incident>(),
                            LogisticRequestId = Guid.NewGuid().ToString()
                        })
                    }
                });
            }
        }

        /// <summary>
        /// Эмулирует ответ от LIPS на команду TransferOrder
        /// </summary>
        /// <param name="done">Если true - результат Done, иначе Failed</param>
        public void LipsSendTransferResponse(bool done = true)
        {
            using (var client = new SmartStart2CashOrderProcessingServiceClient())
            {
                client.SendCommandTransferGoodsTransferGoodsResponseContinue(new ReceiverCommandResponseRequest
                {
                    TicketId = Caller.TicketId,
                    ReceiverCommandResponse = new ReceiverCommandResponse
                    {
                        SerializedData = JsonConvert.SerializeObject(new MMS.Cloud.Commands.LIPS.TransferGoodsResponse
                        {
                            Result = done ? MMS.Cloud.Commands.LIPS.Result.Done : MMS.Cloud.Commands.LIPS.Result.Failed,
                            Incidents = new HashSet<MMS.Cloud.Commands.LIPS.Incident>(),
                            LogisticRequestId = Guid.NewGuid().ToString()
                        })
                    }
                });
            }
        }

        /// <summary>
        /// Отправить команду ProcessingInfo от LIPS
        /// </summary>
        /// <param name="status">Статус. По умолчанию - Shipped</param>
        /// <param name="incidents">Список инцидентов</param>
        public void LipsSendTransferShippedProcessingInfoResponse(string status = "Shipped", ICollection<MMS.Cloud.Commands.FORD.Incident> incidents = null)
        {
            using (var client = new SmartStart2CashOrderProcessingServiceClient())
            {
                client.ReceiveCommandDeliverOrderProcessingInfoContinue(new SenderCommandRequest
                {
                    TicketId = Caller.TicketId,
                    Command = new ReceiverCommand
                    {
                        SerializedData =
                            JsonConvert.SerializeObject(new MMS.Cloud.Commands.FORD.DeliverOrderProcessingInfo
                            {
                                LogisticRequestId = Guid.NewGuid().ToString(),
                                CustomerOrder=Caller.OrderId,
                                Status = status,
                                StatusDateTime = DateTime.Today,
                                Incidents = incidents ?? new Collection<MMS.Cloud.Commands.FORD.Incident>()
                            }),
                        CommandKey = new Key
                        {
                            CorrelationId = Guid.NewGuid().ToString(),
                            Id = Guid.NewGuid().ToString()
                        },      
                        RelatedEntityId=Caller.OrderId,
                        CreatorSystemId="LIPS",
                        CommandName = "DeliverOrderProcessingInfo",
                        ClientCreatedDate = DateTime.UtcNow,
                        ResponseTypeName = "",
                        TypeName = "",
                    },
                });
            }
        }

        #endregion

    }
}