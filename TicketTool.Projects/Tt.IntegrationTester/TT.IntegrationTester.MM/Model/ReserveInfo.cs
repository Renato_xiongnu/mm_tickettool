﻿namespace TT.IntegrationTester.MM.Model
{
    public class ReserveInfo
    {
        public string OrderNumber { get; set; }

        public string SapCode { get; set; }
    }
}
