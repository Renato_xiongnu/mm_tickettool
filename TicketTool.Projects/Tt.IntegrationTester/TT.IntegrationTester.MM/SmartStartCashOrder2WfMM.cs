﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TT.IntegrationTester.MM.Proxy.WfSmartStart2;

namespace TT.IntegrationTester.MM
{
    public class SmartStartCashOrder2WfMM : WfMm
    {
        protected override object GetWfService()
        {
            return new SmartStart2CashOrderProcessingServiceClient();
        }
    }
}
