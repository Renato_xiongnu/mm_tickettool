﻿using TT.IntegrationTester.MM.Proxy.WfSocialCardOrder;

namespace TT.IntegrationTester.MM
{
    public class SocialCardOrderWfMm : WfMm
    {
        protected override object GetWfService()
        {
            return new SocialCardOrderProcessingServiceClient();
        }
    }
}
