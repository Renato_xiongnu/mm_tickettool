﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace TicketTool.Projects.AppFabric.WorkflowManagement
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IWorkflowManagement
    {

        [OperationContract]
        void SuspendWorkflow(Guid workflowId);

        [OperationContract]
        void ResumeWorkflow(Guid workflowId);

        [OperationContract]
        void TerminateWorkflow(Guid workflowId);

        [OperationContract]
        void CancelWorkflow(Guid workflowId);

        [OperationContract]
        void DeleteWorkflow(Guid workflowId);

        [OperationContract]
        Guid GetWorkflowId(string orderId);

        [OperationContract]
        WorkflowTO GetWorkflow(string orderId);

    }

    [DataContract]
    public class WorkflowTO
    {
        [DataMember]
        public string WorkflowId { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public bool IsSuspended { get; set; }

        [DataMember]
        public string Exception { get; set; }
    }

}
