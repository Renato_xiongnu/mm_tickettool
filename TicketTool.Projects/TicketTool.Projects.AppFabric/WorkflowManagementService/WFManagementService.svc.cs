﻿using log4net;
using Microsoft.ApplicationServer.StoreManagement.Control;
using Microsoft.ApplicationServer.StoreManagement.Query;
using Microsoft.ApplicationServer.StoreManagement.Sql.Control;
using Microsoft.ApplicationServer.StoreManagement.Sql.Query;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace TicketTool.Projects.AppFabric.WorkflowManagement
{
    public class WFManagementService : IWorkflowManagement
    {
        private static readonly string _defaultName = ConfigurationManager.AppSettings["defaultName"];    // ZZT
        private static readonly string _path = ConfigurationManager.AppSettings["defaultPath"];           // "/WF/ZztOrders.xamlx";

        private static readonly NameValueCollection _connection = new NameValueCollection { { "connectionString", ConfigurationManager.ConnectionStrings["AppFabricPersistenceSql"].ConnectionString } };
        private static readonly string _connectionString = ConfigurationManager.ConnectionStrings["AppFabricPersistenceSql"].ConnectionString;
        private  readonly SqlInstanceControlProvider _controlProvider;
        private static ILog _logger;

        public WFManagementService()
        {            
            if (_logger == null)
            {
                log4net.Config.XmlConfigurator.Configure();
                _logger = LogManager.GetLogger(typeof(WFManagementService));
            }

            _controlProvider = new SqlInstanceControlProvider();
            _controlProvider.Initialize(_defaultName, _connection);            
        }

        public void SuspendWorkflow(Guid workflowId)
        {
            ExecuteCommand(workflowId, CommandType.Suspend);
        }

        public void ResumeWorkflow(Guid workflowId)
        {
            ExecuteCommand(workflowId, CommandType.Resume);
        }

        public void TerminateWorkflow(Guid workflowId)
        {
            ExecuteCommand(workflowId, CommandType.Terminate);
        }

        public void CancelWorkflow(Guid workflowId)
        {
            ExecuteCommand(workflowId, CommandType.Cancel);
        }

        public void DeleteWorkflow(Guid workflowId)
        {
            ExecuteCommand(workflowId, CommandType.Delete);
        }


        public WorkflowTO GetWorkflow(string orderId)
        {
            WorkflowTO result = null;
            try
            {
                var workflowId = GetWorkflowId(orderId);

                var instance = GetInstance(workflowId);

                result = new WorkflowTO()
                {
                    WorkflowId = instance.InstanceId.ToString(),
                    Status = instance.InstanceStatus.ToString(),
                    Exception = instance.ExceptionMessage,
                    IsSuspended = instance.InstanceStatus == InstanceStatus.Suspended
                };
            }
            catch(Exception err)
            {
                result = new WorkflowTO()
                {
                    Exception = err.Message
                };
            }
            return result;
        }

        public Guid GetWorkflowId(string orderId)
        {
            string query = @"Select WorkflowId FROM [TicketTool.Workflow].[DeadWorkflow] where OrderId = @orderId";

            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                var command = new SqlCommand(query, connection);
                command.Parameters.Add(new SqlParameter("@orderId", orderId));

                var reader = command.ExecuteReader();

                try
                {
                    if (reader.HasRows && reader.Read())
                    {
                        var result = reader.GetFieldValue<Guid>(0);

                        return result;
                    }
                    else
                        throw new ApplicationException(String.Format("No workflow for order {0} is found", orderId));
                }
                finally
                {
                    reader.Close();
                }
            }

        }

        #region Protected and private methods

        private InstanceInfo GetInstance(Guid instanceId)
        {
            InstanceInfo instance = null;

            var provider = new SqlInstanceQueryProvider();
            provider.Initialize(_defaultName, _connection);

            var query = provider.CreateInstanceQuery();

            var asyncResult = query.BeginExecuteQuery(new InstanceQueryExecuteArgs { InstanceId = new List<Guid> { instanceId }, Count = 1 }, new TimeSpan(0, 1, 0), null, null);                        

            var result = query.EndExecuteQuery(asyncResult).ToList();

            if (result != null)
                instance = result.FirstOrDefault();

            return instance;

        }

        private void ExecuteCommand(Guid instanceId, CommandType commandType)
        {
            _logger.DebugFormat("--- Starting executing command {0}, instance {1}", commandType.ToString(), instanceId);

            var control = _controlProvider.CreateInstanceControl();
            var instance = GetInstance(instanceId);
            if (instance != null)
            {
                var command = new InstanceCommand { CommandType = commandType, InstanceId = instanceId, ServiceIdentifier = instance.HostInfo.HostMetadata };
                command.ServiceIdentifier["VirtualPath"] = _path;

                try
                {
                    var commandAsyncResult = control.CommandSend.BeginSend(command, new TimeSpan(0, 1, 0), null, null);
                    control.CommandSend.EndSend(commandAsyncResult);

                    _logger.DebugFormat("--- Command {0}, instance {1} successfully executed", commandType.ToString(), instanceId);

                }
                catch (Exception err)
                {
                    _logger.Error(String.Format("Error executing command {0}, instance {1}", commandType.ToString(), instanceId), err);
                    throw;
                }
            }
        }


        //private Guid GetWorkflowIdByTicket(string ticketId)
        //{

        //}


        #endregion
    }
}
