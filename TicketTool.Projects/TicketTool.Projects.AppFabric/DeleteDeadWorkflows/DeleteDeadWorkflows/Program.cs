﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TicketTool.Projects.AppFabric.DeleteDeadWorkflows
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            log4net.Config.XmlConfigurator.Configure();

            if (Environment.UserInteractive)
            {

                new WorkflowsCreatedPolling().Start();

                new TicketOrderStatePolling().Start();

                Console.WriteLine("Working...");
                //Console.ReadKey();

                while (true)
                {
                    Thread.Sleep(10000);
                    Console.Write(".");
                }

            }

            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
                        { 
                            new DeleteDeadWorkflow() 
                        };
                ServiceBase.Run(ServicesToRun);

            }                       

        }
    }
}
