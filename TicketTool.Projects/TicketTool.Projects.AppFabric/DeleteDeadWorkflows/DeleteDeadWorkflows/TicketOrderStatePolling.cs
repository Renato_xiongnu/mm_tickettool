﻿using log4net;
//using Orders.Backend.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using TicketTool.Projects.AppFabric.DeleteDeadWorkflows.OrderServiceProxy;
using TicketTool.Projects.AppFabric.DeleteDeadWorkflows.TicketToolProxy;
using TicketTool.Projects.AppFabric.DeleteDeadWorkflows.WorkflowManagementProxy;


namespace TicketTool.Projects.AppFabric.DeleteDeadWorkflows
{
    /// <summary>
    /// Производит опрос ticket'ов и и order'ов на предмет определения их текущего сосотяния 
    /// и соответствия этого состояния условиям прибивания Workflow
    /// </summary>
    public class TicketOrderStatePolling
    {
        private static ILog _logger = LogManager.GetLogger(typeof(TicketOrderStatePolling).Name);

        private Timer _timer;

        public TicketOrderStatePolling()
        {
            _timer = new Timer(AppSettings.Default.TicketOrderStatePollingInterval * 1000); 
            _timer.Elapsed += (a, e) =>
            {
                _timer.Stop();

                try
                {
                    Execute();
                }
                catch (Exception err)
                {
                    _logger.Error("TicketOrderStatePolling failed", err);
                }
                finally
                {
                    if (Environment.UserInteractive)
                        Console.Write("#");
                }

                _timer.Start();
            };

        }

        public void Start()
        {
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }

        public void Execute()
        {
            _logger.Info("***** Start TicketOrderStatePolling Execute *****");
            //int chunksize = 100;    // TODO: Move to configuration

            // Получить chunk записей WF->TT->Order            
            using (var ctx = new AppFabricPersistence())
            {
                var created = DeadWorkflowStatus.Created.ToString();
                //var wflist = ctx.DeadWorkflow.Where(wf => wf.Status == created).Take(chunksize).ToList();

                var wflist = (
                                from df in ctx.DeadWorkflow
                                from instance in ctx.Instances
                                where df.WorkflowId == instance.Id && df.Status == created
                                select df
                              )
                             .ToList();

                _logger.InfoFormat("{0} workflow instances detected", wflist.Count);

                wflist.ForEach(wf =>
                {

                    TicketTO ticket = null;

                    try
                    {
                        // Получить ticket
                        ticket = GetTicket(wf.TicketId);

                        if (ticket != null)
                        {
                            if (ticket.IsClosed())
                            {
                                // Для закрытого ticket'а прибить WF
                                Kill(wf.WorkflowId);
                                wf.Status = DeadWorkflowStatus.Processed.ToString();
                            }
                            else
                            {
                                // Получить order, проверить его состояние
                                var order = GetOrder(wf.OrderId);

                                if (order != null && order.IsReadyToClose())
                                {
                                    _logger.DebugFormat("Closing ticket {0} for order #{1}. Order status'{2}'", wf.TicketId, wf.OrderId, order.OrderInfo.OrderState);

                                    // Закрыть ticket
                                    CloseTicket(wf.TicketId);

                                    // Прибить WF
                                    Kill(wf.WorkflowId);

                                    wf.Status = DeadWorkflowStatus.Processed.ToString();
                                }
                            }

                            // Проставить статус для записи WF->TT->Order
                            if (ctx.Entry<DeadWorkflow>(wf).Property<string>(d => d.Status).IsModified)
                            {
                                ctx.SaveChanges();

                                _logger.DebugFormat("Workflow status is updated to {0}", wf.Status);
                            }
                        }
                    }
                    catch (Exception err)
                    {
                        _logger.Error(String.Format("Error processing Workflow {0}, ticket {1}, order {2}", wf.WorkflowId, wf.TicketId, wf.OrderId), err);
                    }
                });
            }

            _logger.Info("***** Finish TicketOrderStatePolling Execute *****");
        }


        #region Protected and private methods

        private TicketTO GetTicket(string ticketId)
        {
            using (var service = new TicketToolServiceClient())
            {
                var result = service.GetTicketInfo(ticketId);

                return result;
            }
        }

        private void Kill(Guid workflowId)
        {
            using (var service = new WorkflowManagementClient())
            {
                service.DeleteWorkflow(workflowId);
            }

            _logger.DebugFormat("Workflow {0} is deleted", workflowId);
        }

        private CustomerOrder GetOrder(string orderId)
        {
            using (var service = new OrderServiceClient())
            {
                var result = service.GetOrder(orderId);

                return result.CustomerOrder;
            }
        }

        private void CloseTicket(string ticketId)
        {
            using (var service = new TicketToolServiceClient())
            {
                service.CloseTicket(ticketId);
            }

            _logger.DebugFormat("Ticket {0} is closed", ticketId);

        }

        #endregion
    }

    public static class Extensions
    {
        // TODO: вынести в конфигурацию
        private static List<string> _list = new List<string>() { "доставлен", "отмена", "отказ", "удален", "удален (тестирование)" };

        public static bool IsClosed(this TicketTO ticket)
        {
            var result = ticket.CloseDate.HasValue;

            return result;
        }
       

        public static bool IsReadyToClose(this CustomerOrder order)
        {

            return _list.Contains(order.OrderInfo.OrderState);
        }
    }
}
