﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace TicketTool.Projects.AppFabric.DeleteDeadWorkflows
{
    public partial class DeleteDeadWorkflow : ServiceBase
    {
        public DeleteDeadWorkflow()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            new WorkflowsCreatedPolling().Start();

            new TicketOrderStatePolling().Start();

        }

        protected override void OnStop()
        {
            base.OnStop();
        }
    }
}
