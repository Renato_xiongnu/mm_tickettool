﻿using Orders.Backend.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Data.Entity.Core.Objects;
using log4net;

namespace TicketTool.Projects.AppFabric.DeleteDeadWorkflows
{
    /// <summary>
    /// Обеспечивает получение номеров заказов и задач из сохраненных в AppFabric WF
    /// </summary>
    public class WorkflowsCreatedPolling
    {
        private static ILog _logger = LogManager.GetLogger(typeof(WorkflowsCreatedPolling).Name);

        private Timer _timer;

        public WorkflowsCreatedPolling()
        {
            _timer = new Timer(AppSettings.Default.WorkflowsPollingInterval * 1000); 

            _timer.Elapsed += (a, e) => {
                _timer.Stop();
                
                try
                {
                    UpdateOrderAndTicketNumbers();
                }
                catch (Exception err)
                {
                    _logger.Error("WorkflowsCreatedPolling failed", err);
                }
                finally
                {
                    if (Environment.UserInteractive)
                        Console.Write("*");
                }

                _timer.Start();
            };
        }

        public void Start()
        {
            _timer.Start();
        }


        public void Stop()
        {
            _timer.Stop();
        }

        public void Execute()
        {
            UpdateOrderAndTicketNumbers();
        }


        private void UpdateOrderAndTicketNumbers()
        {

            _logger.Info("----- Start Updating Order & Ticket Numbers -----");

            //var newInstances = GetInstanceUidList();

            List<Instance> instances = null;

            using (var ctx = new AppFabricPersistence())
            {
                ctx.Database.CommandTimeout = 600;
                
                
                //instances = ctx.Instances
                //                   .Where(i => newInstances.Contains(i.Id)
                //                        && (i.ComplexDataProperties != null)
                //                        /*&& (i.ServiceDeploymentId == 4)*/)
                //                   .ToList();

                instances = (from df in ctx.DeadWorkflow
                            from i in ctx.Instances
                            where df.WorkflowId == i.Id 
                                    && (i.ComplexDataProperties != null)
                                    && (df.OrderId == null) 
                                    && (df.TicketId == null) 
                                    && (df.Status == null)
                            select i).ToList();
            }

            _logger.InfoFormat("{0} new workflow instances created", instances.Count);

            var serializer = new GZipInstanceSerializer();

            Parallel.ForEach(instances, new ParallelOptions() { MaxDegreeOfParallelism = 1 }, // TODO: Move to configuration
                (instance) =>
                {
                    var df = new DeadWorkflow()
                        {
                            WorkflowId = instance.Id,
                            //CreateDate = DateTime.Now,
                            UpdateDate = DateTime.Now
                        };
                    try
                    {
                        var data = serializer.DeserializePropertyBag(instance.ComplexDataProperties);
                        var activityExecutor = data.First().Value;
                        var ticketId = activityExecutor.ExecuteProperty<string>("SerializedProgramMapping.SerializedInstanceLists[0].SingleItem.SerializedEnvironment.SerializedLocations[3].SerializedValue");
                        var order = activityExecutor.ExecuteProperty<CustomerOrder>("SerializedProgramMapping.SerializedInstanceLists[0].SingleItem.SerializedChildren.SingleItem.SerializedEnvironment.SerializedLocations[1].SerializedValue");

                        if (order != null)
                        {
                            var orderId = order.OrderId;

                            df.OrderId = orderId;
                            df.TicketId = ticketId;
                            df.Status = DeadWorkflowStatus.Created.ToString();
                        }
                        
                    }
                    catch (Exception err)
                    {                        
                        df.Status = DeadWorkflowStatus.SerializationError.ToString();
                        df.Message = err.Message;
                    }
                     
                    try
                    {
                        using (var ctx = new AppFabricPersistence())
                        {
                            ctx.DeadWorkflow.Attach(df);
                            //ctx.Entry<DeadWorkflow>(df).State = System.Data.Entity.EntityState.Modified;
                            ctx.Entry<DeadWorkflow>(df).Property<string>(d => d.OrderId).IsModified = true;
                            ctx.Entry<DeadWorkflow>(df).Property<string>(d => d.TicketId).IsModified = true;
                            ctx.Entry<DeadWorkflow>(df).Property<string>(d => d.Status).IsModified = true;                            
                            ctx.Entry<DeadWorkflow>(df).Property<string>(d => d.Message).IsModified = true;
                            ctx.Entry<DeadWorkflow>(df).Property<DateTime?>(d => d.UpdateDate).IsModified = true;
                            ctx.SaveChanges();
                        }
                    }
                    catch (Exception err)
                    {
                        _logger.Error("Error saving workflow info. ", err);
                    }

                });

            _logger.Info("----- Finish Updating Order & Ticket Numbers -----");
        }

        /// <summary>
        /// Получает список вновь созданных WF
        /// </summary>
        /// <returns></returns>
        private List<System.Guid> GetInstanceUidList()
        {
            using (var ctx = new AppFabricPersistence())
            {
                var list = ctx.DeadWorkflow.Where(df => (df.OrderId == null) && (df.TicketId == null) && (df.Status == null)).Select(df => df.WorkflowId).ToList();

                return list;
            }
        }


    }

    public enum DeadWorkflowStatus
    {
        Created,
        Processed,
        SerializationError
    }
}
