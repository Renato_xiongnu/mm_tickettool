﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Linq;

namespace TicketTool.Projects.AppFabric.DeleteDeadWorkflows
{
    internal class InstanceSerializer
    {
        private readonly NetDataContractSerializer _serializer;

        public InstanceSerializer()
        {
            this._serializer = new NetDataContractSerializer();
        }

        public Dictionary<XName, object> DeserializePropertyBag(byte[] serializedValue)
        {
            using (var memoryStream = new MemoryStream(serializedValue))
                return this.DeserializePropertyBag(memoryStream);
        }

        public object DeserializeValue(byte[] serializedValue)
        {
            using (var memoryStream = new MemoryStream(serializedValue))
                return this.DeserializeValue(memoryStream);
        }

        public ArraySegment<byte> SerializePropertyBag(Dictionary<XName, object> value)
        {
            using (var memoryStream = new MemoryStream(4096))
            {
                this.SerializePropertyBag(memoryStream, value);
                return new ArraySegment<byte>(memoryStream.GetBuffer(), 0, Convert.ToInt32(memoryStream.Length));
            }
        }

        public ArraySegment<byte> SerializeValue(object value)
        {
            using (var memoryStream = new MemoryStream(4096))
            {
                this.SerializeValue(memoryStream, value);
                return new ArraySegment<byte>(memoryStream.GetBuffer(), 0, Convert.ToInt32(memoryStream.Length));
            }
        }

        protected virtual Dictionary<XName, object> DeserializePropertyBag(Stream stream)
        {
            //using (XmlDictionaryReader binaryReader = XmlDictionaryReader.CreateBinaryReader(stream, XmlDictionaryReaderQuotas.Max))
            //{
            //    if (binaryReader.Read())
            //        File.WriteAllText(String.Format("C:\\Users\\Igor\\Documents\\test.xml"), binaryReader.ReadInnerXml());
            //}
            using (XmlDictionaryReader binaryReader = XmlDictionaryReader.CreateBinaryReader(stream, XmlDictionaryReaderQuotas.Max))
            {                
                var dictionary = new Dictionary<XName, object>();
                if (binaryReader.ReadToDescendant("Property"))
                {
                    do
                    {
                        binaryReader.Read();
                        var keyValuePair = (KeyValuePair<XName, object>) this._serializer.ReadObject(binaryReader);
                        dictionary.Add(keyValuePair.Key, keyValuePair.Value);
                    } while (binaryReader.ReadToNextSibling("Property"));
                }
                return dictionary;
            }
        }

        protected virtual object DeserializeValue(Stream stream)
        {
            using (XmlDictionaryReader binaryReader = XmlDictionaryReader.CreateBinaryReader(stream, XmlDictionaryReaderQuotas.Max))
                return this._serializer.ReadObject(binaryReader);
        }

        protected virtual void SerializePropertyBag(Stream stream, Dictionary<XName, object> propertyBag)
        {
            using (XmlDictionaryWriter binaryWriter = XmlDictionaryWriter.CreateBinaryWriter(stream, null, null, false))
            {
                binaryWriter.WriteStartElement("Properties");
                foreach (KeyValuePair<XName, object> keyValuePair in propertyBag)
                {
                    binaryWriter.WriteStartElement("Property");
                    this._serializer.WriteObject(binaryWriter, keyValuePair);
                    binaryWriter.WriteEndElement();
                }
                binaryWriter.WriteEndElement();
            }
        }

        protected virtual void SerializeValue(Stream stream, object value)
        {
            using (XmlDictionaryWriter binaryWriter = XmlDictionaryWriter.CreateBinaryWriter(stream, null, null, false))
                this._serializer.WriteObject(binaryWriter, value);
        }
    }
}

