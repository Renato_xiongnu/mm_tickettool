﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace ZZTIntegration.Web.Controllers
{
    public class ErrorsController : Controller
    {
        public ActionResult General(Exception exception)
        {
            return View("Error", exception);
        }

        public ActionResult Http404()
        {
            return View("Error", new Exception("Not found"));
        }

        public ActionResult Http403()
        {
            return View("Error", new Exception("Forbidden"));
        }

        public ActionResult Error(string message)
        {
            return View("Error", new Exception(message));
        }
    }
}
