﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using ZZTIntegration.Web.Common;

namespace ZZTIntegration.Web.Controllers
{
    public class CoachController : Controller
    {
        public JsonResult GetMetroStations(string cityId)
        {
            var data = CoachDataLoader.MetroStations(cityId);
            return Json(data.Select(m => new { key = m.Value, value = m.Value }), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCities(string term)
        {
            var cities = CoachRepository.CitiesRepository.Where(m => m.Name.ToLower().StartsWith(term.ToLower())).Take(10).OrderBy(m => m.Name).ToList();
            return Json(cities.Select(m => new { label = m.Name, value = m.Name, code = m.CityId }), JsonRequestBehavior.AllowGet);
        }
    }
}
