﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicketTool.Integration.ZZT.Storage;
using ZZTIntegration.Web.Common;
using ZZTIntegration.Web.Models;

namespace ZZTIntegration.Web.Controllers
{
    public class HomeController : Controller
    {
        [NonAction]
        public OrderItem PrepareOrder(OrderItem order)
        {
            ModelState.Clear();
            order.ErrorMessage = "";
            order.IsApproved = false;
            order = OrderProcessing.ValidateOrder(order);
            return order;
        }

        [HttpGet]
        public ActionResult Order(string orderId, string ro, string fi)
        {
            bool orderReadonly = !String.IsNullOrEmpty(ro) && ro == "1" ? true : false;
            bool orderFixedMethod = !String.IsNullOrEmpty(fi) && fi == "1" ? true : false;
            var ord = OrderProcessing.GetOrder(orderId);
            ord = PrepareOrder(ord);
            ord.ReadOnly = orderReadonly;
            ord.IsFixed = orderFixedMethod;
            if (ord.ReadOnly)
                return View("Display", ord);
            return View("Index", ord);
        }

        // from php callcenter
        public ActionResult BaseOrder(string data)
        {
            OrderItem order = new OrderItem();
            try
            {
                order = JsonConvert.DeserializeObject<OrderItem>(data);
                foreach (var art in order.BasketArticles)
                {
                    art.Type = ArticleType.Product;
                }
                order = PrepareOrder(order);
                order.ExternalInterface = true;
            }
            catch(Exception ex)
            {
            }
            return View("Index", order);
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View("Index", new OrderItem
            {
                CampaignId = ConfigurationManager.AppSettings["CampaignName"],
                ConsumerId = ConfigurationManager.AppSettings["ConsumerName"],
            });
        }

        [HttpGet]
        public ActionResult Result(string orderId, string boNumber, bool externalInterface)
        {
            ResultItem result = new ResultItem 
            {
                BoOrderId = boNumber,
                OrderId = orderId,
                IsExternalInterface = externalInterface
            };
            if(result.IsExternalInterface && !String.IsNullOrEmpty(result.BoOrderId) && !String.IsNullOrEmpty(result.OrderId))
            {
                return Redirect(String.Format(ConfigurationManager.AppSettings["CallCenterFinishPage"], result.OrderId, result.BoOrderId));
            }
            return View(result);
        }

        public ActionResult GetBoNumber(string orderId)
        {
            if (String.IsNullOrEmpty(orderId))
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            try
            {
                var order = OrderProcessing.GetOrder(orderId);
                return Json(order.BoOrderId, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GeneratedOrder()
        {
            var order = OrderProcessing.GenerateOrder();
            order = OrderProcessing.ValidateOrder(order);
            return View("Index", order);
        }

        [HttpPost]
        public ActionResult Index(OrderItem item)
        {
            item.ChangeErrors(ModelState);
            item = OrderProcessing.ValidateOrder(item);
            if (ModelState.IsValid && item.IsApproved && item.HasArticles && item.AllArticles.All(m => !m.IsDataWrong && !m.IsWrong))
            {
                if (!String.IsNullOrEmpty(item.DeliveryAddress.CityId))
                {
                    var city = CoachDataLoader.GetCity(item.DeliveryAddress.CityId);
                    if (city != null)
                    {
                        item.DeliveryAddress.City = city.Name;
                        item.DeliveryAddress.RegionId = city.RegionId;
                    }
                }
                if (item.DeliveryPrice.DeliveryZone != "-1")
                {
                    item.DeliveryPrice.DeliveryPrice = item.DeliveryPrice.Zones.First(m => m.Id == item.DeliveryPrice.DeliveryZone).Price;
                }
                if (String.IsNullOrEmpty(item.OrderId))
                {
                    var createResult = OrderProcessing.CreateOrder(item);
                    if (createResult.ReturnCode == RouterLink.ReturnCode.Ok)
                    {
                        item.OrderId = createResult.OrderId;
                        return RedirectToAction("Result", new { orderId = createResult.OrderId, externalInterface = item.ExternalInterface });
                    }
                    else
                    {
                        item.ErrorMessage = "Произошла ошибка сохранения заказа: " + createResult.ErrorMessage;
                        return View("Index", item);
                    }
                }
                else
                {
                    var updateResult = OrderProcessing.UpdateOrder(item);
                    if (updateResult.ReturnCode != OrderLink.ReturnCode.Ok)
                    {
                        item.ErrorMessage = "Произошла ошибка обновления данных заказа: " + updateResult.ErrorMessage;
                        return View("Index", item);
                    }
                }
            }
            return View("Index", item);
        }

        [HttpPost]
        public ActionResult Validate(OrderItem item)
        {
            item = PrepareOrder(item);
            return View("Index", item);
        }

        #region [ArticleActions]

        [HttpPost]
        public ActionResult AddArticle(OrderItem item)
        {
            item.NewArticles.Add(new ArticleLineItem{IsWrong = true, Type = ArticleType.Product});
            item = PrepareOrder(item);
            return View("Index", item);
        }

        [HttpPost]
        public ActionResult DeleteNewArticle(OrderItem item, int index)
        {
            item.NewArticles.RemoveAt(index);
            item = PrepareOrder(item);
            return View("Index", item);
        }

        [HttpPost]
        public ActionResult DeleteBasketArticle(OrderItem item, int index)
        {
            item.BasketArticles.RemoveAt(index);
            item = PrepareOrder(item);
            return View("Index", item);
        }

        [HttpPost]
        public ActionResult AddQty(OrderItem item, int index)
        {
            item.BasketArticles[index].Qty++;
            item = PrepareOrder(item);
            return View("Index", item);
        }

        [HttpPost]
        public ActionResult SubQty(OrderItem item, int index)
        {
            item.BasketArticles[index].Qty--;
            if (item.BasketArticles[index].Qty == 0)
                item.BasketArticles.RemoveAt(index);
            item = PrepareOrder(item);
            return View("Index", item);
        }

        #endregion
    }
}
