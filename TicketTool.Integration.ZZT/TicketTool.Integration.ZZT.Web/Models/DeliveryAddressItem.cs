﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using TicketTool.Integration.ZZT.Storage;
using ZZTIntegration.Web.Common;

namespace ZZTIntegration.Web.Models
{
    public class DeliveryAddressItem
    {
        [Display(Name = "Индекс")]
        public string PostalCode { get; set; }

        [Display(Name = "Область")]
        public string Region { get; set; }

        [Display(Name = "Район")]
        public string District { get; set; }

        [Display(Name = "Станция метро")]
        public string MetroStation { get; set; }

        [Required(ErrorMessage = "Поле '{0}' не заполнено.")]
        [Display(Name = "Полный адрес")]
        public string Street { get; set; }

        [Display(Name = "Дом")]
        public string House { get; set; }

        [Display(Name = "Корпус")]
        public string Housing { get; set; }

        [Display(Name = "Строение")]
        public string Building { get; set; }

        [Display(Name = "Квартира")]
        public string Flat { get; set; }

        [Display(Name = "Подъезд")]
        public string Entrance { get; set; }

        [Display(Name = "Код подъезда")]
        public string EntranceCode { get; set; }

        [Display(Name = "Этаж")]
        public string Floor { get; set; }

        [Display(Name = "Грузовой лифт")]
        public bool HasElevator { get; set; }

        [Display(Name = "Комментарий заказчика")]
        public string Additional { get; set; }


        [Required(ErrorMessage = "Поле '{0}' не заполнено.")]
        [Display(Name = "Страна")]
        public string Country
        {
            get
            {
                return "Россия";
            }
        }

        //[Required(ErrorMessage = "Поле '{0}' не заполнено.")]
        [Display(Name = "Город")]
        public string CityId { get; set; }

        public string RegionId { get; set; }

        [Required(ErrorMessage = "Поле '{0}' не заполнено.")]
        [Display(Name = "Город")]
        public string City { get; set; }


        [Display(Name = "Адрес доставки")]
        public string SummaryAddress
        {
            get 
            {
                List<string> entries = new List<string>();
                if(!String.IsNullOrEmpty(PostalCode))
                {
                    entries.Add(PostalCode);
                }
                if (!String.IsNullOrEmpty(Region))
                {
                    entries.Add(Region + (Regex.IsMatch(Region, @"(о|О)бласть") ? "" : " область"));
                }
                if (!String.IsNullOrEmpty(District))
                {
                    entries.Add(District + (Regex.IsMatch(District, @"(р|Р)айон") ? "" : " район"));
                }
                if (!String.IsNullOrEmpty(Street))
                {
                    entries.Add("ул. " + Street);
                }
                if (!String.IsNullOrEmpty(House))
                {
                    entries.Add("д. " + House);
                }
                if (!String.IsNullOrEmpty(Housing))
                {
                    entries.Add("корп. " + Housing);
                }
                if (!String.IsNullOrEmpty(Building))
                {
                    entries.Add("стр. " + Building);
                }
                if (!String.IsNullOrEmpty(Entrance))
                {
                    entries.Add("подъезд. " + Entrance);
                }
                if (!String.IsNullOrEmpty(Floor))
                {
                    entries.Add("эт. " + Floor);
                }
                if (!String.IsNullOrEmpty(Flat))
                {
                    entries.Add("кв. " + Flat);
                }
                if (!String.IsNullOrEmpty(EntranceCode))
                {
                    entries.Add("код. " + EntranceCode);
                }
                return String.Join(", ", entries);
            }
        }
    }
}