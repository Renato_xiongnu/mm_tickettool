﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Web;
using ZZTIntegration.Web.Common;

namespace ZZTIntegration.Web.Models
{
    public class ArticleLineItem
    {
        [Display(Name="Артикул")]
        public string ArticleNum { get; set; }

        [Display(Name = "Цена")]
        public decimal Price { get; set; }

        [Display(Name = "Количество")]
        public int Qty { get; set; }

        public OrderLink.ReviewItemState State { get; set; }

        [Display(Name = "Статус")]
        public string StateName
        {
            get
            {
                switch (State)
                {
                    case OrderLink.ReviewItemState.Empty:
                        return "";
                    case OrderLink.ReviewItemState.IncorrectPrice:
                        return "Неправильная цена";
                    case OrderLink.ReviewItemState.NotFound:
                        return "Не найден";
                    case OrderLink.ReviewItemState.Reserved:
                        return "Отложен";
                    case OrderLink.ReviewItemState.ReservedDisplayItem:
                        return "Отложен витринный экземпляр";
                    case OrderLink.ReviewItemState.ReservedPart:
                        return "Отложен частично";
                    default:
                        return "";
                }
            }
        }

        [Display(Name = "Комментарий менеджера")]
        public string Comment { get; set; }
        public long LineId { get; set; }

        public bool IsWrong { get; set; }

        public bool IsAbsent { get; set; }

        public bool IsDataWrong
        {
            get
            {
                if (String.IsNullOrEmpty(ArticleNum) || Price <= 0 || (Qty <= 0 && Type != ArticleType.Service))
                    return true;
                return false;
            }
        }

        public string AvailablePlaces { get; set; }

        public ArticleType Type { get; set;  }
        //[Display(Name = "Артикул доставки")]
        //public bool IsDelivery { get; set; }

        public string ErrorMessage
        {
            get
            {
                if (IsDataWrong)
                {
                    if (String.IsNullOrEmpty(ArticleNum))
                        return "Не задан номер артикула";
                    //if (Price <= 0)
                    //    return "Не задана цена";
                    if (Qty <= 0)
                        return "Не задано количество";
                }
                else if (IsWrong)
                {
                    return "Неправильный артикул";
                }
                return "";
            }
        }

        public bool ReadOnly { get; set; }

        public string Description { get; set; }

        public string ProductLink
        {
            get
            {
                return String.Format(ConfigurationManager.AppSettings["CallCenterProductPage"], ArticleNum);
            }
        }

        public void FillAdditionalInfo(ArticleDataLink.SMItemInfo item)
        {
            Description = item.ShortDescription;
        }
    }
}