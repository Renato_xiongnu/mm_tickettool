﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZZTIntegration.Web.Common;

namespace ZZTIntegration.Web.Models
{
    public class OrderItem
    {
        public OrderItem()
        {
            DeliveryAddress = new DeliveryAddressItem();
            DeliveryInfo = new DeliveryInfoItem();
            Customer = new CustomerItem();
            DeliveryPrice = new DeliveryPriceItem();
            BasketArticles = new List<ArticleLineItem>();
            NewArticles = new List<ArticleLineItem>();
        }

        [UIHint("Controls/DeliveryAddress")]
        public DeliveryAddressItem DeliveryAddress { get; set; }

        [UIHint("Controls/DeliveryInfo")]
        public DeliveryInfoItem DeliveryInfo { get; set; }

        [UIHint("Controls/DeliveryPrice")]
        public DeliveryPriceItem DeliveryPrice { get; set; }

        [UIHint("Controls/Customer")]
        public CustomerItem Customer { get; set; }

        [UIHint("Controls/BasketArticleLine")]
        public List<ArticleLineItem> BasketArticles { get; set; }

        [UIHint("Controls/ArticleLine")]
        public List<ArticleLineItem> NewArticles { get; set; }

        public IEnumerable<ArticleLineItem> AllArticles
        {
            get
            {
                return BasketArticles.Concat(NewArticles);
            }
        }

        public bool ExternalInterface { get; set; }

        public bool ReadOnly { get; set; }
        public bool IsFixed { get; set; }

        [Display(Name = "Номер заказа")]
        public string OrderId { get; set; }

        [Display(Name = "Номер резерва")]
        public string WWSOrderId { get; set; }

        [Display(Name = "Номер заказа БО")]
        public string BoOrderId { get; set; }

        [Display(Name = "Название акции")]
        public string CampaignId { get; set; }

        [Display(Name = "Потребитель акции")]
        public string ConsumerId { get; set; }

        public bool HasArticles
        {
            get
            {
                return AllArticles.Count() > 0;
            }
        }

        public bool IsApproved { get; set; }

        public ErrorObject ErrorType { get; set; }

        public string ErrorMessage { get; set; }

        public decimal TotalPrice
        {
            get
            {
                return BasketArticles.Sum(m => m.Price * m.Qty)/*+ DeliveryP.Sum(m => m.Price)*/;
            }
        }

        public void ChangeErrors(ModelStateDictionary errors)
        {
            //TODO: refact later
            if (!Customer.InvoiceNotSame)
            {
                errors.Remove("Customer.RecipientName");
            }
            if (DeliveryPrice.DeliveryZone != "-1")
            {
                errors.Remove("DeliveryPrice.DeliveryPrice");
            }
            if (DeliveryInfo.DeliveryType == "20")
            {
                errors.Remove("DeliveryAddress.Street");
                errors.Remove("DeliveryAddress.City");
                errors.Remove("DeliveryPrice.DeliveryPrice");
            }
            else if (DeliveryInfo.DeliveryType == "2")
            {
                errors.Remove("DeliveryInfo.DeliveryPlace");
            }
        }
    }
}