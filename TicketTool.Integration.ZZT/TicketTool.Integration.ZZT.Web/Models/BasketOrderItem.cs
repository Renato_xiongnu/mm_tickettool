﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ZZTIntegration.Web.Models
{
    public class BasketOrderItem
    {
        public BasketOrderItem() 
        {
            Articles = new List<ArticleLineItem>();
            Customer = new CustomerItem();
            DeliveryInfo = new DeliveryInfoItem();
        }

        [UIHint("Controls/BasketArticleLine")]
        public List<ArticleLineItem> Articles { get; set; }

        [UIHint("Controls/BasketCustomer")]
        public CustomerItem Customer { get; set; }

        [UIHint("Controls/DeliveryInfo")]
        public DeliveryInfoItem DeliveryInfo { get; set; }

        public decimal TotalPrice
        {
            get
            {
                return Articles.Sum(m => m.Price * m.Qty);
            }
        }
    }
}