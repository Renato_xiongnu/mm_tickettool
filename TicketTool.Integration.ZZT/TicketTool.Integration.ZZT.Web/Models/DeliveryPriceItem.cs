﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ZZTIntegration.Web.Common;

namespace ZZTIntegration.Web.Models
{
    public class DeliveryPriceItem
    {
        public DeliveryPriceItem()
        {
            DeliveryZone = "-1";
        }

        public bool IsWrong { get; set; }
        public string ErrorMessage { get; set; }

        [Required(ErrorMessage = "Поле '{0}' не заполнено.")]
        [Display(Name = "Стоимость доставки")]
        public decimal? DeliveryPrice { get; set; }

        [Display(Name = "Зона доставки")]
        public string DeliveryZone { get; set; }

        public List<BasketServiceLink.GlobalZoneInfo> Zones
        {
            get
            {
                return ZonesProcessing.GetZones();
            }
        }
    }
}