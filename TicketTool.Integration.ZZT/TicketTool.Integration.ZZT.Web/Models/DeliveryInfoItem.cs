﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ZZTIntegration.Web.Models
{
    public class DeliveryInfoItem
    {
        [Display(Name = "Выбранный способ получения")]
        public string SelectedTypeName { get; set; }

        public string SelectedTypeCode { get; set; }

        [Display(Name = "Выбранная точка самовывоза")]
        public string SelectedPlaceName { get; set; }

        public string SelectedPlaceCode { get; set; }

        [Display(Name = "Способ получения")]
        [Required(ErrorMessage = "Поле '{0}' не заполнено.")]
        public string DeliveryType { get; set; }

        public string DeliveryTypeName { get; set; }

        [Display(Name = "Точка самовывоза")]
        [Required(ErrorMessage = "Поле '{0}' не заполнено.")]
        public string DeliveryPlace { get; set; }

        public string DeliveryPlaceName { get; set; }

        public string DeliveryPlaceSapCode 
        {
            get
            {
                if (String.IsNullOrEmpty(DeliveryPlace))
                    return null;
                return DeliveryPlace.Split(';').First();
            }
        }

        public string DeliveryPlaceId 
        {
            get
            {
                if (String.IsNullOrEmpty(DeliveryPlace))
                    return null;
                var data = DeliveryPlace.Split(';');
                return data.Length > 1 ? data[1] : "";
            }
        }

        [Display(Name = "Дата получения")]
        [Required(ErrorMessage = "Поле '{0}' не заполнено.")]
        public DateTime? DeliveryDate { get; set; }

        [Display(Name = "Время доставки")]
        public string DeliveryTime { get; set; }

        public Dictionary<string, string> ActualTypes { get; set; }

        public BasketServiceLink.PickupPoint[] PlacesGeo { get; set; }

        public object PlacesGeoJson
        {
            get
            {
                if (PlacesGeo == null)
                    return null;
                return JsonConvert.SerializeObject(PlacesGeo.Select(m => new { 
                    latitude = m.Latitude, 
                    longitude = m.Longitude, 
                    sapCode = m.SapCode,
                    name = m.Name,
                    address = m.Address,
                    description = m.WorkTime
                }).ToList());
                 
            }
        }

        public Dictionary<string, string> Types
        {
            get
            {
                BasketServiceLink.BasketServiceClient client = new BasketServiceLink.BasketServiceClient();
                var result = client.GetDeliveryTypes();
                if (result.Result == BasketServiceLink.ResponseResult.OK)
                {
                    return result.DeliveryTypes;
                }
                return new Dictionary<string, string>();
            }
        }

        public Dictionary<string, string> Places { get; set; }
    }
}