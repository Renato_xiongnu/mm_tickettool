﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZZTIntegration.Web.Common
{
    public static class ZonesProcessing
    {
        public static List<BasketServiceLink.GlobalZoneInfo> GetZones()
        {
            BasketServiceLink.BasketServiceClient client = new BasketServiceLink.BasketServiceClient();
            var response = client.GetDeliveryZones();
            List<BasketServiceLink.GlobalZoneInfo> zones = new List<BasketServiceLink.GlobalZoneInfo>();
            if (response.Result == BasketServiceLink.ResponseResult.OK)
            {
                zones.AddRange(response.Zones);
            }
            zones.Add(new BasketServiceLink.GlobalZoneInfo { Id = "-1", Name = "Не задана" });
            return zones;
        }
    }
}