﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZZTIntegration.Web.Common
{
    public enum ErrorObject
    {
        Undefined = 4,
        Article = 0,
        Global = 1,
        Internal = 2
    }

    public enum ArticleType
    {
        None = 0,
        Product = 1,
        Service = 2
    }
}