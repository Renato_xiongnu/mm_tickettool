﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZZTIntegration.Web.Models;

namespace ZZTIntegration.Web.Common
{
    public static class OrderProcessing
    {
        public static string GenerateOrderId()
        {
            BasketServiceLink.BasketServiceClient client = new BasketServiceLink.BasketServiceClient();
            return client.GenerateOrderId(ConfigurationManager.AppSettings["GenerationBaseId"]);
        }

        public static OrderItem GenerateOrder()
        {
            return new OrderItem
            {
                CampaignId = ConfigurationManager.AppSettings["CampaignName"],
                ConsumerId = ConfigurationManager.AppSettings["ConsumerName"],
                BasketArticles = new List<ArticleLineItem> 
                { 
                    new ArticleLineItem
                    {
                        Description = "",
                        ArticleNum = "1161629",
                        Price = 5199,
                        Qty = 1,
                        Type = ArticleType.Product
                    },
                    new ArticleLineItem
                    {
                        Description = "",
                        ArticleNum = "1188761",
                        Price =  27999,
                        Qty = 1,
                        Type = ArticleType.Product
                    }
                }
            };
        }

        public static OrderItem ValidateOrder(OrderItem order)
        {
            var response = CallValidate(order);
            if (!String.IsNullOrEmpty(order.DeliveryInfo.DeliveryType))
            {
                order.DeliveryInfo.SelectedTypeCode = order.DeliveryInfo.DeliveryType;
                order.DeliveryInfo.SelectedTypeName = order.DeliveryInfo.DeliveryTypeName;
            }
            if (!String.IsNullOrEmpty(order.DeliveryInfo.DeliveryPlace))
            {
                order.DeliveryInfo.SelectedPlaceCode = order.DeliveryInfo.DeliveryPlace;
                order.DeliveryInfo.SelectedPlaceName = order.DeliveryInfo.DeliveryPlaceName;
            }
            foreach (var article in order.AllArticles)
            {
                article.IsAbsent = false;
            }
            if (response.Result == BasketServiceLink.ResponseResult.OK)
            {
                var places = response.Places.ToDictionary(m => m.SapCode, z => z.Name);

                var types = response.Places.SelectMany(m => m.Types).Distinct().ToDictionary(m => m.Key, z => z.Value);
                // ugly
                order.DeliveryInfo.DeliveryPlace = order.DeliveryInfo.SelectedPlaceCode;
                order.DeliveryInfo.Places = places;
                order.DeliveryInfo.DeliveryType = order.DeliveryInfo.SelectedTypeCode;
                order.DeliveryInfo.ActualTypes = types;
                order.DeliveryInfo.PlacesGeo = response.Places;

                order.IsApproved = true;
                order.ErrorMessage = "";
                ApproveArticles(order, response.Articles, order.DeliveryInfo.SelectedTypeCode, order.DeliveryInfo.SelectedPlaceCode);
            }
            else
            {
                order.IsApproved = false;
                order.ErrorType = (ErrorObject)Enum.ToObject(typeof(ErrorObject), response.ErrorObjectType);
                switch (response.ErrorObjectType)
                {
                    case BasketServiceLink.ErrorObject.Article:
                        order.ErrorMessage = response.ErrorObjectId + ": " + response.ErrorMessage;
                        var article = order.AllArticles.Where(m => m.ArticleNum == response.ErrorObjectId).FirstOrDefault();
                        if (article != null)
                        {
                            article.IsWrong = true;
                        }
                        break;
                    case BasketServiceLink.ErrorObject.Internal:
                        order.ErrorMessage = "Артикульный состав содержит одну или несколько некорректных позиций.";
                        break;
                    case BasketServiceLink.ErrorObject.Global:
                        order.ErrorMessage = response.ErrorMessage;
                        break;
                }
            }
            return order;
        }

        public static RouterLink.CreateOrderOperationResult CreateOrder(OrderItem order)
        {
            RouterLink.OrderServiceClient client = new RouterLink.OrderServiceClient();
            var articles = order.BasketArticles.Concat(order.NewArticles);
            var response = client.CreateOrder(order.Customer.ToCustomerContract(), order.ToCreateDataContract(), new RouterLink.DeliveryInfo { }, articles.Select(m => m.ToArticleContract()).ToArray());
            return response;
        }

        public static OrderItem GetOrder(string orderId)
        {
            OrderLink.OrderServiceClient client = new OrderLink.OrderServiceClient();
            var data = client.GerOrderDataByOrderId(orderId);
            if (data.ReturnCode == OrderLink.ReturnCode.Error)
                throw new InvalidOperationException(orderId + ": " + data.ErrorMessage);
            OrderItem ord = data.ToOrderModel();
            return ord;
        }

        public static OrderLink.UpdateOrderOperationResult UpdateOrder(OrderItem order)
        {
            OrderLink.OrderServiceClient client = new OrderLink.OrderServiceClient();
            var articles = order.BasketArticles.Concat(order.NewArticles);
            var response = client.UpdateOrder(new OrderLink.UpdateOrderData
            {
                OrderId = order.OrderId,
                ClientData = order.Customer.ToOrderCustomerContract(),
                ZZTData = new OrderLink.ZZTHeaderData
                {
                    DeliveryAddress = order.DeliveryAddress.ToOrderContract(order.Customer),
                    DeliveryInfo = order.DeliveryInfo.ToOrderContract(order.DeliveryPrice),
                    RecipientName = order.Customer.RecipientName
                }
            }, new OrderLink.ReserveInfo
            {
                ReserveLines = articles.Select(m => m.ToOrderReserveContract()).ToArray()
            });
            return response;
        }

        private static BasketServiceLink.ValidateResponse CallValidate(OrderItem order)
        {
            List<ArticleLineItem> articlesToRemove = new List<ArticleLineItem>();
            foreach (var newArticle in order.NewArticles)
            {
                var basketArticle = order.BasketArticles.FirstOrDefault(m => m.ArticleNum == newArticle.ArticleNum);
                if (basketArticle != null)
                {
                    basketArticle.Qty += newArticle.Qty;
                    articlesToRemove.Add(newArticle);
                }
            }
            foreach(var removeArticle in articlesToRemove)
            {
                order.NewArticles.Remove(removeArticle);
            }
            var articles = order.BasketArticles.Concat(order.NewArticles);
            var client = new BasketServiceLink.BasketServiceClient();
            var basketValidation = client.Validate(new BasketServiceLink.ValidateRequest
            {
                Articles = articles.Select(m => new BasketServiceLink.ArticleData
                {
                    Id = m.ArticleNum,
                    Price = m.Price,
                    Qty = m.Qty,
                    CampaignId = order.CampaignId,
                    Type = (BasketServiceLink.ArticleType)Enum.Parse(typeof(BasketServiceLink.ArticleType), m.Type.ToString())
                }).ToArray(),
                Consumer = order.ConsumerId
            });
            return basketValidation;
        }

        private static OrderItem ApproveArticles(OrderItem order, BasketServiceLink.ArticleData[] approvedArticles, string selectedTypeCode, string selectedPlaceCode)
        {
            var allArticles = order.BasketArticles.Concat(order.NewArticles);
            foreach (var item in allArticles)
            {
                var approvedArticle = approvedArticles.Where(m => m.Id == item.ArticleNum).FirstOrDefault();
                if (approvedArticle != null)
                {
                    item.IsWrong = false;
                    if (item.Price == 0)
                        item.Price = approvedArticle.Price;
                    item.Description = approvedArticle.Name;
                    var sapCodes = approvedArticle.Places.Select(m => m.SapCode).ToList();
                    var types = approvedArticle.DeliveryTypes;
                    if(!String.IsNullOrEmpty(selectedTypeCode) && !types.Contains(selectedTypeCode))
                    {
                        item.IsAbsent = true;
                    }
                    if (selectedTypeCode != "2" && !String.IsNullOrEmpty(selectedPlaceCode) && !sapCodes.Contains(selectedPlaceCode))
                    {
                        item.IsAbsent = true;
                    }
                }
            }
            var newBasketArticles = order.NewArticles.Where(m => !String.IsNullOrEmpty(m.Description) && !m.IsWrong && !m.IsDataWrong).ToList();
            order.BasketArticles.AddRange(newBasketArticles);
            foreach (var article in newBasketArticles)
            {
                order.NewArticles.Remove(article);
            }
            return order;
        }
    }
}