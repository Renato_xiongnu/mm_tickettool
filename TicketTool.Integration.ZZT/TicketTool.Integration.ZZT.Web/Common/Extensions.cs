﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZZTIntegration.Web.Models;

namespace ZZTIntegration.Web.Common
{
    public static class Extensions
    {
        public static RouterLink.CreateOrderData ToCreateDataContract(this OrderItem order)
        {
            var sapCode = order.DeliveryInfo.DeliveryType == "2" ? "R930" : order.DeliveryInfo.DeliveryPlaceSapCode;
            return new RouterLink.CreateOrderData
            {
                OrderId = OrderProcessing.GenerateOrderId(),
                StoreInfo = new RouterLink.StoreInfo { SapCode = sapCode },
                ExternalSystem = RouterLink.ExternalSystem.ZZT,
                OrderSource = RouterLink.OrderSource.MetroCallCenter,
                PaymentType = RouterLink.PaymentType.Cash,
                ZZTData = new RouterLink.ZZTHeaderData
                { 
                    CampaignId = order.CampaignId,
                    ConsumerId = order.ConsumerId,
                    DeliveryAddress = order.DeliveryAddress.ToContract(order.Customer),
                    DeliveryInfo = order.DeliveryInfo.ToContract(order.DeliveryPrice),
                    RecipientName = order.Customer.RecipientName
                }
            };
        }

        public static RouterLink.ClientData ToCustomerContract(this CustomerItem item)
        {
            return new RouterLink.ClientData
            {
                Birthday = item.Birthday,
                Name = String.IsNullOrEmpty(item.CustomerName) ? "" : item.CustomerName,
                Surname = String.IsNullOrEmpty(item.CustomerSurname) ? "" : item.CustomerSurname,
                Gender = item.Gender,
                LastName = item.CustomerLastName,
                Email = item.Email,
                Fax = item.Fax,
                Phone = item.MainPhone,
                Phone2 = item.OptionalPhones,
                ZZTCustomerId = item.CustomerId
            };
        }

        public static RouterLink.ZZTDeliveryAddress ToContract(this DeliveryAddressItem deliveryAddress, CustomerItem customer)
        {
            return new RouterLink.ZZTDeliveryAddress
            {
                Additional = deliveryAddress.Additional,
                Building = deliveryAddress.Building,
                District = deliveryAddress.District,
                Entrance = deliveryAddress.Entrance,
                EntranceCode = deliveryAddress.EntranceCode,
                Flat = deliveryAddress.Flat,
                Floor = deliveryAddress.Floor,
                HasElevator = deliveryAddress.HasElevator,
                House = deliveryAddress.House,
                Housing = deliveryAddress.Housing,
                MetroStation = deliveryAddress.MetroStation,
                PostalCode = deliveryAddress.PostalCode,
                Region = deliveryAddress.Region,
                Street = deliveryAddress.Street,
                City = deliveryAddress.City,
                CityId = deliveryAddress.CityId,
                RegionId = deliveryAddress.RegionId,
                Country = deliveryAddress.Country
            };
        }

        public static RouterLink.ZZTDeliveryInfo ToContract(this DeliveryInfoItem deliveryInfo, DeliveryPriceItem price)
        {
            return new RouterLink.ZZTDeliveryInfo
            {
                DeliveryDate = deliveryInfo.DeliveryDate,
                DeliveryPlaceSapCode = deliveryInfo.DeliveryPlaceSapCode,
                DeliveryPlaceId = deliveryInfo.DeliveryPlaceId,
                DeliveryTime = deliveryInfo.DeliveryTime,
                DeliveryType = deliveryInfo.DeliveryType,
                DeliveryPrice = price.DeliveryPrice
            };
        }

        public static RouterLink.ArticleData ToArticleContract(this ArticleLineItem item)
        {
            return new RouterLink.ArticleData
            { 
                ArticleNum = item.ArticleNum.ToString(),
                Price = item.Price,
                Qty = item.Qty,
                Title = item.Description
            };
        }

        public static RouterLink.ReserveLine ToReserveContract(this ArticleLineItem item)
        {
            return new RouterLink.ReserveLine
            {
                LineId = item.LineId,
                ArticleData = new RouterLink.ArticleData 
                {
                    ArticleNum = item.ArticleNum.ToString(),
                    Price = item.Price,
                    Qty = item.Qty
                }
            };
        }
    }
}