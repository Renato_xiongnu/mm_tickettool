﻿using System.Collections.Generic;
using System.Linq;
using TicketTool.Integration.ZZT.Storage;
using ZZTModel = MMS.Storage.DataModel.ZZT;

namespace ZZTIntegration.Web.Common
{
    public static class CoachDataLoader
    {
        public static ZZTModel.City GetCity(string cityId)
        {
            var storage = StorageFactory.Create();
            var city = storage.GetEntities<ZZTModel.City>("bycityId", new string[] { cityId }).FirstOrDefault();
            return city;
        }

        public static List<ZZTModel.City> AucCities()
        {
            var storage = StorageFactory.Create();
            var cities = storage.GetEntities<ZZTModel.City>("byenabled", new string[] { }).ToList();
            return cities;
        }

        public static Dictionary<string, string> AllCities()
        {
            var storage = StorageFactory.Create();
            var cities = storage.GetEntities<ZZTModel.City>("byenabled", new string[] { }).ToList();
            var moscowIndex = cities.FindIndex(m => m.Name == "Москва");
            var item = cities[moscowIndex];
            cities[moscowIndex] = cities[0];
            cities[0] = item;
            return cities.ToDictionary(m => m.CityId, m => m.Name);
        }

        public static Dictionary<string, string> MoscowPickupPoints()
        {
            var moscowCity = GetMoscowCity();
            if (moscowCity == null)
                return null;
            return PickupPoints(moscowCity.CityId);
        }

        public static Dictionary<string, string> MoscowMetroStations()
        {
            var moscowCity = GetMoscowCity();
            if (moscowCity == null)
                return null;
            return MetroStations(moscowCity.CityId);
        }

        public static Dictionary<string, string> PickupPoints(string cityId)
        {
            var storage = StorageFactory.Create();
            var pickupPoints = storage.GetEntities<ZZTModel.PickUpPoint>("bycityId", new string[] { cityId });
            return pickupPoints.ToDictionary(m => m.PickUpPointId, m => m.Name);
        }

        public static Dictionary<string, string> MetroStations(string cityId)
        {
            var storage = StorageFactory.Create();
            var metroStations = storage.GetEntities<ZZTModel.MetroStation>("bycityId", new string[] { cityId });
            return metroStations.OrderBy(m => m.Name).ToDictionary(m => m.MetroStationId, m => m.Name);
        }

        public static ZZTModel.City GetMoscowCity()
        {
            var storage = StorageFactory.Create();
            var moscowCity = storage.GetEntities<ZZTModel.City>("byName", new string[] { "Москва" }).FirstOrDefault();
            return moscowCity;
        }
    }
}