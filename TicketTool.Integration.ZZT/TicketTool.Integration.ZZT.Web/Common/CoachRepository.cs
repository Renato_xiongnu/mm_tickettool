﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZZTModel = MMS.Storage.DataModel.ZZT;

namespace ZZTIntegration.Web.Common
{
    public static class CoachRepository
    {
        public static List<ZZTModel.City> CitiesRepository
        {
            get
            {
                if (HttpContext.Current.Session["CitiesRepository"] == null)
                {
                    lock (HttpContext.Current.Session.SyncRoot)
                    {
                        if (HttpContext.Current.Session["CitiesRepository"] == null)
                            HttpContext.Current.Session["CitiesRepository"] = CoachDataLoader.AucCities();
                    }
                }
                return HttpContext.Current.Session["CitiesRepository"] as List<ZZTModel.City>;
                //return CoachDataLoader.AucCities();
            }
        }
    }
}