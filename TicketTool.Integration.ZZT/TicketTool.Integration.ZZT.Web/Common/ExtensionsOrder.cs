﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZZTIntegration.Web.Models;

namespace ZZTIntegration.Web.Common
{
    public static class ExtensionsOrder
    {

        public static OrderItem ToOrderModel(this OrderLink.GerOrderDataResult order)
        {
            var item = new OrderItem();
            item.BasketArticles = order.ReserveInfo.ReserveLines.Select(m => m.ToOrderModel()).ToList();
            if (order.ZZTData != null)
            {
                item.OrderId = order.OrderInfo.OrderId;
                item.CampaignId = order.ZZTData.CampaignId;
                item.ConsumerId = order.ZZTData.ConsumerId;
                item.DeliveryAddress = order.ZZTData.DeliveryAddress.ToOrderModel();
                item.DeliveryInfo = order.ZZTData.DeliveryInfo.ToOrderInfo();
                item.DeliveryPrice = order.ZZTData.DeliveryInfo.ToOrderPrice();
                item.Customer = order.ClientData.ToOrderModel(order.ZZTData);
                item.WWSOrderId = order.OrderInfo.WWSOrderId;
                item.BoOrderId = order.ZZTData.ZztOrderId;
            }
            return item;
        }

        public static CustomerItem ToOrderModel(this OrderLink.ClientData customer, OrderLink.ZZTHeaderData header)
        {
            var cust = new CustomerItem
            {
                Birthday = customer.Birthday,
                CustomerName = customer.Name,
                CustomerSurname = customer.Surname,
                CustomerLastName = customer.LastName,
                Email = customer.Email,
                Fax = customer.Fax,
                MainPhone = customer.Phone,
                OptionalPhones = customer.Phone2,
                CustomerId = customer.ZZTCustomerId
            };
            if (!String.IsNullOrEmpty(header.RecipientName))
            {
                cust.RecipientName = header.RecipientName;
                cust.InvoiceNotSame = true;
            }
            else
            {
                cust.InvoiceNotSame = false;
            }
            return cust;
        }

        public static DeliveryPriceItem ToOrderPrice(this OrderLink.ZZTDeliveryInfo deliveryInfo)
        {
            var result = new DeliveryPriceItem();
            if(deliveryInfo != null)
            {
                 result.DeliveryPrice = deliveryInfo.DeliveryPrice;
                 var zone = result.Zones.SingleOrDefault(m => m.Price == deliveryInfo.DeliveryPrice);
                 if (zone != null)
                     result.DeliveryZone = zone.Id;
            };
            return result;
        }

        public static DeliveryInfoItem ToOrderInfo(this OrderLink.ZZTDeliveryInfo deliveryInfo)
        {
            return deliveryInfo == null ? new DeliveryInfoItem() : new DeliveryInfoItem
            {
                DeliveryDate = deliveryInfo.DeliveryDate,
                DeliveryPlace = deliveryInfo.DeliveryPlaceSapCode + (String.IsNullOrEmpty(deliveryInfo.DeliveryPlaceId) ? "" :  ";" + deliveryInfo.DeliveryPlaceId ),
                DeliveryTime = deliveryInfo.DeliveryTime,
                DeliveryType = deliveryInfo.DeliveryType
            };
        }

        public static DeliveryAddressItem ToOrderModel(this OrderLink.ZZTDeliveryAddress deliveryAddress)
        {
            return deliveryAddress == null ? new DeliveryAddressItem() : new DeliveryAddressItem
            {
                Additional = deliveryAddress.Additional,
                Building = deliveryAddress.Building,
                City = deliveryAddress.City,
                CityId = deliveryAddress.CityId,
                District = deliveryAddress.District,
                Entrance = deliveryAddress.Entrance,
                EntranceCode = deliveryAddress.EntranceCode,
                Flat = deliveryAddress.Flat,
                Floor = deliveryAddress.Floor,
                HasElevator = deliveryAddress.HasElevator.HasValue ? deliveryAddress.HasElevator.Value : false,
                House = deliveryAddress.House,
                Housing = deliveryAddress.Housing,
                MetroStation = deliveryAddress.MetroStation,
                PostalCode = deliveryAddress.PostalCode,
                Region = deliveryAddress.Region,
                Street = deliveryAddress.Street
            };
        }

        public static ArticleLineItem ToOrderModel(this OrderLink.ArticleData line)
        {
            return new ArticleLineItem
            {
                ArticleNum = line.ArticleNum,
                Price = line.Price,
                Qty = line.Qty,
                Description = line.ArticleNum
            };
        }

        public static ArticleLineItem ToOrderModel(this OrderLink.ReserveLine line)
        {
            return new ArticleLineItem
            {
                LineId = line.LineId,
                ArticleNum = line.ArticleData.ArticleNum,
                Price = line.ArticleData.Price,
                Qty = line.ArticleData.Qty,
                Description = line.Title,
                Type = ArticleType.Product,
                State = line.ReviewItemState,
                Comment = line.ArticleCondition
                
            };
        }

        public static OrderLink.ClientData ToOrderCustomerContract(this CustomerItem item)
        {
            return new OrderLink.ClientData
            {
                Birthday = item.Birthday,
                Name = item.CustomerName,
                Surname = item.CustomerSurname,
                Gender = item.Gender,
                LastName = item.CustomerLastName,
                Email = item.Email,
                Fax = item.Fax,
                Phone = item.MainPhone,
                Phone2 = item.OptionalPhones,
                ZZTCustomerId = item.CustomerId
            };
        }

        public static OrderLink.ZZTDeliveryAddress ToOrderContract(this DeliveryAddressItem deliveryAddress, CustomerItem customer)
        {
            return new OrderLink.ZZTDeliveryAddress
            {
                Additional = deliveryAddress.Additional,
                Building = deliveryAddress.Building,
                District = deliveryAddress.District,
                Entrance = deliveryAddress.Entrance,
                EntranceCode = deliveryAddress.EntranceCode,
                Flat = deliveryAddress.Flat,
                Floor = deliveryAddress.Floor,
                HasElevator = deliveryAddress.HasElevator,
                House = deliveryAddress.House,
                Housing = deliveryAddress.Housing,
                MetroStation = deliveryAddress.MetroStation,
                PostalCode = deliveryAddress.PostalCode,
                Region = deliveryAddress.Region,
                Street = deliveryAddress.Street,
                City = deliveryAddress.City,
                CityId = deliveryAddress.CityId,
                RegionId = deliveryAddress.RegionId,
                Country = deliveryAddress.Country
            };
        }

        public static OrderLink.ZZTDeliveryInfo ToOrderContract(this DeliveryInfoItem deliveryInfo, DeliveryPriceItem price)
        {
            return new OrderLink.ZZTDeliveryInfo
            {
                DeliveryDate = deliveryInfo.DeliveryDate,
                DeliveryPlaceSapCode = deliveryInfo.DeliveryPlaceSapCode,
                DeliveryPlaceId = deliveryInfo.DeliveryPlaceId,
                DeliveryTime = deliveryInfo.DeliveryTime,
                DeliveryType = deliveryInfo.DeliveryType
            };
        }

        public static OrderLink.ArticleData ToOrderArticleContract(this ArticleLineItem item)
        {
            return new OrderLink.ArticleData
            { 
                ArticleNum = item.ArticleNum.ToString(),
                Price = item.Price,
                Qty = item.Qty,
                Title = item.Description
            };
        }

        public static OrderLink.ReserveLine ToOrderReserveContract(this ArticleLineItem item)
        {
            return new OrderLink.ReserveLine
            {
                LineId = item.LineId,
                ArticleData = new OrderLink.ArticleData 
                {
                    ArticleNum = item.ArticleNum.ToString(),
                     Price = item.Price,
                     Qty = item.Qty
                }
            };
        }
    }
}