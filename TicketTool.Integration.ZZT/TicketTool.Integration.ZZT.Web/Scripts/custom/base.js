﻿function generateLoader(container, optWidth, optHeight) {
    var height = optHeight === undefined || optHeight === null ? $(container).height() : optHeight;
    var width = optWidth === undefined || optWidth === null ? $(container).width() : optWidth;
    if (height === 0) height = 50;
    if (width === 0) width = 50;
    return '<div style="text-align:center;width:' + width + 'px;height:' + height + 'px;"><img src="' + approot + '/Content/Images/ajax-loader.gif"></div>';
}


$(function () {
    //$.ajax({
    //    type: "post",
    //    url: 'http://orders.backend.mediasaturnrussia.ru/ru/Services.Metro.Proxy/Basket/CreateOrder',
    //    data: '{ "system_comment" : "Иван Павлович Колесов","comment_from_customer" : null,"sap_code" : "R930","consumer" :"metro","delivery" :{"address" : null,"delivery_type" : "20","pickup_place_sapcode" : "R202","pickup_place_id" : null,"delivery_price" : 0,"delivery_date" : "01.01.1970"},"articles" : [{"id":"1188761","qty":1,"price":28999.0,"type":"Product","title":"Apple iPhone 5 16Gb Black Смартфон","campaign_id":"5902_metro"}],"customer" : {"name" : null,"surname" : null,"phone" : 9169302391,"email" : "ivan.kolesov@gmail.com"}}',
    //    contentType: 'application/json',
    //    success: function (response) {
    //        console.log(response);
    //    }
    //});
    //$.ajax({
    //    type: "get",
    //    url: 'http://localhost/TicketTool.Integration.ZZT.Web/Home/BaseOrder',
    //    data: 'data={"CampaignId":"5902_metro","ConsumerId":"metro","BasketArticles":[{"ArticleNum":1203473,"Price":"16999","Qty":2},{"ArticleNum":1159934,"Price":"18999","Qty":"1"},{"ArticleNum":1188761,"Price":"28999","Qty":2}]}',
    //    //contentType: 'application/json',
    //    success: function (response) {
    //        console.log(response);
    //    }
    //});

    //$.ajax({
    //    type: "post",
    //    url: 'http://localhost/TicketTool.Integration.ZZT.Basket/Basket/CreateOrder',
    //    data: '{"system_comment":"\u0418\u0432\u0430\u043d \u041f\u0430\u0432\u043b\u043e\u0432\u0438\u0447 \u041a\u043e\u043b\u0435\u0441\u043e\u0432","comment_from_customer":null,"sap_code":"R930;1102","consumer":"metro","delivery":{"address":null,"delivery_type":"20","pickup_place_sapcode":"R930;1102","pickup_place_id":null,"delivery_price":"0","delivery_date":"01.01.1970"},"articles":[{"id":"1188761","price":28999,"qty":1,"title":"Apple iPhone 5 16Gb Black \u0421\u043c\u0430\u0440\u0442\u0444\u043e\u043d","type":"Product","campaign_id":"5902_metro"}],"customer":{"name":null,"surname":null,"phone":"+79169302384","email":"ivan.kolesov@gmail.com"}}',
    //    contentType: 'application/json',
    //    success: function (response) {
    //        console.log(response);
    //    }
    //});

    //$('select[id$="CityId"]').on('change', function () {
    //    var self = $(this);
    //    if (self.val() == "-1") {
    //        $('[id$="City"]').show();
    //    }
    //    else {
    //        $('[id$="City"]').hide();
    //    }
    //    var target = $('select[id$="MetroStation"]');
    //    target.attr('disabled', 'disabled');
    //    $.getJSON($.routeLinks.GetStations, { cityId: self.val() },
    //        function (response) {
    //            var savedValue = $('[id$="metroName"]').val();
    //            var options = '';
    //            if (response.length == 0) {
    //                target.parent().parent().hide();
    //            }
    //            else {
    //                target.parent().parent().show();
    //            }
    //            for (var i = 0; i < response.length; i++) {
    //                options += "<option value='" + response[i].key + "'>" + response[i].value + "</option>";
    //            }
    //            target.removeAttr('disabled').html(options);
    //            target.find('option').filter(function () {
    //                return $(this).text() == savedValue;
    //            }).prop('selected', true);
    //        });
    //});
    //$('select[id$="CityId"]').trigger('change');

    //$('[id$="DeliveryPrice"], [id$="UpliftPrice"]').on('keyup', function () {
    //    var firstPrice = $('[id$="DeliveryPrice"]').val();
    //    var secondPrice = $('[id$="UpliftPrice"]').val();
    //    if (secondPrice == '') secondPrice = 0;
    //    var total = parseFloat(firstPrice) + parseFloat(secondPrice);
    //    $('[id$="TotalPrice"]').val(total);
    //});

    //$('[id$="NeedUplift"]').on('click', function () {
    //    if ($(this).is(':checked')) {
    //        $('.deliveryUpliftBlock').show();
    //    }
    //    else {
    //        $('.deliveryUpliftBlock').hide();
    //        $('.deliveryUpliftBlock input').val('');
    //        $('.deliveryUpliftBlock input').trigger('keyup');
    //    }
    //});

    //$('[id$="Birthday"]').inputmask("d.m.y");
    //$('[id$="MainPhone"]').mask('+9(999)999-99-99', { placeholder: '_' });
    var $typeSelect = $('select[id$="DeliveryType"]');
    var $preservedType = $('[id$="SelectedTypeCode"]');
    var $preservedPlace = $('[id$="SelectedPlaceCode"]');
    var $placeSelect = $('select[id$="DeliveryPlace"]');
    if ($preservedType.val() != $typeSelect.val()) {
        $typeSelect.prop("selectedIndex", -1)
    }
    if ($preservedPlace.val() != $placeSelect.val()) {
        $placeSelect.prop("selectedIndex", -1)
    }
    $typeSelect.on('change', function () {
        $('.datepicker').datepicker('option', 'minDate', getMinDeliveryDate());
        $('[id$="DeliveryTypeName"]').val($(this).find(":selected").text());
        if ($(this).val() === "20") {
            $('.deliveryPlace').show();
            $('.deliveryPrice').hide();
            $('.deliveryAddress').hide();
        }
        else if ($(this).val() === "2") {
            $('.deliveryPlace').hide();
            $('.deliveryPrice').show();
            $('.deliveryAddress').show();
        }
        else {
            $('.deliveryPlace').hide();
            $('.deliveryPrice').hide();
            $('.deliveryAddress').hide();
        }
    });
    
    $placeSelect.on('change', function () {
        $('.datepicker').datepicker('option', 'minDate', getMinDeliveryDate());
        $('[id$="DeliveryPlaceName"]').val($(this).find(":selected").text());
    });

    $placeSelect.trigger('change');
    $typeSelect.trigger('change');
    if ($('input[id$="DeliveryZone"]:checked').val() == "-1") {
        $('.unknownPrice').show();
    }
    $('[id$="DeliveryZone"]').on('click', function () {
        if ($(this).val() == "-1") {
            $('.unknownPrice').show();
        }
        else {
            $('.unknownPrice').hide();
        }
    });

    $(".expandableIcon").on('click', function () {
        var container = $(this).parent();
        if (container.hasClass("show")) {
            $(this).removeClass("ui-icon-triangle-1-n").addClass("ui-icon-triangle-1-s");
            container.removeClass("show");
        }
        else {
            $(this).removeClass("ui-icon-triangle-1-s").addClass("ui-icon-triangle-1-n");
            container.addClass("show");
        }
    });

    //$('[id$="DeliveryZone"]').trigger('click');

    $(".autocompleteCity").autocomplete({
        source: $.routeLinks.AucCity,
        select: function (event, ui) {
            $('[id$="CityId"]').val(ui.item.code);
        },
        search: function (event, ui) {
            $('[id$="CityId"]').val('');
            //$(this).attr('disabled', 'disabled');
            var $loader = $(this).parent().find('.loader');
            $loader.html(generateLoader($loader, 20, 20));
        },
        response: function (event, ui) {
           // $(this).removeAttr('disabled', 'disabled');
            var $loader = $(this).parent().find('.loader');
            $loader.html('');
            if (ui.content.length == 1 && $(this).val().toLowerCase() == ui.content[0].value.toLowerCase())
            {
                $('[id$="CityId"]').val(ui.content[0].code);
                $(this).val(ui.content[0].value);
                $(this).autocomplete("close");
            }
        }
    });

    $('.datepicker').inputmask("d.m.y").datepicker({
        minDate: getMinDeliveryDate(),
        dateFormat: 'dd.mm.yy',
        buttonImage: approot + '/Content/Images/calendar.gif',
        buttonImageOnly: true,
        showOn: 'both'
    });
    $('input[type="checkbox"][id$="InvoiceNotSame"]').on('click', function () {
        if ($(this).is(':checked')) {
            $('.invoiceCustomerBlock').show();
        }
        else {
            $('.invoiceCustomerBlock').hide();
        }
    });
    $('.submitButton').on('click', function () {
        var href = $(this).attr('data-href');
        if (href !== null && href !== undefined) {
            $(this).closest('form').attr('action', href).submit();
        }
    });
    var additionalMasks = {
        phone: /[\d\.\, \-\+\(\)]/
    };
    $.extend($.fn.keyfilter.defaults.masks, additionalMasks);
    $('.keyfilter-phone').keyfilter($.fn.keyfilter.defaults.masks.phone);
});

function getMinDeliveryDate() {
    var correctDate = new Date();
    var typeId = $('select[id$="DeliveryType"]').val();
    var placeId = $('select[id$="DeliveryPlace"]').val();
    if ((typeId === "20" && placeId !== null && placeId.indexOf("R930") == 0) || typeId === "2") {
        var hours = correctDate.getHours();
        var minutes = correctDate.getMinutes();
        if (hours > 18 || (hours === 18 && minutes > 30))
            correctDate.setDate(correctDate.getDate() + 2);
        else
            correctDate.setDate(correctDate.getDate() + 1);
    }
    else if (typeId === "2") {

    }
    return correctDate;
}