﻿var pickupMap;
var currentLocationCollection;
$(function () {
    ymaps.ready(init);

    var pickupDialog = $('#pickupMapDialog');
    pickupDialog.find('.pickupAddress').on('keypress', function (e, args) {
        if (e.keyCode == $.ui.keyCode.ENTER) {
            pickupDialog.find('.pickupSubmit').click();
        }
    });
    var $descs = pickupDialog.find('.pointDescription');
    $descs.on('click', function () {
        $(this).find('.pickupRadio').prop('checked', true).trigger('change');
    });
    pickupDialog.find('.pickupRadio').on('change', function () {
        $descs.removeClass('selected');
        $(this).closest('.pointDescription').addClass('selected');
    });
    pickupDialog.find('.pickupSubmit').on('click', function (event) {
        var address = pickupDialog.find('.pickupAddress').val();
        event.preventDefault();
        if (address !== '') {
            var myGeocoder = ymaps.geocode(address);
            if (currentLocationCollection !== undefined) {
                pickupMap.geoObjects.remove(currentLocationCollection);
            }
            myGeocoder.then(
                function (res) {
                    var myPoint = res.geoObjects.get(0);
                    if (myPoint !== null && myPoint !== undefined) {
                        myPoint.options.set('preset', 'twirl#redIcon');
                        currentLocationCollection = new ymaps.GeoObjectCollection({});
                        currentLocationCollection.add(myPoint);
                        var coords = myPoint.geometry.getCoordinates();
                        pickupMap.geoObjects.add(currentLocationCollection);
                        pickupMap.setCenter(coords);
                        $('.pointDescription', pickupDialog).each(function () {
                            handleKilos($(this), coords);
                        });
                        $('.pointDescription', pickupDialog).tsort({ attr: "data-kilos" });
                    }
                    else {
                        alert('Заданный адрес не найден');
                    }
                },
                function (err) {
                    alert('Заданный адрес не найден');
                }
            );
        }
        else {

            alert('Не заполнен адрес');
        }
    });
});
function init() {
    $('#pickupYandexMap').html('');
    pickupMap = new ymaps.Map("pickupYandexMap", {
        center: [55.76, 37.64],
        zoom: 10,
    });
    pickupMap.controls.add(new ymaps.control.ZoomControl({ noTips: true }), { left: '10px', top: '20px' });
    var pointsCollection = new ymaps.GeoObjectCollection({});
    var pointObject = $.parseJSON($('[id$="placesGeo"]').val());
    for(var i = 0; i < pointObject.length; i++)
    {
        pointsCollection.add(new ymaps.Placemark([pointObject[i].latitude, pointObject[i].longitude], { content: pointObject[i].sapCode + ' - ' + pointObject[i].name, balloonContent: pointObject[i].sapCode + ' - ' + pointObject[i].name }));
    }
    pickupMap.geoObjects.add(pointsCollection);
}
