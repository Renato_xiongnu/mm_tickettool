﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TicketTool.Integration.ZZT.Services.Common;
using System.Text;
using System.Linq;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;

namespace ZZTIntegration.Test
{
    [TestClass]
    public class OrderCreationTest
    {
        private string GenerateOrderId()
        {
            var rand = new Random();
            return string.Format("{0:000}-{1:000}-{2:000}", rand.Next(0, 999), rand.Next(0, 999), rand.Next(0, 999));
        }

        [TestMethod]
        public void BasketServiceTest()
        {
            //"https://nbo.srv.itdelo.com/connector.php"
            //"http://avoiteh.dev.itdelo.com/back/connector.php"
            System.Net.ServicePointManager.ServerCertificateValidationCallback =
                ((sender, certificate, chain, sslPolicyErrors) => true);
            var request =
                (HttpWebRequest) System.Net.WebRequest.Create(new Uri("https://nbo.srv.itdelo.com/connector.php"));
            request.Timeout = 10000;
            request.Credentials = new NetworkCredential("", "");
            request.PreAuthenticate = true;
            var cert = X509Certificate.CreateFromCertFile(@"C:\ClientCertificate.cer");
            request.ClientCertificates.Add(cert);
            request.Method = "POST";

            var byteArray =
                Encoding.UTF8.GetBytes(string.Format("request={0}",
                                                     "{\"requests\":[{\"num\":\"1\",\"type\":\"select\",\"content\":{\"name\":\"City\",\"filter\":\"\"}}]}"));
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;
            var dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            var response = request.GetResponse();
            var streamReader = new StreamReader(response.GetResponseStream(),
                                                Encoding.GetEncodings().Single(t => t.CodePage == 1251).GetEncoding());
            var data = streamReader.ReadToEnd();
            var ccc = 5;
            //BasketServiceLink.BasketServiceClient client = new BasketServiceLink.BasketServiceClient();
            //var data = client.Validate(new BasketServiceLink.ValidateRequest
            //    {
            //        consumer = "metro",
            //        articles = new BasketServiceLink.ArticleBaseData[] 
            //        {
            //            new BasketServiceLink.ArticleBaseData
            //            {
            //                id = "1192725",
            //                price = 21111,
            //                qty = 2,
            //                campaign_id = "5902_metro"
            //            },
            //            new BasketServiceLink.ArticleBaseData
            //            {
            //                id = "1159934",
            //                price = 18999,
            //                qty = 1,
            //                campaign_id = "5902_metro"
            //            }
            //        }
            //    });
        }

        //[TestMethod]
        //public void Test123()
        //{
        //    BasketLink.BasketServiceClient client = new BasketLink.BasketServiceClient();
        //    var data = client.Validate(new BasketLink.ValidateRequest 
        //    { 
        //        Consumer = "metro", 
        //        Articles = new BasketLink.ArticleData[]
        //        {
        //            new BasketLink.ArticleData
        //            {
        //             Id = "1188761",
        //              Qty = 1,
        //               CampaignId = "5902_metro",
        //               Type = BasketLink.ArticleType.Product
        //            }
        //        }
        //    }
        //        );
        //}
        //[TestMethod]
        //public void TestMethod4()
        //{
        //    //log4net.Config.XmlConfigurator.Configure();
        //    var storage = StorageFactory.Create();
        //    //var data = storage.GetEntities<PickUpPoint>("bycityid", new string[] { "2" });
        //    //var data2 = storage.GetEntities<City>("byenabled", new string[] {});
        //}

        //[TestMethod]
        //public void TestMethod3()
        //{
        //    ArticleDataLink.Win8SMServiceClient client = new ArticleDataLink.Win8SMServiceClient();
        //    var p = new ArticleDataLink.GetSMItemsResponse();
        //    var data = client.GetFilteredItems(new ArticleDataLink.GetSMFilteredItemsRequest
        //    {
        //        Query = "фильм",
        //        SapCode = "R202"
        //    });
        //}

        [TestMethod]
        public void TestMethod1()
        {
            var client = new OrderLink.OrderServiceClient();
            var orderId = GenerateOrderId();
            var index = 6;
            var data = client.CreateOrder(new OrderLink.ClientData
                {
                    Name = "test" + index,
                    Surname = "test" + index,
                    LastName = "test" + index,
                    Email = "asdasd",
                    Fax = "111",
                    Phone = "123123",
                    ZZTCustomerId = index
                },
                                          new OrderLink.CreateOrderData
                                              {
                                                  ExternalSystem = OrderLink.ExternalSystem.ZZT,
                                                  OrderId = orderId,
                                                  OrderSource = OrderLink.OrderSource.Metro,
                                                  PaymentType = OrderLink.PaymentType.Cash,
                                                  StoreInfo = new OrderLink.StoreInfo {SapCode = "R218"},
                                                  ZZTData = new OrderLink.ZZTHeaderData
                                                      {
                                                          ConsumerId = "metro",
                                                          CampaignId = "5902_metro",
                                                          DeliveryAddress = new OrderLink.ZZTDeliveryAddress
                                                              {
                                                              },
                                                          DeliveryInfo = new OrderLink.ZZTDeliveryInfo
                                                              {
                                                                  DeliveryPlaceSapCode = "R218",
                                                                  DeliveryDate = new DateTime(2013, 10, 23),
                                                                  DeliveryType = "3"
                                                              }
                                                      }
                                              },
                                          new OrderLink.DeliveryInfo
                                              {
                                              },
                                          new OrderLink.ArticleData[]
                                              {
                                                  new OrderLink.ArticleData()
                                                      {
                                                          ArticleNum = "1161629",
                                                          Price = 5199,
                                                          Qty = 1
                                                      },
                                                  new OrderLink.ArticleData()
                                                      {
                                                          ArticleNum = "1188761",
                                                          Price = 28999,
                                                          Qty = 1
                                                      }
                                              });
            var c = data;
            var newData = client.GerOrderDataByOrderId(orderId);
        }


        //[TestMethod]
        //public void TestMethod2()
        //{
        //    var client = new OrderLink.OrderServiceClient();
        //    //var orderId = GenerateOrderId();
        //    var index = 3;
        //    var data = client.UpdateOrder(new OrderLink.UpdateOrderData
        //    {
        //        OrderId = "285-792-680",
        //        ClientData = new OrderLink.ClientData { LastName = "Абыр" },
        //        ZZTData = new OrderLink.ZZTHeaderData
        //        {
        //            DeliveryAddress = new OrderLink.ZZTDeliveryAddress { District = "customDist" },
        //            DeliveryInfo = new OrderLink.ZZTDeliveryInfo { DeliveryPrice = 200 },
        //            InvoiceCustomer = new OrderLink.ClientData {
        //                Name = "test5" + index,
        //                Surname = "test5" + index,
        //                LastName = "test5" + index,
        //                Email = "asdasd",
        //                Fax = "111",
        //                Phone = "123123",
        //                ZZTCustomerId = index
        //            }
        //        }
        //    }, new OrderLink.ReserveInfo { });
        //    var c = data;
        //}

        [TestMethod]
        public void GenerateOrderId_Test()
        {
            var orderId = BasketProcessing.GenerateOrderId("990");

            Assert.IsFalse(string.IsNullOrEmpty(orderId));
        }
    }
}
