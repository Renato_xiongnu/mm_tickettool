﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace TicketTool.Integration.ZZT.Services.Contracts
{
    [Serializable]
    [DataContract]
    public class GlobalZoneInfo
    {
        public GlobalZoneInfo()
        {
        }

        [XmlElement("Name")]
        [DataMember]
        public string Name { get; set; }

        [XmlElement("Price")]
        [DataMember]
        public decimal? Price { get; set; }

        [XmlElement("Id")]
        [DataMember]
        public string Id { get; set; }

        [XmlElement("Colour")]
        [DataMember]
        public string Colour { get; set; }
    }
}