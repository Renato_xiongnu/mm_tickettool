﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TicketTool.Integration.ZZT.Services.Contracts.Requests
{
    [DataContract]
    public class DeliveryPriceRequest
    {
        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string Latitude { get; set; }

        [DataMember]
        public string Longitude { get; set; }
    }

}