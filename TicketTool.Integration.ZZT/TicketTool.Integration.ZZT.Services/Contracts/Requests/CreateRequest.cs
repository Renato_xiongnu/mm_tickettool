﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TicketTool.Integration.ZZT.Services.Contracts.Requests
{
    [DataContract]
    public class CreateRequest
    {
        public CreateRequest() 
        {
            Articles = new List<ArticleData>();
            Customer = new CustomerData();
        }

        [DataMember]
        public string SystemComment { get; set; }

        [DataMember]
        public string CommentFromCustomer { get; set; }

        [DataMember]
        public string SapCode { get; set; }

        [DataMember]
        public string Consumer { get; set; }

        [DataMember]
        public DeliveryData Delivery { get; set; }

        [DataMember]
        public List<ArticleData> Articles { get; set; }

        [DataMember]
        public CustomerData Customer { get; set; }
    }
}