﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TicketTool.Integration.ZZT.Services.Contracts.Requests
{
    [DataContract]
    public class DeliveryArticleRequest
    {
        [DataMember]
        public string ArticleNum { get; set; }
    }

}