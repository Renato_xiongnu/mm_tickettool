﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using TicketTool.Integration.ZZT.Services.Common;

namespace TicketTool.Integration.ZZT.Services.Contracts.Requests
{
    [DataContract]
    public class ValidateRequest
    {
        public ValidateRequest() 
        {
            Articles = new List<ArticleData>();
        }

        [DataMember]
        public string Consumer { get; set; }

        [DataMember]
        public List<ArticleData> Articles { get; set; }
    }

}