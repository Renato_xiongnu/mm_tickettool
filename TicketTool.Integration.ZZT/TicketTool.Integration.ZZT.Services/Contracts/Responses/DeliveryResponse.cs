﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using TicketTool.Integration.ZZT.Services.Common;

namespace TicketTool.Integration.ZZT.Services.Contracts.Responses
{
    [DataContract]
    public class DeliveryResponse
    {
        public DeliveryResponse()
        {
            Zones = new List<string>();
            //Article = new ArticleBaseData();
        }

        [DataMember]
        public string X { get; set; }

        [DataMember]
        public string Y { get; set; }

        [DataMember]
        public string Price { get; set; }
        //[DataMember]
        //public ArticleBaseData Article { get; set; }

        [DataMember]
        public List<string> Zones { get; set; }

        [DataMember]
        public ResponseResult Result { get; set; }

        [DataMember]
        public string ErrorMessage { get; set; }
    }

}