﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using TicketTool.Integration.ZZT.Services.Common.Geolocation;

namespace TicketTool.Integration.ZZT.Services.Contracts.Responses
{
    [DataContract]
    public class GlobalZonesResponse
    {
        public GlobalZonesResponse()
        {
            Zones = new List<GlobalZoneInfo>();
        }

        [DataMember]
        public ResponseResult Result { get; set; }

        [DataMember]
        public string ErrorMessage { get; set; }

        [DataMember]
        public List<GlobalZoneInfo> Zones { get; set; }
    }
}