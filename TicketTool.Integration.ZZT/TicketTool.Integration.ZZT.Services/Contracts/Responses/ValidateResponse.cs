﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using TicketTool.Integration.ZZT.Services.Common;

namespace TicketTool.Integration.ZZT.Services.Contracts.Responses
{
    [DataContract]
    public class ValidateResponse
    {
        public ValidateResponse()
        {
            Places = new List<PickupPoint>();
            Articles = new List<ArticleData>();
        }

        [DataMember]
        public ResponseResult Result { get; set; }

        [DataMember]
        public ErrorObject ErrorObjectType { get; set; }

        [DataMember]
        public string ErrorMessage { get; set; }

        [DataMember]
        public string ErrorObjectId { get; set; }

        [DataMember]
        public List<PickupPoint> Places { get; set; }

        [DataMember]
        public List<ArticleData> Articles { get; set; }
    }
}