﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using TicketTool.Integration.ZZT.Services.Common;

namespace TicketTool.Integration.ZZT.Services.Contracts.Responses
{
    [DataContract]
    public class DeliveryTypesResponse
    {
        public DeliveryTypesResponse()
        {
            DeliveryTypes = new Dictionary<string, string>();
        }

        [DataMember]
        public Dictionary<string, string> DeliveryTypes { get; set; }

        [DataMember]
        public ResponseResult Result { get; set; }

        [DataMember]
        public string ErrorMessage { get; set; }
    }

}