﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TicketTool.Integration.ZZT.Services.Common;
using ZZTModel = MMS.Storage.DataModel.ZZT;

namespace TicketTool.Integration.ZZT.Services.Contracts
{
    public static class Extensions
    {
        public static PickupPoint ToModel(this BasketLink.ShopInfo shop)
        {
            PickupPoint point = new PickupPoint
            {
                SapCode = shop.SapCode,
                Name = shop.Title,
                Address = shop.ContactInfo.Address,
                Phone = shop.ContactInfo.Phone,
                WorkTime = shop.OpeningHours,
                Latitude = shop.Location.Latitude.ToString(),
                Longitude = shop.Location.Longitude.ToString()
            };
            point.Types = DeliveryTypesProcessing.GetTypesByChain(shop.Chain);
            if (shop.Chain == BasketLink.ChainIdEnum.ZZT)
            {
                point.Type = PointType.ZZT;
            }
            else
            {
                point.Type = PointType.Media;
            }
            return point;
        }

        public static PickupPoint ToModel(this ZZTModel.PickUpPoint shop, string sapCode)
        {
            PickupPoint point = new PickupPoint
            {
                SapCode = sapCode + ";" + shop.PickUpPointId,
                Name = shop.Name,
                Address = shop.Address,
                Phone = shop.Phone,
                WorkTime = shop.WorkingTime,
                Latitude = shop.Latitude.ToString(),
                Longitude = shop.Longitude.ToString()
            };
            point.Type = PointType.ZZT;
            point.Types = DeliveryTypesProcessing.GetAllTypes();
            return point;
        }
    }
}