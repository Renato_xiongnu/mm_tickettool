﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TicketTool.Integration.ZZT.Services.Contracts
{
    [DataContract]
    public class DeliveryData
    {
        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string Longitude { get; set; }

        [DataMember]
        public string Latitude { get; set; }

        [DataMember]
        public string DeliveryType { get; set; }

        [DataMember]
        public DateTime? DeliveryDate { get; set; }

        [DataMember]
        public string PickupPlaceSapCode { get; set; }

        [DataMember]
        public string PickupPlaceId { get; set; }

        [DataMember]
        public decimal? DeliveryPrice { get; set; }
    }
}