﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicketTool.Integration.ZZT.Services.Contracts
{
    public enum ResponseResult
    {
        OK, 
        Error
    }
    public enum ErrorObject
    {
        Article = 0,
        Global = 1,
        Internal = 2
    }

    public enum ArticleType
    {
        None = 0,
        Product = 1,
        Service = 2
    }

    public enum PointType
    {
        Media, 
        ZZT
    }
}