﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TicketTool.Integration.ZZT.Services.Contracts
{
    [DataContract]
    public class ArticleData : ArticleBaseData
    {
        public ArticleData()
        {
            Places = new List<PickupPoint>();
            DeliveryTypes = new List<string>();
        }

        [DataMember]
        public int AvailableQty { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public List<PickupPoint> Places { get; set; }

        [DataMember]
        public List<string> DeliveryTypes { get; set; }
    }
}