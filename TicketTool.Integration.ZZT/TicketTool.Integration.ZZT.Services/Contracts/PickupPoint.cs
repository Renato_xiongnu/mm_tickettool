﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using TicketTool.Integration.ZZT.Services.Common;

namespace TicketTool.Integration.ZZT.Services.Contracts
{
    [DataContract]
    public class PickupPoint
    {
        public PickupPoint()
        {
            Articles = new List<ArticleData>();
            Types = new Dictionary<string, string>();
        }

        [DataMember]
        public string SapCode { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string WorkTime { get; set; }

        [DataMember]
        public PointType Type { get; set; }

        [DataMember]
        public Dictionary<string, string> Types { get; set; }

        [DataMember]
        public string Latitude { get; set; }

        [DataMember]
        public string Longitude { get; set; }

        [DataMember]
        public List<ArticleData> Articles { get; set; }
    }
}