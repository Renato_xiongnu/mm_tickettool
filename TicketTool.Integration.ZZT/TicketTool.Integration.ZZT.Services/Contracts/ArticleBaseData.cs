﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace TicketTool.Integration.ZZT.Services.Contracts
{
    [DataContract]
    public class ArticleBaseData
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public int Qty { get; set; }

        [DataMember]
        public decimal Price { get; set; }

        [DataMember]
        public string CampaignId { get; set; }

        [DataMember]
        public ArticleType Type { get; set; }
    }
}