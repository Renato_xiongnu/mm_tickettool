﻿using TicketTool.Integration.ZZT.Services.Common;
using TicketTool.Integration.ZZT.Services.Contracts.Requests;
using TicketTool.Integration.ZZT.Services.Contracts.Responses;

namespace TicketTool.Integration.ZZT.Services
{
    public class BasketService : IBasketService
    {
        public ValidateResponse Validate(ValidateRequest request)
        {
            return BasketProcessing.ValidateOrder(request);
        }

        public GlobalZonesResponse GetDeliveryZones()
        {
            var result = BasketProcessing.GetGlobalZones();
            return result;
        }

        public DeliveryResponse GetDeliveryPrice(DeliveryPriceRequest request)
        {
            return BasketProcessing.GetDeliveryPrice(request);
        }

        public DeliveryTypesResponse GetDeliveryTypes()
        {
            return BasketProcessing.GetDeliveryTypes();
        }

        public CreateResponse CreateOrder(CreateRequest request)
        {
            return BasketProcessing.CreateOrder(request);
        }

        public string GenerateOrderId(string sapCode)
        {
            return BasketProcessing.GenerateOrderId(sapCode);
        }

    }
}
