﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using TicketTool.Integration.ZZT.Services.Contracts;
using TicketTool.Integration.ZZT.Services.Contracts.Requests;
using TicketTool.Integration.ZZT.Services.Contracts.Responses;

namespace TicketTool.Integration.ZZT.Services
{
    [ServiceContract]
    public interface IBasketService
    {
        [OperationContract]
        ValidateResponse Validate(ValidateRequest value);

        [OperationContract]
        DeliveryResponse GetDeliveryPrice(DeliveryPriceRequest request);

        [OperationContract]
        DeliveryTypesResponse GetDeliveryTypes();

        [OperationContract]
        CreateResponse CreateOrder(CreateRequest request);

        [OperationContract]
        string GenerateOrderId(string sapCode);

        [OperationContract]
        GlobalZonesResponse GetDeliveryZones();
    }
}
