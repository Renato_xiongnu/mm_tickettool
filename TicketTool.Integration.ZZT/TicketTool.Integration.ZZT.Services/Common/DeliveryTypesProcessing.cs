﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicketTool.Integration.ZZT.Services.Common
{
    public static class DeliveryTypesProcessing
    {
        public static Dictionary<string, string> GetAllTypes()
        {
            return new Dictionary<string, string> { { "2", "Доставка" }, { "20", "Самовывоз" } };
        }

        public static Dictionary<string, string> GetPickupTypes()
        {
            return new Dictionary<string, string> { { "20", "Самовывоз" } };
        }

        public static Dictionary<string, string> GetTypesByChain(BasketLink.ChainIdEnum chainType)
        {
            if (chainType == BasketLink.ChainIdEnum.ZZT)
                return GetAllTypes();
            return GetPickupTypes();
        }
        public static Dictionary<string, string> GetTypesByDeliverySaps(List<string> deliveryCodes, List<string> sapCodes)
        {
            if (sapCodes.Any(m => deliveryCodes.Contains(m)))
                return GetAllTypes();
            return GetPickupTypes();
        }
    }
}