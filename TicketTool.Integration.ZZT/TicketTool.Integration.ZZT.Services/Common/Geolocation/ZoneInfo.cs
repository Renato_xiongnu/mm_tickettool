﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace TicketTool.Integration.ZZT.Services.Common.Geolocation
{
    [XmlRootAttribute("ZonesCollection")]
    public class ZoneInfoCollection
    {
        public ZoneInfoCollection()
        {
            ZoneInfoList = new List<ZoneInfo>();
        }

        [XmlElement("ZoneInfo")]
        public List<ZoneInfo> ZoneInfoList { get; set; }
    }

    [Serializable]
    public class ZoneInfo
    {
        public ZoneInfo()
        {
            PolygonPoints = new List<PointD>();
        }

        [XmlElement("Name")]
        public string Name { get; set; }

        [XmlElement("Price")]
        public string Price { get; set; }

        [XmlElement("RawData")]
        public string RawData { get; set; }

        public List<PointD> PolygonPoints { get; set; }

        public PointD LLBound { get; set; }

        public PointD URBound { get; set; }
    }
}