﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml.Serialization;
using TicketTool.Integration.ZZT.Services.Common.Geolocation.Coordinates;

namespace TicketTool.Integration.ZZT.Services.Common.Geolocation
{
    public static class ZonesProcessing
    {
        public static List<ZoneInfo> CheckPoint(PointD point)
        {
            List<ZoneInfo> result = new List<ZoneInfo>();
            var collection = GetZones();
            PrepareZones(collection);
            foreach (var zone in collection.ZoneInfoList)
            {
                if (CheckPointInZone(zone, point))
                {
                    result.Add(zone);
                }
            }
            return result;
        }
        public static List<ZoneInfo> CheckPoint(string address)
        {
            if (String.IsNullOrEmpty(address))
                throw new InvalidOperationException("address is empty");
            var point = GetCoordinatesFromAddress(address);
            return CheckPoint(point);
        }

        public static PointD GetCoordinatesFromAddress(string address)
        {
            Uri uri = new Uri(ConfigurationManager.AppSettings["YandexApi"] + "?" + String.Format("format=json&geocode={0}", address));
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    var resultObj = JsonConvert.DeserializeObject<RootObject>(reader.ReadToEnd());
                    int resultAmount = resultObj.response.GeoObjectCollection.metaDataProperty.GeocoderResponseMetaData.found;
                    if (resultAmount != 0)
                    {
                        string firstPoint = resultObj.response.GeoObjectCollection.featureMember.First().GeoObject.Point.pos;
                        var data = firstPoint.Split(' ');
                        PointD resultPoint = new PointD(double.Parse(data[0]), double.Parse(data[1]));
                        resultPoint.IsEmpty = false;
                        return resultPoint;
                    }
                }
            }
            return new PointD { IsEmpty = true };
        }

        public static bool CheckPointInZone(ZoneInfo zone, PointD point)
        {
            if (point.X < zone.LLBound.X ||
                point.X > zone.URBound.X ||
                point.Y < zone.LLBound.Y ||
                point.Y > zone.URBound.Y)
            {
                return false;
            }
            bool IsInZone = false;
            for (int i = 0; i < zone.PolygonPoints.Count - 1; i++)
            {
                if (zone.PolygonPoints[i] == point || zone.PolygonPoints[i + 1] == point)
                {
                    return true;
                }
                if (zone.PolygonPoints[i].Y == zone.PolygonPoints[i + 1].Y &&
                    zone.PolygonPoints[i].Y == point.Y &&
                    ((zone.PolygonPoints[i].X > point.X) != (zone.PolygonPoints[i + 1].X > point.X)))
                {
                    //boundary, true
                    return true;
                }
                if (((zone.PolygonPoints[i].Y > point.Y) != (zone.PolygonPoints[i + 1].Y > point.Y)) &&
                    (point.X < (zone.PolygonPoints[i].X - zone.PolygonPoints[i + 1].X) * (point.Y - zone.PolygonPoints[i + 1].Y) / (zone.PolygonPoints[i].Y - zone.PolygonPoints[i + 1].Y) + zone.PolygonPoints[i + 1].X)
                    )
                {
                    IsInZone = !IsInZone;
                }
            }
            return IsInZone;
        }

        private static ZoneInfoCollection GetZones()
        {
            string filePath = System.Web.Hosting.HostingEnvironment.MapPath(ConfigurationManager.AppSettings["ZonesFilePath"]);

            ZoneInfoCollection collection = new ZoneInfoCollection();
            using (var sr = new StreamReader(filePath))
            {
                var xs = new XmlSerializer(collection.GetType());
                collection = (ZoneInfoCollection)(xs.Deserialize(sr));
            
            }
            return collection;
        }

        public static GlobalZoneInfoCollection GetGlobalZones()
        {
            string filePath = System.Web.Hosting.HostingEnvironment.MapPath(ConfigurationManager.AppSettings["GlobalZonesFilePath"]);

            GlobalZoneInfoCollection collection = new GlobalZoneInfoCollection();
            using (var sr = new StreamReader(filePath))
            {
                var xs = new XmlSerializer(collection.GetType());
                collection = (GlobalZoneInfoCollection)(xs.Deserialize(sr));
            }
            return collection;
        }

        private static void PrepareZones(ZoneInfoCollection collection)
        {
            foreach (var item in collection.ZoneInfoList)
            {
                var points = item.RawData.Split(' ').Select(m => double.Parse(m)).ToList();
                if (points.Count % 2 != 0)
                    throw new InvalidOperationException("wrong input file, total is not even");
                double minY = double.MaxValue, minX = double.MaxValue;
                double maxY = double.MinValue, maxX = double.MinValue;
                for (int i = 0; i < points.Count - 1; i += 2)
                {
                    if (points[i + 1] < minY)
                    {
                        minY = points[i + 1];
                    }
                    if (points[i + 1] > maxY)
                    {
                        maxY = points[i + 1];
                    }
                    if (points[i] < minX)
                    {
                        minX = points[i];
                    }
                    if (points[i] > maxX)
                    {
                        maxX = points[i];
                    }
                    item.PolygonPoints.Add(new PointD(points[i], points[i + 1]));
                }
                if (item.PolygonPoints.Count <= 2)
                    throw new InvalidOperationException("wrong input file, not enough points");
                if (item.PolygonPoints[0] != item.PolygonPoints[item.PolygonPoints.Count - 1])
                {
                    item.PolygonPoints.Add(item.PolygonPoints[0]);
                }
                item.LLBound = new PointD(minX, minY);
                item.URBound = new PointD(maxX, maxY);
            }
        }
    }
}