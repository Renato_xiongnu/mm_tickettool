﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using TicketTool.Integration.ZZT.Services.Contracts;

namespace TicketTool.Integration.ZZT.Services.Common.Geolocation
{
    [XmlRootAttribute("GlobalZonesCollection")]
    public class GlobalZoneInfoCollection
    {
        public GlobalZoneInfoCollection()
        {
            GlobalZoneInfoList = new List<GlobalZoneInfo>();
        }

        [XmlElement("GlobalZoneInfo")]
        public List<GlobalZoneInfo> GlobalZoneInfoList { get; set; }
    }

}