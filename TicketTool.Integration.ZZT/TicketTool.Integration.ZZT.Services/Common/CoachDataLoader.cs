﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TicketTool.Integration.ZZT.Storage;
using ZZTModel = MMS.Storage.DataModel.ZZT;

namespace ZZTIntegration.Web.Common
{
    public static class CoachDataLoader
    {
        public static List<ZZTModel.PickUpPoint> GetMetroPickup()
        {
            var storage = StorageFactory.Create();
            var points = storage.GetEntities<ZZTModel.PickUpPoint>("getMetroPoints", new string[] { });
            // coach wrong data
            foreach (var point in points)
            {
                decimal lat = point.Latitude;
                point.Latitude = point.Longitude;
                point.Longitude = lat;
            }
            return points.ToList();
        }
    }
}