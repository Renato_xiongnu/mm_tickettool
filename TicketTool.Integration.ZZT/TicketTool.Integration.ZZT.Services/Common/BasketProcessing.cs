﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using TicketTool.Integration.ZZT.Services.Common.Geolocation;
using TicketTool.Integration.ZZT.Services.Contracts;
using TicketTool.Integration.ZZT.Services.Contracts.Requests;
using TicketTool.Integration.ZZT.Services.Contracts.Responses;
using ZZTIntegration.Web.Common;

namespace TicketTool.Integration.ZZT.Services.Common
{
    public static class BasketProcessing
    {
        private static string CheckCreateRequest(CreateRequest request)
        {
            //if (String.IsNullOrEmpty(request.CampaignName))
            //{
            //    return "CampaignName required";
            //}
            if (request == null)
            {
                return "Не передан заказ";
            }
            if (request.Customer == null)
            {
                return "Не передан заказчик";
            }
            if (request.Delivery == null)
            {
                return "Не переданы данные доставки";
            }
            if (String.IsNullOrEmpty(request.Consumer))
            {
                return "Не передан потребитель";
            }
            if (String.IsNullOrEmpty(request.SapCode))
            {
                return "Не передан sapCode";
            }
            if (String.IsNullOrEmpty(request.Customer.Name) && String.IsNullOrEmpty(request.Customer.Surname) && String.IsNullOrEmpty(request.SystemComment))
            {
                return "Не заполнены контактные данные";
            }
            if (String.IsNullOrEmpty(request.Customer.Email))
            {
                return "Не заполнен email";
            }
            if (request.Articles == null || request.Articles.Count == 0)
            {
                return "Не заданы артикулы";

            }
            var campaignName = request.Articles.Select(m => m.CampaignId).Distinct().ToList();
            if (campaignName.Count != 1)
            {
                return "Все артикулы должны принадлежат одной акции";
            }
            if (String.IsNullOrEmpty(request.Articles.First().CampaignId))
            {
                return "Не задана акция";
            }
            foreach (var data in request.Articles)
            {
                if (String.IsNullOrEmpty(data.Id))
                {
                    return "Не задан артикул";
                }
                if (data.Qty == 0)
                {
                    return data.Id + ": Не задано количество";
                }
            }
            return null;
        }

        private static bool PreCheckValidateRequest(ValidateRequest request, ref ValidateResponse response)
        {
            if (request == null)
            {
                response = new ValidateResponse
                {
                    ErrorObjectType = ErrorObject.Global,
                    ErrorMessage = "запрос не был передан",
                    Result = ResponseResult.Error
                };
                return false;
            }
            if (request.Articles == null || request.Articles.Count == 0)
            {
                response = new ValidateResponse
                {
                    ErrorObjectType = ErrorObject.Global,
                    ErrorMessage = "не заданы артикулы",
                    Result = ResponseResult.Error
                };
                return false;
            }
            var emptyArticle = request.Articles.FirstOrDefault(m => m.Type == ArticleType.None);
            if (emptyArticle != null)
            {
                response = new ValidateResponse
                {
                    ErrorObjectType = ErrorObject.Article,
                    ErrorObjectId = emptyArticle.Id,
                    ErrorMessage = "Тип артикула не задан",
                    Result = ResponseResult.Error
                };
                return false;
            }
            var emptyIdArticle = request.Articles.FirstOrDefault(m => String.IsNullOrEmpty(m.Id));
            if (emptyIdArticle != null)
            {
                response = new ValidateResponse
                {
                    ErrorObjectType = ErrorObject.Article,
                    ErrorObjectId = emptyArticle.Id,
                    ErrorMessage = "Не задан номер артикула",
                    Result = ResponseResult.Error
                };
                return false;
            }
            var zeroArticle = request.Articles.FirstOrDefault(m => m.Qty == 0);
            if (zeroArticle != null)
            {
                response = new ValidateResponse
                {
                    ErrorObjectType = ErrorObject.Article,
                    ErrorObjectId = zeroArticle.Id,
                    ErrorMessage = "Количество товара 0",
                    Result = ResponseResult.Error
                };
                return false;
            }
            var tooMuchArticle = request.Articles.FirstOrDefault(m => m.Qty > 3);
            if (tooMuchArticle != null)
            {
                response = new ValidateResponse
                {
                    ErrorObjectType = ErrorObject.Article,
                    ErrorObjectId = tooMuchArticle.Id,
                    ErrorMessage = "Количество товара не может превышать 3 единиц",
                    Result = ResponseResult.Error
                };
                return false;
            } 
            var productArticles = request.Articles.Where(m => m.Type == ArticleType.Product).ToList();
            var campaignName = productArticles.Select(m => m.CampaignId).Distinct().ToList();
            if (campaignName.Count != 1)
            {
                response = new ValidateResponse
                {
                    ErrorObjectType = ErrorObject.Global,
                    ErrorMessage = "Все артикулы должны принадлежать одной акции",
                    Result = ResponseResult.Error
                };
                return false;
            }
            if (String.IsNullOrEmpty(campaignName[0]))
            {
                response = new ValidateResponse
                {
                    ErrorObjectType = ErrorObject.Global,
                    ErrorMessage = "Не передано имя акции",
                    Result = ResponseResult.Error
                };
                return false;
            }
            if (String.IsNullOrEmpty(request.Consumer))
            {
                response = new ValidateResponse
                {
                    ErrorObjectType = ErrorObject.Global,
                    ErrorMessage = "Не передан потребитель акции",
                    Result = ResponseResult.Error
                };
                return false;
            }
            return true;
        }
        
        private static bool PostCheckValidateRequest(ValidateRequest request, BasketLink.ValidateResponse response, ref ValidateResponse postValidationResp)
        {
            var wrongArticle = response.Articles.FirstOrDefault(m => !m.Price.HasValue);
            if (wrongArticle != null)
            {
                postValidationResp = new ValidateResponse
                {
                    ErrorObjectType = ErrorObject.Article,
                    ErrorObjectId = wrongArticle.Article.ToString(),
                    ErrorMessage = "Артикул не участвует в акции",
                    Result = ResponseResult.Error
                };
                return false;
            }

            foreach (var article in request.Articles)
            {
                var responseArticle = response.Articles.FirstOrDefault(m => m.Article.ToString() == article.Id);
                if (responseArticle == null)
                {
                    postValidationResp = new ValidateResponse
                    {
                        ErrorObjectType = ErrorObject.Article,
                        ErrorObjectId = article.Id,
                        ErrorMessage = "Отсутсвует информация об артикуле",
                        Result = ResponseResult.Error
                    };
                    return false;
                }
                if (article.Price != 0 && article.Price != responseArticle.Price)
                {
                    postValidationResp = new ValidateResponse
                    {
                        ErrorObjectType = ErrorObject.Article,
                        ErrorObjectId = article.Id,
                        ErrorMessage = "Неправильная цена артикула, правильная цена: " + String.Format("{0:0.00}", responseArticle.Price),
                        Result = ResponseResult.Error
                    };
                    return false;
                }
                var availableShopsCount = responseArticle.ShopData.Count(m => m.Qty >= article.Qty);
                if (availableShopsCount == 0)
                {
                    postValidationResp = new ValidateResponse
                    {
                        ErrorObjectType = ErrorObject.Global,
                        ErrorMessage = "Товара нет в наличии",
                        Result = ResponseResult.Error
                    };
                    return false;
                }

                int minStoreCount;
                if(!Int32.TryParse(ConfigurationManager.AppSettings["MinStoreCount"],out minStoreCount))
                {
                    minStoreCount = 3;
                }
            }
            return true;
        }

        public static DeliveryResponse GetDeliveryPrice(DeliveryPriceRequest request)
        {
            try
            {
                if (request == null)
                {
                    return new DeliveryResponse
                    {
                        Result = ResponseResult.Error,
                        ErrorMessage = "Не передан запрос"
                    };
                }
                var point = new PointD();
                if (!String.IsNullOrEmpty(request.Address))
                {
                     point = ZonesProcessing.GetCoordinatesFromAddress(request.Address);
                }
                else if (!String.IsNullOrEmpty(request.Latitude) && !String.IsNullOrEmpty(request.Longitude))
                {
                    point = new PointD(double.Parse(request.Latitude), double.Parse(request.Longitude));
                }
                else
                {
                    return new DeliveryResponse
                    {
                        Result = ResponseResult.Error,
                        ErrorMessage = "Адрес не был передан"
                    };
                }
                if (point.IsEmpty)
                {
                    return new DeliveryResponse
                    {
                        Result = ResponseResult.Error,
                        ErrorMessage = "Адрес не был найден"
                    };
                }
                var zones = ZonesProcessing.CheckPoint(point);
                if(zones == null || zones.Count == 0)
                {
                    return new DeliveryResponse
                    {
                        X = point.X.ToString(),
                        Y = point.Y.ToString(),
                        Zones = null,
                        Result = ResponseResult.OK,
                        ErrorMessage = ""
                    };
                }
                var requestedZone = zones.First();
                return new DeliveryResponse
                {
                    X = point.X.ToString(),
                    Y = point.Y.ToString(),
                    Zones = new List<string> { requestedZone.Name },
                    Price = requestedZone.Price,
                    //Article = /*articleResponse.Article*/articleResponse,
                    Result = ResponseResult.OK,
                    ErrorMessage = ""
                };
            }
            catch (Exception ex)
            {
                return new DeliveryResponse
                {
                    Result = ResponseResult.Error,
                    ErrorMessage = "Internal error"
                };
            }

        }

        public static string GenerateOrderId(string sapCode)
        {
            int maxCount;
            int.TryParse(ConfigurationManager.AppSettings["IdGenerationTryCount"], out maxCount);
            for (var i = 0; i < maxCount; i++)
            {
                try
                {
                    var uri = new Uri(String.Format(ConfigurationManager.AppSettings["IdGenerationUrl"], sapCode));
                    string username = ConfigurationManager.AppSettings["IdGenerationLogin"];
                    string password = ConfigurationManager.AppSettings["IdGenerationPassword"];
                    string authInfo = username + ":" + password;
                    authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                    var request = (HttpWebRequest)WebRequest.Create(uri);
                    request.Headers["Authorization"] = "Basic " + authInfo;
                    using (var response = (HttpWebResponse)request.GetResponse())
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        var js = new JavaScriptSerializer();
                        var result = reader.ReadToEnd();
                        var order = js.Deserialize<dynamic>(result);

                        var orderId = order["orderNum"];
                        if (string.IsNullOrEmpty(orderId))
                        {
                            throw new InvalidDataException();
                        }
                        return order["orderNum"];
                    }

                }
                catch
                {

                }
            }

            return string.Format("990-ПЕРЕСОЗДАТЬ-{0}", Guid.NewGuid());
        }

        public static DeliveryTypesResponse GetDeliveryTypes()
        {
            try
            {
                return new DeliveryTypesResponse
                {
                    Result = ResponseResult.OK,
                    DeliveryTypes = DeliveryTypesProcessing.GetAllTypes(),
                };
            }
            catch(Exception ex)
            {
                return new DeliveryTypesResponse
                {
                   Result = ResponseResult.Error,
                   ErrorMessage = "InternalError"
                };
            }
        }

        public static CreateResponse CreateOrder(CreateRequest request)
        {
            try
            {
                var client = new RouterLink.OrderServiceClient();
                var message = CheckCreateRequest(request);
                if (!String.IsNullOrEmpty(message))
                {
                    return new CreateResponse
                    {
                        ErrorMessage = message,
                        Result = ResponseResult.Error
                    };
                }
                var orderId = GenerateOrderId(ConfigurationManager.AppSettings["GenerationBaseId"]);
                var orders = request.Delivery.PickupPlaceSapCode != null ? request.Delivery.PickupPlaceSapCode.Split(';') : new string[0];
                var response = client.CreateOrder(new RouterLink.ClientData
                    {
                        Name = String.IsNullOrEmpty(request.Customer.Name) ? "" : request.Customer.Name,
                        Surname =
                            String.IsNullOrEmpty(request.Customer.Surname)
                                ? request.SystemComment
                                : request.Customer.Surname,
                        Email = request.Customer.Email,
                        Phone = request.Customer.Phone,
                    }, new RouterLink.CreateOrderData
                        {
                            ExternalSystem = RouterLink.ExternalSystem.ZZT,
                            OrderSource = RouterLink.OrderSource.Metro,
                            OrderId = orderId,
                            PaymentType = RouterLink.PaymentType.Cash,
                            StoreInfo = new RouterLink.StoreInfo {SapCode = request.SapCode},
                            ZZTData = new RouterLink.ZZTHeaderData
                                {
                                    DeliveryAddress = new RouterLink.ZZTDeliveryAddress
                                        {
                                            Street = request.Delivery.Address,
                                            Additional = request.CommentFromCustomer
                                        },
                                    DeliveryInfo = new RouterLink.ZZTDeliveryInfo
                                        {
                                            DeliveryPrice = request.Delivery.DeliveryPrice,
                                            DeliveryPlaceSapCode =
                                                orders.Length > 1 ? orders[0] : request.Delivery.PickupPlaceSapCode,
                                            DeliveryPlaceId =
                                                orders.Length > 1 ? orders[1] : request.Delivery.PickupPlaceId,
                                            DeliveryType = request.Delivery.DeliveryType,
                                            DeliveryDate = request.Delivery.DeliveryDate
                                        },
                                    ConsumerId = request.Consumer,
                                    CampaignId = request.Articles.First().CampaignId
                                }
                        }, new RouterLink.DeliveryInfo(),
                                                  request.Articles.Select(m => new RouterLink.ArticleData
                                                      {
                                                          ArticleNum = m.Id.ToString(),
                                                          Price = m.Price,
                                                          Qty = m.Qty
                                                      }).ToArray());
                return new CreateResponse
                    {
                        OrderId = response.OrderId,
                        Result =
                            response.ReturnCode == RouterLink.ReturnCode.Ok ? ResponseResult.OK : ResponseResult.Error,
                        ErrorMessage = response.ErrorMessage
                    };
            }
            catch(Exception ex)
            {
                return new CreateResponse
                {
                    Result = ResponseResult.Error,
                    ErrorMessage = "Internal error " + ex.Message
                };
            }
        }

        public static CreateResponse CreateInternalOrder(CreateRequest request)
        {
            try
            {
                var client = new OrderLink.OrderServiceClient();
                var message = CheckCreateRequest(request);
                if (!String.IsNullOrEmpty(message))
                {
                    return new CreateResponse
                    {
                        ErrorMessage = message,
                        Result = ResponseResult.Error
                    };
                }
                var orderId = GenerateOrderId("999");
                var response = client.CreateOrder(new OrderLink.ClientData
                {
                    Name = String.IsNullOrEmpty(request.Customer.Name) ? "" : request.Customer.Name,
                    Surname = String.IsNullOrEmpty(request.Customer.Surname) ? request.SystemComment : request.Customer.Surname,
                    Email = request.Customer.Email,
                    Phone = request.Customer.Phone,
                }, new OrderLink.CreateOrderData
                {
                    ExternalSystem = OrderLink.ExternalSystem.ZZT,
                    OrderSource = OrderLink.OrderSource.Metro,
                    OrderId = orderId,
                    Comment = request.CommentFromCustomer,
                    PaymentType = OrderLink.PaymentType.Cash,
                    StoreInfo = new OrderLink.StoreInfo { SapCode = request.SapCode },
                    ZZTData = new OrderLink.ZZTHeaderData
                    {
                        DeliveryInfo = new OrderLink.ZZTDeliveryInfo
                        {
                            DeliveryPlaceSapCode = request.Delivery.PickupPlaceSapCode,
                            DeliveryPlaceId = request.Delivery.PickupPlaceId,
                            DeliveryType = request.Delivery.DeliveryType
                        },
                        ConsumerId = request.Consumer,
                        CampaignId = request.Articles.First().CampaignId
                    }
                }, new OrderLink.DeliveryInfo { },
                request.Articles.Select(m => new OrderLink.ArticleData
                {
                    ArticleNum = m.Id.ToString(),
                    Price = m.Price,
                    Qty = m.Qty,
                    Title = m.Name
                }).ToArray());
                return new CreateResponse
                {
                    OrderId = response.OrderId,
                    Result = response.ReturnCode == OrderLink.ReturnCode.Ok ? ResponseResult.OK : ResponseResult.Error,
                    ErrorMessage = response.ErrorMessage
                };
            }
            catch(Exception ex)
            {
                return new CreateResponse
                {
                    Result = ResponseResult.Error,
                    ErrorMessage = "Internal error"
                };
            }
        }

        public static ValidateResponse ValidateOrder(ValidateRequest request)
        {
            try
            {
                var validateResp = new ValidateResponse();
                if (!PreCheckValidateRequest(request, ref validateResp))
                    return validateResp;
                var productArticles = request.Articles.Where(m => m.Type == ArticleType.Product).ToList();
                var campaignName = productArticles.Select(m => m.CampaignId).Distinct().ToList();
                var client = new BasketLink.BasketServiceClient();
                var response = client.Validate(new BasketLink.ValidateRequest
                {
                    Campaign = campaignName[0],
                    Consumer = request.Consumer,
                    Articles = productArticles.Select(m => long.Parse(m.Id)).ToArray()
                });
                if (!PostCheckValidateRequest(request, response, ref validateResp))
                    return validateResp;
                var shopsCodes = new List<List<string>>();
                var responseArticles = new List<ArticleData>();
                var deliverySapCodes = response.Shops.Where(m => m.Chain == BasketLink.ChainIdEnum.ZZT).Select(m => m.SapCode).ToList();
                var zztPickup = CoachDataLoader.GetMetroPickup();
                foreach (var requestArticle in request.Articles)
                {
                    var responseArticle = response.Articles.FirstOrDefault(m => m.Article.ToString() == requestArticle.Id);
                    int minStoreCount;
                    if (!Int32.TryParse(ConfigurationManager.AppSettings["MinStoreCount"], out minStoreCount))
                    {
                        minStoreCount = 3;
                    }
                    var availableShops =
                        responseArticle.ShopData.Where(m => m.Qty >= requestArticle.Qty && m.Qty >= minStoreCount)
                                       .Select(m => m.SapCode)
                                       .ToList();
                    var deliveryTypes = new List<string>();
                    if (availableShops.Count > 0)
                    {
                        deliveryTypes = DeliveryTypesProcessing.GetTypesByDeliverySaps(deliverySapCodes, availableShops).Select(m => m.Key).ToList();
                    }
                    var places = availableShops.Where(m => !deliverySapCodes.Contains(m)).Select(m => new PickupPoint { SapCode = m }).ToList();
                    if (availableShops.Any(deliverySapCodes.Contains))
                    {
                        var firstDeliveryCode = deliverySapCodes.First();
                        places.AddRange(zztPickup.Select(m => new PickupPoint { SapCode = firstDeliveryCode + ";" + m.PickUpPointId }));
                    }
                    responseArticles.Add(new ArticleData
                    {
                        Id = responseArticle.Article.ToString(),
                        Name = responseArticle.ModelType + " " + responseArticle.Model,
                        Price = responseArticle.Price.Value,
                        Places = places,
                        DeliveryTypes = deliveryTypes
                    });
                    shopsCodes.Add(availableShops);
                }

                var data = shopsCodes.First();
                var commonCodes = data.Where(item => shopsCodes.All(m => m.Any(z => z == item))).ToList();
                var commonPlaces = commonCodes.Select(m =>
                {
                    var shop = response.Shops.Single(z => z.SapCode == m);
                    var point = shop.ToModel();
                    return point;
                }).OrderBy(m => m.SapCode).ToList();

                var zztElem = commonPlaces.SingleOrDefault(m => m.Type == PointType.ZZT);
                if (zztElem != null)
                {
                    var metroPoints = zztPickup.Select(m => m.ToModel(zztElem.SapCode));
                    commonPlaces.AddRange(metroPoints);
                    commonPlaces.Remove(zztElem);
                }
                return new ValidateResponse
                {
                    Result = ResponseResult.OK,
                    Places = commonPlaces,
                    Articles = responseArticles
                };
            }
            catch(Exception)
            {
                return new ValidateResponse
                {
                    ErrorObjectType= ErrorObject.Internal,
                    Result = ResponseResult.Error,
                    ErrorMessage = "Internal error"
                };
            }
        }

        public static GlobalZonesResponse GetGlobalZones()
        {
            try
            {
                var collection = ZonesProcessing.GetGlobalZones();
                return new GlobalZonesResponse
                {
                    Result = ResponseResult.OK,
                    Zones = collection.GlobalZoneInfoList
                };
            }
            catch(Exception)
            {
                return new GlobalZonesResponse
                {
                    Result = ResponseResult.Error,
                    ErrorMessage = "Internal error"
                };
            }
        }

    }
}