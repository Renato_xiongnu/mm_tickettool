﻿using MMS.Storage.CouchDB.DreamSeat;
using TicketTool.Integration.ZZT.Storage.Implementation;
using TicketTool.Integration.ZZT.Storage.Interfaces;

namespace TicketTool.Integration.ZZT.Storage
{
    public static class StorageFactory
    {
        public static IStorage Create()
        {
            return new MasterStorage(new CouchDbStorage());
        }
    }
}
