﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicketTool.Integration.ZZT.Storage.Interfaces
{
    public interface IStorage
    {
        TEntity GetEntity<TEntity>(string key) where TEntity : class, new();
        ICollection<TEntity> GetEntities<TEntity>(ICollection<string> keys) where TEntity : class, new();
        ICollection<TEntity> GetEntities<TEntity>(string viewName, ICollection<string> keys) where TEntity : class, new();
    }
}
