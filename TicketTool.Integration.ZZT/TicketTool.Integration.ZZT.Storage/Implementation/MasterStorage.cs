﻿using System.Linq;
using System.Collections.Generic;
using MMS.PushServices.Interfaces;
using TicketTool.Integration.ZZT.Storage.Interfaces;
using System;

namespace TicketTool.Integration.ZZT.Storage.Implementation
{
    public class MasterStorage : IStorage
    {
        private readonly IEntityStorage _entityStorage;

        public MasterStorage(IEntityStorage entityStorage)
        {
            _entityStorage = entityStorage;
        }

        public TEntity GetEntity<TEntity>(string key) where TEntity : class, new()
        {
            return _entityStorage.GetEntity<TEntity>(key);
        }

        public ICollection<TEntity> GetEntities<TEntity>(ICollection<string> keys) where TEntity : class, new()
        {
            return _entityStorage.GetEntities<TEntity>(keys);
        }

        public ICollection<TEntity> GetEntities<TEntity>(string viewName, ICollection<string> fields)
            where TEntity : class, new()
        {
            if (fields.Count > 1)
                return _entityStorage.GetEntities<TEntity>("utils", viewName, fields);
            if (fields == null || !fields.Any())
                return _entityStorage.GetEntities<TEntity>("utils", viewName);
            return _entityStorage.GetEntities<TEntity>("utils", viewName, fields.First());
        }
    }
}
