﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicketTool.Integration.ZZT.Basket.Models
{
    public class ValidateRequest
    {
        public ValidateRequest() 
        {
            articles = new List<ArticleBaseData>();
        }

        public string consumer { get; set; }
        public List<ArticleBaseData> articles { get; set; }
    }

}