﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicketTool.Integration.ZZT.Basket.Models
{
    public class DeliveryRequest
    {
        public string address { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
    }

}