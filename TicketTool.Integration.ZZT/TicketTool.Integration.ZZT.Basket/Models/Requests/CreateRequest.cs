﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicketTool.Integration.ZZT.Basket.Models
{
    public class CreateRequest
    {
        public CreateRequest() 
        {
            articles = new List<ArticleBaseData>();
            customer = new CustomerData();
        }

        public string system_comment { get; set; }
        public string comment_from_customer { get; set; }
        public string sap_code { get; set; }
        public string consumer { get; set; }

        public DeliveryData delivery { get; set; }
        public List<ArticleBaseData> articles { get; set; }
        public CustomerData customer { get; set; }

    }

}