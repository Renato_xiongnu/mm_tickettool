﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicketTool.Integration.ZZT.Basket.Models
{
    public class ArticleBaseData
    {
        public string id { get; set; }
        public int qty { get; set; }
        public decimal price { get; set; }
        public string type { get; set; }
        public string title { get; set; }
        public string campaign_id { get; set; }
    }
}