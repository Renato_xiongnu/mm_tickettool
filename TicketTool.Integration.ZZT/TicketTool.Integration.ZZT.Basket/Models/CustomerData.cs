﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicketTool.Integration.ZZT.Basket.Models
{
    public class CustomerData
    {
        public string name { get; set; }
        public string surname { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
    }
}