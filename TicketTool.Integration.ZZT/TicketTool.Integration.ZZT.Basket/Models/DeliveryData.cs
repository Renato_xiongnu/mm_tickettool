﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicketTool.Integration.ZZT.Basket.Models
{
    public class DeliveryData
    {
        public string address { get; set;}
        public string address_longitude { get; set; }
        public string address_latitude { get; set; }
        public string delivery_type { get; set; }
        public string delivery_date { get; set; }
        public decimal? delivery_price { get; set; }
        public string pickup_place_sapcode { get; set; }
        public string pickup_place_id { get; set; }
    }
}