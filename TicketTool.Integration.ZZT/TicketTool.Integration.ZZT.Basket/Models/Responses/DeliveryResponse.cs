﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TicketTool.Integration.ZZT.Basket.Common;

namespace TicketTool.Integration.ZZT.Basket.Models
{
    public class DeliveryResponse
    {
        public DeliveryResponse()
        {
            zones = new List<string>();
        }

        public string x { get; set; }
        public string y { get; set; }
        public string delivery_price { get; set; }

        public List<string> zones { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public ResponseResult result { get; set; }

        public string error_message { get; set; }
    }

}