﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TicketTool.Integration.ZZT.Basket.Common;

namespace TicketTool.Integration.ZZT.Basket.Models
{
    public class ValidateResponse
    {
        public ValidateResponse()
        {
            places = new List<PickupPoint>();
            articles = new List<ArticleData>();
        }

        [JsonConverter(typeof(StringEnumConverter))]
        public ResponseResult result { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public ErrorObject error_object_type { get; set; }

        public string error_message { get; set; }
        public string error_object_id { get; set; }
        public List<PickupPoint> places { get; set; }
        public List<ArticleData> articles { get; set; }
    }
}