﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TicketTool.Integration.ZZT.Basket.Common;

namespace TicketTool.Integration.ZZT.Basket.Models
{
    public class CreateResponse
    {
        public CreateResponse()
        {
        }

        [JsonConverter(typeof(StringEnumConverter))]
        public ResponseResult result { get; set; }

        public string error_message { get; set; }
        public string order_id { get; set; }
    }
}