﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicketTool.Integration.ZZT.Basket.Models
{
    public class PickupPoint
    {
        public PickupPoint()
        {
            articles = new List<ArticleData>();
            types = new Dictionary<string, string>();
        }
        public string code { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string work_time { get; set; }

        public Dictionary<string, string> types { get; set; }

        public string latitude { get; set; }
        public string longitude { get; set; }

        public List<ArticleData> articles { get; set; }
    }
}