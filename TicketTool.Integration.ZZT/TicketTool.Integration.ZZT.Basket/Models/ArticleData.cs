﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicketTool.Integration.ZZT.Basket.Models
{
    public class ArticleData : ArticleBaseData
    {
        public int available_qty { get; set; }
        public string name { get; set; }
    }
}