﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicketTool.Integration.ZZT.Basket.Common
{
    public enum ResponseResult
    {
        OK, 
        Error
    }
    public enum ErrorObject
    {
        Article = 0,
        Global = 1,
        Internal = 2
    }
}