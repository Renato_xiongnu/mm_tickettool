﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using TicketTool.Integration.ZZT.Basket.Models;

namespace TicketTool.Integration.ZZT.Basket.Common
{
    public static class Extensions
    {
        #region [BasketContract]

        public static ValidateResponse ToModel(this BasketServiceLink.ValidateResponse response)
        {
            return new ValidateResponse
            {
                 error_message = response.ErrorMessage,
                 error_object_id = response.ErrorObjectId,
                 error_object_type = (ErrorObject)Enum.ToObject(typeof(ErrorObject), response.ErrorObjectType),
                 result = (ResponseResult)Enum.ToObject(typeof(ResponseResult), response.Result),
                 articles = response.Articles.Select(m => m.ToModel()).ToList(),
                 places = response.Places.Select(m => m.ToModel()).ToList()
            };
        }

        public static PickupPoint ToModel(this BasketServiceLink.PickupPoint data)
        {
            return new PickupPoint
            {
                address = data.Address,
                articles = data.Articles.Select(m => m.ToModel()).ToList(),
                code = data.SapCode,
                latitude = data.Latitude,
                longitude = data.Longitude,
                name = data.Name,
                phone = data.Phone,
                types = data.Types,
                work_time = data.WorkTime
            };
        }

        public static ArticleData ToModel(this BasketServiceLink.ArticleData data)
        {
            return new ArticleData 
            {
                 available_qty = data.AvailableQty,
                 campaign_id = data.CampaignId,
                 id = data.Id,
                 name = data.Name,
                 price = data.Price,
                 qty = data.Qty
            };
        }

        public static CreateResponse ToModel(this BasketServiceLink.CreateResponse data)
        {
            return new CreateResponse
            {
                 error_message = data.ErrorMessage,
                 order_id = data.OrderId,
                 result = (ResponseResult)Enum.ToObject(typeof(ResponseResult), data.Result),
            };
        }

        public static DeliveryResponse ToModel(this BasketServiceLink.DeliveryResponse data)
        {
            return new DeliveryResponse
            {
                 //delivery_price = data.DeliveryPrice,
                 delivery_price = data.Price,
                 error_message = data.ErrorMessage,
                 x = data.X,
                 y = data.Y,
                 zones = data.Zones != null ? data.Zones.ToList() : new List<string>()
            };
        }

        public static BasketServiceLink.ValidateRequest ToContract(this ValidateRequest request)
        {
            return new BasketServiceLink.ValidateRequest 
            {
                 Articles = request.articles.Select(m => m.ToContract()).ToArray(),
                 Consumer = request.consumer
            };
        }

        public static BasketServiceLink.ArticleData ToContract(this ArticleBaseData data)
        {
            return new BasketServiceLink.ArticleData
            {
                CampaignId = data.campaign_id,
                Id = data.id,
                Price = data.price,
                Qty = data.qty,
                Name = data.title,
                Type = (BasketServiceLink.ArticleType)Enum.Parse(typeof(BasketServiceLink.ArticleType), data.type)
            };
        }

        public static BasketServiceLink.CustomerData ToContract(this CustomerData data)
        {
            return new BasketServiceLink.CustomerData
            {
                Email = data.email,
                Name = data.name,
                Phone = data.phone,
                Surname = data.surname
            };
        }

        public static BasketServiceLink.DeliveryData ToContract(this DeliveryData data)
        {
            DateTime date;
            return new BasketServiceLink.DeliveryData
            {
                DeliveryPrice = data.delivery_price,
                Address = data.address,
                PickupPlaceId = data.pickup_place_id,
                PickupPlaceSapCode = data.pickup_place_sapcode,
                DeliveryType = data.delivery_type,
                DeliveryDate = !String.IsNullOrEmpty(data.delivery_date) && 
                                DateTime.TryParseExact(data.delivery_date, "dd.MM.yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date) ? date : (DateTime?)null,
                Latitude = data.address_latitude,
                Longitude = data.address_longitude
            };
        }

        public static BasketServiceLink.CreateRequest ToContract(this CreateRequest request)
        {
            return new BasketServiceLink.CreateRequest
            {
                 Articles = request.articles == null ? null : request.articles.Select(m => m.ToContract()).ToArray(),
                 CommentFromCustomer = request.comment_from_customer,
                 Consumer = request.consumer,
                 Customer = request.customer == null ? null : request.customer.ToContract(),
                 Delivery = request.delivery == null ? null : request.delivery.ToContract(),
                 SapCode = request.sap_code,
                 SystemComment = request.system_comment
            };
        }

        public static BasketServiceLink.DeliveryPriceRequest ToContract(this DeliveryRequest request)
        {
            return new BasketServiceLink.DeliveryPriceRequest 
            {
                Address = request.address,
                Latitude = request.latitude,
                Longitude = request.longitude,
            };
        }

        #endregion
    }
}