﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using TicketTool.Integration.ZZT.Basket.Common;
using TicketTool.Integration.ZZT.Basket.Models;

namespace TicketTool.Integration.ZZT.Basket.Controllers
{
    public class BasketController : Controller
    {
        readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public ActionResult Validate(ValidateRequest request)
        {
            log.Info("Validate");
            log.Info(JsonConvert.SerializeObject(request));
            JsonNetResult jsonNetResult = new JsonNetResult();
            jsonNetResult.Formatting = Formatting.Indented;
            var client = new BasketServiceLink.BasketServiceClient();
            log.Info("Validate response");
            var result = client.Validate(request.ToContract()).ToModel();
            log.Info(JsonConvert.SerializeObject(result));
            jsonNetResult.Data = result;
            return jsonNetResult;
        }

        public ActionResult GetDeliveryInfo(DeliveryRequest request)
        {
            log.Info("Delivery");
            log.Info(JsonConvert.SerializeObject(request));
            JsonNetResult jsonNetResult = new JsonNetResult();
            jsonNetResult.Formatting = Formatting.Indented;
            var client = new BasketServiceLink.BasketServiceClient();
            log.Info("Delivery response");
            var result = client.GetDeliveryPrice(request.ToContract()).ToModel();
            log.Info(JsonConvert.SerializeObject(result));
            jsonNetResult.Data = result;
            return jsonNetResult;
        }

        public ActionResult CreateOrder(CreateRequest request)
        {
            log.Info("CreateOrder");
            log.Info(JsonConvert.SerializeObject(request));
            JsonNetResult jsonNetResult = new JsonNetResult();
            jsonNetResult.Formatting = Formatting.Indented;
            var client = new BasketServiceLink.BasketServiceClient();
            log.Info("Create response");
            var result = client.CreateOrder(request.ToContract()).ToModel();
            log.Info(JsonConvert.SerializeObject(result));
            jsonNetResult.Data = result;
            return jsonNetResult;
        }
    }
}
