﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using TicketTool.Integration.ZZT.Basket.Common;
using TicketTool.Integration.ZZT.Basket.Models;

namespace TicketTool.Integration.ZZT.Basket.Controllers
{
    public class TestController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
